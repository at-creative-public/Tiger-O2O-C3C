# Setup O2O-C3C

System requirements:

1. PHP 7.3 or above;
2. Composer
3. MySQL or MariaDB;
4. Apache 2.4 with modRewrite enabled
5. Apple Pass Type ID (.p12 + .p8)
6. Google Wallet API
7. SendGrid API

Procedure:

1. Copy all codes and folder to your document root
2. Run 'composer install' in command prompt
3. Import tiger_migration.sql to your database
4. Follow the Tiger_migration_user_manual.txt and fill in the variables.
