$(document).ready(function () {
    $('.btn-air-freight').on('click', function () {
        window.location.href = site_url + "services/air_freight";
    });
    $('.btn-sea-freight').on('click', function () {
        window.location.href = site_url + "services/sea_freight";
    });
});