$(function () {
    $('.navbar-nav li a').filter(function (i) {
        return $(this).attr('href') === '/'
    }).css('color', '#0071bc');
    $('.rowcontent').find('p:eq(0)').css({
        'margin-bottom': '1em'
    });
    $('.btn-language').on('click', function (event) {
        event.preventDefault();
        window.location.href = site_url + 'portal/change_language/' + $(this).attr('data-lang');
    });
});
