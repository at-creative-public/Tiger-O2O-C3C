$(document).ready(function () {

	$('.menu-controller').on('click', function() {
		var id = $(this).attr('href');
		var section = $(this).attr('data-section');
		$('.' + section + ' .menu-controller').removeClass('active');
		$('.' + section + ' .tab-pane').removeClass('active');
		$(id).addClass('active');
		$(this).addClass('active');
	})

});