
const ctx = document.getElementById('bar_chart').getContext('2d');
const bar_chart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});


$.ajax({
    url: base_url + "/merchant/dashboard/get_issued_number",
    method: "post",
    data: [],
    dataType: "json",
    success: function (data) {
        console.log(data);
        $("#total_issued").text(data.total);
        $("#total_membership").text(data.result.membership);
        $("#total_coupon").text(data.result.coupon);
        $("#total_event_ticket").text(data.result.event_ticket);

    }
})

$.ajax({
    url: base_url + "/merchant/dashboard/get_pass_distribution",
    method: "post",
    data: {},
    dataType: "json",
    success: function (data) {
        var label_set = JSON.parse(data.result);
        console.log(Object.keys(data.result));
        console.log(label_set);
        $("#total_pass").text(data.total);
        $("#membership_passes").text(label_set.membership + " (" + ((label_set['membership'] / data.total) * 100).toFixed(2) + "%)");
        $("#coupon_passes").text(label_set.coupon + " (" + ((label_set['coupon'] / data.total) * 100).toFixed(2) + "%)");
        $("#event_ticket_passes").text(label_set.event_ticket + " (" + ((label_set['event_ticket'] / data.total) * 100).toFixed(2) + "%)");
        //console.log(label_set);
        //label_set["coupon"] = ((label_set['coupon'] / data.total) * 100);
        //label_set["event_ticket"] = ((label_set['event_ticket'] / data.total) * 100);
        //label_set["membership"] = ((label_set['membership'] / data.total) * 100);
        const ctx2 = document.getElementById('pie_chart').getContext('2d');
        const pie_chart = new Chart(ctx2, {
            type: 'doughnut', data: {
                labels: Object.keys(label_set),
                datasets: [{
                    label: 'My First Dataset',
                    data:
                        Object.values(label_set)
                    ,
                    backgroundColor: [
                        'rgb(255, 99, 132)',
                        'rgb(54, 162, 235)',
                        'rgb(255, 205, 86)'
                    ],
                    hoverOffset: 4

                }]
            }
        });
    }


})

$.ajax(
    {
        url: base_url + "/merchant/dashboard/get_top_ten_users",
        data: { "pass_id": $("#selection_index").val() },
        dataType: "json",
        method: "post",
        success: function (data) {

            $("#table_selection_bar").empty();
            var index_first = true;
            for (var i in data.level) {
                console.log(i);
                console.log(data.level);
                var keys = Object.keys(data.level);
                console.log(keys);
                //console.log(data.name[keys[i]]);
                $("#table_selection_bar").append(
                    '<a class="tab_button ' + (index_first === true ? "active" : "") + '" data-id="' + i + '"  href="#t_' + [i] + '" data-toggle="tab" role="tab">' + data.name[i] + '</a>'
                );
                index_first = false;

            }
            $("#table_tabs").empty();
            var first = true;
            for (var i in data.level) {
                var append_string = "";
                $("#table_tabs").append(
                    '<div class="tab-pane" id="t_' + i + '">'
                );
                if (first == true) {
                    first_default = "show active";
                } else {
                    first_default = "";
                }
                append_string = append_string + '<div class="tab-pane fade ' + first_default + '" id="t_' + i + '">';
                append_string = append_string + '<table class="table"><thead><th>ID</th><th>Name</th><th>Points</th></thead><tbody>';
                //$("#table_tabs").append('<table class="table"><thead><th>ID</td><th>Points</td></thead>');
                //$("#table_tabs").append("<tbody>");
                for (var y in data.level[i]) {

                    // $("#table_tabs").append(
                    //     '<tr><td>' + data.level[i][y].id + '</td><td>' + data.level[i][y].points + '</td></tr>'
                    // );

                    append_string = append_string + '<tr><td>' + data.level[i][y].account_id + '</td><td>' + data.level[i][y].display_name + '</td><td>' + data.level[i][y].points + '</td></tr>';
                }
                append_string = append_string + "</tbody></table>";
                append_string = append_string + "</div>";
                //$("#table_tabs").append("</tbody></table>");

                $("#table_tabs").append(
                    "</div>"
                );

                $("#table_tabs").append(append_string);
                first = false;
            }
            button_init();


        }

    }
)



function update_top_ten(index) {
    $.ajax(
        {
            url: base_url + "/merchant/dashboard/get_top_ten_users",
            data: { "pass_id": index },
            dataType: "json",
            method: "post",
            success: function (data) {
                if (data.api == 1) {
                    console.log("This case");
                    $("#top_ten_selection").hide();
                    $("#warning_box").show();
                } else {
                    $("#warning_box").hide();
                    $("#table_selection_bar").empty();
                    var index_first = true;
                    for (var i in data.level) {
                        console.log(i);
                        console.log(data.level);
                        var keys = Object.keys(data.level);
                        console.log(keys);
                        //console.log(data.name[keys[i]]);
                        $("#table_selection_bar").append(
                            '<a class="tab_button ' + (index_first === true ? "active" : "") + '" data-id="' + i + '"  href="#t_' + [i] + '" data-toggle="tab" role="tab">' + data.name[i] + '</a>'
                        );
                        index_first = false;

                    }
                    $("#table_tabs").empty();
                    var first = true;
                    for (var i in data.level) {
                        var append_string = "";
                        /*$("#table_tabs").append(
                            '<div class="tab-pane" id="t_' + i + '">'
                        );*/
                        if (first == true) {
                            first_default = "show active";
                        } else {
                            first_default = "";
                        }
                        append_string = append_string + '<div class="tab-pane fade ' + first_default + '" id="t_' + i + '">';
                        append_string = append_string + '<table class="table"><thead><th>ID</th><th>Name</th><th>Points</th></thead><tbody>';
                        //$("#table_tabs").append('<table class="table"><thead><th>ID</td><th>Points</td></thead>');
                        //$("#table_tabs").append("<tbody>");
                        for (var y in data.level[i]) {
                            /*
                            $("#table_tabs").append(
                                '<tr><td>'+data.level[i][y].id+'</td><td>'+data.level[i][y].points+'</td></tr>'
                            );
                            */
                            append_string = append_string + '<tr><td>' + data.level[i][y].account_id + '</td><td>' + data.level[i][y].display_name + '</td><td>' + data.level[i][y].points + '</td></tr>';
                        }
                        append_string = append_string + "</tbody></table>";
                        append_string = append_string + "</div>";
                        //$("#table_tabs").append("</tbody></table>");
                        /*
                        $("#table_tabs").append(
                            "</div>"
                        );
                         */
                        $("#table_tabs").append(append_string);
                        first = false;
                    }
                    button_init();
                    $("#top_ten_selection").show();

                }
            }

        }
    )
}

function button_init() {
    $(".tab_button").click(function () {
        $(".tab_button").removeClass("active");
        $(this).addClass("active");
        $(".tab-pane").removeClass("show active");
        $("#t_" + $(this).data("id")).addClass("show active");
    })
}

$("#selection_type").on("change", function () {
    $("#selection_index").empty();
    switch ($(this).val()) {
        case "overall":
            $("#selection_index").append("<option>----------</option>");
            break;
        case "membership":
            $.ajax({
                url: base_url + "merchant/dashboard/update_selection",
                data: { "selection": "membership" },
                method: "post",
                dataType: "json",
                success: function (data) {
                    for (var i in data.result) {
                        $("#selection_index").append('<option value="' + data.result[i].id + '">' + data.result[i].project_name + '</option>');
                    }
                }
            })
            break;
        case "coupon":
            $.ajax({
                url: base_url + "merchant/dashboard/update_selection",
                data: { "selection": "coupon" },
                method: "post",
                dataType: "json",
                success: function (data) {
                    for (var i in data.result) {
                        $("#selection_index").append('<option value="' + data.result[i].id + '">' + data.result[i].project_name + '</option>');
                    }
                }
            })
            break;
        case "event_ticket":
            $.ajax({
                url: base_url + "merchant/dashboard/update_selection",
                data: { "selection": "event_ticket" },
                method: "post",
                dataType: "json",
                success: function (data) {
                    for (var i in data.result) {
                        $("#selection_index").append('<option value="' + data.result[i].id + '">' + data.result[i].event_name + '</option>');
                    }
                }
            })
            break;
    }
})

$("#review_button").on("click", function () {
    var selected_type = $("#selection_type").val();
    var selected_index = $("#selection_index").val();
    if (selected_type == "membership") {
        update_top_ten(selected_index);

    } else {
        $("#top_ten_selection").hide();
    }
})

$(document).ready(function () {
    //$("#review_button").trigger("click");    
})


