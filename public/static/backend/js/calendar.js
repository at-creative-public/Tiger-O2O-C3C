/******/ (function (modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if (installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
            /******/
        }
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
            /******/
        };
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
        /******/
    }
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function (exports, name, getter) {
/******/ 		if (!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
        /******/
    });
            /******/
        }
        /******/
    };
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function (module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
        /******/
    };
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function (object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
    /******/
})
/************************************************************************/
/******/({

/***/ "./resource/js/pages/schedule-calendar.js":
/***/ (function (module, __webpack_exports__, __webpack_require__) {

            "use strict";
            Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
    /* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils_url__ = __webpack_require__("./resource/js/utils/url.js");
    /* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_strings__ = __webpack_require__("./resource/js/utils/strings.js");
    /* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utils_date_picker__ = __webpack_require__("./resource/js/utils/date-picker.js");




            var _parseURL = Object(__WEBPACK_IMPORTED_MODULE_0__utils_url__["a" /* default */])(),
                url = _parseURL.url,
                query = _parseURL.query,
                searchParams = _parseURL.searchParams,
                reload = _parseURL.reload; //---currently happy coding
            // Select a default display mode directly from the view 

            window.pageVh = _pageVh;
            window.cursorMode = searchParams.get('mode') ? searchParams.get('mode') : $('.schedule-calendar-mode[data-mode="display"]').find("option:selected").val();
            window.calendarWeekdays = [1, 2, 3, 4, 5, 6];
            window.events = _events;
            window.resources = _resources;
            window.calendarIndentation = false;
            console.log(Object(__WEBPACK_IMPORTED_MODULE_0__utils_url__["a" /* default */])()); //--- Vertical Spinbox

            var vSpinbox = $(".calendar-toolbar .vertical-spinbox"); // datepicker

            var vSpinboxDatepicker = vSpinbox.find(".date-picker");
            Object(__WEBPACK_IMPORTED_MODULE_2__utils_date_picker__["a" /* datepicker */])(vSpinboxDatepicker); // datepicker on change date event listener and handler function

            vSpinboxDatepicker.on("changeDate", function (e) {
                var _this = $(e.currentTarget);

                window.cursorDate = _this.val();

                _initVSpinbox(window.cursorDate);

                // _initCalendar(window.cursorDate);

                // _initCalendarComplete();

                //changed to use scheduleCalendar by jack at 2020-11-04
                scheduleCalendarRefreshHandler();


            }); // btn value left and btn value right

            var vSpinboxBtnValueLeft = vSpinbox.find(".btn-value-left");
            var vSpinboxBtnValueRight = vSpinbox.find(".btn-value-right"); // btn value left and btn value right on click event listener and handler function

            [vSpinboxBtnValueLeft, vSpinboxBtnValueRight].forEach(function (element) {
                element.click(function (e) {
                    var _this = $(e.currentTarget);

                    window.cursorDate = _this.data("date");

                    _initVSpinbox(window.cursorDate);

                    // _initCalendar(window.cursorDate);

                    // _initCalendarComplete();

                    //added by jack 2020-11-04
                    //refresh calendar with date mode
                    scheduleCalendarRefreshHandler();

                });
            }); // initialize, and change the date vertical spinbox behaviour

            var _initVSpinbox = function _initVSpinbox() {
                var _date = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

                if (_date === null) {
                    _date = searchParams.get("date") ? searchParams.get("date") : new Date();
                }

                vSpinboxDatepicker.val(moment(_date).format("YYYY-MM-DD"));
                console.log(window.cursorMode);

                switch (window.cursorMode) {
                    case "d":
                        vSpinboxBtnValueLeft.data("date", moment(_date).subtract(1, "d").format("YYYY-MM-DD"));
                        vSpinboxBtnValueRight.data("date", moment(_date).add(1, "d").format("YYYY-MM-DD"));
                        break;

                    case "w":
                        vSpinboxBtnValueLeft.data("date", moment(_date).subtract(7, "d").format("YYYY-MM-DD"));
                        vSpinboxBtnValueRight.data("date", moment(_date).add(7, "d").format("YYYY-MM-DD"));
                        break;

                    case "m":
                        /**
                         * How to subtract one month using moment.js
                         * https://stackoverflow.com/questions/41505492/how-to-subtract-one-month-using-moment-js/41505553
                         */
                        vSpinboxBtnValueLeft.data("date", moment(_date).subtract(1, "months").format("YYYY-MM-DD"));
                        vSpinboxBtnValueRight.data("date", moment(_date).add(1, "months").format("YYYY-MM-DD"));
                        break;
                }
            };

            _initVSpinbox();

            $('.schedule-calendar-mode[data-mode="display-resource"]').find("option").each(function (index, element) {
                console.log($(element).val());

                if ($(element).val() == searchParams.get("resource")) {
                    $(element).attr("selected", true);
                }
            });
            $(".schedule-calendar-mode").each(function (index, element) {
                var mode = $(this).data("mode"); // on change event listener and handler

                $(this).change(function () {
                    var value;
                    console.log(mode);

                    switch (mode) {
                        case "display":
                            value = $(this).find("option:selected").val();

                            switch (value) {
                                case "d":
                                case "w":
                                case "m":
                                case "mc":
                                case "ms":
                                    window.cursorMode = value;

                                    _initCalendar(window.cursorDate);

                                    //_initCalendarComplete();
                                    //change to use refresher by jack at 2020-11-04
                                    scheduleCalendarRefreshHandler();

                                    break;

                                default:
                                    window.cursorMode = 'd';

                                    _initCalendar(window.cursorDate);

                                    //_initCalendarComplete();
                                    //change to use refresher by jack at 2020-11-04
                                    scheduleCalendarRefreshHandler();

                                    break;
                            }

                            break;

                        case "display-resource":
                            scheduleCalendarRefreshHandler();
                            break;

                        case "filter":
                        case "and-search":
                            value = $('.schedule-calendar-mode[data-mode="and-search"]').val();

                            if (value) {
                                var filterValue = $('.schedule-calendar-mode[data-mode="filter"] option:selected').val();
                                console.log(filterValue);

                                switch (filterValue) {
                                    case "course":
                                        $('a.calendar-event:not([students-no-results]) .course-details .title:not(:contains("' + value + '"))').closest("a.calendar-event").attr("filter-no-results", true);
                                        $('a.calendar-event:not([students-no-results]) .course-details .title:contains("' + value + '")').closest("a.calendar-event").removeAttr("filter-no-results");
                                        break;

                                    case "teacher":
                                        $("a.calendar-event:not([students-no-results]) .course-details").each(function (index, element) {
                                            var str = $(this).data("teachers");

                                            if (str.indexOf(value) >= 0) {
                                                $(this).closest("a.calendar-event").removeAttr("filter-no-results");
                                            } else {
                                                $(this).closest("a.calendar-event").attr("filter-no-results", true);
                                            }
                                        });
                                        break;
                                }

                                break;
                            } else {
                                $("a.calendar-event").removeAttr("filter-no-results");
                            }

                            break;

                        case "student":
                            value = $(this).val();
                            //alert('student search');
                            if (value) {
                                $("a.calendar-event:not([filter-no-results]) .course-details").each(function (index, element) {
                                    var str = $(this).data("students");
                                    // alert("students data"+str);
                                    if (str.indexOf(value) >= 0) {
                                        $(this).closest("a.calendar-event").removeAttr("students-no-results");
                                    } else {
                                        $(this).closest("a.calendar-event").attr("students-no-results", true);
                                    }
                                });
                            } else {
                                $("a.calendar-event").removeAttr("students-no-results");
                            }

                            break;

                        case "sundays":
                            window.calendarWeekdays = $(this).is(":checked") ? [0, 1, 2, 3, 4, 5, 6] : [1, 2, 3, 4, 5, 6];

                            //_initCalendarComplete();
                            //change to use refresher by jack at 2020-11-04
                            scheduleCalendarRefreshHandler();
                            break;

                        case "indentation":
                            window.calendarIndentation = $(this).is(":checked");

                            //_initCalendarComplete();
                            //change to use refresher by jack at 2020-11-04
                            scheduleCalendarRefreshHandler();
                            break;
                    }
                });
            });

            function _set_time() {
                var timeFormat = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "00:00";
                var date = new Date();
                var parts = timeFormat.split(":");
                date.setHours(parts[0]);
                date.setMinutes(parts[1]);
                return date;
            }

            function _addVhiddenTd($trSelector, index) {
                if ($trSelector.find("td").eq(index).hasClass("vhidden")) {
                    _addVhiddenTd($trSelector, ++index);
                } else {
                    $trSelector.find("td").eq(index).addClass("vhidden");
                }
            }

            var scheduleCalendarRefreshHandler = function scheduleCalendarRefreshHandler() {
                var selectedResource = 'room';
                // var selectedResource = $('.schedule-calendar-mode[data-mode="display-resource"]').find("option:selected").val();
                var selectedDate = $('.table-toolbar .vertical-spinbox .date-picker').val();
                window.cursorDate = selectedDate;

                const new_url = "".concat(url, "/view_calendar?resource=").concat(selectedResource, "&date=").concat(selectedDate, "&mode=").concat(window.cursorMode);
                // window.history.pushState({}, null, new_url);
                //_fetch({
                $.ajax({
                    //async: false,
                    //timeout: 30000, //最長等候回應時間
                    type: "GET",
                    //提交類型
                    url: new_url,
                    //提交地址
                    data: {},
                    //提交內容
                    dataType: "json",
                    //返回數據類型
                    success: function success(response) {
                        console.log(response);
                        //請求完成並成功
                        window.events = JSON.parse(response.data.data._events);
                        window.resources = JSON.parse(response.data.data._resources);
                        console.log(window.events);
                        console.log(window.resources);
                        _initCalendar(window.cursorDate);
                        _initCalendarComplete();
                        $("[data-toggle='tooltip']").tooltip();
                    },
                    error: function error(response) { },
                    beforeSend: function beforeSend(XHR) {//请求開始前执行
                    },
                    complete: function complete(XHR, status) {
                        //请求完成后最终执行

                    }
                });
            };

            var resizeHandler = function resizeHandler() {
                const h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
                window.calendarResizeHeight = _pageVh * h / 100 - $('#page-schedule-calendar div.table-toolbar').height() - $('#page-schedule-calendar div.ul-table-toolbar-container').height();

                $('#schedule-calendar').css('height', window.calendarResizeHeight);
            }; // mode: 'day (d)/week (w)/month (m)'

            // added to refresh calendar
            //var vSpinboxBtnValueLeft = vSpinbox.find(".btn-value-left");
            $(".btn-update").click(function () {
                //alert("The paragraph was clicked.");
                scheduleCalendarRefreshHandler();
            });
            //end added to refresh calendar



            function _initCalendar() {
                var _date = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

                var hideEmptyResources = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
                var empty = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
                // data
                var resources = window.resources;
                console.log(resources);
                var events = window.events; // schedule settings

                var timeslotBegin = "09:00";
                var timeslotEnd = "21:00";
                var timeslotInterval = 5;
                var timeslotDisplayInterval = 120; // It is not an option to call window onresize for the first initialization.

                resizeHandler();
                $('.schedule-calendar td:not(.has-events.event-td)').off('dblclick');
                $('#schedule-calendar .calendar-event').find('.calendar-event-content').off('click');
                $('#schedule-calendar .calendar-event').find('.btn-sign.user-sign-in').off('click');
                $('#schedule-calendar .calendar-event').find('.btn-sign.user-sign-out').off('click');

                if (empty === true) {
                    $("#schedule-calendar").empty(); // $("#schedule-calendar").slimScroll({
                    //   width: width,
                    //   height: height,
                    //   axis: "both",
                    //   // size: '10px',
                    //   // position: 'left',
                    //   // color: '#ffcc00',
                    //   alwaysVisible: true,
                    //   // distance: '20px',
                    //   // start: $('#child_image_element'),
                    //   railVisible: false,
                    //   // railColor: '#222',
                    //   railOpacity: 0.3,
                    //   wheelStep: 110,
                    //   touchScrollStep: 10,
                    //   // allowPageScroll: false,
                    //   disableFadeOut: false,
                    // });
                } //---START


                var __events = {};
                var resourceIndex = 0;
                var startingTdIndex = 0;

                var starttime = _set_time(timeslotBegin);

                var endtime = _set_time(timeslotEnd);

                var time = new Date(starttime);
                var duration = 0;
                var tdTotal = 0; // date

                _date = _date === null ? searchParams.get("date") ? new Date(searchParams.get("date")) : new Date() : new Date(_date);
                var dates = [];

                switch (window.cursorMode) {
                    case "d":
                        dates.push(moment(_date));
                        break;

                    case "w":
                        window.calendarWeekdays.forEach(function (weekday) {
                            dates.push(moment(_date).day(weekday));
                        });
                        break;

                    case "m":
                    case "mc":
                    case "ms":
                        window.cursorMode = "w";

                        var whenIFirstMeetYou = moment(window.cursorDate).startOf("month").format("YYYY-MM-DD");
                        var andIFinallyLost = moment(whenIFirstMeetYou).format("YYYY-MM-") + moment().daysInMonth();
                        var youVoiceTouchesMe = whenIFirstMeetYou;

                        _initCalendar(youVoiceTouchesMe, false, true);

                        youVoiceTouchesMe = moment(whenIFirstMeetYou).add(7, "d").format("YYYY-MM-DD");

                        while (youVoiceTouchesMe < andIFinallyLost) {
                            _initCalendar(youVoiceTouchesMe, false, false);

                            youVoiceTouchesMe = moment(youVoiceTouchesMe).add(7, "d").format("YYYY-MM-DD");
                        }

                        window.cursorMode = "m";
                        return;
                        break;
                }

                var zoom = window.cursorMode === 'd' ? '' : 'style="zoom: .7;"';
                var calendarNode = empty ? $("<table class=\"schedule-calendar first\" ".concat(zoom, "></table>")) : $("<table class=\"schedule-calendar\" ".concat(zoom, "></table>"));
                calendarNode.append("<thead></thead>");
                calendarNode.append("<tbody></tbody>");
                calendarNode.find("thead").append('<tr class="theading-date"><th><div>&nbsp;</div></th></tr>').append('<tr class="theading-publicholiday"><th></th></tr>').append('<tr class="theading-resource"><th><div>&nbsp;</div></th></tr>');
                dates.forEach(function (d) {
                    var tdTotalPerResource = 0;
                    var tdResourcePublicHolidays = {};
                    var date = moment(d).format("YYYY-MM-DD");
                    resources.forEach(function (resource) {
                        $.each(events, function (index, event) {
                            switch (true) {
                                // course
                                case event.date === date && event.resource === resource:
                                    var _node = __events[date + "|" + resource];

                                    var hasOverlapped = false;
                                    console.log(_node);
                                    if (_node === undefined) {
                                        __events[date + "|" + resource] = {
                                            ranges: [{
                                                start: event.start,
                                                end: event.end,
                                                overlapped: 0
                                            }],
                                            maxOverlapped: 0
                                        };
                                    } else {
                                        console.log('event:');
                                        console.log(event);
                                        $.each(_node.ranges, function (index, range) {
                                            console.log("find whether there is any overlaps.");
                                            console.log(range);
                                            /**
                                             * stlog = range.start
                                             * etlog = range.end
                                             *
                                             * st = event.start
                                             * et = event.end
                                             *
                                             * stlog < st < etlog || stlog < et < etlog
                                             */
                                            
                                            const stlog = event.start;
                                            const etlog = event.end;
                                            const rangeOverlappedStlogCond = range.start === stlog || range.start < stlog && stlog < range.end;
                                            const rangeOverlappedEtlogCond = range.start < etlog && etlog < range.end;
                                            if (rangeOverlappedStlogCond || rangeOverlappedEtlogCond) {
                                                console.log("Found overlapped course within timeslot.");
                                                range.overlapped++;
                                                range.end = event.end;

                                                if (_node.maxOverlapped < range.overlapped) {
                                                    _node.maxOverlapped = range.overlapped;
                                                }

                                                hasOverlapped = true;
                                                return false;
                                            }
                                        });

                                        if (!hasOverlapped) {
                                            _node.ranges.push({
                                                start: event.start,
                                                end: event.end,
                                                overlapped: 0
                                            });
                                        }
                                    }

                                    break;

                                case event.category === "publicholiday" && moment.unix(event.start).format("YYYY-MM-DD") === date:
                                    tdResourcePublicHolidays[date] = event.title;
                                    break;
                            }
                        });
                        var node = __events[date + "|" + resource];

                        switch (true) {
                            case hideEmptyResources && node === undefined:
                                break;

                            case hideEmptyResources && node !== undefined:
                            case !hideEmptyResources:
                                var maxOverlapped = node === undefined ? 0 : node.maxOverlapped;
                                tdTotal += maxOverlapped + 1;
                                tdTotalPerResource += maxOverlapped + 1;

                                var _colspanHtml = maxOverlapped ? ' colspan="' + (maxOverlapped + 1) + '"' : "";

                                calendarNode.find("thead .theading-resource").append("<th data-date=\"".concat(date, "\" data-resourceindex=\"").concat(resourceIndex++, "\" data-startingtdindex=\"").concat(startingTdIndex, "\" data-resource=\"").concat(resource, "\"").concat(_colspanHtml, ">\n                <span>").concat(resource, "</span>\n               </th>\n              "));
                                startingTdIndex = maxOverlapped ? startingTdIndex + maxOverlapped + 1 : startingTdIndex + 1;
                                break;
                        }
                    });

                    switch (true) {
                        case hideEmptyResources && tdTotalPerResource === 0:
                            tdTotal += 1;
                            calendarNode.find("thead .theading-resource").append('<th data-date="' + date + '" data-resourceindex="' + resourceIndex++ + '"></th>');
                            calendarNode.find("thead .theading-date").append("<th><div>".concat(moment(d).format("YYYY-MM-DD (ddd)"), "</div></th>"));
                            break;

                        case hideEmptyResources && tdTotalPerResource !== 0:
                        case !hideEmptyResources:
                            calendarNode.find("thead .theading-date").append("<th colspan=\"".concat(tdTotalPerResource, "\"><div>").concat(moment(d).format("YYYY-MM-DD (ddd)"), "</div></th>"));
                            break;
                    }

                    var colspanHtml = tdTotalPerResource !== 0 ? " colspan=\"".concat(tdTotalPerResource, "\"") : "";
                    var publicHolidayHtml = tdResourcePublicHolidays[date] !== undefined ? "<div>".concat(tdResourcePublicHolidays[date], "</div>") : "";
                    calendarNode.find("thead .theading-publicholiday").append("<th".concat(colspanHtml, ">").concat(publicHolidayHtml, "</th>"));
                });
                var tdHtml = "";
                var temp = tdTotal;

                while (temp-- > 0) {
                    tdHtml += "<td></td>";
                }

                while (true) {
                    var str = moment(time).format("HH:mm");

                    switch (true) {
                        case str === timeslotBegin:
                            calendarNode.find("tbody").append("<tr timeline data-timeslot=".concat(str, "><th><span>").concat(str, "</span></th>").concat(tdHtml, "</tr>"));
                            break;

                        case str >= timeslotEnd:
                            calendarNode.find("tbody").append("<tr timeline data-timeslot=".concat(str, "><th><span>").concat(str, "</span></th>").concat(tdHtml, "</tr>"));
                            break;

                        case duration % timeslotDisplayInterval === 0:
                            calendarNode.find("tbody").append("<tr timeline data-timeslot=".concat(str, "><th><span>").concat(str, "</span></th>").concat(tdHtml, "</tr>"));
                            break;

                        default:
                            calendarNode.find("tbody").append("<tr data-timeslot=".concat(str, "><th></th>").concat(tdHtml, "</tr>"));
                            break;
                    }

                    if (str >= timeslotEnd) {
                        break;
                    } else {
                        time.setMinutes(time.getMinutes() + timeslotInterval);
                        duration += timeslotInterval;
                    }
                }

                $("#schedule-calendar").append(calendarNode); // Terminated.
            }

            function _initCalendarComplete() {
                // Complete initialize Calendar
                var events = window.events;
                var timeslotInterval = 5;
                var itemWidth = 188;
                $.each(events, function (eventIndex, event) {
                    var selector = 'th[data-date="' + event.date + '"][data-resource="' + event.resource + '"]';

                    if ($(selector).length) {
                        console.log("Events");
                        console.log(event);
                        var resourceIndex = $(selector).data("resourceindex");
                        var tdIndex = $(selector).data("startingtdindex");
                        var totalTdInResourceIndex = $(selector).attr("colspan");
                        totalTdInResourceIndex = totalTdInResourceIndex ? parseInt(totalTdInResourceIndex) : 1;
                        var rowspan = event.duration % timeslotInterval > 0 ? event.duration / timeslotInterval + 1 : event.duration / timeslotInterval;
                        var timeslot_start = moment.unix(event.start).format("HH:mm");
                        var timeslot_end = moment.unix(event.end).format("HH:mm");
                        var $tr_selector = $(selector).closest("table").find('tr[data-timeslot="' + timeslot_start + '"]');
                        var maxTdIndex = tdIndex + totalTdInResourceIndex;
                        var $td = $tr_selector.find("td").eq(tdIndex);
                        console.log('selector');
                        console.log($(selector));
                        var counter = tdIndex;

                        while ($td.find(".calendar-event").length) {
                            if (counter >= maxTdIndex) {
                                console.log('reach maximum');
                                break;
                            } else {
                                counter++;
                                tdIndex++;
                                $td = $td.next();
                            }
                        }

                        console.log(tdIndex);
                        /**
                         * Fix duplicated event adhersion during monthly view
                         */

                        var toBeAdheredEvent = $(event.content);
                        var toBeAdheredEventBookingId = toBeAdheredEvent.find(".course-details").data("id");

                        if ($td.find(".course-details").data("id") === toBeAdheredEventBookingId) { } // console.log(
                        //   "Here comes duplication because of the monthly view OR you have called more than once __initCalendarComplete(). Skipping..."
                        // );
                        // return;
                        // Spare td field elimination


                        var _time;

                        var str;
                        var vHiddenCount;

                        switch (true) {
                            // Absolute overlapped two events st1 = st2,  et1 = et2
                            case $td.find(".calendar-event").length > 0:
                                break;

                            case !$td.hasClass("has-events") && $td.hasClass("vhidden"):
                                console.log(tdIndex);

                                _addVhiddenTd($tr_selector, tdIndex);

                                _time = new Date(event.start * 1000);

                                while (true) {
                                    _time.setMinutes(_time.getMinutes() + timeslotInterval);

                                    str = moment(_time).format("HH:mm");

                                    if (str >= timeslot_end) {
                                        break;
                                    }

                                    _addVhiddenTd($(selector).closest("table").find('tr[data-timeslot="' + str + '"]'), tdIndex);
                                }

                                break;

                            default:
                                _time = new Date(event.start * 1000);

                                while (true) {
                                    _time.setMinutes(_time.getMinutes() + timeslotInterval);

                                    str = moment(_time).format("HH:mm");

                                    if (str >= timeslot_end) {
                                        break;
                                    }

                                    _addVhiddenTd($(selector).closest("table").find('tr[data-timeslot="' + str + '"]'), tdIndex);
                                }

                                break;
                        } // Adhere Event to calendar


                        $td.addClass("has-events event-td").attr("rowspan", rowspan);
                        $('<a class="calendar-event clickable"></a>')
                            .attr("data-category", event.category)
                            .attr("data-start", event.start)
                            .attr("data-end", event.end)
                            .attr("data-eventindex", eventIndex)
                            .attr("data-resourceindex", resourceIndex)
                            .attr("data-tdindex", tdIndex).append(event.content)
                            .appendTo($td);
                    } else { }
                }); // re-render styling only. control table cell width by sum of occurrence of each item

                var tds = {};

                const buttonFormEditPageRedirect = (actionUrl, id) => {
                    var newForm = $('<form>', {
                        'action': actionUrl,
                        'method': 'post',
                    }).append($('<input>', {
                        'name': 'id',
                        'value': id,
                        'type': 'hidden'
                    }));
                    $(document.body).append(newForm);
                    newForm.submit();
                }

                var promise = new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        function hideNext($element) {
                            if ($element.next().hasClass('.has-events.vhidden')) {
                                hideNext($element.next());
                            } else {
                                $element.next().addClass('vhidden');
                            }
                        }

                        $('#schedule-calendar .schedule-calendar tbody tr').each(function (index, element) {
                            const _this = $(element);

                            _this.find(".has-events.vhidden").each(function (index, element) {
                                hideNext($(element));
                            });

                            _this.find(".has-events").each(function (index, element) {
                                console.log(index);
                                var $event = $(element).find('.calendar-event');
                                var isTeacher = $event.find('.calendar-event-content').data('isteacher') == "1";
                                // var width = $(element).outerWidth();
                                // // $event.css('width', width + '');
                                // $(element).css('height', '336.54');
                                if (window.calendarIndentation) {
                                    $(element).addClass('indented');
                                    var width = $(element).width();
                                    var height = $(element).height();
                                    $(element).css('width', width);
                                    $event.attr('data-width', width);
                                    $event.attr('data-height', height);
                                    $event.css('width', '100%');
                                    $event.css('height', 185.54);
                                    $event.css('min-height', 'auto');
                                    $(element).hover(function () {
                                        $event.css('width', width);
                                        $event.css('min-height', 185.54);
                                        $event.css('height', 'auto');
                                    }, function () {
                                        $event.css('width', '100%');
                                        $event.css('height', 185.54);
                                        $event.css('min-height', 'auto');
                                    });
                                } else {
                                    $(element).removeClass('indented');
                                }

                                if ($event.data("category") !== "publicholiday") {
                                    var resourceColors = ['rgb(222, 245, 235)', 'rgb(203, 247, 248)', 'rgb(228, 235, 254)', 'rgb(255, 242, 207)', 'rgb(252, 227, 223)', 'rgb(244, 230, 255)', 'rgb(229, 243, 254)'];
                                    var resourceColorLength = resourceColors.length;
                                    var day = moment($event.data('start') * 1000).day();
                                    var bgcolor = $event.find('.course-details').data('bgcolor');
                                    $event.css("background", bgcolor ? bgcolor : resourceColors[day % resourceColorLength]);
                                    // $event.find(".students a").prepend('<i class="fa fa-phone" style="color: #898788; position: relative; top: 1px;"></i>');
                                    // $event.find("a").removeAttr("href");
                                }

                                if (tds[index] === undefined) {
                                    tds[index] = 0;
                                }

                                ++tds[index];

                                // $event.click(function (e) {
                                //     var _this = $(e.currentTarget);
                                //     var eventId = _this.find('.course-details').data('id');
                                //     var eventTimeslotId = _this.find('.course-details').data('timeslotid');
                                //     window.location.href = "/booking/".concat(eventId, "/").concat(eventTimeslotId, isTeacher ? "/attendance" : "/enroll");
                                // });

                                // if (!isTeacher) {
                                //     $event.find('button.title').click((e) => {
                                //         const _this = $(e.currentTarget);
                                //         e.stopPropagation();

                                //         buttonFormEditPageRedirect('/course/edit', _this.data('courseid'));
                                //     });
                                //     $event.find('button.main-teachers').click((e) => {
                                //         const _this = $(e.currentTarget);
                                //         e.stopPropagation();

                                //         window.location.href = `${url.replace('event', '')}/employee/${_this.data('teacherid')}/edit?tenant=${_this.data('tenant')}`;
                                //     });
                                //     $event.find('button.room').click((e) => {
                                //         const _this = $(e.currentTarget);
                                //         e.stopPropagation();

                                //         buttonFormEditPageRedirect('/room/edit', _this.data('roomid'));
                                //     });
                                //     $event.find('button.student_chi_name').click((e) => {
                                //         const _this = $(e.currentTarget);
                                //         e.stopPropagation();

                                //         buttonFormEditPageRedirect('/student/edit', _this.data('studentid'));
                                //     });
                                //     $event.find('button.event-redirect').click((e) => {
                                //         const _this = $(e.currentTarget);
                                //         e.stopPropagation();

                                //         window.location.href = `${url.replace('/event', '')}${_this.data('url')}`;
                                //     });
                                // }
                            });
                        });
                        resolve();
                    });
                }); // CSS styling

                promise.then(function () {
                    $('.schedule-calendar').each(function (index, el) {
                        var scheduleCalendar = $(el);
                        var theadingDateHeight = scheduleCalendar.find('.theading-date').height();
                        var theadingPublicHolidayHeight = scheduleCalendar.find('.theading-publicholiday').height();
                        scheduleCalendar.find('.theading-publicholiday th').css('top', "".concat(theadingDateHeight, "px"));
                        scheduleCalendar.find('.theading-resource th').css('top', "".concat(theadingDateHeight + theadingPublicHolidayHeight - 1, "px"));
                        // $.each(tds, function (index, occurrence) {
                        //     scheduleCalendar.find("thead th").eq(index).css("width", itemWidth * occurrence);
                        // });
                    }); // 

                    $('.schedule-calendar td:not(.has-events.event-td)').dblclick(function (e) {
                        var _this = $(e.currentTarget);

                        var targetTime = _this.closest('tr').data('timeslot');

                        var targetIndex = _this.index() - 1;
                        var targetDate;
                        var targetResourceEl;
                        var targetResource;
                        var targetResourceName;

                        _this.closest('table').find('.theading-resource th').each(function (index, el) {
                            if ($(el).data('startingtdindex') === undefined) {
                                // continue;
                                return;
                            }

                            if (targetIndex >= parseInt($(el).data('startingtdindex'))) {
                                targetResourceEl = $(el);
                            } else {
                                // break the each loop
                                return false;
                            }
                        }).promise().done(function () {
                            targetDate = targetResourceEl.data('date');
                            targetResource = $('select[data-mode="display-resource"]').find('option:selected').val();
                            targetResourceName = targetResourceEl.data('resource');
                            window.location.href = "".concat(url, "/add?date=").concat(targetDate, "&time=").concat(targetTime, "&resource=").concat(targetResource, "&resourceName=").concat(targetResourceName);
                        });
                    });
                }); // Stretch all weekly calendar to the same width (monthly calendar mode)

                if (window.cursorMode === "m") {
                    var largestIndex;
                    var largestWidth = 0;
                    $("#schedule-calendar .schedule-calendar").each(function (index, element) {
                        if ($(element).outerWidth > largestWidth) {
                            largestIndex = index;
                            largestWidth = $(element).outerWidth;
                        }
                    });
                    $("#schedule-calendar .schedule-calendar").css('width', largestWidth);
                }

                // $('.schedule-calendar').each(function (index, element) {
                //     var currentWidth = $(element).innerWidth() - 83;
                //     $(element).find('td').css('width', currentWidth / 4);
                // });
            }

            _initCalendar();

            _initCalendarComplete();

            $("[data-toggle='tooltip']").tooltip();

            $('.btn-filter-expand').click(function (e) {
                var _this = $(e.currentTarget);

                _this.toggleClass('checked');

                $('ul.table-toolbar').find('li').each(function (index, element) {
                    $(element).toggleClass('show');
                });
            });

            window.onresize = function () {
                resizeHandler();
            };

            /***/
        }),

    /***/ "./resource/js/utils/date-picker.js":
    /***/ (function (module, __webpack_exports__, __webpack_require__) {

            "use strict";
    /* unused harmony export datepickerIconClick */
    /* unused harmony export timepickerIconClick */
    /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function () { return datepicker; });
            /* unused harmony export timepicker */
            var datepickerIconClick = function datepickerIconClick($el) {
                $el.each(function (index, value) {
                    $(value).next().click(function (e) {
                        var _this = $(e.currentTarget);

                        _this.closest('div').find('input.date-picker').datepicker('show');
                    });
                });
            };
            var timepickerIconClick = function timepickerIconClick($el) {
                $el.each(function (index, value) {
                    $(value).next().click(function (e) {
                        var _this = $(e.currentTarget);

                        _this.closest('div').find('input.timepicker').timepicker('show');
                    });
                });
            };
            var datepicker = function datepicker($el) {
                $el.datepicker({
                    format: "yyyy-mm-dd",
                    language: "zh-TW",
                    todayHighlight: true,
                    toggleActive: true
                });
                setTimeout(function () {
                    datepickerIconClick($el);
                }, .7);
            };
            var timepicker = function timepicker($el) {
                $el.timepicker({
                    timeFormat: "H:i",
                    showMeridian: false,
                    defaultTime: false,
                    readOnly: false
                });
                $el.each(function (index, element) {
                    $(element).removeAttr('readonly').attr('autocomplete', 'off');
                });
                setTimeout(function () {
                    timepickerIconClick($el);
                }, .7);
            };

            /***/
        }),

    /***/ "./resource/js/utils/strings.js":
    /***/ (function (module, __webpack_exports__, __webpack_require__) {

            "use strict";
    /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function () { return stripTrailingSlash; });
    /* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function () { return stripLeadingSlashes; });
            /* unused harmony export pos */
            /* unused harmony export replace */
            /**
             * A ES6 module with with multiple exports including a default export
             * @link https://github.com/microsoft/TypeScript/issues/2440
             */

            /**
             * Return string without trailing slash
             * @param {String} str 
             * @returns {String}
             */
            var stripTrailingSlash = function stripTrailingSlash(str) {
                return str.endsWith('/') ? str.slice(0, -1) : str;
            };
            /**
             * Return string without leading slashes
             * @param {String} str 
             * @returns {String}
             */

            var stripLeadingSlashes = function stripLeadingSlashes(str) {
                return str.replace(/^\/+/, '');
            };
            /**
             * Return true if the haystack contains the characters defined by neddle, otherwise false
             * @param {String} haystack
             * @param {String} needle
             * @returns {Boolean}
             */

            var pos = function pos(haystack, needle) {
                return haystack.includes(needle);
            };
            var replace = function replace(haystack, needle, _replace) {
                return haystack.replace(needle, _replace);
            };

            /***/
        }),

    /***/ "./resource/js/utils/url.js":
    /***/ (function (module, __webpack_exports__, __webpack_require__) {

            "use strict";
    /* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__strings__ = __webpack_require__("./resource/js/utils/strings.js");

    /* harmony default export */ __webpack_exports__["a"] = (function (urlString) {
                /**
                 * Domain, Host, URL
                 * @link http://www.usefulutilities.com/support/cfg/dhu.html
                 * 
                 * Sample URLs and their components
                 * 
                 * URL 6	http://search.somedb.com:8080/history?era=darkages
                 * scheme	http
                 * hostname	search.somedb.com
                 * port	8080
                 * origin	http://search.somedb.com:8080
                 * path	    /history
                 * pathname	history
                 * query	?era=darkages
                 * fragment	 
                 * 
                 * URL 7	http://search.somedb.com:8080/history#?modern
                 * scheme	http
                 * hostname	search.somedb.com
                 * port	    8080
                 * origin	http://search.somedb.com:8080
                 * pathname	/history
                 * pathname	history
                 * query	 
                 * fragment	#?modern
                 */
                var obj = new URL(urlString !== undefined ? urlString : window.location.href);
                var path = __WEBPACK_IMPORTED_MODULE_0__strings__["b" /* stripTrailingSlash */](obj.pathname.split('#')[0].split('?')[0]);
                var pathname = __WEBPACK_IMPORTED_MODULE_0__strings__["a" /* stripLeadingSlashes */](path);
                return {
                    scheme: obj.protocol,
                    hostname: obj.hostname,
                    port: obj.port,
                    origin: obj.origin,
                    // path without ? and #
                    path: path,
                    pathname: pathname,
                    query: obj.search,
                    fragment: obj.hash,
                    // url without ? and #
                    url: obj.origin + path,
                    // Bonus: searchParams 
                    // @link https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams
                    searchParams: obj.searchParams,

                    /**
                     * Reload the page
                     * @param {Boolean} bool
                     * @returns {Function}
                     */
                    reload: function reload(bool) {
                        return window.location.reload(bool === undefined ? true : bool);
                    }
                };
            });

            /***/
        }),

    /***/ 0:
    /***/ (function (module, exports, __webpack_require__) {

            module.exports = __webpack_require__("./resource/js/pages/schedule-calendar.js");


            /***/
        })

    /******/
});