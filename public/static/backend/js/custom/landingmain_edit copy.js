function rerank() {
    var i = 1;
    $('.rank_no').each(function () {
        $(this).text(i);
        i = i + 1;
    });
}

$(document).ready(function () {

    var table_name = 'page_objects';
    var rank_name = 'displayorder';
    var id_name = 'id';

    rerank();

    $("#content-list").sortable({
        handle: '.handle',
        update: function () {
            rerank();
            var order = $('#content-list').sortable('serialize');
            var postData = {
                'table_name': table_name,
                'rank_name': rank_name,
                'id_name': id_name,
                'order': order,
            }
            postData[csrf_name] = csrf_hash;
            $.ajax({
                type: 'POST',                     //GET or POST
                url: base_url + '/backend/pageobjects/update_rank',               //請求的頁面
                cache: false,                     //防止抓到快取的回應
                data: postData,      //要傳送到頁面的參數
                success: function (data) {
                    $("#info").append('<b style="color:red;" class="this_info" id="this_info">更新成功!!</b>');
                    setTimeout(function () {
                        $('.this_info').fadeOut(500);
                    }, 2000);
                },         //當請求成功後此事件會被呼叫
                // error: functionFailed,            //當請求失敗後此事件會被呼叫
                statusCode: {                     //狀態碼處理
                    404: function () {
                        alert("page not found");
                    }
                }
            });
            // end ajax
        }
    });

    var _preventDefault = function (evt) {
        evt.preventDefault();
    };

    $(".handle").bind("dragstart", _preventDefault).bind("selectstart", _preventDefault);

    $(".remove").click(function () {
        var id = $(this).parents("tr").attr("id");
        if (confirm('Are you sure to remove this record ?')) {
            $.ajax({
                url: base_url + 'backend/pageobjects/delete/' + id,
                type: 'DELETE',
                error: function () {
                    alert('Something is wrong');
                },
                success: function (data) {
                    $("#" + id).remove();
                    alert("Record removed successfully");
                }
            });
        }
    });

    $(".add-title").click(function () {
        data = {
            id: $('INPUT[name="id"]').val(),
            subid: 0,
        }
        data[csrf_name] = csrf_hash;
        $.ajax({
            url: base_url + 'backend/pageobjects/add_title',
            method: 'POST',
            data: data,
            error: function () {
                alert('Something is wrong');
            },
            success: function (data) {
                alert("已加入新的標題");
                window.location.reload();
            }
        });
    });

    $(".add-picture").click(function () {
        data = {
            id: $('INPUT[name="id"]').val(),
            subid: 0,
        }
        data[csrf_name] = csrf_hash;
        $.ajax({
            url: base_url + 'backend/pageobjects/add_picture',
            method: 'POST',
            data: data,
            error: function () {
                alert('Something is wrong');
            },
            success: function (data) {
                alert("已加入新的圖片");
                window.location.reload();
            }
        });
    });

});