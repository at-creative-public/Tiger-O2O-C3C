function rerank_tag() {
    var i = 1;
    $('.rank_no_tag').each(function () {
        $(this).text(i);
        i = i + 1;
    });
}




$(document).ready(function () {

    var rank_name = 'displayorder';
    var id_name = 'id';


    rerank_tag();

    $("#content-list-tag").sortable({

        handle: '.handle',
        update: function () {
            rerank_tag();

            var order = $('#content-list-tag').sortable('serialize');
            var postData = {
                'rank_name': rank_name,
                'id_name': id_name,
                'order': order,
            }
            postData[csrf_name] = csrf_hash;
            $.ajax({
                type: 'POST',                     //GET or POST
                url: base_url + '/backend/contact_us_item_management/update_rank',               //請求的頁面
                cache: false,                     //防止抓到快取的回應
                data: postData,      //要傳送到頁面的參數
                success: function (data) {
                    $("#info-tag").append('<b style="color:red;" class="this_info" id="this_info">更新成功!!</b>');
                    setTimeout(function () {
                        $('.this_info').fadeOut(500);
                    }, 2000);
                },         //當請求成功後此事件會被呼叫
                // error: functionFailed,            //當請求失敗後此事件會被呼叫
                statusCode: {                     //狀態碼處理
                    404: function () {
                        alert("page not found");
                    }
                }
            });
            // end ajax
        }
    });

    $(".add-tag").click(function () {
        data = {

        }
        data[csrf_name] = csrf_hash;
        $.ajax({
            url: base_url + 'backend/contact_us_item_management/add',
            method: 'POST',
            data: data,
            error: function () {
                alert('Something is wrong');
            },
            success: function (data) {
                alert("已加入新的標籤");
                window.location.reload();
            }
        });
    });


    var _preventDefault = function (evt) {
        evt.preventDefault();
    };

    $(".handle").bind("dragstart", _preventDefault).bind("selectstart", _preventDefault);

    $(".remove").click(function () {
        var id = $(this).parents("tr").attr("id");
        var type = $(this).parents("tr").attr("data-selector");

        if (confirm('Are you sure to remove this record ?')) {
            $.ajax({
                url: base_url + 'backend/contact_us_item_management/delete/' + id,
                type: 'DELETE',
                error: function () {
                    alert('Something is wrong');
                },
                success: function (data) {
                    $("#" + id).remove();
                    alert("Record removed successfully");
                }
            });
        }
    });






});