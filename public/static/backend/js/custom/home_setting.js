function rerank_image() {
    var i = 1;
    $('.rank_no_image').each(function () {
        $(this).text(i);
        i = i + 1;
    });
}

function rerank_text() {
    var i = 1;
    $('.rank_no_text').each(function () {
        $(this).text(i);
        i = i + 1;
    });
}

function rerank_link() {
    var i = 1;
    $('.rank_no_link').each(function () {
        $(this).text(i);
        i = i + 1;
    });
}

function rerank_row_description() {
    var i = 1;
    $(".rank-no_row_description").each(function () {
        $(this).text(i);
        i = i + 1;
    });
}


$(document).ready(function () {

    var image = 'app_image';
    var text = 'app_text';
    var link = 'app_link';
     var row_description = 'app_row_description';
    var rank_name = 'displayorder';
    var id_name = 'id';


    rerank_image();
    rerank_text();
    rerank_link();
    rerank_row_description();

    $("#content-list-image").sortable({
        handle: '.handle',
        update: function () {
            rerank_image();
            var order = $('#content-list-image').sortable('serialize');
            var postData = {
                'table_name': image,
                'rank_name': rank_name,
                'id_name': id_name,
                'order': order,
            }

            postData[csrf_name] = csrf_hash;
            $.ajax({
                type: 'POST',                     //GET or POST
                url: base_url + '/backend/home_setting/update_rank',               //請求的頁面
                cache: false,                     //防止抓到快取的回應
                data: postData,      //要傳送到頁面的參數
                success: function (data) {
                    $("#info-image").append('<b style="color:red;" class="this_info" id="this_info">更新成功!!</b>');
                    setTimeout(function () {
                        $('.this_info').fadeOut(500);
                    }, 2000);
                },         //當請求成功後此事件會被呼叫
                // error: functionFailed,            //當請求失敗後此事件會被呼叫
                statusCode: {                     //狀態碼處理
                    404: function () {
                        alert("page not found");
                    }
                }
            });
            // end ajax
        }
    });

    $("#content-list-text").sortable({
        handle: '.handle',
        update: function () {
            rerank_text();
            var order = $('#content-list-text').sortable('serialize');
            var postData = {
                'table_name': text,
                'rank_name': rank_name,
                'id_name': id_name,
                'order': order,
            }
            postData[csrf_name] = csrf_hash;
            $.ajax({
                type: 'POST',                     //GET or POST
                url: base_url + '/backend/home_setting/update_rank',               //請求的頁面
                cache: false,                     //防止抓到快取的回應
                data: postData,      //要傳送到頁面的參數
                success: function (data) {
                    $("#info-text").append('<b style="color:red;" class="this_info" id="this_info">更新成功!!</b>');
                    setTimeout(function () {
                        $('.this_info').fadeOut(500);
                    }, 2000);
                },         //當請求成功後此事件會被呼叫
                // error: functionFailed,            //當請求失敗後此事件會被呼叫
                statusCode: {                     //狀態碼處理
                    404: function () {
                        alert("page not found");
                    }
                }
            });
            // end ajax
        }
    });

    $("#content-list-link").sortable({

        handle: '.handle',
        update: function () {
            rerank_link();

            var order = $('#content-list-link').sortable('serialize');
            var postData = {
                'table_name': link,
                'rank_name': rank_name,
                'id_name': id_name,
                'order': order,
            }
            postData[csrf_name] = csrf_hash;
            $.ajax({
                type: 'POST',                     //GET or POST
                url: base_url + '/backend/home_setting/update_rank',               //請求的頁面
                cache: false,                     //防止抓到快取的回應
                data: postData,      //要傳送到頁面的參數
                success: function (data) {
                    $("#info-hyperlink").append('<b style="color:red;" class="this_info" id="this_info">更新成功!!</b>');
                    setTimeout(function () {
                        $('.this_info').fadeOut(500);
                    }, 2000);
                },         //當請求成功後此事件會被呼叫
                // error: functionFailed,            //當請求失敗後此事件會被呼叫
                statusCode: {                     //狀態碼處理
                    404: function () {
                        alert("page not found");
                    }
                }
            });
            // end ajax
        }
    });

    $("#content-list-video").sortable({

        handle: '.handle',
        update: function () {
            rerank_link();

            var order = $('#content-list-row-description').sortable('serialize');
            var postData = {
                'table_name': row_description,
                'rank_name': rank_name,
                'id_name': id_name,
                'order': order,
            }
            postData[csrf_name] = csrf_hash;
            $.ajax({
                type: 'POST',                     //GET or POST
                url: base_url + '/backend/home_setting/update_rank',               //請求的頁面
                cache: false,                     //防止抓到快取的回應
                data: postData,      //要傳送到頁面的參數
                success: function (data) {
                    $("#info-row").append('<b style="color:red;" class="this_info" id="this_info">更新成功!!</b>');
                    setTimeout(function () {
                        $('.this_info').fadeOut(500);
                    }, 2000);
                },         //當請求成功後此事件會被呼叫
                // error: functionFailed,            //當請求失敗後此事件會被呼叫
                statusCode: {                     //狀態碼處理
                    404: function () {
                        alert("page not found");
                    }
                }
            });
            // end ajax
        }
    });


    var _preventDefault = function (evt) {
        evt.preventDefault();
    };

    $(".handle").bind("dragstart", _preventDefault).bind("selectstart", _preventDefault);

    $(".remove").click(function () {
        var id = $(this).parents("tr").attr("id");
        var type = $(this).parents("tr").attr("data-selector");

        if (confirm('Are you sure to remove this record ?')) {
            $.ajax({
                url: base_url + 'backend/home_setting/delete/' + id + "/" + type,
                type: 'DELETE',
                error: function () {
                    alert('Something is wrong');
                },
                success: function (data) {
                    $("#" + id).remove();
                    alert("Record removed successfully");
                }
            });
        }
    });

    $(".add-title").click(function () {
        data = {

        }
        data[csrf_name] = csrf_hash;
        $.ajax({
            url: base_url + 'backend/home_setting/add_title',
            method: 'POST',
            data: data,
            error: function () {
                alert('Something is wrong');
            },
            success: function (data) {
                alert("已加入新的標題");
                window.location.reload();
            }
        });
    });

    $(".add-picture").click(function () {
        data = {

        }
        data[csrf_name] = csrf_hash;
        $.ajax({
            url: base_url + 'backend/home_setting/add_image',
            method: 'POST',
            data: data,
            error: function () {
                alert('Something is wrong');
            },
            success: function (data) {
                alert("已加入新的圖片");
                window.location.reload();
            }
        });
    });

    $(".add-hyperlink-description").click(function () {
        data = {

        }
        data[csrf_name] = csrf_hash;
        $.ajax({
            url: base_url + 'backend/home_setting/add_link',
            method: 'POST',
            data: data,
            error: function () {
                alert('Something is wrong');
            },
            success: function (data) {
                alert("已加入新的連結");
                window.location.reload();
            }
        });
    });

    $(".add-video").click(function () {
        data = {

        }
        data[csrf_name] = csrf_hash;
        $.ajax({
            url: base_url + 'backend/home_setting/add_video',
            method: 'POST',
            data: data,
            error: function () {
                alert('Something is wrong');
            },
            success: function (data) {
                alert("已加入新的標題");
                window.location.reload();
            }
        });
    });

    $(".add-row-description").click(function () {
        data = {

        };
        data[csrf_name] = csrf_hash;
        $.ajax({
            url: base_url + 'backend/home_setting/add_row_description',
            method: 'POST',
            data: data,
            error: function () {
                alert('Something is wrong');
            },
            success: function (data) {
                alert("已加入新的連結");
                window.location.reload();
            }
        });

    });


});