function rerank() {
    var i = 1;
    $('.rank_no').each(function () {
        $(this).text(i);
        i = i + 1;
    });
}

$(document).ready(function () {

    rerank();

    var table_name = 'page_object_pictures';
    var rank_name = 'displayorder';
    var id_name = 'id';
    var counter = 0;

    $("#content-list").sortable({
        handle: '.handle',
        update: function () {
// alert('update');
            rerank();
            var order = $('#content-list').sortable('serialize');
            var postData = {
                'table_name': table_name,
                'rank_name': rank_name,
                'id_name': id_name,
                'order': order,
            }
            postData[csrf_name] = csrf_hash;
            $.ajax({
                type: 'POST',                     //GET or POST
                url: base_url + '/backend/pageobjects/update_rank_picture',               //請求的頁面
                cache: false,                     //防止抓到快取的回應
                data: postData,      //要傳送到頁面的參數
                success: function (data) {
                    $("#info").append('<b style="color:red;" class="this_info" id="this_info">更新成功!!</b>');
                    setTimeout(function () {
                        $('.this_info').fadeOut(500);
                    }, 2000);
                },         //當請求成功後此事件會被呼叫
// error: functionFailed,            //當請求失敗後此事件會被呼叫
                statusCode: {                     //狀態碼處理
                    404: function () {
                        alert("page not found");
                    }
                }
            });
// end ajax
        }
    });

    var _preventDefault = function (evt) {
        evt.preventDefault();
    };

    $(".handle").bind("dragstart", _preventDefault).bind("selectstart", _preventDefault);

    $(".remove").click(function () {
        var id = $(this).parents("tr").attr("id");
        if (confirm('Are you sure to remove this record ?')) {
            $.ajax({
                url: base_url + 'backend/pageobjects/delete_picture/' + id,
                type: 'DELETE',
                error: function () {
                    alert('Something is wrong');
                },
                success: function (data) {
                    $("#" + id).remove();
                    alert("Record removed successfully");
                }
            });
        }
    });

});
