/*
Copyright 2017 Ziadin Givan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

https://github.com/givanz/Vvvebjs
*/

Vvveb.BlocksGroup['Bootstrap 4 Snippets'] =
[ "bootstrap4/portfolio-one-column", "bootstrap4/portfolio-two-column", "bootstrap4/portfolio-three-column", "bootstrap4/portfolio-four-column"];









Vvveb.Blocks.add("bootstrap4/portfolio-one-column", {
    name: "One Column Portfolio Layout",
	dragHtml: '<img src="' + Vvveb.baseUrl + 'icons/image.svg">',        
    image: Vvveb.baseUrl+"img/1-col-portfolio.jpg",
    html:`

    <div class="container">

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">1 Col Portfolio
                    <small>Showcase Your Work One Column at a Time</small>
                </h1>
            </div>

        </div>

        <div class="row">

            <div class="col-lg-7 col-md-7">
                <a href="#">
                    <img class="img-responsive" src="http://placehold.it/700x300" alt="">
                </a>
            </div>

            <div class="col-lg-5 col-md-5">
                <h3>Project One</h3>
                <h4>Subheading</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>
                <a class="btn btn-primary" href="#">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>

        </div>

        <hr>

        <div class="row">

            <div class="col-lg-7 col-md-7">
                <a href="#">
                    <img class="img-responsive" src="http://placehold.it/700x300" alt="">
                </a>
            </div>

            <div class="col-lg-5 col-md-5">
                <h3>Project Two</h3>
                <h4>Subheading</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>
                <a class="btn btn-primary" href="#">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>

        </div>

        <hr>

        <div class="row">

            <div class="col-lg-7 col-md-7">
                <a href="#">
                    <img class="img-responsive" src="http://placehold.it/700x300" alt="">
                </a>
            </div>

            <div class="col-lg-5 col-md-5">
                <h3>Project Three</h3>
                <h4>Subheading</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>
                <a class="btn btn-primary" href="#">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>

        </div>

        <hr>

        <div class="row">

            <div class="col-lg-7 col-md-7">
                <a href="#">
                    <img class="img-responsive" src="http://placehold.it/700x300" alt="">
                </a>
            </div>

            <div class="col-lg-5 col-md-5">
                <h3>Project Four</h3>
                <h4>Subheading</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>
                <a class="btn btn-primary" href="#">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>

        </div>

        <hr>

        <div class="row">

            <div class="col-lg-7 col-md-7">
                <a href="#">
                    <img class="img-responsive" src="http://placehold.it/700x300" alt="">
                </a>
            </div>

            <div class="col-lg-5 col-md-5">
                <h3>Project Five</h3>
                <h4>Subheading</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim.</p>
                <a class="btn btn-primary" href="#">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>

        </div>

        

    </div>
    
`,
});



Vvveb.Blocks.add("bootstrap4/portfolio-two-column", {
    name: "Two Column Portfolio Layout",
	dragHtml: '<img src="' + Vvveb.baseUrl + 'icons/image.svg">',        
    image: Vvveb.baseUrl+"img/2-col-portfolio.jpg",
    html:`
<div class="container">

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">2 Col Portfolio
                    <small>Showcase Your Work Two Columns at a Time</small>
                </h1>
            </div>

        </div>

        <div class="row">

            <div class="col-lg-6 col-md-6 portfolio-item">
                <a href="#project-one">
                    <img class="img-responsive" src="http://placehold.it/700x400">
                </a>
                <h3><a href="#project-one">Project One</a>
                </h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>

            <div class="col-lg-6 col-md-6 portfolio-item">
                <a href="#project-two">
                    <img class="img-responsive" src="http://placehold.it/700x400">
                </a>
                <h3><a href="#project-two">Project Two</a>
                </h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>

        </div>

        <div class="row">

            <div class="col-lg-6 col-md-6 portfolio-item">
                <a href="#project-three">
                    <img class="img-responsive" src="http://placehold.it/700x400">
                </a>
                <h3><a href="#project-three">Project Three</a>
                </h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>

            <div class="col-lg-6 col-md-6 portfolio-item">
                <a href="#project-four">
                    <img class="img-responsive" src="http://placehold.it/700x400">
                </a>
                <h3><a href="#project-four">Project Four</a>
                </h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>

        </div>

        <div class="row">

            <div class="col-lg-6 col-md-6 portfolio-item">
                <a href="#project-five">
                    <img class="img-responsive" src="http://placehold.it/700x400">
                </a>
                <h3><a href="#project-five">Project Five</a>
                </h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>

            <div class="col-lg-6 col-md-6 portfolio-item">
                <a href="#project-six">
                    <img class="img-responsive" src="http://placehold.it/700x400">
                </a>
                <h3><a href="#project-six">Project Six</a>
                </h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>

        </div>


    </div>
`,
});

Vvveb.Blocks.add("bootstrap4/portfolio-three-column", {
    name: "Three Column Portfolio Layout",
	dragHtml: '<img src="' + Vvveb.baseUrl + 'icons/image.svg">',        
    image: Vvveb.baseUrl+"img/3-col-portfolio.jpg",
    html:`
<div class="container">

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">3 Col Portfolio
                    <small>Showcase Your Work</small>
                </h1>
            </div>

        </div>

        <div class="row">

            <div class="col-md-4 portfolio-item">
                <a href="#project-link">
                    <img class="img-responsive" src="http://placehold.it/700x400">
                </a>
                <h3><a href="#project-link">Project Name</a>
                </h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>

            <div class="col-md-4 portfolio-item">
                <a href="#project-link">
                    <img class="img-responsive" src="http://placehold.it/700x400">
                </a>
                <h3><a href="#project-link">Project Name</a>
                </h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>

            <div class="col-md-4 portfolio-item">
                <a href="#project-link">
                    <img class="img-responsive" src="http://placehold.it/700x400">
                </a>
                <h3><a href="#project-link">Project Name</a>
                </h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>

        </div>

        <div class="row">

            <div class="col-md-4 portfolio-item">
                <a href="#project-link">
                    <img class="img-responsive" src="http://placehold.it/700x400">
                </a>
                <h3><a href="#project-link">Project Name</a>
                </h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>

            <div class="col-md-4 portfolio-item">
                <a href="#project-link">
                    <img class="img-responsive" src="http://placehold.it/700x400">
                </a>
                <h3><a href="#project-link">Project Name</a>
                </h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>

            <div class="col-md-4 portfolio-item">
                <a href="#project-link">
                    <img class="img-responsive" src="http://placehold.it/700x400">
                </a>
                <h3><a href="#project-link">Project Name</a>
                </h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>

        </div>

        <div class="row">

            <div class="col-md-4 portfolio-item">
                <a href="#project-link">
                    <img class="img-responsive" src="http://placehold.it/700x400">
                </a>
                <h3><a href="#project-link">Project Name</a>
                </h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>

            <div class="col-md-4 portfolio-item">
                <a href="#project-link">
                    <img class="img-responsive" src="http://placehold.it/700x400">
                </a>
                <h3><a href="#project-link">Project Name</a>
                </h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>

            <div class="col-md-4 portfolio-item">
                <a href="#project-link">
                    <img class="img-responsive" src="http://placehold.it/700x400">
                </a>
                <h3><a href="#project-link">Project Name</a>
                </h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
            </div>

        </div>

        

    </div>
`,
});


Vvveb.Blocks.add("bootstrap4/portfolio-four-column", {
    name: "Four Column Portfolio Layout",
	dragHtml: '<img src="' + Vvveb.baseUrl + 'icons/image.svg">',        
    image: Vvveb.baseUrl+"img/4-col-portfolio.jpg",
    html:`
<div class="container">

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">4 Col Portfolio
                    <small>Showcase Your Work</small>
                </h1>
            </div>

        </div>

        <div class="row">

            <div class="col-md-3 portfolio-item">
                <a href="portfolio-item.html">
                    <img class="img-responsive" src="http://placehold.it/750x450">
                </a>
            </div>

            <div class="col-md-3 portfolio-item">
                <a href="portfolio-item.html">
                    <img class="img-responsive" src="http://placehold.it/750x450">
                </a>
            </div>

            <div class="col-md-3 portfolio-item">
                <a href="portfolio-item.html">
                    <img class="img-responsive" src="http://placehold.it/750x450">
                </a>
            </div>

            <div class="col-md-3 portfolio-item">
                <a href="portfolio-item.html">
                    <img class="img-responsive" src="http://placehold.it/750x450">
                </a>
            </div>

        </div>

        <div class="row">

            <div class="col-md-3 portfolio-item">
                <a href="portfolio-item.html">
                    <img class="img-responsive" src="http://placehold.it/750x450">
                </a>
            </div>

            <div class="col-md-3 portfolio-item">
                <a href="portfolio-item.html">
                    <img class="img-responsive" src="http://placehold.it/750x450">
                </a>
            </div>

            <div class="col-md-3 portfolio-item">
                <a href="portfolio-item.html">
                    <img class="img-responsive" src="http://placehold.it/750x450">
                </a>
            </div>

            <div class="col-md-3 portfolio-item">
                <a href="portfolio-item.html">
                    <img class="img-responsive" src="http://placehold.it/750x450">
                </a>
            </div>

        </div>

        <div class="row">

            <div class="col-md-3 portfolio-item">
                <a href="portfolio-item.html">
                    <img class="img-responsive" src="http://placehold.it/750x450">
                </a>
            </div>

            <div class="col-md-3 portfolio-item">
                <a href="portfolio-item.html">
                    <img class="img-responsive" src="http://placehold.it/750x450">
                </a>
            </div>

            <div class="col-md-3 portfolio-item">
                <a href="portfolio-item.html">
                    <img class="img-responsive" src="http://placehold.it/750x450">
                </a>
            </div>

            <div class="col-md-3 portfolio-item">
                <a href="portfolio-item.html">
                    <img class="img-responsive" src="http://placehold.it/750x450">
                </a>
            </div>

        </div>

        

    </div>
`,
});
