// import $ from "jQuery";

$(document).ready(function () {
    const tables = $('table[datatables_existing]');
    if (tables.length > 0) {
        tables.each((index, element) => {
            let table = $(element);
            let container = table.parent();

            // Initialize container
            // if (!container.length) {

            // }
            $('<div class="datatables-container"></div>').insertBefore(table);
            container = table.prev();

            // Initialize filter bar
            const filterTpl = `
                <ul class="table-toolbar" style="text-align: left;">
                    <li>
                        <span class="toolbar-element search">
                            <i class="fa fa-search"></i><span>搜尋</span>
                        </span>
                    </li>
                </ul>
            `;
            container.append(filterTpl);
            const tableToolbar = $('.table-toolbar', container);

            // Add additionButtonTpl if data-addUrl is found
            const addUrl = $(element).data('add');
            const backUrl = $(element).data('back');
            if (addUrl) {
                const addTpl = `
                <li>
                    <a class="toolbar-element btn btn-default btn-create" href="${addUrl}">
                        <i class="fa fa-plus-circle" style="margin-right: 8px;"></i>
                        創建
                    </a>
                </li>
                `;
                tableToolbar.prepend(addTpl);
            }
            if (backUrl) {
                const backTpl = `
                <li>
                    <a class="toolbar-element btn btn-default btn-create" href="${backUrl}">
                        <i class="fa fa-chevron-left" style="margin-right: 8px;"></i>
                        返回
                    </a>
                </li>
                `;
                tableToolbar.prepend(backTpl);
            }

            table.detach().appendTo(container);

            var defaults = {
                /**
                 * Client-side processing - DOM sourced data: ~5'000 rows. Speed options:
                 */
                orderClasses: false,
                order: table.hasClass("sortable") ? [] : [[table.attr('order_col') ?? '0', "desc"]],
                /**
                 * Feature control deferred rendering for additional speed of initialisation
                 */
                deferRender: true,
                processing: true,
                // serverSide: true,
                responsive: false,
                // @link https://cdn.datatables.net/plug-ins/1.10.16/i18n/Chinese-traditional.json
                language: {
                    "processing": "處理中...",
                    "loadingRecords": "載入中...",
                    "lengthMenu": "顯示數量 _MENU_ ",
                    "zeroRecords": "沒有符合的結果",
                    "info": "顯示 _START_-_END_（總數：_TOTAL_）",
                    "infoEmpty": "0 項結果",
                    "infoFiltered": "(從 _MAX_ 項結果中過濾)",
                    "infoPostFix": "",
                    "search": "搜尋:",
                    "paginate": {
                        "first": "<<",
                        "previous": "<",
                        "next": ">",
                        "last": ">>"
                    },
                    "aria": {
                        "sortAscending": ": 升冪排列",
                        "sortDescending": ": 降冪排列"
                    }
                },
                // paging configuration
                pagingType: "four_button",
                /**
                 * This call frequency (throttling) can be controlled using the searchDelay parameter (ms) for both client-side and server-side processing.
                 */
                searchDelay: 350,
                initComplete: function (setting, json) {
                    // Layout adjustment using Javascript
                    if ($(".dataTables_wrapper", table).length === 0) {
                        $(".dataTables_filter input", container).detach().appendTo($(".toolbar-element.search", container));
                        $(".dataTables_filter", container).remove();
                        // $('#datatables.datatables').detach().insertAfter(".dataTables_processing");
                        $('<div class="row dataTables_footer"><div class="col-sm-7"></div><div class="col-sm-5" style="float: right; text-align: right;"></div></div>').insertAfter(table);
                        $(".dataTables_length", container).detach().appendTo($(".dataTables_footer .col-sm-7", container));
                        $(".dataTables_paginate.paging_four_button", container).detach().appendTo($(".dataTables_footer .col-sm-7", container));
                        $(".dataTables_info", container).detach().appendTo($(".dataTables_footer .col-sm-5", container));
                        $(".buttons-csv", container).addClass('action-btn clickable export').html('導出 CSV');
                    } else {
                        $(".dataTables_filter input", container).detach().appendTo($(".toolbar-element.search", container));
                        $(".dataTables_filter", container).remove();
                        $(".dataTables_length", container).detach().insertBefore($(".dataTables_paginate", container));
                        $(".dataTables_info", container).parent().detach().css('float', 'right').css('textAlign', 'right').appendTo($('.dataTables_paginate', container).closest('.row').addClass('dataTables_footer'));
                    }
                    $(".datatables-container.initializing", container).removeClass("initializing").addClass("initialized");

                    // Auto adjust add page url
                    // $(".btn-create:not(.custom-url)", container).attr('href', url + '/add');


                    $(".control-delete.sorting_disabled", container).append('<input type="checkbox" class="checkbox-delete-all" />');

                    $(".checkbox-delete-all", container).change(function () {
                        if ($(this).is(':checked')) {
                            $('.control-delete input:checkbox').prop('checked', true);
                        } else {
                            $('.control-delete input:checkbox').prop('checked', false);
                        }
                    });

                    $('.datatables-submit', container).click(function () {
                        var ids = [];
                        $('.checkbox-delete:checked').each(function () {
                            ids.push($(this).data('id'));
                        });
                        var idsCount = ids.length;
                        if (ids.length > 0) {
                            var ans = confirm('系統將刪除' + idsCount + '項記錄，是否繼續?');

                            if (ans) {
                                var html = '<form id="form" action="' + pathname + '" method="POST">';
                                html += '<input type="hidden" name="_method" value="DELETE" />';
                                html += '<input type="hidden" name="ids" value="' + ids + '"/>';
                                html += '<input type="hidden" name="redirect" value="' + path + '"/>';
                                html += '</form>';
                                $(html).appendTo('body');
                                $('#form').submit();
                            }
                        } else {
                            reload();
                        }
                    });

                    $('.control-modal-details a', container).click(function (e) {
                        console.log(123);
                        rfind('.modal-backdrop', 0.1, el => {
                            $('.modal-backdrop').detach().appendTo('.widget');
                            $('body').removeClass('modal-open');
                            $('.modal-footer a').click(() => {
                                $('.modal-body').html('...');
                            });
                        });
                    });
                }
            };

            const datatable = table.DataTable(defaults);
            table.addClass('initialized');

            // switch, add filter
            switch (table.attr('filter')) {
                case 'event':
                case 'enroll':
                case 'comment':
                case 'referral_scheme':
                // case 'student_leave_rules':
                case 'student_leave_quota':
                case 'student_leave':
                case 'slip':
                case 'messaging':
                case 'messaging_approval':
                case 'promotion':
                case 'promotion_sortable':
                case 'promotion_approval':
                    // Searchable columns amongst the table fields
                    const searchableColumns = {
                        'event': {
                            0: '日期',
                            1: '名稱',
                            2: '地點'
                        },
                        'enroll': {
                            0: '名稱',
                            1: '地點',
                            2: '日期',
                            3: '英文名稱',
                            4: '中文名稱',
                            5: '學生編號'
                        },
                        'comment': {
                            0: '課程名稱',
                            1: '學生名稱',
                            3: '評語日期'
                        },
                        'referral_scheme': {
                            2: '顯示名稱'
                        },
                        // 'student_leave_rules': {
                        //     0: '期限'
                        // },
                        'student_leave': {
                            0: '英文名稱',
                            1: '中文名稱',
                            2: '學生編號',
                            5: '請假',
                            7: '請假類別'
                        },
                        'student_leave_quota': {
                            0: '英文名稱',
                            1: '中文名稱',
                            2: '學生編號'
                        },
                        'slip': {
                            3: '發佈日期',
                            4: '到期日',
                            5: '推播狀態',
                            6: '發佈狀態'
                        },
                        'messaging': {
                            2: '發佈日期',
                            3: '發佈狀態',
                            4: '推播狀態'
                        },
                        'messaging_approval': {
                            2: '審批狀態',
                            3: '發佈日期',
                            4: '發佈狀態',
                            5: '推播狀態'
                        },
                        'promotion': {
                            2: '截止日期',
                            3: '發佈日期',
                            4: '發佈狀態',
                            5: '推播狀態',
                            6: '顯示主頁banner',
                        },
                        'promotion_sortable': {
                            3: '截止日期',
                            4: '發佈日期',
                            5: '發佈狀態',
                            6: '推播狀態',
                            7: '顯示主頁banner',
                        },
                        'promotion_approval': {
                            3: '截止日期',
                            4: '審批狀態',
                            5: '發佈日期',
                            6: '發佈狀態',
                            7: '推播狀態',
                            8: '顯示主頁banner',
                        }
                    };
                    const entity = table.attr('filter');
                    datatable.columns().flatten().each(function (colIdx) {
                        if (searchableColumns[entity][colIdx] === undefined) {
                            console.log(colIdx);
                            return;
                        }
                        // Create the select list and search operation
                        const toolbarElement = $(`<span class="toolbar-element search">${searchableColumns[entity][colIdx]}：</span>`).appendTo(tableToolbar);
                        var select = $('<select><option value="">全部</option></select>')
                            .appendTo(toolbarElement)
                            .on('change', function () {
                                datatable
                                    .column(colIdx)
                                    .search($(this).val() ? '^' + $(this).val() + '$' : '', true, false)
                                    .draw();
                            });

                        // Get the search data for the first column and add to the select list
                        datatable
                            .column(colIdx)
                            .cache('search')
                            .sort()
                            .unique()
                            .each(function (d) {
                                select.append($('<option value="' + d + '">' + d + '</option>'));
                            });
                    });
                    break;

                default:
                    console.log('No custom filter found');
            }

            // TODO: Custom search filter 
        });
    }
});

