$(".checkbox_button").on("click", function (event) {
    event.preventDefault();
})

$(".blue_button").on("click", function (event) {
    event.preventDefault();
})

$(".radio_button").on("click", function (event) {
    event.preventDefault();
})

$("#button_simple").on("click", function () {
    $("#button_informative").removeClass("selected");
    $(this).addClass("selected");
    $("#data_requirement").val("simple");
    $("#personal_information_table").hide();
    $("#contact_information_table").hide();
    $("#work_information_table").hide();
    $("#simple_remark").show();
})

$("#button_informative").on("click", function () {
    $("#button_simple").removeClass("selected");
    $(this).addClass("selected");
    $("#data_requirement").val("informative");
    $("#personal_information_table").show();
    $("#contact_information_table").show();
    $("#work_information_table").show();
    $("#simple_remark").hide();

})

$(".checkbox_button").on("click", function () {
    console.log($(this).val());
    if ($("#" + $(this).val().toUpperCase()).prop("checked")) {
        $("#" + $(this).val().toUpperCase()).prop("checked", false);
        $(this).removeClass("selected");
    } else {
        $("#" + $(this).val().toUpperCase()).prop("checked", true);
        $(this).addClass("selected");
    }
})


$("#next_button").on("click", function () {

    $(".nav-link.active").parent().next().children().click();
    if (current_page == "add") {
        if ($(".nav-link.active").attr('id') == "details-tab") {

            $("#next_button").hide();
            $("#export_button").show();
        } else {

            $("#next_button").show();
            $("#export_button").hide();
        }
    } else {
        if ($(".nav-link.active").attr('id') == "export-tab") {

            $("#next_button").hide();
            $("#export_button").show();
        } else {

            $("#next_button").show();
            $("#export_button").hide();
        }
    }
})

$(".nav-link").on("click", function () {
    if ($(this).attr("id") == "help-tab") {
        $("#next_button").hide();
        $("#export_button").hide();
    } else {
        if (current_page == "add") {
            if ($(this).attr('id') == "details-tab") {
                $("#next_button").hide();
                $("#export_button").show();
            } else {
                $("#next_button").show();
                $("#export_button").hide();
            }
        } else {
            if ($(this).attr('id') == "export-tab") {
                $("#next_button").hide();
                $("#export_button").show();
            } else {
                $("#next_button").show();
                $("#export_button").hide();
            }
        }
    }
})

var current_preview = "apple_front";

$(".mobile_preview_button").on("click", function () {
    $(".mobile_preview_button").each(function () {
        $(this).removeClass("preview_selected");
        $("#" + $(this).data("preview")).hide();
    })
    $(this).addClass("preview_selected");
    $("#" + $(this).data("preview")).show();

})

$("#website_url").on("input", function () {
    $("#backside_hyperlink").text($(this).val());
})

$("#about_us_content").on("input", function () {
    $("#backside_about_us").text($(this).val());
    $("#google_backside_about_us").text($(this).val());
})

$("#terms_content").on("input", function () {
    $("#backside_terms_conditions").text($(this).val());
    $("#google_backside_terms_conditions").text($(this).val());
})

$("#event_name").on("input", function () {
    $("#event_name_display").text($(this).val());
    $("#google_card_name").text($(this).val());
})

$("#event_venue").on("input", function () {
    $("#google_location").text($(this).val());
})

$("#logo_upload_n").on("change", function () {
    if ($(this).val() != "") {
        $("#apple_logo_image").attr("src", URL.createObjectURL(event.target.files[0]));
        $("#google_logo_image").attr("src", URL.createObjectURL(event.target.files[0]));

    } else {



    }
})

$("#icon_upload_n").on("change", function () {
    if ($(this).val() != "") {

    } else {

    }
})

$("#banner_upload_n").on("change", function () {
    if ($(this).val() != "") {
        $("#apple_strip_image").attr("src", URL.createObjectURL(event.target.files[0]));
        $("#google_hero_image").attr("src", URL.createObjectURL(event.target.files[0]));
    }
})

$("#content_color").on("input", function () {
    $(".apple_phone_value").css("color", $(this).val());
    $("#content_color_hex").val($(this).val());
})

$("#content_color").on("change", function () {
    $(".apple_phone_value").css("color", $(this).val());
})

$("#content_color_hex").on("input", function () {
    $("#content_color").val($(this).val());
    $("#content_color").trigger("change");
})

$("#label_color").on("input", function () {
    $(".apple_phone_label").css("color", $(this).val());
    $("#secondary_color_hex").val($(this).val());
})

$("#label_color").on("change", function () {
    $(".apple_phone_label").css("color", $(this).val());
})

$("#label_color").on("change", function () {
    $(".apple_phone_label").css("color", $(this).val());
})

$("#background_color").on("input", function () {
    var raw = $(this).val();
    update_color(raw);
    $("#background_color_hex").val($(this).val());
})

$("#background_color").on("change", function () {
    var raw = $(this).val();
    update_color(raw);
})

$("#background_color_hex").on("input", function () {
    $("#background_color").val($(this).val());
    $("#background_color").trigger("change");
})

function update_color(raw) {
    var raw_red = raw.substr(1, 2);
    var red = parseInt(raw_red.toString(16), 16);
    var raw_green = raw.substr(3, 2);
    var green = parseInt(raw_green.toString(16), 16);
    var raw_blue = raw.substr(5, 2);
    var blue = parseInt(raw_blue.toString(16), 16);

    console.log((red * 0.299 + green * 0.587 + blue * 0.114));
    if ((red * 0.299 + green * 0.587 + blue * 0.114) > 150) {
        $(".google_phone_text").css("color", "#000000");
    } else {
        $(".google_phone_text").css("color", "#ffffff");
    }
    $("#apple_card_frame").css("background-color", raw);
    $("#google_card_frame").css("background-color", raw);
}


function apple_data_row_update() {
    let data_1_display = $("#front_data_1_status").prop("checked");
    let data_2_display = $("#front_data_2_status").prop("checked");
    let data_3_display = $("#front_data_3_status").prop("checked");
    let data_4_display = $("#front_data_4_status").prop("checked");
    let data_5_display = $("#front_data_5_status").prop("checked");
    let data_6_display = $("#front_data_6_status").prop("checked");

    if (data_1_display) {
        $("#apple_data_1").show();
    } else {
        $("#apple_data_1").hide();
    }
    if (data_2_display) {
        $("#apple_data_2").show();
    } else {
        $("#apple_data_2").hide();
    }
    if (data_3_display) {
        $('#apple_data_3').show();
    } else {
        $("#apple_data_3").hide();
    }

    if (data_4_display) {
        $("#apple_data_4").show();
    } else {
        $("#apple_data_4").hide();
    }
    if (data_5_display) {
        $("#apple_data_5").show();
    } else {
        $("#apple_data_5").hide();
    }
    if (data_6_display) {
        $("#apple_data_6").show();
    } else {
        $("#apple_data_6").hide();
    }

    $("#apple_data_1_label").text($("#front_data_1_label").val().toUpperCase());
    $("#apple_data_2_label").text($("#front_data_2_label").val().toUpperCase());
    $("#apple_data_3_label").text($("#front_data_3_label").val().toUpperCase());
    $("#apple_data_4_label").text($("#front_data_4_label").val().toUpperCase());
    $("#apple_data_5_label").text($("#front_data_5_label").val().toUpperCase());
    $("#apple_data_6_label").text($("#front_data_6_label").val().toUpperCase());
}

function google_data_row_update() {
    let data_1_display = $("#front_data_1_status").prop("checked");
    let data_2_display = $("#front_data_2_status").prop("checked");
    let data_3_display = $("#front_data_3_status").prop("checked");
    let data_4_display = $("#front_data_4_status").prop("checked");
    let data_5_display = $("#front_data_5_status").prop("checked");
    let data_6_display = $("#front_data_6_status").prop("checked");
    if (data_1_display || data_2_display || data_3_display) {
        $("#google_data_row_1").show();
    } else {
        $("#google_data_row_1").hide();
    }
    if (data_4_display || data_5_display || data_6_display) {
        $("#google_data_row_2").show();
    } else {
        $("#google_data_row_2").hide();
    }
    $("#google_phone_data_1_set").removeClass("visible");
    $("#google_phone_data_2_set").removeClass("visible");
    $("#google_phone_data_3_set").removeClass("visible");
    $("#google_phone_data_4_set").removeClass("visible");
    $("#google_phone_data_5_set").removeClass("visible");
    $("#google_phone_data_6_set").removeClass("visible");
    $("#google_phone_data_1_set").removeClass("invisible");
    $("#google_phone_data_2_set").removeClass("invisible");
    $("#google_phone_data_3_set").removeClass("invisible");
    $("#google_phone_data_4_set").removeClass("invisible");
    $("#google_phone_data_5_set").removeClass("invisible");
    $("#google_phone_data_6_set").removeClass("invisible");

    switch (true) {
        case (data_1_display && !data_2_display && !data_3_display):
            //$("#google_phone_data_1_set").css("text-align", "left");
            $("#google_phone_data_1_set").addClass("visible");
            $("#google_phone_data_2_set").addClass("invisible");
            $("#google_phone_data_3_set").addClass("invisible");
            break;
        case (!data_1_display && data_2_display && !data_3_display):
            //$("#google_phone_data_2_set").css("text-align", "left");
            $("#google_phone_data_1_set").addClass("invisible");
            $("#google_phone_data_2_set").addClass("visible");
            $("#google_phone_data_3_set").addClass("invisible");
            break;
        case (!data_1_display && !data_2_display && data_3_display):
            //$("#google_phone_data_3_set").css("text-align", "left");
            $("#google_phone_data_1_set").addClass("invisible");
            $("#google_phone_data_2_set").addClass("invisible");
            $("#google_phone_data_3_set").addClass("visible");
            break;
        case (data_1_display && data_2_display && !data_3_display):
            //$("#google_phone_data_1_set").css("text-align", "left");
            //$("#google_phone_data_2_set").css("text-align", "center");
            $("#google_phone_data_1_set").addClass("visible");
            $("#google_phone_data_2_set").addClass("visible");
            $("#google_phone_data_3_set").addClass("invisible");
            break;
        case (!data_1_display && data_2_display && data_3_display):
            //$("#google_phone_data_2_set").css("text-align", "left");
            //$("#google_phone_data_3_set").css("text-align", "center");
            $("#google_phone_data_1_set").addClass("invisible");
            $("#google_phone_data_2_set").addClass("visible");
            $("#google_phone_data_3_set").addClass("visible");
            break;
        case (data_1_display && !data_2_display && data_3_display):
            //$("#google_phone_data_1_set").css('text-align', "left");
            //$("#google_phone_data_3_set").css("text-align", "center");
            $("#google_phone_data_1_set").addClass("visible");
            $("#google_phone_data_2_set").addClass("invisible");
            $("#google_phone_data_3_set").addClass("visible");
            break;
        case (data_1_display && data_2_display && data_3_display):
            //$("#google_phone_data_1_set").css("text-align", "left");
            //$("#google_phone_data_2_set").css("text-align", "center");
            //$("#google_phone_data_3_set").css("text-align", "right");
            $("#google_phone_data_1_set").addClass("visible");
            $("#google_phone_data_2_set").addClass("visible");
            $("#google_phone_data_3_set").addClass("visible");
            break;
    }
    switch (true) {
        case (data_4_display && !data_5_display && !data_6_display):
            //$("#google_phone_data_4_set").css("text-align", "left");
            $("#google_phone_data_4_set").addClass("visible");
            $("#google_phone_data_5_set").addClass("invisible");
            $("#google_phone_data_6_set").addClass("invisible");
            break;
        case (!data_4_display && data_5_display && !data_6_display):
            //$("#google_phone_data_5_set").css("text-align", "left");
            $("#google_phone_data_4_set").addClass("invisible");
            $("#google_phone_data_5_set").addClass("visible");
            $("#google_phone_data_6_set").addClass("invisible");
            break;
        case (!data_4_display && !data_5_display && data_6_display):
            //$("#google_phone_data_6_set").css("text-align", "left");
            $("#google_phone_data_4_set").addClass("invisible");
            $("#google_phone_data_5_set").addClass("invisible");
            $("#google_phone_data_6_set").addClass("visible");
            break;
        case (data_4_display && data_5_display && !data_6_display):
            // $("#google_phone_data_4_set").css("text-align", "left");
            //$("#google_phone_data_5_set").css("text-align", "center");
            $("#google_phone_data_4_set").addClass("visible");
            $("#google_phone_data_5_set").addClass("visible");
            $("#google_phone_data_6_set").addClass("invisible");
            break;
        case (!data_4_display && data_5_display && data_6_display):
            // $("#google_phone_data_5_set").css("text-align", "left");
            //$("#google_phone_data_6_set").css("text-align", "center");
            $("#google_phone_data_4_set").addClass("invisible");
            $("#google_phone_data_5_set").addClass("visible");
            $("#google_phone_data_6_set").addClass("visible");
            break;
        case (data_4_display && !data_5_display && data_6_display):
            //$("#google_phone_data_4_set").css("text-align", "left");
            //$("#google_phone_data_6_set").css("text-align", "center");
            $("#google_phone_data_4_set").addClass("visible");
            $("#google_phone_data_5_set").addClass("invisible");
            $("#google_phone_data_6_set").addClass("visible");
            break;
        case (data_4_display && data_5_display && data_6_display):
            //$("#google_phone_data_4_set").css("text-align", "left");
            //$("#google_phone_data_5_set").css("text-align", "center");
            //  $("#google_phone_data_6_set").css("text-align", "right");
            $("#google_phone_data_4_set").addClass("visible");
            $("#google_phone_data_5_set").addClass("visible");
            $("#google_phone_data_6_set").addClass("visible");
            break;
    }
    $("#google_data_1_label").text($("#front_data_1_label").val().toUpperCase());
    $("#google_data_2_label").text($("#front_data_2_label").val().toUpperCase());
    $("#google_data_3_label").text($("#front_data_3_label").val().toUpperCase());
    $("#google_data_4_label").text($("#front_data_4_label").val().toUpperCase());
    $("#google_data_5_label").text($("#front_data_5_label").val().toUpperCase());
    $("#google_data_6_label").text($("#front_data_6_label").val().toUpperCase());
}

$(".data_trigger").on("input", function () {
    apple_data_row_update();
    google_data_row_update();
});

$("#event_code").on("input", function () {
    $("#event_code_display").text($(this).val());
    $("#event_code_display_google").text($(this).val());
    $.ajax({
        url: "/merchant/event_ticket/check_duplicate_code",
        data: {
            "code": $(this).val()
        },
        dataType: "json",
        success: function (data) {
            if (data.pass == "true") {
                event_code_check = "true";
            } else {
                event_code_check = "false";
            }
        }
    });
})


$("#event_ticket_type").on("change", function () {
    if ($(this).val() == 1) {
        $("#event_code").attr("disabled", true);
        $("#event_code").val("");
    } else {
        $("#event_code").attr("disabled", false);
    }
})

$("#header_text").on("input", function () {
    $("#apple_header_text").text($(this).val());
    $("#google_header_text").text($(this).val());
})


$("#export_button").on("click", function () {
    if (current_page == "add") {
        if (form_checking_n()) {
            $("#loading").show();
            $("#creator_form").submit();
        }
    } else {
        if (form_checking()) {
            $("#loading").show();
            $("#creator_form").submit();
        }

    }
})

function form_checking_n() {
    let message = "";
    let first_index = "";

    if ($("#management_name").val() == "") {
        message += "please enter the Project name!\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }

    if ($("#event_name").val() == "") {
        message += "Please enter the Event name!\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }

    if ($("#issuer_name").val() == "") {
        message += "Please enter the Issuer name!\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }

    if ($("#event_ticket_type").val() == 0) {

        if ($("#event_code").val() == "") {
            message += "Please enter the Event code!\n";
            if (first_index == "") {
                first_index = "setting-tab";
            }
        }

        if (event_code_check == "false") {
            message += "Please enter another event code!\n";
            if (first_index == "") {
                first_index = "setting-tab";
            }
        }
    }

    if ($("#event_start_time").val() == "") {
        message += "Please enter the Event start time!\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }

    if ($("#event_end_time").val() == "") {
        message += "Please enter the Event end time!\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }

    if ($("#event_date").val() == "") {
        message += "Please enter the Event date!\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }

    if ($("#event_venue").val() == "") {
        message += "Please enter the Event venue!\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }

    if ($("#location_detail").val() == "") {
        message += "Please enter the Location detail!\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }

    if ($("#icon_upload_n").val() == "") {
        message += "Please upload the icon image!\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }

    if ($("#email_banner_upload_n").val() == "") {
        message += "Please upload the email banner!\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }

    if ($("#logo_upload_n").val() == "") {
        message += "Please upload the logo image!\n";
        if (first_index == "") {
            first_index = "header-tab";
        }
    }

    if ($("#logo_text").val() == "") {
        message += "Please enter the header!\n";
        if (first_index == "") {
            first_index = "header-tab";
        }
    }

    if ($("#banner_upload_n").val() == "") {
        message += "Please upload the banenr image!\n";
        if (first_index == "") {
            first_index = "theme-tab";
        }
    }

    if ($("#website_url").val() == "") {
        message += "Please enter the url!\n";
        if (first_index == "") {
            first_index = "details-tab";
        }
    }

    if ($("#about_us_content").val() == "") {
        message += "Please enter the about us!\n";
        if (first_index == "") {
            first_index = "details-tab";
        }
    }

    if ($("#terms_content").val() == "") {
        message += "Please enter the terms and conditions!\n";
        if (first_index == "") {
            first_index = "details-tab";
        }
    }

    if (message == "") {
        return true;
    } else {
        $('#' + first_index).click();
        alert(message);
        return false;
    }




}
