
$("#copy_button").on("click", function () {
    var link = document.getElementById("generated_url");
    link.select();
    link.setSelectionRange(0, 99999);
    document.execCommand("copy");
    link.setSelectionRange(0, 0);
    $("#copy_alert").show();
    setInterval(function () {
        $("#copy_alert").hide();
    }, 3000
    );
})

$("#copy_ref_key_button").on("click", function () {
    var ref_key = document.getElementById('ref_key');
    ref_key.select();
    ref_key.setSelectionRange(0, 99999);
    document.execCommand("copy");
    ref_key.setSelectionRange(0, 0);
    $("#ref_key_alert").show();
    setInterval(function () {
        $("#ref_key_alert").hide();
    }, 3000);
})


function form_checking() {
    let message = "";
    let first_index = "";

    if ($("#management_name").val() == "") {
        message += "Please enter the Project name!\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }

    if ($("#event_name").val() == "") {
        message += "Please enter the Event name!\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }
    if ($("#issuer_name").val() == "") {
        message += "Please enter the Issuer name!\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }



    if ($("#event_start_time").val() == "") {
        message += "Please enter the Event start time!\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }

    if ($("#event_end_time").val() == "") {
        message += "Please enter the Event end time!\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }

    if ($("#event_date").val() == "") {
        message += "Please enter the Event date!\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }

    if ($("#event_venue").val() == "") {
        message += "Please enter the Event venue!\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }

    if ($("#location_detail").val() == "") {
        message += "Please enter the Location detail!\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }

    if ($("#logo_text").val() == "") {
        message += "Please enter the header!\n";
        if (first_index == "") {
            first_index = "header-tab";
        }
    }

    if ($("#website_url").val() == "") {
        message += "Please enter the url!\n";
        if (first_index == "") {
            first_index = "details-tab";
        }
    }

    if ($("#about_us_content").val() == "") {
        message += "Please enter the about us!\n";
        if (first_index == "") {
            first_index = "details-tab";
        }
    }

    if ($("#terms_content").val() == "") {
        message += "Please enter the terms and conditions!\n";
        if (first_index == "") {
            first_index = "details-tab";
        }
    }

    if (message == "") {
        return true;
    } else {
        $('#' + first_index).click();
        alert(message);
        return false;
    }


}

$("#banner_upload").on("change", function () {
    if ($(this).val() != "") {
        $("#apple_strip_image").attr("src", URL.createObjectURL(event.target.files[0]));
        $("#google_hero_image").attr("src", URL.createObjectURL(event.target.files[0]));
    } else {
        $("#apple_strip_image").attr("src", ori_banner);
        $("#google_hero_image").attr("src", ori_banner);
    }
})

$("#logo_upload").on("change", function () {
    if ($(this).val() != "") {
        $("#apple_logo_image").attr("src", URL.createObjectURL(event.target.files[0]));
        $("#google_logo_image").attr("src", URL.createObjectURL(event.target.files[0]));
    } else {
        $("#apple_logo_image").attr("src", ori_logo);
        $("#google_logo_image").attr("src", ori_logo);
    }
})


