$("#logo_upload").on("change", function () {
    if ($(this).val() != "") {
        $("#apple_logo_image").attr("src", URL.createObjectURL(event.target.files[0]));
        $("#google_logo_image").attr("src", URL.createObjectURL(event.target.files[0]));
        $("#logo_preview_frame").attr("src", URL.createObjectURL(event.target.files[0]));
    } else {
        $("#apple_logo_image").attr("src", ori_logo);
        $("#google_logo_image").attr("src", ori_logo);
        $("#logo_preview_frame").attr("src", ori_logo);
    }
})

$("#icon_upload").on("change", function () {
    if ($(this).val() != "") {
        $("#icon_preview_frame").attr("src", URL.createObjectURL(event.target.files[0]));
    } else {
        $("#icon_preview_frame").attr("src", ori_icon);
    }
})

$("#banner_upload").on("change", function () {
    if ($(this).val() != "") {
        $("#apple_strip_image").attr("src", URL.createObjectURL(event.target.files[0]));
        $("#google_hero_image").attr("src", URL.createObjectURL(event.target.files[0]));
        $("#banner_preview").attr("src", URL.createObjectURL(event.target.files[0]));
    } else {
        $("#apple_strip_image").attr("src", ori_banner);
        $("#google_hero_image").attr("src", ori_banner);
        $("#banner_preview").attr("src", ori_banner);

    }
})

$("#copy_button").on('click', function () {
    var link = document.getElementById("generated_url");
    link.select();
    link.setSelectionRange(0, 99999);
    document.execCommand("copy");
    link.setSelectionRange(0, 0);
    $("#copy_alert").show();
    setInterval(function () {
        $("#copy_alert").hide();
    }, 3000);
})

function form_checking(){
    let message = "";
    let first_index = "";
    let first_focus = "";
    
    if($("#management_name").val() == ""){
        message += "Please enter the Project name !\n";
        if(first_index == ""){
            first_index = "setting-tab";
            first_focus = "management_name";
        }
    }
    
    if($("#project_name").val() == ""){
        message += "Please enter the Coupon name !\n";
        if(first_index == ""){
            first_index = "setting-tab";
            first_focus = "project_name";
        }
    }
    
    if($("#coupon_amount").val() == ""){
        message += "Please enter the Coupon amount !\n";
        if(first_index == ""){
            first_index = "setting-tab";
            first_focus = "coupon_amount";
        }
    }
    
    if($("#expiry_date").val() == ""){
        message += "Please enter the Expiry date !\n";
        if(first_index == ""){
            first_index = "setting-tab";
            first_focus = "expiry_date";
        }
    }
    
    if($("#header_text").val() == ""){
        message += "Please enter the Header text !\n";
        if(first_index == ""){
            first_index = "header-tab";
            first_focus = "header_text";
        }
    }
    
    if($("#website_url").val() == ""){
        message += "Please enter the url !\n";
        if(first_index == ""){
            first_index = "details-tab";
            first_focus = "website_url";
        }
    }
    
    if($("#about_us_content").val() == ""){
        message += "Please enter the about us content !\n";
        if(first_index == ""){
            first_index = "details-tab";
            first_focus = "about_us_content";
        }
    }
    
    if($("#terms_content").val() == ""){
        message += "Please enter the Terms & Conditions content !\n";
        if(first_index == ""){
            first_index = "details-tab";
            first_focus = "terms_content";
        }
    }
    
    if(message == ""){
        return true;
    }else{
        $("#"+first_index).click();
        alert(message);
        return false;
        
    }
}
