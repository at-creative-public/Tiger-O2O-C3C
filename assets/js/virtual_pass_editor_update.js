$("#add_level_row").on("click", function () {
    $("#level_listing").append(
        '<li class="d-flex" id="level_' + adj_count + '"><div class="d-flex align-items-center justify-content-center" style="width:12rem;">Optional :</div><input type="hidden" name="level[exist_record][]" value="false"><input type="hidden" name="level[level_id][]" value=""><input type="text" class="form-control col" name="level[value][]"><a data-exist="false" data-id="' + adj_count + '" class="delete_level d-flex align-items-center justify-content-center" style="width:50px;cursor:pointer;"><i class="fa fa-times" aria-hidden="true"></i></a></li>'
    );
    adj_count++;
    delete_button_init();
})


function delete_button_init() {
    $(".delete_level").unbind();

    $(".delete_level").on("click", function () {
        if ($(this).data("exist") == true) {
            $.ajax({
                url: base_url + "/merchant/virtual_passes/check_user_ref",
                method: "post",
                dataType: "json",
                data: {
                    "id": $(this).data("id"),
                    "pass_id": pass_id,
                    "row_id": $(this).data('row_id')
                },
                success: function (res) {
                    if (res.delete == 0) {
                        alert("有會員處於這個等級，不能刪除");
                    }
                    if (res.delete == 1) {
                        $("#level_" + res.row_id).remove();
                        alert("刪除成功");
                    }
                },
                failed: function (res) {

                }
            });
        }
        else {
            $("#level_" + $(this).data("id")).remove();
        }

    })

}


$("#logo_upload").on("change", function () {
    if ($(this).val() != "") {
        $("#apple_logo_image").attr("src", URL.createObjectURL(event.target.files[0]));
        $("#google_logo_image").attr("src", URL.createObjectURL(event.target.files[0]));
        $("#logo_preview_frame").attr("src", URL.createObjectURL(event.target.files[0]));
    } else {
        $("#apple_logo_image").attr("src", ori_logo);
        $("#google_logo_image").attr("src", ori_logo);
        $("#logo_preview_frame").attr("src", ori_logo);
    }
})

$("#icon_upload").on("change", function () {
    if ($(this).val() != "") {
        $("#icon_preview_frame").attr("src", URL.createObjectURL(event.target.files[0]));
    } else {
        $("#icon_preview_frame").attr("src", ori_icon);
    }
})

$("#banner_upload").on("change", function () {
    if ($(this).val() != "") {
        $("#apple_strip_image").attr("src", URL.createObjectURL(event.target.files[0]));
        $("#google_hero_image").attr("src", URL.createObjectURL(event.target.files[0]));
        $("#banner_preview").attr("src", URL.createObjectURL(event.target.files[0]));
    } else {
        $("#apple_strip_image").attr("src", ori_banner);
        $("#google_hero_image").attr("src", ori_banner);
        $("#banner_preview").attr("src", ori_banner);

    }
})

$("#copy_button").on('click', function () {
    var link = document.getElementById("generated_url");
    link.select();
    link.setSelectionRange(0, 99999);
    document.execCommand("copy");
    $("#copy_alert").show();
    setInterval(function () {
        $("#copy_alert").hide();
    }, 3000);
})

$("#copy_ref_key_button").on("click", function () {
    var link = document.getElementById("ref_key");
    link.select();
    link.setSelectionRange(0, 99999);
    document.execCommand("copy");
    $("#ref_key_alert").show();
    setInterval(function () {
        $("#ref_key_alert").hide();
    }, 3000)
})



function form_checking() {
    let message = "";
    let first_index = "";
    let first_focus = "";

    if ($("#management_name").val() == "") {
        message += "Please enter the Project name !\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }

    if ($('#project_name').val() == "") {
        message += "Please enter the Programme name !\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }

    if ($("#card_name").val() == "") {
        message += "Please enter the card name !\n";
        if (first_index == "") {
            first_index = "setting-tab";
        }
    }
    if ($("#header_text").val() == "") {
        message += "Please enter the header !\n";
        if (first_index == "") {
            first_index = "header-tab";
        }
    }
    if ($("#website_url").val() == "") {
        message += "Please enter the url !\n;";
        if (first_index == "") {
            first_index = "details-tab";
        }
    }
    if ($("#about_us_content").val() == "") {
        message += "Please enter the abous us !\n";
        if (first_index == "") {
            first_index = "details-tab";
        }
    }
    if ($('#terms_conditions').val() == "") {
        message += "Please enter the terms & conditions !\n";
        if (first_index == "") {
            first_index = "details-tab";
        }
    }
    if ($("#default_level").val() == "") {
        message += "Please enter the default level !\n";
        if (first_index == "") {
            first_index = "details-tab";
        }
    }
    if (message == "") {
        return true;
    } else {
        $("#" + first_index).click();
        alert(message);
        return false;
    }


}