$(".checkbox_button").on("click", function (event) {
    event.preventDefault();
})

$(".blue_button").on("click", function (event) {
    event.preventDefault();
})


$(".radio_button").on("click", function (event) {
    event.preventDefault();
    console.log("trigger");
    switch ($(this).val()) {
        case "simple":
            $("#data_requirement_simple").prop("checked", true);
            $("#data_requirement_informative").prop("checked", false);
            if ($("#data_requirement_simple").prop("checked")) {
                $(".button_informative").removeClass("selected");
                $(".button_simple").addClass("selected");
                $("#personal_information_table").hide();
                $("#contact_information_table").hide();
                $("#work_information_table").hide();
                $("#simple_remark").show();
            }
            if ($("#data_requirement_informative").prop("checked")) {
                $(".button_simple").removeClass("selected");
                $(".button_informative").addClass("selected");
                $("#personal_information_table").show();
                $("#contact_information_table").show();
                $("#work_information_table").show();
                $("#simple_remark").hide();
            }
            break;
        case "informative":
            $("#data_requirement_simple").prop("checked", false);
            $("#data_requirement_informative").prop("checked", true);
            if ($("#data_requirement_simple").prop("checked")) {
                $(".button_informative").removeClass("selected");
                $(".button_simple").addClass("selected");
                $("#personal_information_table").hide();
                $("#contact_information_table").hide();
                $("#work_information_table").hide();
                $("#simple_remark").show();
            }
            if ($("#data_requirement_informative").prop("checked")) {
                $(".button_simple").removeClass("selected");
                $(".button_informative").addClass("selected");
                $("#personal_information_table").show();
                $("#contact_information_table").show();
                $("#work_information_table").show();
                $("#simple_remark").hide();
            }
            break;
        case "LEVEL":
            $("#LEVEL").prop("checked", true);
            $(".RADIO_LEVEL").removeClass("selected");
            $(".RADIO_NICK_NAME").removeClass("selected");
            $(".RADIO_POINTS").removeClass("selected");
            $(".RADIO_STAMPS").removeClass("selected");
            $(".RADIO_LEVEL").addClass("selected");
            break;
        case "NICK_NAME":
            $("#NICK_NAME").prop("checked", true);
            $(".RADIO_LEVEL").removeClass("selected");
            $(".RADIO_NICK_NAME").removeClass("selected");
            $(".RADIO_POINTS").removeClass("selected");
            $(".RADIO_STAMPS").removeClass("selected");
            $(".RADIO_NICK_NAME").addClass("selected");
            break;
        case "POINTS":
            $("#POINTS").prop("checked", true);
            $(".RADIO_LEVEL").removeClass("selected");
            $(".RADIO_NICK_NAME").removeClass("selected");
            $(".RADIO_POINTS").removeClass("selected");
            $(".RADIO_STAMPS").removeClass("selected");
            $(".RADIO_POINTS").addClass("selected");
            break;
        case "STAMPS":
            $("#STAMPS").prop("checked", true);
            $(".RADIO_LEVEL").removeClass("selected");
            $(".RADIO_NICK_NAME").removeClass("selected");
            $(".RADIO_POINTS").removeClass("selected");
            $(".RADIO_STAMPS").removeClass("selected");
            $(".RADIO_STAMPS").addClass("selected");
            break;
        case "ORDINARY":
            $("#ORDINARY").prop("checked", true);
            $(".RADIO_ORDINARY").removeClass("selected");
            $(".RADIO_MODERN").removeClass("selected");
            $(".RADIO_PLAYFUL").removeClass("selected");
            $(".RADIO_FREATURED").removeClass("selected");
            $(".RADIO_ORDINARY").addClass("selected");
            break;
        case "MODERN":
            $("#MODERN").prop("checked", true);
            $(".RADIO_ORDINARY").removeClass("selected");
            $(".RADIO_MODERN").removeClass("selected");
            $(".RADIO_PLAYFUL").removeClass("selected");
            $(".RADIO_FREATURED").removeClass("selected");
            $(".RADIO_MODERN").addClass("selected");
            break;
        case "PLAYFUL":
            $("#PLAYFUL").prop("checked", true);
            $(".RADIO_ORDINARY").removeClass("selected");
            $(".RADIO_MODERN").removeClass("selected");
            $(".RADIO_PLAYFUL").removeClass("selected");
            $(".RADIO_FREATURED").removeClass("selected");
            $(".RADIO_PLAYFUL").addClass("selected");
            break;
        case "FREATURED":
            $("#FREATURED").prop("checked", true);
            $(".RADIO_ORDINARY").removeClass("selected");
            $(".RADIO_MODERN").removeClass("selected");
            $(".RADIO_PLAYFUL").removeClass("selected");
            $(".RADIO_FREATURED").removeClass("selected");
            $(".RADIO_FREATURED").addClass("selected");
            break;
    }

})




$(".checkbox_button").on("click", function () {
    console.log($(this).val());
    if ($("#" + $(this).val().toUpperCase()).prop("checked")) {
        $("#" + $(this).val().toUpperCase()).prop("checked", false);
        $(this).removeClass("selected");
    } else {
        $("#" + $(this).val().toUpperCase()).prop("checked", true);
        $(this).addClass("selected");
    }
})

$(".color_ball").on("click", function () {
    $(".color_ball").removeClass("selected");
    if ($(".color_ball").data("value") != "add") {
        $(".color_ball").empty();
    }
    $(this).addClass("selected");
    $(this).append('<i class="fas fa-check" style="color:white;padding-top:3px;"></i>');

})


$("#color_picker").on('click', function () {
    $("#color_picker_body").click();
})
$("#color_picker_body").on("change", function () {
    $("#color_picker").css("background-color", $(this).val());
})

$(".color_picker").on("click", function () {
    console.log("trigger");
    $(this).parent("div").prev("div").children("input").click();
})

$(".color_picker_selector").on("change", function () {
    $(this).parent("div").next("div").children("div").css("background-color", $(this).val())
})

$("#trial-tab").on("change", function () {
    if ($(this).hasClass("active")) {
        $("#next_button").hide();
        $("#submit_button").show();
    } else {
        $("#next_button").show();
        $("#submit_button").hide();
    }
})

$("#next_button").on("click", function () {

    $(".nav-link.active").parent().next().children().click();
    if (current_page == "add") {
        if ($(".nav-link.active").attr('id') == "geotag-tab") {

            $("#next_button").hide();
            $("#export_button").show();
        } else {

            $("#next_button").show();
            $("#export_button").hide();
        }
    } else {
        if ($(".nav-link.active").attr('id') == "export-tab") {

            $("#next_button").hide();
            $("#export_button").show();
        } else {

            $("#next_button").show();
            $("#export_button").hide();
        }
    }
})

$(".nav-link").on("click", function () {
   if ($(this).attr("id") == "help-tab") {
        $("#next_button").hide();
        $("#export_button").hide();
    } else {
        if (current_page == "add") {
            if ($(this).attr('id') == "geotag-tab") {
                $("#next_button").hide();
                $("#export_button").show();
            } else {
                $("#next_button").show();
                $("#export_button").hide();
            }
        } else {
            if ($(this).attr('id') == "export-tab") {
                $("#next_button").hide();
                $("#export_button").show();
            } else {
                $("#next_button").show();
                $("#export_button").hide();
            }
        }
    }
})

$(".invert_color_button").on("click", function (event) {
    event.preventDefault();

    if ($(this).prev().css("background-color") != "rgb(0, 0, 0)") {
        $(this).prev().css("background-color", "#000000");
    } else {
        $(this).prev().css("background-color", "transparent");
    }

})

var current_preview = "apple_front";

$(".mobile_preview_button").on("click", function () {
    $(".mobile_preview_button").each(function () {
        $(this).removeClass("preview_selected");
        $("#" + $(this).data("preview")).hide();
    })
    $(this).addClass("preview_selected");
    $("#" + $(this).data("preview")).show();

})

$("#background_color").on("input", function () {
    var raw = $(this).val();
    update_color(raw);
    $("#background_color_hex").val($(this).val());
})

$("#background_color").on("change", function () {
    var raw = $(this).val();
    update_color(raw);
})


function update_color(raw) {
    var raw_red = raw.substr(1, 2);
    var red = parseInt(raw_red.toString(16), 16);
    var raw_green = raw.substr(3, 2);
    var green = parseInt(raw_green.toString(16), 16);
    var raw_blue = raw.substr(5, 2);
    var blue = parseInt(raw_blue.toString(16), 16);

    console.log((red * 0.299 + green * 0.587 + blue * 0.114));
    if ((red * 0.299 + green * 0.587 + blue * 0.114) > 150) {
        $(".google_phone_text").css("color", "#000000");
    } else {
        $(".google_phone_text").css("color", "#ffffff");
    }
    $("#apple_card_frame").css("background-color", raw);
    $("#google_card_frame").css("background-color", raw);
}

$("#value_color").on("input", function () {
    $(".apple_phone_value").css("color", $(this).val());
    $("#pri_color_hex").val($(this).val());
})

$("#value_color").on("change", function () {
    $(".apple_phone_value").css("color", $(this).val());
})

$("#label_color").on("input", function () {
    $(".apple_phone_label").css("color", $(this).val());
    $("#secondary_color_hex").val($(this).val());
})

$("#label_color").on("change", function () {
    $(".apple_phone_label").css("color", $(this).val());
})

$("#secondary_color_hex").on("input", function () {
    $("#label_color").val($(this).val());
    $("#label_color").trigger("change");
})

$("#pri_color_hex").on("input", function () {
    $("#value_color").val($(this).val());
    $("#value_color").trigger("change");
})

$("#background_color_hex").on("input", function () {
    $("#background_color").val($(this).val());
    $("#background_color").trigger("change");
})

$("#coupon_code").on("input", function () {
    $("#coupon_code_display").text($(this).val());
    $("#coupon_code_display_google").text($(this).val());
    $.ajax({
        "url": "/merchant/coupon/check_duplicate_code",
        "data": {
            "code": $(this).val()
        },
        "dataType": "json",
        "success": function (data) {
            if (data.pass == "true") {
                coupon_check = "true";
            } else {
                coupon_check = "false";
            }
        }
    })
})



function google_data_row_update() {
    let data_1_display = $("#front_data_1_status").prop("checked");
    let data_2_display = $("#front_data_2_status").prop("checked");
    let data_3_display = $("#front_data_3_status").prop("checked");
    let data_4_display = $("#front_data_4_status").prop("checked");
    if (data_1_display || data_2_display) {
        $("#google_data_row_1").show();
    } else {
        $("#google_data_row_1").hide();
    }
    if (data_3_display || data_4_display) {
        $("#google_data_row_2").show();
    } else {
        $("#google_data_row_2").hide();
    }
    switch (true) {
        case (data_1_display && !data_2_display):
            $("#google_phone_data_1_set").css("text-align", "left");
            $("#google_phone_data_1_set").show();
            $("#google_phone_data_2_set").hide();
            break;
        case (!data_1_display && data_2_display):
            $("#google_phone_data_1_set").hide();
            $("#google_phone_data_2_set").show();
            $("#google_phone_data_2_set").css("text-align", "left");
            break;
        case (data_1_display && data_2_display):
            $("#google_phone_data_1_set").show();
            $("#google_phone_data_2_set").show();
            $("#google_phone_data_1_set").css("text-align", "left");
            $("#google_phone_data_2_set").css("text-align", "right");
            break;
    }
    switch (true) {
        case (data_3_display && !data_4_display):
            $('#google_phone_data_3_set').show();
            $('#google_phone_data_4_set').hide();
            $("#google_phone_data_3_set").css("text-align", "left");
            break;
        case (!data_3_display && data_4_display):
            $("#google_phone_data_3_set").hide();
            $("#google_phone_data_4_set").show();
            $("#google_phone_data_4_set").css("text-align", "left");
            break;
        case (data_3_display && data_4_display):
            $("#google_phone_data_3_set").show();
            $("#google_phone_data_4_set").show();
            $("#google_phone_data_3_set").css("text-align", "left");
            $("#google_phone_data_4_set").css("text-align", "right");
            break;
    }
    $("#google_data_1_label").text($("#front_data_1_label").val().toUpperCase());
    $("#google_data_2_label").text($("#front_data_2_label").val().toUpperCase());
    $("#google_data_3_label").text($("#front_data_3_label").val().toUpperCase());
    $("#google_data_4_label").text($("#front_data_4_label").val().toUpperCase());


}

function apple_data_row_update() {
    let data_1_display = $("#front_data_1_status").prop("checked");
    let data_2_display = $("#front_data_2_status").prop("checked");
    let data_3_display = $("#front_data_3_status").prop("checked");
    let data_4_display = $("#front_data_4_status").prop("checked");
    if (data_1_display) {
        $("#apple_data_1").show();
    } else {
        $("#apple_data_1").hide();
    }
    if (data_2_display) {
        $("#apple_data_2").show();
    } else {
        $("#apple_data_2").hide();
    }
    if (data_3_display) {
        $('#apple_data_3').show();
    } else {
        $("#apple_data_3").hide();
    }
    if (data_4_display) {
        $("#apple_data_4").show();
    } else {
        $("#apple_data_4").hide();
    }
    $("#apple_data_1_label").text($("#front_data_1_label").val().toUpperCase());
    $("#apple_data_2_label").text($("#front_data_2_label").val().toUpperCase());
    $("#apple_data_3_label").text($("#front_data_3_label").val().toUpperCase());
    $("#apple_data_4_label").text($("#front_data_4_label").val().toUpperCase());
}

$(".data_trigger").on('input', function () {
    apple_data_row_update();
    google_data_row_update();
})

$("#website_url").on("input", function () {
    $("#backside_hyperlink").text($(this).val());
})
$("#about_us_content").on("input", function () {
    $("#backside_about_us").text($(this).val());
    $("#google_backside_about_us").text($(this).val());
})

$("#terms_content").on("input", function () {
    $("#backside_terms_conditions").text($(this).val());
    $("#google_backside_terms_conditions").text($(this).val());
})

$("#project_name").on("input", function () {
    $("#google_card_name").text($(this).val());
})



$("#logo_upload_n").on("change", function () {
    if ($(this).val() != "") {
        $("#apple_logo_image").attr("src", URL.createObjectURL(event.target.files[0]));
        $("#google_logo_image").attr("src", URL.createObjectURL(event.target.files[0]));

    } else {



    }
})

$("#icon_upload_n").on("change", function () {
    if ($(this).val() != "") {

    } else {

    }
})

$("#banner_upload_n").on("change", function () {
    if ($(this).val() != "") {
        $("#apple_strip_image").attr("src", URL.createObjectURL(event.target.files[0]));
        $("#google_hero_image").attr("src", URL.createObjectURL(event.target.files[0]));
    }
})

$('#header_text').on("input", function () {
    $("#google_header_text").text($(this).val());
    $('#apple_header_text').text($(this).val());
})

$("#card_name").on("input", function () {
    $("#google_card_name").text($(this).val());
})

$("#export_button").on("click", function () {
    console.log("Trigger");
    if (current_page == "add") {
        if (form_checking_n()) {
            $("#loading").show();
            $("#creator_form").submit();
        }
    } else {
        if (form_checking()) {
            $("#loading").show();
            $("#creator_form").submit();
        }
    }
})



function form_checking_n() {
    let message = "";
    let first_index = "";
    let first_focus = "";

    if($("#managment_name").val() == ""){
        message += "Please enter the Project name !\n";
        if(first_index == ""){
            first_index = "setting-tab";
            first_focus = "management_name";
        }
    }

    if ($("#project_name").val() == "") {
        message += "Please enter the Coupon name !\n";
        if (first_index == "") {
            first_index = "setting-tab";
            first_focus = "project_name";
        }
    }

    if ($("#coupon_code").val() == "") {
        message += "Please enter the Coupon code !\n";
        if (first_index == "") {
            first_infex = "setting-tab";
            first_focus = "coupon_code";
        }
    }

    if (coupon_check == "false") {
        message += "Please enter another coupon code !\n";
        if (first_index == "") {
            first_index = "setting-tab";
            first_focus = "coupon_code";
        }
    }

    if ($("#coupon_amount").val() == "") {
        message += "Please enter the Coupon amount !\n";
        if (first_index == "") {
            first_index = "setting-tab";
            first_focus = "coupon_amount";
        }
    }

    if ($("#expiry_date").val() == "") {
        message += "Please enter the Expriy date !\n";
        if (first_index == "") {
            first_idnex = "setting-tab";
            first_focus = "expiry_date";
        }
    }

    if ($("#icon_upload_n").val() == "") {
        message += "Please upload a icon image !\n";
        if (first_index == "") {
            first_index = "setting-tab";
            first_focus = "icon_upload_n";
        }
    }

    if ($("#logo_upload_n").val() == "") {
        message += "Please upload a logo !\n";
        if (first_index == "") {
            first_index = "header-tab";
            first_focus = "logo_upload_n";
        }
    }

    if ($("#header_text").val() == "") {
        message += "Please upload a header text!\n";
        if (first_index == "") {
            first_index = "header-tab";
            first_focus = "header_text";
        }
    }

    if ($("#banner_upload_n").val() == "") {
        message += "Please upload a banner !\n";
        if (first_index == "") {
            first_index = "theme-tab";
            first_focus = "banner_upload_n";
        }
    }



    if ($("#website_url").val() == "") {
        message += "Please enter the url !\n";
        if (first_index == "") {
            first_index = "details-tab";
            first_focus = "website_url";
        }
    }

    if ($("#about_us_content").val() == "") {
        message += "Please enter the about us content !\n";
        if (first_index == "") {
            first_index = "details-tab";
            first_focus = "about_us_content";
        }
    }

    if ($("#terms_content").val() == "") {
        message += "Please enter the terms & Conditions content!\n";
        if (first_index == "") {
            first_index = "details-tab";
            first_focus = "terms_content";
        }
    }



    if (message == "") {
        return true;
    } else {


        $("#" + first_index).click();
        alert(message);
        return false;
    }









}

