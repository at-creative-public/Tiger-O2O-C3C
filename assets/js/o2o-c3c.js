/**
 * child class .content-list is required
 * dataset is composed of object with properties: name, formatted_size, size
 * 
 * @param {Array} dataset 
 * @param {String} foldername 
 * @param {String} statsDomId 
 */
function insertuploadfilerow(domId, dataset, foldername) {
    let totalfilesize = 0;
    let html = '';
    dataset.forEach(function (value, index) {
        const file = value;
        const file_url = `/assets/uploads/${foldername}/${file.name}`;

        const icon = /\.(jpe?g|png|gif)$/i.test(file.name)
            ? `
                <div class="col-auto p-0 d-flex align-items-center" style="width: 80px; max-height: 40px;">
                    <img class="m-auto" src="${file_url}" style="height: 100%;margin: auto;text-align: center;display: inherit;"> 
                </div>
              `
            : '<div class="col-auto p-0 d-flex align-items-center" style="width: 80px; max-height: 40px;"><i class="fas fa-file mr-3 mx-auto col-auto"></i></div>';

        html += `
                <div class="row p-0 mb-2">
                    <div class="handle col-auto clickable"><i class="fas fa-arrows-alt ml-4"></i></div>
                    <a class="col" href="${file_url}" target="_blank">
                        <div class="row pl-2 pl-md-5 file-stats d-flex clickable" data-filename="${file.name}" data-folder="${foldername}">
                            ${icon}
                            <div class="col filename d-inline-block my-auto pl-4 pr-0">${file.name}<span class="filesize"> - ${file.formatted_size}</span></div>
                        </div>
                    </a>
                    <div class="remove col-auto clickable"><i class="fas fa-trash mr-4"></i></div>
                </div>
            `;
        totalfilesize += file.size;
    });
    // debugger;
    console.log($(domId).length);
    console.log($(domId).find('.content-list').length);
    $(domId).attr('data-totalfilesize', totalfilesize);
    $(domId).find('.content-list').empty().append(html);
}

function postfileupload(e, mode = "single", domId, apiUrl, formAttributeName, onLoading = null, onSuccess = null) {
    debugger;
    // const input = e.currentTarget;
    const input = e.target;
    // FileList object
    // const file = mode === "single" ? input.files.item(0) : input.files;

    let formData = new FormData();
    if (mode !== "single") formAttributeName = formAttributeName + "[]";
    for (var i = 0; i < input.files.length; i++) {
        formData.append(formAttributeName, input.files.item(i));
    }

    if (onLoading !== null) onLoading();
    // DO NOT supply headers with 'Content-Type' if it's using FormData
    fetch(apiUrl, {
        method: 'POST',
        body: formData
    }).then(res => res.json())
        .then(response => {
            if (onSuccess !== null) onSuccess(response);
        });
}

/**
 * Upload file, child class .content-list is required
 * 
 * @param {String} domId 
 * @param {String} foldername 
 * @param {Number} maxCount 
 * @param {Number} maxsize 
 * @param {Function} onUpload 
 */
function uploadfile(domId, foldername, maxCount = 0, maxsize = 0, onUpload = null) {
    $(domId).off("change").on("change", function (e) {
        debugger;
        const input = e.target;
        const fileData = $(this).data();

        let filesize = parseInt(fileData.filesize) > 0 ? parseInt(fileData.filesize) : 0;
        let fileCount = parseInt(fileData.filecount) > 0 ? parseInt(fileData.filecount) : 0;
        let html = '';
        let appendHtml = true;

        fileCount += input.files.length;

        function formatFilesize(size) {
            if (size > 1 * 1024 * 1024) return Math.round(size / 1024 / 1024 * 10) / 10 + ' MB';
            else return Math.floor(size / 1024) + ' KB';
        }


        // validate filesize
        // var reader = new FileReader();
        for (var i = 0; i < fileCount; i++) {
            const file = input.files.item(i);

            filesize += file.size;
            console.log(filesize);

            html += `
                <div class="row p-2 mb-4 newrow">
                    <div class="handle col-auto clickable"><i class="fas fa-arrows-alt ml-4"></i></div>
                    <div class="file-stats col clickable">
                        <a class="col">
                            <div class="row pl-2 pl-md-5 file-stats d-flex clickable" data-filename="${file.name}" data-folder="${foldername}">
                                <div class="col-auto p-0 d-flex align-items-center" style="width: 80px; max-height: 40px;"><i class="fas fa-file mr-3 mx-auto col-auto"></i></div>    
                                <div class="col filename d-inline-block my-auto">${file.name}<span class="filesize"> - ${formatFilesize(file.size)}</span></div>
                            </div>
                        </a>
                    </div>
                    <div class="remove col-auto clickable"><i class="fas fa-trash mr-4"></i></div>
                </div>
            `;

            // const is_image = /\.(jpe?g|png|gif)$/i.test(file.name);
            // let file_url;
            // let icon;
            // if (is_image) {
            //     reader.onload = function (evt) {
            //         file_url = evt.target.result;
            //         icon = `
            //             <div class="col-auto p-0 d-flex align-items-center" style="width: 80px; max-height: 40px;">
            //                 <img class="m-auto" src="${file_url}" style="height: 100%;margin: auto;text-align: center;display: inherit;"> 
            //             </div>
            //         `;
            //         const _html = `
            //             <div class="row p-2 mb-4">
            //                 <div class="handle col-auto clickable"><i class="fas fa-arrows-alt ml-4"></i></div>
            //                 <a class="col" href="${file_url}" target="_blank">
            //                     <div class="row pl-2 pl-md-5 file-stats d-flex clickable" data-filename="${file.name}"">
            //                         ${icon}
            //                         <div class="col filename d-inline-block my-auto">${file.name}<span class="filesize"> - ${formatFilesize(file.size)}</span></div>
            //                     </div>
            //                 </a>
            //                 <div class="remove col-auto clickable"><i class="fas fa-trash mr-4"></i></div>
            //             </div>
            //         `;

            //         $(domId).find('.content-list').append(_html);
            //     };

            //     reader.readAsDataURL(file);
            //     setTimeout(function () { }, 300);
            // } else {
            //     file_url = '#';
            //     icon = '<i class="fas fa-file mr-3 my-auto col-auto"></i>';

            //     html += `
            //         <div class="row p-2 mb-4">
            //             <div class="handle col-auto clickable"><i class="fas fa-arrows-alt ml-4"></i></div>
            //             <a class="col" href="${file_url}" target="_blank">
            //                 <div class="row pl-2 pl-md-5 file-stats d-flex clickable" data-filename="${file.name}">
            //                     ${icon}
            //                     <div class="col filename d-inline-block my-auto">${file.name}<span class="filesize"> - ${formatFilesize(file.size)}</span></div>
            //                 </div>
            //             </a>
            //             <div class="remove col-auto clickable"><i class="fas fa-trash mr-4"></i></div>
            //         </div>
            //     `;
            // }
        }

        if (maxCount > 0 && fileCount > maxCount) {
            alert(`You cannot upload more than ${maxCount} files`);
        }
        if (maxsize > 0 && filesize > maxsize * 1024 * 1024) {
            alert(`You cannot upload more than ${maxsize} MB`);
        }
        $(domId).find('.content-list').append(html);

        if (onUpload !== null) onUpload(e);
    });
}

function deleterow(domId, alert = true, fnDelete) {
    $(`${domId} .remove`).off('click').click(function (e) {
        // var id = $(this).parents("tr").attr("id");
        if (alert === false || alert && confirm('Are you sure to remove this record ?')) {
            $(this).parent().remove();
            if (fnDelete !== undefined) fnDelete($(this));

            // $.ajax({
            //     url: base_url + 'backend/booking_questions/delete/' + id,
            //     type: 'DELETE',
            //     error: function () {
            //         alert('Something is wrong');
            //     },
            //     success: function (data) {
            //         $("#" + id).remove();
            //         alert("Record removed successfully");
            //     }
            // });
        }
    });
}


function sortable(domId, fnUpdate) {
    var _preventDefault = function (evt) {
        evt.preventDefault();
    };

    function rerank() {
        var i = 1;
        $(domId).find('.rank_no').each(function () {
            $(this).text(i);
            i = i + 1;
        });
    }

    rerank();
    $(domId).find(".handle").bind("dragstart", _preventDefault).bind("selectstart", _preventDefault);
    $(domId).sortable({
        axis: "y",
        containment: "parent",
        cursor: "move",
        handle: '.handle',
        update: function () {
            // alert('update');
            rerank();
            var order = $(domId).sortable('serialize');
            if (fnUpdate !== undefined) {
                fnUpdate(order);
            }

            // Example of fnUpdate 
            // var postData = {
            //     'table_name': table_name,
            //     'rank_name': rank_name,
            //     'id_name': id_name,
            //     'order': order,
            // }
            // postData[csrf_name] = csrf_hash;
            // $.ajax({
            //     type: 'POST',                     //GET or POST
            //     url: base_url + '/backend/booking_questions/update_rank',               //請求的頁面
            //     cache: false,                     //防止抓到快取的回應
            //     data: postData,      //要傳送到頁面的參數
            //     success: function (data) {
            //         $("#info").append('<b style="color:red;" class="this_info" id="this_info">更新成功!!</b>');
            //         setTimeout(function () {
            //             $('.this_info').fadeOut(500);
            //         }, 2000);
            //     },         //當請求成功後此事件會被呼叫
            //     // error: functionFailed,            //當請求失敗後此事件會被呼叫
            //     statusCode: {                     //狀態碼處理
            //         404: function () {
            //             alert("page not found");
            //         }
            //     }
            // });
        }
    });
}

function barchart(domId) {
    var id = domId.replace('#', '');
    var el = $(domId);
    var ctx = document.getElementById(id).getContext('2d');
    var chartData = el.data();
    var data = {
        labels: chartData.labels,
        datasets: [{
            data: chartData.data,
            backgroundColor: chartData.bgcolor,
            barThickness: chartData.thickness
        }]
    }
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            "hover": {
                "animationDuration": 0
            },
            "animation": {
                "duration": 1,
                "onComplete": function () {
                    var chartInstance = this.chart,
                        ctx = chartInstance.ctx;

                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            ctx.fillText(data, bar._model.x, bar._model.y - 5);
                        });
                    });
                }
            },
            legend: {
                "display": false
            },
            tooltips: {
                "enabled": false
            },
            scales: {
                yAxes: [{
                    display: false,
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        max: Math.max(...data.datasets[0].data) + 1,
                        display: false,
                        beginAtZero: true
                    }
                }],
                xAxes: [{
                    barPercentage: 1,
                    categoryPercentage: 1,
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

function horizontalbarchart(domId) {
    var id = domId.replace('#', '');
    var el = $(domId);
    var ctx = document.getElementById(id).getContext('2d');
    var chartData = el.data();
    var data = {
        labels: chartData.labels,
        datasets: [{
            data: chartData.data,
            backgroundColor: chartData.bgcolor,
            barThickness: chartData.thickness
        }]
    }
    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: data,
        options: {
            "hover": {
                "animationDuration": 0
            },
            "animation": {
                "duration": 1,
                "onComplete": function () {
                    var chartInstance = this.chart,
                        ctx = chartInstance.ctx;

                    ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            ctx.fillText(data, bar._model.x + 15, bar._model.y + 6);
                        });
                    });
                }
            },
            legend: {
                "display": false
            },
            tooltips: {
                "enabled": false,
                "mode": "index",
                "axis": "y"
            },
            scales: {
                xAxes: [{
                    display: false,
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        max: Math.max(...data.datasets[0].data) + 150,
                        display: false,
                        beginAtZero: true
                    }
                }],
                yAxes: [{
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

function doughnutchart(domId) {
    var id = domId.replace('#', '');
    var el = $(domId);
    var ctx = document.getElementById(id).getContext('2d');
    var chartData = el.data();
    var data = {
        labels: chartData.labels,
        datasets: [{
            data: chartData.data,
            // backgroundColor: chartData.bgcolor,

            backgroundColor: ["#f2a1a0", "#d9e021", "#f98d2b", "#79c0e0", "#00b09c", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"]
        }]
    }
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: data,
        options: {
            weight: chartData.thickness,
            legend: {
                "display": false
            }
        }
    });
}