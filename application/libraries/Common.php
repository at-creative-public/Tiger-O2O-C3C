<?php

class Common
{
    private $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
    }

    /**
     * For absolute path, $this->common->make_directory_if_not_exist('your-path', '') instead
     * 
     * @param string $__path
     * @param string $prefix
     * @return string
     */
    public function make_directory_if_not_exist($path, $prefix = APPPATH . '../assets/uploads/')
    {
        if ($prefix != '') $fullpath = $prefix . $path;
        // echo $path . '<br>';
        // die();
        if (!file_exists($fullpath)) {
            $path_segments = [];
            $segments = explode('/', $path);
            while (count($segments) > 0) {
                $segment = array_shift($segments);
                array_push($path_segments, $segment);
                if (in_array($segment, ['.', '..'])) continue;

                $_path = $prefix . implode('/', $path_segments);
                if (!file_exists($_path)) {
                    // echo $_path;
                    log_message('info', 'making directory: ' . $_path);
                    mkdir($_path, 0777, true);
                }
            }
        }

        return $fullpath;
    }


    /**
     * For absolute path, $this->common->get_files_from_directory('your-path', '') instead
     * 
     * @param string $str
     * @param string $prefix
     * @return array
     */
    public function get_files_from_directory($str, $prefix = APPPATH . '../assets/uploads/')
    {
        $path = rtrim($str, "/") . '/';
        $files = [];

        if ($prefix != '') $fullpath = $prefix . $path;

        if (!file_exists($fullpath)) {
            log_message('error', $fullpath . ' not found');
            return $files;
        }
        if ($handle = opendir($fullpath)) {
            while (false !== ($filename = readdir($handle))) {
                if (in_array($filename, ['.', '..'])) {
                    continue;
                }

                $file = $fullpath . $filename;
                array_push($files, [
                    'name' => basename($file),
                    'type' => '',
                    'tmp_name' => '',
                    'error' => '',
                    'size' => $this->convert_readable_filesize(filesize($file)),
                    'formatted_size' => $this->human_filesize(filesize($file)),
                ]);
            }

            closedir($handle);
        }

        return $files;
    }

    public function convert_readable_filesize($bytes)
    {
        $factor = floor((strlen($bytes) - 1) / 3);
        return $bytes / pow(1024, $factor);
    }

    public function human_filesize($bytes, $decimals = 1)
    {
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);

        return sprintf("%.{$decimals}f", $this->convert_readable_filesize($bytes, $decimals)) . ' ' . @$sz[$factor] . 'B';
    }

    public function single_upload($upload_path, $upload_filename, $file_keyname = 'file')
    {
        if (count($_FILES) == 0) return;
        $upload_path = rtrim($upload_path, "/") . '/';
        // $config['upload_path'] = 'uploads/';
        // $config['allowed_types'] = 'jpg|jpeg|png|gif';
        // $config['max_size'] = '5000';
        $config['file_name'] = $upload_filename;
        $this->CI->load->library('upload', $config);

        /**
         * @var CI_Upload
         */
        $upload = $this->CI->upload;

        $upload->set_upload_path($upload_path);
        $upload->detect_mime = FALSE;
        $upload->allowed_types = "*";
        $upload->do_upload($file_keyname);

        return $upload->error_msg;
    }

    /**
     * 
     *
     * @param string $upload_path
     * @param string $file_keyname
     * @return array
     */
    public function multiple_upload($upload_path, $file_keyname = 'upload')
    {
        if (count($_FILES) == 0) return;
        $count = count($_FILES[$file_keyname]['name']);

        $data = [];
        for ($i = 0; $i < $count; $i++) {

            if (!empty($_FILES[$file_keyname]['name'][$i])) {

                $_FILES['file']['name'] = $_FILES[$file_keyname]['name'][$i];
                $_FILES['file']['type'] = $_FILES[$file_keyname]['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES[$file_keyname]['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES[$file_keyname]['error'][$i];
                $_FILES['file']['size'] = $_FILES[$file_keyname]['size'][$i];

                $filename = $_FILES[$file_keyname]['name'][$i];
                if ($this->single_upload($upload_path, $filename, 'file')) {
                    $uploadData = $this->CI->upload->data();
                    $filename = $uploadData['file_name'];

                    $data[] = $filename;
                }
            }
        }
        if (property_exists($this->CI, 'upload')) {
            return !empty($this->CI->upload->error_msg) ? $this->CI->upload->error_msg : $data; 
        }
        return $data;
        // return $this->CI->upload->error_msg;
        // print_r($this->CI->upload->error_msg);
        // die();
    }

    public function unlink_file($filename, $folder = '')
    {
        if (!empty($folder)) $folder = rtrim($folder, "/") . '/';
        $file = APPPATH . '../assets/uploads/' . $folder . $filename;
        if (file_exists($file)) unlink($file);
    }

    public function rename_file($new_file, $old_file)
    {
        @rename($new_file, $old_file);
    }
}
