<?php

namespace app\libraries;

use Exception;

class CSVClassImportDataException extends Exception
{
}
class CsvClassImportResult
{
    public  $errors;
    public $res;

    public function __construct($errors, $res)
    {
        $this->errors = $errors;
        $this->res = $res;
    }

    public function expose()
    {
        return ["errors" => $this->errors->expose(), "items" => $this->res];
    }
}
class CsvClassImportErrorItems
{
    public $items = [];

    public function add(CsvClassImportErrorItem $item)
    {
        array_push($this->items, $item);
    }

    public function has_error()
    {
        return count($this->items) > 0;
    }

    public function expose()
    {
        $errors = [];
        if ($this->has_error()) {
            /**
             * @var CsvClassImportErrorItem $item
             */
            foreach ($this->items as $item) {
                array_push($errors, $item->expose());
            }
        }
        return $errors;
    }
}
class CsvClassImportErrorItem
{
    public $item_array;
    public $message;

    public function __construct($item_array, $message)
    {
        $this->item_array = $item_array;
        $this->message = $message;
        // $this->message = $message;
    }

    public function expose()
    {
        return get_object_vars($this);
    }
}
class CSVClass
{
    /**
     * Using POST method to import data
     * 
     *
     * @return void
     */
    public function import($file, $omit_first_row = true, $required_label_attributes = [], $data_limit = 0, $delimiter = ',', $callback)
    {
        if (!isset($file["tmp_name"]) || !isset($file["size"])) throw new CSVClassImportDataException("\$file 'tmp_name' or 'size' not found in \$_FILES !");

        $filename = $file["tmp_name"];
        if ($file["size"] <= 0) throw new CSVClassImportDataException("Filesize is invalid.");
        $name = $_FILES["file"]["name"];
        $parts = explode(".", $name);
        if (array_pop($parts) != 'csv') throw new CSVClassImportDataException("File extenstion is invalid.");

        $file = fopen($filename, "r");

        while (($row = fgetcsv($file, $data_limit, $delimiter)) !== FALSE) {
            if ($omit_first_row) {
                // As omitted, change this flag to false
                $omit_first_row = false;
                $index = 0;
                foreach ($required_label_attributes as $attribute) {
                    $first_row_fields_value = strtolower($row[$index++]);
                    $attribute_name = strtolower($attribute);
                    if ($first_row_fields_value != $attribute_name) {
                        throw new CSVClassImportDataException(sprintf("The first row %s does not contain either one of attributes %s", implode(", ", $row), implode(", ", $required_label_attributes)));
                    }
                }

                // Do the omit job
                continue;
            }

            if (array(null) !== $row) { // ignore blank lines
                $callback($row);
            }
        }
    }


    public function export($labelAttributes, $resultArray, $filename, $delimiter = ',')
    {
        $sort_keys = [];
        $sort_values = [];
        foreach ($labelAttributes as $key => $attribute) {
            if (!empty($attribute['omitted']) && $attribute['omitted'] == true) continue;
            else {
                array_push($sort_keys, $key);
                array_push($sort_values, $attribute['value']);
            }
        }

        // open raw memory as file so no temp files needed, you might run out of memory though
        $f = fopen('php://memory', 'w');
        fputcsv($f, $sort_values, $delimiter);
        // loop over the input array
        foreach ($resultArray as $result) {
            $row = [];
            foreach ($sort_keys as $key) {
                // generate csv lines from the inner arrays
                array_push($row, $result[$key]);
            }
            fputcsv($f, $row, $delimiter);
        }
        // reset the file pointer to the start of the file
        fseek($f, 0);
        // tell the browser it's going to be a csv file
        header('Content-Type: application/csv');
        // tell the browser we want to save it instead of displaying it
        header('Content-Disposition: attachment; filename="' . $filename . '";');
        // make php send the generated csv lines to the browser
        fpassthru($f);
    }
}
