<?php

use SendGrid\Mail\Mail;
use SendGrid as Client;
use SendGrid\Response;

class SendgridClient
{
    // "http://stsa.at-appmaker.com/" was successfully created and added to the next step.
    private $SENDGRID_API_KEY = ''; //Input the sendgrid api key

    /**
     * Create a Sendgrid Mail instance
     * 
     * $email->setFrom("test@example.com", "Example User");
     * $email->setSubject("Sending with SendGrid is Fun");
     * $email->addTo("test@example.com", "Example User");
     * $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
     * $email->addContent(
     *    "text/html", "<strong>and easy to do anywhere, even with PHP</strong>"
     * );
     *
     * @return \SendGrid\Mail\Mail
     */
    public function create()
    {
        return new Mail();
    }

    public function send(Mail $mail)
    {
        $sendgrid = new Client($this->SENDGRID_API_KEY);
        try {
            /**@var Response $response */
            $response = $sendgrid->send($mail);
            // print $response->statusCode() . "\n";
            // print_r($response->headers());
            // print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: ' . $e->getMessage() . "\n";
        }
    }
}
