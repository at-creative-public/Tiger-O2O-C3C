<?php

use app\libraries\paypal\PaypalClient;

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property Order_output_model $Order_output_model
 */
class Order_model extends MY_Model
{
    public $tbl = 'orders';

    public function table_view($limit, $per_page, $searchValue = "")
    {
        $this->load->library('pagination');
        $this->config->load('pagination');

        $limits = [10, 25, 50, 100];
        // Show per page
        $limit = $limit ?? $limits[0];
        // Page number
        $per_page = $per_page ?? 0;
        $total_rows = $this->table_view_count($searchValue);
        // $this->pagination->initialize($config);

        $users = $this->table_view_get([
            'limit' => $limit,
            'offset' => $per_page
        ], $searchValue);

        $config = $this->config->item('pagination');
        $config['base_url'] = base_url('admin_user');
        $config['total_rows'] = $total_rows;
        $this->pagination->initialize($config);


        $cur_page = $per_page + 1;
        $next_page = ($cur_page + 1) * $limit > $total_rows ? $cur_page : $cur_page + 1;
        $max_page = floor($total_rows % $limit > 0 ? $total_rows / $limit + 1 : $total_rows / $limit);

        $prev_page = $cur_page == 1 ? 1 : $cur_page - 1;
        return [
            'total_rows' => $total_rows,
            'limit' => (int) $limit,
            'per_page' => $this->pagination->per_page,
            'per_pages' => $limits,
            'min_limit' => $limits[0],
            'max_limit' => $limits[count($limits) - 1],
            'prev_page' => $prev_page,
            'curr_page' => $cur_page,
            'next_page' => $next_page,
            'max_page' => $max_page,
            'prev_link' => '#',
            'next_link' => '#',
            'items' => $users,
        ];
    }

    private function table_view_get_criteria($searchValue)
    {
        $this->db->from($this->table_prefix . $this->tbl);
        // $this->db->join('rbac_users_roles ur', 'ur.user_id = u.id');
        // if (count($exclude_roles) > 0) {
        //     $this->db->where_not_in('ur.role_id', $exclude_roles);
        // }
        // $this->db->where_not_in('ur.role_id', [1]);
        // $this->db->where('u.deleted_at IS NULL', null, false);

        if (trim($searchValue)) {
            $performSearchInFields = ['name'];

            $concatSegment = implode(', " ",', $performSearchInFields);
            $concatWhere = sprintf('CONCAT(%s) LIKE ', $concatSegment) . '"%' . $searchValue . '%"';
            $this->db->where($concatWhere, null, false);
        }

        $this->db->where('deleted_at IS NULL', null, false);
    }

    /*
     * Get all users
     */
    public function table_view_get($params = array(), $searchValue)
    {
        $this->table_view_get_criteria($searchValue);

        if (isset($params) && !empty($params)) {
            $this->db->limit($params['limit'], $params['offset'] > 0 ? $params['limit'] * $params['offset'] : $params['offset']);
        }

        $res = $this->db
            ->order_by('created_at', 'desc')
            ->get()->result_array();

        // log_message('debug', $this->db->last_query());
        return $res;
    }

    /*
     * Get all users count
     */
    public function table_view_count($search)
    {
        $this->table_view_get_criteria($search);

        $count = $this->db->count_all_results();
        $this->db->reset_query();
        return $count;
    }

    public function add_order($params)
    {
        $id = $this->insertDB($this->database, $this->tbl, $params, false, [
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' =>  $this->session->userdata('userid') ?? 0,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' =>  $this->session->userdata('userid') ?? 0,
        ]);

        $params['id'] = $id;
        return $params;
    }

    public function get_all_orders()
    {
        return $this->getAll($this->database, $this->tbl, [], ['deleted_at' => null]);
    }

    public function get_order($id, $select_fields = [])
    {
        return $this->getOneRow($this->database, $this->tbl, $select_fields, ['id' => $id, 'deleted_at' => null]);
    }

    public function edit_order($params, $id)
    {
        $updateParams = [
            'name' => $params['name'],
            'published_at' => $params['published_at'],
            'end_date' => $params['end_date'],
            'years_of_maintenance' => $params['years_of_maintenance'],
            'price' => $params['price'],
        ];
        $this->Product_model->saveDB($this->Product_model->database, $this->Product_model->tbl, $updateParams, $id, 'id', [
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => $this->session->userdata('userid'),
        ]);
    }

    public function paypal_checkout($params)
    {
        // $params = [
        //     "payment_description" => "付款",
        //     "payment_total" => 1840.00,
        //     "payment_currency" => 'HKD',
        //     "total_quantity" => 3,
        //     "product_json" => json_decode('[{"id":1,"name":"HND90-4D 即熱式水機濾蕊","quantity":1,"currency":"HKD","price":540},{"id":2,"name":"JVG S7 電解水機濾蕊","quantity":1,"currency":"HKD","price":600},{"id":3,"name":"JVG 水療淋浴花灑","quantity":1,"currency":"HKD","price":700}]', true),
        //     "billing_address_1" => 'Billing address 1',
        //     "billing_address_2" => 'Billing address 2' ?? '',
        //     "billing_address_3" => 'Billing address 3' ?? '',
        //     "shipping_address_1" => 'Shipping address 1',
        //     "shipping_address_2" => 'Shipping address 2' ?? '',
        //     "shipping_address_3" => 'Shipping address 3' ?? '',
        //     "member_id" => 1,
        //     "remarks" => null,
        // ];
        $member_id = $params["member_id"];

        $this->load->model("Member_model");
        $this->load->model("Product_model");
        $member = $this->Member_model->get_member($member_id, ["id", "code", "display_name", "phone", "email", "birth_date", "gender", "remarks", "correspondence_address_1", "correspondence_address_2", "last_login_at"]);
        $params["member_json"] = json_encode($member);
        $params["product_json"] = $params["item_list"];
        unset($params["item_list"]);

        $params["purchase_date"] = date('Y-m-d H:i:s');

        $payment_description = $params["payment_description"];

        unset($params["payment_description"]);

        $order = $this->add_order(array_merge($params, ["product_json" => json_encode($params["product_json"])]));

        $success_url = base_url('paypal/success?order_id=' . $order['id']);
        $cancel_url = base_url('paypal/cancel?order_id=' . $order['id']);

        $this->load->model('Order_payment_paypal_model');
        $paypal = $this->Order_payment_paypal_model->paypal_create_payment($payment_description, $params, $member, $success_url, $cancel_url);
        $paypal_transaction = $paypal['transaction'];
        $paypal_transaction['order_id'] = $order['id'];
        $this->Order_payment_paypal_model->add_order_payment_paypal($paypal_transaction);

        header(sprintf('Location: %s', $paypal['approval_link']));
        // $params = json_decode('{"payment_total":1840.0,"payment_curre
    }

    public function paypal_checkout_success($order_id, $payment_id, $payer_id)
    {
        $this->load->model('Order_payment_paypal_model');
        try {
            $paypal = $this->Order_payment_paypal_model->paypal_execute($payment_id, $payer_id);
            $this->Order_payment_paypal_model->update_order_payment_paypal($paypal['transaction'], $order_id, $payment_id);
        } catch (Exception $ex) {
            $this->Order_payment_paypal_model->paypal_exception($order_id, $payment_id, $ex);
        }

        $this->load->model('Order_payment_model');
        $this->Order_payment_model->add_order_payment([
            'order_id' => $order_id,
            'order_id' => $order_id,
            'payment_method' => 'paypal',
            'amount_paid' => $paypal['amount']['total'],
        ]);

        $order = $this->get_order($order_id, ['id', 'member_id', 'member_json', 'product_json', 'created_by']);
        $member = json_decode($order['member_json'], true);
        $order_products = json_decode($order['product_json'], true);

        $this->load->model("Rbac_user_model");
        $staff = $this->Rbac_user_model->get_user($order['created_by']);

        $output = [];
        $date = date('Y-m-d H:i:s');

        foreach ($order_products as $order_product) {
            array_push($output, [
                'order_id' => $order_id,
                'member_id' => $order['member_id'],
                'member_display_name' => $member['display_name'],
                'product_id' => $order_product['id'],
                'product_name' => $order_product['name'],
                'quantity' => $order_product['quantity'],
                'amount_paid' => $order_product['price'],
                'years_of_maintenance' => (int) $order_product['years_of_maintenance'],
                'end_date' => date('Y-m-d H:i:s', strtotime('+' . (int) $order_product['years_of_maintenance'] . 'year, -1 day', (new DateTime($date))->getTimestamp())),
                'staff_id' => $order['created_by'],
                'staff_name' => $staff['display_name'],
                'payment_date' => $date,
                'payment_method' => 'paypal',
                'updated_at' => $date,
                'created_at' => $date,
            ]);
        }

        $this->load->model('Order_output_model');
        $this->Order_output_model->create_batch($output);

        // TODO: connect Order_maintenance_model when requested
    }

    public function paypal_checkout_cancelled($order_id, $payment_id, $payer_id)
    {
        // No handling at this moment
    }
}
