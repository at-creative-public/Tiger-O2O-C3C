<?php

class Related_documents_model extends MY_Model
{
    public $tbl = 'related_documents';
    public $fillable = ['path', 'display_order'];
}
