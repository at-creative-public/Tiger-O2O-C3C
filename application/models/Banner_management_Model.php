<?php

defined("BASEPATH") or exit("No direct script access allowed");
class Banner_management_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_id($main_id, $sub_id = "")
    {
        if ($sub_id == "") {
            return $this->db->select("*")
                ->from("tbl_page_banner")
                ->where('display', '1')
                ->where("main_id", $main_id)
                ->get()
                ->result_array();
        } else {
            return $this->db->select("*")
                ->from("tbl_page_banner")
                ->where('display', '1')
                ->where("main_id", $main_id)
                ->where("sub_id", $sub_id)
                ->get()
                ->result_array();
        }
    }

    public function update($main_id, $type, $path, $sub_id = "")
    {
        if ($type == "Desktop") {
            if ($sub_id == "") {
                $data = [
                    "main_id" => $main_id,
                    "type" => "Desktop_banner",
                    "path" => $path
                ];
                $this->db->replace("tbl_page_banner", $data);
            } else {
                /* $this->db->set("main_id", $main_id)
                    ->set("sub_id", $sub_id)
                    ->set("type", "Desktop_banner")
                    ->set('path', $path)*/
                $data = [
                    "main_id" => $main_id,
                    "sub_id" => $sub_id,
                    "type" => "Desktop_banner",
                    "path" => $path
                ];
                $this->db->replace("tbl_page_banner", $data);
            }
        }
        if ($type == "Mobile") {
            if ($sub_id == "") {
                $data = [
                    "main_id" => $main_id,
                    "type" => "Mobile_banner",
                    "path" => $path
                ];
                $this->db->replace('tbl_page_banner', $data);
            } else {
                $data = [
                    "main_id" => $main_id,
                    "sub_id" => $sub_id,
                    "type" => "Mobile_banner",
                    "path" => $path
                ];
                $this->db->replace('tbl_page_banner', $data);
            }
        }
    }
}
