<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page_object_title_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id)
    {
        return $this->db->where('id', $id)
           
            ->from('_page_object_title')
            ->get()->row_array();
    }

    public function update($params, $id)
    {
        $this->db->where('id', $id)
            ->update('_page_object_title', $params);
    }
    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('_page_object_title');
    }
}
