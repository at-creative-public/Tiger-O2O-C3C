<?php


use app\libraries\CSVClass;

class Event_ticket_memberbase_model extends CI_Model
{

    public function build_datatables($draw, $total, $filtered, $data)
    {
        return [
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $filtered,
            "data" => $data,
        ];
    }

    public function get_id($id)
    {
        return $this->db->select('*')->from("tbl_event_ticket_holder")->where("id", $id)->get()->row_array();
    }

    public function insert_user($params)
    {
        $this->db->insert("tbl_event_ticket_holder", $params);
        return $this->db->insert_id();
    }

    public function update_info($params, $id)
    {
        unset($params['id']);
        $this->db->update("tbl_event_ticket_holder", $params, ['id' => $id]);
    }

    public function listing($params, $ticket_id)
    {
        $fields = ["id", "display_code","name","surname","given_name","english_name","chinese_name","email","phone","enter_time","quit_time", "event_code_series", "status","CONCAT(name,english_name,chinese_name,given_name,surname) as display_name", "concat_ws(' - ',`quit_time`,`enter_time`) as attendance,api,ref_id"];
        $start = $params['start'];
        $length = $params['length'];
        //$orders = $params['order'][0]['dir'];
         switch ($params["columns"][$params['order'][0]['column']]["name"]) {
            case "name";
                $orders["field"] = "display_name";
                $orders["dir"] = $params['order'][0]["dir"];
                break;
            case "attendace":
                $orders["field"] = "attendace";
                $orders["dir"] = $params['order'][0]["dir"];
                break;
            default:
                $orders["field"] = $params["columns"][$params['order'][0]['column']]["name"];
                $orders["dir"] = $params['order'][0]["dir"];
        };
        $search = $params['columns'][0]["search"]["value"];

        $result = $this->getFromDBFilter("tbl_event_ticket_holder", $fields, $start, $length, $orders, $search, [], "", $ticket_id);
        $data = [];
        foreach ($result as $row) {
            $attendace = "";
            switch(true){
                case $row["enter_time"] === "" && $row["quit_time"] === "":
                    $attendace = "absent";
                    break;
                case $row["enter_time"] !== "" && $row["quit_time"] === "":
                    $attendace = "attending";
                    break;
                case $row["enter_time"] === "" && $row['quit_time'] !== "":
                    $attendace = "leave";
                    break;
                case $row['enter_time'] !== "" && $row["quit_time"] !== "":
                    $attendace = "leave";
                    break;
            }
            $name = "";
            switch(true){
                case $row['name'] !== "":
                    $name = $row['name'];
                    break;
                case $row['english_name'] !== "":
                    $name = $row['english_name'];
                    break;
                case $row['chinese_name'] !== "";
                    $name = $row['chinese_name'];
                    break;
                case $row['surname'] !== "" || $row['given_name'] !== "":
                    $name = $row['given_name']." ".$row['surname'];
                    break;
                default:
                    $name = "No Information";
                    break;
            }
            $data[] = [
                "event_code_series" => $row['api'] == 1 ?"NaN":$row['event_code_series'],
                "display_code" => $row['api'] == 1 ?$row['ref_id']:$row['display_code'],
                "name" => $name,
                "email" => $row["email"],
                "phone" => $row['phone'],
                "attendance" => $attendace,
                "status" => $row['status'],
                "qr_code" => "<a data-id='".$row['id']."' class='btn btn-default qr_button font-weight-bold'>QR</a> ",
                "invite" => $row['email'] != "" ? "<a data-id='".$row['id']."' class='btn btn-primary text-white email_button font-weight-bold'>Invite</a>":"",
                "view" => "<a data-id='" . $row['id'] . "' class='btn btn-success detail_button font-weight-bold text-white'>view</a>",
                "void" => $row['api'] == 1 ? "":($row['status'] == "void" ? "" : ("<a data-id='" . $row['id'] . "' class='btn btn-danger void_button font-weight-bold text-white'>void</a>"))
            ];
        }
        return $data;
    }

    public function getFromDBFilter($table, $fields, $start, $length, $orders, $search, $joins = [], $keywordfield = "", $ticket_id)
    {
        $this->db->distinct();
        $this->db->select(implode(",", $fields));
        $this->db->from($table);
        $this->db->where("event_ticket_id", $ticket_id);
        if ($search !== "") {
            $this->db->group_start();
            $this->db->or_like("event_code_series", $search);
            $this->db->or_like("name",$search);
            $this->db->or_like("given_name", $search);
            $this->db->or_like("surname", $search);
            $this->db->or_like("english_name", $search);
            $this->db->or_like("chinese_name", $search);
            $this->db->or_like("phone", $search);
            $this->db->or_like("email", $search);
            $this->db->group_end();
        }
        //$this->db->order_by("event_code_series", $orders);
        $this->db->order_by($orders['field'], $orders['dir']);
        $this->db->limit($length, $start);
        return $this->db->get()->result_array();
    }

    public function filteredCounter($params, $ticket_id)
    {
        $this->db->select("*");
        $this->db->from("tbl_event_ticket_holder");
        $this->db->where("event_ticket_id", $ticket_id);
        $search = $params['columns'][0]["search"]['value'];
        if ($search !== "") {
            $this->db->group_start();
             $this->db->or_like("event_code_series", $search);
            $this->db->or_like("given_name", $search);
            $this->db->or_like("surname", $search);
            $this->db->or_like("english_name", $search);
            $this->db->or_like("chinese_name", $search);
            $this->db->or_like("phone", $search);
            $this->db->or_like("email", $search);
            $this->db->group_end();
        }
        return $this->db->count_all_results();
    }

    public function totalCount($ticket_id)
    {
        return $this->db->from("tbl_event_ticket_holder")->where("event_ticket_id", $ticket_id)->count_all_results();
    }

    public function get_holder($id)
    {
        return $this->db->select('*')->from("tbl_event_ticket_holder")->where("id", $id)->get()->row_array();
    }
    
    public function get_holders($id){
        return $this->db->select("*")->from("tbl_event_ticket_holder")->where("event_ticket_id",$id)->get()->result_array();
    }

    public function get_apple_holder($id)
    {
        return $this->db->select("*")->from("tbl_event_ticket_holder")->where("event_ticket_id", $id)->where("device_token !=", "")->get()->result_array();
    }
    
    public function get_apple_holders(){
        return $this->db->select("*")->from("tbl_event_ticket_holder")->where("device_token !=", "")->get()->result_array();
    }

    public function get_google_holder($id)
    {
        return $this->db->select("*")->from("tbl_event_ticket_holder")->where("event_ticket_id",$id)->where("object_id !=","")->get()->result_array();
    }

    public function update_device_token($serial_code, $device_token)
    {
        $serial_num = explode("_", $serial_code);
        $this->db->update("tbl_event_ticket_holder", ['device_token' => $device_token], ["id" => $serial_num[1]]);
    }

    public function update_object_id($objectId, $holder_id)
    {
        $this->db->update("tbl_event_ticket_holder", ["object_id" => $objectId], ['id' => $holder_id]);
    }
    
    public function store_apple_qr($path,$holder_id,$hash){
        $this->db->update("tbl_event_ticket_holder",['apple_qr'=>$path,"hash"=>$hash],["id"=>$holder_id]);
    }
    
    public function store_google_qr($path,$holder_id){
        $this->db->update("tbl_event_ticket_holder",['google_qr'=>$path],["id"=>$holder_id]);
    }
    
    public function store_url($url, $holder_id)
    {
        $this->db->update("tbl_event_ticket_holder", ['google_re' => $url], ['id' => $holder_id]);
    }
    

    
    public function search_user($code,$ids){
        return $this->db->select("*")->from("tbl_event_ticket_holder")->where("status","valid")->where("display_code",$code)->where_in("event_ticket_id",$ids)->get()->row_array();
    }
    
    public function get_user_from_code($code){
        return $this->db->select("*")->from("tbl_event_ticket_holder")->where("display_code",$code)->get()->row_array();
    }
    
    public function update_enter_record($code){
        $this->db->update("tbl_event_ticket_holder",["enter_time"=>date("Y-m-d H:i:s",time())],["display_code"=>$code]);
    }
    
    public function update_exit_record($code){
        $this->db->update("tbl_event_ticket_holder",["quit_time"=>date("Y-m-d H:i:s",time())],["display_code"=>$code]);
    }
    
    
    public function void($id){
        $this->db->update("tbl_event_ticket_holder",['status'=>"void"],['id'=>$id]);
    }
    
    public function export_csv($ticket_id){
        $event_ticket_info = $this->db->select('*')->from("tbl_event_ticket")->where("id",$ticket_id)->get()->row_array();
        $filename = "Event_Ticket_".$event_ticket_info['event_name']."_export_".date("H:i d-m-Y").".csv";
        $infos = $this->get_holders($ticket_id);
        $e = new CSVClass();
          return $e->export(
            [
                "event_code_series" => ["value"=>"event_code_series"],
                "display_code" => ["value"=>"display_code"],
                "seat"=>["value"=>"seat"],
                "row"=>["value"=>"row"],
                "section" => ["value"=>"section"],
                "name" => ["value" => "name"],
                "surname" => ["value" => "surname"],
                "given_name" => ["value" => "given_name"],
                "english_name" => ["value" => "english_name"],
                "chinese_name" => ['value' => "chinese_name"],
                "gender" => ["value" => "gender"],
                "birthday" => ["value" => "birthday"],
                "age" => ["value" => "age"],
                "nationality" => ["value" => "nationality"],
                "phone" => ["value" => "phone"],
                "email" => ["value" => "email"],
                "address" => ["value" => "address"],
                "industry" => ["value" => "industry"],
                "industry_2" => ["value" => "industry_2"],
                "job_title" => ["value" => "job_title"],
                "company" => ["value" => "company"],
                "salary" => ["value" => "salary"],
                "enter_time" => ["value"=>"enter_time"],
                "quit_time" => ["value"=>"quit_time"],
                "status" => ["value" => "status"]
            ],
            $infos,
            $filename
        );
    }
    
}
