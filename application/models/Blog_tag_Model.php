<?php
class Blog_tag_Model extends CI_Model{
        
        function insert($arr)
        {
                
                $this->db->insert('_blog_tag_list', $arr);
                return $this->db->insert_id();
                
        }
        
        function update($id, $arr)
        {
        
                $this->db->where('id', $id);
                $this->db->update('_blog_tag_list', $arr);
        
        }
        
        function get()
        {       

                    $this->db->select('blog_tag_list.id,blog_tag_list.name,blog_tag_list.color,blog_tag_list.status, blog_tag_list.deleted, blog_tag_list.display_order, blog_tag_list.remark, blog_tag_list.update_time, blog_tag_list.create_time');
                    
                   
                    
                    $this->db->from('_blog_tag_list blog_tag_list');
                    $this->db->where('deleted', 0);
                     $this->db->order_by("blog_tag_list.display_order desc");
                    $query=$this->db->get();
                    //echo $this->db->last_query();
                return $query->result();
        }
        
        function get_id($id){
                $this->db->where('id', $id);
                $query = $this->db->get('_blog_tag_list');
                return $query->result();
        }
        
        function get_lang($lang)
        {
                
                $this->db->where('lang', $lang);
                $query = $this->db->get('_blog_tag_list');
                return $query->result();
                
        }
        
        function delete($id)
        {
                $arr=array();
                $arr['deleted'] =1;
                $this->db->where('id', $id);
                $this->db->update('_blog_tag_list', $arr);
        }
        
}

?>