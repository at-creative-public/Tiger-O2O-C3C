<?php

/**
 * @property MY_IndexCrudModel $this
 */
class Booking_questions_submit_model extends MY_IndexCrudModel
{
    public $has_displayorder = false;
    public $table_prefix = '';
    public $tbl = 'booking_questions_submit';
    public $short = 'questions_submit';
    public $database = 'default';

    public function delete_by_booking_id($booking_id)
    {
        return $this->db->delete($this->table_prefix . $this->tbl, ['booking_id' => $booking_id]);
    }
}
