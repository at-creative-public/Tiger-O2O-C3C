<?php
defined("BASEPATH") or exit("No direct script access allowed");

/** 
 * Booking model
 * 
 * @property MY_Model $this
 * @property MY_IndexCrudModel $this
 * 
 * ALTER TABLE `tblbooking_record` ADD `timeslot_id` INT DEFAULT 0 AFTER `remark`;
 */
class Booking_model extends MY_IndexCrudModel
{
    public $has_displayorder = false;
    public $table_prefix = '';
    public $tbl = 'booking_record';
    public $short = 'bkgr';
    public $database = 'default';

    public $ages = [
        "" => "年齡組別",
        "18-25" => "18-25",
        "25-35" => "25-35",
        "35-45" => "35-45",
        "45-60" => "45-60",
        "60+" => "60+",
    ];
    public $genders = [
        "" => "性別",
        "M" => "男",
        "F" => "女"
    ];
    public $all_services;
    public $services;
    public $service_groups;
    // use construct to make key => value array
    private $_services = [
        "白" => [
            "激光脫毛療程",
            "幹細胞肌底活膚療程"
        ],
        "滑" => [
            "Bellasonic 皮膚打氣機療程",

            "水感透肌修護療程"
        ],
        "彈" => [
            "Liftera V-Lift 無痛瘦面(局部)療程",
            "水感透肌補濕療程"
        ],
        "身體療程" => [
            "淋巴引流排毒按摩",
            "熱石按摩",
            "行氣活血養生療程"
        ],
        "美甲服務" => [
            "光療樹脂手甲",
            "Soak Off可卸式光療指甲",
            "手部護理"
        ],
        "醫學療程" => [
            "Siax極速V面輪廓提升療程"
        ],
        "纖體護理" => [
            "雙極射頻定位爆脂"
        ],
        "眼部療程" => [
            "雙效幹細胞抗皺眼部療程",
            "海洋膠原眼唇療程"
        ]
    ];

    public function __construct()
    {
        $this->all_services = $this->_services;
        $services = $service_groups = [];
        foreach ($this->_services as $service_group => $services_array) {
            array_push($service_groups, $service_group);
            $services = array_merge($services, $services_array);
        }

        $this->service_groups = array_combine($service_groups, $service_groups);
        $this->services = array_combine($services, $services);

        parent::__construct();
    }

    public function get_booking_data($selected_date)
    {
        $this->load->model('Booking_calendar_setting_model');
        $res = $this->Booking_calendar_setting_model->get();
        $item = $this->Booking_calendar_setting_model->flatten_keyname_value($res);
        $this->load->model('Calendar_timetable_model', 'cal');
        $this->cal->set_icsparser();
        $this->cal->load_setting_item($item, $this->Booking_calendar_setting_model->fields);
        $this->cal->generate_all_timeslots($this->cal->time_interval);
        $this->cal->get_available_timeslots_dates([$selected_date]);

        $this->load->model('Booking_questions_model');

        $this->load->helper('html');
        return [
            'cal' => $this->cal,
            'selected_date' => $selected_date,
            'booking_startdate' => $this->cal->booking_startdate,
            'booking_enddate' => $this->cal->booking_enddate,
            "services" => $this->all_services,
            'questions' => $this->Booking_questions_model->all()
        ];
    }

    public function send_new_booking_mail($params)
    {
        $email = $params["email"];
        // if ($this->is_test_email($email)) return;

        $title = '預約成功';
        $display_name = $params["name"];
        // $password = $params["password"];

        ob_start();
        error_reporting(0);
        $client = new \app\libraries\SendgridClient();
        $mail = $client->create();
        $mail->setFrom("no-reply@at-appmaker.com", "O2O C3C");
        $mail->setSubject($title);
        $mail->addTo($email, $display_name);
        if (!in_array($email, ['louis@at-creative.com'])) $mail->addBcc('louis@at-creative.com');

        $doc = new Document();
        $doc->loadHTMLFile('application/libraries/email.html');
        // $doc->getElementsByTagName('preheader')->item(0)->nodeValue = $title;
        $doc->getElementsByTagName('title')->item(0)->nodeValue = 'Contact';

        $doc->getElementById('greeting')->nodeValue = "致" . $display_name . " " . (empty($params['gender']) ? '先生/小姐' : ($params['gender'] == "M" ? "先生" : "小姐"));
        $c = $doc->getElementById('content');
        $c->nodeValue = "";
        $c->appendChild($doc->createElement('p', $title));
        $c->appendChild($doc->createElement('p', ""));
        $c->appendChild($doc->createElement('p', "專科項目：" . $params['service']));
        $c->appendChild($doc->createElement('p', "預約日期：" . $params['booking_date']));
        $c->appendChild($doc->createElement('p', "預約時間：" . $params['booking_time']));
        // $c->appendChild($doc->createElement('p', "姓名:" . $display_name));
        $c->appendChild($doc->createElement('p', "聯絡電話：" . $params['phone']));
        $c->appendChild($doc->createElement('p', "年齡組別：" . $params['age']));
        $c->appendChild($doc->createElement('p', "性別：" . $params['gender']));

        $html = $doc->saveHTML();

        $mail->addContent("text/html", $html);

        $client->send($mail);

        ob_get_clean();
    }

    public function get_services_by_service_group($service_group)
    {
        if (!empty($service_group) && array_key_exists($service_group, $this->all_services)) {
            $services = $this->Booking_model->all_services[$service_group];
            $services = array_combine($services, $services);
        } else {
            $services = $this->services;
        }

        return $services;
    }

    public function save_member($request)
    {
        $this->load->model('Member_model');
        $member = $this->Member_model->get_member(null, [], ['email' => $request['email']]);
        if (empty($member)) {
            $member = $this->Member_model->register([
                "display_name" => $request['name'],
                "phone" => $request['phone'],
                "email" => $request['email'],
                "age" => $request['age'],
                "gender" => $request['gender'],
                "remark" => $request['remark'],
            ], false, true);

            $this->Member_model->send_credentials_mail($member);
        }
        return $member;
        // TODO: member update
    }
}
