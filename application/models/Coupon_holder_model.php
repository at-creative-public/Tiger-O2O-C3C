<?php

use app\libraries\CSVClass;

class Coupon_holder_model extends CI_Model
{

    public function get_holder($id)
    {
        return $this->db->select("*")->from('tbl_coupons_holder')->where("id", $id)->get()->row_array();
    }

    public function get_holders($coupon_id)
    {
        return $this->db->select("*")->from("tbl_coupons_holder")->where("coupon_id", $coupon_id)->get()->result_array();
    }

    public function update_holder($params, $id)
    {
        unset($params['id']);
        $this->db->update("tbl_coupons_holder", $params, ['id' => $id]);
    }

    public function update_object_id($object_id, $id)
    {
        $this->db->update("tbl_coupons_holder", ["object_id" => $object_id], ["id" => $id]);
    }

    public function update_device_token($serial_code, $device_token)
    {
        $serial_num = explode("_", $serial_code);

        $this->db->update("tbl_coupons_holder", ["device_token" => $device_token], ["id" => $serial_num[1]]);
    }

    public function get_apple_holders()
    {
        return $this->db->select("*")->from("tbl_coupons_holder")->where("platform", "apple")->get()->result_array();
    }

    public function get_apple_holder($coupon_id)
    {
        return $this->db->distinct()->select("device_token")->from("tbl_coupons_holder")->where("platform", "apple")->where("coupon_id", $coupon_id)->get()->result_array();
    }

    public function get_google_holder($coupon_id)
    {
        return $this->db->select("*")->from("tbl_coupons_holder")->where("platform", "google")->where("coupon_id", $coupon_id)->get()->result_array();
    }

    public function search_holder($display_code, $range)
    {
        return $this->db->select("*")->from("tbl_coupons_holder")->where("display_code", $display_code)->where_in("coupon_id", $range)->get()->row_array();
    }

    public function void_coupon($id)
    {
        $this->db->update("tbl_coupons_holder", ['status' => "void", "void_date" => date("Y-m-d H:i", time())], ["id" => $id]);
    }



    public function build_datatables($draw, $total, $filtered, $data)
    {
        return [
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $filtered,
            "data" => $data,
        ];
    }

    public function listing($params, $coupon_id)
    {
        $fields = ["id","name","surname","given_name","english_name","chinese_name","email","phone", "display_code", "coupon_series", "status","CONCAT(name,english_name,chinese_name,given_name,surname) as display_name"];
        $start = $params['start'];
        $length = $params['length'];
        //$orders = $params['order'][0]["dir"];
         if ($params["columns"][$params['order'][0]['column']]["name"] == "name") {
            $orders["field"] = "display_name";
            $orders["dir"] = $params['order'][0]["dir"];
        } else {
            $orders["field"] = $params["columns"][$params['order'][0]['column']]["name"];
            $orders["dir"] = $params['order'][0]["dir"];
        }
        $search = $params['columns'][0]["search"]['value'];


        $result = $this->getFromDBFilter("tbl_coupons_holder", $fields, $start, $length, $orders, $search, null, null, $coupon_id);
        $data = [];
        foreach ($result as $row) {
            $name = "";
            switch(true){
                case $row['name'] !== "";$name=  $row['name'];break;
                case $row['english_name'] != "":$name = $row['english_name'];break;
                case $row['chinese_name'] != "":$name = $row['chinese_name'];break;
                case $row['surname'] != "" || $row['given_name'] != "":$name = $row['given_name']." ".$row['surname'];
                default:$name = "No Information";break;
            }
            $data[] = [
                "id" => $row['id'],
                "display_code" => $row['display_code'],
                "coupon_series" => $row["coupon_series"],
                "name" => $name,
                "email" => $row['email'],
                "phone" => $row['phone'],
                "status" => $row['status'],
                "view" => "<a data-id='" . $row['id'] . "' class='btn btn-primary detail_button font-weight-bold text-white'>view</a>",
                "edit" => '<a href="/merchant/coupon_memberbase/edit/'.$row['id'].'" class="btn btn-success font-weight-bold text-white">edit</a>',
                "void" => $row['status'] == "void" ? "" : ("<a data-id='" . $row['id'] . "' class='btn btn-danger void_button font-weight-bold text-white'>void</a>")
            ];
        }
        return $data;
    }

    public function getFromDBFilter($table, $fields, $start, $length, $orders, $search, $joins = [], $keywordfield = "", $coupon_id)
    {
        $this->db->distinct();
        $this->db->select(implode(",", $fields));
        $this->db->from($table);
        $this->db->where("coupon_id", $coupon_id);
        if ($search !== "") {
            $this->db->group_start();
            $this->db->or_like("coupon_series", $search);
            $this->db->or_like("name",$search);
            $this->db->or_like("given_name",$search);
            $this->db->or_like("surname",$search);
            $this->db->or_like("chinese_name",$search);
            $this->db->or_like("english_name",$search);
            $this->db->or_like("email",$search);
            $this->db->or_like("phone",$search);
            $this->db->group_end();
        }
        //$this->db->order_by("coupon_series", $orders);
        $this->db->order_by($orders['field'], $orders['dir']);


        $this->db->limit($length, $start);
        return $this->db->get()->result_array();
    }

    public function filteredCounter($params, $coupon_id)
    {
        $this->db->select("*");
        $this->db->from("tbl_coupons_holder");
        $this->db->where("coupon_id", $coupon_id);
        $search = $params['columns'][0]["search"]['value'];
        if ($search !== "") {
            $this->db->group_start();
            $this->db->or_like("coupon_series", $search);
            $this->db->or_like("name",$search);
            $this->db->or_like("given_name",$search);
            $this->db->or_like("surname",$search);
            $this->db->or_like("english_name",$search);
            $this->db->or_like("chinese_name",$search);
            $this->db->or_like("phone",$search);
            $this->db->or_like("email",$search);
            $this->db->group_end();
        }
        return $this->db->count_all_results();
    }

    public function totalCount($coupon_id)
    {
        return $this->db->from("tbl_coupons_holder")->where("coupon_id", $coupon_id)->count_all_results();
    }

    public function export_csv($coupon_id)
    {
        $coupon_info = $this->db->select("*")->from("tbl_coupons")->where('id', $coupon_id)->get()->row_array();
        $filename = "Coupon_" . $coupon_info['project_name'] . "_export_" . date("H-i d-m-Y") . ".csv";
        $infos = $this->get_holders($coupon_id);
        $e = new CSVClass();
        return $e->export(
            [
                "coupon_series" => ["value" => "coupon_series"],
                "coupon_name" => ["value" => "coupon_name"],
                "coupon_amount" => ["value" => "coupon_amount"],
                "expiry_date" => ["value" => "expiry_date"],
                "name" => ["value" => "name"],
                "surname" => ["value" => "surname"],
                "given_name" => ["value" => "given_name"],
                "english_name" => ["value" => "english_name"],
                "chinese_name" => ['value' => "chinese_name"],
                "gender" => ["value" => "gender"],
                "birthday" => ["value" => "birthday"],
                "age" => ["value" => "age"],
                "nationality" => ["value" => "nationality"],
                "phone" => ["value" => "phone"],
                "email" => ["value" => "email"],
                "address" => ["value" => "address"],
                "industry" => ["value" => "industry"],
                "industry_2" => ["value" => "industry_2"],
                "job_title" => ["value" => "job_title"],
                "company" => ["value" => "company"],
                "salary" => ["value" => "salary"],
                "status" => ["value" => "status"]
            ],
            $infos,
            $filename
        );
    }
}
