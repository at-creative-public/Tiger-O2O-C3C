<?php

class A_Statistics_model extends CI_Model
{

    public function __construct()
    {
    }

    public function get_merchants()
    {
        return $this->db->select('*')->from("tbl_merchants")->where("deleted_at", 0)->get()->num_rows();
    }

    public function sum_sales()
    {
        $result = $this->db->select("SUM(sales) as sum")->from("tbl_merchants")->get()->row_array();
        return $result['sum'];
    }

    public function get_distribution()
    {
        $pre_result = $this->db->select("merchant_plan,count(*) as num")->from("tbl_merchants")->where("deleted_at", 0)->group_by("merchant_plan")->get()->result_array();
        $result = ["Lite" => 0, "Pro" => 0, "Enterprise" => 0, "Expired" => 0];
        foreach ($pre_result as $process) {
            $result[$process['merchant_plan']] = $process['num'];
        }
        return $result;
    }

    public function get_all_pass()
    {
        $membership = $this->db->select("*")->from("tbl_virtual_passes")->where("display", 1)->get()->num_rows();
        $coupon = $this->db->select("*")->from("tbl_coupons")->where("display", 1)->get()->num_rows();
        $event_ticket = $this->db->select("*")->from("tbl_event_ticket")->where("display", 1)->get()->num_rows();
        $result = [
            "Membership" => $membership,
            "Coupon" => $coupon,
            "Event Ticket" => $event_ticket
        ];
        return $result;
    }

    public function get_cards_distribution()
    {
        $membership = $this->db->select("*")->from("tbl_members")->get()->num_rows();
        $coupon = $this->db->select("*")->from("tbl_coupons_holder")->get()->num_rows();
        $event_ticket = $this->db->select("*")->from("tbl_event_ticket_holder")->get()->num_rows();
        $result = [
            "Membership" => $membership,
            "Coupon" => $coupon,
            "Event Ticket" => $event_ticket
        ];
        return $result;
    }

    public function get_top_ten_merchant()
    {
        $membership_users = $this->db->select("pass_id,count(*) as num")->from("tbl_members")->group_by("pass_id")->get()->result_array();
        $form_membership_users = [];
        foreach ($membership_users as $index => $membership_list) {
            $form_membership_users[$membership_list['pass_id']] = $membership_list['num'];
        }


        $coupon_users = $this->db->select("coupon_id,count(*) as num")->from("tbl_coupons_holder")->group_by("coupon_id")->get()->result_array();
        $form_coupon_holders = [];
        foreach ($coupon_users as $coupon_holder_list) {
            $form_coupon_holders[$coupon_holder_list['coupon_id']] = $coupon_holder_list['num'];
        }

        $event_ticket_users = $this->db->select("event_ticket_id,count(*) as num")->from("tbl_event_ticket_holder")->group_by("event_ticket_id")->get()->result_array();
        $form_event_ticket_holders = [];
        foreach ($event_ticket_users as $event_ticket_holder) {
            $form_event_ticket_holders[$event_ticket_holder['event_ticket_id']] = $event_ticket_holder['num'];
        }

        $merchants = $this->db->select("id,merchant_id")->from("tbl_merchants")->where("deleted_at", 0)->get()->result_array();
        $simplified = [];
        foreach ($merchants as $index => $merchant) {
            $membership_range = $this->db->select("*")->from("tbl_virtual_passes")->where("user_id", $merchant['id'])->where("display", 1)->get()->result_array();
            $simplified[$index]["users"] = 0;
            foreach ($membership_range as $r) {
                if (isset($form_membership_users[$r['id']])) {
                    $simplified[$index]["users"] += $form_membership_users[$r['id']];
                }
            }
            $coupon_range = $this->db->select('*')->from("tbl_coupons")->where("user_id", $merchant['id'])->where("display", 1)->get()->result_array();
            foreach ($coupon_range as $r) {
                if (isset($form_coupon_holders[$r['id']])) {
                    $simplified[$index]['users'] += $form_coupon_holders[$r['id']];
                }
            }
            $event_ticket_range = $this->db->select("*")->from("tbl_event_ticket")->where("user_id", $merchant['id'])->where("display", 1)->get()->result_array();
            foreach ($event_ticket_range as $r) {
                if (isset($form_event_ticket_holders[$r['id']])) {
                    $simplified[$index]["users"] += $form_event_ticket_holders[$r['id']];
                }
            }
        }
        //var_dump($simplified);
        arsort($simplified);
        //echo "-----";
        //var_dump($simplified);
        $result = [];
        $order = array_keys($simplified);
        //var_dump($order);
        for ($i = 0; $i < 3; $i++) {
            if (isset($order[$i]) && isset($merchants[$order[$i]]) && isset($simplified[$order[$i]]['users'])) {
                $result[$i] = $merchants[$order[$i]];
                $result[$i]["user"] = $simplified[$order[$i]]['users'];
            } else {
                $result[$i] = [
                    "merchant_id" => "null"
                ];
                $result[$i]["user"] = 0;
            }
        }



        //var_dump($result);
        return $result;
    }
}
