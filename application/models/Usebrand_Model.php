<?php
class Usebrand_Model extends CI_Model{
        
        function insert($arr)
        {
                


                $this->db->insert('_use_brand', $arr);
                return $this->db->insert_id();
                
        }
        
        function update($id, $arr)
        {
        
                $this->db->where('id', $id);
                $this->db->update('_use_brand', $arr);
        
        }
        
        function get()
        {       

                    // $this->db->select('landing_content_multistylepage.id,landing_content_multistylepage.main_id,landing_main.title as main_title,landing_content_multistylepage.submenu_id,landing_submenu.title as submenu_title,landing_content_multistylepage.title,landing_content_multistylepage.partaimage1,landing_content_multistylepage.partaimage2,landing_content_multistylepage.partbtext,landing_content_multistylepage.partcimage,landing_content_multistylepage.partcpart2,landing_content_multistylepage.partdpart1,landing_content_multistylepage.partetext,landing_content_multistylepage.partftext,landing_content_multistylepage.partfimage,landing_content_multistylepage.partdimage, landing_content_multistylepage.displayorder, landing_content_multistylepage.updated,landing_main.displayorder as maindisplayorder, landing_submenu.displayorder as submenudisplayorder ');

                    $this->db->select('use_brand.id,use_brand.use_brand_image,use_brand.alt_text,use_brand.use_brand_text,use_brand.displayorder, use_brand.updated ');
                    
                    $this->db->order_by("displayorder", "asc");
                    $this->db->from('_use_brand use_brand');
                    $this->db->where('deleted', 0);
                    $query=$this->db->get();

                return $query->result();
        }
        
        function get_id($id){
                $this->db->where('id', $id);
                $query = $this->db->get('_use_brand');
                return $query->result();
        }
        
        function get_lang($lang)
        {
                
                $this->db->where('lang', $lang);
                $query = $this->db->get('_use_brand');
                return $query->result();
                
        }
        
        function delete($id)
        {   

                //unlink original image



                //end unlink original image

                $arr=array();
                $arr['deleted'] =1;
                $this->db->where('id', $id);
                $this->db->update('_use_brand', $arr);
        }
        function update_rank($id, $rank)
        {
                $data = array(
                        'displayorder' => $rank
                );

                $this->db->where('id', $id);
                $this->db->update('_use_brand', $data);

        }
        
}

?>