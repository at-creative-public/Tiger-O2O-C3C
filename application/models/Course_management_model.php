<?php
defined("BASEPATH") or exit("no Direct access allowed");

class Course_management_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function listing($param)
    {
        $fields = ['id', 'name', 'phone_no', 'email', "category", 'message', 'replied'];
        $start = $param['start'];
        $length = $param['length'];
        $order = [];
        $search = ['name'];
        $joins = [];


        $result = $this->getFromDBFilter("tbl_course_registration_record", $fields, $start, $length, $order, $search, $joins, '');

        foreach ($result as $row) {
            $data[] = [
                'id' => $row['id'],
                'name' => $row['name'],
                'phone_no' => $row['phone_no'],
                "email" => $row['email'],
                "process" => $row['process'] == "1" ? "已處理" : "尚待處理",
                "result" => $row['replied'] == "1" ? "報名成功" : "報名失敗",
                "reply" => '<a href="' . base_url("backend/'place the controller name here'/reply/" . $row['id']) . '?id=' . $row['id'] . '" class="btn btn-default reply">回覆</a>'

            ];
        }
        return $data;
    }

    public function get()
    {
        return $this->db->select("*")
            ->from("tbl_course_registration_record")
            ->get()
            ->result_array();
    }

    public function get_id($id)
    {
        return $this->db->select("*")
            ->from("tbl_course_registration_record")
            ->where('id', $id)
            ->get()
            ->result_array();
    }

    public function display_off($id)
    {
        $this->db->set("display", '0')
            ->where('id', $id)
            ->update("tbl_course_registration_record");
    }

    public function totalCount()
    {
        return $this->db->from("tbl_course_registration_record")
            ->where('display', '1')
            ->count_all_results();
    }

    public function filteredCount($param)
    {
        $start = $param['start'];
        $length = $param['length'];
        $search = [];
        $this->db->select("*");
        $this->db->from('tbl_course_registration_record');
        foreach ($param['columns'] as $column) {
            if ($column['searchable'] === "true" && !empty($column['search']['value'])) {

                $searchfield = "name";
                $search[$searchfield] = $column['search']['value'];
            }
        }


        if (!empty($search)) {
            foreach ($search as $k => $v) {
                if ($v !== '') {
                    if ($k === 'name') {
                        $this->db->like($search);
                    }
                    $this->db->like($search, 'none');
                }
            }
        }
        return count($this->db->get()->result_array());
    }

    public function getFromDBFilter($table, $fields, $start, $length, $orders, $search, $joins = [], $keywordfieldname = '')
    {

        //$db = $this->load->database($database, TRUE);
        $this->db->distinct();
        $this->db->select(implode(', ', $fields));
        $this->db->from($table);
        $this->db->where('display','1');
        //$this->db->select("tbl_contact_us_form_category.name AS category_name");
        //$this->db->join('tbl_contact_us_form_category', 'tbl_contact_us_form_category.id = category_id');


        if (!empty($search) && !empty($keywordfieldname)) {
            //            log_message('debug','here');
            foreach ($search as $k => $v) {
                if ($v !== '') {
                    if ($k === $keywordfieldname) {
                        $this->db->like($search);
                    }
                    $this->db->like($search, 'none');
                }
            }
        }

        $this->db->order_by(implode(', ', $orders));
        if ($start > -1) {
            $this->db->limit($length, $start);
        }
        return $this->db->get()->result_array();
    }

    public function add($params)
    {
        $params['process'] = "0";
        $params['result'] = "0";
        $params['display'] = "1";
        $this->db->insert("tbl_course_registration_record", $params);
    }

    public function process($id)
    {
        $this->db->set("process", '1')
            ->where("id", $id)
            ->update("tbl_course_registration_record");
    }

    
}
