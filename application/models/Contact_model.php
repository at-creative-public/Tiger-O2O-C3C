<?php

class Contact_model extends MY_Model
{
    public $tbl = 'contact';
    public $fillable = ['contact_person','contact_title','contact_position','contact_phone','contact_email'];
}
