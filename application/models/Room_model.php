<?php

use app\libraries\CommonHelper;
use app\libraries\DateHelper;
use app\libraries\Tenants;

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Room_model extends MY_Model
{
    use Tenants;
    public $short = "r";
    public $table;
    public $table_prefix = '';
    public $tbl = 'room';
    public $database = 'default';

    public function __construct()
    {
        parent::__construct();
        $this->database = 'default';
        // $this->database = $this->getTenantNo();
        // $this->tbl = 'room';
        $this->table = $this->tbl . ' ' . $this->short;
    }
    public function listing_selection()
    {
        $this->tenant();
        return $this->all([$this->short . '.status' => 1]);
    }
    public function listing_in_datatables()
    {
        $this->tenant();
        $db = $this->load->database($this->database, TRUE);
        $db->from($this->table_prefix . $this->tbl);
        $db->order_by('id', 'DESC');
        $result = $db->get()->result_array();
        $data = [];
        foreach ($result as $row) {
            $data[] = [
                'id' => trim($row['id']),
                'name' => trim($row['name']),
                'capacity' => trim($row['capacity']),
                'create_time' => date("Y-m-d H:i:s", trim($row['create_time'])),
                'update_time' => date("Y-m-d H:i:s", trim($row['update_time'])),
                'details' => '<button id="details-' . trim($row['id']) . '">詳情</button>',
                'status' => $this->status[$row['status']],
            ];
        }
        return $data;
    }
    public function listing($param)
    {
        $this->tenant();
        $fields = [$this->short . '.id', $this->short . '.name', $this->short . '.status', $this->short . '.capacity', $this->short . '.create_time', $this->short . '.update_time'];
        $orders = [];
        foreach ($param['order'] as $order) {
            $colIdx = $order['column'];
            $colName = $param['columns'][$colIdx]['data'];
            $dir = strtoupper($order['dir']);
            $orders[] = $this->short . "." . $colName . '  ' . $dir;
        }
        $start = $param['start'];
        $length = $param['length'];
        $joins = [];
        //        $joins = ['product_price pp' => $this->short . '.id=pp.product_id'];
        $searchVal = trim($param['search']['value']);
        $search = [];
        if (trim($searchVal) !== "") {
            foreach ($param['columns'] as $column) {
                if ($column['searchable'] === "true") {
                    $search[$this->short . "." . $column['data']] = $searchVal;
                }
            }
        }
        $result = $this->getFromDB($this->database, $this->table, $fields, $start, $length, $orders, $search, $joins);
        $data = [];
        foreach ($result as $row) {
            $data[] = [
                'id' => trim($row['id']),
                'name' => trim($row['name']),
                'capacity' => trim($row['capacity']),
                'create_time' => date("Y-m-d H:i:s", trim($row['create_time'])),
                'update_time' => date("Y-m-d H:i:s", trim($row['update_time'])),
                'details' => '<button id="details-' . trim($row['id']) . '">詳情</button>',
                'status' => $this->status[$row['status']],
                'delete' => '<input type="checkbox" class="checkbox-delete" id="' . trim($row['id']) . '" />',
            ];
        }
        return $data;
    }
    public function totalCount()
    {
        $db = $this->load->database($this->database, TRUE);
        $db->from($this->table_prefix . $this->table);
        if ($this->database == 'default') {
            $db->where(['tenant_no' => $this->session->userdata('tenant_no')]);
        }
        return $db->count_all_results();
    }
    public function filteredCount($param)
    {
        $db = $this->load->database($this->database, TRUE);
        $searchVal = isset($param['search']['value']) ? trim($param['search']['value']) : '';
        $search = [];
        if (trim($searchVal) !== "") {
            foreach ($param['columns'] as $column) {
                if ($column['searchable'] === "true") {
                    $search[$this->short . "." . $column['data']] = $searchVal;
                }
            }
        }
        if ($this->database == 'default') {
            $db->where(['tenant_no' => $this->session->userdata('tenant_no')]);
        }
        $db->or_like($search);
        $db->from($this->table_prefix . $this->table);
        //        $db->join($this->table_prefix . 'product_price pp', $this->short . '.id=pp.product_id');
        return $db->count_all_results();
    }
    public function detail($id)
    {
        $this->tenant();
        return $this->getOne($this->database, $this->tbl, [], $id);
    }
    public function detail_name($name, $idxname = 'name')
    {
        $this->tenant();
        return $this->db->from($this->table_prefix . $this->tbl)->where($idxname, $name)->get()->row_array();
    }
    public function all($where = [])
    {
        $fields = [$this->short . '.*'];
        return $this->getAll($this->database, $this->table, $fields, $where);
    }
    public function one($room_id)
    {
        $fields = [$this->short . '.*'];
        return $this->getOne($this->database, $this->table, $fields, $room_id);
    }
    public function save($param, $id)
    {
        $this->saveDB($this->database, $this->tbl, $param, $id);
    }
    public function insert($param)
    {
        if ($this->database === "") {
            return false;
        }
        $db = $this->load->database($this->database, TRUE);

        // $batch ? $db->insert_batch($this->table_prefix . $this->tbl, $param) : $db->insert($this->table_prefix . $this->tbl, $param);
        $db->insert($this->table_prefix . $this->tbl, $param);
        return $db->insert_id();
        // return $batch ? [] : $db->insert_id();
    }
    public function deleteMultiple($ids)
    {
        $this->deleteDB($this->database, $this->tbl, $ids, true);
    }
    public function deleteAll()
    {
        $this->deleteAllDB($this->database, $this->tbl);
    }
    public function getAllIds()
    {
        $ids = [];
        $all = $this->all();
        foreach ($all as $k => $v) {
            $ids[] = $v['id'];
        }
        return $ids;
    }
}
