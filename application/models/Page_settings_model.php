<?php

defined('BASEPATH') or exit('No direct script access allowed');



class Page_settings_model extends CI_Model

{



    public function __construct()

    {

        parent::__construct();

    }



    public function get_page_settings($main_id, $submenu_id = 0)

    {

        if ($submenu_id == 0) {

            $this->db->where([

                'main_id' => $main_id,

                'submenu_id' => "0"

            ]);

        } else {

            $this->db->where([

                'submenu_id' => $submenu_id,

            ]);
        }

        $query = $this->db->get('_page_settings');
        
        return $query->row_array();

    }



    public function update_page_settings($params, $main_id, $sub_id = 0)

    {



        $data = [];

        foreach (['navigation', 'banner', 'sidebar', 'breadcrumb', 'footer', 'single_page'] as $item) {

            $data['use_' . $item] = array_key_exists('use_' . $item, $params) ? $params['use_' . $item] : 0;

        }

        foreach (['banner_desktop', 'banner_mobile', 'background_image'] as $item) {

            if (array_key_exists($item, $params)) {



                if ($params[$item] !== '') {

                    $data[$item] = $params[$item];

                }

            }

        }

        $data['main_id'] = $main_id;

        $data["submenu_id"] = $sub_id;

        if(isset($params["mobile_banner_alttext"]) && !empty($params["mobile_banner_alttext"])){
            $data["mobile_banner_alttext"]  = $params["mobile_banner_alttext"];
        }

        if(isset($params["desktop_banner_alttext"]) && !empty($params["desktop_banner_alttext"])){
            $data["desktop_banner_alttext"]  = $params["desktop_banner_alttext"];
        }

        

        $result = $this->db->select("*")

            ->from("_page_settings")

            ->where("main_id", $main_id)

            ->where('submenu_id', $sub_id)

            ->get()

            ->row_array();
           




        if ($result != null) {

            $this->db->where('id', $result['id']);

            $this->db->update("_page_settings", $data);

        } else {

            $this->db->insert('_page_settings', $data);

        }

    }

}

