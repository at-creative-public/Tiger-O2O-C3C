<?php

class Virtual_passes_model extends CI_Model
{

    public function get_id($id)
    {
        return $this->db->select("*")->from("tbl_virtual_passes")->where("id", $id)->where("display", 1)->get()->row_array();
    }

    public function get_card($id)
    {
        return $this->db->select("*")->from("tbl_virtual_passes")->where("id", $id)->get()->row_array();
    }

    public function get_cards_by_id_all($id)
    {
        return $this->db->select("*")->from("tbl_virtual_passes")->where("user_id", $id)->where("display", 1)->get()->result_array();
    }

    public function get_cards_by_id($id)
    {
        return $this->db->select("*")->from("tbl_virtual_passes")->where("user_id", $id)->where("type", 0)->where("display", 1)->get()->result_array();
    }

    public function get_form_config($id)
    {
        return $this->db->select("*")->from("tbl_virtual_pass_e_form_config")->where("id", $id)->get()->row_array();
    }

    public function update_pass($request, $id)
    {

        $this->db->where("id", $id)->update("tbl_virtual_passes", $request);
    }

    public function get_e_form($id)
    {
        return $this->db->select("*")->from("tbl_virtual_pass_e_form_config")->where("id", $id)->get()->row_array();
    }

    public function insert_e_form($request)
    {
        $this->db->insert("tbl_virtual_pass_e_form_config", $request);
    }

    public function update_e_form($request, $id)
    {
        $this->db->where("id", $id)->update("tbl_virtual_pass_e_form_config", $request);
    }

    public function get_front_setting($id)
    {
        return $this->db->from("tbl_virtual_passes_data_config")->where("id", $id)->get()->row_array();
    }

    public function insert_front_setting($request)
    {
        $this->db->insert("tbl_virtual_passes_data_config", $request);
    }

    public function update_front_setting($request, $id)
    {
        $this->db->where("id", $id)->update("tbl_virtual_passes_data_config", $request);
    }

    public function get_location_setting($id)
    {
        return $this->db->select("*")->from("tbl_virtual_passes_locations")->where("id", $id)->get()->row_array();
    }

    public function insert_location_setting($request)
    {
        $this->db->insert("tbl_virtual_passes_locations", $request);
    }

    public function update_location_setting($request, $id)
    {
        $this->db->where("id", $id)->update("tbl_virtual_passes_locations", $request);
    }

    public function insert_default_level($params)
    {
        $this->db->insert("tbl_virtual_passes_levels", $params);
        return $this->db->insert_id();
    }

    public function insert_level($params)
    {
        $this->db->insert("tbl_virtual_passes_levels", $params);
    }

    public function update_level($params, $id)
    {
        $this->db->where("id", $id)->update("tbl_virtual_passes_levels", $params);
    }

    public function delete_level($id)
    {
        $this->db->where("id", $id)->update("tbl_virtual_passes_levels", ['display' => 0]);
    }

    public function get_default_level($id)
    {
        return $this->db->select("*")->from("tbl_virtual_passes_levels")->where("pass_id", $id)->where("default_value", "1")->get()->row_array();
    }

    public function get_level_setting($id)
    {
        return $this->db->select('*')->from("tbl_virtual_passes_levels")->where("pass_id", $id)->where("default_value", 0)->where("display", 1)->order_by('id')->get()->result_array();
    }

    public function get_full_level_setting($id)
    {
        return $this->db->select("*")->from("tbl_virtual_passes_levels")->where("pass_id", $id)->where("display", 1)->get()->result_array();
    }

    public function get_user_level($id)
    {
        $result = $this->db->select("*")->from("tbl_virtual_passes_levels")->where("id", $id)->get()->row_array();
        return $result["value"];
    }

    public function get_all_level_setting($id)
    {
        $result  = $this->db->select("*")->from("tbl_virtual_passes_levels")->where("pass_id", $id)->where("display", "1")->get()->result_array();
        $data = [];
        foreach ($result as $index => $res) {
            $data[$res['id']] = $res;
        }
        return $data;
    }

    public function get_range($id)
    {
        $result =  $this->db->select("id")->from("tbl_virtual_passes")->where('user_id', $id)->get()->result_array();
        $data = [];
        foreach ($result as $res) {
            $data[] = $res["id"];
        }
        return $data;
    }

    public function check_type($id)
    {
        $result = $this->db->select("*")->from("tbl_virtual_passes")->where("id", $id)->get()->row_array();
        if ($result['type'] == 0) {
            return false;
        } else {
            return true;
        }
    }
}
