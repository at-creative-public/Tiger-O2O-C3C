<?php
    class Contact_us_item_model extends CI_Model
    {

        public function __construct()
        {
            parent::__construct();
        }

        public function listing_front()
        {
            return $this->db->from("tbl_contact_us_form_category")
                ->select("*")
                ->where("display", "1")
                ->order_by("displayorder", "ASC")
                ->get()
                ->result();
        }

        public function listing()
        {
            return $this->db->from("tbl_contact_us_form_category")
                ->select("*")
                ->order_by("displayorder", "ASC")
                ->get()
                ->result_array();
        }

        public function get_id($id)
        {
            return $this->db->select("*")
                ->from("tbl_contact_us_form_category")
                ->where("id", $id)
                ->get()
                ->row_array();
        }

        public function update($params, $id)
        {
            $this->db->set($params)
                ->where('id', $id)
                ->update("tbl_contact_us_form_category");
        }

        public function delete($id)
        {
            $this->db->where('id', $id)
                ->delete("tbl_contact_us_form_category");
        }

        public function update_rank($id, $rank)
        {
            $data = array(
                'displayorder' => $rank
            );

            $this->db->where('id', $id);
            $this->db->update('tbl_contact_us_form_category', $data);
        }
    }
