<?php

use app\libraries\CSVClass;
use app\libraries\CSVClassImportDataException;
use app\libraries\CSVClassImportErrorItem;
use app\libraries\CSVClassImportErrorItems;
use app\libraries\CsvClassImportResult;

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property Booking_model $Booking_model
 * @property Booking_timeslot_model $Booking_timeslot_model
 */
class Member_model extends MY_IndexCrudModel
{
    const GOOGLE_LOGIN = 'google';
    const FACEBOOK_LOGIN = 'facebook';
    const PHONE_LOGIN = 'phone';
    const USER_CREDENTIALS_LOGIN = 'user_credentials';

    public $tbl = 'tblmembers';
    public $username_field = 'email';
    public $disable_limit_per_page = false;
    public $last_login_at;


    public function table_view($limit, $per_page, $searchValue = "")
    {
        $this->load->library('pagination');
        $this->config->load('pagination');

        $limits = [10, 25, 50, 100];
        // Show per page
        $limit = $limit ?? $limits[0];
        // Page number
        $per_page = $per_page ?? 0;
        $total_rows = $this->table_view_count($searchValue);
        // $this->pagination->initialize($config);

        $users = $this->table_view_get([
            'limit' => $limit,
            'offset' => $per_page
        ], $searchValue);



        if ($this->disable_limit_per_page) return [
            'total_rows' => $total_rows,
            'items' => $users
        ];

        $config = $this->config->item('pagination');
        $config['base_url'] = base_url('admin_user');
        $config['total_rows'] = $total_rows;
        $this->pagination->initialize($config);


        $max_page = floor($total_rows % $limit > 0 ? $total_rows / $limit + 1 : $total_rows / $limit);
        $cur_page = $per_page + 1 > $max_page ? $max_page :  $per_page + 1;
        $next_page = ($cur_page + 1) * $limit > $total_rows ? $cur_page : $cur_page + 1;

        $prev_page = $cur_page == 1 ? 1 : $cur_page - 1;
        return [
            'total_rows' => $total_rows,
            'limit' => (int) $limit,
            'per_page' => $this->pagination->per_page,
            'per_pages' => $limits,
            'min_limit' => $limits[0],
            'max_limit' => $limits[count($limits) - 1],
            'prev_page' => $prev_page,
            'curr_page' => $cur_page,
            'next_page' => $next_page,
            'max_page' => $max_page,
            'prev_link' => '#',
            'next_link' => '#',
            'items' => $users,
        ];
    }

    protected function table_view_get_criteria($searchValue)
    {
        $this->db->from($this->table_prefix . $this->tbl);
        // $this->db->join('rbac_users_roles ur', 'ur.user_id = u.id');
        // if (count($exclude_roles) > 0) {
        //     $this->db->where_not_in('ur.role_id', $exclude_roles);
        // }
        // $this->db->where_not_in('ur.role_id', [1]);
        // $this->db->where('u.deleted_at IS NULL', null, false);

        if (trim($searchValue)) {
            $performSearchInFields = ['display_name', 'phone', 'email'];
            // $performSearchInFields = ['display_name', 'phone', 'email', 'birth_date', 'gender', 'remarks'];

            $concatSegment = implode(', " ",', $performSearchInFields);
            $concatWhere = sprintf('CONCAT(%s) LIKE ', $concatSegment) . '"%' . $searchValue . '%"';
            $this->db->where($concatWhere, null, false);
        }

        $this->db->where('deleted_at IS NULL', null, false);
    }

    /*
     * Get all users
     */
    public function table_view_get($params = array(), $searchValue)
    {
        $this->table_view_get_criteria($searchValue);

        if (isset($params) && !empty($params) && !$this->disable_limit_per_page) {
            $this->db->limit($params['limit'], $params['offset'] > 0 ? $params['limit'] * $params['offset'] : $params['offset']);
        }

        $res = $this->db
            ->order_by('created_at', 'desc')
            ->get()->result_array();
        //var_dump($this->db->last_query()); exit;
        // log_message('debug', $this->db->last_query());
        return $res;
    }

    /*
     * Get all users count
     */
    public function table_view_count($search)
    {
        $this->table_view_get_criteria($search);

        $count = $this->db->count_all_results();
        $this->db->reset_query();
        return $count;
    }

    public function import_members_from_csv($file, $required_attributes, $required_label_attributes)
    {
        $e = new CSVClass();
        $batches = [];
        $batchesWithCu = [];
        $errors = new CSVClassImportErrorItems();
        // Callable recognize $this as lexical variable, i.e. shall not be included in use statement
        $callable = function ($row) use (&$batches, &$batchesWithCu, $errors, $required_attributes, $required_label_attributes) {
            // echo json_encode($row);
            try {
                $params = [];
                // $display_params = [];
                for ($index = 0; $index < count($row); $index++) {
                    $params[$required_attributes[$index]] = $row[$index];
                    // $display_params[$required_label_attributes[$index]] = $row[$index];
                }

                //var_dump($params); exit;
                $this->validate_required_attributes($params, $required_attributes, $required_label_attributes);
                // $this->validate_code($params);
                $this->validate_phone($params);
                $this->validate_email($params);
                array_push($batches, $params);

                $params = array_merge($params, [
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' =>  $this->session->userdata('userid') ?? 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>  $this->session->userdata('userid') ?? 0,
                ]);
                array_push($batchesWithCu, $params);
            } catch (Exception $ex) {
                $errors->add(new CSVClassImportErrorItem($params, $ex->getMessage()));
            }
        };

        try {
            $e->import($file, true, $required_label_attributes, 0, ',',  $callable);
        } catch (CSVClassImportDataException $cSVClassImportDataException) {
            throw $cSVClassImportDataException;
        }

        if ($errors->has_error()) {
            return new CsvClassImportResult($errors, []);
        } else if (!empty($batches)) {
            array_walk($batchesWithCu, function (&$item) {
                if ($item['gender'] == "男") {
                    $item['gender'] = "0";
                } else if ($item['gender'] == "女") {
                    $item['gender'] = "1";
                }
            });

            $this->create_batch($batchesWithCu);
            return new CsvClassImportResult($errors, $batches);
        }
    }

    public function create_batch($params)
    {
        $this->db = $this->load->database($this->database, true);
        $this->_table = "tblmembers";
        return $this->db->insert_batch($this->_table, $params);
    }

    public function getMemberDonation($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_payment_record');
        $this->db->where("user_id", $id);
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function export_members_to_csv()
    {
        $this->disable_limit_per_page = true;
        $res = $this->table_view(0, 0);
        $filename = lang("pages.member.import.export_csv_filename", ['datetime' => date("YmdHis")]);
        $e = new CSVClass();
        array_walk($res['items'], function (&$item) {
            if ($item['gender'] === "0") {
                $item['gender'] = "男";
            } else if ($item['gender'] === "1") {
                $item['gender'] = "女";
            }
        });

        return $e->export(
            [
                'id' => ['omitted' => true],
                // 'first_name' => ['value' => lang("pages.member.form.first_name")],
                // 'last_name' => ['value' => lang("pages.member.form.last_name")],
                'display_name' => ['value' => lang("pages.member.form.display_name")],
                'gender' => ['value' => lang("pages.member.form.gender")],
                'phone' => ['value' => lang("pages.member.form.phone")],
                'email' => ['value' => lang("pages.member.form.email")],
                // 'password' => ['value' => lang("pages.member.form.password")],
            ],

            $res['items'],
            $filename
        );
    }

    public function send_credentials_mail($params)
    {
        $first_name = $params['first_name'] ?? '';
        $last_name = $params['last_name'] ?? '';

        $email = $params["email"];
        // if ($this->is_test_email($email)) return;
        $title = "成功登記帳戶";
        $display_name = $params['display_name'] ?? '';

        ob_start();
        error_reporting(0);
        $client = new \app\libraries\SendgridClient();
        $name = $first_name . $last_name != "" ? $first_name . ' ' . $last_name : $display_name;
        $mail = $client->create();
        $mail->setFrom("no-reply@at-appmaker.com", "O2O C3C");
        $mail->setSubject($title);
        $mail->addTo($params['email'], $name);
        // $mail->addContent("text/plain", "您已成功註冊帳戶");
        // $mail->addContent(
        //     "text/html",
        //     sprintf("成功登記帳戶<br /><br />帳戶名稱：%s<br />帳戶密碼：%s<br />", $params[$this->username_field], $params['password'])
        // );
        if (!in_array($email, ['louis@at-creative.com'])) $mail->addBcc('louis@at-creative.com');

        $doc = new Document();
        $doc->loadHTMLFile('application/libraries/email.html');
        // $doc->getElementsByTagName('preheader')->item(0)->nodeValue = $title;
        $doc->getElementsByTagName('title')->item(0)->nodeValue = 'Contact';

        $doc->getElementById('greeting')->nodeValue = "致" . $display_name . " " . (empty($params['gender']) ? '先生/小姐' : ($params['gender'] == "M" ? "先生" : "小姐"));
        $c = $doc->getElementById('content');
        $c->nodeValue = "";
        $c->appendChild($doc->createElement('p', $title));
        $c->appendChild($doc->createElement('p', ""));
        $c->appendChild($doc->createElement('p', "帳戶名稱：" . $params[$this->username_field]));
        $c->appendChild($doc->createElement('p', "帳戶密碼：" . $params['password']));
        
        $html = $doc->saveHTML();

        $mail->addContent("text/html", $html);

        $client->send($mail);

        ob_get_clean();
    }

    public function generate_code($id)
    {
        $codePrefix = 'AM' . date("Ym");
        $_digit = 3;
        $tableName = $this->table_prefix . $this->tbl;
        $sql = sprintf("UPDATE `%s` SET `code` = UCASE(" .
            "   CONCAT ('%s', " .
            "       LPAD(" .
            "           CONVERT(" .
            "               SUBSTRING(" .
            "                   IFNULL((SELECT * FROM (SELECT MAX(`code`) FROM `%s` WHERE `code` LIKE '%s%%') AS `temp`), 0)," .
            "               -%s)," . // SUBSTRING
            "           UNSIGNED INTEGER) + 1," . // CONVERT
            "       %s, 0)" . // LPAD
            "   ))" . // CONCAT and UCASE
            "WHERE `id` = %s", $tableName, $codePrefix, $tableName, $codePrefix, $_digit, $_digit, $id);
        $this->queryExecute($this->database, $sql);
    }

    public function add_member($params)
    {
        $create_params = [];
        $create_fields = [
            'code', 'display_name', 'phone', 'email', 'birth_date', 'gender', 'remarks', 'correspondence_address_1', 'correspondence_address_2',
            'password',
            'login_method', 'firebase_user_id',
            'push_message_enabled', 'fcm_token',
            'last_login_at',
        ];

        foreach ($create_fields as $field) {
            if (!empty($params[$field])) $create_params[$field] = $params[$field];
        }

        return $this->insertDB($this->database, $this->tbl, $create_params, false, [
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' =>  $this->session->userdata('userid') ?? 0,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' =>  $this->session->userdata('userid') ?? 0,
        ]);
    }

    public function get_member_task($id = null, $select_fields = [], $where = [])
    {

        $this->db->select('*');
        $this->db->from('members m');
        $this->db->join("tblmember_task_record mtask", 'mtask.user_id = m.id');
        $this->db->join("tblmember_task mk", "mk.id= mtask.task_id");
        $this->db->where("m.id", $id);
        $result = $this->db->get()->result_array();
        return $result;

        // if ($id) $where['id'] = $id;
        // $where['deleted_at'] = null;

        // return $this->getOneRow($this->database, $this->tbl, $select_fields, $where);
    }

    public function get_member($id = null, $select_fields = [], $where = [])
    {
        $this->tbl = 'members';
        if ($id) $where['id'] = $id;
        $where['deleted_at'] = null;

        return $this->getOneRow($this->database, $this->tbl, $select_fields, $where);
    }

    public function get_members($ids = null, $select_fields = [], $where = [])
    {
        $where['deleted_at'] = null;

        $res = $ids != null
            ? $this->getAllIn($this->database, $this->tbl,  $select_fields, 'id', $ids, true, $where)
            : $this->getAll($this->database, $this->Member_model->tbl, $select_fields, $where);

        return $res;
    }

    public function update_member($params, $id)
    {
        $update_params = [];

        // $update_fields = [
        //     'code', 'display_name', 'phone', 'email', 'birth_date', 'gender', 'remarks', 'correspondence_address_1', 'correspondence_address_2',
        //     'password',
        //     'login_method', 'firebase_user_id',
        //     'push_message_enabled', 'fcm_token'
        // ];

        // foreach ($update_fields as $field) {
        //     if (!empty($params[$field])) $update_params[$field] = $params[$field];
        // }

        $update_params = [
            'first_name' => $params['first_name'] ?? '',
            'last_name' => $params['last_name'] ?? '',
            'email' => $params['email'],
            'gender' => $params['gender'] ?? 'M',
            'display_name' => $params['display_name'] ?? '',
            //$this->Rbac_user_model->username_field => $params['username'],
            'phone' => $params['phone'],
        ];

        $password = $params['password'] == $params['confirm_password'] ? $params['password'] : '';
        if (!empty($password)) {
            $update_params['password'] = $this->encrypt_password($params['password']);
        }

        if ($this->last_login_at != null)
            $update_params['last_login_at'] = $this->last_login_at;
        $result = $this->saveDB($this->database, $this->tbl, $update_params, $id, 'id', [
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => $this->session->userdata('userid'),
        ]);



        return $result;
    }
    /**
     * Return a member array
     */
    public function register($params, $validate_code = false, $generate_password = false)
    {
        $this->tbl = 'members';
        if ($validate_code) $this->validate_code($params);
        $this->validate_phone($params, false);
        $this->validate_email($params);

        $params['password'] = $generate_password ? $this->generate_password(4) : $params['password'];

        $id = $this->insertDB($this->database, $this->tbl, [
            "display_name" => $params['display_name'] ?? "",
            "first_name" => $params["first_name"] ?? "",
            "last_name" => $params["last_name"] ?? "",
            "phone" => $params["phone"],
            "email" => strtolower($params["email"]),
            "age" => $params["age"] ?? "",
            "password" => $this->encrypt_password($params['password']),
            "birth_date" => $params["birth_date"] ?? null,
            "gender" => $params["gender"] ?? "M",
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' =>  $this->session->userdata('userid') ?? 0,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' =>  $this->session->userdata('userid') ?? 0,
        ], false);

        return array_merge($params, ["id" => $id, "password" => $params['password']]);

        /*if ($validate_code) $this->validate_code($params);
        $this->validate_phone($params);
        $this->validate_email($params);

        $password = $this->generate_password(4);

        $id = $this->insertDB($this->database, $this->tbl, [
            "display_name" => $params["display_name"],
            "phone" => $params["phone"],
            "email" => strtolower($params["email"]),
            "password" => $this->encrypt_password($password),
            "birth_date" => $params["birth_date"],
            "gender" => $params["gender"],
            "correspondence_address_1" => $params["correspondence_address_1"] ?? null,
            "correspondence_address_2" => $params["correspondence_address_2"] ?? null,
            "remarks" => $params["remarks"] ?? null,
            $this->username_field => strtolower($params[$this->username_field]),
        ], false, [
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' =>  $this->session->userdata('userid') ?? 0,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' =>  $this->session->userdata('userid') ?? 0,
        ]);

        return array_merge($params, ["id" => $id, "password" => $password]);*/
    }

    public function validate_required_attributes($params, $required_attributes, $required_label_attributes)
    {
        $emptyFields = [];
        foreach ($required_attributes as $attribute) {
            if (empty($params[$attribute])) {
                array_push($emptyFields, $attribute);
            }
        }
        if (!empty($emptyFields)) {
            throw new Exception(lang("pages.member.exception_empty_required_attributes"));
        }
    }


    public function validate_code($params)
    {
        if (strlen($params['code']) != 11) {
            throw new Exception(lang("form_validation_exact_length", ["field" => lang("pages.member.form.code"), "param" => 11]));
        }

        if ($this->getOneRow($this->database, $this->tbl, [], ['code' => $params['code'], 'deleted_at' => null]))
            throw new Exception(lang("pages.member.exception_duplicate_code"));
    }

    public function validate_phone($params, $validate = true)
    {
        if (strlen($params['phone']) < 8) {
            throw new Exception(lang("form_validation_min_length", ["field" => lang("pages.member.form.phone"), "param" => 8]));
        }
    }

    public function validate_email($params)
    {
        if (!filter_var($params['email'], FILTER_VALIDATE_EMAIL)) {
            throw new Exception(lang("form_validation_valid_email", ["field" => lang("pages.member.form.email")]));
        }

        if ($this->getOneRow($this->database, $this->tbl, [], ['email' => strtolower($params['email']), 'deleted_at' => null]))
            throw new Exception(lang("pages.member.exception_duplicate_email"));
    }

    public function validate($params)
    {
        $login_method = $params['login_method'] ?? self::USER_CREDENTIALS_LOGIN;
        $where = [];
        if ($login_method == self::USER_CREDENTIALS_LOGIN) {
            $username = $this->security->xss_clean($params[$this->username_field]);
            $password = $this->security->xss_clean($params['password']);
            $encrypted_password = $this->encrypt_password($password);
            $where = [$this->username_field => $username, 'password' => $encrypted_password];
            unset($params['password']);
        } else if (in_array($login_method, [self::GOOGLE_LOGIN, self::FACEBOOK_LOGIN, self::PHONE_LOGIN])) {
            // if (!empty($params['email'])) $where['email'] = $params['email'];
            // if (!empty($params['phone'])) $where['phone'] = $params['phone'];
            $where['firebase_user_id'] = $params['firebase_user_id'];
        }

        $where['login_method'] = $login_method;
        $where['deleted_at'] = null;
        $res = $this->getOneRow($this->database, $this->tbl, [], $where);
        log_message("debug", $this->db->last_query());
        // Cannot find the user
        if (!$res) {
            if ($login_method == self::USER_CREDENTIALS_LOGIN) {
                return ['create_record' => false];
            } else if (in_array($login_method, [self::GOOGLE_LOGIN, self::FACEBOOK_LOGIN, self::PHONE_LOGIN])) {
                return ['create_record' => true];
            }
        }
        // $this->connectTenantByUsername($result['username']);

        $this->update_entity(['last_login_at' => date("Y-m-d H:i:s")], ['id' => $res['id']]);
        unset($res['password']);
        return $res;
    }


    public function member_info($id)
    {
        if (!empty($id)) {
            return $this->db->select('*')
                ->from('members')
                ->where("id", $id)
                ->get()
                ->row_array();
        }
    }

    public function task_info($task_id)
    {
        if (!empty($task_id)) {
            $result = $this->db->select('*')
                ->from('member_task_record mkr')
                ->join('member_task mk', 'mkr.task_id = mk.id')
                ->join('members m', 'm.id = mkr.user_id')
                ->where('mk.id', $task_id)
                ->get()
                ->row_array();

            $task_member_ids = $result["task_members_ids"];
            $task_member_ids_arr = explode(",", $task_member_ids);

            $task_member_ids_result = $this->db->select('first_name,last_name,id')
                ->from('members m')
                ->where_in("m.id", $task_member_ids_arr)
                ->get()
                ->result_array();

            if (!empty($task_member_ids_arr)) {
                $result["task_members_ids"] = $task_member_ids_result;
            }

            return $result;
        }
    }

    public function member_booking_record($member_id)
    {
        $this->load->model('Booking_model');
        $this->load->model('Booking_timeslot_model');
        $this->load->model('Booking_consultation_model');
        $this->load->model('Rbac_user_model');

        if (!empty($member_id)) {
            $this->db
                ->from($this->table_prefix . $this->tbl . " " . $this->short)
                ->join($this->Booking_model->table_prefix . $this->Booking_model->tbl . " " . $this->Booking_model->short, $this->short . '.id = ' . $this->Booking_model->short . '.member_id')
                ->join($this->Booking_timeslot_model->table_prefix . $this->Booking_timeslot_model->tbl . " " . $this->Booking_timeslot_model->short, $this->Booking_model->short . '.timeslot_id = ' . $this->Booking_timeslot_model->short . '.id');
                // ->join($this->Booking_consultation_model->table_prefix . $this->Booking_consultation_model->tbl . " " . $this->Booking_consultation_model->short, $this->Booking_consultation_model->short . '.booking_id = ' . $this->Booking_model->short . '.id')
                // ->join($this->Rbac_user_model->table_prefix . $this->Rbac_user_model->tbl . " " . $this->Rbac_user_model->short, $this->Booking_consultation_model->short . '.doctor_id = ' . $this->Rbac_user_model->short . '.id');
            // ->join($this->Rbac_user_model->table_prefix . $this->Rbac_user_model->tbl . " " . $this->Rbac_user_model->short, $this->Booking_consultation_model->short . '.doctor_id = ' . $this->Rbac_user_model->short . '.id', 'LEFT')

            if (!empty($_SESSION['is_doctor']) && $_SESSION['is_doctor']) {
                $this->db->where($this->Rbac_user_model->short . '.id = ' . $member_id);
            } else {
                $this->db->where($this->short . '.id = ' . $member_id);
            }

            $result = $this->db->select(implode(',', [
                $this->short . '.*',
                $this->short . '.display_name AS patient_name',
                $this->Booking_timeslot_model->short . '.*',
                $this->Booking_model->short . '.id AS booking_id',
                // $this->Rbac_user_model->short . '.display_name AS doctor_name',
                // $this->Booking_consultation_model->short . '.id AS booking_consultation_id',
                // $this->Booking_consultation_model->short . '.booking_id',
                // $this->Booking_consultation_model->short . '.paid',
                // $this->Booking_consultation_model->short . '.consultation_end_at'
            ]))
                ->order_by($this->Booking_model->short . '.booking_timestamp', 'desc')
                ->get()
                ->result_array();

            return $result;
        }
    }

    public function member_booking_detail($member_id, $booking_id)
    {
        $this->load->model('Booking_model');
        $this->load->model('Booking_timeslot_model');
        $this->load->model('Booking_consultation_model');
        $this->load->model('Rbac_user_model');

        if (!empty($member_id)) {
            $this->db
                ->from($this->table_prefix . $this->tbl . " " . $this->short)
                ->join($this->Booking_model->table_prefix . $this->Booking_model->tbl . " " . $this->Booking_model->short, $this->short . '.id = ' . $this->Booking_model->short . '.member_id')
                ->join($this->Booking_timeslot_model->table_prefix . $this->Booking_timeslot_model->tbl . " " . $this->Booking_timeslot_model->short, $this->Booking_model->short . '.timeslot_id = ' . $this->Booking_timeslot_model->short . '.id');
                // ->join($this->Booking_consultation_model->table_prefix . $this->Booking_consultation_model->tbl . " " . $this->Booking_consultation_model->short, $this->Booking_consultation_model->short . '.booking_id = ' . $this->Booking_model->short . '.id')
                // ->join($this->Rbac_user_model->table_prefix . $this->Rbac_user_model->tbl . " " . $this->Rbac_user_model->short, $this->Booking_consultation_model->short . '.doctor_id = ' . $this->Rbac_user_model->short . '.id');
            // ->join($this->Rbac_user_model->table_prefix . $this->Rbac_user_model->tbl . " " . $this->Rbac_user_model->short, $this->Booking_consultation_model->short . '.doctor_id = ' . $this->Rbac_user_model->short . '.id', 'LEFT')

            if (!empty($_SESSION['is_doctor']) && $_SESSION['is_doctor']) {
                $this->db->where($this->Rbac_user_model->short . '.id = ' . $member_id);
            } else {
                $this->db->where($this->short . '.id = ' . $member_id);
            }

            $result = $this->db->where($this->Booking_model->short . '.id = ' . $booking_id)
                // ->where($this->)
                ->select(implode(',', [
                    $this->short . '.*',
                    $this->short . '.display_name AS patient_name',
                    $this->Booking_timeslot_model->short . '.*',
                    // $this->Rbac_user_model->short . '.display_name AS doctor_name',
                    // $this->Booking_consultation_model->short . '.id AS booking_consultation_id',
                    // $this->Booking_consultation_model->short . '.consultation_end_at'
                ]))
                ->get()
                ->row_array();

            return $result;
        }
    }

    public function member_prescript_detail($member_id, $consult_id)
    {
        $this->load->model('Booking_model');
        $this->load->model('Booking_timeslot_model');
        $this->load->model('Booking_consultation_model');
        $this->load->model('Rbac_user_model');

        if (!empty($member_id)) {
            $this->db
                ->from($this->table_prefix . $this->tbl . " " . $this->short)
                ->join($this->Booking_model->table_prefix . $this->Booking_model->tbl . " " . $this->Booking_model->short, $this->short . '.id = ' . $this->Booking_model->short . '.member_id')
                ->join($this->Booking_timeslot_model->table_prefix . $this->Booking_timeslot_model->tbl . " " . $this->Booking_timeslot_model->short, $this->Booking_model->short . '.timeslot_id = ' . $this->Booking_timeslot_model->short . '.id')
                ->join($this->Booking_consultation_model->table_prefix . $this->Booking_consultation_model->tbl . " " . $this->Booking_consultation_model->short, $this->Booking_consultation_model->short . '.booking_id = ' . $this->Booking_model->short . '.id')
                ->join($this->Rbac_user_model->table_prefix . $this->Rbac_user_model->tbl . " " . $this->Rbac_user_model->short, $this->Booking_consultation_model->short . '.doctor_id = ' . $this->Rbac_user_model->short . '.id');
            // ->join($this->Rbac_user_model->table_prefix . $this->Rbac_user_model->tbl . " " . $this->Rbac_user_model->short, $this->Booking_consultation_model->short . '.doctor_id = ' . $this->Rbac_user_model->short . '.id', 'LEFT')

            $result = $this->db->where($this->Booking_consultation_model->short . '.id = ' . $consult_id)
                // ->where($this->)
                ->select(implode(',', [
                    $this->short . '.*',
                    $this->short . '.display_name AS patient_name',
                    $this->Booking_timeslot_model->short . '.*',
                    $this->Rbac_user_model->short . '.display_name AS doctor_name',
                    $this->Booking_consultation_model->short . '.consultation_end_at',
                    $this->Booking_consultation_model->short . '.consultation_report',
                    $this->Booking_consultation_model->short . '.consultation_prescription'
                ]))
                ->get()
                ->row_array();

            return $result;
        }
    }

    public function member_task_info($id)
    {

        if (!empty($id)) {
            $result = $this->db->select('*')
                ->from('member_task_record mkr')
                ->join('member_task mk', 'mkr.task_id = mk.id')
                ->join('members m', 'm.id = mkr.user_id')
                ->where('m.id', $id)
                ->get()
                ->result_array();


            return $result;
        }
    }

    public function member_donation_info($id)
    {
        if (!empty($id)) {
            return $this->db->select('*')
                ->from('tbl_payment_record')
                ->join('tblmembers m', 'm.id = tbl_payment_record.user_id')
                ->where('m.id', $id)
                ->get()
                ->result_array();
        }
    }


    function member_update($params)
    {
        if (!empty($params["id"])) {
            if (!empty($params['password'])) $params['password'] = $this->encrypt_password($params['password']);

            $this->db->where('id', $params['id']);
            return $this->db->update('tblmembers', $params);
        }
    }

    public function save($params = [])
    {
        return  $this->db->insert('members', $params);
    }

    public function checkLogin($params = [])
    {
        $fields = array_keys($params);
        $result = [];
        if (in_array("name", $fields)) {
            $result = $this->db->select("*")
                ->where("first_name", $params['name'])
                ->where("password", $params['password'])
                ->from("tblmembers")
                ->limit(1)
                ->get()
                ->row_array();
        } else if (in_array("email", $fields)) {
            $result = $this->db->select("*")
                ->where("email", $params['email'])
                ->where("password", $params['password'])
                ->from("tblmembers")
                ->limit(1)
                ->get()
                ->row_array();
        }

        return $result;
    }



    public function getPassword($params = [])
    {
        $fields = array_keys($params);

        $result = [];
        if (in_array("name", $fields)) {
            $result = $this->db->select("*")
                ->where("display_name", $params['name'])
                // ->where("first_name", $params['name'])
                ->from("tblmembers")
                ->limit(1)
                ->get()
                ->row_array();
        } else if (in_array("email", $fields)) {
            $result = $this->db->select("*")
                ->where("email", $params['email'])
                ->from("tblmembers")
                ->limit(1)
                ->get()
                ->row_array();
        }

        //$email  = $result['email'];
        //send email function here
        return $result;
    }

    public function generate_token($id)
    {
        $token =  md5(time() . '' . sha1(time()));
        $this->db->set("token", $token)
            ->where("id", $id)
            ->update('tblmembers');
        return $token;
    }

    public function get_id($id)
    {
        return  $this->db->select("*")
            ->from("tblmembers")
            ->where('id', $id)
            ->get()
            ->result_array();
    }

    public function get_token($token)
    {
        return $this->db->select("*")
            ->from("tblmembers")
            ->where("token", $token)
            ->get()
            ->row_array();
    }

    public function update_password($id, $password)
    {
        $this->db->where('id', $id)
            ->set("password", $password)
            ->update("tblmembers");
    }

    public function disable_token($token)
    {
        $this->db->set('token', null)
            ->where("token", $token)
            ->update("tblmembers");
    }
}
