<?php
class Blog_list_Model extends CI_Model{
        
        function insert($arr)
        {
                
                $this->db->insert('blog_list', $arr);
                return $this->db->insert_id();
                
        }
        
        function update($id, $arr)
        {
        
                $this->db->where('id', $id);
                $this->db->update('blog_list', $arr);
        
        }
        
        function get()
        {       

                    $this->db->select('bl.id,bl.cover_img_link,bl.code,bl.title, bl.date, bl.description, bl.content, bl.status, bl.display, bl.display_order, bl.update_time');
                    
                   
                    
                    $this->db->from('_blog_list bl');
                    $this->db->where('is_deleted', 0);
                     $this->db->order_by("bl.date desc");
                    $query=$this->db->get();
                    //echo $this->db->last_query();
                return $query->result();
        }
        
        function get_id($id){
                $this->db->where('id', $id);
                $query = $this->db->get('_blog_list');
                return $query->result();
                // $row = $query->row();
                // return $row;
        }
        
        function get_lang($lang)
        {
                
                $this->db->where('lang', $lang);
                $query = $this->db->get('_blog_list');
                return $query->result();
                
        }
        
        function delete($id)
        {
                $arr=array();
                $arr['is_deleted'] =1;
                $this->db->where('id', $id);
                $this->db->update('_blog_list', $arr);
        }
    public function getView($data) {

        //ajax blog load content


        $field = 'bl.id,bl.cover_img_link,bl.title,bl.date';
        $this->db->select($field)->from('_blog_list bl')->order_by('bl.display_order DESC, bl.create_time DESC, bl.id DESC');
        $this->db->where(['bl.status != ' => 0, 'bl.display' => 1, 'bl.is_deleted' => 0]);

        if (isset($data['tag_id']) && !empty($data['tag_id'])) {
            $this->db->join('_blog_tag_hub bth', 'bth.blog_id = bl.id');
            $this->db->where(['bth.tag_id' => $data['tag_id']]);
        }
//        $this->db->join('tag_list', 'tag_list.id = blog_tag_hub.tag_id');
//        $this->db->where(['tag_list.status' => 1, 'tag_list.display' => 1]);

        $page = isset($data['page']) && !empty($data['page']) ? $data['page'] - 1 : 0;
        $pages = 0;
        $per = $data['per'];
        if (is_numeric($page)) {
            $page = $page < 0 ? 0 : $page;
            $pages = $page * $per;
        }

//        $sql = $this->db->get_compiled_select(false); echo $sql; 
        //獲得
        $count = $this->db->count_all_results('', FALSE);
        $pages = $pages > $count ? $count - $per : $pages;
        $query = $this->db->limit($per, $pages)->get(); //limit(數量,起點)
        $result = [];
        foreach ($query->result_array() as $row) {
            $this->db->select('btl.id,btl.name,btl.color')->from('_blog_tag_list btl')
                    ->join('_blog_tag_hub bth', 'bth.tag_id = btl.id')
                    ->where(['bth.blog_id' => $row['id']]);
            $this->db->where(['btl.status' => 1, 'btl.deleted' => 0]);
            $this->db->where(['bth.status' => 1]);
            $tags = $this->db->get();
            $row['tag'] = $tags->result_array();

            $result[] = $row;
        }

        return [$result, $count];
    }
    function countNumOfBlog(){
            $this->db->select('id');
            $this->db->from('_blog_list');
            $this->db->where('is_deleted', 0);
            $query=$this->db->get();
            $num=$query->num_rows();

            return $num;
    }
        
}

?>