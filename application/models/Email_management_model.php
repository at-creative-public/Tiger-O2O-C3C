<?php

class Email_management_model extends CI_Model
{

    public function __construct()
    {
    }

    public function add_email_record($merchant_id, $timestamp, $type, $customer_type, $customer_id)
    {
        $this->db->insert("tbl_merchant_email_record", [
            "merchant_id" => $merchant_id,
            "type" => $type,
            "customer_type" => $customer_type,
            "customer_id" => $customer_id,
            "sent_date" => $timestamp
        ]);
        $this->update_merchant_email_sta($merchant_id);
    }



    public function get_merchant_sent_email_sta($merchant_id, $month, $year)
    {
        $start_date = strtotime("01-" . $month . "-" . $year . " 00:00:00");
        $end_date = strtotime("01-" . $month . "-" . $year . " 00:00:00" . "+1month");
        $result = $this->db->select("COUNT(*) as email_total")
            ->from("tbl_merchant_email_record")
            ->where("merchant_id", $merchant_id)
            ->where("sent_date >=", $start_date)
            ->where("sent_date <", $end_date)
            ->get()
            ->row_array();
        return $result;
    }

    public function check_exist_record($merchant_id, $month, $year)
    {
        $result = $this->db->select("*")
            ->from("tbl_merchant_email")
            ->where("year", $year)
            ->where("month", $month)
            ->get()
            ->row_array();
        if ($result === null) {
            $result = [
                'merchant_id' => $merchant_id,
                "month" => $month,
                "year" =>  $year,
                "sum" => 0
            ];
            $this->db->insert("tbl_merchant_email", $result);
            $result['id'] = $this->db->insert_id();
        }
        return $result;
    }

    public function update_merchant_email_sta($merchant_id)
    {
        $month = date("m", time());
        $year = date("Y", time());
        $record = $this->check_exist_record($merchant_id, $month, $year);

        $sta = $this->get_merchant_sent_email_sta($merchant_id, $month, $year);
        $this->db->update(
            "tbl_merchant_email",
            ["sum" => $sta['email_total']],
            ["id" => $record['id']]
        );
    }

    public function get_record($merchant_id, $month, $year)
    {
        $result = $this->db->select("*")
            ->from("tbl_merchant_email")
            ->where("merchant_id", $merchant_id)
            ->where("month", $month)
            ->where("year", $year)
            ->get()
            ->row_array();
        if ($result === null) {
            return [
                "merchant_id" => $merchant_id,
                "month" => $month,
                "year" => $year,
                "sum" => 0
            ];
        }
        return $result;
    }
}
