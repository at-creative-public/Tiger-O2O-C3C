<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Footer_management_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get()
    {
        $text = $this->db->select("*")
            ->order_by("displayorder", "asc")
            ->from('tbl_footer')
            ->get()
            ->result();
        /*  $image = $this->db->select('*')
            ->order_by("displayorder", "asc")
            ->from('footer_social_media')
            ->get()
            ->result();*/

        $data['text'] = json_decode(json_encode($text), true);
        //  $data['image'] = json_decode(json_encode($image), true);

        return $data;
    }

    public function get_front()
    {
        $text = $this->db->select("*")
            ->order_by("displayorder", "asc")
            ->from('tbl_footer')
            ->where('display', "1")
            ->get()
            ->result();
        /*   $image = $this->db->select('*')
            ->from('footer_social_media')
            ->where('display', "1")
            ->get()
            ->result();*/
        $data['text'] = json_decode(json_encode($text), true);
        //  $data['image'] = json_decode(json_encode($image), true);
        return $data;
    }

    public function get_address_front()
    {
        return $this->db->select("*")
            ->from("tbl_footer")
            ->order_by('displayorder', "ASC")
            ->where("display", 1)
            ->get()
            ->result_array();
    }

    public function get_address_id($id)
    {
        return  $this->db->select("*")
            ->from("tbl_footer")
            ->where("id", $id)
            ->get()
            ->result_array();
    }

    public function update_address($params, $id)
    {
        $this->db->where("id", $id);
        $this->db->update("tbl_footer", $params);
    }

    public function get_social_media_get_id($id)
    {
        return $this->db->select("*")
            ->from('footer_social_media')
            ->where('id', $id)
            ->get()
            ->result();
    }

    public function get_hyperlink_list_get_id($id)
    {
        return $this->db->select("*")
            ->from('footer_hyperlink_list')
            ->where('id', $id)
            ->get()
            ->result();
    }

    function update_image_rank($id, $rank)
    {
        $data = array(
            'displayorder' => $rank
        );

        $this->db->where('id', $id);
        $this->db->update('footer_Social_media', $data);
    }

    function update_text_rank($id, $rank)
    {
        $data = array(
            'displayorder' => $rank
        );

        $this->db->where('id', $id);
        $this->db->update('footer_hyperlink_list', $data);
    }



    public function delete_social($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('footer_social_media');
    }

    public function delete_hyperlink_list($id)
    {
        $this->db->where("id", $id);
        $this->db->delete('footer_hyperlink_list');
    }
}
