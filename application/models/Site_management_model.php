<?php
defined("BASEPATH") or exit("No direct access allowed");
class Site_management_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get()
    {
        return $this->db->select("*")
            ->from("tbl_social_media")
            ->order_by("displayorder", "ASC")
            ->get()
            ->result_array();
    }

    public function get_front()
    {
        return $this->db->select("*")
            ->from("tbl_social_media")
            ->order_by("displayorder", "ASC")
            ->where("display", 1)
            ->get()
            ->result_array();
    }

    public function get_id($id)
    {
        return $this->db->select("*")
            ->from("tbl_social_media")
            ->where('id', $id)
            ->get()
            ->result_array();
    }

    public function update($params, $id)
    {
        $this->db->where("id", $id);
        $this->db->update("tbl_social_media", $params);
    }

    public function delete($id)
    {
        $this->db->where("id", $id)
            ->delete("tbl_social_media", $id);
    }

    public function update_rank($id, $rank)

    {
        $data = array(
            'displayorder' => $rank
        );


        $this->db->where('id', $id);
        $this->db->update('tbl_news', $data);
    }
}
