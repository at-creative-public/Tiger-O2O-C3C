<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order_maintenance_output_model extends MY_Model
{
    public $tbl = 'orders_maintenance_output';
    public $search_value = '';
    public $filters;
    public $member_id;

    public function __construct()
    {
        parent::__construct();
    }

    public function set_params($filters = null, $start = null, $end = null)
    {
        if (!empty($filters)) $this->filters = json_decode($filters, true);
    }

    public function table_view($limit, $per_page)
    {
        $this->load->library('pagination');
        $this->config->load('pagination');

        $limits = [10, 25, 50, 100];
        // Show per page
        $limit = $limit ?? $limits[0];
        // Page number
        $per_page = $per_page ?? 0;
        $total_rows = $this->table_view_count();
        // $this->pagination->initialize($config);

        $users = $this->table_view_get([
            'limit' => $limit,
            'offset' => $per_page
        ],);

        $config = $this->config->item('pagination');
        $config['base_url'] = base_url('admin_user');
        $config['total_rows'] = $total_rows;
        $this->pagination->initialize($config);


        $max_page = floor($total_rows % $limit > 0 ? $total_rows / $limit + 1 : $total_rows / $limit);
        $cur_page = $per_page + 1 > $max_page ? $max_page :  $per_page + 1;
        $next_page = ($cur_page + 1) * $limit > $total_rows ? $cur_page : $cur_page + 1;

        $prev_page = $cur_page == 1 ? 1 : $cur_page - 1;
        return [
            'total_rows' => $total_rows,
            'limit' => (int) $limit,
            'per_page' => $this->pagination->per_page,
            'per_pages' => $limits,
            'min_limit' => $limits[0],
            'max_limit' => $limits[count($limits) - 1],
            'prev_page' => $prev_page,
            'curr_page' => $cur_page,
            'next_page' => $next_page,
            'max_page' => $max_page,
            'prev_link' => '#',
            'next_link' => '#',
            'items' => $users,
        ];
    }

    private function table_view_get_criteria()
    {
        $this->db->from($this->table_prefix . $this->tbl);
        // $this->db->join('rbac_users_roles ur', 'ur.user_id = u.id');
        // if (count($exclude_roles) > 0) {
        //     $this->db->where_not_in('ur.role_id', $exclude_roles);
        // }
        // $this->db->where_not_in('ur.role_id', [1]);
        // $this->db->where('u.deleted_at IS NULL', null, false);

        if (trim($this->search_value)) {
            $performSearchInFields = ['product_id'];

            $concatSegment = implode(', " ",', $performSearchInFields);
            $concatWhere = sprintf('CONCAT(%s) LIKE ', $concatSegment) . '"%' . $this->search_value . '%"';
            $this->db->where($concatWhere, null, false);
        }

        $this->filters;
        if (!empty($this->filters)) {
            foreach ($this->filters as $filter_where_condition_field => $filter_where_condition_field_value) {
                $this->db->where($filter_where_condition_field, $filter_where_condition_field_value, false);
            }
        }

        $this->db->where('member_id', $this->member_id);
        $this->db->where('deleted_at IS NULL', null, false);
    }

    /*
     * Get all users
     */
    public function table_view_get($params = array())
    {
        $this->table_view_get_criteria();

        if (isset($params) && !empty($params)) {
            $this->db->limit($params['limit'], $params['offset'] > 0 ? $params['limit'] * $params['offset'] : $params['offset']);
        }

        $res = $this->db
            ->order_by('main_product_id', 'desc')
            ->order_by('created_at', 'desc')
            ->get()->result_array();

        log_message('debug', $this->db->last_query());
        return $res;
    }

    /*
     * Get all users count
     */
    public function table_view_count()
    {
        $this->table_view_get_criteria();

        $count = $this->db->count_all_results();
        $this->db->reset_query();
        return $count;
    }

    public function get_stats()
    {
        $this->db->reset_query();
        $this->table_view_get_criteria();

        $res = $this->db->select("SUM(amount_paid) AS product_revenue, COUNT(*) AS sold_quantity", false)->get()->row_array();

        // log_message('debug', $this->db->last_query());
        // log_message('debug', "");
        return $res;
    }

    public function get_all_order_maintenance_output($member_id = null)
    {
        $this->db = $this->load->database($this->database, true);
        $this->db->from($this->table_prefix . $this->tbl);

        if ($member_id) {
            $this->db->where('member_id', $member_id);
        }

        return $this->db
            ->where('purchase_date <= NOW()', null, false)
            ->where('end_date > NOW()', null, false)
            ->where('deleted_at IS NULL', null, false)
            ->order_by('main_product_id', 'desc')
            ->order_by('created_at', 'desc')
            ->get()->result_array();
    }

    public function get_maintenance_listing($member_id)
    {
        $items = $this->get_all_order_maintenance_output($member_id);

        $stored_main_product_id = [];
        $res = [];

        foreach ($items as $item) {
            $maintenance = ['type' => $item['sub_product_type'], 'no_of_years' => $item['sub_product_years_of_maintenance'], 'end_date' => $item['end_date']];
            if (!in_array($item['main_product_id'], $stored_main_product_id)) {
                array_push($stored_main_product_id, $item['main_product_id']);
                $res[$item['main_product_id']] = [
                    'product_id' => $item['main_product_id'],
                    'product_name' => $item['main_product_name'],
                    'purchase_date' => $item['purchase_date'],
                    'maintenances' => [$maintenance]
                ];
            } else {
                array_push($res[$item['main_product_id']]['maintenances'], $maintenance);
            }
        }

        return array_values($res);
    }
}
