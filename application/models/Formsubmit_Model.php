<?php
class Formsubmit_Model extends CI_Model{
        
        function insertForm($arr)
        {

                $questionanswer="";
                if(!empty($arr['questionanswer'])){
                    $questionanswer="[".implode(',',$arr['questionanswer'])."]";
                }

                $data = array(
                    'inputname' => $arr['inputname'] ,
                    'inputemail' => $arr['inputemail'] ,
                    'staticmobile' => $arr['staticmobile'] ,
                    'inputgender' => $arr['inputgender'] ,
                    'inputage' => $arr['inputage'] ,
                    'questionanswer' => $questionanswer,
                );
                
                $this->db->insert('_questions_submit', $data);
                //return $this->db->insert_id();
                if($this->db->affected_rows() != 1){
                    return false;
                }
                else{
                    return true;
                }
                
        }
        
        function update($id, $arr)
        {
                $questionanswer="";
                if(!empty($arr['questionanswer'])){
                    $questionanswer="[".implode(',',$arr['questionanswer'])."]";
                }

                $data = array(
                    'inputname' => $arr['inputname'] ,
                    'inputemail' => $arr['inputemail'] ,
                    'staticmobile' => $arr['staticmobile'] ,
                    'inputgender' => $arr['inputgender'] ,
                    'inputage' => $arr['inputage'] ,
                    'questionanswer' => $questionanswer,
                );


                $this->db->where('id', $id);
                $this->db->update('_questions_submit', $data);
                if($this->db->affected_rows() != 1){
                    return false;
                }
                else{
                    return true;
                }
        
        }
        
        function get()
        {       

                $this->db->select('id,inputname,inputemail,staticmobile,inputgender,inputage,questionanswer,updated  ');
                $this->db->from('_questions_submit');
                $this->db->where('deleted', 0);
                $this->db->order_by("id", "asc");
                $query=$this->db->get();

                return $query->result();
        }
        
        function get_id($id){
                $this->db->select('id,inputname,inputemail,staticmobile,inputgender,inputage,questionanswer,updated  ');
                $this->db->where('id', $id);
                $query = $this->db->get('_questions_submit');
                return $query->result();
        }
        
        function get_lang($lang)
        {
                
                $this->db->where('lang', $lang);
                $query = $this->db->get('_questions_submit');
                return $query->result();
                
        }
        
        function delete($id)
        {
                // $this->db->where('id', $id);
                // $this->db->delete('feature');
                $arr=array();
                $arr['deleted'] =1;
                $this->db->where('id', $id);
                $this->db->update('_questions_submit', $arr);
        }

        function countNumOfFormsubmit(){
            $this->db->select('id');
            $this->db->from('_questions_submit');
            $this->db->where('deleted', 0);
            $query=$this->db->get();
            $num=$query->num_rows();

            return $num;
        }
        
}

?>