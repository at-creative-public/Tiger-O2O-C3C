<?php
class Blog_tag_hub_Model extends CI_Model{
        
        function insert($arr)
        {




                $this->db->insert('_blog_tag_hub', $arr);
                return $this->db->insert_id();
                
        }
        
        function update($id, $arr)
        {
        
                $this->db->where('id', $id);
                $this->db->update('_blog_tag_hub', $arr);
        
        }
        
        function get()
        {       

                    $this->db->select('a.id,a.blog_id,a.tag_id,a.status, a.update_time, a.create_time');
                    
                   
                    
                    $this->db->from('_blog_tag_hub a');
                    $this->db->join('_blog_tag_list b', 'a.blog_id=b.id', 'left');
                     $this->db->order_by("b.display_order asc");
                     $this->db->order_by("b.id asc");
                    $query=$this->db->get();
                    //echo $this->db->last_query();
                return $query->result();
        }
        
        function get_id($id){
                $this->db->where('id', $id);
                $query = $this->db->get('_blog_tag_hub');
                return $query->result();
        }
        function get_blog_id($id){
                $this->db->where('blog_id', $id);
                $query = $this->db->get('_blog_tag_hub');
                return $query->result();
        }
        
        function get_lang($lang)
        {
                
                $this->db->where('lang', $lang);
                $query = $this->db->get('_blog_tag_hub');
                return $query->result();
                
        }
        
        function delete($id)
        {
                $this->db->where('id', $id);
                $this->db->delete('_blog_tag_hub');
        }
        function deleteByblogid($id)
        {
                $this->db->where('blog_id', $id);
                $this->db->delete('_blog_tag_hub');
        }
        
}

?>