<?php
class Feature_Model extends CI_Model{
        
        function insert($arr)
        {
                
                $this->db->insert('_feature', $arr);
                return $this->db->insert_id();
                
        }
        
        function update($id, $arr)
        {
        
                $this->db->where('id', $id);
                $this->db->update('_feature', $arr);
        
        }
        
        function get()
        {       

                    $this->db->select('feature.id,feature.main_content,feature.sub_content,  ');
                    $this->db->from('_feature feature');
                    $query=$this->db->get();

                return $query->result();
        }
        
        function get_id($id){
                $this->db->where('id', $id);
                $query = $this->db->get('_feature');
                return $query->result();
        }
        
        function get_lang($lang)
        {
                
                $this->db->where('lang', $lang);
                $query = $this->db->get('_feature');
                return $query->result();
                
        }
        
        function delete($id)
        {
                $this->db->where('id', $id);
                $this->db->delete('_feature');
        }
         function update_rank($id, $rank)
        {
                $data = array(
                        'displayorder' => $rank
                );

                $this->db->where('id', $id);
                $this->db->update('_feature', $data);

        }
}

?>