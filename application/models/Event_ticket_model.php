<?php

class Event_ticket_model extends CI_Model
{

    public function get_event_ticket($id)
    {
        return $this->db->select("*")->from("tbl_event_ticket")->where("id", $id)->get()->row_array();
    }

    public function get_event_tickets_all($id)
    {
        return $this->db->select("*")->from("tbl_event_ticket")->where("user_id", $id)->where("display", 1)->get()->result_array();
    }

    public function get_event_tickets($id)
    {
        return $this->db->select("*")->from("tbl_event_ticket")->where("user_id", $id)->where("event_ticket_type", 0)->where("display", 1)->get()->result_array();
    }

    public function get_range($id)
    {
        $result = $this->db->select("id")->from("tbl_event_ticket")->where("user_id", $id)->where("display", 1)->get()->result_array();
        $data = [];
        foreach ($result as $res) {
            $data[] = $res["id"];
        }
        return $data;
    }

    public function check_event_type($id)
    {
        $record = $this->db->select('*')->from("tbl_event_ticket")->where("id", $id)->get()->row_array();
        if ($record !== null) {
            if ($record['event_type'] == "public") {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function check_event_ticket_type($id)
    {
        $record = $this->db->select('*')->from("tbl_event_ticket")->where('id', $id)->get()->row_array();
        if ($record !== null) {
            if ($record['event_ticket_type'] == 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function check_duplicate_code($code)
    {
        $result = $this->db->select("*")->from("tbl_event_ticket")->where("event_code", $code)->where("event_ticket_type", 0)->get()->num_rows();
        if ($result != 0) {
            return 0;
        } else {
            return 1;
        }
    }


    public function insert_event_ticket($params)
    {
        $this->db->insert("tbl_event_ticket", $params);
        return $this->db->insert_id();
    }

    public function update_event_ticket($params, $id)
    {
        $this->db->update("tbl_event_ticket", $params, ["id" => $id]);
    }

    public function get_form($id)
    {
        return $this->db->select("*")->from("tbl_event_ticket_form")->where("id", $id)->get()->row_array();
    }

    public function insert_form($params)
    {
        $this->db->insert("tbl_event_ticket_form", $params);
    }

    public function update_form($params, $id)
    {
        $this->db->update("tbl_event_ticket_form", $params, ['id' => $id]);
    }

    public function get_layout($id)
    {
        return $this->db->select("*")->from("tbl_event_ticket_layout")->where("id", $id)->get()->row_array();
    }

    public function insert_layout($params)
    {
        $this->db->insert("tbl_event_ticket_layout", $params);
    }

    public function update_layout($params, $id)
    {
        $this->db->update("tbl_event_ticket_layout", $params, ["id" => $id]);
    }



    public function delete($id)
    {
        $this->db->update("tbl_event_ticket", ['display' => 0], ["id" => $id]);
    }
}
