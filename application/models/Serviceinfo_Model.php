<?php
class Serviceinfo_Model extends CI_Model{
        
        function insert($arr)
        {
                
                $this->db->insert('_serviceinfo_content', $arr);
                return $this->db->insert_id();
                
        }
        
        function update($id, $arr)
        {
        
                $this->db->where('id', $id);
                $this->db->update('_serviceinfo_content', $arr);
        
        }
        
        function get()
        {       

                    $this->db->select('serviceinfo_content.id,serviceinfo_content.slogan_text  ');
                    $this->db->from('_serviceinfo_content serviceinfo_content');
                    $query=$this->db->get();

                return $query->result();
        }
        
        function get_id($id){
                $this->db->where('id', $id);
                $query = $this->db->get('_serviceinfo_content');
                return $query->result();
        }
        
        function get_lang($lang)
        {
                
                $this->db->where('lang', $lang);
                $query = $this->db->get('_serviceinfo_content');
                return $query->result();
                
        }
        
        function delete($id)
        {
                $this->db->where('id', $id);
                $this->db->delete('_serviceinfo_content');
        }
        
}

?>