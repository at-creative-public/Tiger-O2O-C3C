<?php

defined('BASEPATH') or exit('No direct script access allowed');



class Page_objects_model extends CI_Model

{



    public $object_type = [

        'MAINTITLE' => '主標題',

        'SUBTITLE' => '子標題',

        'PICTURES' => '圖片',

        "TITLELESS_CONTAINER_ROW" => '文字格列',

        "IMAGE_DESCRIPTION_BOX_ROW" => "圖片描述格列",

        "ROW_DESCRIPTION" => '圖文說明列',

        "CONTAINER_START" => '背景容器-開始',

        "CONTAINER_END" => '背景容器-結束'

    ];


    public function __construct()
    {

        parent::__construct();

    }



    public function getImageBoxObject($object_id,$id){
         $this->db->select("*");
         $this->db->where('object_id',$object_id);
         $this->db->where("image_box_id",$id);
         $this->db->from('_image_box_objects');
         $result = $this->db->get()->row_array();
        // var_dump($this->db->last_query()); exit;

        $this->load->model('Page_object_title_model', 'object_title');

        $this->load->model('Page_object_picture_model', 'object_picture');

        $result['objects'] = [];

        $objects = [];


        // $result['objects'] = [];
         if(!empty($result)){
             //var_dump($result['page_object_type']);
             switch ($result['page_object_type']) {

                case "TITLE":

                    $result['objects'][] = $this->object_title->get($result['object_id']);

                    break;

                case "PICTURES":
                    //var_dump($this->object_picture);exit;
                    $result['objects'][] = $this->object_picture->get($result['object_id']);

                    break;

                case "TITLELESS_CONTAINER_ROW":

                    $this->load->model("Page_object_titleless_container_Model");

                    $result['objects'][] = $this->Page_object_titleless_container_Model->get($result['object_id']);

                    break;

                case "IMAGE_DESCRIPTION_BOX_ROW":

                    $this->load->model("Page_object_image_box_Model");

                    $result['objects'][] = $this->Page_object_image_box_Model->get($result['object_id']);

                    break;

                case "ROW_DESCRIPTION":

                    $this->load->model("Page_object_row_description_Model");

                    $result["objects"][] = $this->Page_object_row_description_Model->get($result['object_id']);

                    break;

                case "CONTAINER_START":

                    $this->load->model("Page_object_background_Model");

                    $result["objects"][] = $this->Page_object_background_Model->get($result['object_id']);

                    break;

            }

            
            return $result;
         }
         
        // ->where('id', $id)

        // ->get()->row_array();

        //     $this->load->model('Page_object_title_model', 'object_title');

        //     $this->load->model('Page_object_picture_model', 'object_picture');

        //     $result['objects'] = [];

        //     $objects = [];

           

        //     switch ($result['page_object_type']) {

        //         case "TITLE":

        //             $result['objects'][] = $this->object_title->get($result['object_id']);

        //             break;

        //         case "PICTURES":

        //             $result['objects'][] = $this->object_picture->get($result['object_id']);

        //             break;

        //         case "TITLELESS_CONTAINER_ROW":

        //             $this->load->model("Page_object_titleless_container_Model");

        //             $result['objects'][] = $this->Page_object_titleless_container_Model->get($result['object_id']);

        //             break;

        //         case "IMAGE_DESCRIPTION_BOX_ROW":

        //             $this->load->model("Page_object_image_box_Model");

        //             $result['objects'][] = $this->Page_object_image_box_Model->get($result['object_id']);

        //             break;

        //         case "ROW_DESCRIPTION":

        //             $this->load->model("Page_object_row_description_Model");

        //             $result["objects"][] = $this->Page_object_row_description_Model->get($result['object_id']);

        //             break;

        //         case "CONTAINER_START":

        //             $this->load->model("Page_object_background_Model");

        //             $result["objects"][] = $this->Page_object_background_Model->get($result['object_id']);

        //             break;

        //     }


            //var_dump($result); exit;
           
    }



    public function get($id)

    {

        $result = $this->db->from('_page_objects')

            ->where('id', $id)

            ->get()->row_array();

        $this->load->model('Page_object_title_model', 'object_title');

        $this->load->model('Page_object_picture_model', 'object_picture');

        $result['objects'] = [];

        $objects = [];



        switch ($result['page_object_type']) {

            case "TITLE":

                $result['objects'][] = $this->object_title->get($result['object_id']);

                break;

            case "PICTURES":

                $result['objects'][] = $this->object_picture->get($result['object_id']);

                break;

            case "TITLELESS_CONTAINER_ROW":

                $this->load->model("Page_object_titleless_container_Model");

                $result['objects'][] = $this->Page_object_titleless_container_Model->get($result['object_id']);

                break;

            case "IMAGE_DESCRIPTION_BOX_ROW":

                $this->load->model("Page_object_image_box_Model");

                $result['objects'][] = $this->Page_object_image_box_Model->get($result['object_id']);

                break;

            case "ROW_DESCRIPTION":

                $this->load->model("Page_object_row_description_Model");

                $result["objects"][] = $this->Page_object_row_description_Model->get($result['object_id']);

                break;

            case "CONTAINER_START":

                $this->load->model("Page_object_background_Model");

                $result["objects"][] = $this->Page_object_background_Model->get($result['object_id']);

                break;

        }



        return $result;

    }


    public function getImageBoxItems($image_box_id){
        if(empty($image_box_id)) {

            exit("ERROR_REQUEST");

        }

        $this->db->select("*,displayorder as items_display_order");
        $this->db->from("_image_box_objects");
        $this->db->where("image_box_id",$image_box_id);
        $this->db->order_by("displayorder","asc");
        $result = $this->db->get()->result_array();
         
        $this->load->model('Page_object_title_model', 'object_title');

        $this->load->model('Page_object_picture_model', 'object_picture');

        
        $objects = [];
        
        //var_dump($result); exit;
        if(!empty($result)){
            foreach($result as $res){
                switch ($res['page_object_type']) {

                    case "TITLE":
    
                        $objects[] = $this->object_title->get($res['object_id']);
                        break;
    
                    case "PICTURES":
    
                        $object = $this->object_picture->get($res['object_id']);
                        $objects = array_merge($objects,$object['pictures']);

                        break;
    
                    case "TITLELESS_CONTAINER_ROW":
    
                        $this->load->model("Page_object_titleless_container_Model");
    
                        $object = $this->Page_object_titleless_container_Model->get($res['object_id']);

                        $objects = array_merge($objects,$object['titleless_container']); 

                        break;
    
                    case "IMAGE_DESCRIPTION_BOX_ROW":
    
                        $this->load->model("Page_object_image_box_Model");
    
                        $objects[] = $this->Page_object_image_box_Model->get($res['object_id']);
    
                        break;
    
                    case "ROW_DESCRIPTION":
    
                        $this->load->model("Page_object_row_description_Model");
    
                        $objects[] = $this->Page_object_row_description_Model->get($res['object_id']);
    
                        break;
    
                    case "CONTAINER_START":
    
                        $this->load->model("Page_object_background_Model");
    
                        $objects[] = $this->Page_object_background_Model->get($res['object_id']);
    
                        break;
                }
            }
            
            return $objects;
        }
    }

    public function load_image_box_objects($image_box_id)
    {
        
        if(empty($image_box_id)) {

            exit("ERROR_REQUEST");

        } 


        $this->db->where([

            "image_box_id"=>$image_box_id,

        ]);


        



        $this->db->order_by('displayorder', 'ASC');

        $result = $this->db->get('_image_box_objects')->result_array();
        
       
        $this->load->model('Page_object_title_model', 'object_title');

        $this->load->model('Page_object_picture_model', 'object_picture');
        
        
      
        foreach ($result as $idx => $res) {



            $result[$idx]['objects'] = [];

            $objects = [];

            switch ($res['page_object_type']) {

                case "TITLE":

                    $result[$idx]['objects'][] = $this->object_title->get($res['object_id']);

                    break;

                case "PICTURES":

                    $result[$idx]['objects'][] = $this->object_picture->get($res['object_id']);
                   
                    

                    break;

                case "TITLELESS_CONTAINER_ROW":

                    $this->load->model("Page_object_titleless_container_Model");

                    $result[$idx]["objects"][] = $this->Page_object_titleless_container_Model->get($res['object_id']);

                    

                    break;

                case "IMAGE_DESCRIPTION_BOX_ROW":

                    $this->load->model("Page_object_image_box_Model");

                    $result[$idx]["objects"][] = $this->Page_object_image_box_Model->get($res['object_id']);

                   

                    break;

                case "ROW_DESCRIPTION":

                    $this->load->model("Page_object_row_description_Model");

                    $result[$idx]["objects"][] = $this->Page_object_row_description_Model->get($res['object_id']);

  

                    

                    break;

                case "CONTAINER_START":

                    $this->load->model("Page_object_background_Model");

                    $result[$idx]["objects"][] = $this->Page_object_background_Model->get($res['object_id']);

                 

                    break;

            }

        }
       
        //var_dump($result); exit;
        return $result;

    }

   

    public function hasDetailPage($main_id){
        $this->db->select("id,page_object_type");
        $this->db->where("main_id",$main_id);
        $this->db->from("tbl_page_objects");
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }



    public function load_objects($main_id, $submenu_id)

    {
      
        if ($submenu_id === 0) {

            $this->db->where([

                'main_id' => $main_id,

                'submenu_id' => "0",

            ]);

        } else {

            $this->db->where([

                'submenu_id' => $submenu_id,

            ]);

        }



        $this->db->order_by('displayorder', 'ASC');

        $result = $this->db->get('_page_objects')->result_array();
        $this->load->model('Page_object_title_model', 'object_title');

        $this->load->model('Page_object_picture_model', 'object_picture');



        foreach ($result as $idx => $res) {



            $result[$idx]['objects'] = [];

            $objects = [];

            switch ($res['page_object_type']) {

                case "TITLE":

                    $result[$idx]['objects'][] = $this->object_title->get($res['object_id']);

                    break;

                case "PICTURES":

                    $result[$idx]['objects'][] = $this->object_picture->get($res['object_id']);

                    

                    break;

                case "TITLELESS_CONTAINER_ROW":

                    $this->load->model("Page_object_titleless_container_Model");

                    $result[$idx]["objects"][] = $this->Page_object_titleless_container_Model->get($res['object_id']);

                    

                    break;

                case "IMAGE_DESCRIPTION_BOX_ROW":

                    $this->load->model("Page_object_image_box_Model");

                    $result[$idx]["objects"][] = $this->Page_object_image_box_Model->get($res['object_id']);

                   

                    break;

                case "ROW_DESCRIPTION":

                    $this->load->model("Page_object_row_description_Model");

                    $result[$idx]["objects"][] = $this->Page_object_row_description_Model->get($res['object_id']);

  

                    

                    break;

                case "CONTAINER_START":

                    $this->load->model("Page_object_background_Model");

                    $result[$idx]["objects"][] = $this->Page_object_background_Model->get($res['object_id']);

                 

                    break;

            }

        }





        return $result;

    }

    function update_imagebox_rank($id, $rank)

    {

        $data = array(

            'displayorder' => $rank

        );



        $this->db->where('id', $id);

        $this->db->update('_image_box_objects', $data);

    }


    function update_rank($id, $rank)

    {

        $data = array(

            'displayorder' => $rank

        );



        $this->db->where('id', $id);

        $this->db->update('_page_objects', $data);

    }


    function deleteImageBoxItem($object_id){
      
        if(!empty($object_id)){
            $this->db->delete("_image_box_objects",["object_id"=>$object_id]);
        }
    }
    
    function delete($id)

    {

        $result = $this->db->from('_page_objects')

            ->where('id', $id)

            ->get()->row_array();

        switch ($result['page_object_type']) {

            case "TITLE":

                $this->db->delete('_page_object_title', ['id' => $result['object_id']]);

                break;

            case "PICTURES":

                $this->db->delete('_page_object_picture_info', ['id' => $result['object_id']]);

                $this->db->delete('_page_object_pictures', ['picture_info_id' => $result['object_id']]);

                break;

        }

        $this->db->where('id', $id);

        $this->db->delete('_page_objects');

    }

}

