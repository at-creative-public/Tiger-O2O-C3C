<?php

defined("BASEPATH") or exit("No direct script access allowed");



class Appointment_setting_model extends CI_Model

{



    public function __construct()

    {

        parent::__construct();

    }



    public function get()

    {

        return $this->db->select("*")

            ->from("tblenquiry_record")

            ->order_by("create_date")

            ->get()

            ->result_array();

    }



    public function get_id($id)

    {

        return $this->db->select("*")

            ->from("tblenquiry_record")

            ->where("id", $id)

            ->get()

            ->result_array();

    }



    public function get_front()

    {

        return $this->db->select("*")

            ->from("tbl_appointment_tuner")

            ->order_by("displayorder", "ASC")

            ->where("display", "1")

            ->get()

            ->result_array();

    }



    public function add($params)

    {

        $this->db->insert("tbl_appointment_tuner", $params);

    }



    public function update($params, $id)

    {

        $this->db->where("id", $id)

            ->update('tbl_appointment_tuner', $params);

    }



    public function delete($id)

    {

        $this->db->where('id', $id)

            ->delete('tbl_appointment_tuner');

    }



    public function update_rank($id, $rank)

    {

        $data = array(

            'displayorder' => $rank

        );



        $this->db->where('id', $id);

        $this->db->update('tbl_appointment_tuner', $data);

    }

}

