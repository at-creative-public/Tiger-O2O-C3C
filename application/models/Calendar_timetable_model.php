<?php

use app\libraries\ICSParser;

/**
 * @property CI_Model $this
 * @property MY_Model $this
 * 
 * @property ICSParser $ics_parser
 * @property Booking_model $Booking_model
 * @property Booking_timeslot_model $Booking_timeslot_model
 * @property Booking_calendar_timeslot_model $Booking_calendar_timeslot_model
 */
class Calendar_timetable_model extends MY_Model
{
    // Calendar Config
    public $online_time = '10:00';
    public $offline_time = '19:00';
    public $lunch_starttime = '13:00';
    public $lunch_endtime = '14:00';
    public $booking_startdate;
    public $booking_enddate;
    public $time_interval = 30;
    public $day_interval = 0;
    public $holiday_saturdays = false;
    public $holiday_sundays = true;
    public $public_holidays = true;
    public $booking_success_content = '';
    public $date;

    public $resource_name = 'room';
    public $resource_keyname = 'room_id';
    public $calendar_view_mode = 'w';
    public $calendar_current_date;

    public $ics_parser;

    // Derived Calendar Variable
    public $last_timeslot_time;
    public $all_timeslots = [];
    public $available_timeslots = [];
    public $disabled_hours = [];
    public $calendar_resources = [];
    public $calendar_timeslots = [];
    public $calendar_enrollments = [];
    public $calendar_not_show_empty_enrollments = true;
    public $calendar_enrollments_threshold = 6;

    public $show_resource = false;

    private $rooms;
    private $alert_messages = [];
    private $empty_rooms = [];
    private $exceed_room_timeslot_capacity = [];
    private $room_timeslot_capacity = [];

    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $this->online_time = $this->online_time ?? '10:00';
        $this->offline_time = $this->offline_time ?? '19:00';
        $this->calendar_current_date = date('Y-m-d');
        $this->date = $this->date ?? $this->calendar_current_date;

        $this->last_timeslot_time = date('H:i', strtotime('- ' . $this->time_interval . ' minutes', strtotime($this->offline_time)));
        // if (empty($this->disabled_timeslots)) $this->get_disabled_timeslots();

        $this->get_disabled_hours();
        $this->booking_startdate = $this->booking_startdate ?? date('Y-m-01');
        $this->booking_enddate = $this->booking_enddate ?? date('Y-m-d', strtotime('+2 months -1 day', strtotime(date('Y-m-01'))));
        // $this->booking_startdate = $this->booking_startdate ?? date('Y-m-01 ' . $this->online_time);
        // $this->booking_enddate = $this->booking_enddate ?? date('Y-m-d ' . $this->last_timeslot_time, strtotime('+2 months -1 day', strtotime(date('Y-m-01'))));
    }


    public function set_icsparser()
    {
        $ics_startdate = date('Y-01-01', strtotime('-1 year'));
        $ics_enddate = date('Y-12-31', strtotime('+1 year'));

        $ics_parser = new ICSParser(null, $ics_startdate, $ics_enddate);
        $ics_parser->fromTime = $this->online_time;
        $ics_parser->toTime = $this->offline_time;

        $this->ics_parser = $ics_parser;
        return $ics_parser;
    }

    public function load_setting_item($item, $fields)
    {
        foreach ($fields as $property_name) {
            if (!empty($item[$property_name]))
                $this->{$property_name} = $item[$property_name];
        }
    }

    public function get_disabled_hours()
    {
        $online_hour = (int) date('H', strtotime($this->online_time));
        $offline_hour = (int) date('H', strtotime($this->offline_time));
        $lunch_starttime = (int) date('H', strtotime($this->lunch_starttime));
        $lunch_endtime = (int) date('H', strtotime($this->lunch_endtime));


        for ($h = 0; $h <= 23; $h++) {
            if ($online_hour <= $h && $h < $offline_hour) continue;
            array_push($this->disabled_hours, $h);
        }
    }

    public function get_calendar_events_resources()
    {
        $this->load->model('Booking_calendar_setting_model');
        $res = $this->Booking_calendar_setting_model->get();
        $item = $this->Booking_calendar_setting_model->flatten_keyname_value($res);
        $ics_parser = $this->set_icsparser();
        $this->load_setting_item($item, $this->Booking_calendar_setting_model->fields);

        if ($this->resource_name == 'room') {
            $this->load->model('Room_model', 'resource_model');
        }

        $resources = $this->resource_model->all();
        $r = [];
        foreach ($resources as $resource) {
            $this->calendar_resources[$resource['id']] = $resource;
            array_push($r, $resource['name']);
        }

        $this->load->model('Booking_model');
        $enrollments = $this->Booking_model->get();
        foreach ($enrollments as $enrollment) {
            if (!array_key_exists($enrollment['timeslot_id'], $this->calendar_enrollments))
                $this->calendar_enrollments[$enrollment['timeslot_id']] = [];
            array_push($this->calendar_enrollments[$enrollment['timeslot_id']], $enrollment);
        }

        // var_dump($this->calendar_enrollments); die();

        $this->load->model('Booking_timeslot_model');
        $timeslots = $this->Booking_timeslot_model->get();
       
        foreach ($timeslots as $timeslot) {
            $enrollments = $this->get_calendar_enrollments($timeslot);
            if ($this->calendar_not_show_empty_enrollments && count($enrollments) == 0) continue;

            $resource = $this->get_calendar_resource($timeslot);
            $template = $this->load->view('/backend/booking/calendar_box', [
                'calendar_timetable' => $this,
                'timeslot' => $timeslot,
                'resource' => $resource,
                'enrollments' => $this->get_calendar_enrollments($timeslot)
            ], true);

            $this->time_interval = $timeslot['booking_time_interval'];
            $ics_parser->addEvent(
                $timeslot['id'],
                $timeslot['booking_date'],
                $this->time_interval,
                $resource['name'],
                $this->get_timestamp($timeslot['booking_date'], $timeslot['booking_time'], 0),
                $this->get_timestamp($timeslot['booking_date'], $timeslot['booking_time'], $this->time_interval),
                false,
                'event',
                $template
            );
        }

        return [
            'events' => $ics_parser->json(),
            'resources' => json_encode($r)
        ];
    }

    public function get_calendar_resource($timeslot)
    {
        return $this->calendar_resources[$timeslot[$this->resource_keyname]];
    }

    public function get_calendar_enrollments($timeslot)
    {
        $timeslot_id = $timeslot['id'];
        return array_key_exists($timeslot_id, $this->calendar_enrollments) ? $this->calendar_enrollments[$timeslot_id] : [];
    }

    public function get_least_viable_timeslot($timeslot = null, $format = 'Y-m-d H:i')
    {
        $date = date('Y-m-d', $timeslot ? strtotime($timeslot) : time());
        $current_hours = date('H');
        $current_minutes = intval(date('i', $timeslot ? strtotime($timeslot) : time()));

        $minutes_interval = $current_minutes >= 30 ? 60 : 30;
        $minutes_interval_plus_day_interval = $minutes_interval + $this->day_interval * 60 * 24;
        $least_viable_timeslot = $this->get_datetime($date, $current_hours . ':00:00', $minutes_interval_plus_day_interval, $format);

        return $least_viable_timeslot;
    }

    public function get_timestamp($date, $time, $add_time_interval = 30)
    {
        $orignal = strtotime($date . ' ' . $time);
        return $add_time_interval && $add_time_interval > 0 ? strtotime('+' . $add_time_interval . ' minutes', $orignal) : $orignal;
    }

    public function get_datetime($date, $time, $add_time_interval = 30, $format = 'Y-m-d H:i:s')
    {
        $timestamp = $this->get_timestamp($date, $time, $add_time_interval);
        return date($format, $timestamp);
    }

    public function split_datetime($datetime, $time_format = 'H:i')
    {
        $parts = explode(' ', $datetime);

        return [
            $parts[0],
            (new DateTime($parts[1]))->format($time_format)
        ];
    }

    public function check_room_capacity($date, $time, $room_id, $to_ymdhis = true)
    {
        // $ymdhis = $date . ' ' . $time;
        // if ($to_ymdhis) $ymdhis = (new DateTime($ymdhis))->format('Y-m-d H:i:s');

        // $given_start = $ymdhis;
        // $given_end = '';

        return true;
    }

    public function get7days($date = null)
    {
        if ($date == null) $date = date('Y-m-d');

        return [
            $date,
            date('Y-m-d', strtotime('+1 day', strtotime($date))),
            date('Y-m-d', strtotime('+2 day', strtotime($date))),
            date('Y-m-d', strtotime('+3 day', strtotime($date))),
            date('Y-m-d', strtotime('+4 day', strtotime($date))),
            date('Y-m-d', strtotime('+5 day', strtotime($date))),
            date('Y-m-d', strtotime('+6 day', strtotime($date))),
        ];
    }

    public function get_available_timeslots_dates($dates = [], $reset = false)
    {
        foreach ($dates as $date) {
            $this->get_available_timeslots($date, $reset);
        }
    }

    public function get_available_timeslots($date = null, $reset = false)
    {
        if ($date == null) $date = date('Y-m-d');
        $this->available_timeslots[$date] = [];

        $this->load->model('Booking_calendar_timeslot_model');

        if ($reset) {
            $this->generate_available_timeslots($date, $this->time_interval);

            $params = [
                'date' => $date,
                'time_interval' => $this->time_interval,
                'all_timeslots' => json_encode($this->all_timeslots),
                'available_timeslots' => json_encode($this->available_timeslots[$date]),
                'updated_at' => date('Y-m-d H:i:s')
            ];
            $this->Booking_calendar_timeslot_model->update_entity($params, ['date' => $date]);
            $this->available_timeslots[$date] = $params;
        } else {
            $res = $this->Booking_calendar_timeslot_model->get_entity(null, [], ['date' => $date]);

            if (!empty($res)) {
                $this->available_timeslots[$date] = $res;

                // $this->generate_available_timeslots($date, $this->time_interval);
            } else {
                $this->generate_available_timeslots($date, $this->time_interval);

                $params = [
                    'date' => $date,
                    'time_interval' => $this->time_interval,
                    'all_timeslots' => json_encode($this->all_timeslots),
                    'available_timeslots' => json_encode($this->available_timeslots[$date]),
                    'created_at' => date('Y-m-d H:i:s')
                ];
                $this->Booking_calendar_timeslot_model->insert_entity($params);
                $this->available_timeslots[$date] = $params;
            }
        }
    }

    public function generate_all_timeslots($time_interval, $time_format = "H:i")
    {
        $this->all_timeslots = [];

        $online_time = $this->online_time;
        $offline_time = $this->offline_time;
        $date = date('Y-m-d');

        $start = $this->get_datetime($date, '00:00', 0, $time_format);
        $end = $this->get_datetime($date, '00:00', $time_interval, $time_format);
        $limit = $this->get_datetime($date, '23:00', 0, $time_format);

        $i = 0;
        while ($start < $limit) {
            $cond = $online_time <= $start && $start < $offline_time;
            $params = $start;
            if ($cond) array_push($this->all_timeslots, $params);

            $start = $end;
            $end = $this->get_datetime($date, $end, $time_interval, $time_format);

            $i++;
            if ($i == 60)
                break;
        }
    }

    public function generate_available_timeslots($date, $time_interval, $time_format = "H:i")
    {
        if (!is_string($date)) throw new Exception('$date must be in date("Y-m-d")');

        $online_time = $this->online_time;
        $offline_time = $this->offline_time;
        $lunch_starttime = $this->lunch_starttime;
        $lunch_endtime = $this->lunch_endtime;

        $start = $this->get_datetime($date, '00:00', 0, $time_format);
        $end = $this->get_datetime($date, '00:00', $time_interval, $time_format);
        $limit = $this->get_datetime($date, $offline_time, 0, $time_format);
        $empty_available_timeslots = empty($this->available_timeslots);

        $i = 0;
        while ($start < $limit) {
            $cond = $online_time <= $start && $start < $offline_time;
            $after_lunch_cond = !($lunch_starttime <= $start && $start < $lunch_endtime);
            $not_holiday_saturdays_cond = !$this->holiday_saturdays || date('D', strtotime($date)) != 'Sat';
            $not_holiday_sundays_cond = !$this->holiday_sundays || date('D', strtotime($date)) != 'Sun';
            $not_public_holiday_cond = empty($this->ics_parser->checkIfHoliday($date));
            // $save_timeslot_cond = $empty_available_timeslots || $reset;

            $params = $start;
            if ($cond && $after_lunch_cond && $not_holiday_saturdays_cond && $not_holiday_sundays_cond && $not_public_holiday_cond)
                $this->available_timeslots[$date][] = $params;

            $start = $end;
            $end = $this->get_datetime($date, $end, $time_interval, $time_format);

            $i++;
            if ($i == 60)
                break;
        }
    }
}
