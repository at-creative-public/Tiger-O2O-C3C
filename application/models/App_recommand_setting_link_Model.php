<?php
defined('BASEPATH') or exit('No direct script access allowed');

class App_recommand_setting_link_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get()
    {
        return $this->db->from('app_link')
            ->order_by("displayorder", "asc")
            ->get()
            ->result();
    }

    function get_front($include_hidden = false)
    {
        $this->db->select('*');
        $this->db->order_by("displayorder", "asc");
        if (!$include_hidden) {
            $this->db->where('display', 1);
        }
        $this->db->from('app_link');
        $query = $this->db->get();
        return $query->result();
    }

    function get_id($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('app_link');
        return $query->result();
    }

    public function update($params, $id)
    {
        $this->db->where('id', $id)
            ->update('app_link', $params);
    }
    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('app_link');
    }

    function update_rank($id, $rank)
    {
        $data = array(
            'displayorder' => $rank
        );

        $this->db->where('id', $id);
        $this->db->update('app_link', $data);
    }
}
