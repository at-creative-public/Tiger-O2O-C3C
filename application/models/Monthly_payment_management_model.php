<?php

defined("BASEPATH") or exit("No dreict script is allowed");

class Monthly_payment_management_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add_subscribe_user($user_id, $amount, $email)
    {
        $this->db->set("user_id", $user_id);
        $this->db->set("amount", $amount);
        $this->db->set("email", $email);
        $this->db->insert("tbl_monthly_subscribe_user");
    }

    public function monthly_subscribe_user_payment_request()
    {
        return $this->db->select("*,tbl_monthly_subscribe_user.id as sub_id")
            ->from("tbl_monthly_subscribe_user")
            ->join("tblmembers", "tblmembers.id = tbl_monthly_subscribe_user.user_id")
            ->get()
            ->result_array();
    }

    public function get_record($id)
    {
        return $this->db->select("*")
            ->from("tbl_monthly_subscribe_user")
            ->where("id", $id)
            ->get()
            ->result_array();
    }

    
}
