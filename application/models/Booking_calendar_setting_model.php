<?php
defined("BASEPATH") or exit("No direct script access allowed");

/** 
 * Booking_calendar_setting_model model
 * 
 * @property MY_Model $this
 * @property MY_IndexCrudModel $this
 * 
 * CREATE TABLE `artechon_noah_medical_specialist`.`tblbooking_calendar_setting` ( `id` INT NOT NULL , `keyname` VARCHAR(191) NOT NULL , `value` TEXT NULL ) ENGINE = InnoDB;
 * ALTER TABLE `tblbooking_calendar_setting` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, add PRIMARY KEY (`id`);
 */
class Booking_calendar_setting_model extends MY_IndexCrudModel
{
    public $has_displayorder = false;
    public $table_prefix = '';
    public $tbl = 'booking_calendar_setting';
    public $short = 'bcs';
    public $database = 'default';
    public $fields = ['time_interval', 'day_interval', 'online_time', 'offline_time', 'lunch_starttime', 'lunch_endtime', 'holiday_saturdays', 'holiday_sundays', 'public_holidays', 'booking_startdate', 'booking_enddate', 'booking_success_content'];

    public function __construct()
    {
        parent::__construct();
    }
}
