<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Api_auth_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }
    /*
    public function auth($access_token)
    {
        if ($access_token == "") {
            $data['error'] = true;
            $data['status'] = "401";
            $data['reason'] = "Invalid Token";
            return $data;
        }

        $result = $this->db->select("*")->from("tbl_merchants")->where("api_key", $access_token)->get()->row_array();
        $data = [];
        if ($result == null) {
            $data['error'] = true;
            $data['status'] = "401";
            $data['reason'] = "Invalid Token";
            return $data;
        } else {
            if ($result['deleted_at'] != "0" || $result['force_disabled']) {
                $data['error'] = true;
                $data['status'] = "401";
                $data['reason'] = "Disabled Account";
                return $data;
            }

            if ($result['merchant_valid_until'] < time()) {
                $data['error'] = true;
                $data['status'] = "401";
                $data['reason'] = "Expired Account";
                return $data;
            }

            $data['error'] = false;
            $data['status'] = "200";
            $data['reason'] = "Valid Account";
            return $data;
        }
    }
    */

    public function auth($access_token)
    {
        if ($access_token == "") {
            $data['error'] = true;
            $data['status'] = "401";
            $data['reason'] = "Invalid Token";
            echo json_encode($data);
            exit;
        }
        $result = $this->db->select("*")->from("tbl_merchants")->where("api_key", $access_token)->get()->row_array();
        $data = [];
        if ($result == null) {
            $data['error'] = true;
            $data['status'] = "401";
            $data['reason'] = "Invalid Token";
            echo json_encode($data);
            exit;
        } else {
            if ($result['deleted_at'] != "0" || $result['force_disabled']) {
                $data['error'] = true;
                $data['status'] = "403";
                $data['reason'] = "Disabled Account";
                echo json_encode($data);
                exit;
            }
            if ($result["merchant_valid_until"] < time()) {
                $data["error"] = true;
                $data['status'] = "403";
                $data['reason'] = "Expired Account";
                echo json_encode($data);
                exit;
            }
            return $result['id'];
        }
    }

    public function ref_auth($merchant_id, $ref_key)
    {
        if ($ref_key == "") {
            $data['error'] = true;
            $data['status'] = "400";
            $data['reason'] = "Invalid Token";
            echo json_encode($data);
            exit;
        }

        $result = $this->db->select('*')->from("tbl_virtual_passes")->where("user_id", $merchant_id)->where("ref_key", $ref_key)->get()->row_array();
        $data = [];
        if ($result == null) {
            $data['error'] = true;
            $data['status'] = "403";
            $data['reason'] = "Invalid Token";
            echo json_encode($data);
            exit;
        } else {
            if ($result['display'] == 0) {
                $data['error'] = true;
                $data['status'] = "403";
                $data['reason'] = 'Deleted resource';
                echo json_encode($data);
                exit;
            }
            return $result['id'];
        }
    }

    public function event_ticket_ref_auth($merchant_id, $ref_key)
    {
        if ($ref_key == "") {
            $data['error'] = true;
            $data['status'] = "400";
            $data['reason'] = "Invalid Token";
            echo json_encode($data);
        }

        $result = $this->db->select("*")->from("tbl_event_ticket")->where("user_id", $merchant_id)->where("ref_key", $ref_key)->get()->row_array();
        $data = [];
        if ($result == null) {
            $data['error'] = true;
            $data['status'] = "403";
            $data['reason'] = "Invalid Token";
            echo json_encode($data);
            exit;
        } else {
            if ($result['display'] == 0) {
                $data['error'] = true;
                $data['status'] = "403";
                $data['reason'] = "Deleted resource";
                echo json_encode($data);
                exit;
            }
            return $result['id'];
        }
    }
}
