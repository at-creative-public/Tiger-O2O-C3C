<?php

class Registration_model extends CI_Model
{

    public function registration($params)
    {
        $pass_info = $this->db->select('*')->from("tbl_virtual_passes")->where("id", $params["pass_id"])->get()->row_array();
        $current_serial = $this->db->select_max("account_id")->from("tbl_members")->where("pass_id", $params['pass_id'])->get()->row_array();
        if ($pass_info['start_from'] > $current_serial['account_id']) {
            $params['account_id'] = $pass_info['start_from'];
        } else {
            $params['account_id'] = intVal($current_serial['account_id']) + 1;
        }
        $params['create_date'] = date("Y-m-d H:i:s", time());
        $params['level'] = $pass_info['default_level'];
        $this->db->insert("tbl_members", $params);
        return $this->db->insert_id();
    }


    public function coupon_registration($params)
    {
        $coupon_info = $this->db->select("*")->from("tbl_coupons")->where("id", $params['coupon_id'])->get()->row_array();
        $current_serial = $this->db->select_max("coupon_series")->from("tbl_coupons_holder")->where("coupon_id", $params['coupon_id'])->get()->row_array();
        $params['coupon_series'] = intVal($current_serial['coupon_series']) + 1;
        $params['display_code'] = $coupon_info['coupon_code'] . "-" . $params["coupon_series"];
        $params['create_date'] = date("Y-m-d H:i:s", time());
        $params['coupon_name'] = $coupon_info['project_name'];
        $params['coupon_amount'] = $coupon_info['coupon_amount'];
        $params['expiry_date'] = $coupon_info['expiry_date'];

        $params['status'] = "valid";
        $this->db->insert("tbl_coupons_holder", $params);
        return $this->db->insert_id();
    }

    public function event_ticket_registration($params)
    {

        $ticket_info = $this->db->select("*")->from("tbl_event_ticket")->where("id", $params["event_ticket_id"])->get()->row_array();
        $current_serial = $this->db->select_max("event_code_series")->from("tbl_event_ticket_holder")->where("event_ticket_id", $params['event_ticket_id'])->get()->row_array();
        $params['event_code_series'] = intVal($current_serial['event_code_series']) + 1;
        $params['display_code'] = $ticket_info['event_code'] . "-" . $params['event_code_series'];
        $params['create_date'] = date("Y-m-d H:i:s", time());
        $params['status'] = "valid";
        $this->db->insert("tbl_event_ticket_holder", $params);
        return $this->db->insert_id();
    }
}
