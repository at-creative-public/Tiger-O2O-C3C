<?php

class Statistics_model extends CI_Model
{

    public function __construct()
    {
    }

    public function total_range($user_id)
    {
    }

    public function report_listing($user_id)
    {
        $result = $this->db->select("*")->from("tbl_crm_report_record")->where("user_id", $user_id)->order_by("year", "desc")->order_by("month", "desc")->get()->result_array();
        //echo "---before---";
        //var_dump($result);
        //echo "---after---";
        $formated = [];
        foreach ($result as $res) {
            $formated[$res['type']][$res['pass_id']][$res['year']][$res['month']] = $res;
        }
        //var_dump($formated);
        return $formated;
    }

    public function get_all_pass($user_id)
    {
        $passes = $this->db->select("*")->from("tbl_virtual_passes")->where("user_id", $user_id)->where("display", 1)->get()->result_array();
        $coupon = $this->db->select("*")->from("tbl_coupons")->where("user_id", $user_id)->where("display", 1)->get()->result_array();
        $event_ticket = $this->db->select("*")->from("tbl_event_ticket")->where("user_id", $user_id)->where("display", 1)->get()->result_array();
        $result = [
            "membership" => $passes,
            "coupon" => $coupon,
            "event_ticket" => $event_ticket
        ];
        return $result;
    }

    public function specific_range($type, $user_id)
    {
        $this->db->select("*");
        switch ($type) {
            case "membership":
                $this->db->from("tbl_virtual_passes");
                break;
            case "coupon":
                $this->db->from("tbl_coupons");
                break;
            case "event_ticket":
                $this->db->from("tbl_event_ticket");
                break;
        }
        $this->db->where("user_id", $user_id);
        $this->db->where("display", 1);
        $result = $this->db->get()->result_array();
        $list = [];
        foreach ($result as $res) {
            $list[] = $res['id'];
        }
        return $list;
    }

    public function listing_pass($user_id, $type, $general = false)
    {
        switch ($type) {
            case "membership":
                $result = $this->db->select("*")->from("tbl_virtual_passes")->where("user_id", $user_id)->where("display", 1)->get()->result_array();
                break;
            case "coupon":
                $result = $this->db->select("*")->from("tbl_coupons")->where("user_id", $user_id)->where("display", 1)->get()->result_array();
                break;
            case "event_ticket":
                $result = $this->db->select("*")->from("tbl_event_ticket")->where("user_id", $user_id)->where("display", 1)->get()->result_array();
                break;
        }
        return $result;
    }

    public function issued_pass($user_id)
    {
        $passes = $this->db->select("*")->from("tbl_virtual_passes")->where("user_id", $user_id)->where("display", 1)->get()->num_rows();
        $coupon = $this->db->select("*")->from("tbl_coupons")->where("user_id", $user_id)->where("display", 1)->get()->num_rows();
        $event_ticket = $this->db->select("*")->from("tbl_event_ticket")->where("user_id", $user_id)->where("display", 1)->get()->num_rows();
        $result = [
            "membership" => $passes,
            "coupon" => $coupon,
            "event_ticket" => $event_ticket
        ];
        return $result;
    }

    public function get_issued_pass_num($type, $range)
    {
        if ($range == []) {
            return 0;
        }
        $this->db->select("*");
        switch ($type) {
            case "membership":
                $this->db->from("tbl_members");
                $this->db->where_in("pass_id", $range);
                break;
            case "coupon":
                $this->db->from("tbl_coupons_holder");
                $this->db->where_in("coupon_id", $range);
                break;
            case "event_ticket":
                $this->db->from("tbl_event_ticket_holder");
                $this->db->where_in("event_ticket_id", $range);
                break;
        }
        return $this->db->get()->num_rows();
    }

    public function issued_pass_detail($user_id)
    {
        $result = [];
        $membership_range = $this->specific_range("membership", $_SESSION['id']);
        $coupon_range = $this->specific_range("coupon", $_SESSION['id']);
        $event_ticket_range = $this->specific_range("event_ticket", $_SESSION['id']);
        $result = [
            "membership" => $this->get_issued_pass_num("membership", $membership_range),
            "coupon" => $this->get_issued_pass_num("coupon", $coupon_range),
            "event_ticket" => $this->get_issued_pass_num("event_ticket", $event_ticket_range)
        ];
        return $result;
    }

    public function list_pass_details($pass_id, $type)
    {
        $this->db->select("*");
        switch ($type) {
            case "membership":
                $this->db->from("tbl_members");
                break;
            case "coupon":
                $this->db->from("tbl_coupons_holder");
                break;
            case "event_ticket":
                $this->db->from("tbl_event_ticket_holder");
                break;
        }
        $result["total"] = $this->db->get()->num_rows();
    }

    public function rank_membership($pass_id, $level)
    {
        $users = $this->db->select('*')->from("tbl_members")->where("level", $level)->order_by("points", "desc")->limit(0, 10)->get()->result_array();
        return $users;
    }

    public function join_in_thirty_days($pass_id, $type)
    {
        $past = strtotime(date("d-m-Y", time()) . "00:00:00 -1 month");
        $this->db->select("*");
        switch ($type) {
            case "membership":
                $this->db->from("tbl_members");
                break;
            case "coupon":
                $this->db->from("tbl_coupons_holder");
                break;
            case "event_ticket":
                $this->db->from("tbl_evnet_ticket_holder");
                break;
        }
        $this->db->where("id", $pass_id);
        $this->db->where("UNIX_TIMESTAMP(create_date) >", $past);
        $result = $this->db->get()->num_rows();
        return $result;
    }

    public function get_top_ten_users_integrated($pass_id)
    {
        $this->load->model("Virtual_passes_model");
        if ($this->Virtual_passes_model->check_type($pass_id)) {
        } else {
            $levels = $this->Virtual_passes_model->get_full_level_setting($pass_id);
            $data = [];
            foreach ($levels as $level) {

                $data['level'][$level['id']] = $this->get_top_ten_users($level['id']);
                $data["name"][$level['id']] = $level['value'];
            }
            return $data;
        }
    }

    public function get_all_level_api($pass_id)
    {
        $this->db->distinct();
        $raws = $this->db->select('*')->from("tbl_members")->where("pass_id", $pass_id)->get()->result_array();
        $levels = [];
        foreach($raws as $level){
            

        }

    }

    public function get_top_ten_users_api($level)
    {
    }

    public function get_top_ten_users($level)
    {
        $result =  $this->db->select("*")->from("tbl_members")->where("level", $level)->where("status", "valid")->order_by("points", "desc")->limit(10, 0)->get()->result_array();
        foreach ($result as $index => $row) {
            switch (true) {
                case $row['name'] !== "":
                    $name = $row['name'];
                    break;
                case $row['english_name'] !== "":
                    $name = $row['english_name'];
                    break;
                case $row['chinese_name'] !== "";
                    $name = $row['chinese_name'];
                    break;
                case $row['surname'] !== "" || $row['given_name'] !== "":
                    $name = $row['given_name'] . " " . $row['surname'];
                    break;
                default:
                    $name = "No Information";
                    break;
            }
            $result[$index]['display_name'] = $name;
        }
        return $result;
    }
}
