<?php

/**
 * @property MY_IndexCrudModel $this
 */
class Booking_questions_model extends MY_IndexCrudModel
{
    public $has_displayorder = true;
    public $table_prefix = '';
    public $tbl = 'booking_questions';
    public $short = 'questions';
    public $database = 'default';

    public function get_question_submit($booking_id)
    {
        $this->load->model('Booking_questions_submit_model');

        return $this->db
            ->from($this->table_prefix . $this->tbl . ' ' . $this->tbl_short)
            ->join(
                $this->Booking_questions_submit_model->table_prefix . $this->Booking_questions_submit_model->tbl . ' ' . $this->Booking_questions_submit_model->tbl_short,
                $this->Booking_questions_submit_model->tbl_short . '.booking_question_id = ' . $this->tbl_short . '.id'
            )
            ->where($this->Booking_questions_submit_model->tbl_short . '.booking_id', $booking_id)
            ->select($this->tbl_short . '.*')
            ->get()
            ->result_array();
    }

    public function get_question_types()
    {
        $items = $this->db
            ->from($this->table_prefix . $this->tbl)
            ->select('type_id')
            ->group_by('type_id')
            ->get()
            ->result_array();

        $res = [];
        foreach ($items as $item) $res[$item['type_id']] = $item['type_id'];

        return $res;
    }

    public function all($where = [])
    {
        // $fields = [$this->short . '.*'];
        // return $this->getAll($this->database, $this->table, $fields, $where);
        $this->db->from($this->table_prefix . $this->tbl);
        if (!empty($where)) $this->db->where($where);

        $this->db->order_by('displayorder', 'asc');
        return $this->db->get()->result_array();
    }
}
