<?php
class Settings_Model extends CI_Model{
        public $columns = array();
        public $_primary_keys = array('key');
        public $_table= "_settings";
        
        function __set($name, $value)
        {
                $this->columns[$name] = $value;
        }

        function insert($arr)
        {
                
                $this->db->insert('_settings', $arr);
                return $this->db->insert_id();
                
        }
        
        function update($id, $arr)
        {
        
                $this->db->where('id', $id);
                $this->db->update('_settings', $arr);
        
        }
        
        function get()
        {
                $query = $this->db->get('_settings');
                return $query->result();
        }
        
        function get_id($id){
                $this->db->where('id', $id);
                $query = $this->db->get('_settings');
                return $query->result();
        }
        
        function get_lang($lang)
        {
                
                $this->db->where('lang', $lang);
                $query = $this->db->get('_settings');
                return $query->result();
                
        }
        
        function delete($id)
        {
                $this->db->where('id', $id);
                $this->db->delete('_settings');
        }

        function save() {
        $update = FALSE;
        if($this->columns)
        foreach($this->columns as $key => $value) {
            if(in_array($key, $this->_primary_keys)){
                $update = TRUE;
                $id = $value;
                $this->db->where($key, $value);
            }
            else
                $this->db->set($key, $value);
        }
        if($update) {
            $this->db->update($this->_table);
            return $this->columns[$this->_primary_keys[0]];
        }
        else {
            $this->db->insert($this->_table);
            return $this->db->insert_id();
        }
    }

        
}

?>