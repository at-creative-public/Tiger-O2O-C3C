<?php

class Upload_model extends CI_Model
{

    public function upload_handler($files)
    {

        if (count($files) == 0) {
            return $filenames;
        }

        foreach (array_keys($files) as $file) {
            if ($files[$file]['error'] == 0) {
                $filenames[$file] = '';
                $result = $this->uploadfile($file);
                if ($result['status'] == 'success') {
                    $filenames[$file] = $result['url'];
                }
            }
        }
        return $filenames;
    }

    public function uploadfile($filename, $classific, $id, $type = "")
    {
        if (!defined('MAX_SIZE')) {
            define("MAX_SIZE", "50000");
        }

        $result = array();
        $result['message'] = "";
        $result['url'] = "";
        $result['status'] = "";

        //upload code
        //handle upload file
        $status = "success";


        $image = $_FILES[$filename]["name"];
        $uploadedfile = $_FILES[$filename]['tmp_name'];

        if ($image) {

            $filename = stripslashes($_FILES[$filename]['name']);

            $extension = $this->getExtension($filename);
            $extension = strtolower($extension);


            if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) {
                // echo "Unknown Extension..!";
                $status = "fail";
                $message = "Unknown Extension..!";
            } else {
                $size = filesize($uploadedfile);
                if ($size > MAX_SIZE * 1024) {
                    // echo "File Size Excedeed..!!";
                    $status = "fail";
                    $message = "File Size Excedeed..!!";
                }

                if ($extension == "jpg" || $extension == "jpeg") {
                    //$uploadedfile = $_FILES[$filename]['tmp_name'];
                    $src = imagecreatefromjpeg($uploadedfile);
                } else if ($extension == "png") {
                    //$uploadedfile = $_FILES[$filename]['tmp_name'];
                    $src = imagecreatefrompng($uploadedfile);
                } else {
                    $src = imagecreatefromgif($uploadedfile);
                    // echo $scr;
                }

                list($width, $height) = getimagesize($uploadedfile);

                if ($width > 1200) { //if width >1000 then resize it
                    $newwidth = 1200;
                    $newheight = ($height / $width) * $newwidth;
                } else {
                    $newwidth = $width;
                    $newheight = $height;
                }

                $tmp = imagecreatetruecolor($newwidth, $newheight);

                imagealphablending($tmp, false);
                imagesavealpha($tmp, true);

                // $newwidth1=1000;
                // $newheight1=($height/$width)*$newwidth1;
                // $tmp1=imagecreatetruecolor($newwidth1,$newheight1);

                imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                // $filename = "../images/product-image/Cars/". $_FILES['image-1']['name'];
                //$newName = date('YmdHis') . rand(1000, 9999);
                if ($classific == "logo") {
                    $newName = "logo";
                }
                if ($classific == "strip") {
                    $newName = "strip";
                }
                if ($classific == "icon") {
                    $newName = "icon";
                }
                if ($classific == "email_banner") {
                    $newName = "email_banner";
                }

                if ($type == "") {
                    if (!file_exists($_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/membership/" . $id . "/")) {
                        mkdir($_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/membership/" . $id . "/", 0755);
                    }
                    $filename = $_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/membership/" . $id . "/" . $newName . "." . $extension;
                } else {
                    if (!file_exists($_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/" . $type . "/" . $id . "/")) {
                        mkdir($_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/" . $type . "/" . $id . "/", 0755);
                    }
                    $filename = $_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/" . $type . "/"  . $id . "/" . $newName . "." . $extension;
                }

                if ($extension == "jpg" || $extension == "jpeg") {
                    imagejpeg($tmp, $filename, 100);
                } else {
                    if ($extension == "png") {



                        imagepng($tmp, $filename);
                    }
                }
                imagedestroy($src);
                imagedestroy($tmp);
                if ($type == "") {
                    $url = 'public/images/uploads/membership/' . $id . '/' . $newName . "." . $extension;
                } else {
                    $url = 'public/images/uploads/' . $type . '/' . $id . '/' . $newName . "." . $extension;
                }
                $message = "successful upload";
            }
        }
        // imagecopyresampled($tmp1,$src,0,0,0,0,$newwidth1,$newheight1,$width,$height);

        $result['message'] = $message;
        $result['url'] = $url;
        $result['status'] = $status;
        return $result;
    }

    public function upload_PDF($filename)
    {
        $targetfolder = "pulbic/pdf/uploads/";
        $targetfolder = $targetfolder . date('YmdHis') . rand(1000, 9999) . '.pdf';
        move_uploaded_file($_FILES['file']['tmp_name'], $targetfolder);
        return  $targetfolder;
    }

    private function getExtension($str)
    {
        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }
        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return $ext;
    }
}
