<?php

defined("BASEPATH") or exit("No direct script access allowed");



class Video_setting_Model extends CI_Model

{

    public function __construct()

    {

        parent::__construct();

    }



    public function getHomeData(){
        $this->db->select("*");
        $this->db->from("_landing_main");
        $this->db->where("title","主頁");
        $this->db->or_where("uri","index");
        $result = $this->db->get()->row_array();
        if(!empty($result)){
            return $result["id"];
        }else{
            return false;
        }
    }


    public function get_video_record($category_id){
        if(!empty($category_id)){
            $result = $this->db->select("*")

            ->order_by("displayorder", "ASC")

            ->where("category_id",$category_id)

            ->from("tbl_video_record")

            ->get()

            ->result_array();

            return $result;
        }
    }
    

    public function get_category($id)

    {

        
        if(!empty($id)){  
            $result["category"] = $this->db->select("*")
                ->where("id",$id)

                ->from('tbl_video_category')
                
                ->get()

                ->row_array();
            

           
            $result['category'] = json_decode(json_encode($result['category']), true);
            
            return $result['category'];

        }else{ 
            return "";
        }

    }


    

    public function get()

    {

        
        $result["video"] = $this->db->select("*")

            ->order_by("displayorder", "asc")

            ->from('tbl_video_category')

            ->get()

            ->result();
          

        
        $result['video'] = json_decode(json_encode($result['video']), true);
        


        return $result;

    }



    public function get_front()

    {

        $result['image'] = $this->db->select("*")

            ->order_by("displayorder", "asc")

            ->where('display', '1')

            ->from('banner')

            ->get()

            ->result();

        $result['image'] = json_decode(json_encode($result['image']), true);



        $result['text'] = $this->db->select('*')

            ->order_by("displayorder", "asc")

            ->where('display', '1')

            ->from('home_text')

            ->get()

            ->result();

        $result['text'] = json_decode(json_encode($result['text']), true);



        $result["link"] = $this->db->select("*")

            ->order_by("displayorder", "ASC")

            ->where("display", '1')

            ->from("home_link")

            ->get()

            ->result();

        $result["link"] = json_decode(json_encode($result["link"]), true);



        $result['row'] = $this->db->select("*")

            ->order_by("displayorder", "ASC")

            ->where('display', '1')

            ->from('home_row_description')

            ->get()

            ->result();

        $result['row'] = json_decode(json_encode($result['row']), true);

        $result["video"] = $this->db->select("*")

            ->order_by("displayorder", "asc")

            ->from('tbl_home_video')

            ->where('display', '1')

            ->get()

            ->result();

        $result['video'] = json_decode(json_encode($result['video']), true);



        return $result;

    }



    public function get_banner_id($id)

    {

        return $this->db->select("*")

            ->from('banner')

            ->where('id', $id)

            ->get()

            ->result();

    }



    public function get_title_id($id)

    {

        return $this->db->select('*')

            ->from("home_text")

            ->where('id', $id)

            ->get()

            ->result();

    }



    public function get_link_id($id)

    {

        return $this->db->select("*")

            ->from("home_link")

            ->where("id", $id)

            ->get()

            ->result_array();

    }



    public function get_row_description_id($id)

    {

        return $this->db->select("*")

            ->from("home_row_description")

            ->where("id", $id)

            ->get()

            ->result_array();

    }
   
    
    public function get_video(){
        // $this->db->select("*,vc.id as cid,vc.title as main_title");
        // $this->db->from("tbl_video_category vc");
        // $this->db->join("tbl_video_record vr","vr.category_id = vc.id");
        // $query = $this->db->get();
        // $result = $query->result_array();
        // return $result;
       

        $this->db->select("*");
        $this->db->from("tbl_video_category ");
        $this->db->where("display=1");
        $query = $this->db->get();
        $result_category = $query->result_array();
        if(!empty($result_category)){
            foreach($result_category as $k=>$category){
                $category_id = $category["id"];
                $this->db->select("*");
                $this->db->where("category_id",$category_id);
                $this->db->from("tbl_video_record ");
                $this->db->where("display=1");
                $query = $this->db->get();
                $result_video = $query->result_array();
                $result_category[$k]['video_records'] = $result_video;

            }
        }

        //var_dump($result_category); exit;
        return $result_category;
    }

    public function delete_video($category_id,$id){
        //var_dump($id); exit;
       $this->db->where('id', $id);
       $this->db->where('category_id', $category_id);
       $this->db->delete('tbl_video_record');

     }
    

    public function delete_category($id){
        //var_dump($id); exit;
       $this->db->where('id', $id);

       $this->db->delete('tbl_video_category');

       // var_dump($this->db->last_query());exit;


    }


    public function get_video_id($id)

    {

        return $this->db->select("*")

            ->from("tbl_video_record")

            ->where("id", $id)

            ->get()

            ->result_array();

    }



    function update_image_rank($id, $rank)

    {

        $data = array(

            'displayorder' => $rank

        );



        $this->db->where('id', $id);

        $this->db->update('banner', $data);

    }



    function update_text_rank($id, $rank)

    {

        $data = array(

            'displayorder' => $rank

        );



        $this->db->where('id', $id);

        $this->db->update('home_text', $data);

    }



    function update_link_rank($id, $rank)

    {

        $data = array(

            'displayorder' => $rank

        );



        $this->db->where('id', $id);

        $this->db->update('home_link', $data);

    }



    function update_row_rank($id, $rank)

    {

        $data = array(

            'displayorder' => $rank

        );



        $this->db->where('id', $id);

        $this->db->update('home_row_description', $data);

    }





    function delete_banner($id)

    {

        $this->db->where('id', $id);

        $this->db->delete('banner');

    }



    function delete_text($id)

    {

        $this->db->where('id', $id);

        $this->db->delete('home_text');

    }



    function delete_link($id)

    {

        $this->db->where('id', $id);

        $this->db->delete("home_link");

    }



    function delete_row_description($id)

    {



        $this->db->where('id', $id);

        $this->db->delete("home_row_description");

    }

}

