<?php

use app\libraries\FcmClient;

/**
 * @property Messaging_model $Messaging_model
 */
class Push_notification_model extends MY_Model
{
    public $title;
    public $body;
    public $route = '/notification';

    private $client;

    public function create_batch_payload_messaging_on_maintenance($output)
    {
        $batches = [];

        foreach ($output as &$params) {
            $messaging_title = lang('push_messages.maintenance_registered_title', ["main_product_name" => $params['main_product_name']]);
            $messaging_content = lang('push_messages.maintenance_registered_body', ["sub_product_name" => $params['sub_product_name'], "sub_product_years_of_maintenance" => $params["sub_product_years_of_maintenance"]]);
            array_push($batches, [
                'title' => $messaging_title,
                'published_at' => date('Y-m-d H:i:s'),
                'end_date' => date('Y-m-d H:i:s', strtotime('+1 year', time())),
                'content' => $messaging_content,
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' =>  $this->session->userdata('userid') ?? 0,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' =>  $this->session->userdata('userid') ?? 0,
            ]);

            $params['title'] = $messaging_title;
            $params['content'] = $messaging_content;
        }

        $this->load->model('Messaging_model');
        $this->Messaging_model->create_batch($batches);

        $this->batch_payload_maintenance($output);
    }

    private function payload_handler(&$res, $params)
    {
        $member_ids = array_key_exists('member_ids', $params) ? $params['member_ids'] : (array_key_exists('member_id', $params) ? [$params['member_id']] : null);

        $this->load->model('Member_model');
        $devices = $this->Member_model->get_members($member_ids, ['id', 'fcm_token'], ['fcm_token !=' => null]);

        $data = [
            'title' => $this->title,
            'body' => $this->body,
            'route' => $this->route,
            'data' => $params,
        ];

        if (!empty($devices)) {
            foreach ($devices as $device) {
                array_push($res, [
                    'data' => $data,
                    'token' => $device['fcm_token'],
                    // update sent flag after calling the method push_notification_sent
                    // 'model' => null,
                    // 'model_id' => null,
                    // 'sent_field' => n,
                    // 'tenant_no' => $tenant_no
                ]);
            }
        }
    }

    public function batch_payload_maintenance($batches)
    {
        $res = $this->session->tempdata('push_notification') ?? [];

        foreach ($batches as $params) {
            $this->title = $params['title'];
            $this->body = $params['content'];
            $this->payload_handler($res, $params);
        };

        $this->session->set_tempdata('push_notification', $res);
    }

    public function payload($params)
    {
        $res = $this->session->tempdata('push_notification') ?? [];

        $this->payload_handler($res, $params);

        $this->session->set_tempdata('push_notification', $res);
    }

    public function post_controller_hook_send()
    {
        $items = $this->session->tempdata('push_notification');

        if (!empty($items)) {
            foreach ($items as $item) {
                // if ($item['model'] && $item['model_id']) {
                //     $this->ci->load->model($item['model'], 'm');
                //     $this->ci->m->push_notification_sent($item['tenant_no'], $item['model_id'], $item['sent_field']);
                // }
                $this->fcm_client_send($item['data'], $item['token']);
            }

            $this->session->unset_tempdata('push_notification');
        }
    }

    private function get_fcm_client()
    {
        if ($this->client == null) {
            $this->client = new FcmClient();
        }
    }

    private function fcm_client_send($data, $token)
    {
        $this->get_fcm_client();

        $resp = $this->client
            ->data([
                'click_action' => 'FLUTTER_NOTIFICATION_CLICK',
                'id' => '1',
                'status' => 'done',
                'data' => $data
            ])
            ->notification([
                'title' => $data['title'],
                'body' => $data['body'],
            ])
            ->token($token)
            ->send();

        log_message('debug', $resp->__toString());
    }
}
