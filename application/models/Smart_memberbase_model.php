<?php

use app\libraries\CSVClass;

class Smart_memberbase_model extends CI_Model
{

    public function build_datatables($draw, $total, $filtered, $data)
    {
        return [
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $filtered,
            "data" => $data,
        ];
    }

    public function get_id($id)
    {
        return $this->db->select("*")->from("tbl_members")->where("id", $id)->get()->row_array();
    }

    

    public function update_info($params, $id)
    {
        $this->db->where("id", $id)->update("tbl_members", $params);
    }

    public function listing($params, $card_id)
    {
        $fields = ['id', "account_id", "name", "surname", "given_name", "birthday", "english_name", "chinese_name", "email", "phone", "status", "level", "points", "CONCAT(name,english_name,chinese_name,given_name,surname) as display_name ,level_api, api,ref_id"];
        $start = $params["start"];
        $length = $params["length"];
        //$orders = $params['order'][0]["dir"];
        if ($params["columns"][$params['order'][0]['column']]["name"] == "name") {
            $orders["field"] = "display_name";
            $orders["dir"] = $params['order'][0]["dir"];
        } else {
            $orders["field"] = $params["columns"][$params['order'][0]['column']]["name"];
            $orders["dir"] = $params['order'][0]["dir"];
        }

        $search = $params["search"]["value"];
        $levels = $this->get_levels($card_id);
        $result = $this->getFromDBFilter("tbl_members", $fields, $start, $length, $orders, $search, null, null, $card_id);
        $data = [];
        foreach ($result as $index => $row) {
            $name = "";
            switch (true) {
                case $row['name'] !== "":
                    $name = $row["name"];
                    break;
                case $row['english_name'] !== "":
                    $name = $row['english_name'];
                    break;
                case $row['chinese_name'] !== "":
                    $name = $row['chinese_name'];
                    break;
                case $row['surname'] !== "" || $row['given_name'] !== "":
                    $name = $row['given_name'] . " " . $row['surname'];
                    break;
                default:
                    $name = "No Information";
                    break;
            }
            if ($row['api'] == 1) {
                $level = $row['level_api'];
            } else {
                $level = $levels[$row['level']]["value"];
            }

            $data[] = [
                "id" => $row['id'],
                "account_id" => $row['api'] == 1 ? "" : $row["account_id"],
                "name" => $name,
                "email" => $row['email'],
                "phone" => $row['phone'],
                "level" => $level,
                "points" => $row['points'],
                "status" => $row['status'],
                "birthday" => $row["birthday"],
                "view" => "<button class='btn btn-primary view' data-id='" . $row['id'] . "'>View</button>",
                "edit" => "<a class='btn btn-success' href='/merchant/smart_memberbase/edit/" . $row['id'] . "'>Edit</a>",
                "del" => $row['api'] == 1 ? "" : "<button class='btn btn-danger delete' data-id='" . $row['id'] . "'>Void</button>"
            ];
        }
        return $data;
    }

    public function getFromDBFilter($table, $fields, $start, $length, $orders, $search, $joins = [], $keywordfield = "", $pass_id)
    {
        $this->db->distinct();
        $this->db->select(implode(",", $fields));
        $this->db->from($table);
        $this->db->where("pass_id", $pass_id);

        if ($search !== "") {
            $this->db->group_start();
            $this->db->or_like("account_id", $search);
            $this->db->or_like("surname", $search);
            $this->db->or_like("given_name", $search);
            $this->db->or_like("english_name", $search);
            $this->db->or_like("chinese_name", $search);
            $this->db->or_like("email", $search);
            $this->db->or_like("phone", $search);
            $this->db->group_end();
        }
        //$this->db->order_by("account_id", $orders);
        $this->db->order_by($orders['field'], $orders['dir']);
        $this->db->limit($length, $start);
        return $this->db->get()->result_array();
    }

    public function filteredCounter($params, $pass_id)
    {

        $this->db->select("*");
        $this->db->from("tbl_members");
        $this->db->where("pass_id", $pass_id);
        $search = $params["search"]['value'];
        if ($search !== "") {
            $this->db->group_start();
            $this->db->or_like("account_id", $search);
            $this->db->or_like("surname", $search);
            $this->db->or_like("given_name", $search);
            $this->db->or_like("english_name", $search);
            $this->db->or_like("chinese_name", $search);
            $this->db->or_like("email", $search);
            $this->db->or_like("phone", $search);
            $this->db->group_end();
        }
        return $this->db->count_all_results();
    }

    public function totalCount($pass_id)
    {
        return $this->db->from("tbl_members")->where("pass_id", $pass_id)->count_all_results();
    }


    /*
    public function listing($params, $card_ids)
    {
        $fields = ["id", "pass_id", "account_id", "name", "gender", "phone", "email", "address"];
        $start = $params["start"];
        $length = $params['length'];

        $orders = [];

        foreach ($params['order'] as $order) {

            $colIdx = $order['column'];

            $colName = $params['columns'][$colIdx]['data'];

            $dir = strtoupper($order['dir']);

            $orders[] = "m" . "." . $colName . ' ' . $dir;
        }

        $searchVal = trim($params['search']['value']);
        $search = [];
        $joins = [];
        $data = [];


        foreach ($params['columns'] as $column) {

            if ($column['searchable'] === "true") {
                if ($column['data'] == "pass_id") {
                    $search["m" . "." . $column['data']] = $column['search']['value'];
                } else {
                    $search["m" . "." . $column['data']] = $searchVal;
                }
            }
        }
        $not_empty = false;
        foreach ($search as $s) {
            if ($s != "") {
                $not_empty = true;
            }
        }
        if ($not_empty == false) {
            $search = [];
        }



        $result = $this->getFromDBFilter("_members m", $fields, $start, $length, $orders, $search, $joins, "pass_id", $card_ids);

        foreach ($result as $row) {
            $data[] = [
                "id" => $row['id'],
                "pass_id" => $row["pass_id"],
                "account_id" => $row['account_id'],
                "name" => $row['name'],
                "gender" => $row['gender'] == 1 ? "Male" : "Female",
                "phone" => $row["phone"],
                "email" => $row['email'],
                'address' => $row['address'],
                "view" => "<button class='btn btn-primary view' data-id='" . $row['id'] . "'>View</button>",
                "edit" => "<a class='btn btn-primary' href='/merchant/smart_memberbase/edit/" . $row['id'] . "' data-id='" . $row['id'] . "'>Edit</a>&nbsp;<button class='btn btn-danger delete' data-id=" . $row['id'] . ">Del</button>"
            ];
        }
        return $data;
    }

    public function getFromDBFilter($table, $fields, $start, $length, $orders, $search, $joins = [], $keywordfield = "", $card_ids)
    {
        $this->db->distinct();
        $this->db->select(implode(', ', $fields));
        $this->db->from($table);
        $this->db->where_in("pass_id", $card_ids);
        $this->db->where("status", "valid");


        if (!empty($search)) {
            $this->db->group_start();

            foreach ($search as $k => $v) {
                if ($v !== '') {

                    if ($k === 'pass_id') {
                        $this->db->like([$k => $v]);
                    }

                    $this->db->or_like([$k => $v], 'none');
                }
            }

            $this->db->group_end();
        }

        $this->db->order_by(implode(', ', $orders));
        if ($start > -1) {
            $this->db->limit($length, $start);
        }

        return $this->db->get()->result_array();
    }

    public function filteredCounter($params, $card_ids)
    {
        $start = $params['start'];
        $length = $params["length"];
        $search = [];
        $searchVal = trim($params['search']['value']);
        $this->db->select("*");
        $this->db->from("tbl_members m");
        $this->db->where_in("pass_id", $card_ids);
        $this->db->where("status", "valid");

        foreach ($params['columns'] as $column) {

            if ($column['searchable'] === "true") {
                if ($column['data'] == "pass_id") {
                    $search["m" . "." . $column['data']] = $column['search']['value'];
                } else {
                    $search["m" . "." . $column['data']] = $searchVal;
                }
            }
        }
        $not_empty = false;
        foreach ($search as $s) {
            if ($s != "") {
                $not_empty = true;
            }
        }
        if ($not_empty == false) {
            $search = [];
        }
        if (!empty($search)) {
            $this->db->group_start();

            foreach ($search as $k => $v) {
                if ($v !== '') {
                    if ($k === 'pass_id') {
                        $this->db->like([$k => $v]);
                    }

                    $this->db->or_like([$k => $v], 'none');
                }
            }

            $this->db->group_end();
        }
        return count($this->db->get()->result_array());
    }

    function totalCount($card_ids)
    {

        return $this->db->from('tbl_members')->where_in("pass_id", $card_ids)->where("status", "valid")
            ->count_all_results();
    }

    */

    public function delete($id)
    {
        $this->db->set("status", "void")->where('id', $id)->update("tbl_members");
    }

    public function listing_pass_member($params)
    {
        $this->db->select("*")->from("tbl_members")->where("pass_id", $params['pass_id']);
        if ($params['keyword'] != "") {
            $this->db->group_start();
            $this->db->or_like("name", $params['keyword']);
            $this->db->or_like("english_name", $params['keyword']);
            $this->db->or_like("chinese_name", $params['keyword']);
            $this->db->or_like("surname", $params['keyword']);
            $this->db->or_like("given_name", $params['keyword']);
            $this->db->or_like("phone", $params['keyword']);
            $this->db->or_like("email", $params['keyword']);
            $this->db->group_end();
        }
        $result = $this->db->order_by("account_id")->get()->result_array();
        foreach ($result as $idx => $res) {
            switch (true) {
                case $res['name'] !== "":
                    $result[$idx]['name'] = $res['name'];
                    break;
                case $res['english_name'] !== "":
                    $result[$idx]['name'] = $res['english_name'];
                    break;
                case $res['chinese_name'] !== "":
                    $result[$idx]['name'] = $res['chinese_name'];
                    break;
                case $res['surname'] !== "" || $res['given_name'] !== "":
                    $result[$idx]['name'] = $res['given_name'] . " " . $res['surname'];
                    break;
                default:
                    $result[$idx]['name'] = "No Information";
                    break;
            }
        }
        return $result;
    }

    public function adding_message($params)
    {
        $this->db->insert("tbl_testing_notification", $params);
    }

    public function export_csv($card_ids)
    {
        $template = $this->db->select("*")->from("tbl_virtual_passes")->where("id", $card_ids)->get()->row_array();
        $cards_info = $this->db->select("*")->from("tbl_members")->where_in("pass_id", $card_ids)->where("status", "valid")->order_by('pass_id')->get()->result_array();
        $all_levels = $this->db->select("*")->from('tbl_virtual_passes_levels')->where("pass_id", $card_ids)->get()->result_array();
        $levels = [];
        foreach ($all_levels as $level) {
            $levels[$level['id']] = $level;
        }
        foreach ($cards_info as $index => $r) {
            if ($r['api'] == 1) {
                $cards_info[$index]['level'] = $r['level_api'];
            } else {
                $cards_info[$index]['level'] = $levels[$r['level']]["value"];
            }
        }
        $filename = "Membership_" . $template['project_name'] . "_export_" . date("H-i d-m-Y") . ".csv";
        $e = new CSVClass();
        return $e->export([
            "account_id" => ["value" => "account_id"],
            "name" => ["value" => "name"],
            "surname" => ["value" => "surname"],
            "given_name" => ["value" => "given_name"],
            "english_name" => ["value" => "english_name"],
            "chinese_name" => ['value' => "chinese_name"],
            "gender" => ["value" => "gender"],
            "birthday" => ["value" => "birthday"],
            "age" => ["value" => "age"],
            "nationality" => ["value" => "nationality"],
            "phone" => ["value" => "phone"],
            "email" => ["value" => "email"],
            "address" => ["value" => "address"],
            "industry" => ["value" => "industry"],
            "industry_2" => ["value" => "industry_2"],
            "job_title" => ["value" => "job_title"],
            "company" => ["value" => "company"],
            "salary" => ["value" => "salary"],
            "level" => ["value" => "level"],
            "points" => ["value" => "points"],
            "status" => ["value" => "status"],

        ], $cards_info, $filename);
    }

    public function get_levels($pass_id)
    {
        $result =  $this->db->select("*")->from("tbl_virtual_passes_levels")->where("pass_id", $pass_id)->where("display", 1)->get()->result_array();
        $data = [];
        foreach ($result as $res) {
            $data[$res['id']] = $res;
        }
        return $data;
    }


    public function check_user_ref($pass_id, $level_id)
    {

        $result = $this->db->from("tbl_members")->where("pass_id", $pass_id)->where("level", $level_id)->get()->num_rows();
        return $result;
    }

    public function get_update_user($id)
    {
        return $this->db->select("tbl_members.*,tbl_google_registration.object_uid")->from("tbl_members")->where("pass_id", $id)->where("platform", "google")->join("tbl_google_registration", "tbl_google_registration.user_id = tbl_members.id")->get()->result_array();
    }

    public function get_user_token($id)
    {
        return $this->db->select("*")->from("tbl_temp_registration")->where("serialnumber", $id)->get()->row_array();
    }

    public function get_user_object($id)
    {
        return $this->db->select("*")->from("tbl_google_registration")->where("user_id", $id)->get()->row_array();
    }



    public function simple_search($params)
    {
        $this->db->distinct();
        $this->db->select("*")->from("tbl_members");
        if ($params['keyword'] !== "") {
            $this->db->group_start();
            $this->db->or_like("name", $params['keyword']);
            $this->db->or_like("surname", $params['keyword']);
            $this->db->or_like("given_name", $params['keyword']);
            $this->db->or_like("english_name", $params['keyword']);
            $this->db->or_like("chinese_name", $params['keyword']);
            $this->db->or_like("phone", $params['keyword']);
            $this->db->or_like("email", $params['keyword']);
            $this->db->or_like("company", $params['keyword']);
            $this->db->or_like("job_title", $params['keyword']);
            $this->db->or_like("industry", $params['keyword']);
            $this->db->or_like("industry_2", $params['keyword']);
            $this->db->group_end();
        }
        $this->db->where("pass_id", $params['id']);
        $this->db->where("api", 0);
        return $this->db->order_by("account_id", "desc")->get()->result_array();
    }
}
