<?php

class Coupon_model extends CI_Model
{


    public function get_coupon($id)
    {
        return $this->db->select('*')->from("tbl_coupons")->where("id", $id)->get()->row_array();
    }

    public function get_coupons($id)
    {
        return $this->db->select('*')->from("tbl_coupons")->where("user_id", $id)->where("display", 1)->get()->result_array();
    }

    public function insert_coupon($params)
    {
        $this->db->insert("tbl_coupons", $params);
        return $this->db->insert_id();
    }

    public function update_coupon($params, $id)
    {
        $this->db->update("tbl_coupons", $params, ['id' => $id]);
    }

    public function delete($id)
    {
        $this->db->update("tbl_coupons", ["display" => 0], ["id" => $id]);
    }

    public function insert_locations($params)
    {
        $this->db->insert("tbl_coupons_locations", $params);
    }

    public function update_locations($params, $id)
    {
        $this->db->update("tbl_coupons_locations", $params, ["id" => $id]);
    }

    public function get_locations_setting($id)
    {
        return $this->db->select("*")->from("tbl_coupons_locations")->where("id", $id)->get()->row_array();
    }

    public function get_form_config($id)
    {
        return $this->db->select("*")->from("tbl_coupons_e_form_config")->where("id", $id)->get()->row_array();
    }

    public function insert_form_config($request)
    {
        $this->db->insert("tbl_coupons_e_form_config", $request);
    }

    public function update_form_config($request, $id)
    {
        $this->db->update("tbl_coupons_e_form_config", $request, ["id" => $id]);
    }

    public function get_layout_config($id)
    {
        return $this->db->select("*")->from("tbl_coupons_data_config")->where("id", $id)->get()->row_array();
    }

    public function insert_layout_config($request)
    {
        $this->db->insert("tbl_coupons_data_config", $request);
    }

    public function update_layout_config($request, $id)
    {
        $this->db->update("tbl_coupons_data_config", $request, ["id" => $id]);
    }

    public function check_duplicate_code($code)
    {
        $result = $this->db->select("*")->from("tbl_coupons")->where("coupon_code", $code)->get()->num_rows();
        if ($result != 0) {
            return 0;
        } else {
            return 1;
        }
    }

    public function coupon_range($id)
    {
        $result = $this->db->select("id")->from("tbl_coupons")->where("user_id", $id)->get()->result_array();
        $data = [];
        foreach ($result as $res) {
            $data[] = $res['id'];
        }
        return $data;
    }
}
