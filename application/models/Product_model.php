<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends MY_Model
{
    public $tbl = 'products';
    public $type;

    const TYPE_MACHINE = 'machine';
    const TYPE_CASE = 'case';
    const TYPE_FILTER = 'filter';
    const TYPE_MOTHERBOARD = 'motherboard';

    public function table_view($limit, $per_page, $searchValue = "")
    {
        $this->load->library('pagination');
        $this->config->load('pagination');

        $limits = [10, 25, 50, 100];
        // Show per page
        $limit = $limit ?? $limits[0];
        // Page number
        $per_page = $per_page ?? 0;
        $total_rows = $this->table_view_count($searchValue);
        // $this->pagination->initialize($config);

        $users = $this->table_view_get([
            'limit' => $limit,
            'offset' => $per_page
        ], $searchValue);

        $config = $this->config->item('pagination');
        $config['base_url'] = base_url('admin_user');
        $config['total_rows'] = $total_rows;
        $this->pagination->initialize($config);


        $cur_page = $per_page + 1;
        $next_page = ($cur_page + 1) * $limit > $total_rows ? $cur_page : $cur_page + 1;
        $max_page = floor($total_rows % $limit > 0 ? $total_rows / $limit + 1 : $total_rows / $limit);

        $prev_page = $cur_page == 1 ? 1 : $cur_page - 1;
        return [
            'total_rows' => $total_rows,
            'limit' => (int) $limit,
            'per_page' => $this->pagination->per_page,
            'per_pages' => $limits,
            'min_limit' => $limits[0],
            'max_limit' => $limits[count($limits) - 1],
            'prev_page' => $prev_page,
            'curr_page' => $cur_page,
            'next_page' => $next_page,
            'max_page' => $max_page,
            'prev_link' => '#',
            'next_link' => '#',
            'items' => $users,
        ];
    }

    private function table_view_get_criteria($searchValue)
    {
        $this->db->from($this->table_prefix . $this->tbl);
        // $this->db->join('rbac_users_roles ur', 'ur.user_id = u.id');
        // if (count($exclude_roles) > 0) {
        //     $this->db->where_not_in('ur.role_id', $exclude_roles);
        // }
        // $this->db->where_not_in('ur.role_id', [1]);
        // $this->db->where('u.deleted_at IS NULL', null, false);

        if (trim($searchValue)) {
            $performSearchInFields = ['name'];

            $concatSegment = implode(', " ",', $performSearchInFields);
            $concatWhere = sprintf('CONCAT(%s) LIKE ', $concatSegment) . '"%' . $searchValue . '%"';
            $this->db->where($concatWhere, null, false);
        }

        $this->db->where('type', $this->type);
        $this->db->where('deleted_at IS NULL', null, false);
    }

    /*
     * Get all users
     */
    public function table_view_get($params = array(), $searchValue)
    {
        $this->table_view_get_criteria($searchValue);

        if (isset($params) && !empty($params)) {
            $this->db->limit($params['limit'], $params['offset'] > 0 ? $params['limit'] * $params['offset'] : $params['offset']);
        }

        $res = $this->db
            ->order_by('created_at', 'desc')
            ->get()->result_array();

        // log_message('debug', $this->db->last_query());
        return $res;
    }

    /*
     * Get all users count
     */
    public function table_view_count($search)
    {
        $this->table_view_get_criteria($search);

        $count = $this->db->count_all_results();
        $this->db->reset_query();
        return $count;
    }

    public function add_purchase_filter($params)
    {
        $id = $this->insertDB($this->database, $this->tbl, [
            'name' => $params['name'],
            'published_at' => $params['published_at'],
            'end_date' => $params['end_date'],
            'years_of_maintenance' => $params['years_of_maintenance'],
            'price' => $params['price'],
        ], false, [
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' =>  $this->session->userdata('userid'),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' =>  $this->session->userdata('userid'),
        ]);

        return $id;
    }

    public function get_all_purchase_filters()
    {
        return $this->getAll($this->database, $this->tbl, [], ['deleted_at' => null, 'type' => 'filter']);
    }

    public function get_product($id, $select_fields = [])
    {
        return $this->getOne($this->database, $this->tbl, $select_fields, $id);
    }

    public function get_all_product($type = null, $not = false)
    {
        $this->db = $this->load->database($this->database, true);
        $this->db->from($this->table_prefix . $this->tbl);

        if ($type) {
            $this->db->where('type' . ($not ? ' !=' : ''), $type);
        }

        return $this->db
            ->where('published_at <= NOW()', null, false)
            ->where('end_date > NOW()', null, false)
            ->where('deleted_at IS NULL', null, false)
            ->order_by('created_at', 'desc')
            ->get()->result_array();
    }


    public function get_children_products($id, $select_fields = [])
    {
        return $this->getAll($this->database, $this->tbl, $select_fields, ['parent_id' => $id]);
    }

    public function edit_purchase_filter($params, $id)
    {
        $update_params = [
            'name' => $params['name'],
            'published_at' => $params['published_at'],
            'end_date' => $params['end_date'],
            'years_of_maintenance' => $params['years_of_maintenance'],
            'price' => $params['price'],
        ];
        $this->Product_model->saveDB($this->Product_model->database, $this->Product_model->tbl, $update_params, $id, 'id', [
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => $this->session->userdata('userid'),
        ]);
    }
}
