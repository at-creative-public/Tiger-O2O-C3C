<?php

use Booking_model as GlobalBooking_model;

defined("BASEPATH") or exit("No direct script access allowed");

require_once __DIR__ . '/Booking_model.php';
/** 
 * Booking model
 * 
 * @property MY_Model $this
 * @property MY_IndexCrudModel $this
 * @property Booking_model $this
 * @property Booking_timeslot_model $Booking_timeslot_model
 * @property Booking_calendar_timeslot_model $Booking_calendar_timeslot_model
 * 
 * ALTER TABLE `tblbooking_record` ADD `room_id` INT DEFAULT NULL AFTER `remark`;
 */
class Booking_timeslot_model extends Booking_model
{
    public $has_displayorder = false;
    public $table_prefix = '';
    public $tbl = 'booking_timeslot';
    public $short = 'booking_timeslot';
    public $database = 'default';
    public function __construct()
    
    {
        parent::__construct();
    }

    public function get_timeslot_count($where)
    {
        $this->load->model('Booking_model');
        $this->db->from($this->table_prefix . $this->tbl . ' ' . $this->short);
        $this->where_criteria($where);
        $this->db->select(implode(',', [$this->short . ".*"]));
        return $this->db->get()->num_rows();
    }

    public function get_timeslot($where)
    {
        $this->load->model('Booking_model');
        $this->db->from($this->table_prefix . $this->tbl . ' ' . $this->short);
        $this->where_criteria($where);
        $this->db->select(implode(',', [$this->short . ".*"]));
        return $this->db->get()->row_array();
    }

    public function create_timeslot($request)
    {
        // $booking_datetime = $request['booking_datetime'] ?? null;
        $timeslot_id = $request['timeslot_id'];
        $service = $request['service'];
        if ($timeslot_id == "0") {
            $booking_date = $request['booking_date'];
            $booking_time = date('g:i a', strtotime($request['booking_time']));
            $service_group = $request['service_group'] ?? '';

            $timeslot_id = $this->get_or_create_timeslot_id($booking_date, $booking_time, $service, $service_group);

            $request['booking_date'] = $booking_date;
            $request['booking_time'] = $booking_time;
        } else {
            $timeslot = $this->get_id($timeslot_id);

            $request['booking_date'] = $timeslot['booking_date'];
            $request['booking_time'] = $timeslot['booking_time'];
        }
        $request['timeslot_id'] = $timeslot_id;

        return $request;
    }

    public function update_timeslot($request)
    {
        // $booking_datetime = $request['booking_datetime'] ?? null;
        $booking_date = $request['booking_date'];
        $booking_time = $request['booking_time'];
        $service = $request['service'];
        $service_group = $request['service_group'] ?? '';
        // $doctor_id = $request['doctor_id'] ?? '';

        $timeslot_id = $this->get_or_create_timeslot_id($booking_date, $booking_time, $service, $service_group);
        // if ($timeslot_id == "0") {
        //     $booking_date = $request['booking_date'];
        //     $booking_time = date('g:i a', strtotime($request['booking_time']));

        //     $request['booking_date'] = $booking_date;
        //     $request['booking_time'] = $booking_time;
        // } else {
        //     $timeslot = $this->get_id($timeslot_id);

        //     $request['booking_date'] = $timeslot['booking_date'];
        //     $request['booking_time'] = $timeslot['booking_time'];
        // }

        // $update_params = [];
        // if (!empty($doctor_id)) $update_params['doctor_id'] = $doctor_id;
        // if (!empty($update_params)) $this->update($update_params, $timeslot_id);
        $request['timeslot_id'] = $timeslot_id;

        return $request;
    }

    public function get_or_create_timeslot_id($booking_date, $booking_time, $service, $service_group)
    {
        $timeslot_id = 0;
        $timeslot = $this->get_timeslot([
            'booking_date' => $booking_date,
            'booking_time' => $booking_time,
            'service' => $service,
        ]);
        if (!empty($timeslot)) {
            $timeslot_id = $timeslot['id'];
        } else {
            $this->load->model('Booking_calendar_timeslot_model');
            $res = $this->Booking_calendar_timeslot_model->get_entity(null, [], ['date' => $booking_date]);

            $timeslot_id = $this->add([
                "booking_date" => $booking_date,
                "booking_time" => $booking_time,
                "booking_time_interval" => $res['time_interval'] ?? 30,
                "service_group" => $service_group,
                "service" => $service,
                "remark" => "",
                "room_id" => 1,
                "create_date" => date('Y-m-d H:i:s')
            ]);
        }

        return $timeslot_id;
    }

    public function get_timeslot_with_booking($where = [], $group_by = [], $db_callback = null)
    {
        $this->load->model('Booking_model');
        $this->db
            ->from($this->table_prefix . $this->tbl . ' ' . $this->short)
            ->join(
                $this->Booking_model->table_prefix . $this->Booking_model->tbl . ' ' . $this->Booking_model->short,
                $this->short . '.id = ' . $this->Booking_model->short . '.timeslot_id'
            );
        $this->where_criteria($where, $db_callback);
        $this->group_by_criteria($group_by);
        $this->db->select(implode(',', [
            $this->short . ".*",
            // "CONCAT(" . $this->short . ".booking_date" . ", ' - ', " . $this->short . ".booking_time" . ") AS booking_datetime",
            $this->Booking_model->short . ".id AS booking_id",
            $this->Booking_model->short . ".timeslot_id",
            $this->Booking_model->short . ".name",
            $this->Booking_model->short . ".phone",
            $this->Booking_model->short . ".email",
            $this->Booking_model->short . ".age",
            $this->Booking_model->short . ".gender",
            $this->Booking_model->short . ".remark"
        ]));
        return $this->db->get()->row_array();
    }

    public function get_timeslots_with_booking($where = [], $group_by = [], $db_callback = null)
    {
        $this->load->model('Booking_model');
        $this->db
            ->from($this->table_prefix . $this->tbl . ' ' . $this->short)
            ->join(
                $this->Booking_model->table_prefix . $this->Booking_model->tbl . ' ' . $this->Booking_model->short,
                $this->short . '.id = ' . $this->Booking_model->short . '.timeslot_id'
            );
        $this->where_criteria($where, $db_callback);
        $this->group_by_criteria($group_by);
        $this->db->select(implode(',', [
            $this->short . ".*",
            // "CONCAT(" . $this->short . ".booking_date" . ", ' - ', " . $this->short . ".booking_time" . ") AS booking_datetime",
            $this->Booking_model->short . ".id AS booking_id",
            $this->Booking_model->short . ".timeslot_id",
            $this->Booking_model->short . ".name",
            $this->Booking_model->short . ".phone",
            $this->Booking_model->short . ".email",
            $this->Booking_model->short . ".age",
            $this->Booking_model->short . ".gender",
            $this->Booking_model->short . ".remark",
            // $this->Booking_model->short . ".vaccine_type",
            // $this->Booking_model->short . ".service_group"
        ]));
        return $this->db->get()->result_array();
    }

    public function get_timeslot_with_room($timeslot_id)
    {
        $this->load->model('Room_model');
        $this->db
            ->from($this->table_prefix . $this->tbl . ' ' . $this->short)
            ->join(
                $this->Room_model->table_prefix . $this->Room_model->tbl . ' ' . $this->Room_model->short,
                $this->short . '.room_id = ' . $this->Room_model->short . '.id'
            )
            ->where($this->short . '.id', $timeslot_id)
            ->select(implode(',', [$this->short . ".*"]));
        return $this->db->get()->row_array();
    }

    public function get_timeslots_with_room($service)
    {
        $this->load->model('Room_model');
        $this->db
            ->from($this->table_prefix . $this->tbl . ' ' . $this->short)
            ->join(
                $this->Room_model->table_prefix . $this->Room_model->tbl . ' ' . $this->Room_model->short,
                $this->short . '.room_id = ' . $this->Room_model->short . '.id'
            )
            ->where($this->short . '.service', $service)
            ->select(implode(',', [$this->short . ".*", $this->Room_model->short . '.name AS room_name']));
        return $this->db->get()->result_array();
    }
}
