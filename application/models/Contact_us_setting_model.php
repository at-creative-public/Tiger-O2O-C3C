<?php
defined("BASEPATH") or exit("No direct scripts access allowed");

class Contact_us_setting_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get()
    {
        return $this->db->select("*")
            ->from("tbl_contact_us_map")
            ->order_by("displayorder")
            ->get()
            ->result_array();
    }

    public function get_id($id)
    {
        return $this->db->select('*')
            ->from("tbl_contact_us_map")
            ->where('id', $id)
            ->get()
            ->result_array();
    }

    public function get_front()
    {
        return $this->db->select("*")
            ->from("tbl_contact_us_map")
            ->order_by('displayorder')
            ->where("display", '1')
            ->get()
            ->result_array();
    }

    public function add($params)
    {
        $this->db->insert("tbl_contact_us_map", $params);
    }

    public function update($params, $id)
    {
        $this->db->where("id", $id)
            ->update("tbl_contact_us_map", $params);
    }

    public function delete($id)
    {
        $this->db->where('id', $id)
            ->delete("tbl_contact_us_map");
    }

    public function update_rank($id, $rank)
    {
        $data = array(
            'displayorder' => $rank
        );

        $this->db->where('id', $id);
        $this->db->update('tbl_contact_us_map', $data);
    }
}
