<?php
defined("BASEPATH") or exit("No direct access allowed");

class Payment_management_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add($paypal_data, $user_data)
    {
        $this->db->set('user_id', $user_data['user_id']);
        $this->db->set("paymentId", $paypal_data['paymentId']);
        $this->db->set("amount", $user_data['amount']);
        $this->db->set("token", $paypal_data['token']);
        $this->db->set("payerId", $paypal_data['PayerID']);
        $this->db->set('date', $user_data['date']);
        $this->db->set("purpose", $user_data['purpose']);
        $this->db->set('status',$user_data['status']);
        $this->db->insert("tbl_payment_record");
    }

    public function add_failed($params)
    {
        $this->db->insert("tbl_payment_record", $params);
    }

    public function get()
    {
        return $this->db->select("*")
            ->from("tbl_payment_record")
            ->get()
            ->result_array();
    }

    public function get_id($id)
    {
        return $this->db->select("*")
            ->from("tbl_payment_record")
            ->where("id", $id)
            ->get()
            ->result_array();
    }

    public function get_user_id($id)
    {
        return $this->db->select("*")
            ->from("tbl_payment_record")
            ->where("user_id", $id)
            ->get()
            ->result_array();
    }

    
}
