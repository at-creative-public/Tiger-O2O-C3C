<?php
class Questions_Model extends CI_Model{
        
        function insert($arr)
        {
                
                $this->db->insert('_questions', $arr);
                return $this->db->insert_id();
                
        }
        
        function update($id, $arr)
        {
        
                $this->db->where('id', $id);
                $this->db->update('_questions', $arr);
        
        }
        
        function get()
        {       

                    $this->db->select('questions.id,questions.type_id,questions.question,questions.displayorder, questions.updated ');
                    
                    $this->db->order_by("type_id", "asc");
                    $this->db->order_by("displayorder","asc");
                    $this->db->from('_questions questions');
                    $this->db->where('deleted', 0);
                    $query=$this->db->get();

                return $query->result();
        }
        function getbytypeid($id){
                $this->db->select('questions.id,questions.type_id,questions.question,questions.displayorder, questions.updated ');
                    
                    $this->db->order_by("type_id", "asc");
                    $this->db->order_by("displayorder","asc");
                    $this->db->from('_questions questions');
                    $this->db->where('deleted', 0);
                    $this->db->where('type_id', $id);
                    $query=$this->db->get();

                return $query->result();
        }
        function get_id($id){
                $this->db->where('id', $id);
                $query = $this->db->get('_questions');
                return $query->result();
        }
        
        function get_lang($lang)
        {
                
                $this->db->where('lang', $lang);
                $query = $this->db->get('_questions');
                return $query->result();
                
        }
        
        function delete($id)
        {
                $arr=array();
                $arr['deleted'] =1;
                $this->db->where('id', $id);
                $this->db->update('_questions', $arr);
        }
        function update_rank($id, $rank)
        {
                $data = array(
                        'displayorder' => $rank
                );

                $this->db->where('id', $id);
                $this->db->update('_questions', $data);

        }
}
