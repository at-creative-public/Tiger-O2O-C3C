<?php

class Api_membership_model extends CI_Model
{
    protected $allow_variables = [
        "ref_id", "name", "surname", "given_name", "english_name", "chinese_name", "gender", "birthday", "age", "nationality", "phone", "email", "address", "industry", "work_information_industry", "job_title", "company", "salary", "status", "level", "points", "platform"
    ];

    protected $must_variables = [
        "ref_id", "email", "platform"
    ];

    protected $update_variable = [
        "name", "surname", "given_name", "english_name", "chinese_name", "gender", "birthday", "age", "nationality", "phone", "email", "address", "industry", "work_information_industry", "job_title", "company", "salary", "status", "level", "points"
    ];


    public function __construct()
    {
        parent::__construct();
    }

    public function get_id($access_resource, $ref_id)
    {

        if ($ref_id == "") {
            $res = [
                "error" => true,
                "status" => "400",
                "reason" => "Invalid Token"
            ];
            echo json_encode($res);
            exit;
        }
        $result = $this->db->select('*')->from("tbl_members")->where("pass_id", $access_resource['pass_id'])->where("ref_id", $ref_id)->where("api", 1)->get()->row_array();
        if ($result == null) {
            $res = [
                "error" => true,
                "status" => "401",
                "reason" => "Resource not found"
            ];
            echo json_encode($res);
            exit;
        } else {
            $info = [
                "ref_id" => $result['ref_id'],
                "name" => $result['name'],
                "surname" => $result['surname'],
                "given_name" => $result['given_name'],
                "english_name" => $result['english_name'],
                "chinese_name" => $result['chinese_name'],
                "gender" => $result['gender'] == 1 ? "male" : "female",
                "birthday" => $result['birthday'],
                "age" => $result['age'],
                "nationality" => $result['nationality'],
                "phone" => $result['phone'],
                "email" => $result['email'],
                "address" => $result['address'],
                "industry" => $result['industry'],
                "work_information_industry" => $result['industry_2'],
                "job_title" => $result['job_title'],
                "company" => $result['company'],
                "salary" => $result['salary'],
                "status" => $result['status'],
                "level" => $result["level_api"],
                "points" => $result['points'],
                "platform" => $result['platform']
            ];
            return $info;
        }
    }

    public function insert($access_resource, $params)
    {

        if (count($this->must_variables) != count(array_intersect_key($params, array_flip($this->must_variables)))) {
            $res = [
                "error" => true,
                "status" => "400",
                "reason" => "Missing required variable"
            ];
            echo json_encode($res);
            exit;
        }
        $exist_records = $this->db->select("ref_id")->from('tbl_members')->where('pass_id', $access_resource['pass_id'])->get()->result_array();
        
        $exist_info = [];
        foreach ($exist_records as $r) {
            $exist_info[] = $r['ref_id'];
        }
        if (!in_array($params['ref_id'], $exist_info)) {
            if (count($params) == count(array_intersect_key($params, array_flip($this->allow_variables)))) {
                $insert_info = [];
                foreach ($params as $key => $v) {
                    switch ($key) {
                        case "gender":
                            if (!($v == "0" || $v == "1")) {
                                $res = [
                                    "error" => true,
                                    "status" => "400",
                                    "reason" => "Invalid gender value"
                                ];
                                echo json_encode($res);
                                exit;
                            }
                            $insert_info["gender"] = $v;
                            break;
                        case "work_information_industry":
                            $insert_info['industry_2'] = $v;
                            break;
                        case "email":
                            if (!filter_var($v, FILTER_VALIDATE_EMAIL)) {
                                $res = [
                                    "error" => true,
                                    "status" => "400",
                                    "reason" => "Invalid email format"
                                ];
                                echo json_encode($res);
                                exit;
                            }
                            $insert_info['email'] = $v;
                            break;
                        case "level":
                            $insert_info['level_api'] = $v;
                            break;
                        case "status":
                            if ($v == "valid" || $v == "void") {
                                $insert_info['status'] = $v;
                            } else {
                                $res = [
                                    "error" => true,
                                    "status" => "400",
                                    "reason" => "Invalid status value"
                                ];
                                echo json_encode($res);
                                exit;
                            }
                            break;
                        case "platform":
                            if ($v == "apple" || $v == "google") {
                                $insert_info['platform'] = $v;
                            } else {
                                $res = [
                                    "error" => true,
                                    "status" => "400",
                                    "reason" => "Invalid platform value"
                                ];
                                echo json_encode($res);
                                exit;
                            }
                            break;
                            /*case "points":
                            if (!is_int($v)) {
                                $res = [
                                    "error" => true,
                                    "status" => "400",
                                    "reason" => "Invalid points value"
                                ];
                                echo json_encode($res);
                                exit;
                            }
                            $insert_info['points'] = $v;
                            break;*/
                        default:
                            $insert_info[$key] = $v;
                            break;
                    }
                }
                $insert_info['api'] = 1;
                $insert_info['create_date'] = time();
                $hash = "";
                for ($i = 1; $i <= 8; $i++) {
                    $hash .= sha1($hash) . sha1($hash . time());
                }
                $insert_info['hash'] = $hash;
                $insert_info['pass_id'] = $access_resource['pass_id'];
                $this->db->insert("tbl_members", $insert_info);
                $result = [
                    "ref_id" => $insert_info['ref_id'],
                    "hash" => $hash,
                    "insert_id" => $this->db->insert_id(),
                    "platform" => $insert_info['platform']
                ];
                return $result;
            } else {
                $res = [
                    "error" => true,
                    "status" => "400",
                    "reason" => "Invalid variable"
                ];
                echo json_encode($res);
                exit;
            }
        } else {
            $res = [
                "error" => true,
                "status" => "400",
                "reason" => "Duplicated ref id received"
            ];
            echo json_encode($res);
            exit;
        }
    }

    public function update($access_resource, $params)
    {
        if (!isset($params['ref_id'])) {
            $res = [
                "error" => true,
                "status" => "400",
                "reason" => "Missing required variable"
            ];
            echo json_encode($res);
            exit;
        }
        $exist_record = $this->db->select("*")->from("tbl_members")->where("pass_id", $access_resource['pass_id'])->where("ref_id", $params['ref_id'])->get()->row_array();
        if (empty($exist_record)) {
            $res = [
                "error" => true,
                "status" => "404",
                "reason" => "Resource not found"
            ];
            echo json_encode($res);
            exit;
        }
        $ref_id = $params['ref_id'];
        unset($params['ref_id']);
        if (count($params) == count(array_intersect_key($params, array_flip($this->update_variable)))) {
            $update_info = [];
            foreach ($params as $key => $v) {
                switch ($key) {
                    case "gender":
                        if ($v > 1 || $v < 0) {
                            $res = [
                                "error" => true,
                                "status" => "400",
                                "reason" => "Invalid gender value"
                            ];
                            echo json_encode($res);
                            exit;
                        }
                        $update_info['gender'] = $v;
                        break;
                    case "work_information_industry":
                        $update_info['industry_2'] = $v;
                        break;
                    case "email":
                        if (!filter_var($v, FILTER_VALIDATE_EMAIL)) {
                            $res = [
                                "error" => true,
                                "status" => "400",
                                "reason" => "Invalid email format"
                            ];
                            echo json_encode($res);
                            exit;
                        }
                        $update_info['email'] = $v;
                        break;
                    case "level":
                        $update_info['level_api'] = $v;
                        break;
                    case "status":
                        if ($v == "valid" || $v == "void") {
                            $update_info['status'] = $v;
                        } else {
                            $res = [
                                "error" => true,
                                "status" => "400",
                                "reason" => "Invalid status value"
                            ];
                            echo json_encode($res);
                            exit;
                        }
                        break;
                        /*
                    case "points":
                        if (!is_int($v)) {
                            $res = [
                                "error" => true,
                                "status" => "400",
                                "reason" => "Invalid points value"
                            ];
                            echo json_encode($res);
                            exit;
                        }
                        $update_info['points'] = $v;
                        break;
                        */
                    default:
                        $update_info[$key] = $v;
                        break;
                }
            }
            $this->db->update("tbl_members", $update_info, ['pass_id' => $access_resource['pass_id'], "ref_id" => $ref_id]);
            return $this->db->select("*")->from("tbl_members")->where(["pass_id" => $access_resource['pass_id'], "ref_id" => $ref_id])->get()->row_array();
        } else {
            $res = [
                "error" => true,
                "status" => "400",
                "reason" => "Invalid variable"
            ];
            echo json_encode($res);
            exit;
        }
    }

    public function update_status($access_resource, $params)
    {
        if (!isset($params['status'])) {
            $res = [
                "error" => true,
                "status" => "400",
                "reason" => "Missing required variable"
            ];
            echo json_encode($res);
            exit;
        }
        if ($params['status'] == "valid" || $params['status'] == "void") {
            $this->db->update("tbl_members", ["status" => $params['status']], ["pass_id" => $access_resource['pass_id'], "ref_id" => $params['ref_id']]);
        } else {
            $res = [
                "error" => true,
                "status" => 400,
                "reason" => "Invalid status value"
            ];
            echo json_encode($res);
            exit;
        }
    }
}
