<?php
class Success_cases_Model extends CI_Model{

        private function _autofill ($arr)
        {
            $title = '';
            $title_en = '';
            if (array_key_exists('title', $arr)) {
                $title = trim($arr['title']);
            }
            if (array_key_exists('title_en', $arr)) {
                $title_en = trim($arr['title_en']);
            }
            if (($title !== "") && ($title_en !== "")) {
                return [
                    'zh' => $title,
                    'en' => $title_en,
                ];
            }
            if (($title === "") && ($title_en === "")) {
                return [
                    'zh' => $title,
                    'en' => $title_en,
                ];
            }
            if ($title === "") {
                return [
                    'zh' => $title_en,
                    'en' => $title_en,
                ];
            }
            return [
                'zh' => $title,
                'en' => $title,
            ];
        }

        function insert($arr)
        {
            $t = $this->_autofill($arr);
            $arr['title'] = $t['zh'];
            $arr['title_en'] = $t['en'];
            $this->db->insert('success_cases', $arr);
            return $this->db->insert_id();

        }
        
        function update($id, $arr)
        {
            $t = $this->_autofill($arr);
            $arr['title'] = $t['zh'];
            $arr['title_en'] = $t['en'];
            $this->db->where('id', $id);
            $this->db->update('success_cases', $arr);
        }
        
        function get($include_hidden = false)
        {       
            $this->db->select('*');
            $this->db->order_by("displayorder","asc");
            if (! $include_hidden) {
                $this->db->where('display', 1);
            }
            $this->db->from('success_cases');
            $query=$this->db->get();
            return $query->result();
        }

        function get_no(){
            $this->db->select('*');
            $this->db->order_by("displayorder","asc");
            $this->db->where('display', 1);
            $this->db->from('success_cases');
            $no = $this->db->count_all_results();
            return (int)ceil($no/4);
        }
        
        function get_id($id)
        {
            $this->db->where('id', $id);
            $query = $this->db->get('success_cases');
            return $query->result();
        }

        function get_partial($start,$length){
          return  $this->db->select("*")
                        ->order_by("displayorder",'asc')
                        ->limit($length,$start)
                        ->from('success_cases')
                        ->get()
                        ->result();

        }

        
        function get_lang($lang)
        {
            $this->db->where('lang', $lang);
            $query = $this->db->get('success_cases');
            return $query->result();
        }

        function search_by_uri($controller)
        {
            $this->db->where('uri', $controller);
            $query = $this->db->get('success_cases');
            return $query->row_array();
        }

        function delete($id)
        {
            $this->db->where('id', $id);
            $this->db->delete('success_cases');
        }
        function update_rank($id, $rank)
        {
            $data = array(
                'displayorder' => $rank
            );

            $this->db->where('id', $id);
            $this->db->update('success_cases', $data);
        }
        

        public function update_image($params, $id)
        {
            $this->db->where('id', $id)
                ->update('success_cases', $params);
        }
    
        public function add_new_picture($params)
        {
            $this->db->insert('_page_object_pictures', $params);
        }
    
    
        function delete_image($id)
        {
            $this->db->where('id', $id);
            $this->db->delete('_page_object_pictures');
        }


}

?>