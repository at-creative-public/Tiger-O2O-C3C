<?php

use app\libraries\Tenants;

class Rbac_menu_model extends CI_Model
{
    use Tenants;

    public $tbl = 'rbac_menu';
    public $short = 'm';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_menu_list()
    {
        $this->tenant();

        $this->db->reset_query();
        $this->db->select('m.id, m.name, m.name AS url, m.display, m.display_name, m.parent_id, m.label, m.icon, m.display_order');
        $this->db->from($this->tbl . ' ' . $this->short);
        $this->db->where(array('status' => 1, 'display' => 1));
        $this->db->order_by('display_order', 'ASC');
        return $this->db->get()->result_array();
    }

    public function get_all_menu_list($params = null)
    {
        $this->tenant();

        $this->db->select('*');
        $this->db->from($this->tbl . ' ' . $this->short);
        $this->db->where(array('status' => 1));
        if (isset($params) && !empty($params)) {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->order_by('display_order ASC');
        return $this->db->get()->result_array();
    }

    public function get_all_menus_count()
    {
        $this->tenant();

        $this->db->from($this->tbl);
        return $this->db->count_all_results();
    }

    public function add($params)
    {
        $this->tenant();

        if ($params['parent_id'] != 0) {
            $data = [
                'label' => 1,
            ];
            $this->db->set('updatedate', 'NOW()', false);
            $this->db->where('id', $params['parent_id']);
            $this->db->update($this->tbl, $data);
        }
        $data = [
            'display_name' => $params['name'],
            'name' => $params['url'],
            'parent_id' => $params['parent_id'],
            'icon' => $params['icon'],
            'display' => $params['display'],
            'display_order' => $params['display_order'],
        ];
        $this->db->set('createdate', 'NOW()', false);
        $this->db->set('updatedate', 'NOW()', false);
        $this->db->insert($this->tbl, $data);
        return $this->db->insert_id();
    }

    public function get($id)
    {
        $this->tenant();

        $this->db->from($this->tbl);
        $this->db->where('id', $id);
        return $this->db->get()->row_array();
    }

    public function update($params, $id)
    {
        $this->tenant();

        $this->db->from($this->tbl);
        $this->db->where('id', $id);
        $old_parent_id = $this->db->get()->row_array()['parent_id'];
        $data = [
            'display_name' => $params['name'],
            'name' => $params['url'],
            'parent_id' => $params['parent_id'],
            'icon' => $params['icon'],
            'display' => $params['display'],
            'display_order' => $params['display_order'],
            'status' => $params['status'],
        ];
        $this->db->where('id', $id);
        $this->db->set('updatedate', 'NOW()', false);
        $this->db->update($this->tbl, $data);
        if ($old_parent_id != $params['parent_id']) {
            if ($params['parent_id'] == 0) {
                $this->db->from($this->tbl);
                $this->db->where('parent_id', $old_parent_id);
                if ($this->db->count_all_results() == 0) {
                    $data = [
                        'label' => 0
                    ];
                    $this->db->where('id', $old_parent_id);
                    $this->db->set('updatedate', 'NOW()', false);
                    $this->db->update($this->tbl, $data);
                }
            } else {
                $data = [
                    'label' => 1,
                ];
                $this->db->set('updatedate', 'NOW()', false);
                $this->db->where('id', $params['parent_id']);
                $this->db->update($this->tbl, $data);
            }
        }
    }
}
