<?php

use app\libraries\CommonHelper;
use app\libraries\DateHelper;
use app\libraries\Tenants;

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Booking_question_options_model extends MY_IndexCrudModel
{
    use Tenants;
    public $short = "r";
    public $table;
    public $table_prefix = '';
    public $tbl = 'booking_question_options';
    public $database = 'default';

    public function __construct()
    {
        parent::__construct();
        $this->database = 'default';
        // $this->database = $this->getTenantNo();
        // $this->tbl = 'room';
        $this->table = $this->tbl . ' ' . $this->short;
    }
}
