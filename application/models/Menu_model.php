<?php

class Menu_model extends CI_Model
{

    public function __construct()
    {  
        parent::__construct();
    }

    public function get_menu_list()
    {
        $this->db->reset_query();
        $this->db->select('m.id, m.name AS url, m.display, m.display_name, m.parent_id, m.label, m.icon, m.display_order');
        $this->db->from('menu m');
        $this->db->where(array('status' => 1, 'display' => 1));
        $this->db->order_by('display_order', 'ASC');
        return $this->db->get()->result_array();
    }

    public function get_all_menu_list($params = null)
    {
        $this->db->select('m.id, m.name AS url, m.display, m.display_name, m.display_order, m.parent_id, m.label, m.status, m.createdate, m.updatedate');
        $this->db->from('menu m');
        $this->db->where(array('status' => 1));
        if (isset($params) && !empty($params)) {
            $this->db->limit($params['limit'], $params['offset']);
        }
        $this->db->order_by('display_order ASC');
        return $this->db->get()->result_array();
    }

    public function get_all_menus_count()
    {
        $this->db->from('menu');
        return $this->db->count_all_results();
    }

    public function add($params)
    {
        if ($params['parent_id'] != 0) {
            $data = [
                'label' => 1,
            ];
            $this->db->set('updatedate', 'NOW()', false);
            $this->db->where('id', $params['parent_id']);
            $this->db->update('menu', $data);
        }
        $data = [
            'display_name' => $params['name'],
            'name' => $params['url'],
            'parent_id' => $params['parent_id'],
            'icon' => $params['icon'],
            'display' => $params['display'],
            'display_order' => $params['display_order'],
        ];
        $this->db->set('createdate', 'NOW()', false);
        $this->db->set('updatedate', 'NOW()', false);
        $this->db->insert('menu', $data);
        return $this->db->insert_id();
    }

    public function get($id)
    {
        $this->db->from('menu');
        $this->db->where('id', $id);
        return $this->db->get()->row_array();
    }

    public function update($params, $id)
    {
        $this->db->from('menu');
        $this->db->where('id', $id);
        $old_parent_id = $this->db->get()->row_array()['parent_id'];
        $data = [
            'display_name' => $params['name'],
            'name' => $params['url'],
            'parent_id' => $params['parent_id'],
            'icon' => $params['icon'],
            'display' => $params['display'],
            'display_order' => $params['display_order'],
            'status' => $params['status'],
        ];
        $this->db->where('id', $id);
        $this->db->set('updatedate', 'NOW()', false);
        $this->db->update('menu', $data);
        if ($old_parent_id != $params['parent_id']) {
            if ($params['parent_id'] == 0) {
                $this->db->from('menu');
                $this->db->where('parent_id', $old_parent_id);
                if ($this->db->count_all_results() == 0) {
                    $data = [
                        'label' => 0
                    ];
                    $this->db->where('id', $old_parent_id);
                    $this->db->set('updatedate', 'NOW()', false);
                    $this->db->update('menu', $data);
                }
            } else {
                $data = [
                    'label' => 1,
                ];
                $this->db->set('updatedate', 'NOW()', false);
                $this->db->where('id', $params['parent_id']);
                $this->db->update('menu', $data);
            }
        }
    }
}
