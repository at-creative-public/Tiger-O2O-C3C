<?php

class Merchant_model extends MY_Model
{
    public $tbl = 'merchants';
    public $fillable = ['company_id', 'contact_id', 'merchant_id', 'merchant_username', 'merchant_password', 'merchant_plan', 'merchant_valid_until', "force_disabled", "sales"];

    public function get_datatables_query()
    {
        $this->add_select_after_join = FALSE;
        $this
            ->join_model('Company_model', 'company_id', 'id')
            ->join_model('Contact_model', 'contact_id', 'id');

        $q = $this->db
            ->from($this->table_prefix . $this->tbl)
            ->where($this->table_prefix . $this->tbl . '.deleted_at', 0)
            ->select(implode(',', [
                ($this->table_prefix . $this->tbl . '.id'),
                'company_industry',
                'company_name',
                'merchant_id',
                'merchant_password',
                'FROM_UNIXTIME(' . $this->table_prefix . $this->tbl . '.created_at, "%Y-%m-%d") AS created_at',
                "sales",
                'FROM_UNIXTIME(' . $this->table_prefix . $this->tbl . '.merchant_valid_until, "%Y-%m-%d") AS merchant_valid_until',
                'merchant_plan',

            ]))
            ->get_compiled_select();

        // var_dump($q);
        // die();

        return $q;
    }

    public function get_joined($where = [], $limit = NULL, $offset = NULL)
    {
        $this
            ->join_model('Company_model', 'company_id', 'id')
            ->join_model('Contact_model', 'contact_id', 'id');

        $this->db->select($this->table_prefix . $this->tbl . '.*');

        $where[$this->table_prefix . $this->tbl . '.deleted_at'] = 0;
        return $this->get($where, $limit, $offset);
    }

    public function validate($merchant_id, $merchant_password)
    {
        //if ($merchant_id == '123' && $merchant_password == '123')
        //return $this->get()->row_array();
        return $this->get([
            'merchant_id' => $merchant_id,
            'merchant_password' => $merchant_password,
            //'merchant_valid_until >' => time()
        ])->row_array();
    }

    public function get_own_plan($merchant_id)
    {
        return $this->db->select("*")->from("tbl_merchants")->where("id", $merchant_id)->get()->row_array();
    }

    public function generate_api_key()
    {
        $api_key = sha1(md5(time()) . "hGLFQp7FGn+Xn/iLn5iCDQ==");
        return $api_key;
    }
}
