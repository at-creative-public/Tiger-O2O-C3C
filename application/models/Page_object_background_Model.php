<?php
defined("BASEPATH") or exit("No direct scripts access allowed");

class Page_object_background_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id)
    {
        return $this->db->select("*")
            ->from("tbl_item_container")
            ->where('id', $id)
            ->get()
            ->result_array();
    }

    public function update($params, $id)
    {
        $this->db->where('id', $id)
            ->update("tbl_item_container", $params);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete("tbl_item_container");
    }
}
