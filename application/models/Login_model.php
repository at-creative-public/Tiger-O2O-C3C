<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Login_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("System_admin_model");
    }

    public function validate()
    {
        $username = $this->security->xss_clean($this->input->post("username"));
        $password = $this->security->xss_clean($this->input->post('password'));
       
        $result = $this->System_admin_model->validate($username, $password);
        if (!$result) {
            return false;
        } else {

            return true;
        }
    }
}
