<?php

defined('BASEPATH') or exit('No direct script access allowed');



class Page_object_picture_model extends CI_Model

{



    public function __construct()

    {
      
        parent::__construct();

    }



    public function get($id)

    {
       
        
        $set = $this->db->select("*")

            ->where('id', $id)

            ->from('_page_object_picture_info')

            ->get()

            ->row_array();

      
        
        $result = [

            "id" => $set['id'],

            'cols' => $set['cols']

        ];



        $result['pictures'] = $this->db->where('picture_info_id', $id)

            //   ->order_by('displayorder', 'ASC')

            ->from('_page_object_pictures')

            ->get()->result_array();

        

        return $result;

    }



    public function update($params, $id)

    {

        $this->db->where('id', $id)

            ->update('_page_object_picture_info', $params);

    }



    public function add_new_picture($params)

    {

        $this->db->insert('_page_object_pictures', $params);

    }



    function update_rank($id, $rank)

    {

        $data = array(

            'displayorder' => $rank

        );



        $this->db->where('id', $id);

        $this->db->update('_page_object_pictures', $data);

    }



    function delete($id)

    {

        $this->db->where('id', $id);

        $this->db->delete('_page_object_pictures');
        
    }

}

