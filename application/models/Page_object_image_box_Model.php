<?php

defined("BASEPATH") or exit("No direct script access allowed");

class Page_object_image_box_Model extends CI_Model

{



    public function __construct()

    {

        parent::__construct();

    }



    public function get($id)

    {



        $set = $this->db->select("*")

            ->where('id', $id)

            ->from('tbl_page_object_image_row_info')

            ->get()

            ->row_array();



        $result = [

            "id" => $set['id'],

            'cols' => $set['col'],

            'cols_m'=> $set['col_m'],

        ];







        $result["image_box"] = $this->db->select("*")

            ->from("tbl_page_object_image_description_box")



            ->where('row_id', $id)

            ->order_by("displayorder", "ASC")

            ->get()

            ->result_array();







        return $result;

    }



    public function get_id($id)

    {

        return $this->db->select("*")

            ->from("tbl_page_object_image_description_box")

            ->where("id", $id)

            ->get()

            ->result_array();

    }



    public function insert($id)

    {

        $data = [

            'title' => 'default',

            "path" => "default",

            "description" => "default",

            'title_display' => "1",

            "image_display" => '1',

            "description_display" => "1",

            "title_order" => "1",

            "image_order" => "2",

            "description_order" => "3",

            "background_display" => '0',

            "color" => "#ffffff",

            'hyperlink' =>"0",

            "link" => "default"

        ];

        $data['row_id'] = $id;

        $this->db->insert("tbl_page_object_image_description_box", $data);

    }



    public function update($params, $id)

    {

        $this->db->where('id', $id)

            ->update("tbl_page_object_image_description_box", $params);

    }



    public function update_rank($id, $rank)

    {



        $data = array(

            'displayorder' => $rank

        );



        $this->db->where('id', $id);

        $this->db->update('tbl_page_object_image_description_box', $data);

    }





    public function delete($id)

    {

        $this->db->where('id', $id);

        $this->db->delete("tbl_page_object_image_description_box");

    }

}

