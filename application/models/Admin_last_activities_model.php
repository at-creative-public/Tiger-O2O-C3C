<?php



if (!defined('BASEPATH')) exit('No direct script access allowed');

class AdminLastActivity
{
    // public $id;
    public $admin_id;
    public $last_activity_time;
    public $status_online;

    public static function fromJson($json)
    {
        if (!is_array($json)) {
            $params = json_decode($json, true);
        } else {
            $params = $json;
        }

        $_this = new self;
        foreach ($params as $key => $value) {
            $_this->{$key} = $value;
        }

        return $_this;
    }
}

class AdminLastActivities
{
    public $items = [];
    private static $filename = APPPATH . 'storage/admin_last_activities.json';

    public static function fromJson($json)
    {
        $items = json_decode($json, true);

        $_this = new self;
        foreach ($items as $adminId => $item) {
            $lastActivity = AdminLastActivity::fromJson($item);
            if (strtotime('+15 minutes', $lastActivity->last_activity_time) < time()) {
                $lastActivity->status_online = false;
            }

            $_this->items[$adminId] = $lastActivity;
        }

        return $_this;
    }

    public function find($adminId)
    {
        return array_key_exists($adminId, $this->items) ? $this->items[$adminId] : false;
    }

    public function updateLastActivityTime($adminId, $timestamp)
    {
        if ($this->find($adminId)) {
            /**@var AdminLastActivity $lastActivity */
            $lastActivity = &$this->items[$adminId];
            $lastActivity->last_activity_time = $timestamp;
            $lastActivity->status_online = true;
        } else {
            $lastActivity = AdminLastActivities::createLastActivity($adminId, $timestamp);
            $this->items[$adminId] = $lastActivity;
        }

        $input = [];
        foreach ($this->items as $key => $item) {
            $input[$key] = (array) $item;
        }

        return file_put_contents(self::$filename, json_encode($input));
    }

    public static function readFile()
    {
        if (!file_exists(self::$filename)) {
            return false;
        }
        return self::fromJson(file_get_contents(self::$filename));
    }

    public static function createLastActivity($adminId, $timestamp)
    {
        $lastActivity = new AdminLastActivity();

        $lastActivity->admin_id = $adminId;
        $lastActivity->last_activity_time = $timestamp;
        $lastActivity->status_online = true;

        return $lastActivity;
    }

    public static function createFile(AdminLastActivity $adminLastActivity)
    {
        $input = [$adminLastActivity->admin_id => (array) $adminLastActivity];
        return file_put_contents(self::$filename, json_encode($input));
    }
}

class Admin_last_activities_model
{
    /**
     * @var AdminLastActivities|null
     */
    public static $lastActivities = null;

    public function listing()
    {
        return self::$lastActivities ?? self::$lastActivities = AdminLastActivities::readFile();
    }

    public function detail_admin_id($adminId)
    {
        self::$lastActivities = AdminLastActivities::readFile();

        return self::$lastActivities[$adminId];
    }

    public function update_and_create_if_not_exist($adminId, $timestamp)
    {
        self::$lastActivities = AdminLastActivities::readFile();
        if (!self::$lastActivities) {
            $lastActivity = AdminLastActivities::createLastActivity($adminId, $timestamp);
            AdminLastActivities::createFile($lastActivity);
            self::$lastActivities = AdminLastActivities::readFile();
        } else {
            self::$lastActivities->updateLastActivityTime($adminId, $timestamp);
        }
    }
}
