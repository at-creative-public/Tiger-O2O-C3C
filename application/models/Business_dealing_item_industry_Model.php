<?php
class Business_dealing_item_industry_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function build_linkage($item_id, $location_tag_id)
    {
        $this->db->set('item_id', $item_id)
            ->set("industry_tag_id", $location_tag_id)
            ->insert("tbl_item_industry_linkage");
    }

    public  function delete($item_id)
    {
        $this->db->where('item_id', $item_id)
            ->delete("tbl_item_industry_linkage");
    }
}
