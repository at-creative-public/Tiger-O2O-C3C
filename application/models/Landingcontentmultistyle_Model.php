<?php
class Landingcontentmultistyle_Model extends CI_Model {

	function insert($arr) {

		$this->db->insert('_landing_content_multistylepage', $arr);
		return $this->db->insert_id();

	}

	function checkexisth1($title) {
		$this->db->select('h1');
		$this->db->from('_landing_content_multistylepage');
		$this->db->where('deleted', 0);
		$this->db->where('h1', $title);
		$query = $this->db->get();
		$num = $query->num_rows();
		if ($num > 0) {
			return true;
		} else {
			return false;
		}
	}

	function checkexisth1byid($id, $title) {
		$this->db->select('h1');
		$this->db->from('_landing_content_multistylepage');
		$this->db->where('deleted', 0);
		$this->db->where('h1', $title);
		$this->db->where('id!=', $id);
		$query = $this->db->get();
		$num = $query->num_rows();
		// echo $this->db->last_query();
		if ($num > 0) {
			return true;
		} else {
			return false;
		}

	}

	function update($id, $arr) {

		$this->db->where('id', $id);
		$this->db->update('_landing_content_multistylepage', $arr);

	}

	function get() {

		// $this->db->select('landing_content_multistylepage.id,landing_content_multistylepage.main_id,landing_main.title as main_title,landing_content_multistylepage.submenu_id,landing_submenu.title as submenu_title,landing_content_multistylepage.title,landing_content_multistylepage.partaimage1,landing_content_multistylepage.partaimage2,landing_content_multistylepage.partbtext,landing_content_multistylepage.partcimage,landing_content_multistylepage.partcpart2,landing_content_multistylepage.partdpart1,landing_content_multistylepage.partetext,landing_content_multistylepage.partftext,landing_content_multistylepage.partfimage,landing_content_multistylepage.partdimage, landing_content_multistylepage.displayorder, landing_content_multistylepage.updated,landing_main.displayorder as maindisplayorder, landing_submenu.displayorder as submenudisplayorder ');

		$this->db->select('lcm.id, lcm.main_id, lcm.landingpage, lcm.mainweb, lm.title as main_title, lcm.submenu_id, ls.title as submenu_title, lcm.h1, lcm.h2, lcm.meta_title, lcm.meta_keyword, lcm.meta_description, lcm.partdimage, lcm.displayorder, lcm.updated, lm.displayorder as maindisplayorder, ls.displayorder as submenudisplayorder ');

		$this->db->order_by("maindisplayorder", "asc");
		$this->db->order_by("submenudisplayorder", "asc");
		$this->db->order_by("displayorder", "asc");
		$this->db->from('_landing_content_multistylepage lcm');
		$this->db->join('_landing_main lm', 'lcm.main_id = lm.id');
		$this->db->join('_landing_submenu ls', 'lcm.submenu_id = ls.id');
		$this->db->where('deleted', 0);
		$query = $this->db->get();

		return $query->result();
	}
	function getsubmenu($id) {
		$this->db->select('id as submenu_id, title as name');
		$this->db->where('main_id', $id);
		$query = $this->db->get('_landing_submenu');
		return $query->result();
	}
	function get_id($id) {
		$this->db->where('id', $id);
		$query = $this->db->get('_landing_content_multistylepage');
		return $query->result();
	}

	function get_lang($lang) {

		$this->db->where('lang', $lang);
		$query = $this->db->get('_landing_content_multistylepage');
		return $query->result();

	}

	function get_sample_page() {
		$this->db->where('id', 1);
		$query = $this->db->get('samplelanding_content_multistylepage');
		return $query->result();
	}

	function delete($id) {
		$arr['deleted'] = 1;
		$this->db->where('id', $id);
		$this->db->update('_landing_content_multistylepage', $arr);
	}
	function findpage($page) {
		//echo "search h1";
		//echo $page;
		$this->db->where('h1', urldecode($page));
		$this->db->from('_landing_content_multistylepage');
		$query = $this->db->get();
		$ret = $query->row();
		return $ret->id;
	}
	function findallpage($submenuid) {
		$this->db->select('lcm.id,lcm.main_id,lcm.landingpage,lcm.mainweb,lm.title as main_title,lcm.partaimage1,lcm.partaimage1_alttext,lcm.partaimage2,lcm.partaimage2_alttext,lcm.submenu_id,ls.title as submenu_title,lcm.h1,lcm.h2,lcm.meta_title,lcm.meta_keyword,lcm.meta_description,lcm.partdimage, lcm.displayorder, lcm.updated,lm.displayorder as maindisplayorder, ls.displayorder as submenudisplayorder ');

		$this->db->order_by("maindisplayorder", "asc");
		$this->db->order_by("submenudisplayorder", "asc");
		$this->db->order_by("displayorder", "asc");
		$this->db->from('_landing_content_multistylepage lcm');
		$this->db->join('_landing_main lm', 'lcm.main_id = lm.id');
		$this->db->join('_landing_submenu ls', 'lcm.submenu_id = ls.id');
		$this->db->where('ls.id=', $submenuid);
		$this->db->where('deleted', 0);
		$query = $this->db->get();

		return $query->result();
	}

	function countNumOfLandingContent() {
		$this->db->select('_landing_content_multistylepage.id');
		$this->db->from('_landing_content_multistylepage');
		$this->db->join('_landing_main', '_landing_content_multistylepage.main_id = _landing_main.id');
		$this->db->join('_landing_submenu', '_landing_content_multistylepage.submenu_id = _landing_submenu.id');
		$this->db->where('deleted', 0);
		$this->db->where('landingpage', 1);
		$query = $this->db->get();
		$num = $query->num_rows();

		return $num;
	}
	function countNumOfMainContent() {
		$this->db->select('_landing_content_multistylepage.id');
		$this->db->from('_landing_content_multistylepage');
		$this->db->join('_landing_main', '_landing_content_multistylepage.main_id = _landing_main.id');
		$this->db->join('_landing_submenu', '_landing_content_multistylepage.submenu_id = _landing_submenu.id');
		$this->db->where('deleted', 0);
		$this->db->where('mainweb', 1);
		$query = $this->db->get();
		$num = $query->num_rows();

		return $num;
	}

	function convertWhatsapFromContent($content, $whatsappnumber) {
		//may be need to change the content phone number to {whatsappnumber}
		$text = str_replace('91629119', $whatsappnumber, $content);
		return $text;
	}

}

?>