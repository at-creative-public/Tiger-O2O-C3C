<?php

class Business_dealing_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /*  public function get_front()
    {
        return $this->db->select("*")
            ->from('tbl_business_dealing_item')
            ->where('display', "1")
            ->get()
            ->result();
    } */

    public function get_front($start, $length, $location_filter, $industry_filter)
    {
        $target_id_1 = [];
        $target_id_2 = [];

        if (!empty($location_filter)) {
            $target_id_1 = $this->db->select("item_id")
                ->from("tbl_item_location_linkage")
                ->where_in('location_tag_id', $location_filter)
                ->get()
                ->result_array();
        }

        if (!empty($industry_filter)) {
            $target_id_2 = $this->db->select("item_id")
                ->from("tbl_item_industry_linkage")
                ->where_in('industry_tag_id', $industry_filter)
                ->get()
                ->result_array();
        }
        $target_ids = ["-1"];

        if (!empty($target_id_1)) {
            foreach ($target_id_1 as $target) {
                $target_ids[] = $target['item_id'];
            }
        }

        if (!empty($target_id_2)) {
            foreach ($target_id_2 as $target) {
                $target_ids[] = $target['item_id'];
            }
        }


        if (empty($location_filter) && empty($industry_filter)) {
            $result =  $this->db->select("*")
                ->from('tbl_business_dealing_item')
                ->where('display', "1")
                ->order_by("timestamp", "DESC")
                ->limit($length, $start)
                ->get()
                ->result_array();
        } else {
            $result =  $this->db->select("*")
                ->from('tbl_business_dealing_item')
                ->where('display', "1")
                ->order_by("timestamp", "DESC")
                ->where_in('id', $target_ids)
                ->limit($length, $start)
                ->get()
                ->result_array();
        }

        $this->load->model("Location_tag_Model");
        $this->load->model("Industry_tag_Model");
        foreach ($result as $index => $res) {
            $location_tag_set = [];
            if ($res['location_tag'] != "") {
                $location_tag_set = explode("|", $res['location_tag']);
            }
            foreach ($location_tag_set as $location_tag) {
                if ($location_tag != "") {
                    $tag_data = $this->Location_tag_Model->get_id($location_tag);
                    $result[$index]['location_tag_information'][] = $tag_data;
                }
            }
            $industry_tag_set = [];
            if ($res['industry_tag'] != "") {
                $industry_tag_set = explode("|", $res['industry_tag']);
            }
            foreach ($industry_tag_set as $industry_tag) {
                if ($industry_tag != "") {
                    $tag_data = $this->Industry_tag_Model->get_id($industry_tag);
                    $result[$index]['industry_tag_information'][] = $tag_data;
                }
            }
        }

        return $result;
    }


    public function insert($params)
    {
        $this->db->insert("tbl_business_dealing_item", $params);
        return $this->db->insert_id();
    }

    function totalCount()
    {
        return $this->db->from('tbl_business_dealing_item')
            ->count_all_results();
    }


    public function build_datatables($draw, $total, $filtered, $data)
    {
        return [
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => $filtered,
            'data' => $data,
        ];
    }

    public function filteredCount($param)
    {
        $start = $param['start'];
        $length = $param['length'];
        $search = [];
        $this->db->select("*");
        $this->db->from('tbl_business_dealing_item');
        foreach ($param['columns'] as $column) {
            if ($column['searchable'] === "true" && !empty($column['search']['value'])) {

                $searchfield = "name";
                $search[$searchfield] = $column['search']['value'];
            }
        }


        if (!empty($search)) {
            foreach ($search as $k => $v) {
                if ($v !== '') {
                    if ($k === 'name') {
                        $this->db->like($search);
                    }
                    $this->db->like($search, 'none');
                }
            }
        }
        return count($this->db->get()->result_array());
    }

    public function getFromDBFilter($table, $fields, $start, $length, $orders, $search, $joins = [], $keywordfieldname = '')
    {

        //$db = $this->load->database($database, TRUE);
        $this->db->distinct();
        $this->db->select(implode(', ', $fields));
        $this->db->from($table);
        //$this->db->select("tbl_contact_us_form_category.name AS category_name");
        //$this->db->join('tbl_contact_us_form_category', 'tbl_contact_us_form_category.id = category_id');


        if (!empty($search) && !empty($keywordfieldname)) {
            //            log_message('debug','here');
            foreach ($search as $k => $v) {
                if ($v !== '') {
                    if ($k === $keywordfieldname) {
                        $this->db->like($search);
                    }
                    $this->db->like($search, 'none');
                }
            }
        }

        $this->db->order_by(implode(', ', $orders));
        if ($start > -1) {
            $this->db->limit($length, $start);
        }
        return $this->db->get()->result_array();
    }

    public function listing($param)
    {
        $fields = ['id', 'title', 'display', 'hit_rate'];
        $start = $param['start'];
        $length = $param['length'];
        $order = [];
        $search = ['name'];
        $joins = [];
        $data = [];

        $result = $this->getFromDBFilter("_business_dealing_item", $fields, $start, $length, $order, $search, $joins, '');

        foreach ($result as $row) {
            $data[] = [
                "id" => $row['id'],
                "title" => $row["title"],
                "display" => $row['display'],
                "hit_rate" => $row['hit_rate'],
                "detail" => '<a class="btn btn-default" href="/backend/business_dealing_management/detail?id=' . $row['id'] . '">詳細</a>',
                "del" => '<a class="btn btn-default" href="/backend/business_dealing_management/delete?id=' . $row['id'] . '">刪除</a>'

            ];
        }
        return $data;
    }

    public function get_id($id)
    {
        return $this->db->from("tbl_business_dealing_item")
            ->where("id", $id)
            ->get()
            ->row_array();
    }

    public function get_id_front($id)
    {
        $result = $this->db->from("tbl_business_dealing_item")
            ->where("id", $id)
            ->get()
            ->row_array();

        $this->load->model("Location_tag_Model");
        $this->load->model("Industry_tag_Model");

        $location_tag_set = [];
        if ($result['location_tag'] != "") {
            $location_tag_set = explode("|", $result['location_tag']);
        }
        foreach ($location_tag_set as $location_tag) {
            if ($location_tag != "") {
                $tag_data = $this->Location_tag_Model->get_id($location_tag);
                $result['location_tag_information'][] = $tag_data;
            }
        }
        $industry_tag_set = [];
        if ($result['industry_tag'] != "") {
            $industry_tag_set = explode("|", $result['industry_tag']);
        }
        foreach ($industry_tag_set as $industry_tag) {
            if ($industry_tag != "") {
                $tag_data = $this->Industry_tag_Model->get_id($industry_tag);
                $result['industry_tag_information'][] = $tag_data;
            }
        }

        return $result;
    }

    public function get_recommand($id)
    {
        $result = [];
        $selected_id = [$id];

        /* return $this->db->select("*")
            ->from('tbl_business_dealing_item')
            ->where('display', '1')
            ->where_not_in('id', $id)
            ->order_by("hit_rate","DESC")
            ->limit(5, 0)
            ->get()
            ->result_array(); */
        $recommand_result = $this->db->select("*")
            ->from('tbl_business_dealing_item')
            ->where('display', '1')
            ->where_not_in('id', $id)
            ->where('pin', '1')

            ->order_by("timestamp", "DESC")
            ->limit(5, 0)
            ->get()
            ->result_array();
        $recommand_result_number = sizeof($recommand_result);
        foreach ($recommand_result as $rec) {
            $result[] = $rec;
            $selected_id[] = $rec['id'];
        }
        if ($recommand_result_number < 5) {
            $secondary_result = $this->db->select("*")
                ->from('tbl_business_dealing_item')
                ->where('display', '1')
                ->where_not_in('id', $selected_id)
                ->order_by("hit_rate", "DESC")
                ->limit(5 - $recommand_result_number, 0)
                ->get()
                ->result_array();
        }


        foreach ($secondary_result as $rec) {
            $result[] = $rec;
        }
        return $result;
    }



    public function update($params, $id)
    {
        $this->db->set($params)
            ->where('id', $id)
            ->update("tbl_business_dealing_item");
    }

    public function add_hit_rate($id)
    {
        $this->db->set("hit_rate", "`hit_rate`+1", false)
            ->where("id", $id)
            ->update("tbl_business_dealing_item");
    }

    public function delete($id)
    {
        $this->db->where('id', $id)
            ->delete("tbl_business_dealing_item");
    }

    public function search_by_title($title)
    {
        return $this->db->select("*")
            ->from("tbl_business_dealing_item")
            ->where('title', urldecode($title))
            ->get()
            ->row_array();
    }
}
