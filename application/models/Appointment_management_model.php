<?php 

defined("BASEPATH") or exit("No direct access allowed");

class Appointment_management_model extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    public function insert($params){
        $this->db->insert("tbl_appointment_record",$params);
    }

    function totalCount()
    {
        return $this->db->from('tbl_appointment_record')
            ->count_all_results();
    }

    public function build_datatables($draw, $total, $filtered, $data)
    {
        return [
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => $filtered,
            'data' => $data,
        ];
    }

    public function getFromDBFilter($table, $fields, $start, $length, $orders, $search, $joins = [], $keywordfieldname = '')
    {

        //$db = $this->load->database($database, TRUE);
        $this->db->distinct();
        $this->db->select(implode(', ', $fields));
        $this->db->from($table);
        $this->db->where("display","1");
        //$this->db->select("tbl_contact_us_form_category.name AS category_name");
        //$this->db->join('tbl_contact_us_form_category', 'tbl_contact_us_form_category.id = category_id');


        if (!empty($search) && !empty($keywordfieldname)) {
            //            log_message('debug','here');
            foreach ($search as $k => $v) {
                if ($v !== '') {
                    if ($k === $keywordfieldname) {
                        $this->db->like($search);
                    }
                    $this->db->like($search, 'none');
                }
            }
        }

        $this->db->order_by(implode(', ', $orders));
        if ($start > -1) {
            $this->db->limit($length, $start);
        }
        return $this->db->get()->result_array();
    }

    public function listing($param)
    {
        $fields = ['id', 'name', 'phone_no', 'email', "services", 'other', 'specific_tuner',"create_date",'process'];
        $start = $param['start'];
        $length = $param['length'];
        $order = [];
        $search = ['name'];
        $joins = [];
        

        $result = $this->getFromDBFilter("tbl_appointment_record", $fields, $start, $length, $order, $search, $joins, '');

        foreach ($result as $row) {
            $data[] = [
                'id' => $row['id'],
                'name' => $row['name'],
                'phone_no' => $row['phone_no'],
                "email" => $row['email'],
                "services" =>$row['servics'],
                "other" => $row['other'],
                "specific_tuner" =>$row['specific_tuner'],
                "create_date" =>date("d-m-Y",$row['create_date']),
                "process" => $row['process'] == "1" ? "已處理" : "未處理",
                "detail" => '<a href="/backend/appointment_management/detail">詳細</a>'
            ];
        }
        return $data;
    }

    public function filteredCount($param)
    {
        $start = $param['start'];
        $length = $param['length'];
        $search = [];
        $this->db->select("*");
        $this->db->from('customer_query_record');
        foreach ($param['columns'] as $column) {
            if ($column['searchable'] === "true" && !empty($column['search']['value'])) {

                $searchfield = "name";
                $search[$searchfield] = $column['search']['value'];
            }
        }


        if (!empty($search)) {
            foreach ($search as $k => $v) {
                if ($v !== '') {
                    if ($k === 'name') {
                        $this->db->like($search);
                    }
                    $this->db->like($search, 'none');
                }
            }
        }
        return count($this->db->get()->result_array());
    }

    

    public function assign_appointment($id,$params){

    }
}
