<?php
class Aboutus_Model extends CI_Model{
        
        function insert($arr)
        {
                
                $this->db->insert('_aboutus_content', $arr);
                return $this->db->insert_id();
                
        }
        
        function update($id, $arr)
        {
        
                $this->db->where('id', $id);
                $this->db->update('_aboutus_content', $arr);
        
        }
        
        function get()
        {       

                    //$this->db->select('aboutus_content.id,aboutus_content.main_content,aboutus_content.sub_content  ');
                    $this->db->from('_aboutus_content aboutus_content');
                    $query=$this->db->get();

                return $query->result();
        }
        
        function get_id($id){
                $this->db->where('id', $id);
                $query = $this->db->get('_aboutus_content');
                return $query->result();
        }
        
        function get_lang($lang)
        {
                
                $this->db->where('lang', $lang);
                $query = $this->db->get('_aboutus_content');
                return $query->result();
                
        }
        
        function delete($id)
        {
                $this->db->where('id', $id);
                $this->db->delete('_aboutus_content');
        }
        
}

?>