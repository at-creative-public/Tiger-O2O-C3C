<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Page_object_titleless_container_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id)
    {

        $set = $this->db->select("*")
            ->where("id", $id)
            ->from("tbl_page_object_titleless_row_info")

            ->get()
            ->row_array();

        $result = [
            'id' => $set['id'],
            'cols' => $set['col']
        ];

        $result['titleless_container'] = $this->db->where("titleless_row_info_id", $id)
            ->from('tbl_page_object_titleless_container')
            ->order_by("displayorder", "ASC")
            ->get()
            ->result_array();

        return $result;
    }

    public function get_id($id)
    {
        return $this->db->select("*")
            ->where("id", $id)
            ->from("tbl_page_object_titleless_container")
            ->get()
            ->result_array();
    }

    public function insert($id)
    {
        $params = [
            "titleless_row_info_id" => $id,
            "content" => "default",
            "background_display" => "0",
            "color" => "#ffffff"
        ];
        $this->db->insert("tbl_page_object_titleless_container", $params);
    }

    public function update($params, $id)
    {
        $this->db->where('id', $id)
            ->update('tbl_page_object_titleless_container', $params);
    }

    function update_rank($id, $rank)
    {

        $data = array(
            'displayorder' => $rank
        );

        $this->db->where('id', $id);
        $this->db->update('tbl_page_object_titleless_container', $data);
    }

    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_page_object_titleless_container');
    }
}
