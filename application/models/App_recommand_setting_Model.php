<?php
defined('BASEPATH') or exit('No direct script access allowed');

class App_recommand_setting_Model extends CI_Model{

    public function __construct()
    {
        parent::__construct();

    }

    public function get()
    {
        $result = [];
        $this->load->model('App_recommand_setting_image_Model');
        $this->load->model('App_recommand_setting_title_Model');
        $this->load->model("App_recommand_setting_link_Model");
        $result['image'] = json_decode(json_encode($this->App_recommand_setting_image_Model->get()),true);
        $result['text'] = json_decode(json_encode($this->App_recommand_setting_title_Model->get()),true);
        $result['link'] = json_decode(json_encode($this->App_recommand_setting_link_Model->get()),true);
        return $result;
    }

   /* public function load_objects($main_id, $submenu_id)
    {
        if ($submenu_id === 0) {
            $this->db->where([
                'main_id' => $main_id,
            ]);
        }
        else {
            $this->db->where([
                'submenu_id' => $submenu_id,
            ]);
        }
        $this->db->order_by('displayorder', 'ASC');
        $result = $this->db->get('_page_objects')->result_array();
        $this->load->model('Page_object_title_model', 'object_title');
        $this->load->model('Page_object_picture_model', 'object_picture');
        foreach ($result as $idx => $res) {
            $result[$idx]['objects'] = [];
            $objects = [];
            switch ($res['page_object_type']) {
                case "TITLE":
                    $result[$idx]['objects'][] = $this->object_title->get($res['object_id']);
                    break;
                case "PICTURES":
                    $result[$idx]['objects'][] = $this->object_picture->get($res['object_id']);
                    break;
            }
        }
        return $result;
    }*/

  /*  function update_rank($id, $rank)
    {
        $data = array(
            'displayorder' => $rank
        );

        $this->db->where('id', $id);
        $this->db->update('_page_objects', $data);
    }
    */

    function delete($id)
    {
        $result = $this->db->from('_page_objects')
            ->where('id', $id)
            ->get()->row_array();
        switch ($result['page_object_type']) {
            case "TITLE":
                $this->db->delete('_page_object_title', ['id' => $result['object_id']]);
                break;
            case "PICTURES":
                $this->db->delete('_page_object_picture_info', ['id' => $result['object_id']]);
                $this->db->delete('_page_object_pictures', ['picture_info_id' => $result['object_id']]);
                break;
        }
        $this->db->where('id', $id);
        $this->db->delete('_page_objects');
    }
}
