<?php

class Landingsubmenu_Model extends CI_Model

{



	function insert($arr)

	{


        
		$this->db->insert('_landing_submenu', $arr);
        
		$rst = $this->db->insert_id();
		//var_dump( $sql = $this->db->last_query()); exit;
		return $rst;

	}



	function update($id, $arr)

	{


        
		$this->db->where('id', $id);

		$this->db->update('_landing_submenu', $arr);
		//var_dump( $sql = $this->db->last_query()); exit;
	}



	function get()

	{



		$this->db->select('ls.id,ls.main_id,lm.title as main_title,ls.title, ls.displayorder, ls.updated ');



		$this->db->order_by("main_id", "asc");

		$this->db->order_by("displayorder", "asc");

		$this->db->from('_landing_submenu ls');

		$this->db->join('_landing_main lm', 'ls.main_id = lm.id');

		$query = $this->db->get();



		return $query->result();

	}



	function getbymainid($id)

	{



		$this->db->select('ls.id,ls.main_id,lm.title as main_title,ls.title, ls.title_en, ls.uri, ls.displayorder, ls.updated,ls.segmentation,ls.segmentation_end,ls.display ');

		$this->db->where('main_id', $id);

		$this->db->order_by("main_id", "asc");

		$this->db->order_by("displayorder", "asc");

		$this->db->from('_landing_submenu ls');

		$this->db->join('_landing_main lm', 'ls.main_id = lm.id');

		$query = $this->db->get();



		return $query->result();

	}



	function get_id($id)

	{

		$this->db->where('id', $id);

		$query = $this->db->get('_landing_submenu');

		return $query->result();

	}



	function get_lang($lang)

	{



		$this->db->where('lang', $lang);

		$query = $this->db->get('_landing_submenu');

		return $query->result();

	}

    

    function search_by_obj($main_id){  
		$this->db->where([

			'main_id' => $main_id,

		]);
        
		//var_dump($main_id);exit;
		$query = $this->db->get('_page_objects');
		$page_obj_row = $query->row_array();
		//var_dump($page_obj_row);exit;
       // $page_obj_link_collections = array();
		if(!empty($page_obj_row["object_id"])){

			 $this->db->where([

				'row_id' => $page_obj_row['object_id'],

			]);

			$query = $this->db->get('_page_object_image_description_box');
            $obj_detail_rows = $query->result_array();
			if(!empty($obj_detail_rows)){
				return $obj_detail_rows;
				
			}else{
				return "";
			}

		}

		
	} 


	function search_by_uri($main_id, $action)

	{

		$this->db->where([

			'main_id' => $main_id,

			'uri' => $action,

		]);

		$query = $this->db->get('_landing_submenu');

		return $query->row_array();

	}



	function delete($id)

	{

		$this->db->where('id', $id);

		$this->db->delete('_landing_submenu');

	}

	function update_rank($id, $rank)

	{

		$data = array(

			'displayorder' => $rank,

		);



		$this->db->where('id', $id);

		$this->db->update('_landing_submenu', $data);

	}

	function findsubmenu($submenu)

	{

		//echo "search h1";

		//echo $page;

		$this->db->where('title', urldecode($submenu));

		$this->db->from('_landing_submenu');

		$query = $this->db->get();

		$ret = $query->row();

		return $ret->id;

	}



	function display_on($id)

	{

		$this->db->where('id', $id)

			->update("_landing_submenu", ['display' => 1]);

	}



	function display_off($id)

	{

		$this->db->where('id', $id)

			->update("_landing_submenu", ['display' => 0]);

	}

}

