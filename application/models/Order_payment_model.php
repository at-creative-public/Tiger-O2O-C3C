<?php

use app\libraries\paypal\PaypalClient;

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order_payment_model extends MY_Model
{
    public $tbl = 'orders_payment';

    public function add_order_payment($params)
    {
        $id = $this->insertDB($this->database, $this->tbl, $params, false, [
            'created_at' => date('Y-m-d H:i:s')
        ]);

        return $id;
    }

    public function get_order_payments($order_ids)
    {
        if (!is_array($order_ids)) $order_ids = [$order_ids];
        return $this->getAllIn($this->database, $this->tbl, [], 'order_id', $order_ids);
    }
}
