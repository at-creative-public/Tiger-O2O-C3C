<?php
defined("BASEPATH") or exit("No direct script access allowed");
class News_management_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get()
    {
        return $this->db->select("*")
            ->from('tbl_news')
            ->order_by("displayorder", "DESC")
            ->get()
            ->result_array();
    }

    public function get_front()
    {
        return $this->db->select("*")
            ->from('tbl_news')
            ->order_by("displayorder", "DESC")
            ->where('display', "1")
            ->get()
            ->result_array();
    }

    public function get_id($id)
    {
        return $this->db->select("*")
            ->from('tbl_news')
            ->where('id', $id)
            ->get()
            ->result_array();
    }

    public function insert($params)
    {

        $params['date'] = time();
        $this->db->insert("tbl_news", $params);
    }

    public function update($params, $id)
    {
        $this->db->where("id", $id)
            ->update("tbl_news", $params);
    }

    public function delete($id)
    {
        $this->db->where('id', $id)
            ->delete('tbl_news');
    }

    public function update_rank($id, $rank)
    {
        $data = array(
            'displayorder' => $rank
        );


        $this->db->where('id', $id);
        $this->db->update('tbl_news', $data);
    }
}
