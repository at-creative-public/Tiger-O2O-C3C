<?php

use app\libraries\CSVClass;

/**
 * @property MY_IndexCrudModel $this
 * @property Booking_questions_submit_model $Booking_questions_submit_model
 * @property Booking_questions_model $Booking_questions_model
 */
class Booking_customers_model extends MY_IndexCrudModel
{
    public $has_displayorder = false;
    public $table_prefix = '';
    public $tbl = 'booking_record';
    public $short = 'booking';
    public $database = 'default';

    // table_view
    public $select_fields = ['*'];
    public $search_fields = [];
    public $where = [];
    public $orderby = '';
    public $softdelete = '';
    public $join_table = true;

    public $disable_limit_per_page = false;
    public $base_url = 'booking_customers';

    public function __construct()
    {
        parent::__construct();

        // No search fields
        $this->search_fields = [];

        // No orderby
        $this->orderby = '';

        $this->load->model('Booking_questions_submit_model');
        $this->load->model('Booking_questions_model');
        $this->load->model('Member_model');
    }

    protected function table_view_get_criteria($searchValue)
    {
        parent::table_view_get_criteria($searchValue);

        $this->db
            ->join(
                $this->Booking_questions_submit_model->table_prefix . $this->Booking_questions_submit_model->tbl . ' ' . $this->Booking_questions_submit_model->tbl_short,
                $this->Booking_questions_submit_model->tbl_short . '.booking_id = ' . $this->tbl_short . '.id',
                'left'
            )
            ->join(
                $this->Booking_questions_model->table_prefix . $this->Booking_questions_model->tbl . ' ' . $this->Booking_questions_model->tbl_short,
                $this->Booking_questions_model->tbl_short . '.id = ' . $this->Booking_questions_submit_model->tbl_short . '.booking_question_id',
                'left'
            );


        $this->db->group_by($this->tbl_short . '.id');
        $this->db->group_by($this->Booking_questions_model->tbl_short . '.type_id');
        $this->select_fields = [
            '*',
            'GROUP_CONCAT(' . $this->Booking_questions_model->tbl_short . '.question) AS question'
        ];
    }

    public function export_csv()
    {
        $this->disable_limit_per_page = true;
        $res = $this->table_view(0, 0);
        $filename = date("Y-m-d") . '-顧客即時預約.csv';
        $e = new CSVClass();
        // array_walk($res['items'], function (&$item) {
        //     if ($item['gender'] === "0") {
        //         $item['gender'] = "男";
        //     } else if ($item['gender'] === "1") {
        //         $item['gender'] = "女";
        //     }
        // });
        return $e->export(
            [
                'id' => ['omitted' => true],
                'booking_date' => ['value' => '預約日期'],
                'booking_time' => ['value' => '預約時間'],
                'service' => ['value' => '專科項目'],
                'name' => ['value' => '姓名'],
                'phone' => ['value' => '聯絡電話'],
                'email' => ['value' => '電郵地址'],
                'type_id' => ['value' => '問題種類'],
                'question' => ['value' => '顧客回答'],
            ],

            $res['items'],
            $filename
        );
    }
}
