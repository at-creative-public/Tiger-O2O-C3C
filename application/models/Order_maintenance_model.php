<?php

use app\libraries\paypal\PaypalClient;

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order_maintenance_model extends MY_Model
{
    public $tbl = 'orders_maintenance';

    public function add_order_maintenance($params)
    {
        $id = $this->insertDB($this->database, $this->tbl, [
            'member_id' => $params['member_id'],
            'product_id' => $params['product_id'],
            'order_id' => $params['order_id'] ?? null,
            'purchase_date' => $params['purchase_date'],
            // TODO: Order_maintenance_file_model
            // 'filename' => $params['filename'] ?? null,
        ], false, [
            'updated_at' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        // Get the main product 
        $this->load->model("Product_model");
        $product = $this->Product_model->get_product($params['product_id'], ['name']);

        // and the children products
        $sub_products = $this->Product_model->get_children_products($params['product_id'], ['id', 'type', 'name', 'years_of_maintenance']);


        // Prepare output 
        $output = [];
        $date = date('Y-m-d H:i:s');


        // $stored_main_product_ids = [];
        // foreach ($sub_products as $sub_product) {
        //     if (!in_array($params['product_id'], $stored_main_product_ids)) {
        //         array_push($stored_main_product_ids, $params['product_id']);
        //         $stored = false;
        //     } else {
        //         $stored = true;
        //     }

        //     array_push($output, [
        //         'member_id' => $params['member_id'],
        //         'order_id' => null,
        //         'order_maintenance_id' => $id,
        //         'purchase_date' => $stored ? "" : $params['purchase_date'],
        //         'main_product_id' => $params['product_id'],
        //         'main_product_name' => $stored ? "" : $product['name'],
        //         'sub_product_id' => $sub_product['id'],
        //         'sub_product_type' => $sub_product['type'],
        //         'sub_product_name' => $sub_product['name'],
        //         'sub_product_years_of_maintenance' => (int) $sub_product['years_of_maintenance'],
        //         'end_date' => date('Y-m-d H:i:s', strtotime('+' . (int) $sub_product['years_of_maintenance'] . 'year, -1 day', (new DateTime($params['purchase_date']))->getTimestamp())),
        //         'deleted_at' => null,
        //         'created_at' => $date
        //     ]);
        // }
        foreach ($sub_products as $sub_product) {

            array_push($output, [
                'member_id' => $params['member_id'],
                'order_id' => null,
                'order_maintenance_id' => $id,
                'purchase_date' => $params['purchase_date'],
                'main_product_id' => $params['product_id'],
                'main_product_name' => $product['name'],
                'sub_product_id' => $sub_product['id'],
                'sub_product_type' => $sub_product['type'],
                'sub_product_name' => $sub_product['name'],
                'sub_product_years_of_maintenance' => (int) $sub_product['years_of_maintenance'],
                'end_date' => date('Y-m-d H:i:s', strtotime('+' . (int) $sub_product['years_of_maintenance'] . 'year, -1 day', (new DateTime($params['purchase_date']))->getTimestamp())),
                'deleted_at' => null,
                'created_at' => $date
            ]);
        }

        $this->load->model("Order_maintenance_output_model");
        $this->Order_maintenance_output_model->create_batch($output);

        return [
            'id' => $id,
            'product' => $product,
            'sub_products' => $sub_products,
            'output' => $output
        ];
    }

    public function get_order_maintenances($member_ids)
    {
        if (!is_array($member_ids)) $order_ids = [$member_ids];
        return $this->getAllIn($this->database, $this->tbl, [], 'member_id', $member_ids);
    }

    // public function push_notification($output, $batch = false)
    // {
    //     $res = $this->session->tempdata('push_notification') ?? [];

    //     foreach ($output as $params) {
    //         $member_ids = [$params['member_id']];

    //         $this->load->model('Member_model');
    //         $devices = $member_ids != null
    //             ? $this->Member_model->getAllIn($this->Member_model->database, $this->Member_model->tbl,  ['id', 'fcm_token'], 'id', $member_ids, true, ['deleted_at' => null, 'fcm_token !=' => null])
    //             : $this->Member_model->getAll($this->Member_model->database, $this->Member_model->tbl,  ['id', 'fcm_token'], 'id', $member_ids, true, ['deleted_at' => null, 'fcm_token !=' => null]);

    //         $data = [
    //             'title' => lang('push_messages.maintenance_registered_title', ['product_name' => $params['product_name']]),
    //             'body' => $params['title']
    //         ];

    //         if (!empty($devices)) {
    //             foreach ($devices as $device) {
    //                 array_push($res, [
    //                     'data' => $data,
    //                     'token' => $device['fcm_token'],
    //                     // update sent flag after calling the method push_notification_sent
    //                     // 'model' => null,
    //                     // 'model_id' => null,
    //                     // 'sent_field' => n,
    //                     // 'tenant_no' => $tenant_no
    //                 ]);
    //             }
    //         }
    //     }


    //     $this->session->set_tempdata('push_notification', $res);
    // }
}
