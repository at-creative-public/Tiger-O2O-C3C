<?php
class Industry_tag_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function listing()
    {
        return $this->db->select("*")
            ->from("tbl_industry_tag")
            ->order_by("displayorder")
            ->get()
            ->result_array();
    }

    public function listing_front()
    {
        return $this->db->select("*")
            ->from("tbl_industry_tag")
            ->where("display", "1")
            ->order_by("displayorder")
            ->get()
            ->result_array();
    }

    public function get_id($id)
    {
        return $this->db->select("*")
            ->from("tbl_industry_tag")
            ->where("id", $id)
            ->get()
            ->row_array();
    }

    public function update($params, $id)
    {
        $this->db->set($params)
            ->where('id', $id)
            ->update("tbl_industry_tag");
    }

    public function delete($id)
    {
        $this->db->where("id", $id)
            ->delete("tbl_industry_tag");
    }


    function update_rank($id, $rank)
    {
        $data = array(
            'displayorder' => $rank
        );

        $this->db->where('id', $id);
        $this->db->update('tbl_industry_tag', $data);
    }

    public function get_by_hit_rate()
    {
        return $this->db->select("*")
            ->from("tbl_industry_tag")
            ->order_by("hit_rate", "DESC")
            ->limit(5, 0)
            ->get()
            ->result_array();
    }

    function add_hit($id)
    {
        $this->db->set("hit_rate", "`hit_rate`+1", false)
            ->where("id", $id)
            ->update("tbl_industry_tag");
    }
}
