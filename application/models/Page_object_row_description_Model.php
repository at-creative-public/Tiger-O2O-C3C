<?php
defined("BASEPATH") or exit("No direct script access allowed");

class Page_object_row_description_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id)
    {
        return $result['row_description'] = $this->db->where('id', $id)
            ->from("tbl_page_object_row_description")
            ->get()
            ->result_array();
    }

    public function update($params, $id)
    {
        $this->db->where('id', $id)
            ->update("tbl_page_object_row_description", $params);
    }

    public function delete($id)
    {
        $this->db->where("id", $id);
        $this->db->delete("tbl_page_object_row_description");
    }
}