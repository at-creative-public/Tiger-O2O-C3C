<?php

class Company_model extends MY_Model
{
    public $tbl = 'company';
    public $fillable = ['company_name', 'company_email', 'company_hotline', 'company_industry', 'company_website'];
}
