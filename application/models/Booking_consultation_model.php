<?php
defined("BASEPATH") or exit("No direct script access allowed");

/** 
 * Booking model
 * 
 * @property MY_Model $this
 * @property MY_IndexCrudModel $this
 * 
 * ALTER TABLE `tblbooking_record` ADD `timeslot_id` INT DEFAULT 0 AFTER `remark`;
 */
class Booking_consultation_model extends MY_IndexCrudModel
{
    public $has_displayorder = false;
    public $table_prefix = '';
    public $tbl = 'booking_consultation';
    public $short = 'consult';
    public $database = 'default';

    public function get_by_booking_member($booking_id, $member_id)
    {
        $this->load->model('Booking_model');
        $this->load->model('Booking_timeslot_model');
        return $this->db
            ->from($this->table_prefix . $this->tbl . ' ' . $this->short)
            ->join(
                $this->Booking_model->table_prefix . $this->Booking_model->tbl . ' ' . $this->Booking_model->short,
                $this->Booking_model->short . '.id = ' . $this->short . '.booking_id'
            )
            ->join(
                $this->Booking_timeslot_model->table_prefix . $this->Booking_timeslot_model->tbl . ' ' . $this->Booking_timeslot_model->short,
                $this->Booking_timeslot_model->short . '.id = ' . $this->Booking_model->short . '.timeslot_id'
            )
            ->where($this->short . '.booking_id', $booking_id)
            ->where($this->short . '.member_id', $member_id)
            ->select('*')
            ->get()
            ->row_array();
    }

    public function get_id($consult_id)
    {
        $this->load->model('Booking_model');
        $this->load->model('Booking_timeslot_model');
        return $this->db
            ->from($this->table_prefix . $this->tbl . ' ' . $this->short)
            ->join(
                $this->Booking_model->table_prefix . $this->Booking_model->tbl . ' ' . $this->Booking_model->short,
                $this->Booking_model->short . '.id = ' . $this->short . '.booking_id'
            )
            ->join(
                $this->Booking_timeslot_model->table_prefix . $this->Booking_timeslot_model->tbl . ' ' . $this->Booking_timeslot_model->short,
                $this->Booking_timeslot_model->short . '.id = ' . $this->Booking_model->short . '.timeslot_id'
            )
            ->where($this->short . '.id', $consult_id)
            ->select('*')
            ->get()
            ->row_array();
    }

    public function can_send_invitation_mail($params)
    {
        $waiting_time = 15 * 60;
        $duration = $params['booking_time_interval'] * 60;

        $timeslot_start = strtotime($params['booking_date'] . ' ' . $params['booking_time']);

        $starttime = strtotime('-' . $waiting_time . ' minutes', $timeslot_start);
        $endtime = $timeslot_end = strtotime('+' . $duration . ' minutes', $starttime);

        $current_time = time();
        $can_send_invitation_mail = $current_time >= $starttime && $current_time <= $endtime;
        return $can_send_invitation_mail;
    }

    public function send_invitation_mail($params)
    {
        $email = $params["email"];
        // if ($this->is_test_email($email)) return;

        $title = '遙距診症';
        $display_name = $params["name"];
        // $password = $params["password"];

        $client = new \app\libraries\SendgridClient();
        $mail = $client->create();
        $mail->setFrom("no-reply@at-appmaker.com");
        $mail->setSubject($title);
        $mail->addTo($email, $display_name);
        $mail->addContent("text/plain", $title);
        $mail->addContent(
            "text/html",
            sprintf(
                "<br /><br />專科項目 ：%s<br />預約日期：%s<br />預約時間：%s<br /><br />姓名 ：%s<br />聯絡電話 ：%s<br />年齡組別 ：%s<br />性別 ：%s<br />",
                $params['service'],
                $params['booking_date'],
                $params['booking_time'],
                $display_name,
                $params['phone'],
                $params['age'],
                $params['gender']
            )
        );

        $client->send($mail);
    }

    public function send_invitation_mails()
    {
        $this->load->model('Booking_model');

        $date = date('Y-m-d');
        // $timestamp = time();
        // $date = '2021-10-15';
        // 2021年10月15日星期五 14:00:00
        // $timestamp = 1634277600;

        $res = $this->db
            ->from($this->table_prefix . $this->tbl . ' ' . $this->short)
            ->join(
                $this->Booking_model->table_prefix . $this->Booking_model->tbl . ' ' . $this->Booking_model->short,
                $this->Booking_model->short . '.id = ' . $this->short . '.booking_id'
            )
            ->where($this->short . '.deleted_at', null)
            ->where($this->Booking_model->short . '.booking_date', $date)
            ->get()
            ->result_array();

        foreach ($res as $item) {
            if ($this->can_send_invitation_mail($item)) $this->send_invitation_mail($item);
        }
    }

    public function verify_doctor_credentials($email, $password)
    {
        $old_password = $password;
        $password = $this->encrypt_password($password);

        $this->load->model('Rbac_user_model');
        $this->load->model('Rbac_role_model');
        $res = $this->Rbac_user_model->get_user_result([], [
            $this->Rbac_user_model->short . '.email' => $email,
            $this->Rbac_user_model->short . '.password' => $password,
            $this->Rbac_user_model->short . '.deleted_at' => null,
            $this->Rbac_role_model->short . '.is_doctor' => 1
        ])->row_array();

        return $res;
    }

    public function save_doctor($params)
    {
        $this->load->model('Rbac_user_model');
        if (!empty($params['id'])) {
            $id = $params['id'];

            $this->Rbac_user_model->update_user($id, $params);
        }
    }

    public function select_doctors()
    {
        $this->load->model('Rbac_user_model');
        $this->load->model('Rbac_role_model');

        $res = $this->Rbac_user_model->get_all_users([], [
            $this->Rbac_user_model->short . '.deleted_at' => null,
            $this->Rbac_role_model->short . '.is_doctor' => 1
        ]);
        $doctors = [];
        foreach ($res as $item) {
            $doctors[$item['id']] = $item['display_name'];
        }

        return $doctors;
    }

    public function get_booking_consultation($where = [])
    {
        $where['deleted_at'] = null;
        $this->where_criteria($where);
        return $this->db->from($this->table_prefix . $this->tbl)->get()->row_array();
    }


    public function get_booking_consultations($where = [])
    {
        $where['deleted_at'] = null;
        $this->where_criteria($where);
        return $this->db->from($this->table_prefix . $this->tbl)->get()->result_array();
    }

    public function add($params)
    {
        $params['consultation_room_name'] = bin2hex(random_bytes(4));
        $params['updated_at'] = date('Y-m-d H:i:s');
        $params['updated_by'] = property_exists($this, 'session') ? ($this->session->userdata('user_id') ?? 0) : 0;
        $params['created_at'] = date('Y-m-d H:i:s');
        $params['created_by'] = property_exists($this, 'session') ? ($this->session->userdata('user_id') ?? 0) : 0;
        return parent::add($params);
    }

    public function update($params, $id)
    {
        $params['updated_at'] = date('Y-m-d H:i:s');
        $params['updated_by'] = property_exists($this, 'session') ? ($this->session->userdata('user_id') ?? 0) : 0;
        return parent::update($params, $id);
    }

    public function create_or_update($params)
    {
        $booking_id = $params['booking_id'] ?? null;
        $res = $booking_id ? $this->get_booking_consultation(['booking_id' => $booking_id]) : [];

        return empty($booking_id) || empty($res) ? $this->add($params) : $this->update($params, $res['id']);
    }
}
