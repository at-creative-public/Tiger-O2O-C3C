<?php
class Customer_contact_Model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function get()
    {
        $result = $this->db->select('*')
            ->from('customer_query_record')
            ->get()
            ->result();
        return $result;
    }

    function get_id($id)
    {
        return  $this->db->select('*')
            ->where('id', $id)
            ->from('customer_query_record')
            ->get()
            ->result();
    }

    function add($request)
    {

        $data['name'] = $request['name'];
        $data['phone_no'] = $request['phone_no'];
        $data['email'] = $request['email'];
        $data['message'] = isset($request['message']) ? $request['message'] : '';
        $data['replied'] = "0";
        $this->db->insert('customer_query_record', $data);
    }

    function totalCount()
    {
        return $this->db->from('customer_query_record')
            ->count_all_results();
    }


    public function build_datatables($draw, $total, $filtered, $data)
    {
        return [
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => $filtered,
            'data' => $data,
        ];
    }

    public function getFromDBFilter($table, $fields, $start, $length, $orders, $search, $joins = [], $keywordfieldname = '')
    {

        //$db = $this->load->database($database, TRUE);
        $this->db->distinct();
        $this->db->select(implode(', ', $fields));
        $this->db->from($table);
        //$this->db->select("tbl_contact_us_form_category.name AS category_name");
        //$this->db->join('tbl_contact_us_form_category', 'tbl_contact_us_form_category.id = category_id');


        if (!empty($search) && !empty($keywordfieldname)) {
            //            log_message('debug','here');
            foreach ($search as $k => $v) {
                if ($v !== '') {
                    if ($k === $keywordfieldname) {
                        $this->db->like($search);
                    }
                    $this->db->like($search, 'none');
                }
            }
        }

        $this->db->order_by(implode(', ', $orders));
        if ($start > -1) {
            $this->db->limit($length, $start);
        }
        return $this->db->get()->result_array();
    }

    public function listing($param)
    {
        $fields = ['id', 'name', 'phone_no', 'email', "category", 'message', 'replied'];
        $start = $param['start'];
        $length = $param['length'];
        $order = [];
        $search = ['name'];
        $joins = [];
        $this->load->model("Contact_us_item_model");

        $result = $this->getFromDBFilter("customer_query_record", $fields, $start, $length, $order, $search, $joins, '');

        foreach ($result as $row) {
            $data[] = [
                'id' => $row['id'],
                'name' => $row['name'],
                'phone_no' => $row['phone_no'],
                "email" => $row['email'],
                "message" => $row['message'],
                "replied" => $row['replied'] == "1" ? "已回覆" : "未回覆",
                "reply" => '<a href="' . base_url("backend/customer_contact/reply/" . $row['id']) . '?id=' . $row['id'] . '" class="btn btn-default reply">回覆</a>'

            ];
        }
        return $data;
    }

    public function filteredCount($param)
    {
        $start = $param['start'];
        $length = $param['length'];
        $search = [];
        $this->db->select("*");
        $this->db->from('customer_query_record');
        foreach ($param['columns'] as $column) {
            if ($column['searchable'] === "true" && !empty($column['search']['value'])) {

                $searchfield = "name";
                $search[$searchfield] = $column['search']['value'];
            }
        }


        if (!empty($search)) {
            foreach ($search as $k => $v) {
                if ($v !== '') {
                    if ($k === 'name') {
                        $this->db->like($search);
                    }
                    $this->db->like($search, 'none');
                }
            }
        }
        return count($this->db->get()->result_array());
    }

    public function replied($id)
    {
        $this->db->set("replied", "1");
        $this->db->where('id', $id);
        $this->db->from("customer_query_record");
        $this->db->update();
    }
}
