<?php

use app\libraries\Userstamp;

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/* Author: Jorge Torres
 * Description: Login model class
 */

class Promotion_setting_model extends CI_Model
{
    const SETTING_ID = 1;

    public function detail()
    {
        return $this->db->get_where('promotion_setting', array('id' => self::SETTING_ID))->row_array();
    }

    public function save($params)
    {
        // if ($mode === 'c') {
        //     $this->db->insert('referral_scheme', Userstamp::create($params));
        // } else {

        // }
        $id = self::SETTING_ID;
        $this->db->update('promotion_setting', Userstamp::update($params), array('id' => $id));
        return $this->db->insert_id();
    }
}
