<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'admin_login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route["timeout"] = 'merchant_login/index/true';
$route['cms/login'] = 'admin_login';
$route['at-admin/login'] = 'admin_login';
//$route['api/auth'] = "api/authentication/auth";
//$route['api/insert'] = "api/input_test/insert";

$route['/'] = 'admin_login';

$route['v1/passes/(:any)/(:any)'] = "registration/update_apple_card/$1/$2";
$route["v1/devices/(:any)/registrations/(:any)"] = "registration/update_handler/$1/$2";
$route["v1/devices/(:any)/registrations/(:any)/(:any)"] = "registration/registration_test/$1/$2/$3";
$route['merchant/login'] = "merchant_login";
$route['merchant/expired_account'] = "login_exception/handle_expired";
$route['merchant/contact_us'] = "login_exception/handle_ban";
$route['merchant_system/preview_QR/(:any)'] = "merchant/preview/show_QR/$1";
$route['merchant_system/(:any)']  = "merchant/preview/$1";
$route['merchant_system/(:any)/(:any)']  = "merchant/preview/$1/$2";

$route['registration/apple/(:any)'] = "registration/apple_registration/$1";
$route['registration/google/(:any)'] = "registration/google_registration/$1";
$route['registration/registration'] = "registration/registration";
$route['registration/(:any)'] = "registration/merge_registration/$1";
$route['coupon/registration/(:any)'] = "couponregistration/registration/$1";
$route['coupon/register'] = "couponregistration/register";

$route['coupon/v1/passes/(:any)/(:any)'] = "couponregistration/card_info_update/$1/$2";
$route['coupon/v1/devices/(:any)/registrations/(:any)'] = "couponregistration/registration_check/$1/$2";
$route["coupon/v1/devices/(:any)/registrations/(:any)/(:any)"] = "couponregistration/registration_callback/$1/$2/$3";

$route['event_ticket/registration/(:any)'] = "eventticketregistration/registration/$1";
$route['event_ticket/register'] = "eventticketregistration/register";
$route['event_ticket/v1/passes/(:any)/(:any)'] = "eventticketregistration/card_info_update/$1/$2";
$route['event_ticket/v1/devices/(:any)/registrations/(:any)'] = "eventticketregistration/registration_check/$1/$2";
$route["event_ticket/v1/devices/(:any)/registrations/(:any)/(:any)"] = "eventticketregistration/registration_callback/$1/$2/$3";

$route['membership/retrieve/(:any)'] = "registration/retrieve/$1";
$route['coupon/retrieve/(:any)'] = "couponregistration/retrieve/$1";
$route['event_ticket/retrieve/apple/(:any)'] = "eventticketregistration/retrieve_apple/$1";
//$route['event_ticket/retrieve/google/(:any)'] = "eventticketregistration/retrieve_google/(:any)";


$route['datatables/(:any)'] = 'admin/api/datatables/$1';
