<?php

function creation_data($post, $logo = null, $icon = null, $strip = null, $email_banner = null)
{
    $data = [
        "icon" => $icon['url'],
        "logo" => $logo['url'],
        "strip" => $strip['url'],
        "email_banner" => $email_banner['url'],
        "management_name" => $post['management_name'],
        "event_name" => $post['event_name'],
        "issuer_name" => $post['issuer_name'],
        "event_code" => isset($post['event_code']) ? $post['event_code']:"",
        //"event_time" => $post['event_time'],
        "event_start_time" => $post["event_start_time"],
        "event_end_time" => $post['event_end_time'],
        "event_date" => $post['event_date'],
        "event_venue" => $post['event_venue'],
        "location_detail" => $post["location_detail"],
        "label_color" => $post['label_color'],
        "content_color" => $post['content_color'],
        "background_color" => $post['background_color'],
        "logo_text" => $post['logo_text'],
        "data_requirement" => $post['data_requirement'],
        "event_type" => $post['event_type'],
        "url" => $post['url'],
        "about_us" => $post['about_us'],
        "terms_conditions" => $post['terms_conditions'],
        "event_ticket_type" => $post["event_ticket_type"]
    ];

    return $data;
}

function update_data($post, $logo = null, $icon = null, $strip = null, $email_banner = null)
{
    $data = [
        "management_name" => $post['management_name'],
        "event_name" => $post["event_name"],
        "issuer_name" => $post['issuer_name'],
        "event_code" => $post["event_code"],
        "event_start_time" => $post['event_start_time'],
        "event_end_time" => $post["event_end_time"],
        "event_date" => $post['event_date'],
        "event_venue" => $post['event_venue'],
        "location_detail" => $post["location_detail"],
        "label_color" => $post['label_color'],
        "content_color" => $post['content_color'],
        "background_color" => $post['background_color'],
        "logo_text" => $post['logo_text'],
        "data_requirement" => $post['data_requirement'],
        "url" => $post['url'],
        "about_us" => $post['about_us'],
        "terms_conditions" => $post['terms_conditions']
    ];
    if ($logo != null) {
        $data['logo'] = $logo['url'];
    }
    if ($icon != null) {
        $data['icon'] = $icon['url'];
    }
    if ($strip != null) {
        $data['strip'] = $strip['url'];
    }
    if ($email_banner != null) {
        $data['email_banner'] = $email_banner['url'];
    }
    return $data;
}

function form_data($post)
{
    $meta_info = ["name", "surname", "given_name", "english_name", "chinese_name", "gender", "birthday", "age", "nationality", "phone", "email", "address", "industry", "industry_2", "job_title", "company", "salary"];
    $simple_info_set = ["english_name", "chinese_name", "phone",  "email"];
    $data = [];
    if ($post['data_requirement'] == "simple") {
        foreach ($meta_info as $value) {
            $data[$value] = 0;
        }
        foreach ($simple_info_set as $value) {
            $data[$value] = 1;
        }
    } else {
        foreach ($meta_info as $value) {
            if (isset($post[$value])) {
                $data[$value] = 1;
            } else {
                $data[$value] = 0;
            }
        }
    }
    return $data;
}

function layout_data($post)
{
    $switches = ["front_data_1_status", "front_data_2_status", "front_data_3_status", "front_data_4_status", "front_data_5_status", "front_data_6_status"];
    $fields = ["front_data_1_data", "front_data_2_data", "front_data_3_data", "front_data_4_data", "front_data_5_data", "front_data_6_data", "front_data_1_label", "front_data_2_label", "front_data_3_label", "front_data_4_label", "front_data_5_label", "front_data_6_label"];
    $data = [];
    foreach ($switches as $switch) {
        if (isset($post[$switch])) {
            $data[$switch] = 1;
        } else {
            $data[$switch] = 0;
        }
    }
    foreach ($fields as $field) {
        $data[$field] = $post[$field];
    }
    return $data;
}
