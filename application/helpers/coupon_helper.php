<?php

function creation_data($params, $logo = null, $icon = null, $strip = null)
{
    $data = [
        "icon" => $icon != null ? $icon['url'] : "",
        "logo" => $logo != null ? $logo['url'] : "",
        "strip" => $strip != null ? $strip['url'] : "",
        "management_name" => $params['management_name'],
        "project_name" => $params['project_name'],
        "coupon_code" => $params["coupon_code"],
        "coupon_amount" => $params['coupon_amount'],
        "expiry_date" => $params['expiry_date'],
        "logo_text" => $params["logo_text"],
        "pri_color" => $params['pri_color'],
        "secondary_color" => $params["secondary_color"],
        "background_color" => $params['background_color'],
        "data_requirement" => $params["data_requirement"],
        "url" => $params['url'],
        "about_us" => $params["about_us"],
        "terms_conditions" => $params["terms_conditions"],
    ];
    return $data;
}

function update_data($params, $logo = null, $icon = null, $strip = null)
{
    $data = [
        "management_name" => $params['management_name'],
        "project_name" => $params['project_name'],
        "coupon_code" => $params['coupon_code'],
        "coupon_amount" => $params['coupon_amount'],
        "expiry_date" => $params['expiry_date'],
        "logo_text" => $params['logo_text'],
        "pri_color" => $params['pri_color'],
        "secondary_color" => $params['secondary_color'],
        "background_color" => $params['background_color'],
        "data_requirement" => $params['data_requirement'],
        "url" => $params['url'],
        "about_us" => $params["about_us"],
        "terms_conditions" => $params['terms_conditions']
    ];
    if ($logo != null) {
        $data["logo"] = $logo['url'];
    }
    if ($icon != null) {
        $data['icon'] = $icon['url'];
    }
    if ($strip != null) {
        $data['strip'] = $strip['url'];
    }
    return $data;
}

function process_locations($params)
{
    $switches = ["location_1_status", "location_2_status", "location_3_status",];
    $fields = ["location_1_lat", "location_1_lon", "location_1_message", "location_2_lat", "location_2_lon", 'location_2_message', "location_3_lat", "location_3_lon", "location_3_message"];
    $data = [];
    foreach ($switches as $switch) {
        if (isset($params[$switch])) {
            $data[$switch] = 1;
        } else {
            $data[$switch] = 0;
        }
    }
    foreach ($fields as $field) {
        $data[$field] = $params[$field];
    }
    return $data;
}

function process_form($post)
{
    $meta_info = ["name", "surname", "given_name", "english_name", "chinese_name", "gender", "birthday", "age", "nationality", "phone", "email", "address", "industry", "industry_2", "job_title", "company", "salary"];
    $simple_info_set = ["english_name", "chinese_name", "phone",  "email"];
    $data = [];
    if ($post['data_requirement'] == "simple") {
        foreach ($meta_info as $value) {
            $data[$value] = 0;
        }
        foreach ($simple_info_set as $value) {
            $data[$value] = 1;
        }
    } else {
        foreach ($meta_info as $value) {
            if (isset($post[$value])) {
                $data[$value] = 1;
            } else {
                $data[$value] = 0;
            }
        }
    }
    return $data;
}

function process_layout($post)
{
    $switches = ["front_data_1_status", "front_data_2_status", "front_data_3_status", "front_data_4_status"];
    $fields = ["front_data_1_data", "front_data_2_data", "front_data_3_data", "front_data_4_data", "front_data_1_label", "front_data_2_label", "front_data_3_label", "front_data_4_label"];
    $data = [];
    foreach ($switches as $switch) {
        if (isset($post[$switch])) {
            $data[$switch] = 1;
        } else {
            $data[$switch] = 0;
        }
    }
    foreach ($fields as $field) {
        $data[$field] = $post[$field];
    }
    return $data;
}
