<?php

/**
 * class CommonHelper
 */
class CommonHelper
{
    private static $fullUrl;

    public static function array_put(&$res, $key, $values)
    {
        if (empty($res[$key])) {
            $res[$key] = [];
        }
        array_push($res[$key], $values);
    }

    public static function dump($var, $exit = 0)
    {
        echo "<pre>";
        var_dump($var);
        echo "</pre>";

        if ($exit) {
            exit('EOF');
        }
    }

    public static function hello()
    {
        echo "Hi! I am here.\n";
    }

    /**
     * Get UNIX Date - Timestamp
     *
     * @see https://www.php.net/manual/en/datetime.gettimestamp.php
     *
     * @return int
     */
    public static function getUNIXDate()
    {
        return (new DateTime())->getTimestamp();
    }

    public static function getChineseYearMonth($date)
    {
        $ymd = Carbon::parse($date)->format('Y-m-d');
        list($year, $month, $day) = explode('-', $ymd);
        return sprintf('%s年%s月份', $year, $month);
    }

    public static function getChineseMonthDay($date)
    {
        $ymd = Carbon::parse($date)->format('Y-m-d');
        list($year, $month, $day) = explode('-', $ymd);
        return sprintf('%s月%s日', $month, $day);
    }

    public static function getChineseDate($date)
    {
        $ymd = Carbon::parse($date)->format('Y-m-d');
        list($year, $month, $day) = explode('-', $ymd);
        return sprintf('%s年%s月%s日', $year, $month, $day);
    }

    public static function getChineseTime($format, $time)
    {
        return str_replace(['am', 'pm'], ['上午', '下午'], Carbon::createFromFormat($format, $time)->format('a g:i'));
    }

    public static function getChineseTimestamp($timestamp)
    {
        return str_replace(['am', 'pm'], ['上午', '下午'], Carbon::createFromTimestamp($timestamp)->format('Y-m-d a g:i'));
    }

    public static function getChineseDateTime($datetime)
    {
        return str_replace(['am', 'pm'], ['上午', '下午'], Carbon::parse($datetime)->format('Y-m-d a g:i'));
    }

    public static function getChineseDateAndTime($date, $time)
    {
        return str_replace(['am', 'pm'], ['上午', '下午'], Carbon::createFromFormat('Y-m-d H:i', sprintf('%s %s', $date, $time))->format('Y-m-d a g:i'));
    }

    public static function ellipsis($text, $length = 60)
    {
        return strlen($text) > $length ? substr($text, 0, $length) . '...' : $text;
    }

    public static function getAmpm($time, $ampm = false, $spaces = true)
    {
        list($hour, $minute) = explode(':', $time);
        $format = $ampm ? ($spaces ? 'g:i a' : 'g:ia') : 'g:i';
        return Carbon::now()->setTime($hour, $minute)->format($format);
    }

    public static function getDateFromTime($time, $format = 'Y-m-d H:i:s')
    {
        list($hour, $minute) = explode(':', $time);
        $t = Carbon::now()->setTime($hour, $minute);
        if ($format) {
            return $t->format($format);
        } else {
            return $t->timestamp;
        }
    }

    public static function getDateFormat($unixDate, $format = 'Y-m-d')
    {
        if (!is_int($unixDate)) {
            throw new Exception('$unixDate must be in integer type.');
        }
        return date($format, $unixDate);
    }

    /**
     * @param null|string $date     can be empty (now) OR 'Y-m-d'
     * @param string $time          time format should be in "H:i" / "H:i:s"
     * @param int $duration         duration in minute unit
     * @param string $format        output
     * @return string
     */
    public static function getEndDateByDuration($date = null, $time, $duration, $format = 'H:i')
    {
        $date = $date ? Carbon::parse($date) : Carbon::now();
        list($hour, $minute) = explode(':', $time);
        return $date->setTime($hour, $minute)->addMinute($duration)->format($format);
    }

    public static function getDefaultYear()
    {
        return date('Y');
    }

    public static function getYearSelection($multiDimension = true, $year = null)
    {
        $years = [];
        $year = $year ?: date('Y');
        for ($i = $year; $i > $year - 12; $i--) {
            if ($multiDimension) {
                $years[] = [
                    'id' => $i,
                    'name' => Transformer::instance()->getYearProperty($i)
                ];
            } else {
                $years[$i] = Transformer::instance()->getYearProperty($i);
            }
        }

        return $years;
    }

    public static function getDefaultMonth()
    {
        return date('m');
    }

    public static function getMonthSelection($multiDimension = true, $prefix = null)
    {
        $months = [];
        for ($i = 1; $i <= 12; $i++) {
            if ($multiDimension) {
                $months[] = [
                    'id' => $i,
                    'name' => $prefix . Transformer::instance()->getDateFProperty(date("F", mktime(0, 0, 0, $i)))
                ];
            } else {
                $months[$i] = $prefix . Transformer::instance()->getDateFProperty(date("F", mktime(0, 0, 0, $i)));
            }
        }
        return $months;
    }

    public static function getMoneyFormat($x)
    {
        return sprintf("$%s", number_format($x, 2, '.', ','));
    }

    public static function getHost()
    {
        $host = $_SERVER['HTTP_HOST'];
        $protocol = $_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';
        return $protocol . '://' . $host;
    }

    public static function setFullUrl($fullUrl = null)
    {
        self::$fullUrl = $fullUrl ?: self::getFullUrl();
    }

    public static function getFullUrl()
    {
        return self::$fullUrl ?: self::getHost() . $_SERVER['REQUEST_URI'];
    }

    public static function getRequestController()
    {
        return Request::instance()->controller();
    }

    public static function getRequestAction()
    {
        return Request::instance()->action();
    }

    public static function getHttpReferer()
    {
        return isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
    }

    public static function notCreateUpdateRequest()
    {
        return !in_array(Request::instance()->action(), ['add', 'edit']);
    }

    public static function isCreateRequest()
    {
        return Request::instance()->action() === 'add';
    }

    public static function isReadRequest()
    {
        return Request::instance()->action() === 'read';
    }

    public static function isUpdateRequest()
    {
        return Request::instance()->action() === 'edit';
    }

    public static function precedingZeros($zeroDigits, $num)
    {
        return sprintf('%0' . ++$zeroDigits . 'd', $num);
    }

    //---Configuration Helpers

    public static function isProductionEnv()
    {
        return config('app.app_env') == 'production';
    }

    //---String Helpers

    /**
     * @param string $value
     * @return int
     */
    public static function string2Boolean($value)
    {
        switch (true) {
            case $value == null:
                return 0;
            case is_int($value):
                return $value === 1 ? 1 : 0;
            default:
                return filter_var($value, FILTER_VALIDATE_BOOLEAN) === true ? 1 : 0;
        }
    }

    // @see https://stackoverflow.com/questions/1993721/how-to-convert-pascalcase-to-pascal-case
    public static function stringCamel2Snake($input)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }

    //---Array Helpers

    public static function arrayWithLabel($array, $label)
    {

        return ['' => $label] + $array;
    }

    /**
     * Just a useful version which returns a simple array with the first key and value. Porbably a better way of doing it, but it works for me ;-)
     * @link https://stackoverflow.com/questions/3317856/php-get-key-from-array
     * 
     * @param $arr
     * @return array|null
     */
    public static function arrayShift(&$arr)
    {
        if (empty($arr)) {
            return null;
        }
        list($k) = array_keys($arr);
        $r  = array($k => $arr[$k]);
        unset($arr[$k]);
        return $r;
    }

    /**
     * Naming conventions: inArray => arrayFindByValue
     * 
     * return array of key and value ON THE FIRST MATCH OCCURENCE, else false
     *
     * @param string $needle
     * @param array $arr
     * @return array|false
     */
    public static function arrayFindByValue($needle, $arr)
    {
        if (false !== $key = array_search($needle, $arr)) {
            return ['key' => $key, 'value' => $arr[$key]];
        } else {
            return false;
        }
    }

    /**
     * @param string $needle
     * @param Collection $collection
     * @return void
     */
    public static function map($needle, Collection $collection)
    {
        $temp = [];
        $collection->each(function (Model $data) use (&$temp, $needle) {
            $temp[$data->$needle] = $data;
        });

        return $temp;
    }


    public static function array2CsvDownload($array, $filename = "export.csv", $delimiter = ";")
    {
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="' . $filename . '";');

        // open the "output" stream
        // see http://www.php.net/manual/en/wrappers.php.php#refsect2-wrappers.php-unknown-unknown-unknown-descriptioq
        $f = fopen('php://output', 'w');

        foreach ($array as $line) {
            fputcsv($f, $line, $delimiter);
        }
    }

    public static function databaseName($name = null)
    {
        $name = $name ?: Tenants::instance()->getConnectedTenant();

        return Env::get('database_prefix') .  $name .  Env::get('database_suffix');
    }

    public static function logCron($text = null)
    {
        $text ?: sprintf('正在分析 %s。', self::databaseName());
        Log::record($text, 'cron');
    }

    public static function logError(Exception $e, $file = null)
    {
        $file = $file ?: 'error';
        /**
         * @link https://www.kancloud.cn/manual/thinkphp5_1/354093
         */
        Log::record(sprintf('[%s on line %d]', $e->getFile(), $e->getLine()), $file);
        Log::record(sprintf('[%s] %s', $e->getCode(), $e->getMessage()), $file);
        foreach (explode(PHP_EOL, $e->getTraceAsString()) as $trace) {
            Log::record(sprintf('%s', $trace), $file);
        }
    }

    public static function logCronError(Exception $e)
    {
        $text = [];
        /**
         * @link https://www.kancloud.cn/manual/thinkphp5_1/354093
         */
        Log::error(sprintf('[%s on line %d]', $e->getFile(), $e->getLine()), 'cron');
        array_push($text, sprintf('[%s on line %d]', $e->getFile(), $e->getLine()));

        Log::error(sprintf('[%s] %s', $e->getCode(), $e->getMessage()), 'cron');
        array_push($text, sprintf('[%s] %s', $e->getCode(), $e->getMessage()));

        foreach (explode(PHP_EOL, $e->getTraceAsString()) as $trace) {
            Log::error(sprintf('%s', $trace));
            array_push($text, sprintf('%s', $trace));
        }

        return implode('', $text);
    }
}
