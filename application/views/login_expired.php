<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo "O2O C3C CMS - 帳戶過期"; ?></title>
    <link href="<?= base_url('public/static/backend/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!--    <link rel="stylesheet" href="/assets/css/cmsstyle.css">-->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900"-->
    <link rel="stylesheet" href="<?= base_url('public/static/backend/css/googlecatamariancss.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('public/static/backend/css/googlelato.css'); ?>">
    <!--    <link rel="stylesheet" href="/assets/at-admin/css/breadcrumbs.css">-->
    <link rel="stylesheet" href="<?= base_url('public/static/backend/css/systemcss.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('public/static/backend/css/admin.css?v=' . rand(10000, 100000)); ?>">
    <link rel="stylesheet" href="<?= base_url('public/static/backend/css/pages/login.css?v=' . rand(10000, 100000)); ?>">
    <link rel="stylesheet" href="<?= base_url('public/static/backend/css/layouts/o2o-c3c.css?v=' . rand(10000, 100000)); ?>">
    <meta name="description" content="">
    <meta name="author" content="">
</head>

<body style="background-image:url('<?= base_url("assets/images/bg-login-image.png") ?>');background-size:100% 100%;background-repeat:repeat-y;">
    <div class="container" style="height:100%;display:flex;align-items:center;justify-content:center;flex-wrap:wrap;">

        <h4>Your account has expired on <?= $expired_date ?>. Please <a href="mailto:info@smartbusiness.com.hk">contact us</a> to renew your account.</h4>

        
        

    </div>
</body>

</html>