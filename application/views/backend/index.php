﻿<!DOCTYPE html>
<html>

<head>
    <?php
    $this->load->view('backend/include/common');
    ?>
</head>

<body>
    <!-- Loading Container -->
    <div class="loading-container<?php echo $_inactive_loading ? ' loading-inactive' : '' ?>">
        <div class="loader"></div>
    </div>
    <!--  /Loading Container -->
    <!-- Navbar -->
    <div class="o2o-c3c navbar">
        <div class="navbar-inner">
            <div class="navbar-container">
                <!-- Navbar Barnd -->
                <div class="navbar-header pull-left">
                    <a href="#" class="navbar-brand">
                        <small>
                            <img class="imglogo" src="<?php echo base_url(); ?>assets/images/logo.png" alt="" />
                        </small>
                    </a>
                </div>
                <!-- /Navbar Barnd -->
                <!-- Sidebar Collapse -->
                <div class="sidebar-collapse" id="sidebar-collapse">
                    <i class="collapse-icon fa fa-bars"></i>
                </div>
                <div id="navbar-app-name"><?= $page ?></div>
                <!-- /Sidebar Collapse -->
                <!-- Account Area and Settings --->
                <?php
                $username = $this->session->userdata('username') ?? '管理員';
                $email = $this->session->userdata('email');
                $avatar = empty($this->session->userdata('image')) ? base_url('public/static/backend/images/backend/icon-admin-user.png') : base_url($this->session->userdata('image'));
                ?>
                <div class="navbar-header pull-right">
                    <div class="navbar-account">
                        <ul class="account-area">
                            <li>
                                <a class="login-area dropdown-toggle" data-toggle="">
                                    <section>
                                        <h2>
                                            <span class="profile">hello，<?php echo $username; ?>!</span>
                                        </h2>
                                    </section>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /Account Area and Settings -->
            </div>
        </div>
    </div>
    <!-- /Navbar -->
    <!-- Main Container -->
    <!-- main content starts -->
    <div class="o2o-c3c main-container container-fluid main-admincontent">
        <!-- Page Container -->
        <div class="page-container">
            <!-- Page Sidebar -->
            <?php
            $this->load->view('backend/include/menu');
            ?>
            <!-- /Page Sidebar -->
            <!-- Chat Bar -->
            <?php
            //        $this->load->view('at-admin/include/chatbar');
            ?>
            <!-- /Chat Bar -->
            <!-- Page Content -->
            <div class="page-content">
                <?php
                if (isset($_view) && $_view)
                    //var_dump($_view);
                    $this->load->view('backend/' . $_view);
                ?>
            </div>
            <!-- /Page Content -->
        </div>
        <!-- /Page Container -->
        <!-- Main Container -->
    </div>
    <!-- main content ends -->
    <?php
    $this->load->view('backend/include/footer');
    ?>