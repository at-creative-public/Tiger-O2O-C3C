<!--<input id="sidebar-toggle-checkbox" type="checkbox" name="" />
<span id="sidebar-toggle"></span>-->
<div class="page-sidebar" id="sidebar">
    <!-- Page Sidebar Header-->
    <!-- <div class="sidebar-header-wrapper">
        <input type="text" class="searchinput" />
        <i class="searchicon fa fa-search"></i>
        <div class="searchhelper">搜索</div>
    </div> -->
    <ul class="nav sidebar-menu">
        <!-- <li>
            <a class="menu-dropdown" href="<?= base_url('backend/dashboard'); ?>">
                <i class="menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont"></i>
                <span class="menu-text">儀表板</span>
            </a>
        </li>
        <li>
            <a class="menu-dropdown" href="<?= base_url('/backend/landingsubmenu'); ?>">
                <i class="menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont"></i>
                <span class="menu-text">網站次目錄</span>
            </a>
        </li>
        <li>
            <a class="menu-dropdown" href="<?= base_url('/backend/home_setting'); ?>">
                <i class="menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont"></i>
                <span class="menu-text">首頁Banner設定</span>
            </a>
        </li>
        <li>
            <a class="menu-dropdown" href="<?= base_url('/backend/video_setting'); ?>">
                <i class="menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont"></i>
                <span class="menu-text">教育短片管理</span>
            </a>
        </li>
        <li>
            <a class="menu-dropdown" href="<?= base_url('/backend/appointment_setting'); ?>">
                <i class="menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont"></i>
                <span class="menu-text">查詢記錄</span>
            </a>
        </li> -->
        <!-- <li>
                <a class="menu-dropdown" href="<?= base_url('/backend/home_setting'); ?>">
                    <i class="menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont"></i>
                    <span class="menu-text">滑動式橫額</span>
                </a>
            </li>
            <li>
                <a class="menu-dropdown" href="<?= base_url('/backend/landingmain'); ?>">
                    <i class="menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont"></i>
                    <span class="menu-text">網站主目錄</span>
                </a>
            </li> -->
        <!-- <li>
            <a class="menu-dropdown" href="<?= base_url('/admin_user'); ?>">
                <i class="menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont"></i>
                <span class="menu-text">管理員帳戶</span>
            </a>
        </li> -->
        <!-- <li>
            <a class="menu-dropdown" href="<?= base_url('/member'); ?>">
                <i class="menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont"></i>
                <span class="menu-text">會員帳戶</span>
            </a>
        </li> -->
        <!-- <li>
            <a class="menu-dropdown" href="<?= base_url('/import_member'); ?>">
                <i class="menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont"></i>
                <span class="menu-text">會員導入導出</span>
            </a>
        </li> -->
        <!-- <li>
            <a class="menu-dropdown" href="<?= base_url('/backend/booking'); ?>">
                <i class="menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont"></i>
                <span class="menu-text">時間表</span>
            </a>
        </li> -->
        <?php
        $menu_items = [
            ['url' => '/backend/dashboard', 'icon' => 'menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont', 'text' => 'Dashboard'],
            ['url' => '/backend/merchant', 'icon' => 'menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont', 'text' => 'Merchant Management'],
            ['url' => '/backend/merchant/add', 'icon' => 'menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont', 'text' => 'Create New Account'],
            ['url' => '/backend/signout', 'icon' => 'menu-icon mr-1 glyphicon glyphicon-log-out', 'text' => 'Logout'],
        ];
        ?>
        <?php foreach ($menu_items as $menu_item) : ?>
            <?php $pos = strpos(current_url(), $menu_item['url']); ?>
            <?php $last_word = $pos + strlen($menu_item['url']) == strlen(current_url()); ?>
            <li <?= $pos !== false && $last_word ? ' class="active"' : '' ?>>
                <a class="menu-dropdown" href="<?= base_url() . $menu_item['url']; ?>">
                    <i class="<?= $menu_item['icon'] ?>"></i>
                    <span class="menu-text"><?= $menu_item['text'] ?></span>
                </a>
            </li>
        <?php endforeach; ?>

        <!-- <li>
            <a class="menu-dropdown" href="<?= base_url('/backend/contactus_record_setting'); ?>">
                <i class="menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont"></i>
                <span class="menu-text">查詢記錄</span>
            </a>
        </li>  -->
        <!-- <li>
            <a class="menu-dropdown" href="<?= base_url('/backend/site_management'); ?>">
                <i class="menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont"></i>
                <span class="menu-text">網頁內容管理</span>
            </a>
        </li> -->
        <!-- <li>
            <a class="menu-dropdown" href="<?= base_url('/backend/news'); ?>">
                <i class="menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont"></i>
                <span class="menu-text">活動及消息管理</span>
            </a>
        </li> -->
        <!-- <li>
            <a class="menu-dropdown" href="<?= base_url('/backend/contact_us_setting') ?>">
                <i class="menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont"></i>
                <span class="menu-text">聯絡我們設定</span>
            </a>
        </li> -->
        <!-- <li>
            <a class="menu-dropdown" href="<?= base_url('/backend/footer_management'); ?>">
                <i class="menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont"></i>
                <span class="menu-text">頁尾內容管理</span>
            </a>
        </li> -->
        <!--
        <li>
            <a class="menu-dropdown" href="#">
                <i class="menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont"></i>
                <span class="menu-text">客戶查詢管理</span>
                <i class="menu-expand"></i>
            </a>
            <ul class="submenu">
                <li>
                    <a class="menu-dropdown" href="<?= base_url('/backend/contact_us_item_management'); ?>">
                        <span class="menu-text">查詢項目管理</span>
                    </a>
                </li>
                <li>
                    <a class="menu-dropdown" href="<?= base_url('/backend/customer_contact'); ?>">
                        <span class="menu-text">客戶查詢管理</span>
                    </a>
                </li>
            </ul>
        </li>-->
        <!--        <li>-->
        <!--            <a class="menu-dropdown" href="--><? //=base_url('/at-admin/settings');
                                                            ?>
        <!--">-->
        <!--                <i class="menu-icon fas fa-cog mr-2 customicon customsidefont"></i>-->
        <!--                <span class="menu-text">設定</span>-->
        <!--            </a>-->
        <!--        </li>-->
        <!--
        <li>
            <a class="menu-dropdown" href="<?= base_url('#'); ?>">
                <i class="menu-icon fas fa-tachometer-alt mr-2 customicon customsidefont"></i>
                <span class="menu-text">SEO設定</span>
            </a>
        </li> -->
        <!-- <li>
            <a class="menu-dropdown" href="<?= base_url('backend/signout'); ?>">
                <i class="menu-icon mr-1 glyphicon glyphicon-log-out"></i>
                <span class="menu-text" style="position: relative;">登出</span>
            </a>
        </li> -->
    </ul>
</div>