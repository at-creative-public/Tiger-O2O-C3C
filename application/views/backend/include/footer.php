<!--Beyond Scripts-->
<script src="<?= base_url() ?>public/static/backend/js/beyond.js"></script>


<!--Page Related Scripts-->
<!--Sparkline Charts Needed Scripts-->
<script src="<?= base_url() ?>public/static/beyond-admin/js/charts/sparkline/jquery.sparkline.js"></script>
<script src="<?= base_url() ?>public/static/beyond-admin/js/charts/sparkline/sparkline-init.js"></script>

<!--Easy Pie Charts Needed Scripts-->
<script src="<?= base_url() ?>public/static/beyond-admin/js/charts/easypiechart/jquery.easypiechart.js"></script>
<script src="<?= base_url() ?>public/static/beyond-admin/js/charts/easypiechart/easypiechart-init.js"></script>

<script src="<?= base_url() ?>public/static/backend/js/admin.js?v=<?= rand(10000, 100000) ?>"></script>

<div class="d-flex flex-column min-vh-100">
    <nav>
    </nav>
    <main class="flex-fill">
    </main>
    <footer>
        &copy; Copyright 2021 Smartbiz
    </footer>
</div>

</body>
<!--  /Body -->

<!-- Mirrored from beyondadmin-v1.4.s3-website-us-east-1.amazonaws.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 May 2015 08:22:34 GMT -->

</html>