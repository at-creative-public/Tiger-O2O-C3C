<div class="page-body">
    <div class="row pl-0 pl-lg-5">
        <div class="col-12">
            <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5"><?= $page ?></h4>
        </div>
        <div class="col-12">
            <?= form_open() ?>
            <div class="row">
                <div class="col-12 col-md-6">
                    <section id="merchant-information">
                        <div class="title pt-4 pb-5 font-weight-bold">Merchant Information</div>
                        <div class="form-group">
                            <?= form_label('Company Name*', 'company_name') ?>
                            <?= form_input('company_name', '', 'id="company_name" class="form-control" required') ?>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Company Email*', 'company_email') ?>
                                    <?= form_input('company_email', '', 'id="company_email" class="form-control" required') ?>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Company Hotline*', 'company_hotline') ?>
                                    <?= form_input('company_hotline', '', 'id="company_hotline" class="form-control" required') ?>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Industry*', 'industry') ?>
                                    <select id="industry" name="industry" class="form-control">
                                        <option value="" disabled selected>--Please Select--</option>
                                        <option value="artwork">Artworks & Crafts</option>
                                        <option value="automobile">Automobiles</option>
                                        <option value="beauty">Beauty & Salon</option>
                                        <option value="ecommerce">eCommerce</option>
                                        <option value="fashion">Fashion & Textiles</option>
                                        <option value="finance">Finance</option>
                                        <option value="food">Food & Beverages</option>
                                        <option value="furniture">Furnitures</option>
                                        <option value="health">Healthcare Household Goods</option>
                                        <option value="hotel">Hotel & Tourism</option>
                                        <option value="industrial">Industrials & Construction</option>
                                        <option value="it">Information Technology</option>
                                        <option value="logistics">Logistics</option>
                                        <option value="media">Media & Entertainment</option>
                                        <option value="clubhouse">Membership Clubhouse</option>
                                        <option value="properties">Properties</option>
                                        <option value="retail">Retail</option>
                                        <option value="service">Service Sector</option>
                                        <option value="spa">Spa/Gym</option>
                                        <option value="support">Support Services</option>
                                        <option value="telecommunications">Telecommunications</option>
                                        <option value="travel">Travel & Leisure</option>
                                        <option value="others">Others</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Company Website', 'company_website') ?>
                                    <?= form_input('company_website', '', 'id="company_website" class="form-control"') ?>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="account-setting">
                        <div class="title pt-4 pb-4 font-weight-bold">Account Setting</div>
                        <div class="form-group">
                            <?= form_label('Merchant ID*', 'merchant_id') ?>
                            <?= form_input('merchant_id', '', 'id="merchant_id" class="form-control" required') ?>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Password*', 'password') ?>
                                    <?= form_password('password', '', 'id="password" class="form-control" required') ?>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Confirm Password*', 'confirm_password') ?>
                                    <?= form_password('confirm_password', '', 'id="confirm_password" class="form-control" required') ?>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <div><?= form_label('Plan*', 'plan') ?></div>
                                    <select name="plan">
                                        <option value="" disabled selected>--Please Select--</option>
                                        <option value="Enterprise">Enterprise</option>
                                        <option value="Pro">Pro</option>
                                        <option value="Lite">Lite</option>
                                    </select>
                                    <i class="fas fa-info-circle"></i>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Valid Until*', 'valid_until') ?>
                                    <?= form_input('valid_until', '', 'id="valid_until" class="form-control" required') ?>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-12 col-md-6">
                    <section id="staff-information">
                        <div class="title pt-4 pb-4 font-weight-bold">Staff Information</div>
                        <div class="form-group">
                            <?= form_label('Contact Person*', 'contact_person') ?>
                            <?= form_input('contact_person', '', 'id="contact_person" class="form-control" required') ?>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Title*', 'title') ?>
                                    <select name="title" id="title" class="form-control">
                                        <option value="" disabled selected>--Please Select--</option>
                                        <option value="Mr">Mr</option>
                                        <option value="Mrs">Mrs</option>
                                        <option value="Ms">Ms</option>
                                        <option value="Miss">Miss</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Position*', 'position') ?>
                                    <?= form_input('position', '', 'id="position" class="form-control" required') ?>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Phone*', 'phone') ?>
                                    <?= form_input('phone', '', 'id="phone" class="form-control" required') ?>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Email*', 'email') ?>
                                    <?= form_input('email', '', 'id="email" class="form-control" required') ?>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="related-documents">
                        <div class="title pt-4 pb-4 font-weight-bold">Related Documents</div>
                        <button onclick="">UPLOAD FILE</button>
                        <span class="upload-text red">(Maximum 5 items, 10MB)</span>
                    </section>
                </div>
            </div>
            <?= form_submit('submit', 'Submit', 'class="btn btn-primary mt-4 white"') ?>
            <?= form_close() ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('form').submit(function(e) {
            e.preventDefault();
        });
    });
</script>