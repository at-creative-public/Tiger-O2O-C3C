<style>
    select {
        -webkit-appearance: none;
    }
</style>
<div class="page-body">
    <div class="row pl-0 pl-xl-5">
        <div class="col-12">
            <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5"><?= $page ?></h4>
        </div>
        <div class="col-12">
            <?php $url = isset($id) ? 'backend/merchant/submit?id=' . $id : 'backend/merchant/submit'; ?>
            <?= form_open($url, ['enctype' => 'multipart/form-data']) ?>
            <div class="row">
                <div class="col-12 col-md-6">
                    <section id="merchant-information">
                        <div class="title pt-4 pb-5 font-weight-bold">Merchant Information</div>
                        <div class="form-group">
                            <?= form_label('Company Name*', 'company_name') ?>
                            <?= form_input('company_name', set_value('company_name', $company_name ?? ''), 'id="company_name" class="form-control" required') ?>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Company Email*', 'company_email') ?>
                                    <input type="email" id="company_email" name="company_email" class="form-control" value="<?= set_value('company_email', $company_email ?? '') ?>" required>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Company Hotline*', 'company_hotline') ?>
                                    <?= form_input('company_hotline', set_value('company_hotline', $company_hotline ?? ''), 'id="company_hotline" class="form-control" required') ?>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Industry*', 'company_industry') ?>
                                    <select id="company_industry" name="company_industry" class="form-control" data-selected="<?= set_value('company_industry', $company_industry ?? '') ?> required">
                                        <option value="" disabled selected>--Please Select--</option>
                                        <option value="artwork">Artworks & Crafts</option>
                                        <option value="automobile">Automobiles</option>
                                        <option value="beauty">Beauty & Salon</option>
                                        <option value="ecommerce">eCommerce</option>
                                        <option value="fashion">Fashion & Textiles</option>
                                        <option value="finance">Finance</option>
                                        <option value="food">Food & Beverages</option>
                                        <option value="furniture">Furnitures</option>
                                        <option value="health">Healthcare Household Goods</option>
                                        <option value="hotel">Hotel & Tourism</option>
                                        <option value="industrial">Industrials & Construction</option>
                                        <option value="it">Information Technology</option>
                                        <option value="logistics">Logistics</option>
                                        <option value="media">Media & Entertainment</option>
                                        <option value="clubhouse">Membership Clubhouse</option>
                                        <option value="properties">Properties</option>
                                        <option value="retail">Retail</option>
                                        <option value="service">Service Sector</option>
                                        <option value="spa">Spa/Gym</option>
                                        <option value="support">Support Services</option>
                                        <option value="telecommunications">Telecommunications</option>
                                        <option value="travel">Travel & Leisure</option>
                                        <option value="others">Others</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Company Website', 'company_website') ?>
                                    <?= form_input('company_website', set_value('company_website', $company_website ?? ''), 'id="company_website" class="form-control"') ?>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="account-setting">
                        <div class="title pt-4 pb-5 font-weight-bold">Account Setting</div>
                        <div class="form-group">
                            <?= form_label('Merchant ID*', 'merchant_id') ?>
                            <?= form_input('merchant_id', set_value('merchant_id', $merchant_id ?? ''), 'id="merchant_id" class="form-control" required') ?>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Password*', 'merchant_password') ?>
                                    <input type="password" name="merchant_password" id="merchant_password" class="form-control" autocomplete="new-password" value="<?= set_value('password', $merchant_password ?? '') ?>" required>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Confirm Password*', 'confirm_password') ?>
                                    <?= form_password('confirm_password', set_value('confirm_password', $merchant_password ?? ''), 'id="confirm_password" class="form-control" required') ?>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <div><?= form_label('Plan*', 'merchant_plan') ?></div>
                                    <div class="row">
                                        <div class="col pr-0">
                                            <select name="merchant_plan" class="form-control" data-selected="<?= set_value('merchant_plan', $merchant_plan ?? '') ?>" required>
                                                <option value="" disabled selected>--Please Select--</option>
                                                <option value="Enterprise">Enterprise</option>
                                                <option value="Pro">Pro</option>
                                                <option value="Lite">Lite</option>
                                            </select>
                                        </div>
                                        <div id="plan_info" class="col-auto m-auto clickable">
                                            <i class="fas fa-info-circle"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Valid Until*', 'merchant_valid_until') ?>
                                    
                                    <input type="date" name="merchant_valid_until" id="merchant_valid_until" class="form-control" value="<?= set_value('merchant_valid_until', date('Y-12-31')) ?>" required>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-12 col-md-6">
                    <section id="staff-information">
                        <div class="title pt-4 pb-5 font-weight-bold">Staff Information</div>
                        <div class="form-group">
                            <?= form_label('Contact Person*', 'contact_person') ?>
                            <?= form_input('contact_person', set_value('company_hotline', $contact_person ?? ''), 'id="contact_person" class="form-control" required') ?>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Title*', 'contact_title') ?>
                                    <select name="contact_title" id="contact_title" class="form-control" data-selected="<?= set_value('contact_title', $contact_title ?? '') ?>" required>
                                        <option value="" disabled selected>--Please Select--</option>
                                        <option value="Mr">Mr</option>
                                        <option value="Mrs">Mrs</option>
                                        <option value="Ms">Ms</option>
                                        <option value="Miss">Miss</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Position', 'contact_position') ?>
                                    <?= form_input('contact_position', set_value('contact_position', $contact_position ?? ''), 'id="contact_position" class="form-control"') ?>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Phone*', 'contact_phone') ?>
                                    <?= form_input('contact_phone', set_value('contact_phone', $contact_phone ?? ''), 'id="contact_phone" class="form-control" required') ?>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Email*', 'contact_email') ?>
                                    <input type="email" id="contact_email" name="contact_email" class="form-control" value="<?= set_value('contact_email', $contact_email ?? '') ?>" required>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="related_documents" class="pb-5" data-files='<?= json_encode($related_documents ?? []) ?>' data-createdat="<?= $created_at ?? 0?>">
                        <div class="heading text-accent font-weight-bold pb-3">Related Documents</div>
                        <div class="row align-items-center">
                            <div class="col-auto pb-4">
                                <input type="file" id="related_documents_upload" name="upload[]" multiple data-filesize="0" data-filecount="0" style="width: 1px; height: 1px !important;" />
                                <label class="btn btn-upload" for="related_documents_upload">UPLOAD FILE</label>
                            </div>
                            <div class="col-auto">
                                <span class="upload-text red ml-2">(Maximum 5 items, 10MB)</span>
                            </div>
                        </div>
                        <div class="content-list ui-sortable mt-3 pt-3 border-top"></div>
                    </section>
                </div>
            </div>
            <?php if (isset($id)) {
                echo form_hidden('id', $id);
                echo form_hidden('company_id', $company_id);
                echo form_hidden('contact_id', $contact_id);
            } ?>
            <?= form_submit('', 'Submit', 'class="btn btn-primary mt-4 white"') ?>
            <?= form_close() ?>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="viewModel" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg mx-auto" role="document">
        <div class="modal-content bg-accent">
            <div class="modal-content-wrapper">
                <div class="modal-header p-0 text-white bg-transparent border-0">
                    <div class="row align-items-center w-100">
                        <div class="col">
                            <h5 class="modal-title uppercase font-weight-bold">O2O C3C SYSTEM SERVICE PLAN</h5>
                        </div>
                        <div class="col-12 col-md-auto pt-3 pt-md-0">
                            <input type="file" id="poster" name="poster" style="width: 1px; height: 1px !important;" />
                            <label class="btn rounded text-accent bg-white pt-2 pb-2 pl-4 pr-4 mr-5" for="poster">UPDATE</label>
                        </div>
                    </div>
                    <button type="button" class="close text-white p-0" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mt-5 p-5 bg-white modal-scrollable modal-hide-scrollbar" style="height: 75vh; overflow: overlay;">
                    <div class="row w-100" id="poster_wrapper">
                        <img class="img-fluid mx-auto" src="<?= base_url() ?>assets/uploads/plan_poster.png">
                    </div>
                </div>
                <div class="modal-footer hide">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        sortable($('#related_documents .content-list'));

        // ad-hoc fix for hard-code select dropdown
        $('form select').each(function(index, elem) {
            $(elem).val($(elem).data('selected'));
        });
        $('#plan_info').click(function(e) {
            $('#viewModel').modal('show');
        });

        $('#poster').on("change", function(e) {
            const input = e.currentTarget;
            // FileList object
            const fileList = input.files;
            const file = input.files.item(0);

            let formData = new FormData();
            formData.append("poster", file);
            $('#poster').next().addClass('disabled').text('UPDATING ...');
            fetch("/backend/api/upload_plan_poster", {
                    method: 'POST',
                    body: formData
                }).then(res => res.json())
                .then(response => {
                    $('#poster_wrapper').find('img').remove();
                    $('#poster_wrapper').append(`<img class="img-fluid mx-auto" src="/assets/uploads/plan_poster.png">`);

                    $('#poster').next().removeClass('disabled').text('UPDATED!');
                });
        });

        const related_documents = $('#related_documents').data('files');
        const created_at = $('#related_documents').data('createdat');
        let _filesize = 0;
        let _html = '';
        related_documents.forEach(function(value, index) {
            const file = value;
            _html += `
                <div class="row p-2">
                    <div class="handle col-auto clickable"><i class="fas fa-arrows-alt ml-4"></i></div>
                    <div class="file-stats col clickable" data-filename="${file.name}" data-folder="${created_at}">
                        <i class="fas fa-file mr-3"></i>
                        <span class="filename">${file.name}</span><span class="filesize"> - ${file.formatted_size}</span>
                    </div>
                    <div class="remove col-auto clickable"><i class="fas fa-trash mr-4"></i></div>
                </div>
            `;
            _filesize += file.size;
        });

        // debugger;
        console.log($('#related_documents').length);
        console.log($('#related_documents').find('.content-list').length);
        $('#related_documents').attr('data-totalfilesize', _filesize);
        $('#related_documents').find('.content-list').empty().append(_html);


        deleterow($('#related_documents .content-list'), true, function(el) {
            const data = el.prev().data();

            const url = `/backend/api/unlink_file?filename=${encodeURIComponent(data.filename)}&folder=${data.folder}`
            fetch(url);
        });
        
        $('#upload').on("change", function(e) {
            const input = e.currentTarget;
            // FileList object
            const fileList = input.files;
            const fileData = $(this).data();

            let filesize = parseInt(fileData.filesize);
            let fileCount = parseInt(fileData.filecount);
            let html = '';
            let appendHtml = true;

            fileCount += fileList.length;

            function formatFilesize(size) {
                if (size > 1 * 1024 * 1024) return Math.round(size / 1024 / 1024 * 10) / 10 + ' MB';
                else return Math.floor(size / 1024) + ' KB';
            }

            // validate filesize
            for (var i = 0; i < fileCount; i++) {
                const file = fileList.item(i);

                filesize += file.size;
                console.log(filesize);

                html += `
                    <div class="row p-2">
                        <div class="handle col-auto clickable"><i class="fas fa-arrows-alt ml-4"></i></div>
                        <div class="file-stats col clickable">
                            <i class="fas fa-file mr-3"></i>
                            <span class="filename">${file.name}</span><span class="filesize"> - ${formatFilesize(file.size)}</span>
                        </div>
                        <div class="remove col-auto clickable"><i class="fas fa-trash mr-4"></i></div>
                    </div>
                `;
            }

            if (fileCount > 5) {
                alert('You cannot upload more than 5 files');
            }
            if (filesize > 10 * 1024 * 1024) {
                alert('You cannot upload more than 10 MB');
            }
            $('#content-list').append(html);
        });

        $('form').submit(function(e) {
            if ($('[name="merchant_password"]').val() !== $('[name="confirm_password"]').val()) {
                e.preventDefault();
                alert('Password and confirm password are not identical. Please check and submit again.');
            }
        });
    });
</script>