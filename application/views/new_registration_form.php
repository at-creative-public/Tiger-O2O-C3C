<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?= base_url('public/static/bootstrap-4.3.1-dist/css/bootstrap.min.css') ?>" rel="stylesheet" />
    <script src="<?= base_url() ?>public/static/backend/js/jquery.min.js"></script>
</head>

<body>
    <div class="container">
        <div style="min-height:100px;background-color:<?= $card_info['background_color'] ?>;color:<?= $card_info['pri_color'] ?>;text-align:center;border-radius:15px;padding-top:30px;padding-bottom:30px;">
            <img class="img-fluid" style="max-height:50px;" src="<?= base_url($card_info['logo']) ?>">
            <h4><?= $card_info['project_name'] ?></h4>
        </div>
        <div style="padding:15px;">
            <form action="/registration/registration" method="post">
                <?php if($info_config['name']){ ?>
                    <div class="form-group">
                        <label for="name" class="control-label">全名</label>
                        <input type="text" id="name" class="form-control" name="name" required>
                    </div>
                <?php } ?>
                <?php if ($info_config["surname"] != 0) { ?>
                    <div class="form-group">
                        <label for="surname" class="control-label">姓氏*</label>
                        <input type="text" id="surname" class="form-control" name="surname" required>
                    </div>
                <?php } ?>
                <?php if ($info_config['given_name'] != 0) { ?>
                    <div class="form-group">
                        <label for="given_name" class="control-label">姓名*</label>
                        <input type="text" id="given_name" class="form-control" name="given_name" required>
                    </div>
                <?php } ?>
                <?php if ($info_config['english_name'] != 0) { ?>
                    <div class="form-group">
                        <label for="english_name" class="control-label">英文姓名*</label>
                        <input type="text" id="english_name" class="form-control" name="english_name" required>
                    </div>
                <?php } ?>
                <?php if ($info_config['chinese_name'] != 0) { ?>
                    <div class="form-group">
                        <label for="chinese_name" class="control-label">中文姓名*</label>
                        <input type="text" id="chinese_name" class="form-control" name="chinese_name" required>
                    </div>
                <?php } ?>
                <?php if ($info_config['gender'] != 0) { ?>
                    <div class="form-group">
                        <label for="gender" class="control-label">性別*</label>
                        <select class="form-control" id="gender" name="gender">
                            <option value="1">男</option>
                            <option value="0">女</option>
                        </select>
                    </div>
                <?php } ?>
                <?php if ($info_config['birthday'] != 0) { ?>
                    <div class="form-group">
                        <label for="birthday" class="control-label">出生日期*</label>
                        <input type="date" class="form-control" name="birthday" id="birthday" max="<?= date("Y-m-d", time()) ?>" required>
                    </div>
                <?php } ?>
                <?php if ($info_config['nationality'] != 0) { ?>
                    <div class="form-group">
                        <label for="nationality" class="control-label">國藉*</label>
                        <input type="text" class="form-control" name="nationality" id="nationality" required>
                    </div>
                <?php } ?>
                <?php if ($info_config['phone'] != 0) { ?>
                    <div class="form-group">
                        <label for="phone" class="control-label">電話號碼*</label>
                        <input type="text" class="form-control" id="phone" name="phone" required>
                    </div>
                <?php } ?>
                <?php if ($info_config['email'] != 0) { ?>
                    <div class="form-group">
                        <label for="email" class="control-label">電郵地址*</label>
                        <input type="email" class="form-control" id="email" name="email" required>
                    </div>
                <?php } ?>
                <?php if ($info_config['address'] != 0) { ?>
                    <div class="form-group">
                        <label for="address" class="control-label">地址*</label>
                        <input type="text" class="form-control" id="address" name="address" required>
                    </div>
                <?php } ?>
                <?php if ($info_config['industry'] != 0) { ?>
                    <div class="form-group">
                        <label for="industry" class="control-label">公司電話*</label>
                        <input type="text" class="form-control" id="industry" name="industry" required>
                    </div>
                <?php } ?>
                <?php if ($info_config['industry_2'] != 0) { ?>
                    <div class="form-group">
                        <label for="industry_2" class="control-label">行業*</label>
                        <input type="text" class="form-control" id="industry_2" name="industry_2" required>
                    </div>
                <?php } ?>
                <?php if ($info_config['job_title'] != 0) { ?>
                    <div class="form-group">
                        <label for="job_title" class="control-label">職銜*</label>
                        <input type="text" class="form-control" id="job_tilte" name="job_title" required>
                    </div>
                <?php } ?>
                <?php if ($info_config['company'] != 0) { ?>
                    <div class="form-group">
                        <label for="company" class="control-label">所屬公司*</label>
                        <input type="text" class="form-control" id="company" name="company" required>
                    </div>
                <?php } ?>
                <?php if ($info_config["salary"] != 0) { ?>
                    <div class="form-group">
                        <label for="salary" class="control-label">薪金*</label>
                        <input type="number" class="form-control" id="salary" name="salary" required>
                    </div>
                <?php } ?>

                <div class="form-group">
                    <small>*必須填寫</small>
                </div>
                <div class="form-group" style="text-align:center;">
                    <input type="hidden" name="pass_id" value="<?= $pass_id ?>">
                    <input type="hidden" name="platform" id="platform" value="">
                    <button type="submit" class="btn btn-success">領取會員卡</button>
                </div>
            </form>
        </div>
    </div>
</body>

<script src="<?= base_url("assets/js/bowser.js") ?>"></script>
<script>
    $(document).ready(function() {
        var result = bowser.getParser(window.navigator.userAgent);
        console.log(result);
        console.log("Browser result:" + result.parsedResult.browser.name);
        if (result.parsedResult.browser.name == "Safari") {
            $("#platform").val("apple");
        } else {
            $("#platform").val("google");
        }
    })
</script>


</html>