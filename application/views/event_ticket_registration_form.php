<html>

<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link href="<?= base_url('public/static/bootstrap-4.3.1-dist/css/bootstrap.min.css') ?>" rel="stylesheet" />
    <script src="<?= base_url() ?>public/static/backend/js/jquery.min.js"></script>
</head>

<body>
    <div class="container">
        <div style="min-height:100px;background-color:<?= $ticket_info['background_color'] ?>;color:<?= $ticket_info['content_color'] ?>;text-align:center;border-radius:15px!important;padding:30px 0px;">
            <img class="img-fluid" style="max-height:50px;max-width:50px" src="<?= base_url($ticket_info['logo']) ?>?id=<?= rand(1, 10000) ?>">
            <h4><?= $ticket_info['event_name'] ?></h4>
        </div>
        <div class="mt-4">
            <form action="/event_ticket/register" method="post">
                <?php if($form_setting['name'] != 0){?>
                <div class="form-group">
                    <label for="name" class="label">全名*</label>
                    <input type="text" id="name" name="name" class="form-control" required>
                </div>
                <?php }?>
                <?php if ($form_setting['surname'] != 0) { ?>
                    <div class="form-group">
                        <label for="surname" class="label">姓氏*</label>
                        <input type="text" id="surname" class="form-control" name="surname" required>
                    </div>
                <?php } ?>
                <?php if ($form_setting['given_name'] != 0) { ?>
                    <div class="form-group">
                        <label for="given_name" class="label">姓名*</label>
                        <input type="text" id="given_name" class="form-control" name="given_name" required>
                    </div>
                <?php } ?>
                <?php if ($form_setting['english_name'] != 0) { ?>
                    <div class="form-group">
                        <label for="english_name" class="label">英文姓名*</label>
                        <input type="text" id="english_name" class="form-control" name="english_name" required>
                    </div>
                <?php } ?>
                <?php if ($form_setting['chinese_name'] != 0) { ?>
                    <div class="form-group">
                        <label for="chinese_name" class="label">中文姓名*</label>
                        <input type="text" id="chinese_name" class="form-control" name="chinese_name" required>
                    </div>
                <?php } ?>
                <?php if ($form_setting['gender'] != 0) { ?>
                    <div class="form-group">
                        <label for="gender" class="label">性別*</label>
                        <select class="form-control" id="gender" name="gender">
                            <option value="1">男</option>
                            <option value="0">女</option>
                        </select>
                    </div>
                <?php } ?>
                <?php if ($form_setting['birthday'] != 0) { ?>
                    <div class="form-group">
                        <label for="birthday" class="label">出生日期*</label>
                        <input type="date" class="form-control" id="birthday" name="birthday" required>
                    </div>
                <?php } ?>
                <?php if ($form_setting['age'] != 0) { ?>
                    <div class="form-group">
                        <label for="age" class="label">年齡*</label>
                        <input type="number" id="age" class="form-control" name="age" required>
                    </div>
                <?php } ?>
                <?php if ($form_setting['nationality'] != 0) { ?>
                    <div class="form-group">
                        <label for="age" class="label">國籍*</label>
                        <input type="text" id="nationality" class="form-control" name="nationality" required>
                    </div>
                <?php } ?>
                <?php if ($form_setting['phone'] != 0) { ?>
                    <div class="form-group">
                        <label for="phone" class="label">電話號碼*</label>
                        <input type="text" id="phone" class="form-control" name="phone" required>
                    </div>
                <?php } ?>
                <?php if ($form_setting['email'] != 0) { ?>
                    <div class="form-group">
                        <label for="email" class="label">電郵地址*</label>
                        <input type="email" id="email" class="form-control" name="email" required>
                    </div>
                <?php } ?>
                <?php if ($form_setting['address'] != 0) { ?>
                    <div class="form-group">
                        <label for="address" class="label">地址*</label>
                        <input type="text" id="address" class="form-control" name="address" required>
                    </div>
                <?php } ?>
                <?php if ($form_setting['industry'] != 0) { ?>
                    <div class="form-group">
                        <label for="industry" class="label">公司電話</label>
                        <input type="text" id="industry" class="form-control" name="industry" required>
                    </div>
                <?php } ?>
                <?php if ($form_setting['industry_2'] != 0) { ?>
                    <div class="form-group">
                        <label for="industry_2" class="label">行業*</label>
                        <input type="text" id="industry_2" class="form-control" name="industry_2" required>
                    </div>
                <?php } ?>
                <?php if ($form_setting['job_title'] != 0) { ?>
                    <div class="form-group">
                        <label for="job_title" class="label">職銜*</label>
                        <input type="text" id="job_title" class="form-control" name="job_title" required>
                    </div>
                <?php } ?>
                <?php if ($form_setting['company'] != 0) { ?>
                    <div class="form-group">
                        <label for="company" class="label">所屬公司*</label>
                        <input type="text" id="company" class="form-control" name="company" required>
                    </div>
                <?php } ?>
                <?php if ($form_setting['salary'] != 0) { ?>
                    <div class="form-group">
                        <label for="salary" class="label">薪金*</label>
                        <input type="text" id="salary" class="form-control" name="salary" required>
                    </div>
                <?php } ?>
                <div class="form-group text-center">
                    <input type="hidden" name="event_ticket_id" value="<?= $ticket_info['id'] ?>">
                    <input type="hidden" id="platform" name="platform" value="">
                    <button type="submit" class="btn btn-success">提交</button>
                </div>

            </form>
        </div>
    </div>
</body>

<script src="<?= base_url("assets/js/bowser.js") ?>"></script>
<script>
    $(document).ready(function() {
        var result = bowser.getParser(window.navigator.userAgent);
        console.log(result);
        console.log("Browser result:" + result.parsedResult.browser.name);
        if (result.parsedResult.browser.name == "Safari") {
            $("#platform").val("apple");
        } else {
            $("#platform").val("google");
        }
    })
</script>

</html>