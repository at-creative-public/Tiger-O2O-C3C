<style>
    select {
        -webkit-appearance: none;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.css" />

<style>
    #distribution_of_plans {}

    #number_of_virtual_passes {}

    #number_of_virtual_passes,
    #inserted_redeemed_ratio,
    #merchant_ranking {
        /* width: 300px !important; */
        /* height: 300px !important; */
    }

    #number_of_themes {
        width: 150px !important;
        height: 150px !important;
    }
</style>
<div class="page-body">
    <div class="row pl-0 pl-xl-5" id="capture">
        <div class="col-12">
            <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5"><?= $page ?></h4>
            <div class="p-0">
                <div class="row">
                    <div class="col-12 col-lg-7 col-xl-8">
                        <div class="row">
                            <div class="col-12 col-xl-5">
                                <?php
                                //$number_of_merchants = $number_of_merchants ?? 2120;
                                $number_of_merchants = $number_of_merchants ?? $merchants;
                                $number_of_merchants_percentage = $number_of_merchants_percentage ?? '+20%';
                                ?>
                                <section id="number_of_merchants" class="mb-5">
                                    <div class="shadow card text-left dashboard-card">
                                        <div class="card-body">
                                            <h4 class="card-title mb-4">Number of Merchants</h4>
                                            <div class="card-text">
                                                <div class="row align-items-end">
                                                    <div class="value col"><?= number_format($number_of_merchants) ?></div>
                                                    <!--<div class="percentage col-auto"><?= $number_of_merchants_percentage ?></div>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <?php
                                //$distribution_of_plans_labels = $distribution_of_plans_labels ?? ['Basic', 'Plus', 'Pro', 'Business'];
                                $distribution_of_plans_labels = $distribution_of_plans_labels ?? ["Lite","Pro","Enterprise","Expired"];
                                
                                //$distribution_of_plans_data = $distribution_of_plans_data ?? [1388, 1023, 950, 597];
                                $distribution_of_plans_data = $distribution_of_plans_data ?? [$distribution['Lite'],$distribution['Pro'],$distribution['Enterprise'],$distribution['Expired']];
                                ?>
                                <section class="mb-5">
                                    <div class="shadow card text-left dashboard-card">
                                        <div class="card-body">
                                            <h4 class="card-title mb-4">Distribution of Plans</h4>
                                            <div class="card-text"></div>
                                            <canvas id="distribution_of_plans" data-thickness="20" data-bgcolor="#455CA8" data-labels='<?= json_encode($distribution_of_plans_labels, JSON_UNESCAPED_UNICODE) ?>' data-data='<?= json_encode($distribution_of_plans_data) ?>'>
                                            </canvas>
                                        </div>
                                    </div>
                                </section>
                                <?php 
                                $items = ["Item 1","Item 2","Item 3","Item 4"];
                                $items_data = ["2","2","3","5"];
                                ?>
                                <section class="mb-5">
                                <div class="shadow card text-left dashboard-card">
                                        <div class="card-body">
                                            <h4 class="card-title mb-4">Sales Generated</h4>
                                             <div style="font-size:3.5rem;font-weight:900;color:#455ca8;"><?=$sales?></div>
                                            <canvas id="sales_generated" data-thickness="20" style="display:none;" data-bgcolor="#455CA8" data-labels='<?= json_encode($items, JSON_UNESCAPED_UNICODE) ?>' data-data='<?= json_encode($items_data) ?>'>
                                            </canvas> 
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="col-12 col-xl-7">
                                <?php
                                //$number_of_virtual_passes = $number_of_virtual_passes ?? 2899;
                                $number_of_virtual_passes = $number_of_virtual_passes ?? $total;
                                $number_of_virtual_passes_percentage = $number_of_virtual_passes_percentage ?? '+45%';
                                //$number_of_virtual_passes_labels = $number_of_virtual_passes_labels ?? ['Point Card', 'Stamp Card', 'Coupon', 'Ticket', 'Namecard'];
                                $number_of_virtual_passes_labels = $number_of_virtual_passes_labels ?? array_keys($passes);
                                //$number_of_virtual_passes_data = $number_of_virtual_passes_data ?? [623, 312, 455, 230, 725];
                                $number_of_virtual_passes_data = $number_of_virtual_passes_data ?? array_values($passes);
                                ?>
                                <section class="mb-5">
                                    <div class="shadow card text-left dashboard-card">
                                        <div class="card-body">
                                            <h4 class="card-title mb-4">Number of Virtual Passes</h4>
                                            <div class="card-text">
                                                <div class="row align-items-end">
                                                    <div class="value col"><?= number_format($number_of_virtual_passes) ?></div>
                                                    <!--<div class="percentage col-auto"><?= $number_of_virtual_passes_percentage ?></div>-->
                                                </div>
                                            </div>
                                            <canvas id="number_of_virtual_passes" width="300" data-thickness="20" data-bgcolor="#455CA8" data-labels='<?= json_encode($number_of_virtual_passes_labels, JSON_UNESCAPED_UNICODE) ?>' data-data='<?= json_encode($number_of_virtual_passes_data) ?>'>
                                        </div>
                                    </div>
                                </section>
                                
                                <?php
                                $inserted_redeemed_ratio = $inserted_redeemed_ratio ?? '65%';
                                $inserted = 1723;
                                $redeemed = 1176;
                                $inserted_redeemed_ratio_percentage = $inserted_redeemed_ratio_percentage ?? '+45%';
                                $inserted_redeemed_ratio_labels = $inserted_redeemed_ratio_labels ?? ['Point Card', 'Stamp Card', 'Coupon', 'Ticket', 'Namecard'];
                                $inserted_redeemed_ratio_data = $inserted_redeemed_ratio_data ?? [623, 312, 455, 230, 725];
                                ?>
                                <section class="mb-5">
                                    <div class="shadow card text-left dashboard-card">
                                        <div class="card-body">
                                            <h4 class="card-title mb-4">Inserted & Redeemed Ratio</h4>
                                            <div class="card-text">
                                                <div class="row align-items-end">
                                                    <div class="col">
                                                        <div class="value"><?= $inserted_redeemed_ratio ?></div>
                                                        <div class="font-weight-bold"><?= $inserted ?> INSERTED : <?= $redeemed ?> REDEEMED</div>
                                                    </div>
                                                    <div class="percentage col-auto"><?= $inserted_redeemed_ratio_percentage ?></div>
                                                </div>
                                            </div>
                                            <canvas id="inserted_redeemed_ratio" data-thickness="20" data-bgcolor="#455CA8" data-labels='<?= json_encode($inserted_redeemed_ratio_labels, JSON_UNESCAPED_UNICODE) ?>' data-data='<?= json_encode($inserted_redeemed_ratio_data) ?>'>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-5 col-xl-4">
                        <?php
                        $number_of_themes = $number_of_themes ?? 21;
                        $number_of_themes_percentage = $number_of_themes_percentage ?? '+4%';
                        $number_of_themes_labels = $number_of_themes_labels ?? ['Point Card', 'Stamp Card', 'Coupon', 'Ticket', 'Namecard'];
                        $number_of_themes_data = $number_of_themes_data ?? [623, 312, 455, 230, 725];
                        ?>
                        <section class="mb-5">
                            <div class="shadow card text-left dashboard-card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <h4 class="card-title mb-4">Number of Themes</h4>
                                            <div class="card-text">
                                                <div class="row align-items-end">
                                                    <div class="col">
                                                        <div class="value"><?= $number_of_themes ?></div>
                                                    </div>
                                                    <div class="percentage col-auto"><?= $number_of_themes_percentage ?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <canvas id="number_of_themes" width="300" height="300" data-thickness="20" data-bgcolor="#455CA8" data-labels='<?= json_encode($number_of_themes_labels, JSON_UNESCAPED_UNICODE) ?>' data-data='<?= json_encode($number_of_themes_data) ?>'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-12">
                                <?php
                                $number_of_landing_page = $number_of_landing_page ?? 1954;
                                $number_of_landing_page_percentage = $number_of_landing_page_percentage ?? '=45%';
                                $number_of_landing_page_view = $number_of_landing_page_view ?? 1954;
                                $number_of_landing_page_clicks = $number_of_landing_page_clicks ?? 10954;
                                ?>
                                <section class="mb-5">
                                    <div class="shadow card text-left dashboard-card">
                                        <div class="card-body">
                                            <h4 class="card-title mb-4">Number of Landing Page</h4>
                                            <div class="card-text">
                                                <div class="row align-items-end mb-3">
                                                    <div class="value col"><?= number_format($number_of_landing_page) ?></div>
                                                    <div class="percentage col-auto"><?= $number_of_landing_page_percentage ?></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-6">
                                                        <h5 class="font-weight-bold">View</h5>
                                                        <div class="value font-weight-bold" style="zoom: .8"><?= $number_of_landing_page_view ?></div>
                                                    </div>
                                                    <div class="col-6">
                                                        <h5 class="font-weight-bold">Clicks</h5>
                                                        <div class="value font-weight-bold" style="zoom: .8"><?= $number_of_landing_page_clicks ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-12">
                                <?php
                                $number_of_marketplace = $number_of_marketplace ?? 1945;
                                $number_of_marketplace_percentage = $number_of_marketplace_percentage ?? '=55%';
                                $number_of_marketplace_view = $number_of_marketplace_view ?? 2769;
                                $number_of_marketplace_clicks = $number_of_marketplace_clicks ?? 9954;
                                ?>
                                <section class="mb-5">
                                    <div class="shadow card text-left dashboard-card">
                                        <div class="card-body">
                                            <h4 class="card-title mb-4">Number of Marketplace</h4>
                                            <div class="card-text">
                                                <div class="row align-items-end mb-3">
                                                    <div class="value col"><?= number_format($number_of_marketplace) ?></div>
                                                    <div class="percentage col-auto"><?= $number_of_marketplace_percentage ?></div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-6">
                                                        <h5 class="font-weight-bold">View</h5>
                                                        <div class="value font-weight-bold" style="zoom: .8"><?= $number_of_marketplace_view ?></div>
                                                    </div>
                                                    <div class="col-6">
                                                        <h5 class="font-weight-bold">Clicks</h5>
                                                        <div class="value font-weight-bold" style="zoom: .8"><?= $number_of_marketplace_clicks ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <?php
                        //$merchant_ranking_labels = $merchant_ranking_labels ?? ['kmstudio', 'keisutle', 'jciharbour'];
                        $merchant_ranking_labels = $merchant_ranking_labels ?? [$top_merchant[0]['merchant_id'],$top_merchant[1]['merchant_id'],$top_merchant[2]['merchant_id']];
                        $merchant_ranking_data = $merchant_ranking_data ?? [$top_merchant[0]['user'],$top_merchant[1]['user'],$top_merchant[2]['user']];
                        ?>
                        <section class="mb-5">
                            <div class="shadow card text-left dashboard-card">
                                <div class="card-body">
                                    <h4 class="card-title mb-4">Merchant Ranking - Top Numbers of Users</h4>
                                    <div class="card-text"></div>
                                    <canvas id="merchant_ranking" data-thickness="20" data-bgcolor="#455CA8" data-labels='<?= json_encode($merchant_ranking_labels, JSON_UNESCAPED_UNICODE) ?>' data-data='<?= json_encode($merchant_ranking_data) ?>'>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
<script>
    barchart('#distribution_of_plans');
    barchart('#sales_generated');
    horizontalbarchart('#number_of_virtual_passes');
    horizontalbarchart('#inserted_redeemed_ratio');
    doughnutchart('#number_of_themes');
    horizontalbarchart('#merchant_ranking');
</script>

<script src="<?= base_url() ?>assets/js/html2canvas.min.js"></script>