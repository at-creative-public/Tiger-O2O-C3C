﻿<!DOCTYPE html>
<html>

<head>
    <?php
    $this->load->view('admin/include/common');
    ?>
</head>

<body>
    <style>
        .mobile_menu.show {
            visibility: visible;
            transition: width 1s ease-in-out;
            width: 100%;
        }

        .mobile_menu {
            background-color: #4a5ba3;
            position: fixed;
            width: 0%;
            height: 100%;
            display: block;
            top: 0;
            visibility: hidden;
            transition: width 1s ease-in-out;
            z-index: 10000;
        }

        .mobile_menu .menu_button {
            border: none;
            font-size: 2.5rem;
            color: white;
            background-color: transparent;
        }


        .mobile_menu a {
            visibility: hidden;
            transition: width 1s ease-in-out;
        }

        .mobile_menu.show a {
            visibility: visible;
            transition: width 1s ease-in-out;
            font-size: 2em;
            color: white;
            text-decoration: none;
            left: 0;
        }
    </style>
    <div class="mobile_menu">
        <div class="d-flex flex-column text-right">
            <div style='float:right;padding-right:30px;'>
                <button id="menu_close_button" class="menu_button" style="color:white;width:fit-content">X</button>
            </div>
        </div>
        <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;">
            <a href="<?= base_url("admin/dashboard") ?>">Dashboard</a>
        </div>
        <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;">
            <a href="<?= base_url("admin/merchant") ?>">Merchant Management</a>
        </div>
        <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;">
            <a href="<?= base_url("admin/merchant/add") ?>">Create New Account</a>
        </div>
        <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;">
            <a href="<?= base_url("admin/upload_elements") ?>">Upload Elements</a>
        </div>
        <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;">
            <a id="Logout" href="<?= base_url("admin/signout") ?>">Logout</a>
        </div>
    </div>
    <!-- Loading Container -->
    <div class="loading-container<?php echo $_inactive_loading ? ' loading-inactive' : '' ?>">
        <div class="loader"></div>
    </div>
    <!--  /Loading Container -->
    <!-- Navbar -->
    <div class="o2o-c3c navbar">
        <div class="navbar-inner">
            <div class="navbar-container">
                <!-- Navbar Barnd -->
                <div class="navbar-header pull-left">
                    <a href="#" class="navbar-brand">
                        <small>
                            <img class="imglogo" src="<?php echo base_url(); ?>assets/images/new_logo.png" alt="" />
                        </small>
                    </a>
                </div>
                <!-- /Navbar Barnd -->
                <!-- Sidebar Collapse -->
                <!-- <div class="sidebar-collapse hidden-md hidden-lg hidden-xl" id="sidebar-collapse">
                    <i class="collapse-icon fa fa-bars"></i>
                </div> -->
                <div class="sidebar-collapse dropdown-menu-controller " id="dropdown_menu_controller">
                    <i class="collapse-icon fa fa-bars"></i>
                </div>
                <div id="navbar-app-name" class="hidden-xs hidden-sm hidden-md"><?= $page ?></div>
                <!-- /Sidebar Collapse -->
                <!-- Account Area and Settings --->
                <?php
                $username = $this->session->userdata('username') ?? '管理員';
                $email = $this->session->userdata('email');
                $avatar = empty($this->session->userdata('image')) ? base_url('public/static/backend/images/backend/icon-admin-user.png') : base_url($this->session->userdata('image'));
                ?>
                <div class="navbar-header pull-right">
                    <div class="navbar-account">
                        <ul class="account-area">
                            <li>
                                <a class="login-area dropdown-toggle" data-toggle="">
                                    <section>
                                        <h2>
                                            <span class="profile">hello，<?php echo $username; ?>!</span>
                                        </h2>
                                    </section>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /Account Area and Settings -->
            </div>
        </div>
    </div>
    <!-- /Navbar -->
    <!-- Main Container -->
    <!-- main content starts -->
    <div class="o2o-c3c main-container container-fluid main-admincontent">
        <!-- Page Container -->
        <div class="page-container">
            <!-- Page Sidebar -->
            <?php
            $this->load->view('admin/include/menu');
            ?>
            <!-- /Page Sidebar -->
            <!-- Chat Bar -->
            <?php
            //        $this->load->view('at-admin/include/chatbar');
            ?>
            <!-- /Chat Bar -->
            <!-- Page Content -->
            <div class="page-content">
                <?php
                if (isset($_view) && $_view)
                    //var_dump($_view);
                    $this->load->view('admin/' . $_view);
                ?>
            </div>
            <!-- /Page Content -->
        </div>
        <!-- /Page Container -->
        <!-- Main Container -->
    </div>
    <!-- main content ends -->
    <?php
    $this->load->view('admin/include/footer');
    ?>

    <script>
        $("#dropdown_menu_controller").on("click", function() {
            $(".mobile_menu").addClass("show");
        })

        $("#menu_close_button").on("click", function() {
            $(".mobile_menu").removeClass("show");
        })
    </script>
</body>
<!--  /Body -->

<!-- Mirrored from beyondadmin-v1.4.s3-website-us-east-1.amazonaws.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 07 May 2015 08:22:34 GMT -->

</html>