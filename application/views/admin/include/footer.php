<!--Beyond Scripts-->
<script src="<?= base_url() ?>public/static/backend/js/beyond.js"></script>


<!--Page Related Scripts-->
<!--Sparkline Charts Needed Scripts-->
<script src="<?= base_url() ?>public/static/beyond-admin/js/charts/sparkline/jquery.sparkline.js"></script>
<script src="<?= base_url() ?>public/static/beyond-admin/js/charts/sparkline/sparkline-init.js"></script>

<!--Easy Pie Charts Needed Scripts-->
<script src="<?= base_url() ?>public/static/beyond-admin/js/charts/easypiechart/jquery.easypiechart.js"></script>
<script src="<?= base_url() ?>public/static/beyond-admin/js/charts/easypiechart/easypiechart-init.js"></script>

<script src="<?= base_url() ?>public/static/backend/js/admin.js?v=<?= rand(10000, 100000) ?>"></script>

<!-- <footer class="fixed-bottom text-right pb-4 pr-4" style="background: #F8F9FE; color: #C8C8CA;">
    &copy; Copyright 2021 Smart Business Consultancy Limited. All Rights Reserved.
</footer> -->
<!-- <div class="d-flex flex-column min-vh-100">
    <nav>
    </nav>
    <main class="flex-fill">
    </main>
    <footer>
        &copy; Copyright 2021 Smartbiz
    </footer>
</div> -->