<div class="page-body">
    <div class="">
        <div class="row pl-0 pl-xl-5">
            <div class="col-12">
                <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5"><?= $page ?></h4>
            </div>
            <div class="col-12">
                <section id="tutorial_video" class="pb-5" data-files='<?= json_encode($tutorial_video ?? []) ?>' data-folder="tutorial_video">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="row align-items-center w-100">
                                <div class="col pr-0">
                                    <div class="heading text-accent font-weight-bold">Tutorial Video</div>
                                </div>
                                <div class="col-auto pt-3 pt-md-0 pr-0">
                                    <input type="file" id="tutorial_video_upload" name="upload[]" multiple data-filesize="0" data-filecount="0" style="width: 1px; height: 1px !important;" />
                                    <label class="btn btn-accent text-white bg-accent" for="tutorial_video_upload">UPLOAD FILE</label>
                                </div>
                            </div>
                            <div class="content-list ui-sortable mt-3 pt-3 border-top"></div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-12">
                <section id="element_type" class="pb-5">
                    <div class="btn-group" role="group" aria-label="Buttons">
                        <button id="ordinary" type="button" class="btn btn-accent active mr-3 ">ORDINARY</button>
                        <button id="modern" type="button" class="btn btn-accent mr-3">MODERN</button>
                        <button id="playful" type="button" class="btn btn-accent mr-3">PLAYFUL</button>
                        <button id="featured" type="button" class="btn btn-accent mr-3">FEATURED</button>
                    </div>
                </section>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <section id="theme_background" class="pb-5" data-files='<?= json_encode($theme_background ?? []) ?>' data-folder="theme_background">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row align-items-center w-100">
                                        <div class="col pr-0">
                                            <div class="heading text-accent font-weight-bold">Theme Background</div>
                                        </div>
                                        <div class="col-auto pt-3 pt-md-0 pr-0">
                                            <input type="file" id="theme_background_upload" name="upload[]" multiple data-filesize="0" data-filecount="0" style="width: 1px; height: 1px !important;" />
                                            <label class="btn btn-accent text-white bg-accent" for="theme_background_upload">UPLOAD FILE</label>
                                        </div>
                                    </div>
                                    <div class="content-list ui-sortable mt-3 pt-3 border-top"></div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-12 col-md-6">
                        <section id="banner" class="pb-5" data-files='<?= json_encode($banner ?? []) ?>' data-folder="banner">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row align-items-center w-100">
                                        <div class="col pr-0">
                                            <div class="heading text-accent font-weight-bold">Banner</div>
                                        </div>
                                        <div class="col-auto pt-3 pt-md-0 pr-0">
                                            <input type="file" id="banner_upload" name="upload[]" multiple data-filesize="0" data-filecount="0" style="width: 1px; height: 1px !important;" />
                                            <label class="btn btn-accent text-white bg-accent" for="banner_upload">UPLOAD FILE</label>
                                        </div>
                                    </div>
                                    <div class="content-list ui-sortable mt-3 pt-3 border-top"></div>
                                </div>
                            </div>
                        </section>
                        <section id="font" class="pb-5" data-files='<?= json_encode($font ?? []) ?>' data-folder="font">
                            <div class="row">
                                <div class="col-12">
                                    <div class="row align-items-center w-100">
                                        <div class="col pr-0">
                                            <div class="heading text-accent font-weight-bold">Font</div>
                                        </div>
                                        <div class="col-auto pt-3 pt-md-0 pr-0">
                                            <input type="file" id="font_upload" name="upload[]" multiple data-filesize="0" data-filecount="0" style="width: 1px; height: 1px !important;" />
                                            <label class="btn btn-accent text-white bg-accent" for="font_upload">UPLOAD FILE</label>
                                        </div>
                                    </div>
                                    <div class="content-list ui-sortable mt-3 pt-3 border-top"></div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let active_id = 'ordinary';

    $(document).ready(function() {
        function fetch_upload_elements(prefix = 'ordinary') {
            fetch('/admin/api/get_upload_elements/' + prefix)
                .then(function(response) {
                    return response.json();
                })
                .then(function(response) {
                    console.log(response);

                    $('#tutorial_video').data('files', response.tutorial_video);
                    $('#theme_background').data('files', response.theme_background);
                    $('#banner').data('files', response.banner);
                    $('#font').data('files', response.font);

                    main(prefix);
                });
        }

        function main(prefix = 'ordinary') {
            ['tutorial_video', 'theme_background', 'banner', 'font'].forEach(function(value, index) {
                const subfolder = value;
                const subfolder_id = '#' + value;

                const files = $(subfolder_id).data('files');
                const folder = 'upload_elements_' + prefix + '_' + $(subfolder_id).data('folder');
                insertuploadfilerow(subfolder_id, files, folder);
                const fnDelete = function() {
                    deleterow(subfolder_id + ' .content-list', true, function(el) {
                        const data = el.prev().find('div').data();

                        const url = `/admin/api/unlink_file?filename=${encodeURIComponent(data.filename)}&folder=${folder}`
                        fetch(url);
                    });
                };
                uploadfile(subfolder_id, subfolder, 0, 0, function(e) {
                    fnDelete();
                    // debugger;
                    postfileupload(e, 'multiple', subfolder_id, "/admin/api/upload_multiple/" + folder, 'files');
                });
                fnDelete();
            });
        }

        function element_type_active(el) {
            const id = el.attr('id');
            const cond = id != active_id;

            if (cond) {
                active_id = id;
                el.parent().find('button').removeClass('active');
                el.addClass('active');
            }

            return cond;
        }

        main();

        $('#ordinary').click(function(e) {
            if (element_type_active($(this))) fetch_upload_elements('ordinary');
        });
        $('#modern').click(function(e) {
            if (element_type_active($(this))) fetch_upload_elements('modern');
        });
        $('#playful').click(function(e) {
            if (element_type_active($(this))) fetch_upload_elements('playful');
        });
        $('#featured').click(function(e) {
            if (element_type_active($(this))) fetch_upload_elements('featured');
        });
    });
</script>