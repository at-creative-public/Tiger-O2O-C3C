<style>
    select {
        -webkit-appearance: none;
    }


    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }
</style>
<div class="page-body">
    <div class="row pl-0 pl-xl-5">
        <div class="col-12">
            <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5"><?= $page ?></h4>
        </div>
        <div class="col-12">
            <?php $url = isset($id) ? 'admin/merchant/submit?id=' . $id : 'admin/merchant/submit'; ?>
            <?= form_open($url, ['enctype' => 'multipart/form-data']) ?>
            <div class="row">
                <div class="col-12 col-md-6">
                    <section id="merchant-information">
                        <div class="title pt-4 pb-5 font-weight-bold">Merchant Information</div>
                        <div class="form-group">
                            <?= form_label('Company Name*', 'company_name') ?>
                            <?= form_input('company_name', set_value('company_name', $company_name ?? ''), 'id="company_name" class="form-control" required') ?>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Company Email*', 'company_email') ?>
                                    <input type="email" id="company_email" name="company_email" class="form-control" value="<?= set_value('company_email', $company_email ?? '') ?>" required>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Company Hotline*', 'company_hotline') ?>
                                    <?= form_input('company_hotline', set_value('company_hotline', $company_hotline ?? ''), 'id="company_hotline" class="form-control" required') ?>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Industry*', 'company_industry') ?>
                                    <select id="company_industry" name="company_industry" class="form-control" data-selected="<?= set_value('company_industry', $company_industry ?? '') ?> required">
                                        <option value="" disabled selected>--Please Select--</option>
                                        <option value="artwork">Artworks & Crafts</option>
                                        <option value="automobile">Automobiles</option>
                                        <option value="beauty">Beauty & Salon</option>
                                        <option value="ecommerce">eCommerce</option>
                                        <option value="fashion">Fashion & Textiles</option>
                                        <option value="finance">Finance</option>
                                        <option value="food">Food & Beverages</option>
                                        <option value="furniture">Furnitures</option>
                                        <option value="health">Healthcare Household Goods</option>
                                        <option value="hotel">Hotel & Tourism</option>
                                        <option value="industrial">Industrials & Construction</option>
                                        <option value="it">Information Technology</option>
                                        <option value="logistics">Logistics</option>
                                        <option value="media">Media & Entertainment</option>
                                        <option value="clubhouse">Membership Clubhouse</option>
                                        <option value="properties">Properties</option>
                                        <option value="retail">Retail</option>
                                        <option value="service">Service Sector</option>
                                        <option value="spa">Spa/Gym</option>
                                        <option value="support">Support Services</option>
                                        <option value="telecommunications">Telecommunications</option>
                                        <option value="travel">Travel & Leisure</option>
                                        <option value="others">Others</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Company Website', 'company_website') ?>
                                    <?= form_input('company_website', set_value('company_website', $company_website ?? ''), 'id="company_website" class="form-control"') ?>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="account-setting">
                        <div class="title pt-4 pb-5 font-weight-bold">Account Setting</div>
                        <div class="form-group">
                            <?= form_label('Merchant ID*', 'merchant_id') ?>
                            <?= form_input('merchant_id', set_value('merchant_id', $merchant_id ?? ''), 'id="merchant_id" class="form-control" required') ?>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Password*', 'merchant_password') ?>
                                    <input type="password" name="merchant_password" id="merchant_password" class="form-control" autocomplete="new-password" value="<?= set_value('password', $merchant_password ?? '') ?>" required>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Confirm Password*', 'confirm_password') ?>
                                    <?= form_password('confirm_password', set_value('confirm_password', $merchant_password ?? ''), 'id="confirm_password" class="form-control" required') ?>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <div><?= form_label('Plan*', 'merchant_plan') ?></div>
                                    <div class="row">
                                        <div class="col pr-0">
                                            <select name="merchant_plan" class="form-control" data-selected="<?= set_value('merchant_plan', $merchant_plan ?? '') ?>" required>
                                                <option value="" disabled selected>--Please Select--</option>
                                                <option value="Enterprise">Enterprise</option>
                                                <option value="Pro">Pro</option>
                                                <option value="Lite">Lite</option>
                                                <option value="Expired">Expired</option>
                                            </select>
                                        </div>
                                        <div id="plan_info" class="col-auto m-auto clickable">
                                            <i class="fas fa-info-circle"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Valid Until*', 'merchant_valid_until') ?>
                                    <?php if (isset($merchant_valid_until)) { ?>
                                        <input type="date" name="merchant_valid_until" id="merchant_valid_until" class="form-control" value="<?= set_value('merchant_valid_until', date("Y-m-d", $merchant_valid_until) ?? date('Y-12-31')) ?>" required>
                                    <?php } else { ?>
                                        <input type="date" name="merchant_valid_until" id="merchant_valid_until" class="form-control" value="<?= set_value('merchant_valid_until', date('Y-12-31')) ?>" required>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <?= form_label("Termination", "force_disabled") ?>
                                    <?php if (isset($force_disabled)) { ?>
                                        <select name="force_disabled" id="force_disabled" class="form-control" data-selected="<?= set_value('force_disabled', $force_disabled ?? '') ?>">
                                            <option <?= $force_disabled === false ? "selected" : "" ?> value="0">False</option>
                                            <option <?= $force_disabled === true ? "selected" : "" ?> value="1">True</option>
                                        </select>
                                    <?php  } else { ?>
                                        <select name="force_disabled" id="force_disabled" class="form-control" data-selected="0">
                                            <option selected value="0">False</option>
                                            <option value="1">True</option>
                                        </select>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <?= form_label("Sales Generated", "sales") ?>
                                    <input type="number" name="sales" class="form-control" id="sales" value="<?= set_value('sales', $sales ?? '') ?>" required>
                                </div>
                            </div>
                            <?php if (isset($api_key)) { ?>
                                <div class="col-12">
                                    <div class="form-group">
                                        <?= form_label("Auth Key", "api_key") ?>
                                        <input type="text" id="api_key" class="form-control" value="<?= $api_key ?>" readonly>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </section>
                </div>
                <div class="col-12 col-md-6">
                    <section id="staff-information" class="pb-4">
                        <div class="title pt-4 pb-5 font-weight-bold">Staff Information</div>
                        <div class="form-group">
                            <?= form_label('Contact Person*', 'contact_person') ?>
                            <?= form_input('contact_person', set_value('company_hotline', $contact_person ?? ''), 'id="contact_person" class="form-control" required') ?>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Title*', 'contact_title') ?>
                                    <select name="contact_title" id="contact_title" class="form-control" data-selected="<?= set_value('contact_title', $contact_title ?? '') ?>" required>
                                        <option value="" disabled selected>--Please Select--</option>
                                        <option value="Mr">Mr</option>
                                        <option value="Mrs">Mrs</option>
                                        <option value="Ms">Ms</option>
                                        <option value="Miss">Miss</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Position', 'contact_position') ?>
                                    <?= form_input('contact_position', set_value('contact_position', $contact_position ?? ''), 'id="contact_position" class="form-control"') ?>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Phone*', 'contact_phone') ?>
                                    <?= form_input('contact_phone', set_value('contact_phone', $contact_phone ?? ''), 'id="contact_phone" class="form-control" required') ?>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <?= form_label('Email*', 'contact_email') ?>
                                    <input type="email" id="contact_email" name="contact_email" class="form-control" value="<?= set_value('contact_email', $contact_email ?? '') ?>" required>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="related_documents" class="pb-5" data-files='<?= json_encode($related_documents ?? []) ?>' data-folder="<?= $merchant_id ?? '' ?>">
                        <div class="heading text-accent font-weight-bold pb-5">Related Documents</div>
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <input type="file" id="related_documents_upload" name="upload[]" multiple data-filesize="0" data-filecount="0" style="width: 1px; height: 1px !important;" />
                                <label class="btn btn-upload" for="related_documents_upload">UPLOAD FILE</label>
                            </div>
                            <div class="col-auto">
                                <span class="upload-text red ml-2">(Maximum 5 items, 10MB)</span>
                            </div>
                        </div>
                        <div class="content-list ui-sortable mt-3 pt-3 border-top"></div>
                    </section>
                </div>
            </div>
            <?php if (isset($id)) {
                echo form_hidden('id', $id);
                echo form_hidden('old_merchant_id', $merchant_id);
                echo form_hidden('company_id', $company_id);
                echo form_hidden('contact_id', $contact_id);
            } ?>
            <?= form_submit('', 'Submit', 'id="update_button" class="btn btn-primary mt-4 white " style="background:#4a5ba3!important;border-color:#4a5ba3!important;"') ?>
            <?= form_close() ?>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="viewModel" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg mx-auto" role="document">
        <div class="modal-content bg-accent">
            <div class="modal-content-wrapper">
                <div class="modal-header p-0 text-white bg-transparent border-0">
                    <div class="row align-items-center w-100">
                        <div class="col">
                            <h5 class="modal-title uppercase font-weight-bold">O2O C3C SYSTEM SERVICE PLAN</h5>
                        </div>
                        <div class="col-12 col-md-auto pt-3 pt-md-0">
                            <input type="file" id="poster" name="poster" style="width: 1px; height: 1px !important;" />
                            <label class="btn rounded text-accent bg-white pt-2 pb-2 pl-4 pr-4 mr-5" for="poster">UPDATE</label>
                        </div>
                    </div>
                    <button type="button" class="close text-white p-0" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mt-5 p-5 bg-white modal-scrollable modal-hide-scrollbar" style="height: 75vh; overflow: overlay;">
                    <div class="row w-100 mx-auto" id="poster_wrapper">
                        <img class="img-fluid mx-auto" src="<?= base_url() ?>assets/uploads/plan_poster.png">
                    </div>
                </div>
                <div class="modal-footer hide">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        sortable($('#related_documents .content-list'));

        // ad-hoc fix for hard-code select dropdown
        $('form select').each(function(index, elem) {
            $(elem).val($(elem).data('selected'));
        });
        $('#plan_info').click(function(e) {
            $('#viewModel').modal('show');
        });

        const reserved_words = ['upload_elements'];
        const old_merchant_id = $('[name="merchant_id"]').val();
        const related_documents = $('#related_documents').data('files');
        const folder = $('#related_documents').data('folder');
        let _filesize = 0;
        let _html = '';

        insertuploadfilerow('#related_documents', related_documents, folder);

        $('#poster').on("change", function(e) {
            postfileupload(e, 'single', '#poster', "/admin/api/upload_plan_poster", 'poster', function() {
                $('#poster').next().addClass('disabled').text('UPDATING ...');
            }, function(response) {
                $('#poster_wrapper').find('img').remove();
                $('#poster_wrapper').append(`<img class="img-fluid mx-auto" src="/assets/uploads/plan_poster.png">`);

                $('#poster').next().removeClass('disabled').text('UPDATED!');
            });
        });


        deleterow('#related_documents .content-list', true, function(el) {
            const data = el.prev().find('.file-stats').data();

            const url = `/admin/api/unlink_file?filename=${encodeURIComponent(data.filename)}&folder=${folder}`
            fetch(url);
        });

        uploadfile('#related_documents', folder, 5, 10, function() {
            sortable('#related_documents .newrow', true);
            deleterow('#related_documents .newrow', true);
        });

        $('form').submit(function(e) {
            const _this = $(this);

            e.preventDefault();
            if ($('[name="merchant_password"]').val() !== $('[name="confirm_password"]').val()) {
                alert('Password and confirm password are not identical. Please check and submit again.');
                return false;
            }

            const merchant_id = $('[name="merchant_id"]').val();
            // if (reserved_words.contains(merchant_id.toLowercase())) {
            //     alert(`Merchant ID ${merchant_id} is reserved, please change and submit again!`);
            //     return false;
            // }

            fetch('/admin/api/check_duplicates/merchant/merchant_id/' + merchant_id).then(function(response) {
                return response.json();
            }).then(function(response) {
                console.log(response);
                if (response.error) {
                    alert(response.data.message);
                } else {
                    const count = response.data.count;
                    const count_cond = old_merchant_id != '' ?
                        count == 1 && old_merchant_id == merchant_id || count == 0 && old_merchant_id != merchant_id :
                        count == 0;
                    if (count_cond == false) {
                        alert(`Merchant ID ${merchant_id} is already used, please change and submit again!`);
                    } else {
                        console.log('submit');
                        _this.unbind('submit').submit();
                    }
                }
            });
        });
    });
    $("#update_button").on("click", function(event) {
        event.preventDefault();
        if (confirm("Are your sure to update this account?")) {
            $("form").submit();
        } else {

        }
    })
</script>