<style>
    select {
        -webkit-appearance: none;
    }
</style>
<div class="page-body">
    <div class="row pl-0 pl-xl-5">
        <div class="col-12">
            <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5"><?= $page ?></h4>
            <div class="page-content-section p-3 p-sm-5">
                <div class="position-relative">
                    <div id="merchant_datatables_filter" class="row position-absolute w-100" style="top: 0; left: 0; z-index: 1">
                        <div class="hidden-xs col-auto"><span class="pagination pt-2 pb-4 invisible">Showing 20 of 1,000 Merchants</span></div>
                        <div class="col pr-0 pt-3 pt-md-0">
                            <div class="row filters align-items-center justify-content-end">
                                <div class="form-group d-inline-block pr-2">
                                    <select name="industry">
                                        <option value="" selected>--Industry--</option>
                                        <option value="artwork">Artworks & Crafts</option>
                                        <option value="automobile">Automobiles</option>
                                        <option value="beauty">Beauty & Salon</option>
                                        <option value="ecommerce">eCommerce</option>
                                        <option value="fashion">Fashion & Textiles</option>
                                        <option value="finance">Finance</option>
                                        <option value="food">Food & Beverages</option>
                                        <option value="furniture">Furnitures</option>
                                        <option value="health">Healthcare Household Goods</option>
                                        <option value="hotel">Hotel & Tourism</option>
                                        <option value="industrial">Industrials & Construction</option>
                                        <option value="it">Information Technology</option>
                                        <option value="logistics">Logistics</option>
                                        <option value="media">Media & Entertainment</option>
                                        <option value="clubhouse">Membership Clubhouse</option>
                                        <option value="properties">Properties</option>
                                        <option value="retail">Retail</option>
                                        <option value="service">Service Sector</option>
                                        <option value="spa">Spa/Gym</option>
                                        <option value="support">Support Services</option>
                                        <option value="telecommunications">Telecommunications</option>
                                        <option value="travel">Travel & Leisure</option>
                                        <option value="others">Others</option>
                                    </select>
                                </div>
                                <div class="form-group d-inline-block pr-2">
                                    <select name="status">
                                        <option value="" selected>--Status--</option>
                                        <option value="Enterprise">Enterprise</option>
                                        <option value="Pro">Pro</option>
                                        <option value="Lite">Lite</option>
                                        <option value="Expired">Expired</option>
                                    </select>
                                </div>
                                <div class="form-group d-inline-block">
                                    <div class="input-group input-group-search pr-2"> <input type="text" class="fa" name="general_search" placeholder="Search">
                                        <div class="input-group-append"> <span class="input-group-text"><i class="fa fa-search"></i></span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="scroller">
                        <table border="0" class="w-100 dataTable border-0 pt-0 pt-md-5 invisible" id="merchant" width="100%">
                            <thead>
                                <tr>
                                    <th>Industry</th>
                                    <th>Company Name</th>
                                    <th>Merchant ID</th>
                                    <th>Password</th>
                                    <th>Date Created</th>
                                    <th>Valid Until</th>
                                    <th>Status</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>loading...</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <!-- <div class="scroller">
                    <table class="dataTable pt-5 w-100">
                        <thead>
                            <th>Merchant ID</th>
                            <th>Password</th>
                            <th>Date Created</th>
                            <th>Valid Until</th>
                            <th>Status</th>
                            <th></th>
                            <th></th>
                        </thead>
                        <tr style="margin-top: 30px;">
                            <td>
                                <p class="m-0 p-0 font-weight-bold">smartbiz</p>
                                <p class="subtitle">Smart Business</p>
                            </td>
                            <td>
                                <div class="text-security">********</div>
                            </td>
                            <td>05/11/2021</td>
                            <td>31/12/2022</td>
                            <td>
                                <div class="status enterprise">Enterprise</div>
                            </td>
                            <td><button class="btn btn-primary"> VIEW </button></td>
                            <td><button class="btn btn-primary"> EDIT </button></td>
                        </tr>
                        <tr style="margin-top: 30px;">
                            <td>
                                <p class="m-0 p-0 font-weight-bold"> kmstudio </p>
                                <p class="subtitle"> KM Studio </p>
                            </td>
                            <td>
                                <div class="text-security">********</div>
                            </td>
                            <td>05/11/2021</td>
                            <td>31/12/2022</td>
                            <td>
                                <div class="status pro">Pro</div>
                            </td>
                            <td><button class="btn btn-primary"> VIEW </button></td>
                            <td><button class="btn btn-primary"> EDIT </button></td>
                        </tr>
                        <tr style="margin-top: 30px;">
                            <td>
                                <p class="m-0 p-0 font-weight-bold"> youthretirement </p>
                                <p class="subtitle"> Youth Retirement </p>
                            </td>
                            <td>
                                <div class="text-security">********</div>
                            </td>
                            <td>05/11/2021</td>
                            <td>31/12/2022</td>
                            <td>
                                <div class="status lite">Lite</div>
                            </td>
                            <td><button class="btn btn-primary"> VIEW </button></td>
                            <td><button class="btn btn-primary"> EDIT </button></td>
                        </tr>
                    </table>
                </div> -->
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="viewModel" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg mx-auto" role="document">
            <div class="modal-content bg-accent">
                <div class="modal-content-wrapper">
                    <div class="modal-header p-0 text-white bg-transparent border-0">
                        <div class="row align-items-center w-100">
                            <div class="col">
                                <h5 class="modal-title uppercase font-weight-bold">Merchant Profile</h5>
                            </div>
                            <div class="col-12 col-md-auto pt-3 pt-md-0 hidden-md hidden-lg hidden-xl">
                                <a class="btn rounded text-accent bg-white pt-2 pb-2 pl-4 pr-4 mr-5 merchant_edit">EDIT</a>
                            </div>
                        </div>
                        <button type="button" class="close text-white p-0" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mt-5 p-5 bg-white">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <section class="pb-5">
                                    <div class="row">
                                        <div class="col-12 heading text-accent font-weight-bold pb-3">Merchant Information</div>
                                    </div>
                                    <div class="row pb-2">
                                        <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Company Name</div>
                                        <div class="col-auto company_name"></div>
                                    </div>
                                    <div class="row pb-2">
                                        <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Company Email</div>
                                        <div class="col-auto company_email"></div>
                                    </div>
                                    <div class="row pb-2">
                                        <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Company Hotline</div>
                                        <div class="col-auto company_hotline"></div>
                                    </div>
                                    <div class="row pb-2">
                                        <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Company Website</div>
                                        <div class="col-auto company_website"></div>
                                    </div>
                                    <div class="row pb-2">
                                        <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Company Website</div>
                                        <div class="col-auto company_industry"></div>
                                    </div>
                                </section>
                                <section class="pb-5">
                                    <div class="row">
                                        <div class="col-12 heading text-accent font-weight-bold pb-3">Account Setting</div>
                                    </div>
                                    <div class="row pb-2">
                                        <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Merchant ID</div>
                                        <div class="col-auto merchant_id"></div>
                                    </div>
                                    <div class="row pb-2">
                                        <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Password</div>
                                        <div class="col-auto merchant_password"></div>
                                    </div>
                                    <div class="row pb-2">
                                        <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Status</div>
                                        <div class="col-auto merchant_plan"></div>
                                    </div>
                                    <div class="row pb-2">
                                        <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Valid Until</div>
                                        <div class="col-auto merchant_valid_until"></div>
                                    </div>
                                </section>
                                <div class="pt-5 text-center hidden-xs hidden-sm">
                                    <a class="btn bg-accent text-white pt-2 pb-2 pl-4 pr-4 merchant_edit">EDIT</a>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <section class="pb-5">
                                    <div class="row">
                                        <div class="col-12 heading text-accent font-weight-bold pb-3">Staff Information</div>
                                    </div>
                                    <div class="row pb-2">
                                        <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Title</div>
                                        <div class="col-auto contact_title"></div>
                                    </div>
                                    <div class="row pb-2">
                                        <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Contact Person</div>
                                        <div class="col-auto contact_person"></div>
                                    </div>
                                    <div class="row pb-2">
                                        <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Phone</div>
                                        <div class="col-auto contact_phone"></div>
                                    </div>
                                    <div class="row pb-2">
                                        <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Email</div>
                                        <div class="col-auto contact_email"></div>
                                    </div>
                                </section>
                                <section id="related_documents" class="pb-5">
                                    <div class="heading text-accent font-weight-bold pb-3">Related Documents</div>
                                    <div class="row align-items-center">
                                        <div class="col-auto pb-4">
                                            <input type="file" id="related_documents_upload" name="upload[]" multiple data-filesize="0" data-filecount="0" style="width: 1px; height: 1px !important;" />
                                            <label class="btn btn-upload" for="related_documents_upload">UPLOAD FILE</label>
                                        </div>
                                        <div class="col-auto">
                                            <span class="upload-text red ml-2">(Maximum 5 items, 10MB)</span>
                                        </div>
                                    </div>
                                    <div class="content-list ui-sortable mt-3 pt-3 border-top"></div>
                                </section>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer hide">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        sortable($('#related_documents .content-list'));
        const onComplete = function() {
            $('.text-security').off('click').on('click', function(e) {
                $(this).toggleClass('reveal');
            });

            $('.view').off('click').on('click', function(e) {
                fetch('/admin/api/get_merchant/' + $(this).data('merchantid'))
                    .then(function(response) {
                        return response.json();
                    })
                    .then(function(response) {
                        console.log(response);
                        for (var key in response) {
                            $('#viewModel').find('.' + key).text(response[key]);
                        }
                        $('.merchant_edit').attr('href', `/admin/merchant/edit/${response.merchant_id}`);

                        insertuploadfilerow('#related_documents', response.related_documents, response.merchant_id);

                        const fnDeleterow = function() {
                            deleterow('#related_documents .content-list', true, function(el) {
                                const data = el.prev().find('.file-stats').data();

                                const url = `/admin/api/unlink_file?filename=${encodeURIComponent(data.filename)}&folder=${response.merchant_id}`
                                fetch(url);
                            });
                        };
                        uploadfile('#related_documents', response.merchant_id, 0, 0, function(e) {
                            fnDeleterow();
                            postfileupload(e, 'multiple', '#related_documents', "/admin/api/upload_multiple/" + response.merchant_id, 'files');
                        });
                        fnDeleterow();

                        $('#viewModel').modal('show');
                    });
            });
        }

        let dtOptions = {
            "serverSide": true,
            // use a scroller class for replacement
            "responsive": false,
            "ajax": "/datatables/merchant",
            // dom, can be different from default
            "dom": '<"top"i>rt',
            "columnDefs": [{
                // company_industry, company_name
                "targets": [0, 1],
                "visible": false,
                "searchable": true,
            }, {
                // merchant_password
                "targets": [3],
                "visible": true,
                "searchable": false,
            }],
            "initComplete": function(settings, json) {
                $('#merchant').removeClass('invisible');

                onComplete();
            },
            "drawCallback": function(settings) {
                onComplete();
            }
        };
        let table = $('#merchant').dataTable(dtOptions);

        // Apply the filter
        $('[name="industry"]').on('keyup change', function() {
            table.fnFilter(this.value);
        });
        $('[name="status"]').on('keyup change', function() {
            table.fnFilter(this.value);
        });
        $('[name="general_search"]').on('input blur', function() {
            table.fnFilter(this.value);
        });
    });
</script>