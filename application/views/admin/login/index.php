<!--<html lang="en">-->
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo "O2O C3C CMS - 登入"; ?></title>
    <link href="<?= base_url('public/static/backend/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!--    <link rel="stylesheet" href="/assets/css/cmsstyle.css">-->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900"-->
    <link rel="stylesheet" href="<?= base_url('public/static/backend/css/googlecatamariancss.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('public/static/backend/css/googlelato.css'); ?>">
    <!--    <link rel="stylesheet" href="/assets/at-admin/css/breadcrumbs.css">-->
    <link rel="stylesheet" href="<?= base_url('public/static/backend/css/systemcss.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/admin-login.css?v=' . rand(10000, 100000)); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/o2o-c3c.css?v=' . rand(10000, 100000)); ?>">
    <meta name="description" content="">
    <meta name="author" content="">
</head>

<body style="background-color:#fff8f2!important;">
    <style>
    </style>
    <div id="flex-box">
        <section id="section-1" data-pwd="<?= md5('123456') ?>">
            <div class="container login-container col-12" style="background-color: #ffffff;">
                <div class="content">
                    <div class="col-md-6 login-form-1 text-center">
                        <div>
                            <img id="logo" class="d-flex" src="<?= base_url('assets/images/logo.png'); ?>">
                            <img class="d-flex logo-bottom-image d-none d-md-block" src="<?= base_url('assets/images/image.png'); ?>">
                        </div>
                        <div class="vl d-none d-md-block"></div>
                    </div>
                    <div class="col-md-6 login-form-2 my-auto">
                        <?php echo form_open('cms/login', ['name' => 'form_reg', 'id' => 'form_reg']); ?>
                        <h3 class="title">Welcome!</h3>
                        <p class="subtitle text-center">Login to your account</p>
                        <div class="form-group row">
                            <div class="col-12">
                                <input id="userName" name="userName" type="text" style="background-color : #d1d1d1;" class="form-control" placeholder="Merchant ID" value="<?= set_value('userName') ?>">
                                <?php echo form_error('email'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <input id="password" name="password" type="password" style="background-color : #d1d1d1;" class="form-control" placeholder="Password" value="<?= set_value('password') ?>">
                                <?php echo form_error('password'); ?>
                                <?php if (!empty($msg)) {
                                    echo $msg;
                                } ?>
                            </div>
                        </div>
                        <p class="forgot-password text-center">Forgot Password? <a href="mailto:info@smartbusiness.com.hk">Contact Us</a></p>
                        <div class="form-group row buttonrow">
                            <div class="col-sm-12 mt-3">
                                <div class="text-center">
                                    <input type="submit" class="btnSubmit" value="LOGIN">
                                </div>
                            </div>
                        </div>

                        <?php if ($this->session->flashdata('logout_msg') != false) { ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <h4><i class="icon fa fa-check"></i> Success!</h4>
                                <?php echo $this->session->flashdata('logout_msg'); ?>
                            </div>
                        <?php } ?>

                        <?php if ($this->session->flashdata('msg') != false) { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                                <?php echo $this->session->flashdata('msg'); ?>
                            </div>
                        <?php } ?>

                        <?php if (validation_errors() != false) { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                                <?php echo validation_errors(); ?>
                            </div>
                        <?php } ?>

                        </form>
                    </div>
                </div>
            </div>

        </section>
    </div>


    <!-- Bootstrap core JavaScript -->
    <script src="<?= base_url('public/static/backend/js/vendor/jquery-1.12.4.min.js'); ?>"></script>
    <script src="<?= base_url('public/static/backend/js/bootstrap.bundle.min.js'); ?>"></script>


</body>

</html>