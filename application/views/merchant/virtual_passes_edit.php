<div class="page-body">
    <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5">Membership</h4>
    <div class="row">
        <div class="col-12 col-md-4 mb-4 mb-md-0 text-center" id="mobile_preview">
            <ul class="nav justify-content-center mb-4">
                <li class="nav-item ml-2 mr-2 mb-2"><a href="#" data-preview="apple_front" class="btn btn-default p-2 mobile_preview_button preview_selected">Apple Front</a></li>
                <li class="nav-item ml-2 mr-2 mb-2"><a href="#" data-preview="apple_back" class="btn btn-default p-2 mobile_preview_button">Apple Back</a></li>
                <li class="nav-item ml-2 mr-2 mb-2"><a href="#" data-preview="google_front" class="btn btn-default p-2 mobile_preview_button">Google Front</a></li>
                <li class="nav-item m1-2 mr-2 mb-2"><a href="#" data-preview="google_back" class="btn btn-default p-2 mobile_preview_button">Google Back</a></li>
            </ul>
            <div class="w-100 mobile_preview_container" id="apple_front">
                <div class="apple_mobile_preview_container">
                    <div class="phone">
                        <div id="apple_card_frame" class="card_frame d-flex flex-column" style="background:#000000;">
                            <div class="card_header">
                                <div class="col-2 p-2 mr-2">
                                    <img id="apple_logo_image" class="apple_mobile_logo" src="">
                                </div>
                                <div class="col-10 pl-4 text-left">
                                    <span id="apple_header_text" class="apple_header_text apple_phone_label"></span>
                                </div>
                            </div>
                            <div class="card_image">
                                <img id="apple_strip_image" class="apple_strip_image" src="">
                            </div>
                            <div class="data_row">
                                <div class="d-flex apple_data_row" id="apple_data_row" style="overflow:scroll">
                                    <div class="col" id="apple_data_1">
                                        <span id="apple_data_1_label" class="data_label apple_phone_label"></span>
                                        <div id="apple_data_1_data" class="apple_phone_value">value</div>
                                    </div>
                                    <div class="col" id="apple_data_2">
                                        <span id="apple_data_2_label" class="data_label apple_phone_label"></span>
                                        <div id="apple_data_2_data" class="apple_phone_value">
                                            value
                                        </div>
                                    </div>
                                    <div class="col" id="apple_data_3">
                                        <span id="apple_data_3_label" class="data_label apple_phone_label"></span>
                                        <div id="apple_data_3_data" class="apple_phone_value">
                                            value
                                        </div>
                                    </div>
                                    <div class="col" id="apple_data_4">
                                        <span id="apple_data_4_label" class="data_label apple_phone_label"></span>
                                        <div id="apple_data_4_data" class="apple_phone_value">
                                            value
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="qr_code">
                                <div class="qr_container">
                                    <img class="" src="<?= base_url('assets/images/example_qr.png') ?>">
                                    <span class="label">ID</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 mobile_preview_container" id="apple_back" style="display:none;">
                <div class="apple_mobile_back_preview_container">
                    <div class="phone">
                        <div class="card_frame d-flex flex-column">
                            <div class="col_1">
                                <div>
                                    Our Website
                                </div>
                                <span id="backside_hyperlink" class="backside_hyperlink"></span>
                            </div>
                            <div class="col_2">
                                <div>
                                    About Us
                                </div>
                                <span id="backside_about_us" class="backside_about_us"></span>
                            </div>
                            <div class="col_3">
                                <div>
                                    Terms & Conditions
                                </div>
                                <span id="backside_terms_conditions" class="backside_terms_conditions"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 mobile_preview_container" id="google_front" style="display:none;">
                <div class="google_mobile_preview_container">
                    <div class="phone">
                        <div id="google_card_frame" class="card_frame" style="background-color:#000000">
                            <div class="card_header d-flex">
                                <div class="col-2">
                                    <img id="google_logo_image" class="google_logo_image" src="">
                                </div>
                                <div class="col-10">
                                    <span id="google_header_text" class="google_phone_text"></span>
                                </div>
                            </div>
                            <div class="data_row">
                                <h2 id="google_card_name" class="m-4 text-left font-weight_bold google_phone_text"></h2>
                                <div id="google_data_row_1" class="row mb-4">
                                    <div id="google_phone_data_1_set" class="col-6">
                                        <h6 id="google_data_1_label" class="ml-4 mr-4 google_phone_text"></h6>
                                        <h5 id="google_data_1_data" class="ml-4 mr-4 google_phone_text">Value</h5>
                                    </div>
                                    <div id="google_phone_data_2_set" class="col-6 ">
                                        <h6 id="google_data_2_label" class="ml-4 mr-4 google_phone_text"></h6>
                                        <h5 id="google_data_2_data" class="ml-4 mr-4 google_phone_text">Value</h5>
                                    </div>
                                </div>
                                <div id="google_data_row_2" class="row mb-4">
                                    <div id="google_phone_data_3_set" class="col-6">
                                        <h6 id="google_data_3_label" class="ml-4 mr-4 google_phone_text"></h6>
                                        <h5 id="google_data_3_data" class="ml-4 mr-4 google_phone_text">Value</h5>
                                    </div>
                                    <div id="google_phone_data_4_set" class="col-6">
                                        <h6 id="google_data_4_label" class="ml-4 mr-4 google_phone_text"></h6>
                                        <h5 id="google_data_4_data" class="ml-4 mr-4 google_phone_text">Value</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="qr_code flex-column">
                                <div class="qr_code_container">
                                    <img src="<?= base_url("assets/images/example_qr.png") ?>">
                                </div>
                                <span class="google_phone_text mt-2" style="font-size:10px!important;">ID</span>
                            </div>
                            <div class="card_image">
                                <img id="google_hero_image" class="google_card_image" src="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 mobile_preview_container" id="google_back" style="display:none;">
                <div class="google_mobile_back_preview_container">
                    <div class="phone">
                        <div class="backlight">
                            <div class="card_frame">
                                <div clsas="header_block"></div>
                                <div class="pl-4 pr-4 pt-3 pb-3">
                                    Details
                                </div>
                                <div class="pl-4 pr-4 pt-3 pb-3 d-flex align-items-center">
                                    <i style="font-size:20px;color:#0b79ff" class="far fa-globe-americas"></i><span class="pl-4 font-weight-bold">Our Website</span>
                                </div>
                                <div class="pl-4 pr-4 pt-3 pb-3">
                                    <div class="font-weight-bold">About Us</div>
                                    <span id="google_backside_about_us"></span>
                                </div>
                                <div class="pl-4 pr-4 pt-3 pb-3">
                                    <div class="font-weight-bold">Terms & Conditions</div>
                                    <span id="google_backside_terms_conditions"></span>
                                </div>
                                <div class="pl-4 pr-4 pt-3 pb-3">
                                    <p style="font-size:10px;line-height:1;">The pass provider or merchant is responsible for the info on this pass and may send you notifications. Contact them with any questions.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="col-12 col-md-8 ">
            <div class="editor" style="max-height:80vh;">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item first-block">
                        CREATOR
                    </li>
                    <!--<li class="nav-item">
                        <a class="nav-link active" id="trial-tab" data-toggle="tab" href="#trial" role="tab" aria-controls="home" aria-selected="true">Trial</a>
                    </li>-->
                    <li class="nav-item">
                        <a class="nav-link active" id="setting-tab" data-toggle="tab" href="#setting" role="tab" aria-controls="home" aria-selected="true"><img class="img-fluid" src="<?= base_url("assets/images/merchant/setting.png") ?>">&nbsp;<span class="tab_word">Setting</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="form-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/form.png") ?>">&nbsp;<span class="tab_word">Form</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="header-tab" data-toggle="tab" href="#header" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/header.png") ?>">&nbsp;<span class="tab_word">Header</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="theme-tab" data-toggle="tab" href="#theme" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/theme.png") ?>">&nbsp;<span class="tab_word">Theme</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="layout-tab" data-toggle="tab" href="#layout" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/layout.png") ?>">&nbsp;<span class="tab_word">Layout</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/detail.png") ?>">&nbsp;<span class="tab_word">Details</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="geotag-tab" data-toggle="tab" href="#geotag" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/geotag.png") ?>">&nbsp;<span class="tab_word">Geotag(Beta)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="level-tab" data-toggle="tab" href="#level_management" aria-controls="contact" aria-selected="false"><i class="fa fa-sort-amount-asc dark_blue" aria-hidden="true"></i>&nbsp;&nbsp;<span class="tab_word">Level</span></a>
                    </li>
                    <!--<li class="nav-item">
                        <a class="nav-link" id="export-tab" data-toggle="tab" href="#export" role="tab" aria-controls="contact" aria-selected="false"><img clsas="img-fluid" src="<?= base_url("assets/images/merchant/export.png") ?>">&nbsp;<span class="tab_word">Export</span></a>
                    </li>-->
                    <li class="nav-item ml-auto end-block">
                        <a class="nav-link" data-toggle="tab" href="#help" aria-controls="contact" aria-selected="false" style="padding:0px!important;border-radius:50px important;display:flex!important;align-items:center;background-color:transparent!important;border:none!important;">
                            <i class="fas fa-list"></i>&nbsp;<span class="tab_word" style="color:white;">Help</span>
                        </a>
                    </li>

                </ul>
                <form id="creator_form" action="/merchant/virtual_passes/create_virtual_passes" method="post" enctype="multipart/form-data">
                    <div class="tab-content" id="myTabContent">
                        <button id="next_button" class="btn blue_button" style="border-radius:15px 0px 15px 0px!important;">NEXT</button>
                        <button id="export_button" class="btn blue_button" style="border-radius:15px 0px 15px 0px!important;">Save</button>
                        <!--<div class="tab-pane fade show " id="trial" role="tabpanel" aria-labelledby="home-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <button type="submit" id="submit_button" class="btn submit_button" style="position:absolute;right:0;bottom:0;background-color:#4a5ba1;color:white;border-radius:15px 0px 15px 0px!important;padding:10px 15px;">SUBMIT</button>
                            </div>
                        </div>-->
                        <div class="tab-pane fade active show" id="setting" role="tabpanel" aria-labelledby="home-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        Type of Virtual Passes &nbsp;&nbsp;&nbsp;<select disabled>
                                            <option>Please Select</option>
                                            <option value="membership" selected>Membership Card</option>
                                            <option>Name Card</option>
                                            <option>Stamp Card</option>
                                            <option>Info Sheet</option>
                                            <option>Ticket</option>
                                            <option>Coupon</option>
                                        </select>
                                    </li>

                                    <li>
                                        <span class="mr-4">Usage</span>
                                        <select name="type" >
                                            <option value="0">General</option>
                                            <option value="1">API purpose</option>
                                        </select>
                                    </li>
                                    <li>
                                        Project Name<span style="color:red;">*</span>
                                        <input type="text" id="management_name" class="form-control" name="management_name" required>
                                    </li>
                                    <li>
                                        Programme Name<span style="color:red;">*</span>
                                        <input type="text" id="project_name" class="form-control" name="project_name" required>
                                    </li>
                                    <li>
                                        Card Name<span style="color:red;">*</span>
                                        <input id="card_name" type="text" class="form-control" name="description" required>
                                    </li>
                                    <li>
                                        <div class="d-flex align-items-center">
                                            <div class="mr-2" style="white-space:nowrap;">
                                                Membership Number starting from
                                            </div>
                                            <input type="number" min="1" class="form-control" name="start_from" value="1">
                                        </div>
                                    </li>
                                    <li>
                                        Icon<span style="color:red;">*</span>
                                        <input type="file" id="icon_upload_n" name="icon" class="form-control mb-2" style="height:40px!important;opacity:1!important;" accept=".png" required>
                                        Recommended size: 29px(W) x 29px(H)
                                        <div class="pt-4">
                                            <h6 style="color:red">* required</h6>
                                        </div>
                                    </li>

                                </ul>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab" style="overflow-y:scroll;">

                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        Data Required<br>
                                        <input type="radio" id="data_requirement_simple" name="data_requirement" checked value="simple" style="display:none">
                                        <input type="radio" id="data_requirement_informative" name="data_requirement" value="informative" style="display:none">
                                        <button class="btn radio_button button_simple selected" value="simple">SIMPLE</button>
                                        <button class="btn radio_button button_informative" value="informative">INFORMATIVE</button>
                                    </li>
                                    <li id="personal_information_table" style="display:none;">Personal Information<br>

                                        <div class="row ">
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="name" name="name" style="display:none;">
                                                <button id="NAME_BUTTON" class="btn checkbox_button" value="NAME">NAME</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="SURNAME" name="surname" style="display:none;">
                                                <button id="SURNAME_BUTTON" class="btn checkbox_button" value="SURNAME">SURNAME</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="GIVEN_NAME" name="given_name" style="display:none;">
                                                <button id="GIVEN_NAME_BUTTON" class="btn checkbox_button" value="GIVEN_NAME">GIVEN NAME</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="ENGLISH_NAME" name="english_name" style="display:none;">
                                                <button id="ENGLISH_NAME_BUTTON" class="btn checkbox_button" value="ENGLISH_NAME">ENGLISH NAME</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="CHINESE_NAME" name="chinese_name" style="display:none;">
                                                <button id="CHINESE_NAME_BUTTON" class="btn checkbox_button" value="CHINESE_NAME">CHINESE NAME</button>
                                            </div>
                                            <!--</div>
                                        <div class="row mb-2"> -->
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="GENDER" name="gender" style="display:none;">
                                                <button id="GENDER_BUTTON" class="btn checkbox_button" value="GENDER">GENDER</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="BIRTHDAY" name="birthday" style="display:none;">
                                                <button id="BIRTHDAY_BUTTON" class="btn checkbox_button" value="BIRTHDAY">BIRTHDAY</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="AGE" name="age" style="display:none;">
                                                <button id="AGE_BUTTON" class="btn checkbox_button" value="AGE">AGE</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="NATIONALITY" name="nationality" style="display:none;">
                                                <button id="NATIONALITY_BUTTON" class="btn checkbox_button" value="NATIONALITY">NATIONALITY</button>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="contact_information_table" style="display:none;">
                                        Contact Information

                                        <div class="row mb-2">
                                            <div class="col-3">
                                                <input type="checkbox" id="PHONE" name="phone" style="display:none;">
                                                <button id="PHONE_BUTTON" class="btn checkbox_button" value="PHONE">PHONE</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="EMAIL" name="email" style="display:none;">
                                                <button id="EMAIL_BUTTON" class="btn checkbox_button" value="EMAIL">EMAIL</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="ADDRESS" name="address" style="display:none;">
                                                <button id="ADDRESS_BUTTON" class="btn checkbox_button" name="address" value="ADDRESS">ADDRESS</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="INDUSTRY" name="industry" style="display:none;">
                                                <button id="INDUSTRY_BUTTON" class="btn checkbox_button" value="INDUSTRY">INDUSTRY</button>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="work_information_table" style="display:none;">
                                        Work Information

                                        <div class="row mb-2">
                                            <div class="col-3">
                                                <input type="checkbox" id="INDUSTRY_2" name="industry_2" style="display:none;">
                                                <button id="INDUSTRY_2_BUTTON" class="btn checkbox_button" value="industry_2">INDUSTRY</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="JOB_TITLE" name="job_title" style="display:none;">
                                                <button id="JOB_TITLE_BUTTON" class="btn checkbox_button" value="JOB_TITLE">JOB TITLE</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="COMPANY" name="company" style="display:none;">
                                                <button id="COMPANY_BUTTON" class="btn checkbox_button" value="COMPANY">COMPANY</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="SALARY" name="salary" style="display:none;">
                                                <button id="SALARY_BUTTON" class="btn checkbox_button" value="SALARY">SALARY</button>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="simple_remark" style="display:block">
                                        Simple includes "English Name","Chinese Name","Phone","Email"
                                    </li>
                                </ul>
                            </div>


                        </div>
                        <div class="tab-pane fade" id="theme" role="tabpanel" aria-labelledby="contact-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">

                                    <li>
                                        Title Colour<span style="color:red;">*</span><br>
                                        <div class="d-flex">
                                            <input type="color" name="secondary_color" id="label_color" value="#FFFFFF">
                                            <input type="text" class="form-control ml-4 mr-4" id="secondary_color_hex" value="#FFFFFF">
                                        </div>

                                    </li>
                                    <li>
                                        Content Colour<span style="color:red;">*</span><br>
                                        <div class="d-flex">
                                            <input type="color" name="pri_color" id="value_color" value="#FFFFFF">
                                            <input type="text" class="form-control ml-4 mr-4" id="pri_color_hex" value="#FFFFFF">
                                        </div>
                                    </li>
                                    <li>Background Colour<span style="color:red;">*</span><br>
                                        <div class="d-flex mb-2">
                                            <input type="color" name="background_color" id="background_color" value="#000000">
                                            <input type="text" class="form-control ml-4 mr-4" id="background_color_hex" value="#000000">
                                        </div>
                                        <span>Google's pass will assign the text colour base on the background colour.</span>
                                    </li>
                                    <li>
                                        <div class="">
                                            <div class="" style="width:65px;">Banner<span style="color:red;">*</span></div>
                                            <div class="col mb-2 p-0"><input type="file" class="form-control" name="pri_strip" id="banner_upload_n" style="height:40px!important;opacity:1!important;" accept=".png" required></div>
                                            Recommended size: 523px(W) x 175px(H)
                                        </div>
                                        <div class="pt-4">
                                            <h6 style="color:red">* required</h6>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="header" role="tabpanel" aria-labelledby="contact-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        LOGO<span style="color:red">*</span>
                                        <input type="file" id="logo_upload_n" name="logo" class="form-control mb-2" style="height:40px!important;opacity:1!important;" accept=".png" required>
                                        Recommended Size: 100px(W) x 100px(H)
                                    </li>
                                    <li>
                                        Header Text<span style="color:red;">*</span><br>
                                        <div class="d-flex">
                                            <input type="text" style="height:100%;" id="header_text" name="logo_text" class="form-control" placeholder="Header Content" required>
                                        </div>
                                        <div class="pt-4">
                                            <h6 style="color:red">* required</h6>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="layout" role="tabpanel" aria-labelledby="contact-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>Content Data</li>
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" id="front_data_1_status" name="front_data_1_status" class="data_trigger" checked style="display:none;">
                                                <span class="slider round d-flex" style="align-items:center;">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" id="front_data_1_label" style="height:100%;" name="front_data_1_label" class="form-control data_trigger" placeholder="Label 1" value="Label 1">
                                        </div>
                                        <div class="col">
                                            <select style="height:100%;" name="front_data_1_data" class="form-control clear_select">
                                                <?php foreach ($front_fields as $index => $field) { ?>
                                                    <?php if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option value="<?= $index ?>"><?= $field ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" class="data_trigger" id="front_data_2_status" name="front_data_2_status" checked style="display:none;">
                                                <span class="slider round d-flex" style="align-items:center;">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" style="height:100%;" id="front_data_2_label" name="front_data_2_label" class="form-control" placeholder="Label 2" value="Label 2">
                                        </div>
                                        <div class="col">
                                            <select style="height:100%;" name="front_data_2_data" class="form-control clear_select">
                                                <?php foreach ($front_fields as $index => $field) { ?>
                                                    <?php if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option value="<?= $index ?>"><?= $field ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" class="data_trigger" id="front_data_3_status" name="front_data_3_status" checked style="display:none;">
                                                <span class="slider round d-flex" style="align-items:center;">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" id="front_data_3_label" style="height:100%;" class="form-control data_trigger" name="front_data_3_label" placeholder="Label 3" value="Label 3">
                                        </div>
                                        <div class="col">
                                            <select style="height:100%;" name="front_data_3_data" class="form-control clear_select">
                                                <?php foreach ($front_fields as $index => $field) { ?>
                                                    <?php if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option value="<?= $index ?>"><?= $field ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" class="data_trigger" id="front_data_4_status" name="front_data_4_status" checked style="display:none;">
                                                <span class="slider round d-flex" style="align-items:center;">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" style="height:100%;" id="front_data_4_label" name="front_data_4_label" class="form-control data_trigger" placeholder="Label 4" value="Label 4">
                                        </div>
                                        <div class="col">
                                            <select style="height:100%;" name="front_data_4_data" class="form-control clear_select">
                                                <?php foreach ($front_fields as $index => $field) { ?>
                                                    <?php if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option value="<?= $index ?>"><?= $field ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                            </select>
                                        </div>

                                    </div>
                                    <!--<div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" name="front_data_5_status" checked style="display:none;">
                                                <span class="slider round d-flex" style="align-items:center;">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" style="height:100%;" name="front_data_5_label" class="form-control" placeholder="Label 5">
                                        </div>
                                        <div class="col">
                                            <select style="height:100%;" name="front_data_5_data" class="form-control clear_select">
                                                <?php foreach ($front_fields as $index => $field) { ?>
                                                    <?php if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option value="<?= $index ?>"><?= $field ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                            </select>
                                        </div>

                                    </div>-->
                                    <!--<div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" name="front_data_6_label" checked style="display:none;">
                                                <span class="slider round d-flex" style="align-items:center;">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" style="height:100%;" name="front_data_6_label" class="form-control" placeholder="Label 6">
                                        </div>
                                        <div class="col">
                                            <select style="height:100%;" name="front_data_6_data" class="form-control clear_select">
                                                <?php foreach ($front_fields as $index => $field) { ?>
                                                    <?php if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option value="<?= $index ?>"><?= $field ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                            </select>
                                        </div>
                                    </div>-->
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="details" role="tabpanel" aria-labelledby="contact-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        <div style="display:flex;width:100%;">
                                            <div class="no_padding" style="width:65px">URL<span style="color:red;">*</span></div>
                                            <div class="col"><input type="text" id="website_url" name="url" class="form-control" placeholder="Please paste your link"></div>
                                        </div>
                                    </li>
                                    <li>
                                        About Us<span style="color:red;">*</span><br>
                                        <textarea class="form-control" id="about_us_content" name="about_us" placeholder="Please type here"></textarea>
                                    </li>
                                    <li>
                                        Terms & Conditions<span style="color:red;">*</span>
                                        <textarea class="form-control" id="terms_content" name="terms_conditions" placeholder="Please type here"></textarea>
                                        <div class="pt-4">
                                            <h6 style="color:red">* required</h6>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="geotag" role="tabpanel" aria-labelledby="contact-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        Location-based Push Notification<i class="fa fa-info-circle tips pl-2" aria-hidden="true"><span class="tips_content" style="z-index:1000;">How to get the coordinate ?<br>Right click at the destination on Google Maps</span></i>
                                        <div class="d-flex" style="margin:15px 0px;">
                                            <div>
                                                <label class="switch">
                                                    <input type="checkbox" name="location_1_status" checked style="display:none;">
                                                    <span class="slider round d-flex" style="align-items:center;">
                                                        <span class="on">ON</span>
                                                        <span class="off">OFF</span>
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="col row">
                                                <div class="col">
                                                    <input type="number" step="0.000001" style="height:100%;" name="location_1_lat" class="form-control" placeholder="Latitude">
                                                </div>
                                                <div class="col">
                                                    <input type="number" step="0.000001" style="height:100%" name="location_1_lon" class="form-control" placeholder="Longitude">
                                                </div>
                                                <div class="col-12 mt-2">
                                                    <textarea class="form-control" name="location_1_message" placeholder="Message"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex" style="margin:15px 0px;">
                                            <div>
                                                <label class="switch">
                                                    <input type="checkbox" name="location_2_status" checked style="display:none;">
                                                    <span class="slider round d-flex" style="align-items:center;">
                                                        <span class="on">ON</span>
                                                        <span class="off">OFF</span>
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="col row">
                                                <div class="col">
                                                    <input type="number" step="0.000001" style="height:100%" name="location_2_lat" class="form-control" placeholder="Latitude">
                                                </div>
                                                <div class="col">
                                                    <input type="number" step="0.000001" style="height:100%" name="location_2_lon" class="form-control" placeholder="Longitude">
                                                </div>
                                                <div class="col-12 mt-2">
                                                    <textarea class="form-control" name="location_2_message" placeholder="Message"></textarea>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="d-flex" style="margin:15px 0px;">
                                            <div>
                                                <label class="switch">
                                                    <input type="checkbox" name="location_3_status" checked style="display:none;">
                                                    <span class="slider round d-flex" style="align-items:center;">
                                                        <span class="on">ON</span>
                                                        <span class="off">OFF</span>
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="col row">
                                                <div class="col">
                                                    <input type="number" step="0.000001" style="height:100%" name="location_3_lat" class="form-control" placeholder="Latitude">
                                                </div>
                                                <div class="col">
                                                    <input type="number" step="0.000001" style="height:100%" name="location_3_lon" class="form-control" placeholder="Longitude">
                                                </div>
                                                <div class="col-12 mt-2">
                                                    <textarea class="form-control" name="location_3_message" placeholder="Message"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <h6 style="color:red">Google: Geofenced notifications are temporarily unavailable for a majority of users while we make platform improvements. We'll completely update this feature in an upcoming release.</h6>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="level_management" role="tabpanel" aria-labelledby="contact-tab" style="overflow-y:scroll;">

                            <div class="inner_padding">
                                <ul id="level_listing" class="d-flex flex-column">
                                    <li>
                                        <div class="mb-4">Level Setting</div>
                                        <div class="d-flex align-items-center justify-content-center">
                                            <div class="d-flex" style="width:12rem;">
                                                Default Level<span style="color:red;">*</span>:
                                            </div>

                                            <input type="text" id="default_level" class="form-control col" name="default_level_value" required>
                                            <div class="" style="width:50px;"></div>
                                        </div>
                                    </li>
                                </ul>
                                <ul class="d-flex flex-column">
                                    <li class="d-flex">
                                        <div>
                                            <a class="btn text-center" id="add_level_row" style="width:5rem;"><i class="fa fa-plus m-0" aria-hidden="true"></i></a>
                                        </div>
                                    </li>
                                    <li class="d-flex">
                                        <div class="pt-4">
                                            <h6 style="color:red">* required</h6>
                                        </div>
                                    </li>
                                </ul>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="export" role="tabpanel" aria-labelledby="contact-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        <div style="display:flex;width:100%;">
                                            <div class="no_padding">Membership Number starting from</div>
                                            <div class="col"><input type="text" class="form-control" placeholder=""></div>
                                        </div>
                                    </li>
                                    <li>
                                        Generated Application URL<i class="fas fa-sync-alt"></i> <br>
                                        <div style="display:flex;width:100%;">
                                            <input type="text" class="form-control"><button class="btn blue_button" style="padding-left:15px;padding-right:15px;border-radius:5px!important;margin-left:15px;">COPY</button>
                                        </div>
                                    </li>
                                    <li>
                                        Generated Application QR Code <i class="fas fa-sync-alt"></i> <br>
                                        <div class="row">
                                            <div class="col-12 col-md-6">
                                                <div style="display:flex;width:100%;margin:15px 0px;">
                                                    <div class="col">
                                                        <select class="form-control clear_select">
                                                            <option>
                                                                --Slect Body Shape--
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div>
                                                        <input type="color" id="" class="color_picker_selector" style="visibility:hidden;height:0px;width:0px;">
                                                    </div>
                                                    <div>
                                                        <div class="color_picker"></div>

                                                    </div>
                                                </div>
                                                <div style="display:flex;width:100%;margin:15px 0px;">
                                                    <div class="col">
                                                        <select class="form-control clear_select">
                                                            <option>
                                                                --Slect Eye Frame--
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div>
                                                        <input type="color" id="" class="color_picker_selector" style="visibility:hidden;height:0px;width:0px;">
                                                    </div>
                                                    <div>
                                                        <div class="color_picker"></div>

                                                    </div>
                                                </div>
                                                <div style="display:flex;width:100%;margin:15px 0px;">
                                                    <div class="col">
                                                        <select class="form-control clear_select">
                                                            <option>
                                                                --Slect Eye Ball--
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div>
                                                        <input type="color" id="" class="color_picker_selector" style="visibility:hidden;height:0px;width:0px;">
                                                    </div>
                                                    <div>
                                                        <div class="color_picker"></div>

                                                    </div>
                                                </div>
                                                <div>
                                                    <img class="img-fluid" style="max-width:50px;" src="<?= base_url("assets/images/temp2.png") ?>">&nbsp;&nbsp;<button class="btn blue_button" style="border-radius:10px!important;">UPLOAD ICON</button>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="QR_container">
                                                            <img class="img-fluid w-100" src="<?= base_url("assets/images/temp_qr_1.png") ?>">
                                                            <h5>
                                                                APPLE WALLET
                                                            </h5>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="QR_container">
                                                            <img class="img-fluid w-100" src="<?= base_url("assets/images/temp_qr_2.png") ?>">
                                                            <h5>
                                                                GOOGLE PAY
                                                            </h5>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="help" role="tabpanel" aria-labelledby="home-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>Got some problems? Watch the tutorial!<br>
                                        <video controls style="width:100%;height:auto;">
                                            <source src="<?= base_url("/assets/tutorial_video/O2OC3C.m4v") ?>" type="video/mp4">
                                        </video>
                                    </li>
                                    <li>Problem not solved? <a href="#">Contact Us</a></li>

                                </ul>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url("assets/js/virtual_pass_editor.js") ?>"></script>

<script>
    var adj_count = 1;
    var current_page = "add";
    $("#add_level_row").on("click", function() {
        $("#level_listing").append(
            '<li class="d-flex" id="level_' + adj_count + '"><div class="d-flex align-items-center justify-content-center" style="width:12rem;">Optional :</div><input type="hidden" name="level[exist_record][]" value="false"><input type="hidden" name="level[level_id][]" value=""><input type="text" class="form-control col" name="level[value][]"><a data-exist="false" data-id="' + adj_count + '" class="delete_level d-flex align-items-center justify-content-center" style="width:50px;cursor:pointer;"><i class="fa fa-times" aria-hidden="true"></i></a></li>'
        );
        adj_count++;
        delete_button_init();
    })

    $(document).ready(function() {
        var init_value_color = "#FFFFFF";
        var init_label_color = "#FFFFFF";
        var init_background_color = "#000000";
        update_color(init_background_color);
        google_data_row_update();
        apple_data_row_update();
        $(".apple_phone_label").css("color", init_value_color);
        $(".apple_phone_value").css("color", init_label_color);

    })

    function delete_button_init() {
        $(".delete_level").unbind();

        $(".delete_level").on("click", function() {
            if ($(this).data("exist") == true) {
                $.ajax({
                    url: base_url + "/merchant/virtual_passes/check_user_ref",
                    method: "post",
                    dataType: "json",
                    data: {
                        "id": $(this).data("id"),
                        "pass_id": pass_id,
                        "row_id": $(this).data('row_id')
                    },
                    success: function(res) {
                        if (res.delete == 0) {
                            alert("有會員處於這個等級，不能刪除");
                        }
                        if (res.delete == 1) {
                            $("#level_" + res.row_id).remove();
                            alert("刪除成功");
                        }
                    },
                    failed: function(res) {

                    }
                });
            } else {
                $("#level_" + $(this).data("id")).remove();
            }

        })

    }
</script>