

<div class="page-body">

   <div class="row pl-0 pl-xl-5">
      <div class="col-12">
         <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5"><?= $page ?></h4>
         <div class="page-content-section p-3 p-sm-5">
            <div class="position-relative">
               <div id="member_database_filter" class="row  w-100" style="top: 0; left : 0; z-index: 1">
                  <div class=" col-auto">

                  </div>
                  <div class="top">
                     <div class="dataTables_info" id="merchant_info" role="status" aria-live="polite">Showing <span id="start"></span> to <span id="end"></span> of <span id="counter"></span> entries</div>
                  </div>
                  <div class="col pr-0 pt-3 pt-md-0">
                     <div class="row filters align-items-center justify-content-end">
                        <div class="form-group d-inline-block pr-2">
                           <a href="/merchant/smart_memberbase/export_csv" class="btn">Export Database</a>
                        </div>
                        <div class="form-group d-inline-block pr-2">
                           <button id="btn_marketing_tools" class="btn">Marketing Tools</button>
                        </div>
                        <!-- -->
                        <div class="form-group d-inline-block pr-2">
                           Virtual pass:&nbsp;
                           <select id="passes" name="passes">
                              <option value="">All</option>
                              <?php foreach ($cards as $card) { ?>
                                 <option value="<?= $card['id'] ?>"><?= $card['project_name'] ?></option>
                              <?php } ?>

                           </select>
                        </div>
                        <!-- -->
                        <div class="form-group d-inline-block">
                           <div class="input-group input-group-search pr-2"> <input type="text" class="fa" name="general_search" placeholder="Search">
                              <div class="input-group-append"> <span class="input-group-text"><i class="fa fa-search"></i></span> </div>
                           </div>
                        </div>
                        <!-- -->
                     </div>
                  </div>
               </div>
               <div class="scroller">

                  <table border="0" class="w-100 dataTable border-0 pt-0 pt-md-5 invisible" id="member" width="100%">
                     <thead>
                        <tr>
                           <th>ID</th>
                           <th>Pass ID</th>
                           <th>Account ID</th>
                           <th>Name</th>
                           <th>Gender</th>
                           <th>Phone</th>
                           <th>Email</th>
                           <th>Address</th>
                           <th></th>
                           <th></th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>Loading...</td>
                        </tr>
                     </tbody>
                  </table>
                  <div style="text-align:center">
                     <button id="previous_page" class="btn">Previous</button>
                     <button id="next_page" class="btn">Next</button>

                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="modal fade" id="viewModel" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg mx-auto" role="document">
         <div class="modal-content bg-accent">
            <div class="modal-content-wrapper">
               <div class="modal-header p-0 text-white bg-transparent border-0">
                  <div class="row align-items-center w-100">
                     <div class="col">
                        <h5 class="modal-title uppercase font-weight-bold">Member Profile</h5>
                     </div>
                     <div class="col-12 col-md-auto pt-3 pt-md-0 hidden-md hidden-lg hidden-xl">
                        <a class="btn rounded text-accent bg-white pt-2 pb-2 pl-4 pr-4 mr-5 merchant_edit">EDIT</a>
                     </div>
                  </div>
                  <button type="button" class="close text-white p-0" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body mt-5 p-5 bg-white" style="border-radius:30px!important;">
                  <div class="row">

                     <div class="col-12 heading text-accent font-weight-bold pb-3">Account Setting</div>

                     <div class="col-6">
                        <div class="row pb-2">

                           <h4 class="col-md-5 pl-4 text-accent font-weight-bold sub-title">Pass Information</h4>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Pass ID</div>
                           <div id="pass_id" class="col-auto "></div>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Acount ID</div>
                           <div id="account_id" class="col-auto"></div>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Level</div>
                           <div id="level" class="col-auto"></div>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Points</div>
                           <div id="points" class="col-auto"></div>
                        </div>
                        <!--<div class="row pb-2">
                              <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Name</div>
                              <div id="account_name" class="col-auto "></div>
                           </div>-->
                        <div class="row pb-2">
                           <h4 class="col-md-5 pl-4 text-accent font-weight-bold sub-title">Personal Info</h4>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Surname</div>
                           <div id="surname" class="col-auto"></div>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Given Name</div>
                           <div id="given_name" class="col-auto"></div>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">English Name</div>
                           <div id="english_name" class="col-auto"></div>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Chinese Name</div>
                           <div id="chinese_name" class="col-auto"></div>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Birthday</div>
                           <div id="birthday" class="col-auto"></div>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Age</div>
                           <div id="age" class="col-auto"></div>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Nationality</div>
                           <div id="nationality" class="col-auto"></div>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Gender</div>
                           <div id="account_gender" class="col-auto "></div>
                        </div>
                     </div>
                     <div class="col-6">
                        <div class="row pb-2">
                           <h4 class="col-md-5 pl-4 text-accent font-weight-bold sub-title">Contact Information</h4>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Phone</div>
                           <div id="account_phone" class="col-auto "></div>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Email</div>
                           <div id="account_email" class="col-auto "></div>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Address</div>
                           <div id="account_address" class="col-auto "></div>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Industry</div>
                           <div id="industry" class="col-auto"></div>
                        </div>
                        <div class="row pb-2">
                           <h4 class="col-md-5 pl-4 text-accent font-weight-bold sub-title">Work Information</h4>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Industry</div>
                           <div id="industry_2" class="col-auto"></div>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Job Title</div>
                           <div id="job_title" class="col-auto"></div>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Company</div>
                           <div id="company" class="col-auto"></div>
                        </div>
                        <div class="row pb-2">
                           <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Salary</div>
                           <div id="salary" class="col-auto"></div>
                        </div>
                     </div>
                     <div class="pt-5 text-center hidden-xs hidden-sm">
                        <a id="edit_link" class="btn bg-accent text-white pt-2 pb-2 pl-4 pr-4 merchant_edit">EDIT</a>
                     </div>


                  </div>
               </div>
               <div class="modal-footer hide">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary">Save</button>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="modal fade" style="overflow-y:auto;" id="market_tools_modal" tabindex="-1" role="dialog" aria-labelledy="modelTitleId" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg mx-auto" role="document">
         <div class="modal-content bg-accent">
            <div class="modal-content-wrapper">
               <div class="modal-header p-0 text-white bg-transparent border-0">
                  Push Notification
               </div>
               <div class="modal-body mt-5 p-5 bg-white" style="border-radius:30px!important;">
                  <ul class="nav align-items-center justify-content-end">

                     <li>
                        Select Virtual Passes:&nbsp;
                     </li>
                     <li>
                        <select id="modal_pass_selection">
                           <option>--Please Select--</option>
                           <?php foreach ($cards as $card) { ?>
                              <option value="<?= $card['id'] ?>"><?= $card['project_name'] ?></option>
                           <?php } ?>

                        </select>
                     </li>
                  </ul>
                  <div>
                     <table class="table" id="card_member_table">
                        <thead>
                           <th width="36px"><input type="checkbox" id="select_all"></th>
                           <th width="200">Member ID</th>
                           <th width="200">Name</th>
                        </thead>

                     </table>
                     <div style="overflow-y:scroll;max-height:60vh;">
                        <table class="table">
                           <thead class="invisible">
                              <th width="36px"></th>
                              <th width="200"></th>
                              <th width="200"></th>
                           </thead>
                           <tbody id="card_member_table_content">

                           </tbody>
                        </table>
                     </div>
                  </div>
                  <div class="pt-2 text-right">
                     <button id="message_trigger" class="btn btn-success">Next</button>
                  </div>
               </div>
            </div>
         </div>


      </div>
   </div>
   <div class="modal fade" id="sending_messaeg_modal" tabindex="-1" role="dialog" aria-labelledby="sending_messaeg_modal" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg- mx-auto" role="document">
         <div class="modal-content bg-accent">
            <div class="modal-content-wrapper">
               <div class="modal-header p-0 text-white bg-transparent border-0">
                  Sending Message
               </div>
               <div class="modal-body mt-5 p-5 bg-white" style="border-radius:30px!important;">

                  Message:
                  <textarea class="form-control" id="message_content" rows="3"></textarea>
                  <div class="text-right pt-3">
                     <button id="sending_message_button" class="btn btn-success">Submit</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>



   <script>
      var table, data = [];
      var selected_members = [];
      var base_url = "<?= base_url() ?>";
      $(document).ready(function() {
         table = $("#member").DataTable({
            "processing": false,
            "serverSide": true,
            "ajax": base_url + "merchant/smart_memberbase/listing",
            "columns": [{
                  "data": "id",
                  "name": "id"
               },
               {
                  "data": "pass_id",
                  "name": "Pass ID"
               },
               {
                  "data": "account_id",
                  "name": "Account ID"
               },
               {
                  "data": "name",
                  'name': "Name"
               },
               {
                  "data": "gender",
                  "name": "Gender"
               },
               {
                  "data": "phone",
                  "name": "Phone"
               },
               {
                  "data": "email",
                  "name": "Email"
               },
               {
                  "data": "address",
                  "name": "Address"
               },
               {
                  "data": "view",
                  "name": "View"
               },
               {
                  "data": "edit",
                  "name": "Edit"
               },

            ],
            "order": [0, "desc"],
            "columnDefs": [{
                  "targets": [0],
                  "visible": true,
                  "searchable": false,
                  "orderable": false
               },
               {
                  "targets": [2, 3, 5, 6, 7],
                  "visible": true,
                  "searchable": true,
                  "orderable": true
               },
               {
                  "targets": [4],
                  "visible": true,
                  "searchable": false,
                  "orderable": true
               },
               {
                  "targets": [8, 9],
                  "visible": true,
                  "searchable": false,
                  "orderable": false
               },
               {
                  "targets": [1],
                  "visible": false,
                  "searchable": true,
                  "orderable": true
               }
            ],
            "drawCallback": function(data) {
               $("#start").text(table.page.info().start + 1);
               $("#end").text(table.page.info().end);
               $("#counter").text(table.page.info().recordsDisplay);
               if (table.page.info().recordsDisplay == 0) {
                  $("#start").text("0");
               }
               init();

            },
            "initComplete": function(settings, json) {
               $('#member').removeClass('invisible');
               //init();

            },

         })

         function init() {
            $(".view").on("click", function(event) {
               event.preventDefault();
               $.ajax({
                  url: "/merchant/smart_memberbase/get_id/" + $(this).data('id'),
                  type: "GET",
                  dataType: "JSON",
                  error: function() {
                     alert("Something is wrong");
                  },
                  success: function(data) {
                     console.log(data);
                     $("#pass_id").empty();
                     $("#pass_id").text(data.id);
                     $("#account_id").empty();
                     $("#account_id").text(data.account_id);
                     $("#level").empty();
                     $("#level").text(data.user_level);
                     $("#points").empty();
                     $("#points").text(data.points);
                     $("#surname").empty();
                     $("#surname").text(data.surname);
                     $("#given_name").empty();
                     $("#given_name").text(data.given_name);
                     $("#english_name").empty();
                     $("#english_name").text(data.english_name);
                     $("#chinese_name").empty();
                     $("#chinese_name").text(data.chinese_name);
                     $("#account_gender").empty();
                     if (data.gender == 1) {
                        $("#account_gender").text("Male");
                     } else {
                        $("#account_gender").text("Female");
                     }
                     $("#birthday").empty();
                     $("#birthday").text(data.birthday);
                     $("#age").empty();
                     $("#age").text(data.age);
                     $("#nationality").empty();
                     $("#nationality").text(data.nationality);

                     $("#account_phone").empty();
                     $("#account_phone").text(data.phone);
                     $("#account_email").empty();
                     $("#account_email").text(data.email);
                     $("#account_address").empty();
                     $("#account_address").text(data.address);
                     $('#industry').empty();
                     $('#industry').text(data.industry);
                     $("#industry_2").empty();
                     $("#industry_2").text(data.industry_2);
                     $('#job_title').empty();
                     $("#job_title").text(data.job_title);
                     $("#company").empty();
                     $("#company").text(data.company);
                     $('#salary').empty();
                     $("#salary").text(data.salary);
                     $("#edit_link").attr("href", "/merchant/smart_memberbase/edit/" + data.id);
                     $('#viewModel').modal('show');
                  }
               })


            })
            $(".delete").on("click", function(event) {
               event.preventDefault();
               if (confirm("Are you sure to delete this record ?")) {

                  $.ajax({
                     url: "/merchant/smart_memberbase/delete/" + $(this).data("id"),
                     error: function() {},
                     success: function(data) {
                        location.reload();
                     }
                  })
               }

            })
         }



         $('[name="passes"]').on('keyup change', function() {
            table.columns(1).search(this.value).draw();
         });
         $('[name="general_search"]').on('keyup change', function() {
            table.search(this.value).draw();
         });
         $("#previous_page").on("click", function() {
            $('#member').DataTable().page('previous').draw('page');
         });
         $('#next_page').on("click", function() {
            $('#member').DataTable().page("next").draw('page');

         });


      })
   </script>

   <script>
      $("#btn_marketing_tools").on("click", function() {
         $("#market_tools_modal").modal("show");
      })
   </script>

   <script>
      $("#modal_pass_selection").on('change', function() {
         $.ajax({
            url: base_url + "merchant/smart_memberbase/listing_pass_member/" + $(this).val(),
            type: "GET",
            dataType: "JSON",
            error: function() {
               alert("Error");
            },
            success: function(data) {
               $("#card_member_table_content").empty();
               selected_members = [];
               data.forEach(element => {
                  $("#card_member_table_content").append(
                     '<tr><td><input type="checkbox" class="select_member" value="' + element.id + '"></td><td>' + element['account_id'] + '</td><td>' + element['name'] + '</td></tr>'
                  );

               });
               checkbox_init();
            }
         })
      })

      $("#select_all").on("change", function() {
         if ($(this).prop("checked")) {
            $(".select_member").prop("checked", true);
         } else {
            $(".select_member").prop("checked", false);
         }
      })

      function checkbox_init() {
         $(".select_member").on("change", function() {
            if ($(this).prop("checked")) {
               selected_members.push($(this).val())
            } else {

            }
         })
      }

      $("#message_trigger").on("click", function() {
         if (selected_members.length != 0) {
            $("#market_tools_modal").modal("hide");
            $("#sending_messaeg_modal").modal("show");
         } else {
            alert("Please selected at least one member");
         }

      })

      $("#sending_message_button").on("click", function() {
         $.ajax({
            url: base_url + "/merchant/smart_memberbase/adding_message",
            data: {
               ids: selected_members,
               message: $("#message_content").val(),

            },
            type: "POST",
            error: function() {
               alert("Error");
            },
            success: function() {
               console.log("Mission completed !!!");
            }


         });
      })
   </script>