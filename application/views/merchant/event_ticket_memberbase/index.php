<div class="page-body">
    <div class="row pl-0 pl-xl-5">
        <div class="col-12">
            <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-5 pb-5"><?= $page ?></h4>
            <div class="page-content-section p-3 p-sm-5">
                <div class="position-relative">
                    <div id="member_database_filter" class="d-flex flex-column w-100">
                        <div class="col-auto">

                        </div>
                        <div class="dataTables_info " id="ticket_info" role="status" aria-live="polite">

                        </div>
                        <div class="top">
                            <div class="dataTables_info pb-2" id="merchant_info" role="status" aria-live="polite">Showing <span id="start"></span> to <span id="end"></span> of <span id="counter"></span> entries</div>
                        </div>
                        <div class="col-auto ">
                            <div class="row align-items-center justify-content-end">
                                <div class="d-inline-block mb-2 mr-auto">
                                    <span class="mr-2">Displaying Virtual Pass:</span>
                                    <select id="selected_ticket_id" style="max-width:50vw;overflow-x:hidden;">
                                        <?php foreach ($tickets as $ticket) { ?>
                                            <option value="<?= $ticket['id'] ?>"><?= $ticket["event_name"] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <a href="/merchant/event_ticket_memberbase/add" class="btn mb-2 mr-4" style="width:15rem;">Add User</a>
                                <a id="export_button" href="#" class="btn mb-2 mr-4" style="width:15rem;">Export Database</a>

                                <div class="d-inline-block">
                                    <div class="input-group input-group-search mb-2 pr-2">
                                        <input type="text" class="fa" id="general_search" name="general_search" placeholder="Search">
                                        <div class="input-group-append"> <span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="scroller">
                        <table id="myTable" border="0" class="w-100 dataTable border-0 pt-0 pt-md-5">
                            <thead>
                                <tr>
                                    <th>Event Ticket Seq</th>
                                    <th>Display Code</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Attendace</th>
                                    <th>QR</th>
                                    <th>Invite</th>
                                    <th>Detail</th>
                                    <th>Void</th>
                                </tr>
                            </thead>
                        </table>
                        <div class="text-center">
                            <button id="previous_page" class="btn mr-4" style="width:9rem;">Previous</button>
                            <button id="next_page" class="btn" style="width:9rem;">Next</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="QR_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg mx-auto">
        <div class="modal-content bg-accent">
            <div class="modal-content-wrapper">
                <div class="modal-header p-0 text-white bg-transparent border-0">
                    QR
                </div>
                <div class="modal-body mt-5 p-5 bg-white" style="border-radius:30px!important;">
                    <div class="row">
                        <div class="col-6 d-flex justifiy-content-center flex-column">
                            <h4 class="text-center">Apple</h4>
                            <img class="img-fluid" id="apple_qr" src="">
                        </div>
                        <div class="col-6 d-flex justifiy-content-center flex-column">
                            <h4 class="text-center">Google</h4>
                            <img class="img-fluid" id="google_qr" src="">
                        </div>
                        <div class="col-12 d-flex justify-content-center">
                            <button id="QR_modal_close" class="btn btn-default font-weight-bold" style="width:6rem;">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="detail_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg mx-auto">
        <div class="modal-content bg-accent">
            <div class="modal-content-wrapper">
                <div class="modal-header p-0 text-white bg-transparent border-0">
                    Detail
                </div>
                <div class="modal-body mt-5 p-5 bg-white" style="border-radius:30px!important;">
                    <div class="row">
                        <div class="col-6">
                            <h4>Event Ticket Info</h4>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Display Code</div>
                                <div id="display_code" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Seat</div>
                                <div id="seat" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Row</div>
                                <div id="row" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Section</div>
                                <div id="section" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Status</div>
                                <div id="status" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Enter time</div>
                                <div id="enter_time" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Quit time</div>
                                <div id="quit_time" class="col-auto"></div>
                            </div>
                            <h4 class="mt-4">Account Info</h4>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Name</div>
                                <div id="name" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Surname</div>
                                <div id="surname" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Given name</div>
                                <div id="given_name" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-noraml sub-title">English name</div>
                                <div id="english_name" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Chinese name</div>
                                <div id="chinese_name" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Gender</div>
                                <div id="gender" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Age</div>
                                <div id="age" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Nationality</div>
                                <div id="nationality" class="col-auto"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <h4>Contact Info</h4>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Phone</div>
                                <div id="phone" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Email</div>
                                <div id="email" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Address</div>
                                <div id="address" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Industry</div>
                                <div id="industry" class="col-auto"></div>
                            </div>
                            <h4 class="mt-4">Work Info</h4>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Industry</div>
                                <div id="industry_2" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Job title</div>
                                <div id="job_title" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Company</div>
                                <div id="company" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Salary</div>
                                <div id="salary" class="col-auto"></div>
                            </div>
                        </div>
                        <div class="col-12 d-flex justify-content-center">
                            <a id="detail_page_hyperlink" href="#" class="btn btn-success font-weight-bold text-white mr-4" style="width:6rem;">Edit</a>
                            <button id="close_button" class="btn btn-default font-weight-bold text-black" style="width:6rem;">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var table, data = [];
    var base_url = "<?= base_url() ?>";
    var selected_members = [];
    $(document).ready(function() {
        let selected = $("#selected_ticket_id").val();
        $("#export_button").attr("href", "/merchant/event_ticket_memberbase/export_csv?ticket_id=" + selected);
        table = $("#myTable").DataTable({
            "processing": false,
            "serverSide": true,
            "ajax": base_url + "merchant/event_ticket_memberbase/listing" + "?ticket_id=" + selected,
            "columns": [{
                "data": "event_code_series",
                "name": "event_code_series"
            }, {
                "data": "display_code",
                "name": "display_code"
            }, {
                "data": "name",
                "name": "name"
            }, {
                "data": "phone",
                "name": "phone"
            }, {
                "data": "email",
                "name": "email"
            },  {
                "data": "status",
                "name": "status"
            },{
                "data": "attendance",
                "name": "attendance"
            }, {
                "data": "qr_code",
                "name": "qr_code"
            }, {
                "data": "invite",
                "name": "invite"
            }, {
                "data": "view",
                "name": "view"
            }, {
                "data": "void",
                "name": "void"
            }],
            "order": [0, "desc"],
            "columnDefs": [{
                "targets": [0],
                "visible": true,
                "searchable": false,
                "orderable": true
            }, {
                "targets": [ 7,8, 9,10],
                "visible": true,
                "searchable": true,
                "orderable": false
            }, {
                "targets": [1,2, 3, 4, 5, 6],
                "visible": true,
                "searchable": false,
                "orderable": true
            }],
            "drawCallback": function() {
                button_init();
                $("#start").text(table.page.info().start + 1);
                $("#end").text(table.page.info().end);
                $("#counter").text(table.page.info().recordsDisplay);
                if (table.page.info().recordsDisplay == 0) {
                    $("#start").text("0");
                }
            }
        })
    })

    $("#selected_ticket_id").on("change", function() {
        $("#myTable").DataTable().ajax.url(base_url + "merchant/event_ticket_memberbase/listing" + "?ticket_id=" + $(this).val());
        $("#myTable").DataTable().draw("page");
        $("#export_button").attr("href", "/merchant/event_ticket_memberbase/export_csv?ticket_id=" + $(this).val());
    })

    $("#previous_page").on("click", function() {
        $("#myTable").DataTable().page("previous").draw("page");
    })

    $("#next_page").on("click", function() {
        $("#myTable").DataTable().page("next").draw("page");
    })

    $("#general_search").on("input", function() {
        $("#myTable").DataTable().columns(0).search($(this).val()).draw();

    })





    function button_init() {
        $(".detail_button").on("click", function() {
            $.ajax({
                url: base_url + "merchant/event_ticket_memberbase/detail",
                data: {
                    "id": $(this).data("id")
                },
                dataType: "json",
                success: function(data) {
                    modal_init(data);
                }
            })
        })
        $(".void_button").on("click", function() {
            if (confirm("Are you sure to void this event ticket?")) {
                $.ajax({
                    url: base_url + "merchant/event_ticket_memberbase/void",
                    method: "post",
                    data: {
                        "id": $(this).data("id")
                    },
                    dataType: "json",
                    success: function() {
                        location.reload();
                    }
                })
            }
        })
        $(".qr_button").on("click", function() {
            $.ajax({
                url: base_url + "merchant/event_ticket_memberbase/simple_get",
                method: "post",
                data: {
                    'id': $(this).data('id')
                },
                dataType: "json",
                success: function(data) {
                    $("#apple_qr").attr("src", base_url + data.apple_qr);
                    $("#google_qr").attr("src", base_url + data.google_qr);
                    $("#QR_modal").modal("show");
                }

            })
        });

        $(".email_button").on("click", function() {
            if (confirm("Are you sure to send the invitation to this user?")) {
                $.ajax({
                    url: base_url + "merchant/event_ticket_memberbase/quick_mail",
                    method: "post",
                    data: {
                        "id": $(this).data('id')
                    },
                    dataType: "json",
                    success: function() {
                        alert("Email Sent");
                    }
                })
            }
        })
    }

    function modal_init(data) {
        $("#display_code").empty();
        $("#display_code").text(data.display_code);
        $("#seat").empty();
        $("#seat").text(data.seat);
        $("#row").empty();
        $("#row").text(data.row);
        $("#section").empty();
        $("#section").text(data.section);
        $("#status").empty();
        $("#status").text(data.status);
        $("#enter_time").empty();
        $("#enter_time").text(data.enter_time);
        $("#quit_time").empty();
        $("#quit_time").text(data.quit_time);
        $("#name").empty();
        $("#name").text(data.name);
        $("#surname").empty();
        $("#surname").text(data.surname);
        $("#given_name").empty();
        $("#given_name").text(data.given_name);
        $("#english_name").empty();
        $("#english_name").text(data.english_name);
        $("#chinese_name").empty();
        $("#chinese_name").text(data.chinese_name);
        $("#gender").empty();
        if (data.gender == 1) {
            $("#gender").text("Male");
        } else {
            $("#gender").text("Female");
        }
        $("#age").empty();
        $("#age").text(data.age);
        $("#nationality").empty();
        $("#nationality").text(data.nationality);
        $("#phone").empty();
        $("#phone").text(data.phone);
        $("#email").empty();
        $("#email").text(data.email);
        $("#address").empty();
        $("#address").text(data.address);
        $("#industry").empty();
        $("#industry").text(data.industry);
        $("#industry_2").empty();
        $("#industry_2").text(data.industry_2);
        $("#job_title").empty();
        $("#job_title").text(data.job_title);
        $("#company").empty();
        $("#company").text(data.company);
        $("#salary").empty();
        $("#salary").text(data.salary);
        $("#detail_page_hyperlink").attr("href", "/merchant/event_ticket_memberbase/edit/" + data.id);
        $("#detail_modal").modal("show");
    }

    $("#close_button").on("click", function() {
        $("#detail_modal").modal("hide");
    })

    $("#QR_modal_close").on("click", function() {
        $("#QR_modal").modal("hide");
    })
</script>