<div class="page-body">
    <div class="col-12 col-md-6">
        <div class="title pt-4 pb-5 font-weight-bold">Event Ticket User Add</div>
        <div class="form-group d-flex align-items-center">
            <div>Import from Memberbase</div>
            <button id="import_button" class="btn btn-default ml-4">Search</button>
        </div>

        <form action="/merchant/event_ticket_memberbase/insert" method="post">
            <div class="form-group d-flex align-items-center">
                <div class="pr-4 text-nowrap">Event Ticket Info</div>
                <hr class="w-100">
            </div>
            <div class="form-group">
                <label>Event name<i class="ml-4 fa fa-info-circle tips" aria-hidden="true"><span class="tips_content">API membership card is not supported.</span></i></label>
                <select class="form-control" name="event_ticket_id">
                    <?php foreach ($tickets as $ticket) { ?>
                        <option value="<?= $ticket['id'] ?>"><?= $ticket['management_name'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Seat</label>
                <input type="text" class="form-control" id="form_seat" name="seat" placeholder="Please enter the seat information">
            </div>
            <div class="form-group">
                <label>Row</label>
                <input type="text" class="form-control" id="form_row" name="row" placeholder="Please enter the row information">
            </div>
            <div class="form-group">
                <label>Section</label>
                <input type="text" class="form-control" id="form_section" name="section" placeholder="Please enter the section information">
            </div>
            <div class="form-group d-flex align-items-center">
                <div class="pr-4 text-nowrap">User Info</div>
                <hr class="w-100">
            </div>
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" id="form_name" name="name" placeholder="Please enter the name">
            </div>
            <div class="form-group">
                <label>Surname</label>
                <input type="text" class="form-control" id="form_surname" name="surname" placeholder="Please enter the surname">
            </div>
            <div class="form-group">
                <label>Given name</label>
                <input type="text" class="form-control" id="form_given_name" name="given_name" placeholder="Please enter the given name">
            </div>
            <div class="form-group">
                <label>English name</label>
                <input type="text" class="form-control" id="form_english_name" name="english_name" placeholder="Please enter the english name">
            </div>
            <div class="form-group">
                <label>Chinese name</label>
                <input type="text" class="form-control" id="form_chinese_name" name="chinese_name" placeholder="Please enter the chinese name">
            </div>
            <div class="form-group">
                <label>Gender</label>
                <select class="form-control" name="gender" id="form_gender">
                    <option value="1">Male</option>
                    <option value="0">Female</option>
                </select>
            </div>
            <div class="form-group">
                <label>Birthday</label>
                <input type="date" class="form-control" id="form_birthday" name="birthday" placeholder="Please enter the birthday">
            </div>
            <div class="form-group">
                <label>Age</label>
                <input type="text" class="form-control" id="form_age" name="age" placeholder="Please enter the age">
            </div>
            <div class="form-group">
                <label>Nationality</label>
                <input type="text" class="form-control" id="form_nationality" name="nationality" placeholder="Please enter the nationality">
            </div>
            <div class="d-flex align-items-center">
                <div class="pr-4 text-nowrap">Contact Info</div>
                <hr class="w-100">
            </div>
            <div class="form-group">
                <label>Phone</label>
                <input type="text" class="form-control" id="form_phone" name="phone" placeholder="Please enter the phone">
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" id="form_email" name="email" placeholder="Pleaes enter the email">
            </div>
            <div class="form-group">
                <label>Address</label>
                <input type="text" class="form-control" id="form_address" name="address" placeholder="Please enter the address">
            </div>
            <div class="form-group">
                <label>Industry</label>
                <input type="text" class="form-control" id="form_industry" name="industry" placeholder="Please enter the industry">
            </div>
            <div class="d-flex align-items-center">
                <div class="pr-4 text-nowrap">Work Info</div>
                <hr class="w-100">
            </div>
            <div class="form-group">
                <label>Industry</label>
                <input type="text" class="form-control" id="form_industry_2" name="industry_2" placeholder="Please enter the industry">
            </div>
            <div class="form-group">
                <label>Job title</label>
                <input type="text" class="form-control" id="form_job_title" name="job_title" placeholder="Pleaes enter the job title">
            </div>
            <div class="form-group">
                <label>Company</label>
                <input type="text" class="form-control" id="form_company" name="company" placeholder="Please enter the company">
            </div>
            <div class="form-group">
                <label>Salary</label>
                <input type="text" class="form-control" id="form_salary" name="salary" placeholder="Please enter the salary">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success font-weight-bold text-white">Submit</button>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="search_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg mx-auto">
        <div class="modal-content bg-accent">
            <div class="modal-content-wrapper">
                <div class="modal-header p-0 text-white bg-transparent border-0">
                    Searching
                </div>
                <div class="modal-body mt-5 p-5 bg-white" style="border-radius:30px!important;">
                    <div class="d-flex">
                        <div class="col">
                            <input type="type" class="form-control" id="general_search" style="height:32px!important;" placeholder="General Searching">
                        </div>
                        <div class="col">
                            <select class="form-control" id="selected_membership_program" style="height:32px!important;">
                                <?php foreach ($membership_program as $program) { ?>
                                    <option value="<?= $program['id'] ?>"><?= $program['project_name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <button id="search_button" class="btn btn_search">Search</button>
                    </div>
                    <div id="searching_result_zone" class="mt-4" style="overflow-x:scroll;overflow-y:visible;padding-left:15px;">
                        <div id="scrolling_zone">
                            <div id="padding_zone">
                                <table id="searching_result_table" class="table table-bordered">

                                    <thead id="header_height">
                                        <tr>
                                            <th style="min-width:8rem;min-height:1rem;"></th>
                                            <th class="col-auto">name</th>
                                            <th class="col-auto">surname</th>
                                            <th class="col-auto">given name</th>
                                            <th class="col-auto">english name</th>
                                            <th class="col-auto">chinese name</th>
                                            <th class="col-auto">gender</th>
                                            <th class="col-auto">birthday</th>
                                            <th class="col-auto">age</th>
                                            <th class="col-auto">nationality</th>
                                            <th class="col-auto">phone</th>
                                            <th class="col-auto">email</th>
                                            <th class="col-auto">address</th>
                                            <th class="col-auto">industry</th>
                                            <th class="col-auto">industry</th>
                                            <th class="col-auto">job title</th>
                                            <th class="col-auto">company</th>
                                            <th class="col-auto">salary</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody" class="">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script>
    var base_url = "<?= base_url() ?>";
    var header_height_value = $("#header_height").height();
    $("#import_button").on("click", function() {
        $("#search_modal").modal("show");
        header_height_value = $("#header_height").height();

    })

    $("#search_button").on("click", function() {
        $.ajax({
            url: base_url + "merchant/virtual_passes/fetch_member",
            method: "post",
            data: {
                "id": $("#selected_membership_program").val(),
                "keyword": $("#general_search").val()
            },
            dataType: "json",
            success: function(data) {
                console.log(data);
                init_search_result(data);
            }
        })
    })


    $("#searching_result_zone").scroll(function() {
        console.log("Trigger:" + $(this).scrollTop());
        if ($(this).scrollTop() > $("#header_height").height()) {
            $("#header_height").addClass("fixed_header");
            $("#tbody").addClass("special_padding");
        } else {
            $("#header_height").removeClass("fixed_header");
            $("#tbody").removeClass("special_padding");
        }

    })

    function init_search_result(data) {
        $("#tbody").empty();
        for (var i in data) {
            if (data[i].gender == 1) {
                var gender = "Male";
            } else {
                var gender = "Female";
            }
            let insert_string = '<tr><th><a class="btn selected_button" data-id="' + data[i].id + '">Select</th><td>' + data[i].name + '</td><td>' + data[i].surname + '</td><td>' + data[i].given_name + '</td><td>' + data[i].english_name + '</td><td>' + data[i].chinese_name + '</td><td>' + gender + '</td><td>' + data[i].birthday + '</td><td>' + data[i].age + '</td><td>' + data[i].nationality + '</td><td>' + data[i].phone + '</td><td>' + data[i].email + '</td><td>' + data[i].address + '</td><td>' + data[i].industry + '</td><td>' + data[i].industry_2 + '</td><td>' + data[i].job_title + '</td><td>' + data[i].company + '</td><td>' + data[i].salary + '</td></tr>';
            $("#tbody").append(insert_string);
            init_select_button();
        }
    }

    function init_select_button() {
        $(".selected_button").on("click", function() {
            $.ajax({
                url: base_url + "merchant/virtual_passes/select_user",
                method: "post",
                data: {
                    "id": $(this).data("id"),
                },
                dataType: "json",
                success: function(data) {
                    init_form_with_data(data);
                    $("#search_modal").modal("hide");
                }
            })
        })

    }

    function init_form_with_data(data) {
        $("#form_name").empty();
        $("#form_name").val(data.name);
        $("#form_surname").empty();
        $("#form_surname").val(data.surname);
        $("#form_given_name").empty();
        $("#form_given_name").val(data.given_name);
        $('#form_english_name').empty();
        $("#form_english_name").val(data.english_name);
        $("#form_chinese_name").empty();
        $("#form_chinese_name").val(data.chinese_name);

        $("#form_gender").empty();
        if (data.gender == 1) {
            $("#form_gender").append("<option selected value='1'>Male</option>");
            $("#form_gender").append("<option  value='0'>Female</option>");
        } else {
            $("#form_gender").append("<option  value='1'>Male</option>");
            $("#form_gender").append("<option selected value='0'>Female</option>");
        }
        $("#form_birthday").empty();
        $("#form_birthday").val(data.birthday);
        $("#form_age").empty();
        $("#form_age").val(data.age);
        $("#form_nationality").empty();
        $("#form_nationality").val(data.nationality);
        $("#form_phone").empty();
        $("#form_phone").val(data.phone);
        $("#form_email").empty();
        $("#form_email").val(data.email);
        $("#form_address").empty();
        $("#form_address").val(data.address);
        $("#form_industry").empty();
        $("#form_industry").val(data.industry);
        $('#form_industry_2').empty();
        $("#form_industry_2").val(data.industry_2);
        $("#form_job_title").empty();
        $("#form_job_title").val(data.job_title);
        $("#form_company").empty();
        $("#form_company").val(data.company);
        $("#form_salary").empty();
        $("#form_salary").val(data.salary);

    }
</script>