<div class="page-body">
    <div class="col-12 col-md-6">
        <div class="title pt-4 pb-5 font-weight-bold">Event Ticket Info Update</div>
        <form method="post" action="/merchant/event_ticket_memberbase/update">
            <div class="form-group d-flex align-items-center">
                <div class="pr-2 text-nowrap">Event Ticket Info</div>
                <hr class="w-100">
            </div>
            <div class="form-group">
                <label>Display Code</label>
                <input readonly class="form-control" value="<?= $holder_info["display_code"] ?>">
            </div>
            <div class="form-group">
                <label>Seat</label>
                <input type="text" class="form-control" id="seat" name="seat" value="<?= $holder_info['seat'] ?>">
            </div>
            <div class="form-group">
                <label>Row</label>
                <input type="text" class="form-control" id="row" name="row" value="<?= $holder_info['row'] ?>">
            </div>
            <div class="form-group">
                <label>Section</label>
                <input type="text" class="form-control" id="section" name="section" value="<?= $holder_info['section'] ?>">
            </div>
            <div class="form-group">
                <label>Status</label>
                <select class="form-control" id="status" name="status">
                    <option <?= $holder_info['status'] == "valid" ? "selected" : "" ?> value="valid">valid</option>
                    <option <?= $holder_info['status'] == "void" ? "selected" : "" ?> value="void">void</option>
                </select>
            </div>
            <div class="form-group">
                <label>Enter time</label>
                <input type="text" class="form-control" id="enter_time" value="<?= $holder_info['enter_time'] ?>" readonly>
            </div>
            <div class="form-group">
                <label>Quit time</label>
                <input type="text" class="form-control" id="quit_time" value="<?= $holder_info['quit_time'] ?>" readonly>
            </div>
            <div class="form-group d-flex align-items-center">
                <div class="pr-2 text-nowrap">User Info</div>
                <hr class="w-100">
            </div>
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" id="name" name="name" value="<?= $holder_info['name'] ?>">
            </div>
            <div class="form-group">
                <label>Surname</label>
                <input type="text" class="form-control" id="surname" name="surname" value="<?= $holder_info['surname'] ?>">
            </div>
            <div class="form-group">
                <label>Given name</label>
                <input type="text" class="form-control" id="given_name" name="given_name" value="<?= $holder_info['given_name'] ?>">
            </div>
            <div class="form-group">
                <label>English name</label>
                <input type="text" class="form-control" id="english_name" name="english_name" value="<?= $holder_info['english_name'] ?>">
            </div>
            <div class="form-group">
                <label>Chinese name</label>
                <input type="text" class="form-control" id="chinese_name" name="chinese_name" value="<?= $holder_info['chinese_name'] ?>">
            </div>
            <div class="form-group">
                <label>Gender</label>
                <select class="form-control" id="gender" name="gender">
                    <option <?= $holder_info['gender'] == 1 ? "selected" : "" ?> value="1">Male</option>
                    <option <?= $holder_info['gender'] == 0 ? "selected" : "" ?> value="0">Female</option>
                </select>
            </div>
            <div class="form-group">
                <label>Birthday</label>
                <input type="date" class="form-control" id="birthday" name="birthday" value="<?= $holder_info['birthday'] ?>">
            </div>
            <div class="form-group">
                <label>Age</label>
                <input type="number" class="form-control" id="age" name="age" value="<?= $holder_info['age'] ?>">
            </div>
            <div class="form-group">
                <label>Nationality</label>
                <input type="text" class="form-control" id="nationality" name="" value="<?= $holder_info['nationality'] ?>">
            </div>
            <div class="form-group d-flex align-items-center">
                <div class="pr-2 text-nowrap">Contact Info</div>
                <hr class="w-100">
            </div>
            <div class="form-group">
                <label>Phone</label>
                <input type="text" class="form-control" id="phone" name="phone" value="<?= $holder_info['phone'] ?>">
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" id="email" name="email" value="<?= $holder_info['email'] ?>">
            </div>
            <div class="form-group">
                <label>Address</label>
                <input type="text" class="form-control" id="address" name="address" value="<?= $holder_info['address'] ?>">
            </div>
            <div class="form-group">
                <label>Industry</label>
                <input type="text" class="form-control" id="industry" name="industry" value="<?= $holder_info['industry'] ?>">
            </div>
            <div class="form-group d-flex align-items-center">
                <div class="pr-2 text-nowrap">Work Info</div>
                <hr class="w-100">
            </div>
            <div class="form-group">
                <label>Industry</label>
                <input type="text" class="form-control" id="industry_2" name="industry_2" value="<?= $holder_info['industry_2'] ?>">
            </div>
            <div class="form-group">
                <label>Job title</label>
                <input type="text" class="form-control" id="job_title" name="job_title" value="<?= $holder_info['job_title'] ?>">
            </div>
            <div class="form-group">
                <label>Company</label>
                <input type="text" class="form-control" id="company" name="company" value="<?= $holder_info['company'] ?>">
            </div>
            <div class="form-group">
                <label>Salary</label>
                <input type="text" class="form-control" id="salary" name="salary" value="<?= $holder_info["salary"] ?>">
            </div>
            <div class="form-group d-flex align-items-center">
                <div class="pr-4 text-nowrap">QR Code</div>
                <hr class="w-100">
            </div>
            <div class="form-group">
                <button id="show_qr" href="#qr_code" data-toggle="collapse" class="btn btn-default">Show</button>
            </div>
            <div id="qr_code" class="form-group collapse">
                <div class="d-flex align-items-center">
                    <div class="col-6 text-center">
                        Apple
                        <img class="img-fluid w-100" src="<?= base_url('assets/user_qr/event_ticket/' . $holder_info['id'] . "/apple.png") ?>">
                    </div>
                    <div class="col-6 text-center">
                        Google
                        <img class="img-fluid w-100" src="<?= base_url('assets/user_qr/event_ticket/' . $holder_info['id'] . "/google.png") ?>">
                    </div>
                </div>
            </div>
            <div class="form-group d-flex align-items-center">
                <div class="pr-2 text-nowrap">Control</div>
                <hr class="w-100">
            </div>
            <div class="form-group">
                <input type="hidden" name="id" value="<?= $holder_info["id"] ?>">
                <button type="submit" class="btn btn-success text-white font-weight-bold">Submit</button>
                <?php if ($holder_info['email'] !== "") { ?>
                    <a href="/merchant/event_ticket_memberbase/send_email/<?= $holder_info['id'] ?>" class="btn btn-default">Send Invitation</a>
                <?php } ?>

            </div>

        </form>
    </div>

</div>

<script>
    $("#show_qr").on("click", function(event) {
        event.preventDefault();

    })
</script>