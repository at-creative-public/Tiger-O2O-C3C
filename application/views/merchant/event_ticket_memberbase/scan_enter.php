<div class="page-body">
    <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5"><?= $page ?></h4>
    <div class="row">
        <div class="col-12">
            <div class="editor">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item first-block">
                        Control
                    </li>

                    <li class="nav-item">
                        <a class="nav-link active" href="/merchant/event_ticket_memberbase/scan_enter">Enter</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/merchant/event_ticket_memberbase/scan_quit">Quit</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/merchant/event_ticket_memberbase/scan_code_enter">Code-Enter</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/merchant/event_ticket_memberbase/scan_code_quit">Code-Quit</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="inner_padding">
                    <h4 class="text-danger font-weight-bold">*Current function is not available for api passes</h4>
                        <div id="reader"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="searching_result_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg mx-auto" role="document">
        <div class="modal-content bg-accent">
            <div class="modal-content-wrapper">
                <div class="modal-header p-0 text-white bg-transparent border-0">
                    Search Result
                </div>
                <div class="modal-body mt-5 p-5 bg-white" style="border-radius:30px!important;">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <h4 class="">Event Info</h4>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Event name</div>
                                <div id="event_name" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Event Date</div>
                                <div id="event_date" class="col-auto"></div>
                            </div>
                            <h4 class="mt-4">Ticket Info</h4>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Display code</div>
                                <div id="display_code" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Seat</div>
                                <div id="seat" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Row</div>
                                <div id="row" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Section</div>
                                <div id="section" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Status</div>
                                <div id="status" class="col-auto"></div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <h4>Account Info</h4>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Surname</div>
                                <div id="surname" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Given name</div>
                                <div id="given_name" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">English name</div>
                                <div id="english_name" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Chinese name</div>
                                <div id="chinese_name" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Gender</div>
                                <div id="gender" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">birthday</div>
                                <div id="birthday" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">age</div>
                                <div id="age" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Nationality</div>
                                <div id="nationality" class="col-auto"></div>
                            </div>
                        </div>
                        <div class="col-12 mt-4 d-flex justify-content-center align-items-center">
                            <button id="close_button" class="btn btn-default font-weight-bold">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?= base_url("assets/js/html5-qrcode.min.js") ?>" type="text/javascript"></script>
<script>
    const base_url = "<?= base_url() ?>";
    const html5QrCode = new Html5Qrcode("reader");
    const config = {
        fps: 60,
        qrbox: {
            width: 250,
            height: 250
        }
    };

    const facingMode = "environment";

    function start_camera() {
        html5QrCode.start({
            facingMode: "environment"
        }, config, qrCodeSuccessCallback);
    }

    const qrCodeSuccessCallback = (decodedText, decodedResult) => {
        console.log(decodedText);
        html5QrCode.stop();
        console.log("Can this run?");
        $.ajax({
            "url": "/merchant/event_ticket_memberbase/scanning",
            "data": {
                "code": decodedText,
                "mode": "enter"
            },
            "dataType": "json",
            "method": "post",
            "success": function(data) {
                if (data.found == "false") {
                    alert("No found");
                    setInterval(
                        start_camera(), 3000);

                } else {
                    
                    if(data.repeat != "true"){
                    console.log(data.result);
                    modal_init(data.result, data.event_info);
                    $("#searching_result_modal").modal("show");
                    }else{
                        alert("This user already enter the venue");
                        setInterval(
                        start_camera(), 3000);
                    }
                    

                }
            }

        })


    }

    $("#close_button").on("click", function() {
        $("#searching_result_modal").modal("hide");
    })

    $("#searching_result_modal").on("hidden.bs.modal", function() {
        start_camera();
    })

    function modal_init(data, event) {
        console.log(data);
        $("#event_name").empty();
        $("#event_name").text(event.event_name);
        $("#event_date").empty();
        $("#event_date").text(event.event_start_time + "-" + event.event_end_time + " " + event.event_date);
        $("#display_code").empty();
        $('#display_code').text(data.display_code);
        $("#seat").empty();
        $("#seat").text(data.seat);
        $("#row").empty();
        $("#row").text(data.row);
        $("#section").empty();
        $("#section").text(data.section);
        $("#status").empty();
        $("#status").text(data.status);
        $("#surname").empty();
        $("#surname").text(data.surname);
        $("#given_name").empty();
        $("#given_name").text(data.given_name);
        $("#english_name").empty();
        $("#english_name").text(data.english_name);
        $("#chinese_name").empty();
        $("#chinese_name").text(data.chinese_name);
        $("#gender").empty();
        if (data.gender == 1) {
            $("#gender").text("Male");
        } else {
            $("#gender").text("Female");
        }
        $('#birthday').empty();
        $("#birthday").text(data.birthday);
        $("#age").empty();
        $("#age").text(data.age);
        $("#nationality").empty();
        $("#nationality").text(data.nationality);

    }

    $(document).ready(function() {


        // If you want to prefer front camera
        html5QrCode.start({
            facingMode: "environment"
        }, config, qrCodeSuccessCallback);

        // If you want to prefer back camera
        html5QrCode.start({
            facingMode: "environment"
        }, config, qrCodeSuccessCallback);

        // Select front camera or fail with `OverconstrainedError`.
        html5QrCode.start({
            facingMode: {
                exact: "environment"
            }
        }, config, qrCodeSuccessCallback);

        // Select back camera or fail with `OverconstrainedError`.
        html5QrCode.start({
            facingMode: {
                exact: "environment"
            }
        }, config, qrCodeSuccessCallback);
    })
</script>