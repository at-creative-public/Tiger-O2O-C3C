<div class="page-body">
    <div class="col-12 col-md-6">
        <div class="title pt-4 pb-5 font-weight-bold">Event Ticket Info</div>
        <div class="form-group d-flex align-items-center">
            <div class="pr-2 text-nowrap">Event Ticket Info</div>
            <hr class="w-100">
        </div>
        <div class="form-group">
            <label class="">Ref ID</label>
            <input class="form-control" value="<?= $holder_info["ref_id"] ?>" readonly>
        </div>
        <div class="form-group">
            <label class="">Seat</label>
            <input type="text" class="form-control" value="<?= $holder_info['seat'] ?>" readonly>
        </div>
        <div class="form-group">
            <label class="">Row</label>
            <input type="text" class="form-control" value="<?= $holder_info['row'] ?>" readonly>
        </div>
        <div class="form-group">
            <label class="">Section</label>
            <input type="text" class="form-control" value="<?= $holder_info['section'] ?>" readonly>
        </div>
        <div class="form-group">
            <label class="">Status</label>
            <select class="form-control" id="status" disabled>
                <option <?=$holder_info['status'] == "valid" ? "selected":""?> value="valid">Valid</option>
                <option <?=$holder_info['status'] == "void" ? "selected":""?> value="void">Void</option>
            </select>
        </div>
        <div class="form-group">
            <div class="pr-2 text-nowrap">
                User Info
            </div>
            <hr class="w-100">
        </div>
        <div class="form-group">
            <label class="">Name</label>
            <input type="text" class="form-control" id="name" value="<?= $holder_info['name'] ?>" readonly>
        </div>
        <div class="form-group">
            <label class="">Surname</label>
            <input type="text" class="form-control" id="surname" value="<?= $holder_info['surname'] ?>" readonly>
        </div>
        <div class="form-group">
            <label class="">Given name</label>
            <input type="text" class="form-control" id="given_name" value="<?= $holder_info['given_name'] ?>" readonly>
        </div>
        <div class="form-group">
            <label class="">English name</label>
            <input type="text" class="form-control" id="english_name" value="<?= $holder_info['english_name'] ?>" readonly>
        </div>
        <div class="form-group">
            <label class="">Chinese name</label>
            <input type="text" class="form-control" id="chinese_name" value="<?= $holder_info['chinese_name'] ?>" readonly>
        </div>
        <div class="form-group">
            <label class="">Gender</label>
            <select class="form-control" id="gender" readonly disabled>
                <option <?= $holder_info['gender'] == 1 ? "selected" : "" ?> value="1">Male</option>
                <option <?= $holder_info['gender'] == 0 ? "selected" : "" ?> value="0">Female</option>
            </select>
        </div>
        <div class="form-group">
            <label class="">Birthday</label>
            <input type="date" class="form-control" id="birthday" value="<?= $holder_info['birthday'] ?>" readonly>
        </div>
        <div class="form-group">
            <label class="">Age</label>
            <input type="text" class="form-control" id="age" value="<?= $holder_info['age'] ?>" readonly>
        </div>
        <div class="form-group">
            <label class="">Nationality</label>
            <input type="text" class="form-control" id="nationality" value="<?= $holder_info['nationality'] ?>" readonly>
        </div>
        <div class="form-group d-flex align-items-center">
            <div class="pr-2 text-nowrap">Contact Info</div>
            <hr class="w-100">
        </div>
        <div class="form-group">
            <label class="">Phone</label>
            <input type="text" class="form-control" id="phone" value="<?= $holder_info['phone'] ?>" readonly>
        </div>
        <div class="form-group">
            <label class="">Email</label>
            <input type="email" class="form-control" id="email" value="<?= $holder_info['email'] ?>" readonly>
        </div>
        <div class="form-group">
            <label class="">Address</label>
            <input type="text" class="form-control" id="address" value="<?= $holder_info['address'] ?>" readonly>
        </div>
        <div class="form-group">
            <label class="">Industry</label>
            <input type="text" class="form-control" id="industry" value="<?= $holder_info['industry'] ?>" readonly>
        </div>
        <div class="form-group d-flex align-items-center">
            <div class="pr-2 text-nowrap">Work Info</div>
            <hr class="w-100">
        </div>
        <div class="form-group">
            <label class="">Industry</label>
            <input type="text" class="form-control" id="industry_2" value="<?= $holder_info['industry_2'] ?>" readonly>
        </div>
        <div class="form-group">
            <label class="">Job Title</label>
            <input type="text" class="form-control" id="job_title" value="<?= $holder_info['job_title'] ?>" readonly>
        </div>
        <div class="form-group">
            <label class="">Company</label>
            <input type="text" class="form-control" id="company" value="<?= $holder_info['company'] ?>" readonly>
        </div>
        <div class="form-group">
            <label class="">Salary</label>
            <input type="text" class="form-control" id="salary" value="<?= $holder_info['salary'] ?>" readonly>
        </div>
        <div class="form-group d-flex align-items-center">
            <div class="pr-4 text-nowrap">QR Code</div>
            <hr class="w-100">
        </div>
        <div class="form-group">
            <button id="show_qr" href="#qr_code" data-toggle="collapse" class="btn btn-default">Show</button>
        </div>
        <div id="qr_code" class="form-group collapse">
            <div class="d-flex align-items-center">
                <div class="col-6 text-center">
                    Apple
                    <img class="img-fluid w-100" src="<?= base_url("assets/user_qr/event_ticket/" . $holder_info['id'] . "/apple.png") ?>">
                </div>
                <div class="col-6 text-center">
                    Google
                    <img class="img-fluid w-100" src="<?= base_url("assets/user_qr/event_ticket/" . $holder_info['id'] . "/google.png") ?>">
                </div>
            </div>
        </div>
        <div class="form-group d-flex align-items-center">
            <div class="pr-2 text-nowrap">Control</div>
            <hr class="w-100">
        </div>
        <div class="form-group">
            <a href="/merchant/event_ticket_memberbase" class="btn btn-default">Return</a>
            <?php if ($holder_info['email'] !== "") { ?>
                <a href="/merchant/event_ticket_memberbase/send_email/<?= $holder_info['id'] ?>" class="btn btn-default">Send Invitaion</a>
            <?php } ?>
        </div>
    </div>
</div>

<script>
    $("#show_qr").on("click", function(event) {
        event.preventDefault();
    })
</script>