<div class="page-body">
    <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5"><?= $page ?></h4>
    <div class="row">
        <div class="col-12 col-md-4 col-xl-3">
            <a class="item" href="/merchant/event_ticket/add">
                <div class="item_container">
                    <div class="box">
                        <span style="font-size:8rem">+</span>
                    </div>
                    <div class="name_container">
                        Create New
                    </div>
                </div>
            </a>
        </div>
        <?php foreach ($event_tickets as $ticket) { ?>
            <div class="col-12 col-md-4 col-xl-3">
                <div class="item_container">
                    <a class="item" href="/merchant/event_ticket/edit/<?= $ticket['id'] ?>">
                        <div class="box">
                            <div class="card_preview_frame" style="background:<?= $ticket['background_color'] ?>">
                                <div class="row pb-2 align-items-center">
                                    <div class="col-4">
                                        <img class="img-fluid w-100" style="border-radius:50%" src="<?= base_url($ticket['logo']) ?>?id=<?= rand(1, 10000) ?>">
                                    </div>
                                    <div class="col-8 pl-0">
                                        <span style="color:<?= $ticket['content_color'] ?>!important;"><?= $ticket['logo_text'] ?></span>
                                    </div>
                                </div>
                                <div style="margin-left:-5px;margin-right:-5px;">
                                    <img class="img-fluid w-100" style="max-width:100%!important;" src="<?= base_url($ticket['strip']) ?>?id=<?= rand(1, 10000) ?>">
                                </div>
                                <div class="row">
                                    <div class="col-4 d-flex flex-column">
                                        <label class="mb-0" style="color:<?= $ticket['label_color'] ?>">LABEL</label>
                                        <span style="color:<?= $ticket['content_color'] ?>">value</span>
                                    </div>
                                    <div class="col-4 d-flex flex-column">
                                        <label class="mb-0" style="color:<?= $ticket['label_color'] ?>">LABEL</label>
                                        <span style="color:<?= $ticket['content_color'] ?>">value</span>
                                    </div>
                                    <div class="col-4 d-flex flex-column">
                                        <label class="mb-0" style="color:<?= $ticket['label_color'] ?>">LABEL</label>
                                        <span style="color:<?= $ticket['content_color'] ?>">value</span>
                                    </div>
                                </div>
                                <div class="row justify-content-center mt-auto mb-4">
                                    <div class="col-5 p-2" style="border-radius:15px;background-color:white;">
                                        <!--<img class="img-fluid" style="max-width:100%!important;" src="<?= base_url('assets/images/example_qr.png') ?>">-->
                                        <img class="img-fluid" style="max-width:100%!important;" src="<?= base_url("public/images/uploads/event_ticket/" . $ticket['id'] . "/QR.png") ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <div class="name_container">
                        <a id="name_<?= $ticket['id'] ?>" href="/merchant/event_ticket/edit/<?= $ticket['id'] ?>"><?= $ticket['management_name'] ?></a>&nbsp;&nbsp;<span class="rename_button" data-id="<?= $ticket['id'] ?>"><i class="fas fa-pencil-alt"></i>&nbsp;&nbsp;</span><span class="copy_button" data-id="<?= $ticket['id'] ?>" data-type="<?= $ticket['event_ticket_type'] ?>"><i class="fas fa-copy"></i></span>&nbsp;&nbsp;<span class="delete_button" data-id="<?= $ticket['id'] ?>"><i class="fas fa-trash-alt"></i></span>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<div class="modal fade" id="copy_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg mx-auto" role="document">
        <div class="modal-content bg-accent">
            <div class="modal-content-wrapper">
                <div class="modal-header p-0 text-white bg-transparent border-0">
                    New Event Code
                </div>
                <div class="modal-body mt-5 p-5 bg-white" style="border-radius:30px!important;">
                    <div class="form-group">
                        <input type="text" class="form-control" id="new_event_code" placeholder="Please enter the new Event Code">
                    </div>
                    <div class="d-flex">
                        <span id="notification_words" class=""></span>
                        <button class="btn btn-success ml-auto" id="submit_button">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="rename_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg mx-auto" role="document">
        <div class="modal-content bg-accent">
            <div class="modal-content-wrapper">
                <div class="modal-header p-0 text-white bg-transparent border-0">
                    Rename Event Ticket project name
                </div>
                <div class="modal-body mt-5 p-5 bg-white" style="border-radius:30px!important;">
                    <div class="form-group">
                        <input type="text" class="form-control" id="new_event_ticket_name" placeholder="Please enter the new project name">
                    </div>
                    <div class="d-flex">
                        <button class="btn btn-default font-weight-bold ml-auto" id="rename_submit" style="background-color:#4a5ba3!important;color:white!important;">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    var base_url = "<?= base_url() ?>";
    var selected_ticket_id = "";
    var pass_checking = "false";
    $(".delete_button").on("click", function() {
        if (confirm("Are you sure to delete this event ticket ?")) {
            $.ajax({
                url: base_url + "merchant/event_ticket/delete/" + $(this).data("id"),
                success: function() {
                    location.reload();
                },
                error: function() {
                    alert("Somthing is wrong");
                }
            });
        }
    })

    $(".copy_button").on("click", function() {
        selected_ticket_id = $(this).data("id");
        if ($(this).data("type") == 1) {
            if(confirm("This is a API event ticket, are you sure to copy this event ticket setting?")){
                $("#loading").show();
                 $.ajax({
            url: "/merchant/event_ticket/duplicate",
            method: "post",
            data: {
                "event_code": "",
                "id": selected_ticket_id
            },
            success: function() {
                location.reload();
            },
            error: function() {
                alert("Something is wrong");
            }
        })
            }
            
        }else{
        $("#copy_modal").modal("show");
        }
    })

    $("#new_event_code").on("input", function() {
        $.ajax({
            url: "/merchant/event_ticket/check_duplicate_code",
            data: {
                "code": $(this).val()
            },
            dataType: "json",
            success: function(data) {
                if (data.pass == "true") {
                    pass_checking = "true";
                    $("#notification_words").empty();
                    $("#notification_words").removeClass("text-danger");
                    $("#notification_words").addClass("text-success");
                    $("#notification_words").text("This event code has not been used");
                } else {
                    pass_checking = "false";
                    $("#notification_words").empty();
                    $("#notification_words").removeClass("text-success");
                    $("#notification_words").addClass("text-danger");
                    $("#notification_words").text("This event code has been used");
                }
            }

        })
    })

    $("#submit_button").on("click", function() {
        if ($("#new_event_code").val() === "") {
            alert("Please enter the new event code");
            return false;
        }

        if (pass_checking == "false") {
            alert("Please enter another event code");
            return false;
        }
        $("#copy_modal").modal("hide");
        $("#loading").show();
        $.ajax({
            url: "/merchant/event_ticket/duplicate",
            method: "post",
            data: {
                "event_code": $("#new_event_code").val(),
                "id": selected_ticket_id
            },
            success: function() {
                location.reload();
            },
            error: function() {
                alert("Something is wrong");
            }
        })
    })

    $(".rename_button").on("click", function() {
        selected_ticket_id = $(this).data('id');
        $("#new_event_ticket_name").val("");
        $("#rename_modal").modal("show");
    })

    $("#rename_submit").on("click", function() {
        if ($("#new_event_ticket_name").val() == "") {
            alert("Please enter the new event ticket project name");
            return;
        }
        if (confirm("Are you sure to rename this event ticket project ?")) {
            $.ajax({
                url: base_url + "merchant/event_ticket/rename_project",
                data: {
                    "id": selected_ticket_id,
                    "new_name": $("#new_event_ticket_name").val()
                },
                method: "post",
                dataType: "json",
                success: function(data) {
                    $("#rename_modal").modal("hide");
                    $("#name_" + data.id).text(data.new_name);
                },
                error: function() {

                }
            })
        }
    })
</script>