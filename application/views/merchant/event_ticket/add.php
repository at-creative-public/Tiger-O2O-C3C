<div class="page-body">
    <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5"><?= $page ?></h4>
    <div class="row">
        <div class="col-12 col-md-4">
            <ul class="nav justify-content-center mb-4">
                <li class="nav-item ml-2 mr-2 mb-2"><a href="#" data-preview="apple_front" class="btn btn-default p-2 mobile_preview_button preview_selected">Apple Front</a></li>
                <li class="nav-item ml-2 mr-2 mb-2"><a href="#" data-preview="apple_back" class="btn btn-default p-2 mobile_preview_button">Apple Back</a></li>
                <li class="nav-item ml-2 mr-2 mb-2"><a href="#" data-preview="google_front" class="btn btn-defualt p-2 mobile_preview_button">Google Front</a></li>
                <li class="nav-item ml-2 mr-2 mb-2"><a href="#" data-preview="google_back" class="btn btn-default p-2 mobile_preview_button">Google Back</a></li>
            </ul>
            <div class="w-100 mobile_preview_container" id="apple_front">
                <div class="apple_mobile_preview_container">
                    <div class="phone">
                        <div id="apple_card_frame" class="card_frame extra_height d-flex flex-column" style="background:#000000;">
                            <div class="card_header">
                                <div class="col-2 p-2 mr-2">
                                    <img id="apple_logo_image" class="apple_mobile_logo" src="">
                                </div>
                                <div class="col-10 pl-4 text-left">
                                    <span id="apple_header_text" class="apple_header_text apple_phone_value"></span>
                                </div>
                            </div>
                            <div class="card_image">
                                <img id="apple_strip_image" class="apple_strip_image" src="">
                            </div>
                            <div class="title_row">
                                <h4 id="event_name_display" class="apple_phone_value font-weight-bold"></h4>
                            </div>
                            <div class="data_row extra_row_top">
                                <div class="d-flex apple_data_row" style="overflow:scroll">
                                    <div class="col" id="apple_data_1">
                                        <span id="apple_data_1_label" class="data_label apple_phone_label"></span>
                                        <div id="apple_data_1_data" class="apple_phone_value">value</div>
                                    </div>
                                    <div class="col" id="apple_data_2">
                                        <span id="apple_data_2_label" class="data_label apple_phone_label"></span>
                                        <div id="apple_data_2_data" class="apple_phone_value">
                                            value
                                        </div>
                                    </div>
                                    <div class="col" id="apple_data_3">
                                        <span id="apple_data_3_label" class="data_label apple_phone_label"></span>
                                        <div id="apple_data_3_data" class="apple_phone_value">
                                            value
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="data_row_2 extra_row_top">
                                <div class="d-flex apple_data_row" style="overflow:scroll">
                                    <div class="col" id="apple_data_4">
                                        <span id="apple_data_4_label" class="data_label apple_phone_label"></span>
                                        <div id="apple_data_4_data" class="apple_phone_value">
                                            value
                                        </div>
                                    </div>
                                    <div class="col" id="apple_data_5">
                                        <span id="apple_data_5_label" class="data_label apple_phone_label"></span>
                                        <div id="apple_data_5_data" class="apple_phone_value">
                                            value
                                        </div>
                                    </div>
                                    <div class="col" id="apple_data_6">
                                        <span id="apple_data_6_label" class="data_label apple_phone_label"></span>
                                        <span id="apple_data_6_data" class="apple_phone_value">
                                            value
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="qr_code">
                                <div class="qr_container">
                                    <img class="" src="<?= base_url('assets/images/example_qr.png') ?>">
                                    <span class="label"><span id="event_code_display"></span>-ID</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 mobile_preview_container" id="apple_back" style="display:none;">
                <div class="apple_mobile_back_preview_container">
                    <div class="phone">
                        <div class="card_frame d-flex flex-column">
                            <div class="col_1">
                                <div>
                                    Our Website
                                </div>
                                <span id="backside_hyperlink" class="backside_hyperlink"></span>
                            </div>
                            <div class="col_2">
                                <div>
                                    About Us
                                </div>
                                <span id="backside_about_us" class="backside_about_us"></span>
                            </div>
                            <div class="col_3">
                                <div>
                                    Terms & Conditions
                                </div>
                                <span id="backside_terms_conditions" class="backside_terms_conditions"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 mobile_preview_container" id="google_front" style="display:none;">
                <div class="google_mobile_preview_container">
                    <div class="phone">
                        <div id="google_card_frame" class="card_frame" style="background-color:#000000">
                            <div class="card_header d-flex">
                                <div class="col-2">
                                    <img id="google_logo_image" class="google_logo_image" src="">
                                </div>
                                <div class="col-10">
                                    <span id="google_header_text" class="google_phone_text"></span>
                                </div>
                            </div>
                            <div class="data_row">
                                <h4 id="google_location" class="mb-1 ml-4 mr-4 text-left font-weight-bold google_phone_text"></h4>
                                <h2 id="google_card_name" class="mb-4 ml-4 mr-4 text-left font-weight-bold google_phone_text"></h2>
                                <div id="google_data_row_1" class="row mb-4">
                                    <div id="google_phone_data_1_set" class="col-4 text-left">
                                        <h6 id="google_data_1_label" class="ml-4 mr-4 google_phone_text"></h6>
                                        <h5 id="google_data_1_data" class="ml-4 mr-4 google_phone_text">Value</h5>
                                    </div>
                                    <div id="google_phone_data_2_set" class="col-4 text-center">
                                        <h6 id="google_data_2_label" class="ml-4 mr-4 google_phone_text"></h6>
                                        <h5 id="google_data_2_data" class="ml-4 mr-4 google_phone_text">Value</h5>
                                    </div>
                                    <div id="google_phone_data_3_set" class="col-4 text-right">
                                        <h6 id="google_data_3_label" class="ml-4 mr-4 google_phone_text"></h6>
                                        <h5 id="google_data_3_data" class="ml-4 mr-4 google_phone_text">Value</h5>
                                    </div>
                                </div>
                                <div id="google_data_row_2" class="row mb-4 text-left">
                                    <div id="google_phone_data_4_set" class="col-4">
                                        <h6 id="google_data_4_label" class="ml-4 mr-4 google_phone_text"></h6>
                                        <h5 id="google_data_4_data" class="ml-4 mr-4 google_phone_text">Value</h5>
                                    </div>
                                    <div id="google_phone_data_5_set" class="col-4 text-center">
                                        <h6 id="google_data_5_label" class="ml-4 mr-4 google_phone_text"></h6>
                                        <h5 id="google_data_5_data" class="ml-4 mr-4 google_phone_text">Value</h5>
                                    </div>
                                    <div id="google_phone_data_6_set" class="col-4 text-right">
                                        <h6 id="google_data_6_label" class="ml-4 mr-4 google_phone_text"></h6>
                                        <h5 id="google_data_6_data" class="ml-4 mr-4 google_phone_text">Value</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="qr_code flex-column">
                                <div class="qr_code_container">
                                    <img src="<?= base_url("assets/images/example_qr.png") ?>">
                                </div>
                                <span class="google_phone_text mt-2" style="font-size:10px!important;"><span id="event_code_display_google"></span> - ID</span>

                            </div>
                            <div class="card_image">
                                <img id="google_hero_image" class="google_card_image" src="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 mobile_preview_container" id="google_back" style="display:none;">
                <div class="google_mobile_back_preview_container">
                    <div class="phone">
                        <div class="backlight">
                            <div class="card_frame">
                                <div clsas="header_block"></div>
                                <div class="pl-4 pr-4 pt-3 pb-3">
                                    Details
                                </div>
                                <div class="pl-4 pr-4 pt-3 pb-3 d-flex align-items-center">
                                    <i style="font-size:20px;color:#0b79ff" class="far fa-globe-americas"></i><span class="pl-4 font-weight-bold">Our Website</span>
                                </div>
                                <div class="pl-4 pr-4 pt-3 pb-3">
                                    <div class="font-weight-bold">About Us</div>
                                    <span id="google_backside_about_us"></span>
                                </div>
                                <div class="pl-4 pr-4 pt-3 pb-3">
                                    <div class="font-weight-bold">Terms & Conditions</div>
                                    <span id="google_backside_terms_conditions"></span>
                                </div>
                                <div class="pl-4 pr-4 pt-3 pb-3">
                                    <p style="font-size:10px;line-height:1;">The pass provider or merchant is responsible for the info on this pass and may send you notifications. Contact them with any questions.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-8">
            <div class="editor">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item first-block">
                        Creator
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" id="setting-tab" data-toggle="tab" href="#setting" role="tab" aria-controls="home" aria-selected="true"><img class="img-fluid" src="<?= base_url("assets/images/merchant/setting.png") ?>">&nbsp;<span class="tab_word">Setting</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="form-tab" data-toggle="tab" href="#requirement" role="tab" aria-controls="profile" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/form.png") ?>">&nbsp;<span class="tab_word">Form</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="header-tab" data-toggle="tab" href="#header" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/header.png") ?>">&nbsp;<span class="tab_word">Header</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="theme-tab" data-toggle="tab" href="#theme" role="tab" aria-controls="false"><img class="img-fliud" src="<?= base_url("assets/images/merchant/theme.png") ?>">&nbsp;<span class="tab_word">Theme</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="layout-tab" data-toggle="tab" href="#layout" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/layout.png") ?>">&nbsp;<span class="tab_word">Layout</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/detail.png") ?>">&nbsp;<span class="tab_word">Details</span></a>
                    </li>
                    <li class="nav-item ml-auto end-block">
                        <a class="nav-link" id="help-tab" data-toggle="tab" href="#help" aria-controls="contact" aria-selected="false" style="padding:0px!important;border-radius:50px important;display:flex!important;align-items:center;background-color:transparent!important;border:none!important;">
                            <i class="fas fa-list"></i>&nbsp;<span class="tab_word" style="color:white">Help</span>
                        </a>
                    </li>
                </ul>

                <form id="creator_form" method="post" action="/merchant/event_ticket/insert" enctype="multipart/form-data">
                    <div class="tab-content" id="myTabContent">
                        <button id="next_button" class="btn blue_button" style="border-radius:15px 0px 15px 0px!important;">NEXT</button>
                        <button id="export_button" class="btn blue_button" style="border-radius:15px 0px 15px 0px!important;">Save</button>
                        <div class="tab-pane fade active show" id="setting" role="tabpanel" aria-labelledby="home">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        <div class="d-flex align-items-center">
                                            <div class="pr-4">
                                                Type of the Event ticket
                                            </div>
                                            <select class="blue_select" id="event_ticket_type" name="event_ticket_type">
                                                <option value="0">General</option>
                                                <option value="1">API</option>
                                            </select>
                                            <div class="pl-4">
                                                <i class="fa fa-info-circle tips" aria-hidden="true"><span class="tips_content">It cannot be changed once the pass is created.</span></i>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="d-flex align-items-center">
                                            <div class="pr-4">
                                                Type of Event
                                            </div>
                                            <select id="type_of_event" name="event_type" class="blue_select">
                                                <option value="public">Public</option>
                                                <option value="private">Private</option>
                                            </select>
                                            <div class="pl-4">
                                                <i class="fa fa-info-circle tips" aria-hidden="true"><span class="tips_content">It cannot be changed once the pass is created.<br>Private event and API event ticket, the system will not generate the registration form. <br>This value will not be used in the API event ticket.</span></i>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        Project name<span style="color:red">*</span>
                                        <input type="text" id="management_name" class="form-control" name="management_name">
                                    </li>
                                    <li>
                                        Event name<span style="color:red">*</span>
                                        <input type="text" id="event_name" class="form-control" name="event_name">
                                    </li>
                                    <li>
                                        Issuer name<span style="color:red">*</span>
                                        <input type="text" id="issuer_name" class="form-control" name="issuer_name">
                                    </li>
                                    <li>
                                        Event code<span style="color:red">*</span>
                                        <i class="fa fa-info-circle tips pl-2" aria-hidden="true"><span class="tips_content">Event Code is the starting code of the event tickets <br>which will be appeared next to the QR code on all event tickets and cannot be changed afterwards.<br>This value will not be used in the API event ticket and replaced by the ref id.</span></i>
                                        <input type="text" id="event_code" class="form-control" name="event_code">
                                    </li>
                                    <li>
                                        Event date<span style="color:red">*</span>
                                        <div class="d-flex" style="flex-wrap:wrap;">
                                            <div class="d-flex col-12 col-md-4 text-left align-items-center p-0">
                                                <span style="width:6rem">Start:</span>
                                                <input type="time" id="event_start_time" class=" form-control" name="event_start_time">
                                            </div>
                                            <div class="d-flex col-12 col-md-4 text-left align-items-center pr-0 pl-0 pt-2 pt-md-0 pl-md-3">
                                                <span style="width:6rem">End:</span>
                                                <input type="time" id="event_end_time" class=" form-control" name="event_end_time">
                                            </div>
                                            <div class="d-flex col-12 col-md-4 text-left align-items-center pr-0 pl-0 pt-2 pt-md-0 pl-md-3">
                                                <span style="width:6rem">Date:</span>
                                                <input type="date" id="event_date" class="form-control" name="event_date">
                                            </div>
                                        </div>

                                    </li>
                                    <li>
                                        Event venue<span style="color:red">*</span>
                                        <input type="text" id="event_venue" class="form-control" name="event_venue">
                                    </li>
                                    <li>
                                        Location detail<span style="color:red">*</span>
                                        <input type="text" id="location_detail" class="form-control" name="location_detail">
                                    </li>
                                    <li>
                                        Icon<span style="color:red">*</span>
                                        <input type="file" id="icon_upload_n" name="icon" class="form-control" style="height:40px!important;opacity:1!important;" accept=".png">
                                        Recommended size: 29px(W) x 29px(H)
                                    </li>
                                    <li>
                                        Email banner<span style="color:red">*</span>
                                        <input type="file" id="email_banner_upload_n" name="email_banner" class="form-control" style="height:40px!important;opacity:1!important;" accept=".png">
                                        Recommended size: 800px(W)
                                        <div class="pt-4">
                                            <h6 style="color:red">* required</h6>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade " id="theme" role="tabpanel" aria-labelledby="home">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        Title Colour<span style="color:red">*</span><br>
                                        <div class="d-flex">
                                            <input type="color" name="label_color" id="label_color" value="#FFFFFF">
                                            <input type="text" class="form-control ml-4 mr-4" id="label_color_hex" value="#FFFFFF">
                                        </div>
                                    </li>
                                    <li>
                                        Content Colour<span style="color:red">*</span>
                                        <div class="d-flex">
                                            <input type="color" name="content_color" id="content_color" value="#FFFFFF">
                                            <input type="text" class="form-control ml-4 mr-4" id="content_color_hex" value="#FFFFFF">
                                        </div>
                                    </li>
                                    <li>
                                        Background Colour<span style="color:red">*</span>
                                        <div class="d-flex">
                                            <input type="color" name="background_color" id="background_color" value="#000000">
                                            <input type="text" class="form-control ml-4 mr-4" id="background_color_hex" value="#000000">
                                        </div>
                                        Google's pass will assign the text colour base on the background colour.
                                    </li>
                                    <li>
                                        <div class="">
                                            Banner<span style="color:red">*</span>
                                            <input type="file" class="form-control" name="strip" id="banner_upload_n" style="height:40px!important;opacity:1!important;" accept=".png">
                                            Recommended size: 320px(W) x 123px(H)

                                        </div>
                                        <div class="pt-4">
                                            <h6 style="color:red">* required</h6>
                                        </div>

                                    </li>


                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="requirement" role="tabpanel">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        Data Required<br>
                                        <!--<input type="radio" id="data_requirement_simple" name="data_requirement" checked value="simple" style="display:none">
                                <input type="radio" id="data_requirement_informative" name="data_requirement" value="informative" style="display:none"> -->
                                        <input type="hidden" id="data_requirement" name="data_requirement" value="simple">
                                        <button id="button_simple" class="btn radio_button button_simple selected" value="simple">SIMPLE</button>
                                        <button id="button_informative" class="btn radio_button button_informative" value="informative">INFORMATIVE</button>
                                    </li>
                                    <li id="personal_information_table" style="display:none;">Personal Information<br>

                                        <div class="row ">
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="NAME" name="name" style="display:none;">
                                                <button id="NAME_BUTTON" class="btn checkbox_button" value="NAME">NAME</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="SURNAME" name="surname" style="display:none;">
                                                <button id="SURNAME_BUTTON" class="btn checkbox_button" value="SURNAME">SURNAME</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="GIVEN_NAME" name="given_name" style="display:none;">
                                                <button id="GIVEN_NAME_BUTTON" class="btn checkbox_button" value="GIVEN_NAME">GIVEN NAME</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="ENGLISH_NAME" name="english_name" style="display:none;">
                                                <button id="ENGLISH_NAME_BUTTON" class="btn checkbox_button" value="ENGLISH_NAME">ENGLISH NAME</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="CHINESE_NAME" name="chinese_name" style="display:none;">
                                                <button id="CHINESE_NAME_BUTTON" class="btn checkbox_button" value="CHINESE_NAME">CHINESE NAME</button>
                                            </div>

                                            <!--<div class="row mb-2">-->
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="GENDER" name="gender" style="display:none;">
                                                <button id="GENDER_BUTTON" class="btn checkbox_button" value="GENDER">GENDER</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="BIRTHDAY" name="birthday" style="display:none;">
                                                <button id="BIRTHDAY_BUTTON" class="btn checkbox_button" value="BIRTHDAY">BIRTHDAY</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="AGE" name="age" style="display:none;">
                                                <button id="AGE_BUTTON" class="btn checkbox_button" value="AGE">AGE</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="NATIONALITY" name="nationality" style="display:none;">
                                                <button id="NATIONALITY_BUTTON" class="btn checkbox_button" value="NATIONALITY">NATIONALITY</button>
                                            </div>

                                    </li>
                                    <li id="contact_information_table" style="display:none;">
                                        Contact Information

                                        <div class="row mb-2">
                                            <div class="col-3">
                                                <input type="checkbox" id="PHONE" name="phone" style="display:none;">
                                                <button id="PHONE_BUTTON" class="btn checkbox_button" value="PHONE">PHONE</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="EMAIL" name="email" style="display:none;">
                                                <button id="EMAIL_BUTTON" class="btn checkbox_button" value="EMAIL">EMAIL</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="ADDRESS" name="address" style="display:none;">
                                                <button id="ADDRESS_BUTTON" class="btn checkbox_button" name="address" value="ADDRESS">ADDRESS</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="INDUSTRY" name="industry" style="display:none;">
                                                <button id="INDUSTRY_BUTTON" class="btn checkbox_button" value="INDUSTRY">INDUSTRY</button>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="work_information_table" style="display:none;">
                                        Work Information

                                        <div class="row mb-2">
                                            <div class="col-3">
                                                <input type="checkbox" id="INDUSTRY_2" name="industry_2" style="display:none;">
                                                <button id="INDUSTRY_2_BUTTON" class="btn checkbox_button" value="industry_2">INDUSTRY</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="JOB_TITLE" name="job_title" style="display:none;">
                                                <button id="JOB_TITLE_BUTTON" class="btn checkbox_button" value="JOB_TITLE">JOB TITLE</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="COMPANY" name="company" style="display:none;">
                                                <button id="COMPANY_BUTTON" class="btn checkbox_button" value="COMPANY">COMPANY</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="SALARY" name="salary" style="display:none;">
                                                <button id="SALARY_BUTTON" class="btn checkbox_button" value="SALARY">SALARY</button>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="simple_remark" style="display:block">
                                        Simple includes "English Name","Chinese Name","Phone","Email"
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="header" role="tabpanel">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        LOGO<span style="color:red">*</span>
                                        <input type="file" id="logo_upload_n" name="logo" class="form-control mb-2" style="height:40px!important;opacity:1!important;" accept=".png">
                                        Recommend size: 100px(W) x 100px(H)
                                    </li>
                                    <li>
                                        Header Text<span style="color:red">*</span><br>
                                        <input type="text" class="form-control" id="header_text" name="logo_text" placeholder="Header Content">
                                        <div class="pt-4">
                                            <h6 style="color:red">* required</h6>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="layout" role="tabpanel">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        Content Data
                                    </li>
                                    Row 1
                                    <div class="d-flex" style="margin:15px 0px">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" id="front_data_1_status" name="front_data_1_status" class="data_trigger" checked style="display:none">
                                                <span class="slider round d-flex align-items-center">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" id="front_data_1_label" name="front_data_1_label" class="form-control data_trigger" placeholder="Label 1" value="Label 1">
                                        </div>
                                        <div class="col">
                                            <select class="form-control h-100 clear_select" name="front_data_1_data">
                                                <?php foreach ($data_fields as $index => $field) {
                                                    if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option value="<?= $index ?>"><?= $field ?></option>
                                                    <?php }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-flex" style="margin:15px 0px">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" id="front_data_2_status" name="front_data_2_status" class="data_trigger" checked style="display:none;">
                                                <span class="slider round d-flex align-items-center">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" id="front_data_2_label" name="front_data_2_label" class="form-control data_trigger" placeholder="Label 2" value="Label 2">
                                        </div>
                                        <div class="col">
                                            <select class="form-control h-100 clear_select" name="front_data_2_data">
                                                <?php foreach ($data_fields as $index => $field) {
                                                    if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option value="<?= $index ?>"><?= $field ?></option>
                                                    <?php }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" id="front_data_3_status" name="front_data_3_status" class="data_trigger" checked style="display:none;">
                                                <span class="slider round d-flex align-items-center">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" id="front_data_3_label" name="front_data_3_label" class="form-control data_trigger" placeholder="Label 3" value="Label 3">
                                        </div>
                                        <div class="col">
                                            <select class="form-control h-100 clear_select" name="front_data_3_data">
                                                <?php foreach ($data_fields as $index => $field) {
                                                    if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option value="<?= $index ?>"><?= $field ?></option>
                                                    <?php }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                    Row 2
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" id="front_data_4_status" name="front_data_4_status" class="data_trigger" checked style="display:none;">
                                                <span class="slider round d-flex align-items-center">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" id="front_data_4_label" name="front_data_4_label" class="form-control data_trigger" placeholder="Label 4" value="Label 4">
                                        </div>
                                        <div class="col">
                                            <select class="form-control h-100 clear_select" name="front_data_4_data">
                                                <?php foreach ($data_fields as $index => $field) {
                                                    if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option value="<?= $index ?>"><?= $field ?></option>
                                                    <?php }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" id="front_data_5_status" name="front_data_5_status" class="data_trigger" checked style="display:none;">
                                                <span class="slider round d-flex align-items-center">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" id="front_data_5_label" name="front_data_5_label" class="form-control data_trigger" placeholder="Label 5" value="Label 5">
                                        </div>
                                        <div class="col">
                                            <select class="form-control h-100 clear_select" name="front_data_5_data">
                                                <?php foreach ($data_fields as $index => $field) {
                                                    if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $field ?>--">
                                                        <?php } else { ?>
                                                            <option value="<?= $index ?>"><?= $field ?></option>
                                                    <?php }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" id="front_data_6_status" name="front_data_6_status" class="data_trigger" checked style="display:none">
                                                <span class="slider round d-flex align-items-center">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" id="front_data_6_label" name="front_data_6_label" class="form-control data_trigger" placeholder="Label 6" value="Label 6">
                                        </div>
                                        <div class="col">
                                            <select class="form-control h-100 clear_select" name="front_data_6_data">
                                                <?php foreach ($data_fields as $index => $field) {
                                                    if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option value="<?= $index ?>"><?= $field ?></option>
                                                    <?php }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>

                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="details" role="tabpanel">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        <div class="d-flex w-100 align-items-center">
                                            <div class="p-0" style="width:65px">URL<span style="color:red">*</span></div>
                                            <div class="col"><input type="text" id="website_url" name="url" class="form-control" placeholder="Please paste your link"></div>
                                        </div>
                                    </li>
                                    <li>
                                        About Us<span style="color:red">*</span><br>
                                        <textarea class="form-control" id="about_us_content" name="about_us" placeholder="Please type here"></textarea>
                                    </li>
                                    <li>
                                        Terms & Conditions<span style="color:red">*</span>
                                        <textarea class="form-control" id="terms_content" name="terms_conditions" placeholder="Please type here"></textarea>
                                    </li>
                                    <div class="pt-4">
                                        <h6 style="color:red">* required</h6>
                                    </div>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="help" role="tabpanel" aria-labelledby="home-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>Got some problems? Watch the tutorial!<br>
                                        <video controls style="width:100%;height:auto;">
                                            <source src="<?= base_url("/assets/tutorial_video/O2OC3C.m4v") ?>" type="video/mp4">
                                        </video>
                                    </li>
                                    <li>Problem not solved? <a href="#">Contact Us</a></li>

                                </ul>

                            </div>
                        </div>
                    </div>
                </form>



            </div>
        </div>
    </div>
</div>

<script src="/assets/js/event_ticket_add.js"></script>
<script>
    var base_url = "<?= base_url() ?>";
    var current_page = "add";
    var event_code_check = false;
    $(document).ready(function() {
        $(".data_trigger").trigger("input");
        $("#content_color").trigger("input");
        $("#label_color").trigger("input");
        $("#background_color").trigger("input");
    })
</script>