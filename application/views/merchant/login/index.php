<!--<html lang="en">-->
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo "O2O C3C CMS - 登入"; ?></title>
    <link href="<?= base_url('public/static/backend/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!--    <link rel="stylesheet" href="/assets/css/cmsstyle.css">-->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900"-->
    <link rel="stylesheet" href="<?= base_url('public/static/backend/css/googlecatamariancss.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('public/static/backend/css/googlelato.css'); ?>">
    <!--    <link rel="stylesheet" href="/assets/at-admin/css/breadcrumbs.css">-->
    <link rel="stylesheet" href="<?= base_url('public/static/backend/css/systemcss.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('public/static/backend/css/admin.css?v=' . rand(10000, 100000)); ?>">
    <link rel="stylesheet" href="<?= base_url('public/static/backend/css/pages/login.css?v=' . rand(10000, 100000)); ?>">
    <link rel="stylesheet" href="<?= base_url('public/static/backend/css/layouts/o2o-c3c.css?v=' . rand(10000, 100000)); ?>">
    <meta name="description" content="">
    <meta name="author" content="">
</head>

<body style="background-image:url('<?= base_url("assets/images/bg-login-image.png") ?>');background-size:100% 100%;background-repeat:repeat-y;">

    <div id="flex-box">
        <section id="section-1" data-pwd="<?= md5('123456') ?>">
            <div>
                <div class="content" style="justify-content:space-around!important;width:100vw;">
                    <div class="col-md-6 login-form-1 text-center " style="padding:30px!important;">
                        <div style="text-align:left;">
                            <img id="logo" class="d-flex" style="margin:0px!important;" src="<?= base_url('assets/images/logo.png'); ?>">

                            <img class="d-flex d-none d-md-block img-fluid" style="max-width:90%" src="<?= base_url('assets/images/image.png'); ?>">

                        </div>

                    </div>
                    <div class="col-md-6 login-form-2 mt-5" style="background-color:transparent;display:flex;
                    justify-content:center;">

                        <?php echo form_open('merchant/login', ['name' => 'form_reg', 'id' => 'form_reg']); ?>
                        <img id="logo" class="d-flex mobile_logo" style="margin-top:3rem!important;margin-bottom:2rem!important;" src="<?= base_url('assets/images/logo.png'); ?>">
                        <h3 class="title">Welcome!</h3>
                        <p class="subtitle text-center">Login to your account</p>
                        <?php if ($timeout) { ?>
                            <div class="form-group d-flex row justify-content-center align-items-center" style="border:1px solid red;border-radius:8px;background-color:rgba(255,0,0,0.2);padding:15px;">
                                <h5 style="font-weight:bold">Operation timeout, please login again!</h5>
                            </div>
                        <?php } ?>
                        <div class="form-group row">
                            <div class="col-12">
                                <input id="userName" name="userName" type="text" style="background-color : #d1d1d1;" class="form-control" placeholder="Merchant ID" value="">
                                <?php echo form_error('email'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <input id="password" name="password" type="password" style="background-color : #d1d1d1;" class="form-control" placeholder="Password" value="">
                                <?php echo form_error('password'); ?>
                                <?php if (!empty($msg)) {
                                    echo $msg;
                                } ?>
                            </div>
                        </div>
                        <p class="forgot-password text-center">Forgot Password? <a target="_blank" href="https://wa.me/85295472118">Contact Us</a></p>
                        <div class="form-group row buttonrow">
                            <div class="col-sm-12 mt-3">
                                <div class="text-center">
                                    <input type="submit" class="btnSubmit" value="LOGIN">
                                </div>
                            </div>
                        </div>

                        <?php if ($this->session->flashdata('logout_msg') != false) { ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <h4><i class="icon fa fa-check"></i> Success!</h4>
                                <?php echo $this->session->flashdata('logout_msg'); ?>
                            </div>
                        <?php } ?>

                        <?php if ($this->session->flashdata('msg') != false) { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <h4><i class="icon fa fa-ban"></i> 錯誤!</h4>
                                <?php echo $this->session->flashdata('msg'); ?>
                            </div>
                        <?php } ?>

                        <?php if (validation_errors() != false) { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                                <?php echo validation_errors(); ?>
                            </div>
                        <?php } ?>

                        </form>
                    </div>
                     <div class="d-sm-block" style="position:reletive;bottom:0px;right:0px;color:darkgray;padding:15px;">
                        © <?= date("Y", time()) ?> <a href="/cms/login" style="color:darkgray;">Smart Business Consultancy Limited.</a> All rights Reserved.
                    </div>
                    <div class="d-none d-md-block" style="position:absolute;bottom:0px;right:0px;color:darkgray;padding:15px;">
                        © <?= date("Y", time()) ?> <a href="/cms/login" style="color:darkgray;">Smart Business Consultancy Limited.</a> All rights Reserved.
                    </div>
                </div>
            </div>

        </section>
    </div>


    <!-- Bootstrap core JavaScript -->
    <script src="<?= base_url('public/static/backend/js/vendor/jquery-1.12.4.min.js'); ?>"></script>
    <script src="<?= base_url('public/static/backend/js/bootstrap.bundle.min.js'); ?>"></script>


</body>

</html>