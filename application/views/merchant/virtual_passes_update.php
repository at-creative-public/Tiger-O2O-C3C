<div class="page-body">
    <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5">Membership</h4>
    <div class="row">
        <div class="col-12 col-md-4 mb-4 mb-md-0 text-center" id="mobile_preview">
            <ul class="nav justify-content-center mb-4">
                <li class="nav-item ml-2 mr-2 mb-2"><a href="#" data-preview="apple_front" class="btn btn-default p-2 mobile_preview_button preview_selected">Apple Front</a></li>
                <li class="nav-item ml-2 mr-2 mb-2"><a href="#" data-preview="apple_back" class="btn btn-default p-2 mobile_preview_button">Apple Back</a></li>
                <li class="nav-item ml-2 mr-2 mb-2"><a href="#" data-preview="google_front" class="btn btn-default p-2 mobile_preview_button">Google Front</a></li>
                <li class="nav-item ml-2 mr-2 mb-2"><a href="#" data-preview="google_back" class="btn btn-default p-2 mobile_preview_button">Google Back</a></li>
            </ul>
            <div class="w-100 mobile_preview_container" id="apple_front">
                <div class="apple_mobile_preview_container">
                    <div class="phone">
                        <div id="apple_card_frame" class="card_frame d-flex flex-column" style="background-color:<?= $card['background_color'] ?>">
                            <div class="card_header">
                                <div class="col-2 p-2 mr-2">
                                    <img id="apple_logo_image" class="apple_mobile_logo" src="<?= base_url($card['logo']) ?>?id=<?= rand(1, 10000) ?>">
                                </div>
                                <div class="col-10 pl-4 text-left">
                                    <span id="apple_header_text" class="apple_header_text apple_phone_label">Header Text</span>
                                </div>
                            </div>
                            <div class="card_image">
                                <img id="apple_strip_image" class="apple_strip_image" src="<?= base_url($card['strip']) ?>?id=<?= rand(1, 100000) ?>">
                            </div>
                            <div class="data_row">
                                <div class="d-flex apple_data_row" id="apple_data_row" style="overflow:scroll">
                                    <div id="apple_data_1" class="col">
                                        <span id="apple_data_1_label" class="data_label apple_phone_label"></span>
                                        <div id="apple_data_1_data" class="apple_phone_value">
                                            value
                                        </div>
                                    </div>
                                    <div id="apple_data_2" class="col">
                                        <span id="apple_data_2_label" class="data_label apple_phone_label"></span>
                                        <div id="apple_data_2_data" class="apple_phone_value">
                                            value
                                        </div>
                                    </div>
                                    <div id="apple_data_3" class="col">
                                        <span id="apple_data_3_label" class="data_label apple_phone_label"></span>
                                        <div id="apple_data_3_data" class="apple_phone_value">
                                            value
                                        </div>
                                    </div>
                                    <div id="apple_data_4" class="col">
                                        <span id="apple_data_4_label" class="data_label apple_phone_label"></span>
                                        <div id="apple_data_4_data" class="apple_phone_value">
                                            value
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="qr_code">
                                <div class="qr_container">
                                    <img class="" src="<?= base_url('assets/images/example_qr.png') ?>">
                                    <span class="label">ID</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="w-100 mobile_preview_container" id="apple_back" style="display:none;">
                <div class="apple_mobile_back_preview_container">
                    <div class="phone">
                        <div class="card_frame d-flex flex-column">
                            <div class="col_1">
                                <div>
                                    Our Website
                                </div>
                                <span id="backside_hyperlink" class="backside_hyperlink"><?= $card['url'] ?></span>
                            </div>
                            <div class="col_2">
                                <div>
                                    About Us
                                </div>
                                <span id="backside_about_us" class="backside_about_us"><?= $card['about_us'] ?></span>
                            </div>
                            <div class="col_3">
                                <div>
                                    Terms & Conditions
                                </div>
                                <span id="backside_terms_conditions" class="backside_terms_conditions"><?= $card['terms_conditions'] ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 mobile_preview_container" id="google_front" style="display:none;">
                <div class="google_mobile_preview_container">
                    <div class="phone">
                        <div id="google_card_frame" class="card_frame" style="background-color:<?= $card['background_color'] ?>">
                            <div class="card_header d-flex">
                                <div class="col-2">
                                    <img id="google_logo_image" class="google_logo_image " src="<?= base_url($card['logo']) ?>?id=<?= rand(1, 10000) ?>">
                                </div>
                                <div class="col-10">
                                    <span id="google_header_text" class="google_phone_text"><?= $card['logo_text'] ?></span>
                                </div>
                            </div>
                            <div class="data_row">
                                <h2 id="google_card_name" class="m-4 text-left font-weight-bold google_phone_text"><?= $card['description'] ?></h2>
                                <div id="google_data_row_1" class="row mb-4">
                                    <div id="google_phone_data_1_set" class="col-6">
                                        <h6 id="google_data_1_label" class="ml-4 mr-4 google_phone_text"></h6>
                                        <h5 id="google_data_1_data" class="ml-4 mr-4 google_phone_text">Value</h5>
                                    </div>
                                    <div id="google_phone_data_2_set" class="col-6 ">
                                        <h6 id="google_data_2_label" class="ml-4 mr-4 google_phone_text"></h6>
                                        <h5 id="google_data_2_data" class="ml-4 mr-4 google_phone_text">Value</h5>
                                    </div>
                                </div>
                                <div id="google_data_row_2" class="row mb-4">
                                    <div id="google_phone_data_3_set" class="col-6">
                                        <h6 id="google_data_3_label" class="ml-4 mr-4 google_phone_text"></h6>
                                        <h5 id="google_data_3_data" class="ml-4 mr-4 google_phone_text">Value</h5>
                                    </div>
                                    <div id="google_phone_data_4_set" class="col-6">
                                        <h6 id="google_data_4_label" class="ml-4 mr-4 google_phone_text"></h6>
                                        <h5 id="google_data_4_data" class="ml-4 mr-4 google_phone_text">Value</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="qr_code flex-column">
                                <div class="qr_code_container">
                                    <img src="<?= base_url("assets/images/example_qr.png") ?>">
                                </div>
                                <span class="google_phone_text mt-2" style="font-size:10px!important;">ID</span>
                            </div>
                            <div class="card_image">
                                <img id="google_hero_image" class="google_card_image" src="<?= base_url($card['strip']) ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 mobile_preview_container" id="google_back" style="display:none;">
                <div class="google_mobile_back_preview_container">
                    <div class="phone">
                        <div class="backlight">
                            <div class="card_frame">
                                <div class="header_block">

                                </div>
                                <div class="pl-4 pr-4 pt-3 pb-3">
                                    Details
                                </div>
                                <div class="pl-4 pr-4 pt-3 pb-3 d-flex align-items-center">
                                    <i style="font-size:20px;color:#0b79ff" class="far fa-globe-americas"></i><span class="pl-4 font-weight-bold">Our Website</span>
                                </div>
                                <div class="pl-4 pr-4 pt-3 pb-3">
                                    <div class="font-weight-bold">About Us</div>
                                    <span id="google_backside_about_us"></span>
                                </div>
                                <div class="pl-4 pr-4 pt-3 pb-3">
                                    <div class="font-weight-bold">Terms & Conditions</div>
                                    <span id="google_backside_terms_conditions"></span>
                                </div>
                                <div class="pl-4 pr-4 pt-3 pb-3">
                                    <p style="font-size:10px;line-height:1;">The pass provider or merchant is responsible for the info on this pass and may send you notifications. Contact them with any questions.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-12 col-md-8">
            <div class="editor" style="max-height:80vh;">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item first-block">
                        CREATOR
                    </li>
                    <!--<li class="nav-item">
                        <a class="nav-link active" id="trial-tab" data-toggle="tab" href="#trial" role="tab" aria-controls="home" aria-selected="true">Trial</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="QR-tab" data-toggle="tab" href="#QR" role="tab" aria-controls="home" aria-selected="true">QR</a>
                    </li>-->
                    <li class="nav-item">
                        <a class="nav-link active" id="setting-tab" data-toggle="tab" href="#setting" role="tab" aria-controls="home" aria-selected="true"><img class="img-fluid" src="<?= base_url("assets/images/merchant/setting.png") ?>">&nbsp;<span class="tab_word">Setting</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="form-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/form.png") ?>">&nbsp;<span class="tab_word">Form</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="header-tab" data-toggle="tab" href="#header" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/header.png") ?>">&nbsp;<span class="tab_word">Header</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="theme-tab" data-toggle="tab" href="#theme" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/theme.png") ?>">&nbsp;<span class="tab_word">Theme</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="layout-tab" data-toggle="tab" href="#layout" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/layout.png") ?>">&nbsp;<span class="tab_word">Layout</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/detail.png") ?>">&nbsp;<span class="tab_word">Details</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="geotag-tab" data-toggle="tab" href="#geotag" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/geotag.png") ?>">&nbsp;<span class="tab_word">Geotag(Beta)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="level-tab" data-toggle="tab" href="#level_management" role="tab" aria-controls="contact" aria-selected="false"><i class="fa fa-sort-amount-asc dark_blue" aria-hidden="true"></i>&nbsp;&nbsp;<span class="tab_word">Level</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="export-tab" data-toggle="tab" href="#export" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/export.png") ?>">&nbsp;<span class="tab_word">Export</span></a>
                    </li>

                    <li class="nav-item ml-auto end-block">
                        <a class="nav-link" data-toggle="tab" href="#help" aria-controls="contact" aria-selected="false" style="padding:0px!important;border-radius:50px important;display:flex!important;align-items:center;background-color:transparent!important;border:none!important;">
                            <i class="fas fa-list"></i>&nbsp;<span class="tab_word" style="color:white;">Help</span>
                        </a>
                    </li>

                </ul>
                <form id="creator_form" action="/merchant/virtual_passes/update_virtual_passes" method="post" enctype="multipart/form-data">
                    <div class="tab-content" id="myTabContent">
                        <input type="hidden" name="user_id" value="<?= $_SESSION['id'] ?>">
                        <input type="hidden" name="id" value="<?= $card['id'] ?>">
                        <!--<button type="submit" id="submit_button" class="btn submit_button" style="position:absolute;right:0;bottom:0;background-color:#4a5ba1;color:white;border-radius:15px 0px 15px 0px!important;padding:10px 15px;">SUBMIT</button>-->
                        <button id="next_button" class="btn blue_button" style="border-radius:15px 0px 15px 0px!important;">NEXT</button>
                        <button id="export_button" class="btn blue_button" style="border-radius:15px 0px 15px 0px!important;">Save</button>
                        <div class="tab-pane fade" id="trial" role="tabpanel" aria-labelledby="home-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <!--
                                <ul class="d-flex flex-column">
                                    <li>
                                        Project name<span style="color:red">*</span><br>
                                        <input type="text" class="form-control" name="project_name" value="<?= $card['project_name'] ?>" required>

                                    </li>
                                    <li>
                                        Logo<br>
                                        <div class="row">
                                            <div class="col-12 col-md-6">
                                                Logo image<span style="color:red">*</span>
                                                <input type="file" name="logo" class="form-control" style="height:40px!important;opacity:1!important;" accept=".png">
                                                Preview:
                                                <img class="img-fluid" style="max-height:50px;max-width:50px;border:1px solid #5566a8" src="<?= base_url($card['logo']) ?>?id=<?= rand(100000, 1000) ?>">
                                                &nbsp;&nbsp;<button class="btn invert_color_button">Invert Color</button>

                                            </div>
                                            <div class="col-12 col-md-6">
                                                Logo Text<span style="color:red">*</span>
                                                <input type="text" name="logo_text" class="form-control" value="<?= $card['logo_text'] ?>" required>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        Icon<span style="color:red">*</span><br>
                                        <input type="file" name="icon" class="form-control" style="height:40px!important;opacity:1!important;" accept=".png">
                                        Preview:
                                        <img class="img-fluid" style="max-width:50px;max-height:50px;border:1px solid #5566a8" src="<?= base_url($card['icon']) ?>?id=<?= rand(100000, 1000) ?>">
                                        &nbsp;&nbsp;<button class="btn invert_color_button">Invert Color</button>
                                    </li>
                                    <li>
                                        Card Name<span style="color:red">*</span><br>
                                        <input type="text" name="description" class="form-control" value="<?= $card['description'] ?>" required>
                                    </li>
                                    <li>
                                        Banner field<br>
                                        <div class="form-group row">
                                            <div class="col-12 col-md-6">
                                                <label class="control-label">Text Overlay<span style="color:red">*</span></label>
                                                <input type="text" class="form-control" name="pri_label" placeholder="Please enter the name of the label" value="<?= $card['pri_label'] ?>" required>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <label class="control-label">Value Overlay<span style="color:red">*</span></label>
                                                <input type="text" class="form-control" name="pri_value" placeholder="Please enther the value " value="<?= $card['pri_value'] ?>" required>
                                            </div>
                                        </div>
                                        <div>
                                            <label class="control-label">Strip<span style="color:red">*</span></label>
                                            <input type="file" class="form-control" name="pri_strip" style="height:40px!important;opacity:1!important;" accept=".png">
                                            Preview:
                                            <img class="img-fluid" style="max-height:50px;border: 1px solid #5566a8" src="<?= base_url($card['strip']) ?>?id=<?= rand(100000, 1000) ?>">
                                            &nbsp;&nbsp;<button class="btn invert_color_button">Invert Color</button>

                                        </div>
                                    </li>

                                    <li>
                                        Colour
                                        <div class="row">
                                            <div class="col-12 col-xl-3 d-flex" style="align-items:center;">
                                                Major color:&nbsp;&nbsp;&nbsp;
                                                <input type="color" name="pri_color" value="<?= $card['pri_color'] ?>">
                                            </div>
                                            <div class="col-12 col-xl-3 d-flex" style="align-items:center;">
                                                Sub-text color:&nbsp;&nbsp;&nbsp;
                                                <input type="color" name="secondary_color" value="<?= $card['secondary_color'] ?>">
                                            </div>
                                            <div class="col-12 col-xl-3 d-flex" style="align-items:center;">
                                                Background color:&nbsp;&nbsp;&nbsp;
                                                <input type="color" name="background_color" value="<?= $card['background_color'] ?>">
                                            </div>
                                            <div class="col-12 col-xl-3 d-flex" style="align-items:center;">
                                                Banner field color:&nbsp;&nbsp;&nbsp;
                                                <input type="color" name="stripColor" value="<?= $card['stripColor'] ?>">
                                            </div>

                                        </div>
                                    </li>
                                    <li>
                                        <div style="display:flex;width:100%;align-items:center;">
                                            <div class="no_padding" style="width:65px">URL<span style="color:red">*</span></div>
                                            <div class="col"><input type="text" class="form-control" placeholder="Please paste your link" value="<?= $card['url'] ?>"></div>
                                        </div>
                                    </li>
                                    <li>
                                        About Us<span style="color:red">*</span><br>
                                        <textarea class="form-control" placeholder="Please type here"><?= $card['about_us'] ?></textarea>
                                    </li>
                                    <li>
                                        Terms & Conditions<span style="color:red">*</span>
                                        <textarea class="form-control" placeholder="Please type here"><?= $card['terms_conditions'] ?></textarea>
                                    </li>
                                    <span style="color:red">*</span> required

-->


                                </ul>



                            </div>
                        </div>
                        <div class="tab-pane fade" id="QR" role="tabpanel" aria-labelledby="QR-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <div class="row" style="max-width: fit-content;">
                                    <!--
                                <div class="col-6">
                                    <img class="img-fluid w-100" src="<?= base_url("public/images/uploads/" . $card['id'] . "/Apple_QR.png") ?>">
                                    <h4>Apple</h4>
                                </div>
                                <div class="col-6">
                                    <img class="img-fluid w-100" src="<?= base_url("public/images/uploads/" . $card['id'] . "/Google_QR.png") ?>">
                                    <h4>Google</h4>
                                </div>-->
                                    <div class="col-12">
                                        <img class="img-fluid w-100" src="<?= base_url("public/images/uploads/" . $card['id'] . "/Merged_QR.png") ?>">
                                        <div class="mt-2">
                                            <a href="<?= base_url('registration/' . $card['id']) ?>"><?= base_url("registration/" . $card['id']) ?></a>
                                        </div>
                                        <h6>For iOS devices,please open with Safari</h6>
                                        <h6>Please open with Safari or click "download" for installation</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade  show active" id="setting" role="tabpanel" aria-labelledby="home-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        Type of Virtual Passes &nbsp;&nbsp;&nbsp;<select disabled>
                                            <option value="membership">Membership Card</option>
                                            <!--<option>Name Card</option>
                                            <option>Stamp Card</option>
                                            <option>Info Sheet</option>
                                            <option>Ticket</option>
                                            <option>Coupon</option>-->
                                        </select>
                                    </li>
                                    <li>
                                        <span class="mr-4">Usage</span>
                                        <select name="type" disabled>
                                            <option <?= $card['type'] == 0 ? "selected" : "" ?> value="0">General</option>
                                            <option <?= $card['type'] == 1 ? "selected" : "" ?> value="1">API purpose</option>
                                        </select>
                                    </li>
                                    <li>
                                        Project Name<span style="color:red">*</span>
                                        <input type="text" class="form-control" id="management_name" name="management_name" value="<?= $card['management_name'] ?>" required>
                                    </li>
                                    <li>
                                        Programme Name<span style="color:red">*</span>
                                        <input type="text" class="form-control" id="project_name" name="project_name" value="<?= $card['project_name'] ?>" required>
                                    </li>
                                    <li>
                                        Card Name<span style="color:red">*</span>
                                        <input id="card_name" type="text" name="description" class="form-control" value="<?= $card['description'] ?>" required>
                                    </li>
                                    <li>
                                        <div class="d-flex align-items-center">
                                            <div class="mr-2" style="white-space:nowrap">
                                                Membership Number starting from
                                            </div>
                                            <input type="text" readonly class="form-control" value="<?= $card['start_from'] ?>">
                                        </div>
                                    </li>
                                    <li>
                                        Icon
                                        <input type="file" id="icon_upload" name="icon" class="form-control mb-2" style="height:40px!important;opacity:1!important;" accept=".png">
                                        <div class="mb-2">
                                            Recommended Size: 29px(W) x 29px(H)
                                        </div>
                                        Preview:
                                        <img id="icon_preview_frame" class="img_preview_frame" src="<?= base_url($card['icon']) ?>?id=<?= rand(1, 100000) ?>">
                                        <button class="btn invert_color_button">Invert Color</button>
                                    </li>
                                    <li class="d-flex">
                                        <h6 style="color:red"><span style="color:red">*</span> required</h6>
                                    </li>

                                    <!--<li>
                                        <div style="display:flex;width:100%;">
                                            <div class="no_padding" style="width:65px">Headline</div>
                                            <div class="col"><input type="text" class="form-control"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div style="display:flex">
                                            <div class="no_padding" style="width:65px;">Banner</div>
                                            <div><img style="height:1em;" class="img-fluid" src="<?= base_url("assets/images/temp2.png") ?>"></div>
                                            <div><button class="btn blue_button" style="border-radius:10px!important;">UPDATE</button></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div style="display:flex;width:100%">
                                            <div class="no_padding" style="width:65px;">Message</div>
                                            <div class="col">
                                                <textarea class="form-control" placeholder="Please type"></textarea>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div style="display:flex;width:100%">
                                            <div class="no_padding" style="width:65px;">Botton</div>
                                            <div class="col">
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                    </li> -->
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        Data Required<br>
                                        <input type="radio" id="data_requirement_simple" name="data_requirement" value="simple" style="display:none">
                                        <input type="radio" id="data_requirement_informative" name="data_requirement" value="informative" style="display:none">
                                        <button class="btn radio_button button_simple" value="simple">SIMPLE</button>
                                        <button class="btn radio_button button_informative" value="informative">INFORMATIVE</button>
                                    </li>
                                    <li id="personal_information_table" style="display:none;">Personal Information<br>
                                        <!--<table>
                                            <tr>
                                                <td class="col-3">
                                                    <input type="checkbox" id="SURNAME" name="surname" style="display:none;">
                                                    <button id="SURNAME_BUTTON" class="btn checkbox_button" value="SURNAME">SURNAME</button>
                                                </td>
                                                <td class="col-3">
                                                    <input type="checkbox" id="GIVEN_NAME" name="given_name" style="display:none;">
                                                    <button id="GIVEN_NAME_BUTTON" class="btn checkbox_button" value="GIVEN_NAME">GIVEN NAME</button>
                                                </td>
                                                <td class="col-3">
                                                    <input type="checkbox" id="ENGLISH_NAME" name="english_name" style="display:none;">
                                                    <button id="ENGLISH_NAME_BUTTON" class="btn checkbox_button" value="ENGLISH_NAME">ENGLISH NAME</button>
                                                </td>
                                                <td class="col-3">
                                                    <input type="checkbox" id="CHINESE_NAME" name="chinese_name" style="display:none;">
                                                    <button id="CHINESE_NAME_BUTTON" class="btn checkbox_button" value="CHINESE_NAME">CHINESE NAME</button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="col-3">
                                                    <input type="checkbox" id="GENDER" name="gender" style="display:none;">
                                                    <button id="GENDER_BUTTON" class="btn checkbox_button" value="GENDER">GENDER</button>
                                                </td>
                                                <td class="col-3">
                                                    <input type="checkbox" id="BIRTHDAY" name="birthday" style="display:none;">
                                                    <button id="BIRTHDAY_BUTTON" class="btn checkbox_button" value="BIRTHDAY">BIRTHDAY</button>
                                                </td>
                                                <td class="col-3">
                                                    <input type="checkbox" id="AGE" name="age" style="display:none;">
                                                    <button id="AGE_BUTTON" class="btn checkbox_button" value="AGE">AGE</button>
                                                </td>
                                                <td class="col-3">
                                                    <input type="checkbox" id="NATIONALITY" name="nationality" style="display:none;">
                                                    <button id="NATIONALITY_BUTTON" class="btn checkbox_button" value="NATIONALITY">NATIONALITY</button>
                                                </td>
                                            </tr>
                                        </table>-->
                                        <div class="row">
                                            <div class="col-3  mb-2">
                                                <input type="checkbox" id="NAME" name="name" style="display:none;">
                                                <button id="NAME_BUTTON" class="btn checkbox_button" value="NAME">NAME</button>
                                            </div>
                                            <div class="col-3  mb-2">
                                                <input type="checkbox" id="SURNAME" name="surname" style="display:none;">
                                                <button id="SURNAME_BUTTON" class="btn checkbox_button" value="SURNAME">SURNAME</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="GIVEN_NAME" name="given_name" style="display:none;">
                                                <button id="GIVEN_NAME_BUTTON" class="btn checkbox_button" value="GIVEN_NAME">GIVEN NAME</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="ENGLISH_NAME" name="english_name" style="display:none;">
                                                <button id="ENGLISH_NAME_BUTTON" class="btn checkbox_button" value="ENGLISH_NAME">ENGLISH NAME</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="CHINESE_NAME" name="chinese_name" style="display:none;">
                                                <button id="CHINESE_NAME_BUTTON" class="btn checkbox_button" value="CHINESE_NAME">CHINESE NAME</button>
                                            </div>
                                            <!--</div>
                                        <div class="row mb-2"> -->
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="GENDER" name="gender" style="display:none;">
                                                <button id="GENDER_BUTTON" class="btn checkbox_button" value="GENDER">GENDER</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="BIRTHDAY" name="birthday" style="display:none;">
                                                <button id="BIRTHDAY_BUTTON" class="btn checkbox_button" value="BIRTHDAY">BIRTHDAY</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="AGE" name="age" style="display:none;">
                                                <button id="AGE_BUTTON" class="btn checkbox_button" value="AGE">AGE</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="NATIONALITY" name="nationality" style="display:none;">
                                                <button id="NATIONALITY_BUTTON" class="btn checkbox_button" value="NATIONALITY">NATIONALITY</button>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="contact_information_table" style="display:none;">
                                        Contact Information
                                        <!--<table>
                                            <tr>
                                                <td class="col-3">
                                                    <input type="checkbox" id="PHONE" name="phone" style="display:none;">
                                                    <button id="PHONE_BUTTON" class="btn checkbox_button" value="PHONE">PHONE</button>
                                                </td>
                                                <td class="col-3">
                                                    <input type="checkbox" id="EMAIL" name="email" style="display:none;">
                                                    <button id="EMAIL_BUTTON" class="btn checkbox_button" value="EMAIL">EMAIL</button>
                                                </td>
                                                <td class="col-3">
                                                    <input type="checkbox" id="ADDRESS" name="address" style="display:none;">
                                                    <button id="ADDRESS_BUTTON" class="btn checkbox_button" name="address" value="ADDRESS">ADDRESS</button>
                                                </td>
                                                <td class="col-3">
                                                    <input type="checkbox" id="INDUSTRY" name="industry" style="display:none;">
                                                    <button id="INDUSTRY_BUTTON" class="btn checkbox_button" value="INDUSTRY">INDUSTRY</button>
                                                </td>
                                            </tr>
                                        </table>-->
                                        <div class="row mb-2">
                                            <div class="col-3">
                                                <input type="checkbox" id="PHONE" name="phone" style="display:none;">
                                                <button id="PHONE_BUTTON" class="btn checkbox_button" value="PHONE">PHONE</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="EMAIL" name="email" style="display:none;">
                                                <button id="EMAIL_BUTTON" class="btn checkbox_button" value="EMAIL">EMAIL</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="ADDRESS" name="address" style="display:none;">
                                                <button id="ADDRESS_BUTTON" class="btn checkbox_button" name="address" value="ADDRESS">ADDRESS</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="INDUSTRY" name="industry" style="display:none;">
                                                <button id="INDUSTRY_BUTTON" class="btn checkbox_button" value="INDUSTRY">INDUSTRY</button>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="work_information_table" style="display:none;">
                                        Work Information
                                        <!--<table>
                                            <tr>
                                                <td class="col-3">
                                                    <input type="checkbox" id="INDUSTRY_2" name="industry_2" style="display:none;">
                                                    <button id="INDUSTRY_2_BUTTON" class="btn checkbox_button" value="industry_2">INDUSTRY</button>
                                                </td>
                                                <td class="col-3">
                                                    <input type="checkbox" id="JOB_TITLE" name="job_title" style="display:none;">
                                                    <button id="JOB_TITLE_BUTTON" class="btn checkbox_button" value="JOB_TITLE">JOB TITLE</button>
                                                </td>
                                                <td class="col-3">
                                                    <input type="checkbox" id="COMPANY" name="company" style="display:none;">
                                                    <button id="COMPANY_BUTTON" class="btn checkbox_button" value="COMPANY">COMPANY</button>
                                                </td>
                                                <td class="col-3">
                                                    <input type="checkbox" id="SALARY" name="salary" style="display:none;">
                                                    <button id="SALARY_BUTTON" class="btn checkbox_button" value="SALARY">SALARY</button>
                                                </td>
                                            </tr>
                                        </table>-->
                                        <div class="row mb-2">
                                            <div class="col-3">
                                                <input type="checkbox" id="INDUSTRY_2" name="industry_2" style="display:none;">
                                                <button id="INDUSTRY_2_BUTTON" class="btn checkbox_button" value="industry_2">INDUSTRY</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="JOB_TITLE" name="job_title" style="display:none;">
                                                <button id="JOB_TITLE_BUTTON" class="btn checkbox_button" value="JOB_TITLE">JOB TITLE</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="COMPANY" name="company" style="display:none;">
                                                <button id="COMPANY_BUTTON" class="btn checkbox_button" value="COMPANY">COMPANY</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="SALARY" name="salary" style="display:none;">
                                                <button id="SALARY_BUTTON" class="btn checkbox_button" value="SALARY">SALARY</button>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="simple_remark" style="display:block">
                                        Simple includes "English Name","Chinese Name","Phone","Email"
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="theme" role="tabpanel" aria-labelledby="contact-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <!--<table>
                                        <tr>
                                            <td class="col-3">
                                                <input type="radio" id="ORDINARY" name="theme" value="ORDINARY" style="display:none">
                                                <button class="btn radio_button w-100 RADIO_ORDINARY" value="ORDINARY">ORDINARY</button>

                                            </td>
                                            <td class="col-3">
                                                <input type="radio" id="MODERN" name="theme" value="MODERN" style="display:none">
                                                <button class="btn radio_button w-100 RADIO_MODERN" value="MODERN">MODERN</button>
                                            </td>
                                            <td class="col-3">
                                                <input type="radio" id="PLAYFUL" name="theme" value="PLAYFUL" style="display:none">
                                                <button class="btn radio_button w-100 RADIO_PLAYFUL" value="PLAYFUL">PLAYFUL</button>
                                            </td>
                                            <td class="col-3">
                                                <input type="radio" id="FREATURED" name="theme" value="FREATURED" style="display:none">
                                                <button class="btn radio_button w-100 RADIO_FREATURED" value="FREATURED">FREATURED</button>
                                            </td>
                                        </tr>
                                    </table> -->
                                    <li>
                                        Title Colour<span style="color:red">*</span><br>
                                        <div class="d-flex">
                                            <input id="label_color" type="color" name="secondary_color" value="<?= $card['secondary_color'] ?>">
                                            <input type="text" class="form-control ml-4 mr-4" id="secondary_color_hex" value="<?= $card['secondary_color'] ?>">
                                        </div>
                                    </li>
                                    <li>
                                        Content Colour<span style="color:red">*</span><br>
                                        <div class="d-flex">
                                            <input id="value_color" type="color" name="pri_color" value="<?= $card['pri_color'] ?>">
                                            <input type="text" class="form-control ml-4 mr-4" id="pri_color_hex" value="<?= $card['pri_color'] ?>">
                                        </div>
                                    </li>
                                    <li>Background Color<span style="color:red">*</span><br>
                                        <div class="d-flex">
                                            <input type="color" id="background_color" name="background_color" value="<?= $card['background_color'] ?>">
                                            <input type="text" class="form-control ml-4 mr-4" id="background_color_hex" value="<?= $card['background_color'] ?>">
                                        </div>
                                        Google's pass will assign the text colour base on the background colour.
                                    </li>
                                    <li>
                                        <div class="">
                                            <div class="" style="width:65px;padding-left:15px;padding-right:15px;">Banner</div>
                                            <div class="col mb-2"><input id="banner_upload" type="file" class="form-control" name="pri_strip" style="height:40px!important;opacity:1!important;" accept=".png"></div>
                                            <div class="col-12">
                                                <div class="mb-2">
                                                    Recommended size: 523px(W) x 175px(H)
                                                </div>
                                                Preview: <img id="banner_preview" class="img_preview_frame" src="<?= base_url($card['strip']) ?>?id=<?= rand(1, 10000000) ?>">
                                                &nbsp;&nbsp;<button class="btn invert_color_button">Invert Color</button>
                                            </div>
                                            <!--<div><img style="height:1em;" class="img-fluid" src="<?= base_url("assets/images/temp2.png") ?>"></div>
                                            <div><button class="btn blue_button" style="border-radius:10px!important;">UPDATE</button></div>-->
                                        </div>
                                        <div class="pt-4">
                                            <h6 style="color:red;">* required</h6>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="header" role="tabpanel" aria-labelledby="contact-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        LOGO
                                        <input type="file" id="logo_upload" name="logo" class="form-control mb-2" style="height:40px!important;opacity:1!important;" accept=".png">
                                        <div class="mb-2">
                                            Recommended size: 100px(W) x 100px(H)
                                        </div>
                                        Preview: <img id="logo_preview_frame" class="img-fluid img_preview_frame" src="<?= base_url($card['logo']) ?>?id=<?= rand(1, 100000000) ?>">
                                        <button class="btn invert_color_button">Invert Color</button>
                                        <!--<div><img style="height:1em;" class="img-fluid" src="<?= base_url("assets/images/temp2.png") ?>"></div>
                                            <div><button class="btn blue_button" style="border-radius:10px!important;">UPDATE</button></div> -->

                                    </li>
                                    <li>
                                        Header Text<span style="color:red">*</span><br>
                                        <div class="d-flex">
                                            <!--<div>
                                                <label class="switch">
                                                    <input type="checkbox" name="logo_text_display" <?= $card['logo_text_display'] == 1 ? "checked" : "" ?> style="display:none;">
                                                    <span class="slider round d-flex" style="align-items:center;">
                                                        <span class="on">ON</span>
                                                        <span class="off">OFF</span>
                                                    </span>
                                                </label>
                                            </div>-->

                                            <input id="header_text" type="text" style="height:100%;" name="logo_text" value="<?= $card['logo_text'] ?>" class="form-control" placeholder="Header Content" required>

                                        </div>
                                        <div class="pt-4">
                                            <h6 style="color:red;">* required</h6>
                                        </div>

                                    </li>

                                    <!--<li>
                                        Header Data<br>
                                      <table>
                                            <tr>
                                                <td class="col-3">
                                                    <input type="radio" id="LEVEL" name="header_data" value="LEVEL" style="display:none">
                                                    <button class="btn radio_button w-100 RADIO_LEVEL" value="LEVEL">LEVEL</button>

                                                </td>
                                                <td class="col-3">
                                                    <input type="radio" id="NICK_NAME" name="header_data" value="NICK_NAME" style="display:none">
                                                    <button class="btn radio_button w-100 RADIO_NICK_NAME" value="NICK_NAME">NICK NAME</button>
                                                </td>
                                                <td class="col-3">
                                                    <input type="radio" id="POINTS" name="header_data" value="POINTS" style="display:none">
                                                    <button class="btn radio_button w-100 RADIO_POINTS" value="POINTS">POINTS</button>
                                                </td>
                                                <td class="col-3">
                                                    <input type="radio" id="STAMPS" name="header_data" value="STAMPS" style="display:none">
                                                    <button class="btn radio_button w-100 RADIO_STAMPS" value="STAMPS">STAMPS</button>
                                                </td>



                                            </tr>
                                        </table>-->
                                    <!--<div class="row mb-2">
                                            <div class="col-6">
                                                <input type="radio" id="LEVEL" name="header_data" value="level" <?= $card['header_data'] == "level" ? "checked" : "" ?> style="display:none">
                                                <button class="btn radio_button w-100 RADIO_LEVEL <?= $card['header_data'] == "level" ? "selected" : "" ?>" value="LEVEL">LEVEL</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="radio" id="NICK_NAME" name="header_data" value="NICK_NAME" style="display:none">
                                                <button class="btn radio_button w-100 RADIO_NICK_NAME" value="NICK_NAME">NICK NAME</button>
                                            </div>
                                            <div class="col-6">
                                                <input type="radio" id="POINTS" name="header_data" value="points" <?= $card['header_data'] == "points" ? "checked" : "" ?> style="display:none">
                                                <button class="btn radio_button w-100 RADIO_POINTS <?= $card['header_data'] == "points" ? "selected" : "" ?>" value="POINTS">POINTS</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="radio" id="STAMPS" name="header_data" value="STAMPS" style="display:none">
                                                <button class="btn radio_button w-100 RADIO_STAMPS" value="STAMPS">STAMPS</button>
                                            </div>
                                        </div>
                                    </li>-->
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="layout" role="tabpanel" aria-labelledby="contact-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">


                                <ul class="d-flex flex-column">
                                    <li>Content Data</li>
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" class="data_trigger" id="front_data_1_status" name="front_data_1_status" <?= $data_config['front_data_1_status'] == 1 ? "checked" : "" ?> style="display:none;">
                                                <span class="slider round d-flex" style="align-items:center;">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" id="front_data_1_label" style="height:100%;" name="front_data_1_label" class="form-control data_trigger" value="<?= $data_config['front_data_1_label'] ?>" placeholder="Label 1">
                                        </div>
                                        <div class="col">
                                            <select style="height:100%;" name="front_data_1_data" class="form-control clear_select">
                                                <?php foreach ($front_fields as $index => $field) { ?>
                                                    <?php if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option <?= $data_config['front_data_1_data'] == $index ? "selected" : "" ?> value="<?= $index ?>"><?= $field ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" class="data_trigger" id="front_data_2_status" name="front_data_2_status" <?= $data_config['front_data_2_status'] == 1 ? "checked" : "" ?> style="display:none;">
                                                <span class="slider round d-flex" style="align-items:center;">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" id="front_data_2_label" style="height:100%;" name="front_data_2_label" class="form-control data_trigger" value="<?= $data_config['front_data_2_label'] ?>" placeholder="Label 2">
                                        </div>
                                        <div class="col">
                                            <select style="height:100%;" name="front_data_2_data" class="form-control clear_select">
                                                <?php foreach ($front_fields as $index => $field) { ?>
                                                    <?php if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option <?= $data_config['front_data_2_data'] == $index ? "selected" : "" ?> value="<?= $index ?>"><?= $field ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" class="data_trigger" id="front_data_3_status" name="front_data_3_status" <?= $data_config['front_data_3_status'] == 1 ? "checked" : "" ?> style="display:none;">
                                                <span class="slider round d-flex" style="align-items:center;">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" id="front_data_3_label" style="height:100%;" name="front_data_3_label" class="form-control data_trigger" value="<?= $data_config['front_data_3_label'] ?>" placeholder="Label 3">
                                        </div>
                                        <div class="col">
                                            <select style="height:100%;" name="front_data_3_data" class="form-control clear_select">
                                                <?php foreach ($front_fields as $index => $field) { ?>
                                                    <?php if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option <?= $data_config['front_data_3_data'] == $index ? "selected" : "" ?> value="<?= $index ?>"><?= $field ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" class="data_trigger" id="front_data_4_status" name="front_data_4_status" <?= $data_config['front_data_4_status'] == 1 ? "checked" : "" ?> style="display:none;">
                                                <span class="slider round d-flex" style="align-items:center;">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" style="height:100%;" id="front_data_4_label" name="front_data_4_label" class="form-control data_trigger" value="<?= $data_config['front_data_4_label'] ?>" placeholder="Label 4">
                                        </div>
                                        <div class="col">
                                            <select style="height:100%;" name="front_data_4_data" class="form-control clear_select">
                                                <?php foreach ($front_fields as $index => $field) { ?>
                                                    <?php if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option <?= $data_config['front_data_4_data'] == $index ? "selected" : "" ?> value="<?= $index ?>"><?= $field ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!--<div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" name="front_data_5_status" <?= $data_config["front_data_5_status"] == 1 ? "checked" : "" ?> style="display:none;">
                                                <span class="slider round d-flex" style="align-items:center;">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" style="height:100%;" name="front_data_5_label" class="form-control " value="<?= $data_config['front_data_5_label'] ?>" placeholder="Label 5">
                                        </div>
                                        <div class="col">
                                            <select style="height:100%;" name="front_data_5_data" class="form-control clear_select">
                                                <?php foreach ($front_fields as $index => $field) { ?>
                                                    <?php if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option <?= $data_config['front_data_5_data'] == $index ? "selected" : "" ?> value="<?= $index ?>"><?= $field ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" name="front_data_6_status" <?= $data_config['front_data_6_status'] == 1 ? "checked" : "" ?> style="display:none;">
                                                <span class="slider round d-flex" style="align-items:center;">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" style="height:100%;" name="front_data_6_label" class="form-control" value="<?= $data_config['front_data_6_label'] ?>" placeholder="Label 6">
                                        </div>
                                        <div class="col">
                                            <select style="height:100%;" name="front_data_6_data" class="form-control clear_select">
                                                <?php foreach ($front_fields as $index => $field) { ?>
                                                    <?php if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option <?= $data_config['front_data_6_data'] == $index ? "selected" : "" ?> value="<?= $index ?>"><?= $field ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                            </select>
                                        </div>
                                    </div>-->
                                    <!-- <li>
                                        Barcode
                                        <div class="d-flex" style="margin:15px 0px;">
                                            <div>
                                                <label class="switch">
                                                    <input type="checkbox" name="barcode_status" style="display:none;">
                                                </label>
                                            </div>
                                        </div>
                                    </li> -->
                                </ul>



                            </div>
                        </div>
                        <div class="tab-pane fade" id="details" role="tabpanel" aria-labelledby="contact-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        <div class="d-flex w-100 align-items-center">
                                            <div class="no_padding" style="width:3rem;">URL<span style="color:red">*</span></div>
                                            <div class="col"><input type="text" id="website_url" class="form-control" name="url" value="<?= $card['url'] ?>" placeholder="Please paste your link" required></div>
                                        </div>
                                    </li>
                                    <li>
                                        About Us<span style="color:red">*</span><br>
                                        <textarea class="form-control" id="about_us_content" name="about_us" placeholder="Please type here" required><?= $card['about_us'] ?></textarea>
                                    </li>
                                    <li>
                                        Terms & Conditions<span style="color:red">*</span>
                                        <textarea class="form-control" id="terms_content" name="terms_conditions" placeholder="Please type here" required><?= $card['terms_conditions'] ?></textarea>
                                        <div class="pt-4">
                                            <h6 style="color:red;">* required</h6>
                                        </div>
                                    </li>


                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="geotag" role="tabpanel" aria-labelledby="contact-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">

                                <!--<form action="/merchant/virtual_passes/post_test" method="post">-->
                                <ul class="d-flex flex-column">
                                    <li>
                                        Location-based Push Notification<i class="fa fa-info-circle tips pl-2" aria-hidden="true"><span class="tips_content" style="z-index:1000;">How to get the coordinate ?<br>Right click at the destination on Google Maps</span></i>
                                        <div class="d-flex" style="margin:15px 0px;">

                                            <div>
                                                <label class="switch">
                                                    <input type="checkbox" name="location_1_status" <?= $location_config["location_1_status"] == 1 ? "checked" : "" ?> style="display:none;">
                                                    <span class="slider round d-flex" style="align-items:center;">
                                                        <span class="on">ON</span>
                                                        <span class="off">OFF</span>
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="col row">
                                                <div class="col">
                                                    <input type="number" step="0.000001" style="height:100%;" name="location_1_lat" class="form-control" value="<?= $location_config["location_1_lat"] ?>" placeholder="Latitude">
                                                </div>
                                                <div class="col">
                                                    <input type="number" step="0.000001" style="height:100%;" name="location_1_lon" class="form-control" value="<?= $location_config['location_1_lon'] ?>" placeholder="Longitude">
                                                </div>
                                                <div class="col-12 mt-2">
                                                    <textarea class="form-control" name="location_1_message" placeholder="Message"><?= $location_config['location_1_message'] ?></textarea>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="d-flex" style="margin:15px 0px;">
                                            <div>
                                                <label class="switch">
                                                    <input type="checkbox" name="location_2_status" <?= $location_config["location_2_status"] == 1 ? "checked" : "" ?> style="display:none;">
                                                    <span class="slider round d-flex" style="align-items:center;">
                                                        <span class="on">ON</span>
                                                        <span class="off">OFF</span>
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="col row">
                                                <div class="col">
                                                    <input type="number" step="0.000001" style="height:100%;" name="location_2_lat" class="form-control" value="<?= $location_config['location_2_lat'] ?>" placeholder="Latitude">
                                                </div>
                                                <div class="col">
                                                    <input type="number" step="0.000001" style="height:100%" name="location_2_lon" class="form-control" value="<?= $location_config['location_2_lon'] ?>" placeholder="Longitude">
                                                </div>
                                                <div class="col-12 mt-2">
                                                    <textarea class="form-control" name="location_2_message" placeholder="Message"><?= $location_config['location_2_message'] ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex" style="margin:15px 0px;">
                                            <div>
                                                <label class="switch">
                                                    <input type="checkbox" name="location_3_status" <?= $location_config["location_3_status"] == 1 ? "checked" : "" ?> style="display:none;">
                                                    <span class="slider round d-flex" style="align-items:center;">
                                                        <span class="on">ON</span>
                                                        <span class="off">OFF</span>
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="col row">
                                                <div class="col">
                                                    <input type="number" step="0.000001" style="height:100%" name="location_3_lat" class="form-control" value="<?= $location_config['location_3_lat'] ?>" placeholder="Latitude">
                                                </div>
                                                <div class="col">
                                                    <input type="number" step="0.000001" style="height:100%;" name="location_3_lon" class="form-control" value="<?= $location_config['location_3_lon'] ?>" placeholder="Longitude">
                                                </div>
                                                <div class="col-12 mt-2">
                                                    <textarea class="form-control" name="location_3_message" placeholder="Message"><?= $location_config['location_3_message'] ?></textarea>
                                                </div>
                                            </div>

                                        </div>
                                    </li>
                                    <li>
                                        <h6 style="color:red">Google: Geofenced notifications are temporarily unavailable for a majority of users while we make platform improvements. We'll completely update this feature in an upcoming release.</h6>
                                    </li>
                                </ul>


                                <!--</form>-->
                            </div>
                        </div>
                        <div class="tab-pane fade" id="level_management" role="tabpanel" aria-labelledby="contact-tab" style="overflow-y:scroll;">
                            <div class="inner_padding ">
                                <!--<form action="/merchant/virtual_passes/post_test" method="post">-->
                                <?php if ($card['type'] == 0) { ?>
                                    <ul id="level_listing" class="d-flex flex-column">
                                        <li>
                                            <div class="mb-4">Level Setting</div>
                                            <div class="d-flex align-items-center justify-content-center">
                                                <div class="d-flex" style="width:12rem;">
                                                    Default Level<span style="color:red">*</span> :
                                                </div>

                                                <input type="hidden" name="default_level" value="<?= $default_level['id'] ?>" readonly>
                                                <input type="text" class="form-control col" id="default_level" name="default_level_value" value="<?= $default_level['value'] ?>" required>
                                                <div class="" style="width:50px;"></div>
                                            </div>
                                        </li>
                                        <?php $i = 1; ?>
                                        <?php foreach ($level_setting as $setting) { ?>
                                            <li class="d-flex" id="level_<?= $i ?>">
                                                <div class="d-flex align-items-center justify-content-center" style="width:12rem;">
                                                    Optional :
                                                </div>
                                                <input type="hidden" name="level[exist_record][]" value="true">
                                                <input type="hidden" name="level[level_id][]" value="<?= $setting['id'] ?>">
                                                <input type="text" class="form-control col" name="level[value][]" value="<?= $setting['value'] ?>">
                                                <a data-exist="true" data-row_id="<?= $i ?>" data-id="<?= $setting['id'] ?>" class="delete_level d-flex align-items-center justify-content-center" style="width:50px;cursor:pointer;"><i class="fa fa-times" aria-hidden="true"></i></a>
                                            </li>
                                            <?php $i++; ?>
                                        <?php } ?>
                                    </ul>
                                    <ul class="d-flex flex-column">
                                        <li class="d-flex">
                                            <a class="btn text-center " id="add_level_row" style="width:5rem;"><i class="fa fa-plus m-0" aria-hidden="true"></i></a>
                                        </li>
                                        <li class="d-flex">
                                            <h6 style="color:red"><span style="color:red">*</span> required</h6>
                                        </li>
                                    </ul>

                                <?php } else { ?>
                                    <div class="d-flex align-items-center justify-content-center pt-4 pb-4 h-100 w-100">
                                        <h4>Disabled</h4>
                                    </div>
                                <?php } ?>


                                <input type="hidden" name="pass_id" value="<?= $card['id'] ?>">
                                <!--<button type="submit">Submit</button>-->
                                <!--</form>-->
                            </div>
                        </div>
                        <div class="tab-pane fade" id="export" role="tabpanel" aria-labelledby="contact-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <?php if ($card['type'] == 0) { ?>
                                    <ul class="d-flex flex-column">
                                        <li>
                                            Generated Application URL<span id="copy_alert" class="text-danger" style="display:none;">&nbsp;&nbsp;Copied !</span>
                                            <br>
                                            <div style="display:flex;width:100%;">
                                                <input id="generated_url" readonly type="text" class="form-control" value="<?= base_url("registration/" . $card['id']) ?>"><button id="copy_button" class="btn blue_button" style="padding-left:15px;padding-right:15px;border-radius:5px!important;margin-left:15px;">COPY</button>
                                            </div>
                                        </li>
                                        <li>
                                            Generated Application QR Code
                                            <div class="row" style="max-width: fit-content;">

                                                <div class="col-12">
                                                    <img class="img-fluid w-100" src="<?= base_url("public/images/uploads/membership/" . $card['id'] . "/Merged_QR.png") ?>">
                                                    <div class="mt-2">
                                                        <a href="<?= base_url('registration/' . $card['id']) ?>"><?= base_url("registration/" . $card['id']) ?></a>
                                                    </div>
                                                    <h6>For iOS devices,please open with Safari</h6>
                                                    <h6>Please open with Safari or click "download" for installation</h6>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                <?php } else { ?>
                                    <ul class="d-flex flex-column">
                                        <li>
                                            Ref key<span id="ref_key_alert" class="text-danger" style="display:none;">&nbsp;&nbsp;Copied !</span>
                                            <br>
                                            <div class="d-flex">
                                                <input type="text" id="ref_key" class="form-control" value="<?= $card['ref_key'] ?>" readonly>
                                                <button id="copy_ref_key_button" class="btn blue_button" style="padding-left:15px;padding-right:15px;border-radius:5px!important;margin-left:15px;">COPY</button>
                                            </div>
                                        </li>
                                    </ul>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="help" role="tabpanel" aria-labelledby="home-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>Got some problems? Watch the tutorial!<br>
                                        <video controls style="width:100%;height:auto;">
                                            <source src="<?= base_url("/assets/tutorial_video/O2OC3C.m4v") ?>" type="video/mp4">
                                        </video>
                                    </li>
                                    <li>Problem not solved? <a href="#">Contact Us</a></li>

                                </ul>

                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>


<script src="<?= base_url("assets/js/virtual_pass_editor.js") ?>"></script>
<script src="<?= base_url("assets/js/virtual_pass_editor_update.js") ?>"></script>
<script>
    var current_page = "edit";
    var adj_count = (<?= count($level_setting) ?> + 1);
    var base_url = "<?= base_url() ?>";
    var pass_id = "<?= $card['id'] ?>";
    var ori_logo = "<?= base_url($card['logo']) ?>";
    var ori_icon = "<?= base_url($card['icon']) ?>";
    var ori_banner = "<?= base_url($card['strip']) ?>";
    $(document).ready(function() {
        var form_config = <?= $form_config_json ?>;
        console.log(form_config);
        for (var i in form_config) {
            if (form_config[i] == 1) {
                $("#" + i.toUpperCase() + "_BUTTON").click();
            }
            console.log(form_config[i]);
        }

        var pass_id = <?= $card['id'] ?>;
        var data_requirement = "<?= $card['data_requirement'] ?>";
        var init_value_color = "<?= $card['pri_color'] ?>";
        $("#value_color").trigger("input");
        var init_label_color = "<?= $card['secondary_color'] ?>";
        $("#label_color").trigger("input");
        var init_background_color = "<?= $card['background_color'] ?>";
        update_color(init_background_color);
        $("#header_text").trigger("input");
        google_data_row_update();
        apple_data_row_update();
        $(".apple_phone_label").css("color", init_value_color);
        $(".apple_phone_value").css("color", init_label_color);
        switch (data_requirement) {
            case "simple":
                $(".button_simple").click();
                break;
            case "informative":
                $(".button_informative").click();
                break;
        }
        delete_button_init();
    })
</script>