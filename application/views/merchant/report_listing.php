<div class="page-body">
    <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5"><?= $page ?></h4>
    <?php foreach ($reports as $index => $pass) { ?>
        <div class="pb-5">
            <h3><?= $index ?></h3>
            <hr>
            <?php foreach ($pass as $name => $report) { ?>
              
                <?php foreach ($report as $year => $res) { ?>
                    <div class="pb-4">
                        <h4><?= $year ?></h4>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Month</th>
                                    <th class="text-center">Download</th>
                                </tr>
                            </thead>
                            <?php foreach ($res as $r) { ?>
                                <tr>
                                    <td width="80%"><?= $r['month'] ?></td>
                                    <td class="text-center" width="20%"><a target="_blank" href="<?= $r['path'] ?>"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    <?php } ?>

</div>