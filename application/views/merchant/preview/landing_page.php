<div class="page-body" style="text-align:center;">
    <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5"><?= $page ?></h4>
    <div class="row" style="justify-content:center;">
        <div class="col-10">
            <h2 class="text-center">Coming Soon</h2>
            <img class="img-fluid" src="<?= base_url("assets/images/temp_landing_page.png") ?>">
        </div>
    </div>

</div>