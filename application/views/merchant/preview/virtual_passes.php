<div class="page-body">
    <div class="row">
        <div class="col-12 col-md-3">
            <a class="item" href="/merchant_system/virtual_passes_edit">
                <div class="item_container">
                    <div class="box">

                        <span style="font-size:8rem">+</span>

                    </div>
                    <div class="name_container">
                        Create New
                    </div>
                </div>
            </a>
        </div>
        <div class="col-12 col-md-3">
            <a class="item" href="/merchant_system/virtual_passes_edit">
                <div class="item_container">
                    <div class="box">

                        <img class="img-fluid" src="<?= base_url("assets/images/temp.png") ?>">

                    </div>
                    <div class="name_container">
                        Membership Card&nbsp;<i class="fas fa-pencil-alt"></i>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-12 col-md-3">
            <a class="item" href="/merchant_system/virtual_passes_edit">
                <div class="item_container">
                    <div class="box">

                        <img class="img-fluid" src="<?= base_url("assets/images/temp.png") ?>">

                    </div>
                    <div class="name_container">
                        Coupon&nbsp;<i class="fas fa-pencil-alt"></i>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-12 col-md-3">
            <a class="item" href="/merchant_system/virtual_passes_edit">
                <div class="item_container">
                    <div class="box">

                        <img class="img-fluid" src="<?= base_url("assets/images/temp.png") ?>">

                    </div>
                    <div class="name_container">
                        VIP Entry Ticket&nbsp;<i class="fas fa-pencil-alt"></i>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-12 col-md-3">
            <a class="item" href="/merchant_system/virtual_passes_edit">
                <div class="item_container">
                    <div class="box">

                        <img class="img-fluid" src="<?= base_url("assets/images/temp.png") ?>">

                    </div>
                    <div class="name_container">
                        Stamp Card&nbsp;<i class="fas fa-pencil-alt"></i>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-12 col-md-3">
            <a class="item" href="/merchant_system/virtual_passes_edit">
                <div class="item_container">
                    <div class="box">

                        <img class="img-fluid" src="<?= base_url("assets/images/temp.png") ?>">

                    </div>
                    <div class="name_container">
                        Company Card&nbsp;<i class="fas fa-pencil-alt"></i>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>