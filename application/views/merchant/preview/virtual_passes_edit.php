<div class="page-body">
    <div class="row">
        <div class="col-12 col-md-4">
            <img class="img-fluid w-100" src="<?= base_url("assets/images/fake_phone.png") ?>">
        </div>
        <div class="col-12 col-md-8">
            <div class="editor">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item first-block">
                        CREATOR
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" id="trial-tab" data-toggle="tab" href="#trial" role="tab" aria-controls="home" aria-selected="true">Trial</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="home-tab" data-toggle="tab" href="#setting" role="tab" aria-controls="home" aria-selected="true"><img class="img-fluid" src="<?= base_url("assets/images/merchant/setting.png") ?>">&nbsp;<span class="tab_word">Setting</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/form.png") ?>">&nbsp;<span class="tab_word">Form</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#theme" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/theme.png") ?>">&nbsp;<span class="tab_word">Theme</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#header" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/header.png") ?>">&nbsp;<span class="tab_word">Header</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#layout" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/layout.png") ?>">&nbsp;<span class="tab_word">Layout</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#details" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/detail.png") ?>">&nbsp;<span class="tab_word">Details</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#geotag" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/geotag.png") ?>">&nbsp;<span class="tab_word">Geotag</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="export-tab" data-toggle="tab" href="#export" role="tab" aria-controls="contact" aria-selected="false"><img clsas="img-fluid" src="<?= base_url("assets/images/merchant/export.png") ?>">&nbsp;<span class="tab_word">Export</span></a>
                    </li>
                    <li class="nav-item ml-auto end-block">
                        <a class="nav-link" data-toggle="tab" href="#help" aria-controls="contact" aria-selected="false" style="padding:0px!important;border-radius:50px important;display:flex!important;align-items:center;background-color:transparent!important;border:none!important;">
                            <i class="fas fa-list"></i>&nbsp;<span class="tab_word" style="color:white;">Help</span>
                        </a>
                    </li>

                </ul>
                <div class="tab-content" id="myTabContent">
                    <button id="next_button" class="btn blue_button" style="border-radius:15px 0px 15px 0px!important;">NEXT</button>
                    <button id="export_button" class="btn blue_button" style="border-radius:15px 0px 15px 0px!important;">EXPORT</button>
                    <div class="tab-pane fade show active" id="trial" role="tabpanel" aria-labelledby="home-tab">
                        <div class="inner_padding">
                            <form action="/merchant_system/submit_form" method="post" enctype="multipart/form-data">
                                <ul class="d-flex flex-column">
                                    <li>
                                        Logo<br>
                                        <div class="row">
                                            <div class="col-12 col-md-6">
                                                Logo image
                                                <input type="file" name="logo" class="form-control" style="height:40px!important;">

                                            </div>
                                            <div class="col-12 col-md-6">
                                                Logo Text
                                                <input type="text" name="logo_text" class="form-control">
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        Icon<br>
                                        <input type="file" name="icon" class="form-control" style="height:40px!important;">
                                    </li>
                                    <li>
                                        Description<br>
                                        <input type="text" name="description" class="form-control">
                                    </li>
                                    <li>
                                        Primary field<br>
                                        <div class="form-group row">
                                            <div class="col-12 col-md-6">
                                                <label class="control-label">Label</label>
                                                <input type="text" class="form-control" name="pri_label" placeholder="Please enter the name of the label">
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <label class="control-label">value</label>
                                                <input type="text" class="form-control" name="pri_value" placeholder="Please enther the value ">
                                            </div>
                                        </div>
                                        <div>
                                            <label class="control-label">Strip</label>
                                            <input type="file" class="form-control" name="pri_strip" style="height:40px!important;">
                                        </div>
                                    </li>
                                    <li>
                                        Secondary fields<br>
                                        <div class="row">
                                            <div class="col-12 col-md-6">
                                                <label class="control-label">Label</label>
                                                <input type="text" class="form-control" name="sec_label_1" placeholder="Please enter the name of the label">
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <label class="control-label">Value</label>
                                                <input type="text" class="form-control" name="sec_value_1" placeholder="Please enter the value">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-6">
                                                <label class="control-label">Label</label>
                                                <input type="text" class="form-control" name="sec_label_2" placeholder="Please enter the name of the label">
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <label class="control-label">Value</label>
                                                <input type="text" class="form-control" name="sec_value_2" placeholder="Please enter the value">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-6">
                                                <label class="control-label">Label</label>
                                                <input type="text" class="form-control" name="sec_label_3" placeholder="Please enter the name of the label">
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <label class="control-label">Value</label>
                                                <input type="text" class="form-control" name="sec_value_3" placeholder="Please enter the value">
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        Colour
                                        <div class="row">
                                            <div class="col-12 col-md-3 d-flex" style="align-items:center;">
                                                Primary color:&nbsp;&nbsp;&nbsp;
                                                <input type="color" name="pri_color">
                                            </div>
                                            <div class="col-12 col-md-3 d-flex" style="align-items:center;">
                                                Secondary color:&nbsp;&nbsp;&nbsp;
                                                <input type="color" name="secondary_color">
                                            </div>
                                            <div class="col-12 col-md-3 d-flex" style="align-items:center;">
                                                Background color:&nbsp;&nbsp;&nbsp;
                                                <input type="color" name="background_color">
                                            </div>
                                            <div class="col-12 col-md-3 d-flex" style="align-items:center;">
                                                Primary field color:&nbsp;&nbsp;&nbsp;
                                                <input type="color" name="stripColor">
                                            </div>

                                        </div>
                                    </li>
                                    <li>
                                        <div style="display:flex;width:100%;align-items:center;">
                                            <div class="no_padding" style="width:65px">URL</div>
                                            <div class="col"><input type="text" name="url" class="form-control" placeholder="Please paste your link"></div>
                                        </div>
                                    </li>
                                    <li>
                                        About Us<br>
                                        <textarea class="form-control" name="about_us" placeholder="Please type here"></textarea>
                                    </li>
                                    <li>
                                        Terms & Conditions
                                        <textarea class="form-control" name="terms_conditions" placeholder="Please type here"></textarea>
                                    </li>
                                    <button type="submit" id="submit_button" class="btn submit_button" style="position:absolute;right:0;bottom:0;background-color:#4a5ba1;color:white;border-radius:15px 0px 15px 0px!important;padding:10px 15px;">SUBMIT</button>

                                </ul>
                            </form>


                        </div>
                    </div>
                    <div class="tab-pane fade " id="setting" role="tabpanel" aria-labelledby="home-tab">
                        <div class="inner_padding">
                            <ul class="d-flex flex-column">
                                <li>
                                    Type of Virtual Passes &nbsp;&nbsp;&nbsp;<select>
                                        <option>Point Card</option>
                                        <!--<option>namecard</option>
                                        <option>stamp card</option>
                                        <option>point card</option>
                                        <option>ticket</option>
                                        <option>coupon</option>-->
                                    </select>
                                </li>
                                <li>
                                    Data Required<br>
                                    <input type="radio" id="data_requirement_simple" name="data_requirement" value="simple" style="display:none">
                                    <input type="radio" id="data_requirement_informative" name="data_requirement" value="informative" style="display:none">
                                    <button class="btn radio_button button_simple" value="simple">SIMPLE</button>
                                    <button class="btn radio_button button_informative" value="informative">INFORMATIVE</button>
                                </li>
                                <li>
                                    <div style="display:flex;width:100%;">
                                        <div class="no_padding" style="width:65px">Headline</div>
                                        <div class="col"><input type="text" class="form-control"></div>
                                    </div>
                                </li>
                                <li>
                                    <div style="display:flex">
                                        <div class="no_padding" style="width:65px;">Banner</div>
                                        <div><img style="height:1em;" class="img-fluid" src="<?= base_url("assets/images/temp2.png") ?>"></div>
                                        <div><button class="btn blue_button" style="border-radius:10px!important;">UPDATE</button></div>
                                    </div>
                                </li>
                                <li>
                                    <div style="display:flex;width:100%">
                                        <div class="no_padding" style="width:65px;">Message</div>
                                        <div class="col">
                                            <textarea class="form-control" placeholder="Please type"></textarea>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div style="display:flex;width:100%">
                                        <div class="no_padding" style="width:65px;">Botton</div>
                                        <div class="col">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="inner_padding">
                            <ul class="d-flex flex-column">
                                <li>Personal Information<br>
                                    <table>
                                        <tr>
                                            <td class="col-3">
                                                <input type="checkbox" id="SURNAME" style="display:none;">
                                                <button class="btn checkbox_button" value="SURNAME">SURNAME</button>
                                            </td>
                                            <td class="col-3">
                                                <input type="checkbox" id="GIVEN_NAME" style="display:none;">
                                                <button class="btn checkbox_button" value="GIVEN_NAME">GIVEN NAME</button>
                                            </td>
                                            <td class="col-3">
                                                <input type="checkbox" id="ENGLISH_NAME" style="display:none;">
                                                <button class="btn checkbox_button" value="ENGLISH_NAME">ENGLISH NAME</button>
                                            </td>
                                            <td class="col-3">
                                                <input type="checkbox" id="CHINESE_NAME" style="display:none;">
                                                <button class="btn checkbox_button" value="CHINESE_NAME">CHINESE NAME</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-3">
                                                <input type="checkbox" id="GENDER" style="display:none;">
                                                <button class="btn checkbox_button" value="GENDER">GENDER</button>
                                            </td>
                                            <td class="col-3">
                                                <input type="checkbox" id="BIRTHDAY" style="display:none;">
                                                <button class="btn checkbox_button" value="BIRTHDAY">BIRTHDAY</button>
                                            </td>
                                            <td class="col-3">
                                                <input type="checkbox" id="AGE" style="display:none;">
                                                <button class="btn checkbox_button" value="AGE">AGE</button>
                                            </td>
                                            <td class="col-3">
                                                <input type="checkbox" id="NATIONALITY" style="display:none;">
                                                <button class="btn checkbox_button" value="NATIONALITY">NATIONALITY</button>
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                                <li>
                                    Contact Information
                                    <table>
                                        <tr>
                                            <td class="col-3">
                                                <input type="checkbox" id="PHONE" style="display:none;">
                                                <button class="btn checkbox_button" value="PHONE">PHONE</button>
                                            </td>
                                            <td class="col-3">
                                                <input type="checkbox" id="EMAIL" style="display:none;">
                                                <button class="btn checkbox_button" value="EMAIL">EMAIL</button>
                                            </td>
                                            <td class="col-3">
                                                <input type="checkbox" id="ADDRESS" style="display:none;">
                                                <button class="btn checkbox_button" value="ADDRESS">ADDRESS</button>
                                            </td>
                                            <td class="col-3">
                                                <input type="checkbox" id="INDUSTRY" style="display:none;">
                                                <button class="btn checkbox_button" value="INDUSTRY">INDUSTRY</button>
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                                <li>
                                    Work Information
                                    <table>
                                        <tr>
                                            <td class="col-3">
                                                <input type="checkbox" id="WORK_INDUSTRY" style="display:none;">
                                                <button class="btn checkbox_button" value="WORK_INDUSTRY">INDUSTRY</button>
                                            </td>
                                            <td class="col-3">
                                                <input type="checkbox" id="JOB_TITLE" style="display:none;">
                                                <button class="btn checkbox_button" value="JOB_TITLE">JOB TITLE</button>
                                            </td>
                                            <td class="col-3">
                                                <input type="checkbox" id="COMPANY" style="display:none;">
                                                <button class="btn checkbox_button" value="COMPANY">COMPANY</button>
                                            </td>
                                            <td class="col-3">
                                                <input type="checkbox" id="SALARY" style="display:none;">
                                                <button class="btn checkbox_button" value="SALARY">SALARY</button>
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="theme" role="tabpanel" aria-labelledby="contact-tab">
                        <div class="inner_padding">
                            <ul class="d-flex flex-column">
                                <table>
                                    <tr>
                                        <td class="col-3">
                                            <input type="radio" id="ORDINARY" name="theme" value="ORDINARY" style="display:none">
                                            <button class="btn radio_button w-100 RADIO_ORDINARY" value="ORDINARY">ORDINARY</button>

                                        </td>
                                        <td class="col-3">
                                            <input type="radio" id="MODERN" name="theme" value="MODERN" style="display:none">
                                            <button class="btn radio_button w-100 RADIO_MODERN" value="MODERN">MODERN</button>
                                        </td>
                                        <td class="col-3">
                                            <input type="radio" id="PLAYFUL" name="theme" value="PLAYFUL" style="display:none">
                                            <button class="btn radio_button w-100 RADIO_PLAYFUL" value="PLAYFUL">PLAYFUL</button>
                                        </td>
                                        <td class="col-3">
                                            <input type="radio" id="FREATURED" name="theme" value="FREATURED" style="display:none">
                                            <button class="btn radio_button w-100 RADIO_FREATURED" value="FREATURED">FREATURED</button>
                                        </td>
                                    </tr>
                                </table>
                                <li>
                                    Colour Scheme<br>
                                    <ul class="nav" style="height:unset!important;">
                                        <li>
                                            <div class="color_ball d-flex" style="background-color:rgb(200,80,64)"></div>
                                        </li>
                                        <li>
                                            <div class="color_ball d-flex" style="background-color:rgb(234,99,54)"></div>
                                        </li>
                                        <li>
                                            <div class="color_ball d-flex" style="background-color:rgb(239,155,57)"></div>
                                        </li>
                                        <li>
                                            <div class="color_ball d-flex" style="background-color:rgb(103,170,91)"></div>
                                        </li>
                                        <li>
                                            <div class="color_ball d-flex" style="background-color:rgb(66,146,135)"></div>
                                        </li>
                                        <li>
                                            <div class="color_ball d-flex" style="background-color:rgb(84,182,207)"></div>
                                        </li>
                                        <li>
                                            <div class="color_ball d-flex" style="background-color:rgb(76,164,238)"></div>
                                        </li>
                                        <li>
                                            <div class="color_ball d-flex" style="background-color:rgb(84,128,233)"></div>
                                        </li>
                                        <li>
                                            <div class="color_ball d-flex" style="background-color:rgb(67,80,174)"></div>
                                        </li>
                                        <li>
                                            <div class="color_ball d-flex" style="background-color:rgb(34,51,102)"></div>
                                        </li>
                                        <li>
                                            <div class="color_ball d-flex" style="background-color:rgb(101,123,134)"></div>
                                        </li>
                                        <li>
                                            <div class="color_ball d-flex" style="background-color:rgb(157,157,157)"></div>
                                        </li>
                                        <li>
                                            <div class="color_ball d-flex" style="background-color:rgb(96,70,43)"></div>
                                        </li>
                                        <li>
                                            <div class="color_ball d-flex" style="background-color:rgb(128,95,64)"></div>
                                        </li>
                                        <li>
                                            <div class="color_ball d-flex" style="background-color:rgb(186,146,121)"></div>
                                        </li>
                                        <li>
                                            <div class="color_ball_add d-flex" data-value="add" style="background-color:rgb(200,200,200);color:black;justify-content:center;align-items:center;">+</div>
                                        </li>
                                    </ul>
                                </li>
                                <li>Background<br>
                                    <ul class="nav" style="height:unset!important;">
                                        <li>
                                            <div class="theme_box" style="background-color:white;border:1px solid #d5d5d5">None</div>
                                        </li>
                                        <li>
                                            <div class="theme_box" style="background-color:#4a5ba1;color:white;border:1px solid #d5d5d5">Cyber</div>
                                        </li>
                                        <li>
                                            <div class="theme_box" style="background-color:#606060;color:white;border:1px solid #d5d5d5">Stripe</div>
                                        </li>
                                        <li>
                                            <div class="theme_box" style="background-color:rgb(239,155,57);color:white;border:1px solid #d5d5d5">Passion</div>
                                        </li>
                                        <li>
                                            <div class="theme_box" style="background-color:rgb(76,164,238);color:white;border:1px solid #d5d5d5">Ocean</div>
                                        </li>
                                        <li>
                                            <div class="theme_box" style="background-color:#d12e2e;color:white;border:1px solid #d5d5d5">Fire</div>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <div style="display:flex">
                                        <div class="no_padding" style="width:65px;">Banner</div>
                                        <div><img style="height:1em;" class="img-fluid" src="<?= base_url("assets/images/temp2.png") ?>"></div>
                                        <div><button class="btn blue_button" style="border-radius:10px!important;">UPDATE</button></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="header" role="tabpanel" aria-labelledby="contact-tab">
                        <div class="inner_padding">
                            <ul class="d-flex flex-column">
                                Header Colour<br>
                                <ul class="nav" style="height:unset!important;">
                                    <li>
                                        <div class="color_ball d-flex" style="background-color:rgb(200,80,64)"></div>
                                    </li>
                                    <li>
                                        <div class="color_ball d-flex" style="background-color:rgb(234,99,54)"></div>
                                    </li>
                                    <li>
                                        <div class="color_ball d-flex" style="background-color:rgb(239,155,57)"></div>
                                    </li>
                                    <li>
                                        <div class="color_ball d-flex" style="background-color:rgb(103,170,91)"></div>
                                    </li>
                                    <li>
                                        <div class="color_ball d-flex" style="background-color:rgb(66,146,135)"></div>
                                    </li>
                                    <li>
                                        <div class="color_ball d-flex" style="background-color:rgb(84,182,207)"></div>
                                    </li>
                                    <li>
                                        <div class="color_ball d-flex" style="background-color:rgb(76,164,238)"></div>
                                    </li>
                                    <li>
                                        <div class="color_ball d-flex" style="background-color:rgb(84,128,233)"></div>
                                    </li>
                                    <li>
                                        <div class="color_ball d-flex" style="background-color:rgb(67,80,174)"></div>
                                    </li>
                                    <li>
                                        <div class="color_ball d-flex" style="background-color:rgb(34,51,102)"></div>
                                    </li>
                                    <li>
                                        <div class="color_ball d-flex" style="background-color:rgb(101,123,134)"></div>
                                    </li>
                                    <li>
                                        <div class="color_ball d-flex" style="background-color:rgb(157,157,157)"></div>
                                    </li>
                                    <li>
                                        <div class="color_ball d-flex" style="background-color:rgb(96,70,43)"></div>
                                    </li>
                                    <li>
                                        <div class="color_ball d-flex" style="background-color:rgb(128,95,64)"></div>
                                    </li>
                                    <li>
                                        <div class="color_ball d-flex" style="background-color:rgb(186,146,121)"></div>
                                    </li>
                                    <li>
                                        <div class="color_ball_add d-flex" data-value="add" style="background-color:rgb(200,200,200);color:black;justify-content:center;align-items:center;">+</div>
                                    </li>
                                </ul>
                                <li>
                                    <div style="display:flex">
                                        <div class="no_padding" style="width:65px;">LOGO</div>
                                        <div><img style="height:1em;" class="img-fluid" src="<?= base_url("assets/images/temp2.png") ?>"></div>
                                        <div><button class="btn blue_button" style="border-radius:10px!important;">UPDATE</button></div>
                                    </div>
                                </li>
                                <li>
                                    Header Text<br>
                                    <div class="d-flex">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" checked style="display:none;">
                                                <span class="slider round d-flex" style="align-items:center;">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" style="height:100%;" class="form-control">
                                        </div>
                                        <div class="col">
                                            <select style="height:100%;" class="form-control clear_select">
                                                <option>--Select Font--</option>
                                            </select>
                                        </div>
                                        <div>
                                            <input type="color" id="color_picker_body" style="visibility:hidden;height:0px;width:0px;">
                                        </div>
                                        <div>
                                            <div id="color_picker"></div>

                                        </div>
                                    </div>
                                </li>
                                <li>
                                    Header Data<br>
                                    <table>
                                        <tr>
                                            <td class="col-3">
                                                <input type="radio" id="LEVEL" name="header_data" value="LEVEL" style="display:none">
                                                <button class="btn radio_button w-100 RADIO_LEVEL" value="LEVEL">LEVEL</button>

                                            </td>
                                            <td class="col-3">
                                                <input type="radio" id="NICK_NAME" name="header_data" value="NICK_NAME" style="display:none">
                                                <button class="btn radio_button w-100 RADIO_NICK_NAME" value="NICK_NAME">NICK NAME</button>
                                            </td>
                                            <td class="col-3">
                                                <input type="radio" id="POINTS" name="header_data" value="POINTS" style="display:none">
                                                <button class="btn radio_button w-100 RADIO_POINTS" value="POINTS">POINTS</button>
                                            </td>
                                            <td class="col-3">
                                                <input type="radio" id="STAMPS" name="header_data" value="STAMPS" style="display:none">
                                                <button class="btn radio_button w-100 RADIO_STAMPS" value="STAMPS">STAMPS</button>
                                            </td>



                                        </tr>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="layout" role="tabpanel" aria-labelledby="contact-tab">
                        <div class="inner_padding">
                            <ul class="d-flex flex-column">
                                <li>Content Data</li>
                                <div class="d-flex" style="margin:15px 0px;">
                                    <div>
                                        <label class="switch">
                                            <input type="checkbox" checked style="display:none;">
                                            <span class="slider round d-flex" style="align-items:center;">
                                                <span class="on">ON</span>
                                                <span class="off">OFF</span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="col">
                                        <input type="text" style="height:100%;" class="form-control" placeholder="NAME">
                                    </div>
                                    <div class="col">
                                        <select style="height:100%;" class="form-control clear_select">
                                            <option>--ENGLISH NAME--</option>
                                        </select>
                                    </div>
                                    <div>
                                        <input type="color" id="" class="color_picker_selector" style="visibility:hidden;height:0px;width:0px;">
                                    </div>
                                    <div>
                                        <div class="color_picker"></div>

                                    </div>
                                    <div>
                                        <input type="color" id="" class="color_picker_selector" style="visibility:hidden;height:0px;width:0px;">
                                    </div>
                                    <div>
                                        <div class="color_picker"></div>

                                    </div>
                                </div>
                                <div class="d-flex" style="margin:15px 0px;">
                                    <div>
                                        <label class="switch">
                                            <input type="checkbox" checked style="display:none;">
                                            <span class="slider round d-flex" style="align-items:center;">
                                                <span class="on">ON</span>
                                                <span class="off">OFF</span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="col">
                                        <input type="text" style="height:100%;" class="form-control" placeholder="PROJECTS">
                                    </div>
                                    <div class="col">
                                        <select style="height:100%;" class="form-control clear_select">
                                            <option>--Select Data--</option>
                                        </select>
                                    </div>
                                    <div>
                                        <input type="color" id="" class="color_picker_selector" style="visibility:hidden;height:0px;width:0px;">
                                    </div>
                                    <div>
                                        <div class="color_picker"></div>

                                    </div>
                                    <div>
                                        <input type="color" class="color_picker_selector" id="" style="visibility:hidden;height:0px;width:0px;">
                                    </div>
                                    <div>
                                        <div class="color_picker"></div>

                                    </div>
                                </div>
                                <div class="d-flex" style="margin:15px 0px;">
                                    <div>
                                        <label class="switch">
                                            <input type="checkbox" checked style="display:none;">
                                            <span class="slider round d-flex" style="align-items:center;">
                                                <span class="on">ON</span>
                                                <span class="off">OFF</span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="col">
                                        <input type="text" style="height:100%;" class="form-control" placeholder="DATE OF BIRTH">
                                    </div>
                                    <div class="col">
                                        <select style="height:100%;" class="form-control clear_select">
                                            <option>--Select Data--</option>
                                        </select>
                                    </div>
                                    <div>
                                        <input type="color" id="" class="color_picker_selector" style="visibility:hidden;height:0px;width:0px;">
                                    </div>
                                    <div>
                                        <div class="color_picker"></div>

                                    </div>
                                    <div>
                                        <input type="color" id="" class="color_picker_selector" style="visibility:hidden;height:0px;width:0px;">
                                    </div>
                                    <div>
                                        <div class="color_picker"></div>

                                    </div>
                                </div>
                                <div class="d-flex" style="margin:15px 0px;">
                                    <div>
                                        <label class="switch">
                                            <input type="checkbox" checked style="display:none;">
                                            <span class="slider round d-flex" style="align-items:center;">
                                                <span class="on">ON</span>
                                                <span class="off">OFF</span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="col">
                                        <input type="text" style="height:100%;" class="form-control" placeholder="">
                                    </div>
                                    <div class="col">
                                        <select style="height:100%;" class="form-control clear_select">
                                            <option>--Select Data--</option>
                                        </select>
                                    </div>
                                    <div>
                                        <input type="color" id="" class="color_picker_selector" style="visibility:hidden;height:0px;width:0px;">
                                    </div>
                                    <div>
                                        <div class="color_picker"></div>

                                    </div>
                                    <div>
                                        <input type="color" id="" class="color_picker_selector" style="visibility:hidden;height:0px;width:0px;">
                                    </div>
                                    <div>
                                        <div class="color_picker"></div>

                                    </div>
                                </div>
                                <div class="d-flex" style="margin:15px 0px;">
                                    <div>
                                        <label class="switch">
                                            <input type="checkbox" checked style="display:none;">
                                            <span class="slider round d-flex" style="align-items:center;">
                                                <span class="on">ON</span>
                                                <span class="off">OFF</span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="col">
                                        <input type="text" style="height:100%;" class="form-control" placeholder="MEMBERSHIP">
                                    </div>
                                    <div class="col">
                                        <select style="height:100%;" class="form-control clear_select">
                                            <option>--Select Data--</option>
                                        </select>
                                    </div>
                                    <div>
                                        <input type="color" id="" class="color_picker_selector" style="visibility:hidden;height:0px;width:0px;">
                                    </div>
                                    <div>
                                        <div class="color_picker"></div>

                                    </div>
                                    <div>
                                        <input type="color" id="" class="color_picker_selector" style="visibility:hidden;height:0px;width:0px;">
                                    </div>
                                    <div>
                                        <div class="color_picker"></div>

                                    </div>
                                </div>
                                <div class="d-flex" style="margin:15px 0px;">
                                    <div>
                                        <label class="switch">
                                            <input type="checkbox" checked style="display:none;">
                                            <span class="slider round d-flex" style="align-items:center;">
                                                <span class="on">ON</span>
                                                <span class="off">OFF</span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="col">
                                        <input type="text" style="height:100%;" class="form-control" placeholder="VALID UNTIL">
                                    </div>
                                    <div class="col">
                                        <select style="height:100%;" class="form-control clear_select">
                                            <option>--Select Data--</option>
                                        </select>
                                    </div>
                                    <div>
                                        <input type="color" id="" class="color_picker_selector" style="visibility:hidden;height:0px;width:0px;">
                                    </div>
                                    <div>
                                        <div class="color_picker"></div>

                                    </div>
                                    <div>
                                        <input type="color" id="" class="color_picker_selector" style="visibility:hidden;height:0px;width:0px;">
                                    </div>
                                    <div>
                                        <div id="tester" class="color_picker"></div>

                                    </div>
                                </div>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="details" role="tabpanel" aria-labelledby="contact-tab">
                        <div class="inner_padding">
                            <ul class="d-flex flex-column">
                                <li>
                                    <div style="display:flex;width:100%;">
                                        <div class="no_padding" style="width:65px">URL</div>
                                        <div class="col"><input type="text" class="form-control" placeholder="Please paste your link"></div>
                                    </div>
                                </li>
                                <li>
                                    Contact Us<br>
                                    <textarea class="form-control" placeholder="Please type here"></textarea>
                                </li>
                                <li>
                                    Terms & Conditions
                                    <textarea class="form-control" placeholder="Please type here"></textarea>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="geotag" role="tabpanel" aria-labelledby="contact-tab">
                        <div class="inner_padding">
                            <ul class="d-flex flex-column">
                                <li>
                                    Location-based Push Notification
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" checked style="display:none;">
                                                <span class="slider round d-flex" style="align-items:center;">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" style="height:100%;" class="form-control" placeholder="Address 1">
                                        </div>
                                    </div>
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" checked style="display:none;">
                                                <span class="slider round d-flex" style="align-items:center;">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" style="height:100%;" class="form-control" placeholder="Address 2">
                                        </div>
                                    </div>
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" checked style="display:none;">
                                                <span class="slider round d-flex" style="align-items:center;">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="text" style="height:100%;" class="form-control" placeholder="Address 3">
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="export" role="tabpanel" aria-labelledby="contact-tab">
                        <div class="inner_padding">
                            <ul class="d-flex flex-column">
                                <li>
                                    <div style="display:flex;width:100%;">
                                        <div class="no_padding">Membership Number starting from</div>
                                        <div class="col"><input type="text" class="form-control" placeholder=""></div>
                                    </div>
                                </li>
                                <li>
                                    Generate URL <i class="fas fa-sync-alt"></i> <br>
                                    <div style="display:flex;width:100%;">
                                        <input type="text" class="form-control"><button class="btn blue_button" style="padding-left:15px;padding-right:15px;border-radius:5px!important;margin-left:15px;">COPY</button>
                                    </div>
                                </li>
                                <li>
                                    Generate QR Code <i class="fas fa-sync-alt"></i> <br>
                                    <div class="row">
                                        <div class="col-12 col-md-6">
                                            <div style="display:flex;width:100%;margin:15px 0px;">
                                                <div class="col">
                                                    <select class="form-control clear_select">
                                                        <option>
                                                            --Slect Body Shape--
                                                        </option>
                                                    </select>
                                                </div>
                                                <div>
                                                    <input type="color" id="" class="color_picker_selector" style="visibility:hidden;height:0px;width:0px;">
                                                </div>
                                                <div>
                                                    <div class="color_picker"></div>

                                                </div>
                                            </div>
                                            <div style="display:flex;width:100%;margin:15px 0px;">
                                                <div class="col">
                                                    <select class="form-control clear_select">
                                                        <option>
                                                            --Slect Eye Frame--
                                                        </option>
                                                    </select>
                                                </div>
                                                <div>
                                                    <input type="color" id="" class="color_picker_selector" style="visibility:hidden;height:0px;width:0px;">
                                                </div>
                                                <div>
                                                    <div class="color_picker"></div>

                                                </div>
                                            </div>
                                            <div style="display:flex;width:100%;margin:15px 0px;">
                                                <div class="col">
                                                    <select class="form-control clear_select">
                                                        <option>
                                                            --Slect Eye Ball--
                                                        </option>
                                                    </select>
                                                </div>
                                                <div>
                                                    <input type="color" id="" class="color_picker_selector" style="visibility:hidden;height:0px;width:0px;">
                                                </div>
                                                <div>
                                                    <div class="color_picker"></div>

                                                </div>
                                            </div>
                                            <div>
                                                <img class="img-fluid" style="max-width:50px;" src="<?= base_url("assets/images/temp2.png") ?>">&nbsp;&nbsp;<button class="btn blue_button" style="border-radius:10px!important;">UPLOAD ICON</button>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="QR_container">
                                                        <img class="img-fluid w-100" src="<?= base_url("assets/images/temp_qr_1.png") ?>">
                                                        <h5>
                                                            APPLE WALLET
                                                        </h5>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="QR_container">
                                                        <img class="img-fluid w-100" src="<?= base_url("assets/images/temp_qr_2.png") ?>">
                                                        <h5>
                                                            GOOGLE PAY
                                                        </h5>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="help" role="tabpanel" aria-labelledby="home-tab">
                        <div class="inner_padding">
                            <ul class="d-flex flex-column">
                                <li>Got some problems? Watch the tutorial!<br>
                                    <div style="background-color:darkgray;width:100%;min-height:600px;;">

                                    </div>
                                </li>
                                <li>Problem not solved? <a href="#">Contact Us</a></li>

                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(".radio_button").on("click", function() {
        console.log("trigger");
        switch ($(this).val()) {
            case "simple":
                $("#data_requirement_simple").prop("checked", true);
                $("#data_requirement_informative").prop("checked", false);
                if ($("#data_requirement_simple").prop("checked")) {
                    $(".button_informative").removeClass("selected");
                    $(".button_simple").addClass("selected");
                }
                if ($("#data_requirement_informative").prop("checked")) {
                    $(".button_simple").removeClass("selected");
                    $(".button_informative").addClass("selected");
                }
                break;
            case "informative":
                $("#data_requirement_simple").prop("checked", false);
                $("#data_requirement_informative").prop("checked", true);
                if ($("#data_requirement_simple").prop("checked")) {
                    $(".button_informative").removeClass("selected");
                    $(".button_simple").addClass("selected");
                }
                if ($("#data_requirement_informative").prop("checked")) {
                    $(".button_simple").removeClass("selected");
                    $(".button_informative").addClass("selected");
                }
                break;
            case "LEVEL":
                $("#LEVEL").prop("checked", true);
                $(".RADIO_LEVEL").removeClass("selected");
                $(".RADIO_NICK_NAME").removeClass("selected");
                $(".RADIO_POINTS").removeClass("selected");
                $(".RADIO_STAMPS").removeClass("selected");
                $(".RADIO_LEVEL").addClass("selected");
                break;
            case "NICK_NAME":
                $("#NICK_NAME").prop("checked", true);
                $(".RADIO_LEVEL").removeClass("selected");
                $(".RADIO_NICK_NAME").removeClass("selected");
                $(".RADIO_POINTS").removeClass("selected");
                $(".RADIO_STAMPS").removeClass("selected");
                $(".RADIO_NICK_NAME").addClass("selected");
                break;
            case "POINTS":
                $("#POINTS").prop("checked", true);
                $(".RADIO_LEVEL").removeClass("selected");
                $(".RADIO_NICK_NAME").removeClass("selected");
                $(".RADIO_POINTS").removeClass("selected");
                $(".RADIO_STAMPS").removeClass("selected");
                $(".RADIO_POINTS").addClass("selected");
                break;
            case "STAMPS":
                $("#STAMPS").prop("checked", true);
                $(".RADIO_LEVEL").removeClass("selected");
                $(".RADIO_NICK_NAME").removeClass("selected");
                $(".RADIO_POINTS").removeClass("selected");
                $(".RADIO_STAMPS").removeClass("selected");
                $(".RADIO_STAMPS").addClass("selected");
                break;
            case "ORDINARY":
                $("#ORDINARY").prop("checked", true);
                $(".RADIO_ORDINARY").removeClass("selected");
                $(".RADIO_MODERN").removeClass("selected");
                $(".RADIO_PLAYFUL").removeClass("selected");
                $(".RADIO_FREATURED").removeClass("selected");
                $(".RADIO_ORDINARY").addClass("selected");
                break;
            case "MODERN":
                $("#MODERN").prop("checked", true);
                $(".RADIO_ORDINARY").removeClass("selected");
                $(".RADIO_MODERN").removeClass("selected");
                $(".RADIO_PLAYFUL").removeClass("selected");
                $(".RADIO_FREATURED").removeClass("selected");
                $(".RADIO_MODERN").addClass("selected");
                break;
            case "PLAYFUL":
                $("#PLAYFUL").prop("checked", true);
                $(".RADIO_ORDINARY").removeClass("selected");
                $(".RADIO_MODERN").removeClass("selected");
                $(".RADIO_PLAYFUL").removeClass("selected");
                $(".RADIO_FREATURED").removeClass("selected");
                $(".RADIO_PLAYFUL").addClass("selected");
                break;
            case "FREATURED":
                $("#FREATURED").prop("checked", true);
                $(".RADIO_ORDINARY").removeClass("selected");
                $(".RADIO_MODERN").removeClass("selected");
                $(".RADIO_PLAYFUL").removeClass("selected");
                $(".RADIO_FREATURED").removeClass("selected");
                $(".RADIO_FREATURED").addClass("selected");
                break;
        }

    })

    $(".checkbox_button").on("click", function() {
        if ($("#" + $(this).val()).prop("checked")) {
            $("#" + $(this).val()).prop("checked", false);
            $(this).removeClass("selected");
        } else {
            $("#" + $(this).val()).prop("checked", true);
            $(this).addClass("selected");
        }
    })

    $(".color_ball").on("click", function() {
        $(".color_ball").removeClass("selected");
        if ($(".color_ball").data("value") != "add") {
            $(".color_ball").empty();
        }
        $(this).addClass("selected");
        $(this).append('<i class="fas fa-check" style="color:white;padding-top:3px;"></i>');

    })


    $("#color_picker").on('click', function() {
        $("#color_picker_body").click();
    })
    $("#color_picker_body").on("change", function() {
        $("#color_picker").css("background-color", $(this).val());
    })

    $(".color_picker").on("click", function() {
        console.log("trigger");
        $(this).parent("div").prev("div").children("input").click();
    })

    $(".color_picker_selector").on("change", function() {
        $(this).parent("div").next("div").children("div").css("background-color", $(this).val())
    })

    $("#trial-tab").on("change", function() {
        if ($(this).hasClass("active")) {
            $("#next_button").hide();
            $("#submit_button").show();
        } else {
            $("#next_button").show();
            $("#submit_button").hide();
        }
    })

    $("#next_button").on("click", function() {

        $(".nav-link.active.show").parent().next().children().click();

        if ($(".nav-link.active.show").attr('id') == "export-tab") {

            $("#next_button").hide();
            $("#export_button").show();
        } else {

            $("#next_button").show();
            $("#export_button").hide();
        }
    })

    $(".nav-link").on("click", function() {
        if ($(this).attr('id') == "export-tab") {
            $("#next_button").hide();
            $("#export_button").show();
        } else {
            $("#next_button").show();
            $("#export_button").hide();
        }
    })
</script>