<meta charset="utf-8" />

<title>O2O C3C Merchant CMS</title>



<meta name="description" content="Dashboard" />

<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="<?= base_url('public/static/backend/css/bootstrap.min.css') ?>" rel="stylesheet" />

<link href="<?= base_url('public/static/bootstrap-4.3.1-dist/css/bootstrap.min.css') ?>" rel="stylesheet" />

<link id="bootstrap-rtl-link" href="#" rel="stylesheet" />

<link href="<?= base_url() ?>public/static/backend/css/font-awesome.min.css" rel="stylesheet" />

<link href="<?= base_url() ?>public/static/backend/css/weather-icons.min.css" rel="stylesheet" />



<!--Fonts-->

<link href="//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">

<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

<!--Beyond styles-->

<link id="beyond-link" href="<?= base_url() ?>public/static/backend/css/beyond.min.css" rel="stylesheet" type="text/css" />

<link href="<?= base_url() ?>public/static/backend/css/demo.min.css" rel="stylesheet" />

<link href="<?= base_url() ?>public/static/backend/css/typicons.min.css" rel="stylesheet" />

<link href="<?= base_url() ?>public/static/backend/css/animate.min.css" rel="stylesheet" />

<link href="<?= base_url() ?>public/static/backend/css/all.css" rel="stylesheet" />

<link href="<?= base_url() ?>public/static/backend/js/fancybox/jquery.fancybox.min.css" rel="stylesheet" />


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom-datetimepicker.css?v=<?= rand(10000, 100000) ?>">

<link href="<?= base_url() ?>public/static/backend/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />

<link href="<?= base_url() ?>public/static/backend/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" />

<link href="<?= base_url() ?>public/static/merchant/css/global.css?v=<?= rand(10000, 10000) ?>" rel="stylesheet">

<link href="<?= base_url() ?>public/static/backend/css/admin.css?v=<?= rand(10000, 100000) ?>" rel="stylesheet" />

<link id="skin-link" href="#" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="<?= base_url() ?>public/static/backend/vendor/datatables/datatables.min.css" />

<link rel="stylesheet" type="text/css" href="<?= base_url() ?>public/static/backend/css/layouts/o2o-c3c.css" />

<!--Skin Script: Place this script in head to load scripts for skins and rtl support-->

<script src="<?= base_url() ?>public/static/backend/js/skins.js"></script>

<script src="<?= base_url() ?>public/static/backend/js/jquery.min.js"></script>

<script src="<?= base_url() ?>public/static/backend/js/popper.min.js"></script>

<script src="<?= base_url() ?>public/static/backend/vendor/datatables/datatables.min.js"></script>

<script src="<?= base_url() ?>public/static/backend/vendor/datatables/four_button.js"></script>

<script src="<?= base_url() ?>public/static/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>

<script src="<?= base_url() ?>public/static/backend/js/slimscroll/jquery.slimscroll.min.js"></script>

<script src="<?= base_url() ?>public/static/backend/js/fancybox/jquery.fancybox.min.js"></script>

<script src="<?= base_url() ?>public/static/backend/vendor/moment/moment.min.js"></script>

<script src="<?= base_url() ?>public/static/backend/vendor/moment/zh-hk.js"></script>

<script src="<?= base_url() ?>public/static/backend/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<script src="<?= base_url() ?>public/static/backend/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>

<script src="<?= base_url() ?>public/static/backend/js/select2/select2.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/o2o-c3c.css" />
<script src="<?php echo base_url(); ?>assets/js/o2o-c3c.js"></script>