<div class="page-body">
    <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5"><?= $page ?></h4>
    <div class="row">
        <div class="col-12">
            <div class="editor">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item first-block">
                        Control
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" id="verify_tab" data-toggle="tab" href="#" aria-controls="home" aria-selected="true">Verify</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="inner_padding">
                        <h4 class="text-danger font-weight-bold">*Current function is not available for api passes</h4>
                        <div id="reader">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="searching_result_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg mx-auto" role="document">
            <div class="modal-content bg-accent">
                <div class="modal-content-wrapper">
                    <div class="modal-header p-0 text-white bg-transparent border-0">
                        Searching Result
                    </div>
                    <div class="modal-body mt-5 p-5 bg-white" style="border-radius:30px!important;">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <h4>Account Info</h4>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Account ID</div>
                                    <div id="account_id" class="col-auto"></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Status</div>
                                    <div id="status" class="col-auto"></div>
                                </div>
                                <div class="row mb-2 align-items-center">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Level</div>
                                    <select id="level" class="col form-control" style="height:35px!important;">
                                        <option>example</option>
                                    </select>
                                </div>
                                <div class="row mb-2 align-items-center">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Points</div>
                                    <input id="points" type="number" class="col form-control" style="height:35px!important;" min="0">
                                </div>
                            </div>
                            <div class="col-12 d-flex justify-content-center mt-4">
                                <button id="submit_button" class="btn btn-success mr-5">Submit</button>
                                <button id="close_button" class="btn btn-default">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="modal fade" id="api_searching_result_modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg mx-auto" role="document">
                <div class="modal-content bg-accent">
                    <div class="modal-content-wrapper">
                        <div class="modal-header p-0 text-white bg-transparent border-0">
                        Searching Result
                        </div>
                        <div class="modal-body mt-5 p-5 bg-white" style="border-radius:30px!important;">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <h4>Account Info</h4>
                                    <div class="row mb-2">
                                        <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Account ID</div>
                                        <div id="account_id" class="col-auto"></div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Status</div>
                                        <div id="status" class="col-auto"></div>
                                    </div>
                                    
                                </div>
                                <div class="col-12 d-flex justify-content-center mt-4">
                                    <button id="submit_button" class="btn btn-success mr-5">Submit</button>
                                    <button id="close_button" class="btn btn-default">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
</div>


<script src="https://unpkg.com/html5-qrcode" type="text/javascript"></script>
<script>
    var base_url = "<?= base_url() ?>";
    const html5QrCode = new Html5Qrcode("reader");
    var scanned_id = "";
    const config = {
        fps: 60,
        qrbox: {
            width: 250,
            height: 250
        }
    };
    const facingMode = "environment";

    function start_camera() {
        html5QrCode.start({
            facingMode: "environment"
        }, config, qrCodeSuccessCallback)
    }

    const qrCodeSuccessCallback = (decodedText, decodedResult) => {
        html5QrCode.stop();
        scanned_id = decodedText;
        $.ajax({
            url: base_url + "merchant/smart_memberbase/scan_qr",
            method: "post",
            data: {
                "id": decodedText
            },
            "dataType": "json",
            "success": function(data) {
                if (data.found == "true") {
                    //if(data.api == 0){
                    modal_init(data.user, data.levels);
                    //}else{
                        
                    //}
                } else {
                    alert("No found");
                    setInterval(start_camera(), 3000);
                }
            }
        })
    }

    $("#close_button").on("click", function() {
        $("#searching_result_modal").modal("hide");
    })

    $("#submit_button").on("click", function() {
        if ($("#level").val() === null) {
            alert("Please select the user level");
            return false;
        }
        if ($("#points").val() < 0) {
            alert("Please enter a number greater than -1");
            return false;
        }
        if (scanned_id === "") {
            alert("Invalid action");
            return false;
        }

        $.ajax({
            url: base_url + "merchant/smart_memberbase/scan_update",
            method: "post",
            data: {
                "id": scanned_id,
                "points": $("#points").val(),
                "level": $("#level").val()
            },
            dataType: "json",
            success: function(result) {
                if (result.update == "invalid") {
                    alert("Invalid action");
                } else {
                    alert("Updated");

                }
            },
            error: function() {
                alert("Something is wrong");
            }
        })

    })

    function modal_init(user, levels) {
        $("#account_id").empty();
        $('#account_id').text(user.account_id);
        $("#status").empty();
        $("#status").text(user.status);
        $("#level").empty();
        for (var i in levels) {
            if (levels[i].id == user.level) {
                $("#level").append("<option selected value='" + levels[i].id + "'>" + levels[i].value + "</option>")
            } else {
                $("#level").append("<option value='" + levels[i].id + "'>" + levels[i].value + "</option>");
            }
        }
        $("#points").val(user.points);
        $("#searching_result_modal").modal("show");


    }

    $("#searching_result_modal").on("hidden.bs.modal", function() {
        start_camera();
    })




    $(document).ready(function() {

        // If you want to prefer front camera
        html5QrCode.start({
            facingMode: "environment"
        }, config, qrCodeSuccessCallback);

        // If you want to prefer back camera
        html5QrCode.start({
            facingMode: "environment"
        }, config, qrCodeSuccessCallback);

        // Select front camera or fail with `OverconstrainedError`.
        html5QrCode.start({
            facingMode: {
                exact: "environment"
            }
        }, config, qrCodeSuccessCallback);

        // Select back camera or fail with `OverconstrainedError`.
        html5QrCode.start({
            facingMode: {
                exact: "environment"
            }
        }, config, qrCodeSuccessCallback);

    })
</script>