<div class="page-body">
    <div class="col-12 col-md-6">
        <div class="title pt-4 pb-5 font-weight-bold">Coupon Info Update</div>
        <form method="post" action="/merchant/coupon_memberbase/update">
            <div class="form-group d-flex align-items-center">
                <div style="white-space:nowrap">Coupon Info</div>
                <hr class="w-100">
            </div>
            <div class="form-group">
                <label>Display Code</label>
                <input readonly class="form-control" value="<?= $detail['display_code'] ?>">
            </div>
            <div class="form-group">
                <label>Coupon Name</label>
                <input readonly class="form-control" value="<?= $detail['coupon_name'] ?>">
            </div>
            <div class="form-group">
                <label>Coupon Amount</label>
                <input readonly class="form-control" value="<?= $detail['coupon_amount'] ?>">
            </div>
            <div class="form-group">
                <label>Expiry Date</label>
                <input readonly class="form-control" value="<?= $detail['expiry_date'] ?>">
            </div>
            <div class="form-group">
                <label>Status</label>
                <input readonly class="form-control" value="<?= $detail['status'] ?>">
            </div>
            <div class="form-group d-flex align-items-center">
                <div style="white-space:nowrap">Account Info</div>
                <hr class="w-100">
            </div>
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" name="name" value="<?=$detail['name']?>">
            </div>
            <div class="form-group">
                <label>Surname</label>
                <input type="text" class="form-control" name="surname" value="<?= $detail['surname'] ?>">
            </div>
            <div class="form-group">
                <label>Given Name</label>
                <input type="text" class="form-control" name="given_name" value="<?= $detail['given_name'] ?>">
            </div>
            <div class="form-group">
                <label>English Name</label>
                <input type="text" class="form-control" name="english_name" value="<?= $detail['english_name'] ?>">
            </div>
            <div class="form-group">
                <label>Chinese Name</label>
                <input type="text" class="form-control" name="chinese_name" value="<?= $detail['chinese_name'] ?>">
            </div>
            <div class="form-group">
                <label>Gender</label>
                <select class="form-control" name="gender">
                    <option <?= $detail['gender'] == 1 ? "selected" : "" ?> value="1">Male</option>
                    <option <?= $detail['gender'] == 0 ? "selected" : "" ?> value="0">Female</option>
                </select>
            </div>
            <div class="form-group">
                <label>Birthday</label>
                <input type="date" class="form-control" name="birthday" value="<?= $detail['birthday'] ?>">
            </div>
            <div class="form-group">
                <label>Age</label>
                <input type="number" class="form-control" name="age" value="<?= $detail['age'] ?>">
            </div>
            <div class="form-group">
                <label>Nationality</label>
                <input type="text" class="form-control" name="nationality" value="<?= $detail['nationality'] ?>">
            </div>
            <div class="form-group d-flex align-items-center">
                <div style="white-space:nowrap;">Contact Info</div>
                <hr class="w-100">
            </div>
            <div class="form-group">
                <label>Phone</label>
                <input type="text" class="form-control" name="phone" value="<?= $detail['phone'] ?>">
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" class="form-control" name="email" value="<?= $detail['email'] ?>">
            </div>
            <div class="form-group">
                <label>Address</label>
                <input type="text" class="form-control" name="address" value="<?= $detail['address'] ?>">
            </div>
            <div class="form-group">
                <label>Industry</label>
                <input type="text" class="form-control" name="industry" value="<?= $detail['industry'] ?>">
            </div>
            <div class="form-group d-flex align-items-center">
                <div style="white-space:nowrap;">Work Info</div>
                <hr class="w-100">
            </div>
            <div class="form-group">
                <label>Industry</label>
                <input type="text" class="form-control" name="industry_2" value="<?= $detail['industry_2'] ?>">
            </div>
            <div class="form-group">
                <label>Job Title</label>
                <input type="text" class="form-control" name="job_title" value="<?= $detail['job_title'] ?>">
            </div>
            <div class="form-group">
                <label>Company</label>
                <input type="text" class="form-control" name="company" value="<?= $detail['company'] ?>">
            </div>
            <div class="form-group">
                <label>Salary</label>
                <input type="text" class="form-control" name="salary" value="<?= $detail['salary'] ?>">
            </div>
            <div class="form-group">
                <label>QR</label>
                <div class="">
                <img src="/assets/user_qr/coupon/<?=$detail['id']?>_QR.png">
                </div>
            </div>
            <div class="form-group mt-5">
                <input type="hidden" name="id" value="<?= $detail['id'] ?>">
                <button type="submit" class="btn btn-success font-weight-bold text-white">Submit</button>
            </div>
        </form>
    </div>
</div>