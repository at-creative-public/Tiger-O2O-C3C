<div class="page-body">
    <div class="row pl-0 pl-xl-5">
        <div class="col-12">
            <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5"><?= $page ?></h4>
            <div class="page-content-section p-3 p-sm-5">
                <div class="position-relative">
                    <div id="member_database_filter" class="d-flex flex-column w-100">
                        <div class="col-auto">

                        </div>
                        <div class="dataTables_info" id="coupon_info" role="status" aria-live="polite">

                        </div>
                        <div class="top">
                            <div class="dataTables_info pb-0" id="merchant_info" role="status" aria-live="polite">Showing <span id="start"></span> to <span id="end"></span> of <span id="counter"></span> entries</div>
                        </div>
                        <div class="col-auto ">
                            <div class="row w-100 align-items-center justify-content-end">
                                <div class="d-inline-block mr-auto mb-2">
                                    <div class="">
                                        <span class="mr-2">Displaying Virtual Pass:</span>
                                        <select id="selected_coupon_id">
                                            <?php foreach ($coupons as $coupon) { ?>
                                                <option value="<?= $coupon['id'] ?>"><?= $coupon["project_name"] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <a id="export_button" href="#" class="btn mr-4 mb-2">Export Database</a>

                                <div class="d-inline-block">
                                    <div class="input-group input-group-search pr-2 mb-2"> <input type="text" class="fa" id="general_search" name="general_search" placeholder="Search">
                                        <div class="input-group-append"> <span class="input-group-text"><i class="fa fa-search"></i></span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="scroller">
                        <table id="myTable" border="0" class="w-100 dataTable border-0 pt-0 pt-md-5">
                            <thead>
                                <tr>
                                    <th>Coupon Seq</th>
                                    <th>Display Code</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Detail</th>
                                    <th>Edit</th>
                                    <th>Void</th>
                                </tr>
                            </thead>
                        </table>
                        <div class="text-center">
                            <button id="previous_page" class="btn mr-4" style="width:9rem;">Previous</button>
                            <button id="next_page" class="btn" style="width:9rem">Next</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="detail_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg mx-auto">
        <div class="modal-content bg-accent">
            <div class="modal-content-wrapper">
                <div class="modal-header p-0 text-white bg-transparent border-0">
                    Detail
                </div>
                <div class="modal-body mt-5 p-5 bg-white" style="border-radius:30px!important;">
                    <div class="row">
                        <div class="col-6">
                            <h4>Coupon Info</h4>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Display Code</div>
                                <div id="display_code" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Coupon Name</div>
                                <div id="coupon_name" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Coupon Amount</div>
                                <div id="coupon_amount" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Expiry Date</div>
                                <div id="expiry_date" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Void Date</div>
                                <div id="void_date" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Status</div>
                                <div id="status" class="col-auto"></div>
                            </div>
                            <h4 class="mt-4">Account Info</h4>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Name</div>
                                <div id="name" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Surname</div>
                                <div id="surname" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Given Name</div>
                                <div id="given_name" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">English Name</div>
                                <div id="english_name" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Chinese Name</div>
                                <div id="chinese_name" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Gender</div>
                                <div id="gender" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Birthday</div>
                                <div id="birthday" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Age</div>
                                <div id="age" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Nationality</div>
                                <div id="nationality" class="col-auto"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <h4>Contact Info</h4>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Phone</div>
                                <div id="phone" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Email</div>
                                <div id="email" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Address</div>
                                <div id="address" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Industry</div>
                                <div id="industry" class="col-auto"></div>
                            </div>
                            <h4 class="mt-4">Work Info</h4>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Industry</div>
                                <div id="industry_2" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Job title</div>
                                <div id="job_title" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Company</div>
                                <div id="company" class="col-auto"></div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Salary</div>
                                <div id="salary" class="col-auto"></div>
                            </div>
                        </div>
                        <div class="col-12 d-flex justify-content-center">
                            <a id="detail_page_hyperlink" href="#" class="btn btn-success font-weight-bold text-white mr-4" style="width:6rem;">Edit</a>
                            <button id="close_button" class="btn btn-default font-weight-bold text-black" style="width:6rem;">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var table, data = [];
    var base_url = "<?= base_url() ?>";
    var selected_members = [];
    $(document).ready(function() {
        let selected = $("#selected_coupon_id").val();
        $("#export_button").attr("href", "/merchant/coupon_memberbase/export_csv?coupon_id=" + selected);
        table = $("#myTable").DataTable({
            "processing": false,
            "serverSide": true,
            "ajax": base_url + "merchant/coupon_memberbase/listing" + "?coupon_id=" + selected,
            "columns": [{
                "data": "coupon_series",
                "name": "coupon_series"
            }, {
                "data": "display_code",
                "name": "display_code"
            }, {
                "data": "name",
                "name": "name"
            },{
                "data": "phone",
                "name": "phone"
            }, {
                "data": "email",
                "name": "email"
            },  {
                "data": "status",
                "name": "status"
            }, {
                "data": "view",
                "name": "view"
            }, {
                "data": "edit",
                "name": "edit"
            }, {
                "data": "void",
                "name": "void"
            }],
            "order": [0, "desc"],
            "columnDefs": [{
                    "targets": [0],
                    "visible": true,
                    "searchable": false,
                    "orderable": true
                },
                {
                    "targets": [6, 7, 8],
                    "visible": true,
                    "searchable": true,
                    "orderable": false
                }, {
                    "targets": [1,2, 3, 4, 5],
                    "visible": true,
                    "searchable": false,
                    "orderable": true
                }
            ],
            "drawCallback": function() {
                button_init();
                $("#start").text(table.page.info().start + 1);
                $("#end").text(table.page.info().end);
                $("#counter").text(table.page.info().recordsDisplay);
                if (table.page.info().recordsDisplay == 0) {
                    $("#start").text("0");
                }
            }
        })
    });
    $("#selected_coupon_id").on("change", function() {
        $("#myTable").DataTable().ajax.url(base_url + "merchant/coupon_memberbase/listing" + "?coupon_id=" + $(this).val());
        $("#myTable").DataTable().draw("page");
        $("#export_button").attr("href", "/merchant/coupon_memberbase/export_csv?coupon_id=" + $(this).val());
    })

    $("#previous_page").on("click", function() {
        $("#myTable").DataTable().page("previous").draw("page");
    })
    $("#next_page").on("click", function() {
        $("#myTable").DataTable().page("next").draw("page");
    })
    $("#general_search").on("input", function() {
        $("#myTable").DataTable().columns(0).search($(this).val()).draw();
    })

    function button_init() {
        $(".detail_button").on("click", function() {
            $.ajax({
                url: base_url + "merchant/coupon_memberbase/detail",
                data: {
                    "id": $(this).data('id')
                },
                dataType: "json",
                success: function(data) {

                    modal_init(data);
                },

            })
        })
        $(".void_button").on("click", function() {
            console.log("void trigger");
            if (confirm("Are you sure to void this coupon?")) {
                $.ajax({
                    url: base_url + "merchant/coupon_memberbase/void",
                    method: "post",
                    data: {
                        "id": $(this).data("id")
                    },
                    dataType: "json",
                    success: function() {
                        window.location.reload();
                    }
                })
            }
        })
    }

    function modal_init(data) {
        $("#display_code").empty();
        $("#display_code").text(data.display_code);
        $("#coupon_name").empty();
        $("#coupon_name").text(data.coupon_name);
        $("#coupon_amount").empty();
        $("#coupon_amount").text(data.coupon_amount);
        $("#expiry_date").empty();
        $("#expiry_date").text(data.expiry_date);
        $("#void_date").empty();
        $("#void_date").text(data.void_date);
        $("#status").empty();
        $("#status").text(data.status);
        $("#name").empty();
        $("#name").text(data.name);
        $("#surname").empty();
        $("#surname").text(data.surname);
        $("#given_name").empty();
        $("#given_name").text(data.given_name);
        $("#english_name").empty();
        $("#english_name").text(data.english_name);
        $("#chinese_name").empty();
        $("#chinese_name").text(data.chinese_name);
        $("#gender").empty();
        if (data.gender == 1) {
            $("#gender").text("Male");
        } else {
            $("#gender").text("Female");
        }
        $("#birthday").empty();
        $("#birthday").text(data.birthday);
        $("#age").empty();
        $("#age").text(data.age);
        $("#nationality").empty();
        $("#nationality").text(data.nationality);
        $("#phone").empty();
        $("#phone").text(data.phone);
        $("#email").empty();
        $("#email").text(data.email);
        $("#address").empty();
        $("#address").text(data.address);
        $("#industry").empty();
        $("#industry").text(data.industry);
        $("#industry_2").empty();
        $("#industry_2").text(data.industry_2);
        $("#job_title").empty();
        $("#job_title").text(data.job_title);
        $("#company").empty();
        $("#company").text(data.company);
        $("#salary").empty();
        $("#salary").text(data.salary);
        $("#detail_page_hyperlink").attr("href", "/merchant/coupon_memberbase/edit/" + data.id);
        $("#detail_modal").modal("show");
    }

    $("#close_button").on("click", function() {
        $("#detail_modal").modal("hide");
    })
</script>