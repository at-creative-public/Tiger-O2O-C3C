<div class="page-body">

    <div class="row">
        <div class="col-12 col-md-6">
            <div class="title pt-4 pb-5 font-weight-bold">Account Information</div>
            <form method="post" action="/merchant/smart_memberbase/update_info">
                <div class="form-group d-flex align-items-center">
                    <div style="white-space:nowrap">Pass Information</div>
                    <hr class="w-100">
                </div>
                <div class="form-group">
                    <label>Card's Project</label>
                    <input readonly class="form-control" value="<?= $pass_info['project_name'] ?>">
                </div>
                <div class="form-group">
                    <label>Account ID</label>
                    <input readonly class="form-control" value="<?= $member_info["account_id"] ?>">
                </div>
                <div class="form-group">
                    <label>Level</label>
                    <select class="form-control " name="level">
                        <?php foreach ($pass_levels as $level) { ?>
                            <option <?= $member_info['level'] == $level['id'] ? "selected" : "" ?> value="<?= $level['id'] ?>"><?= $level['value'] ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Points</label>
                    <input type="number" class="form-control" name="points" value="<?= $member_info['points'] ?>">
                </div>
                <!--<div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" name="name" value="<?= $member_info['name'] ?>">
                </div>-->
                <div class="form-group d-flex align-items-center">
                    <div style="white-space:nowrap">Personal Information&nbsp;</div>
                    <hr class="w-100">
                </div>
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" name="name" value="<?=$member_info['name']?>">
                </div>
                <div class="form-group">
                    <label>Surname</label>
                    <input type="text" class="form-control" name="surname" value="<?= $member_info['surname'] ?>">
                </div>
                <div class="form-group">
                    <label>Given Name</label>
                    <input type="text" class="form-control" name="given_name" value="<?= $member_info['given_name'] ?>">
                </div>
                <div class="form-group">
                    <label>English Name</label>
                    <input type="text" class="form-control" name="english_name" value="<?= $member_info['english_name'] ?>">
                </div>
                <div class="form-group">
                    <label>Chinese Name</label>
                    <input type="text" class="form-control" name="chinese_name" value="<?= $member_info['chinese_name'] ?>">
                </div>
                <div class="form-group">
                    <label>Gender</label>
                    <select name="gender" class="form-control">
                        <option <?= $member_info['gender'] == 1 ? "selected" : "" ?> value="1">Male</option>
                        <option <?= $member_info['gender'] == 0 ? "selected" : "" ?> value="0">Female</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Birthday</label>
                    <input type="date" class="form-control" name="birthday" value="<?= $member_info['birthday'] ?>">
                </div>
                <div class="form-group">
                    <label>Age</label>
                    <input type="text" class="form-control" name="age" value="<?= $member_info['age'] ?>">
                </div>
                <div class="form-group">
                    <label>Nationality</label>
                    <input type="text" class="form-control" name="nationality" value="<?= $member_info['nationality'] ?>">
                </div>
                <div class="form-group d-flex align-items-center">
                    <div style="white-space:nowrap">
                        Contact Information
                    </div>
                    <hr class="w-100">
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    <input type="text" class="form-control" name="phone" value="<?= $member_info['phone'] ?>">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" value="<?= $member_info['email'] ?>">
                </div>
                <div class="form-group">
                    <label>Address</label>
                    <input type="text" class="form-control" name="address" value="<?= $member_info['address'] ?>">
                </div>
                <div class="form-group">
                    <label>Industry</label>
                    <input type="text" class="form-control" name="industry" value="<?= $member_info['industry'] ?>">
                </div>
                <div class="form-group d-flex align-items-center">
                    <div style="white-space:nowrap">
                        Work Information
                    </div>
                    <hr class="w-100">
                </div>
                <div class="form-group">
                    <label>Industry</label>
                    <input type="text" class="form-control" name="industry_2" value="<?= $member_info['industry_2'] ?>">
                </div>
                <div class="form-group">
                    <label>Job Title</label>
                    <input type="text" class="form-control" name="job_title" value="<?= $member_info['job_title'] ?>">
                </div>
                <div class="form-group">
                    <label>Company</label>
                    <input type="text" class="form-control" name="company" value="<?= $member_info["company"] ?>">
                </div>
                <div class="form-group">
                    <label>Salary</label>
                    <input type="text" class="form-control" name="salary" value="<?= $member_info['salary'] ?>">
                </div>
                <div class="form-group">
                    <label>QRcode</label>
                    <div>
                       
          
                     <img src="/assets/user_qr/membership/<?=$member_info['id']?>_QR.png">
               
                    </div>
                </div>
                <div class="form-group">
                    <input type="hidden" name="id" value="<?= $member_info['id'] ?>">
                    <button type="submit" class="btn btn-primary" style="color:white;font-weight:bold;">Submit</button>
                </div>

            </form>
        </div>
    </div>
</div>