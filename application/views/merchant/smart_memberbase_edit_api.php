<div class="page-body">

    <div class="row">
        <div class="col-12 col-md-6">
            <div class="title pt-4 pb-5 font-weight-bold">Account Information</div>

            <div class="form-group d-flex align-items-center">
                <div style="white-space:nowrap">Pass Information</div>
                <hr class="w-100">
            </div>
            <div class="form-group">
                <label>Card's Project</label>
                <input readonly class="form-control" value="<?= $pass_info['project_name'] ?>">
            </div>
            <div class="form-group">
                <label>Account ID</label>
                <input readonly class="form-control" value="<?= $member_info["account_id"] ?>">
            </div>
            <div class="form-group">
                <label>Ref id</label>
                <input readonly class="form-control" value="<?= $member_info['ref_id'] ?>">
            </div>
            <div class="form-group">
                <label>Level</label>
                <input type="text" class="form-control" value="<?= $member_info['level_api'] ?>" readonly>
            </div>
            <div class="form-group">
                <label>Points</label>
                <input type="number" class="form-control" value="<?= $member_info['points'] ?>" readonly>
            </div>
            <div class="form-group">
                <label>Platform</label>
                <input type="text" class="form-control" value="<?= $member_info['platform'] ?>" readonly>
            </div>
            <!--<div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" name="name" value="<?= $member_info['name'] ?>" >
                </div>-->
            <div class="form-group d-flex align-items-center">
                <div style="white-space:nowrap">Personal Information&nbsp;</div>
                <hr class="w-100">
            </div>
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" value="<?= $member_info['name'] ?>" readonly>
            </div>
            <div class="form-group">
                <label>Surname</label>
                <input type="text" class="form-control" value="<?= $member_info['surname'] ?>" readonly>
            </div>
            <div class="form-group">
                <label>Given Name</label>
                <input type="text" class="form-control" value="<?= $member_info['given_name'] ?>" readonly>
            </div>
            <div class="form-group">
                <label>English Name</label>
                <input type="text" class="form-control" value="<?= $member_info['english_name'] ?>" readonly>
            </div>
            <div class="form-group">
                <label>Chinese Name</label>
                <input type="text" class="form-control" value="<?= $member_info['chinese_name'] ?>" readonly>
            </div>
            <div class="form-group">
                <label>Gender</label>
                <select class="form-control" disabled>
                    <option <?= $member_info['gender'] == 1 ? "selected" : "" ?> value="1">Male</option>
                    <option <?= $member_info['gender'] == 0 ? "selected" : "" ?> value="0">Female</option>
                </select>
            </div>
            <div class="form-group">
                <label>Birthday</label>
                <input type="date" class="form-control" value="<?= $member_info['birthday'] ?>" readonly>
            </div>
            <div class="form-group">
                <label>Age</label>
                <input type="text" class="form-control" value="<?= $member_info['age'] ?>" readonly>
            </div>
            <div class="form-group">
                <label>Nationality</label>
                <input type="text" class="form-control" value="<?= $member_info['nationality'] ?>" readonly>
            </div>
            <div class="form-group d-flex align-items-center">
                <div style="white-space:nowrap">
                    Contact Information
                </div>
                <hr class="w-100">
            </div>
            <div class="form-group">
                <label>Phone</label>
                <input type="text" class="form-control" value="<?= $member_info['phone'] ?>" readonly>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" value="<?= $member_info['email'] ?>" readonly>
            </div>
            <div class="form-group">
                <label>Address</label>
                <input type="text" class="form-control" value="<?= $member_info['address'] ?>" readonly>
            </div>
            <div class="form-group">
                <label>Industry</label>
                <input type="text" class="form-control" value="<?= $member_info['industry'] ?>" readonly>
            </div>
            <div class="form-group d-flex align-items-center">
                <div style="white-space:nowrap">
                    Work Information
                </div>
                <hr class="w-100">
            </div>
            <div class="form-group">
                <label>Industry</label>
                <input type="text" class="form-control" value="<?= $member_info['industry_2'] ?>" readonly>
            </div>
            <div class="form-group">
                <label>Job Title</label>
                <input type="text" class="form-control" value="<?= $member_info['job_title'] ?>" readonly>
            </div>
            <div class="form-group">
                <label>Company</label>
                <input type="text" class="form-control" value="<?= $member_info["company"] ?>" readonly>
            </div>
            <div class="form-group">
                <label>Salary</label>
                <input type="text" class="form-control" value="<?= $member_info['salary'] ?>" readonly>
            </div>
            <div class="form-group">
                <label>QRcode</label>
                <div>
                    <img src="/assets/user_qr/membership/<?= $member_info['id'] ?>_QR.png">
                </div>
            </div>
            <div class="form-group">
                <input type="hidden" name="id" value="<?= $member_info['id'] ?>">
                <a href="/merchant/smart_memberbase" class="btn btn-default">
                    Return
                </a>
            </div>
        </div>
    </div>
</div>