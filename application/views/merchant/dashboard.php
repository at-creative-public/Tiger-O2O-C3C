<link rel="stylesheet" href="/public/static/backend/css/pages/dashboard.css?v=<?= rand(10000, 100000) ?>">

<div class="page-body">

    <div class="row">
        <div class="col-12">
            <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5"><?= $page ?></h4>
            <div class="row">
                <div class="col-12 w-100 d-flex">
                    <h5 class="mr-auto">Current Plan: <?= $current_plan ?></h5>
                    <h5>Exipred on: <?= $expired_date ?></h5>
                </div>
                <div class="col-12 w-100 row">
                    <div class="col-12 col-xl-5 mb-2 d-flex align-items-center" style="visibility:hidden;height:0px!important;">
                        <span class="pr-4">Type:</span>
                        <select id="selection_type" class="form-control" style="height:32px;">
                            <!--<option value="overall">Overall</option>-->
                            <option value="membership">Membership</option>
                            <!--<option value="coupon">Coupon</option>
                            <option value="event_ticket">Event Ticket</option>-->
                        </select>
                    </div>
                    <div class="col-12 col-xl-10 mb-2 d-flex align-items-center">
                        <span class="pr-4">Name:</span>
                        <select id="selection_index" class="form-control" style="height:32px;">
                            <!--<option>---</option>-->
                        </select>
                    </div>
                    <div class="col-12 col-xl-2 mb-2 d-flex align-items-center">
                        <button id="review_button" class="btn btn-primary font-weight-bold mr-4">Review</button>
                        <!--<a href="/merchant/dashboard/report_listing" class="btn btn-primary">View CRM Report</a>-->
                        <a href="#" class="btn btn-primary">View CRM Report</a>
                    </div>
                </div>

            </div>
            <div class="d-flex flex-column">
                <div class="col-12 row ">
                    <div class="col-12 col-md-4 pt-4 pb-4 d-none">
                        <div class="data_frame">
                            <canvas id="bar_chart" width="100%"></canvas>
                        </div>
                    </div>
                    <div class="col-12 col-md-8  pt-4 pb-4">
                        <div class="data_frame d-flex justify-content-center" style="flex-wrap:wrap">

                            <canvas id="pie_chart" style="max-width:320px;max-height:320px;" width="100%"></canvas>

                            <div class="d-flex flex-column w-100 col-12 col-md">
                                <h3 class="text-accent font-weight-bold">Total:<span id="total_pass" class="float-right"></span></h3>
                                <hr class="w-100">
                                <h4 class="mb-4 text-accent font-weight-bold">Membership:<span id="membership_passes" class="float-right"></span></h4>
                                <h4 class="mb-4 text-accent font-weight-bold">Coupon:<span id="coupon_passes" class="float-right"></span></h4>
                                <h4 class="mb-4 text-accent font-weight-bold">Event Ticket:<span id="event_ticket_passes" class="float-right"></span></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 pt-4 pb-4">
                        <div class="data_frame h-100">

                            <h3 class="mb-4 text-accent font-weight-bold">Issued:<span id="total_issued" class="pr-2 float-right"></span></h3>
                            <hr>
                            <h4 class="mb-4 text-accent font-weight-bold">Membership:<span id="total_membership" class="pr-2 float-right"></span></h4>
                            <h4 class="mb-4 text-accent font-weight-bold">Coupon:<span id="total_coupon" class="pr-2 float-right"></span></h4>
                            <h4 class="mb-4 text-accent font-weight-bold">Event Ticket:<span id="total_event_ticket" class="pr-2 float-right"></span></h4>
                        </div>
                    </div>
                </div>
                <div class="col-12 row pt-4 pb-4 d-none">
                    <div class="col-8 ">
                        <div class="data_frame">4</div>
                    </div>
                    <div class="col-4 ">
                        <div class="data_frame">5</div>
                    </div>
                </div>
                <div id="top_ten_selection" class="col-12 row pt-4 pb-4 " style="display:none;">
                    <div class="col-12">
                        <div class="data_frame pt-4 pb-4">
                            <div id="table_selection_bar" class="pt-3" style="overflow-x:hidden;white-space:nowrap;">

                            </div>
                            <div id="table_tabs" class="tab-content" style="height:unset!important;min-height:unset!important;background-color:unset!important;border-radius:0px!important;">

                            </div>
                        </div>
                    </div>
                </div>
                <div id="warning_box" class="col-12 row pt-4 pb-4 justify-content-center" style="display:none">
                    <div class="col-12">
                        <div class="data_frame">
                            <h4 class="text-center mt-4 mb-4">Current function is not available for api passes</h4>
                        </div>
                    </div>
                </div>
            </div>
            <!--<img src="<?= base_url() ?>assets/images/temp_merchant_dashboard.png">-->
        </div>
    </div>
</div>

<script src="<?= base_url("public/static/chart-3.8/chart.min.js") ?>"></script>
<script>
    const base_url = "<?= base_url(); ?>";
</script>
<script src="<?= base_url("public/static/merchant/js/dashboard.js") ?>"></script>
<script>
    $("#selection_type").trigger("change");
</script>