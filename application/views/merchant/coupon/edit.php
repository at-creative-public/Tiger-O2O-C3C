<div class="page-body">
    <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5"><?= $page ?></h4>
    <div class="row">
        <div class="col-12 col-md-4">
            <ul class="nav justify-content-center mb-4">
                <li class="nav-item ml-2 mr-2 mb-2"><a href="#" data-preview="apple_front" class="btn btn-default p-2 mobile_preview_button preview_selected">Apple Front</a></li>
                <li class="nav-item ml-2 mr-2 mb-2"><a href="#" data-preview="apple_back" class="btn btn-default p-2 mobile_preview_button">Apple Back</a></li>
                <li class="nav-item ml-2 mr-2 mb-2"><a href="#" data-preview="google_front" class="btn btn-defualt p-2 mobile_preview_button">Google Front</a></li>
                <li class="nav-item ml-2 mr-2 mb-2"><a href="#" data-preview="google_back" class="btn btn-default p-2 mobile_preview_button">Google Back</a></li>
            </ul>
            <div class="w-100 mobile_preview_container" id="apple_front">
                <div class="apple_mobile_preview_container">
                    <div class="phone">
                        <div id="apple_card_frame" class="card_frame d-flex flex-column" style="background:#000000;">
                            <div class="card_header">
                                <div class="col-2 p-2 mr-2">
                                    <img id="apple_logo_image" class="apple_mobile_logo" src="<?= base_url($coupon_setting['logo']) ?>?id=<?= rand(1, 10000) ?>">
                                </div>
                                <div class="col-10 pl-4 text-left">
                                    <span id="apple_header_text" class="apple_header_text apple_phone_label"></span>
                                </div>
                            </div>
                            <div class="card_image">
                                <img id="apple_strip_image" class="apple_strip_image" src="<?= base_url($coupon_setting['strip']) ?>?id=<?= rand(1, 10000) ?>">
                            </div>
                            <div class="data_row">
                                <div class="d-flex apple_data_row" style="overflow:scroll">
                                    <div class="col" id="apple_data_1">
                                        <span id="apple_data_1_label" class="data_label apple_phone_label"></span>
                                        <div id="apple_data_1_data" class="apple_phone_value">value</div>
                                    </div>
                                    <div class="col" id="apple_data_2">
                                        <span id="apple_data_2_label" class="data_label apple_phone_label"></span>
                                        <div id="apple_data_2_data" class="apple_phone_value">
                                            value
                                        </div>
                                    </div>
                                    <div class="col" id="apple_data_3">
                                        <span id="apple_data_3_label" class="data_label apple_phone_label"></span>
                                        <div id="apple_data_3_data" class="apple_phone_value">
                                            value
                                        </div>
                                    </div>
                                    <div class="col" id="apple_data_4">
                                        <span id="apple_data_4_label" class="data_label apple_phone_label"></span>
                                        <div id="apple_data_4_data" class="apple_phone_value">
                                            value
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="qr_code">
                                <div class="qr_container">
                                    <img class="" src="<?= base_url('assets/images/example_qr.png') ?>">
                                    <span class="label mt-2" style="font-size:10px!important;"><span id="coupon_code_display"><?= $coupon_setting['coupon_code'] ?></span> - ID</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 mobile_preview_container" id="apple_back" style="display:none;">
                <div class="apple_mobile_back_preview_container">
                    <div class="phone">
                        <div class="card_frame d-flex flex-column">
                            <div class="col_1">
                                <div>
                                    Our Website
                                </div>
                                <span id="backside_hyperlink" class="backside_hyperlink"><?= $coupon_setting['url'] ?></span>
                            </div>
                            <div class="col_2">
                                <div>
                                    About Us
                                </div>
                                <span id="backside_about_us" class="backside_about_us"><?= $coupon_setting['about_us'] ?></span>
                            </div>
                            <div class="col_3">
                                <div>
                                    Terms & Conditions
                                </div>
                                <span id="backside_terms_conditions" class="backside_terms_conditions"><?= $coupon_setting['terms_conditions'] ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 mobile_preview_container" id="google_front" style="display:none;">
                <div class="google_mobile_preview_container">
                    <div class="phone">
                        <div id="google_card_frame" class="card_frame" style="background-color:#000000">
                            <div class="card_header d-flex">
                                <div class="col-2">
                                    <img id="google_logo_image" class="google_logo_image" src="<?= base_url($coupon_setting["logo"]) ?>?id=<?= rand(1, 10000) ?>">
                                </div>
                                <div class="col-10">
                                    <span id="google_header_text" class="google_phone_text"></span>
                                </div>
                            </div>
                            <div class="data_row">
                                <h4 id="google_card_name" class="m-4 text-left font-weight_bold google_phone_text"><?= $coupon_setting['project_name'] ?></h4>
                                <div id="google_data_row_1" class="row mb-4">
                                    <div id="google_phone_data_1_set" class="col-6">
                                        <h6 id="google_data_1_label" class="ml-4 mr-4 google_phone_text"></h6>
                                        <h5 id="google_data_1_data" class="ml-4 mr-4 google_phone_text">Value</h5>
                                    </div>
                                    <div id="google_phone_data_2_set" class="col-6 ">
                                        <h6 id="google_data_2_label" class="ml-4 mr-4 google_phone_text"></h6>
                                        <h5 id="google_data_2_data" class="ml-4 mr-4 google_phone_text">Value</h5>
                                    </div>
                                </div>
                                <div id="google_data_row_2" class="row mb-4">
                                    <div id="google_phone_data_3_set" class="col-6">
                                        <h6 id="google_data_3_label" class="ml-4 mr-4 google_phone_text"></h6>
                                        <h5 id="google_data_3_data" class="ml-4 mr-4 google_phone_text">Value</h5>
                                    </div>
                                    <div id="google_phone_data_4_set" class="col-6">
                                        <h6 id="google_data_4_label" class="ml-4 mr-4 google_phone_text"></h6>
                                        <h5 id="google_data_4_data" class="ml-4 mr-4 google_phone_text">Value</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="qr_code flex-column">
                                <div class="qr_code_container">
                                    <img src="<?= base_url("assets/images/example_qr.png") ?>">
                                </div>
                                <span class="google_phone_text mt-2" style="font-size:10px"><span id="coupon_coupon_display_google"><?= $coupon_setting['coupon_code'] ?></span>-ID</span>
                            </div>
                            <div class="card_image">
                                <img id="google_hero_image" class="google_card_image" src="<?= base_url($coupon_setting['strip']) ?>?id=<?= rand(1, 10000) ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-100 mobile_preview_container" id="google_back" style="display:none;">
                <div class="google_mobile_back_preview_container">
                    <div class="phone">
                        <div class="backlight">
                            <div class="card_frame">
                                <div clsas="header_block"></div>
                                <div class="pl-4 pr-4 pt-3 pb-3">
                                    Details
                                </div>
                                <div class="pl-4 pr-4 pt-3 pb-3 d-flex align-items-center">
                                    <i style="font-size:20px;color:#0b79ff" class="far fa-globe-americas"></i><span class="pl-4 font-weight-bold">Our Website</span>
                                </div>
                                <div class="pl-4 pr-4 pt-3 pb-3">
                                    <div class="font-weight-bold">About Us</div>
                                    <span id="google_backside_about_us"><?= $coupon_setting['about_us'] ?></span>
                                </div>
                                <div class="pl-4 pr-4 pt-3 pb-3">
                                    <div class="font-weight-bold">Terms & Conditions</div>
                                    <span id="google_backside_terms_conditions"><?= $coupon_setting['terms_conditions'] ?></span>
                                </div>
                                <div class="pl-4 pr-4 pt-3 pb-3">
                                    <p style="font-size:10px;line-height:1;">The pass provider or merchant is responsible for the info on this pass and may send you notifications. Contact them with any questions.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-8">
            <div class="editor">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item first-block">
                        Creator
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" id="setting-tab" data-toggle="tab" href="#setting" role="tab" aria-controls="home" aria-selected="true">
                            <img class="img-fluid" src="<?= base_url("assets/images/merchant/setting.png") ?>">&nbsp;<span class="tab_word">Setting</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="restrict-tab" data-toggle="tab" href="#restrict" role="tab" aria-controls="contact" aria-selected="false">
                            <i class="fa fa-exclamation-triangle dark_blue d-flex align-items-center" style="height:25px!important;" aria-hidden="true"></i><span class="tab_word">&nbsp;&nbsp;Restrict</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="form-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/form.png") ?>">&nbsp;<span class="tab_word">Form</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="header-tab" data-toggle="tab" href="#header" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/header.png") ?>">&nbsp;<span class="tab_word">Header</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="theme-tab" data-toggle="tab" href="#theme" role="tab" aria-controls="false"><img class="img-fliud" src="<?= base_url("assets/images/merchant/theme.png") ?>">&nbsp;<span class="tab_word">Theme</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="layout-tab" data-toggle="tab" href="#layout" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/layout.png") ?>">&nbsp;<span class="tab_word">Layout</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/detail.png") ?>">&nbsp;<span class="tab_word">Details</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="geotag-tab" data-toggle="tab" href="#geotag" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/geotag.png") ?>">&nbsp;<span class="tab_word">Geotag(Beta)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="export-tab" data-toggle="tab" href="#export" role="tab" aria-controls="contact" aria-selected="false"><img class="img-fluid" src="<?= base_url("assets/images/merchant/export.png") ?>">&nbsp;<span class="tab_word">Export</span></a>
                    </li>
                    <li class="nav-item ml-auto end-block">
                        <a class="nav-link" id="help-tab" data-toggle="tab" href="#help" aria-controls="contact" aria-selected="false" style="padding:0px!important;border-radius:50px important;display:flex!important;align-items:center;background-color:transparent!important;border:none!important;">
                            <i class="fas fa-list"></i>&nbsp;<span class="tab_word" style="color:white">Help</span>
                        </a>
                    </li>
                </ul>
                <form id="creator_form" method="post" action="/merchant/coupon/update" enctype="multipart/form-data">
                    <div class="tab-content" id="myTabContent">
                        <button id="next_button" class="btn blue_button" style="border-radius:15px 0px 15px 0px!important;">NEXT</button>
                        <button id="export_button" class="btn blue_button" style="border-radius:15px 0px 15px 0px!important;">Save</button>
                        <div class="tab-pane fade active show" id="setting" role="tabpanel" aria-labelledby="home-tab">
                            <div class="inner_padding">
                                <ul>
                                    <li>
                                        <div class="d-flex align-items-center">
                                            <div class="pr-4">
                                                Type of Coupon
                                            </div>
                                            <select disabled>
                                                <option selected value="coupon">Cash Coupon</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li>
                                        Project Name<span style="color:red">*</span>
                                        <input type="text" id="management_name" name="management_name" class="form-control" value="<?=$coupon_setting['management_name']?>" required>
                                    </li>
                                    <li>
                                        Coupon Name<span style="color:red">*#</span>
                                        <input type="text" id="project_name" name="project_name" class="form-control" value="<?= $coupon_setting['project_name'] ?>" required>
                                    </li>
                                    <li>
                                        Coupon code<span style="color:red">*</span>
                                        <input type="text" id="coupon_code" name="coupon_code" class="form-control" value="<?= $coupon_setting['coupon_code'] ?>" readonly>
                                    </li>
                                    <li>
                                        Coupon amount<span style="color:red">*#</span>
                                        <input type="text" id="coupon_amount" name="coupon_amount" class="form-control" value="<?= $coupon_setting['coupon_amount'] ?>">
                                    </li>
                                    <li>
                                        Coupon expiry date<span style="color:red">*#</span>
                                        <input type="date" id="expiry_date" name="expiry_date" class="form-control" value="<?= $coupon_setting['expiry_date'] ?>">
                                    </li>
                                    <li>
                                        Icon<span style="color:red">*</span>
                                        <input type="file" id="icon_upload" name="icon" class="form-control mb-2" style="height:40px!important;opacity:1!important;" accept=".png">
                                        Recommended size: 29px(W) x 29px(H)<br>
                                        Preview:
                                        <img id="icon_preview_frame" class="img_preview_frame" src="<?= base_url($coupon_setting['icon']) ?>?id=<?= rand(1, 100000) ?>">
                                        <button class="btn invert_color_button">Invert Color</button>
                                    </li>
                                    <li class="d-flex flex-column">
                                        <div>
                                            <h6 style="color:red">* required</h6>
                                        </div>
                                        <div>
                                            <h6 style="color:red"># Updated value will not affect the issued coupons.</h6>
                                        </div>
                                    </li>
                                    <input type="hidden" name="id" value="<?= $coupon_setting['id'] ?>">
                                    <!--<button type="submit" class="btn">submit</button>-->

                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="restrict" role="tabpanel">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        Minimum spend
                                        <input type="number" class="form-control" name="min_spend" id="min_spend">
                                    </li>
                                    <li>
                                        Maximum spend
                                        <input type="number" class="form-control" name="max_spend" id="max_spend">
                                    </li>
                                    <li>
                                        <div class="d-flex align-items-top">
                                            <span class="mr-5" style="min-width:135px;white-space:nowrap;">Individual use only</span>
                                            <input type="checkbox" name="individual_use" id="individual_use" class="ml-2 mr-2 mt-0" style="min-width:18px;margin-top:-6px!important;">
                                            Check this box if the coupon cannot be used in conjunction with other coupons.
                                        </div>
                                        <div class="d-flex align-items-top mt-4">
                                            <span class="mr-5" style="min-width:135px;white-space:nowrap;">Exclude sale items</span>

                                            <input type="checkbox" name="exclude_sale_items" id="exclude_sale_items" class="ml-2 mr-2 " style="min-width:18px;margin-top:-6px!important;">
                                            Check this box if the coupon should not apply to items on sale. Per-item coupons will only work if the item is not on sale. Per-cart coupons will only work if there are items in the cart that are not on sale.

                                        </div>
                                    </li>
                                    <li>
                                        Products
                                        <input type="text" class="form-control" name="products" id="products">
                                    </li>
                                    <li>
                                        Exclude products
                                        <input type="text" class="form-control" name="exclude_products" id="exclude_products">
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        Data Required<br>
                                        <input type="radio" id="data_requirement_simple" name="data_requirement" value="simple" style="display:none">
                                        <input type="radio" id="data_requirement_informative" name="data_requirement" value="informative" style="display:none">
                                        <button class="btn radio_button button_simple selected" value="simple">SIMPLE</button>
                                        <button class="btn radio_button button_informative" value="informative">INFORMATIVE</button>
                                    </li>
                                    <li id="personal_information_table" style="display:none;">Personal Information<br>

                                        <div class="row">
                                            <div class="col-3  mb-2">
                                                <input type="checkbox" id="NAME" name="name" style="display:none;">
                                                <button id="NAME_BUTTON" class="btn checkbox_button" value="NAME">NAME</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="SURNAME" name="surname" style="display:none;">
                                                <button id="SURNAME_BUTTON" class="btn checkbox_button" value="SURNAME">SURNAME</button>
                                            </div>
                                            <div class="col-3  mb-2">
                                                <input type="checkbox" id="GIVEN_NAME" name="given_name" style="display:none;">
                                                <button id="GIVEN_NAME_BUTTON" class="btn checkbox_button" value="GIVEN_NAME">GIVEN NAME</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="ENGLISH_NAME" name="english_name" style="display:none;">
                                                <button id="ENGLISH_NAME_BUTTON" class="btn checkbox_button" value="ENGLISH_NAME">ENGLISH NAME</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="CHINESE_NAME" name="chinese_name" style="display:none;">
                                                <button id="CHINESE_NAME_BUTTON" class="btn checkbox_button" value="CHINESE_NAME">CHINESE NAME</button>
                                            </div>
                                        <!--</div>
                                        <div class="row mb-2">-->
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="GENDER" name="gender" style="display:none;">
                                                <button id="GENDER_BUTTON" class="btn checkbox_button" value="GENDER">GENDER</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="BIRTHDAY" name="birthday" style="display:none;">
                                                <button id="BIRTHDAY_BUTTON" class="btn checkbox_button" value="BIRTHDAY">BIRTHDAY</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="AGE" name="age" style="display:none;">
                                                <button id="AGE_BUTTON" class="btn checkbox_button" value="AGE">AGE</button>
                                            </div>
                                            <div class="col-3 mb-2">
                                                <input type="checkbox" id="NATIONALITY" name="nationality" style="display:none;">
                                                <button id="NATIONALITY_BUTTON" class="btn checkbox_button" value="NATIONALITY">NATIONALITY</button>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="contact_information_table" style="display:none;">
                                        Contact Information

                                        <div class="row mb-2">
                                            <div class="col-3">
                                                <input type="checkbox" id="PHONE" name="phone" style="display:none;">
                                                <button id="PHONE_BUTTON" class="btn checkbox_button" value="PHONE">PHONE</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="EMAIL" name="email" style="display:none;">
                                                <button id="EMAIL_BUTTON" class="btn checkbox_button" value="EMAIL">EMAIL</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="ADDRESS" name="address" style="display:none;">
                                                <button id="ADDRESS_BUTTON" class="btn checkbox_button" name="address" value="ADDRESS">ADDRESS</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="INDUSTRY" name="industry" style="display:none;">
                                                <button id="INDUSTRY_BUTTON" class="btn checkbox_button" value="INDUSTRY">INDUSTRY</button>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="work_information_table" style="display:none;">
                                        Work Information

                                        <div class="row mb-2">
                                            <div class="col-3">
                                                <input type="checkbox" id="INDUSTRY_2" name="industry_2" style="display:none;">
                                                <button id="INDUSTRY_2_BUTTON" class="btn checkbox_button" value="industry_2">INDUSTRY</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="JOB_TITLE" name="job_title" style="display:none;">
                                                <button id="JOB_TITLE_BUTTON" class="btn checkbox_button" value="JOB_TITLE">JOB TITLE</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="COMPANY" name="company" style="display:none;">
                                                <button id="COMPANY_BUTTON" class="btn checkbox_button" value="COMPANY">COMPANY</button>
                                            </div>
                                            <div class="col-3">
                                                <input type="checkbox" id="SALARY" name="salary" style="display:none;">
                                                <button id="SALARY_BUTTON" class="btn checkbox_button" value="SALARY">SALARY</button>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="simple_remark" style="display:block">
                                        Simple includes "English Name","Chinese Name","Phone","Email"
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="theme" role="tabpanel" aria-labelledby="home-tab">
                            <div class="inner_padding">
                                <ul>
                                    <li>
                                        Title Colour<span style="color:red">*</span><br>
                                        <div class="d-flex">
                                            <input type="color" name="secondary_color" id="label_color" value="<?= $coupon_setting['secondary_color'] ?>">
                                            <input type="text" class="form-control ml-4 mr-4" id="secondary_color_hex" value="<?= $coupon_setting['secondary_color'] ?>">
                                        </div>
                                    </li>
                                    <li>
                                        Content Colour<span style="color:red">*</span><br>
                                        <div class="d-flex">
                                            <input type="color" name="pri_color" id="value_color" value="<?= $coupon_setting['pri_color'] ?>">
                                            <input type="text" class="form-control ml-4 mr-4" id="pri_color_hex" value="<?= $coupon_setting['pri_color'] ?>">
                                        </div>
                                    </li>
                                    <li>
                                        Background Colour<span style="color:red">*</span><br>
                                        <div class="d-flex mb-2">
                                            <input type="color" name="background_color" id="background_color" value="<?= $coupon_setting['background_color'] ?>">
                                            <input type="text" class="form-control ml-4 mr-4" id="background_color_hex" value="<?= $coupon_setting['background_color'] ?>">
                                        </div>
                                        <span>Google's pass will assign the text colour base on the background colour.</span>
                                    </li>
                                    <li>
                                        <div class="">
                                            <div class="" style="width:65px;">Banner<span style="color:red">*</span></div>
                                            <div class="col mb-2 p-0"><input type="file" class="form-control" name="strip" id="banner_upload" style="height:40px!important;opacity:1!important;"></div>
                                            Recommended size: 320px(W) x 123px(H)
                                        </div>
                                        <div class="pt-4">
                                            <h6 style="color:red">* required</h6>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="header" role="tabpanel" aria-labelledby="home-tab">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        LOGO<span style="color:red">*</span>
                                        <input type="file" id="logo_upload" name="logo" class="form-control mb-2" style="height:40px!important;opacity:1!important;">
                                        Recommended size: 100px(W) x 100px(H)
                                    </li>
                                    <li>
                                        Header Text<span style="color:red">*</span><br>
                                        <input type="text" class="form-control" id="header_text" name="logo_text" placeholder="Header Content" value="<?= $coupon_setting['logo_text'] ?>" required>
                                        <div class="pt-4">
                                            <h6 style="color:red">* required</h6>
                                        </div>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="layout" role="tabpanel">
                            <div class="inner_padding">
                                <ul>
                                    <li>
                                        Content Data
                                    </li>
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" id="front_data_1_status" name="front_data_1_status" class="data_trigger" <?= $layout_setting['front_data_1_status'] == 1 ? "checked" : "" ?> style="display:none;">
                                                <span class="slider round d-flex align-items-center">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>

                                        <div class="col">
                                            <input type="text" id="front_data_1_label" name="front_data_1_label" class="form-control data_trigger" placeholder="Label 1" value="<?= $layout_setting["front_data_1_label"] ?>">
                                        </div>
                                        <div class="col">
                                            <select class="form-control h-100 clear_select" name="front_data_1_data">
                                                <?php foreach ($data_fields as $index => $field) {
                                                    if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option <?= $layout_setting['front_data_1_data'] == $index ? "selected" : "" ?> value="<?= $index ?>"><?= $field ?></option>
                                                    <?php }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" id="front_data_2_status" name="front_data_2_status" class="data_trigger" <?= $layout_setting['front_data_2_status'] == 1 ? "checked" : "" ?> style="display:none;">
                                                <span class="slider round d-flex align-items-center">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>

                                        <div class="col">
                                            <input type="text" id="front_data_2_label" name="front_data_2_label" class="form-control data_trigger" placeholder="Label 2" value="<?= $layout_setting['front_data_2_label'] ?>">
                                        </div>
                                        <div class="col">
                                            <select class="form-control h-100 clear_select" name="front_data_2_data">
                                                <?php foreach ($data_fields as $index => $field) {
                                                    if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option <?= $layout_setting['front_data_2_data'] == $index ? "selected" : "" ?> value="<?= $index ?>"><?= $field ?></option>
                                                    <?php }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" id="front_data_3_status" name="front_data_3_status" class="data_trigger" <?= $layout_setting['front_data_3_status'] == 1 ? "checked" : "" ?> style="display:none;">
                                                <span class="slider round d-flex align-items-center">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>

                                        <div class="col">
                                            <input type="text" id="front_data_3_label" name="front_data_3_label" class="form-control data_trigger" placeholder="Label 3" value="<?= $layout_setting['front_data_3_label'] ?>">
                                        </div>
                                        <div class="col">
                                            <select class="form-control h-100 clear_select" name="front_data_3_data">
                                                <?php foreach ($data_fields as $index => $field) {
                                                    if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option <?= $layout_setting["front_data_3_data"] == $index ? "selected" : "" ?> value="<?= $index ?>"><?= $field ?></option>
                                                    <?php }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-flex" style="margin:15px 0px;">
                                        <div>
                                            <label class="switch">
                                                <input type="checkbox" id="front_data_4_status" name="front_data_4_status" class="data_trigger" <?= $layout_setting["front_data_4_status"] == 1 ? "checked" : "" ?> style="display:none;">
                                                <span class="slider round d-flex align-items-center">
                                                    <span class="on">ON</span>
                                                    <span class="off">OFF</span>
                                                </span>
                                            </label>
                                        </div>

                                        <div class="col">
                                            <input type="text" id="front_data_4_label" name="front_data_4_label" class="form-control data_trigger" placeholder="Label 4" value="<?= $layout_setting["front_data_4_label"] ?>">
                                        </div>
                                        <div class="col">
                                            <select class="form-control h-100 clear_select" name="front_data_4_data">
                                                <?php foreach ($data_fields as $index => $field) {
                                                    if ($field == "separator") { ?>
                                                        <optgroup label="--<?= $index ?>--">
                                                        <?php } else { ?>
                                                            <option <?= $layout_setting["front_data_4_data"] == $index ? "selected" : "" ?> value="<?= $index ?>"><?= $field ?></option>
                                                    <?php }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="details" role="tabpanel">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        <div class="d-flex w-100">
                                            <div class="p-0" style="width:65px">URL<span style="color:red">*</span></div>
                                            <div class="col"><input type="text" id="website_url" name="url" class="form-control" value="<?= $coupon_setting['url'] ?>" placeholder="Please paste your link"></div>
                                        </div>
                                    </li>
                                    <li>
                                        About Us<span style="color:red">*</span><br>
                                        <textarea class="form-control" id="about_us_content" name="about_us" placeholder="Please type here"><?= $coupon_setting['about_us'] ?></textarea>
                                    </li>
                                    <li>
                                        Terms & Conditions<span style="color:red">*</span>
                                        <textarea class="form-control" id="terms_content" name="terms_conditions" placeholder="Please type here"><?= $coupon_setting['terms_conditions'] ?></textarea>
                                    </li>
                                    <li>
                                        <div class="d-flex">
                                            
                                            <h6 style="color:red">* required</h6>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="geotag" role="tabpanel">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                        Location-based Push Notification<i class="fa fa-info-circle tips pl-2" aria-hidden="true"><span class="tips_content" style="z-index:1000;">How to get the coordinate ?<br>Right click at the destination on Google Maps</span></i>
                                        <div class="d-flex" style="margin:15px 0px">
                                            <div>
                                                <label class="switch">
                                                    <input type="checkbox" name="location_1_status" <?= $location_setting['location_1_status'] == 1 ? "checked" : "" ?> style="display:none;">
                                                    <span class="slider round d-flex align-items-center">
                                                        <span class="on">ON</span>
                                                        <span class="off">OFF</span>
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="col row">
                                                <div class="col">
                                                    <input type="number" step="0.000001" name="location_1_lat" class="form-control" value="<?= $location_setting['location_1_lat'] ?>">
                                                </div>
                                                <div class="col">
                                                    <input type="number" step="0.000001" name="location_1_lon" class="form-control" value="<?= $location_setting['location_1_lon'] ?>">
                                                </div>
                                                <div class="col-12 mt-2">
                                                    <textarea class="form-control" name="location_1_message" class="form-control" placeholder="Message"><?= $location_setting['location_1_message'] ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex" style="margin:15px 0px;">
                                            <div>
                                                <label class="switch">
                                                    <input type="checkbox" name="location_2_status" <?= $location_setting['location_2_status'] == 1 ? "checked" : "" ?> style="display:none;">
                                                    <span class="slider round d-flex align-items-center">
                                                        <span class="on">ON</span>
                                                        <span class="off">OFF</span>
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="col row">
                                                <div class="col">
                                                    <input type="number" step="0.000001" name="location_2_lat" class="form-control" value="<?= $location_setting['location_2_lat'] ?>">
                                                </div>
                                                <div class="col">
                                                    <input type="number" step="0.000001" name="location_2_lon" class="form-control" value="<?= $location_setting['location_2_lon'] ?>">
                                                </div>
                                                <div class="col-12 mt-2">
                                                    <textarea class="form-control" name="location_2_message" class="form-control" placeholder="Message"><?= $location_setting['location_2_message'] ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex" style="margin:15px 0px;">
                                            <div>
                                                <label class="switch">
                                                    <input type="checkbox" name="location_3_status" <?= $location_setting['location_3_status'] ==  1 ? "checked" : "" ?> style="display:none;">
                                                    <span class="slider round d-flex align-items-center">
                                                        <span class="on">ON</span>
                                                        <span class="off">OFF</span>
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="col row">
                                                <div class="col">
                                                    <input type="number" step="0.000001" name="location_3_lat" class="form-control" value="<?= $location_setting['location_3_lat'] ?>">
                                                </div>
                                                <div class="col">
                                                    <input type="number" step="0.000001" name="location_3_lon" class="form-control" value="<?= $location_setting['location_3_lon'] ?>">
                                                </div>
                                                <div class="col-12 mt-2">
                                                    <textarea class="form-control" name="location_3_message" placeholder="Message"><?= $location_setting['location_3_message'] ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <h6 style="color:red">Google: Geofenced notifications are temporarily unavailable for a majority of users while we make platform improvements. We'll completely update this feature in an upcoming release.</h6>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="export" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>
                                    Generated Application URL<span id="copy_alert" style="display:none;">&nbsp;&nbsp;Copied !</span>
                                        <div class="d-flex w-100">
                                            <input type="text" id="generated_url" class="form-control" value="<?= base_url("coupon/registration/" . $coupon_setting['id']) ?>" readonly><button id="copy_button" class="btn blue_button" style="padding-left:15px;padding-right:15px;border-radius:5px!important;margin-left:15px;">COPY</button>
                                        </div>
                                    </li>
                                    <li>
                                    Generated Application QR Code
                                        <div class="row" style="max-width:fit-content;">
                                            <div class="col-12">
                                                <img class="img-fluid w-100" src="<?= base_url("public/images/uploads/coupon/" . $coupon_setting['id'] . "/QR.png") ?>">
                                                <div class="mt-2">
                                                    <a href="#">link</a>
                                                    <h6>For iOS devices, please open with Safari</h6>
                                                    <h6>Please open with Safari or click "download" for installation</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                         <div class="tab-pane fade" id="help" role="tabpanel" aria-labelledby="home-tab" style="overflow-y:scroll;">
                            <div class="inner_padding">
                                <ul class="d-flex flex-column">
                                    <li>Got some problems? Watch the tutorial!<br>
                                        <video controls style="width:100%;height:auto;">
                                            <source src="<?= base_url("/assets/tutorial_video/O2OC3C.m4v") ?>" type="video/mp4">
                                        </video>
                                    </li>
                                    <li>Problem not solved? <a href="#">Contact Us</a></li>

                                </ul>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script src="<?= base_url('assets/js/coupon_add.js') ?>"></script>
<script src="<?= base_url("assets/js/coupon_update.js") ?>"></script>
<script>
    var current_page = "edit";
    var base_url = "<?= base_url() ?>";
    var ori_logo = "<?= base_url($coupon_setting['logo']) ?>";
    var ori_icon = "<?= base_url($coupon_setting['icon']) ?>";
    var ori_banner = "<?= base_url($coupon_setting['strip']) ?>";
    $(document).ready(function() {

        var data_requirement = "<?= $coupon_setting['data_requirement'] ?>";
        switch (data_requirement) {
            case "simple":
                $(".button_simple").click();
                break;
            case "informative":
                $(".button_informative").click();
                break;
        }
        var form_config = <?= $form_config_json ?>;
        for (var i in form_config) {
            if (form_config[i] == 1) {
                $("#" + i.toUpperCase() + "_BUTTON").click();
            }
        }

        var init_value_color = "<?= $coupon_setting['pri_color'] ?>";
        $("#value_color").trigger("input");
        var init_label_color = "<?= $coupon_setting['secondary_color'] ?>";
        $("#label_color").trigger("input");
        var init_background_color = "<?= $coupon_setting["background_color"] ?>";
        update_color(init_background_color);
        $("#header_text").trigger("input");
        google_data_row_update();
        apple_data_row_update();
        $(".apple_phone_label").css("color", init_value_color);
        $(".apple_phone_value").css("color", init_label_color);

    })
</script>