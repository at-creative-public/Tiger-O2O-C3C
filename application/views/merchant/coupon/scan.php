<div class="page-body">
    <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5"><?= $page ?></h4>
    <div class="row">
        <div class="col-12">
            <div class="editor">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item first-block">
                        Control
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" id="points_tab" data-toggle="tab" href="/merchant/coupon/scanning" aria-controls="home" aria-selected="true">Verify</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/merchant/coupon/scan_code">Code-Verify</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="inner_padding">
                    <h4 class="text-danger font-weight-bold">*Current function is not available for api passes</h4>
                        <div id="reader"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="searching_result_modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg- mx-auto" role="document">
            <div class="modal-content bg-accent">
                <div class="modal-content-wrapper">
                    <div class="modal-header p-0 text-white bg-transparent border-0">
                        Searching Result
                    </div>
                    <div class="modal-body mt-5 p-5 bg-white" style="border-radius:30px!important;">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <h4>Coupon Info</h4>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Display Code</div>
                                    <div id="display_code" class="col-auto"></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Coupon Name</div>
                                    <div id="coupon_name" class="col-auto"></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Coupon Amount</div>
                                    <div id="coupon_amount" class="col-auto"></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Expiry Date</div>
                                    <div id="expiry_date" class="col-auto"></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Void Date</div>
                                    <div id="void_date" class="col-auto"></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Status</div>
                                    <div id="status" class="col-auto "></div>
                                </div>
                                <h4 class="mt-4">Account Info</h4>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Surname</div>
                                    <div id="surname" class="col-auto "></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Given Name</div>
                                    <div id="given_name" class="col-auto "></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">English Name</div>
                                    <div id="english_name" class="col-auto "></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Chinese Name</div>
                                    <div id="chinese_name" class="col-auto "></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Gender</div>
                                    <div id="gender" class="col-auto "></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Birthday</div>
                                    <div id="birthday" class="col-auto "></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Age</div>
                                    <div id="age" class="col-auto "></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">nationality</div>
                                    <div id="nationality" class="col-auto "></div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <h4>Contact Info</h4>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Phone</div>
                                    <div id="phone" class="col-auto "></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Email</div>
                                    <div id="email" class="col-auto "></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Address</div>
                                    <div id="address" class="col-auto "></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Industry</div>
                                    <div id="industry" class="col-auto "></div>
                                </div>
                                <h4 class="mt-4">Work Info</h4>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Industry</div>
                                    <div id="industry_2" class="col-auto "></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Job Title</div>
                                    <div id="job_title" class="col-auto "></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Company</div>
                                    <div id="company" class="col-auto "></div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-5 pl-4 text-accent font-weight-normal sub-title">Salary</div>
                                    <div id="salary" class="col-auto "></div>
                                </div>
                            </div>
                            <div class="col-12 d-flex justify-content-center">
                                <form action="/merchant/coupon/void_coupon" method="post">
                                    <input type="hidden" id="form_coupon_id" name="id" value="">
                                    <div id="void_button_zone">
                                        <input type="hidden" name="return_to" value="qr">
                                        <button type="submit" class="btn btn-danger font-weight-bold mr-4" style="color:white!important;">Void</button>
                                    </div>
                                </form>
                                <button id="close_button" class="btn btn-default font-weight-bold">Close</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://unpkg.com/html5-qrcode" type="text/javascript"></script>
<script>
    const html5QrCode = new Html5Qrcode("reader");
    const config = {
        fps: 60,
        qrbox: {
            width: 250,
            height: 250
        }
    };
    const facingMode = "environment";

    function start_camera() {
        html5QrCode.start({
            facingMode: "environment"
        }, config, qrCodeSuccessCallback);
    }
    const qrCodeSuccessCallback = (decodedText, decodedResult) => {
        /* handle success */
        $("#result").val(decodedText);
        console.log(decodedText);
        html5QrCode.stop();
        /*html5QrCode.stop().then((ignore) => {
            // QR Code scanning is stopped.
        }).catch((err) => {
            // Stop failed, handle it.
        });*/
        $.ajax({
            "url": "/merchant/coupon/scan_qr",
            "data": {
                "code": decodedText
            },
            "dataType": "json",
            "success": function(data) {
                if (data.found == "false") {
                    alert("No found");
                    setInterval(
                        start_camera(), 3000)

                } else {
                    alert("found");
                    console.log(data.result);
                    modal_init(data.result);
                    $("#searching_result_modal").modal("show");

                }
            }

        })
    };


    $("#close_button").on("click", function() {
        $("#searching_result_modal").modal("hide");
    })

    $("#searching_result_modal").on("hidden.bs.modal", function() {
        start_camera();
    })

    function modal_init(data) {
        console.log(data);
        $("#display_code").empty();
        $("#display_code").text(data.display_code);
        $("#coupon_name").empty();
        $("#coupon_name").text(data.coupon_name);
        $("#coupon_amount").empty();
        $("#coupon_amount").text(data.coupon_amount);
        $("#expiry_date").empty();
        $('#expiry_date').text(data.expiry_date);
        $("#void_date").empty();
        $("#void_date").text(data.void_date);
        $("#status").empty();
        $("#status").text(data.status);
        $("#surname").empty();
        $("#surname").text(data.surname);
        $("#given_name").empty();
        $("#given_name").text(data.given_name);
        $("#english_name").empty();
        $("#english_name").text(data.english_name);
        $("#chinese_name").empty();
        $("#chinese_name").text(data.chinese_name);
        $("#gender").empty();
        if (data.gender == 1) {
            $('#gender').text("Male");
        } else {
            $("#gender").text("Female");
        }
        $("#birthday").empty();
        $("#birthday").text(data.birthday);
        $("#age").empty();
        $("#age").text(data.age);
        $("#nationality").empty();
        $("#nationality").text(data.nationality);
        $("#phone").empty();
        $("#phone").text(data.phone);
        $("#email").empty();
        $("#email").text(data.email);
        $("#address").empty();
        $("#address").text(data.address);
        $("#industry").empty();
        $("#industry").text(data.industry);
        $("#industry_2").empty();
        $("#industry_2").text(data.industry_2);
        $("#job_title").empty();
        $("#job_title").text(data.job_title);
        $("#company").empty();
        $("#company").text(data.company);
        $("#salary").empty();
        $("#salary").text(data.salary);
        $("#form_coupon_id").val(data.id);
        if (data.status == "void") {
            $("#void_button_zone").hide();
        } else {
            $("#void_button_zone").show();
        }
    }

    $(document).ready(function() {


        // If you want to prefer front camera
        html5QrCode.start({
            facingMode: "environment"
        }, config, qrCodeSuccessCallback);

        // If you want to prefer back camera
        html5QrCode.start({
            facingMode: "environment"
        }, config, qrCodeSuccessCallback);

        // Select front camera or fail with `OverconstrainedError`.
        html5QrCode.start({
            facingMode: {
                exact: "environment"
            }
        }, config, qrCodeSuccessCallback);

        // Select back camera or fail with `OverconstrainedError`.
        html5QrCode.start({
            facingMode: {
                exact: "environment"
            }
        }, config, qrCodeSuccessCallback);
    })
</script>