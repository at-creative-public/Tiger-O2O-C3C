﻿<!DOCTYPE html>
<html>

<head>
    <?php
    $this->load->view('merchant/include/common');
    ?>
</head>

<body>
    <div id="loading">
        <div id="loader">

        </div>
    </div>
    <div class="mobile_menu">
        <div class="d-flex flex-column text-right">
            <div style='float:right;padding-right:30px;'>
                <button id="menu_close_button" class="menu_button" style="color:white;width:fit-content">X</button>
            </div>
        </div>
        <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;">
            <a href="<?= base_url("merchant/preview/dashboard") ?>">Dashboard</a>
        </div>
        <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;">
            <!--<a href="<?= base_url("merchant/virtual_passes") ?>">Virtual Passes</a>-->
            <a href="#vp" data-toggle="collapse" role="button">Virtual Passes</a>
        </div>
        <div class="collapse" id="vp">
            <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;background:#4a5ba3;">
                <a href="<?= base_url("merchant/virtual_passes") ?>">Membership</a>
            </div>
            <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;background:#4a5ba3;">
                <a href="<?= base_url("merchant/coupon") ?>">Coupon</a>
            </div>
            <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;background:#4a5ba3;">
                <a href="<?= base_url("merchant/event_ticket") ?>">Event Ticket</a>
            </div>
        </div>
        <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;">
            <!--<a href="<?= base_url("merchant/smart_memberbase") ?>">Smart MemberBase</a>-->
            <a href="#sm" data-toggle="collapse" role="button">Smart MemberBase</a>
        </div>
        <div class="collapse" id="sm">
            <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;background:#4a5ba3;">
                <a href="<?= base_url("merchant/smart_memberbase") ?>">Membership</a>
            </div>
            <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;background:#4a5ba3;">
                <a href="<?= base_url("merchant/coupon_memberbase") ?>">Coupon</a>
            </div>
            <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;background:#4a5ba3;">
                <a href="<?=base_url("merchant/event_ticket_memberbase")?>">Event Ticket</a>    
            </div>
        </div>
        <!--
        <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;">
            <a href="<?= base_url("merchant/preview/landing_page") ?>">Landing Page</a>
        </div>
        <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;">
            <a href="<?= base_url("merchant/preview/marketplace") ?>">Marketplace</a>
        </div>
        -->
        <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;">
            <a href="#sc" data-toggle="collapse" role="button">Scanning Tools</a>
        </div>
        <div class="collapse" id="sc">
            <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;background:#4a5ba3;">
                <a href="<?= base_url("merchant/smart_memberbase/scanning") ?>">Membership</a>
            </div>
            <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;background:#4a5ba3;">
                <a href="<?= base_url("merchant/coupon/scanning") ?>">Coupon</a>
            </div>
            <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;background:#4a5ba3;">
                <a href="<?=base_url("merchant/event_ticket_memberbase/scan_enter")?>">Event Ticket</a>
            </div>
        </div>
        <div style="float:right;padding-right:30px;padding-top:15px;padding-bottom:15px;border-top:1px solid #d0d0d0;border-bottom:1px solid #d0d0d0;width:100%;text-align:right;">
            <a href="<?= base_url("merchant/login") ?>">Logout</a>
        </div>

    </div>
    <!-- Loading Container -->
    <div class=" loading-container<?php echo $_inactive_loading ? ' loading-inactive' : '' ?>">
        <div class="loader"></div>
    </div>
    <!--  /Loading Container -->
    <!-- Navbar -->
    <div class="o2o-c3c navbar">
        <div class="navbar-inner">
            <div class="navbar-container">
                <!-- Navbar Barnd -->
                <div class="navbar-header pull-left">
                    <a href="#" class="navbar-brand">
                        <small>
                            <img class="imglogo " src="<?php echo base_url(); ?>assets/images/new_logo.png" alt="" />
                        </small>
                    </a>
                </div>
                <!-- /Navbar Barnd -->
                <!-- Sidebar Collapse -->
                <div class="sidebar-collapse dropdown-menu-controller " id="dropdown_menu_controller">
                    <i class="collapse-icon fa fa-bars"></i>
                </div>
                <div class="sidebar-collapse " style="display:none" id="sidebar-collapse">
                    <i class="collapse-icon fa fa-bars"></i>
                </div>
                <div id="navbar-app-name" class="hidden-xs hidden-sm hidden-md"><?= $page ?></div>
                <!-- /Sidebar Collapse -->
                <!-- Account Area and Settings --->
                <?php
                $username = $this->session->userdata('username') ?? '管理員';
                $email = $this->session->userdata('email');
                $avatar = empty($this->session->userdata('image')) ? base_url('public/static/backend/images/backend/icon-admin-user.png') : base_url($this->session->userdata('image'));
                ?>
                <div class="navbar-header pull-right">
                    <div class="navbar-account">
                        <ul class="account-area">
                            <li>
                                <a class="login-area dropdown-toggle" data-toggle="">
                                    <section>
                                        <h2>
                                            <span class="profile">hello，<?php echo $username; ?>!</span>
                                        </h2>
                                    </section>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /Account Area and Settings -->
            </div>
        </div>
    </div>
    <!-- /Navbar -->
    <!-- Main Container -->
    <!-- main content starts -->

    <div class="o2o-c3c main-container container-fluid main-admincontent">
        <!-- Page Container -->
        <div class="page-container">
            <!-- Page Sidebar -->
            <?php
            $this->load->view('merchant/include/menu');
            ?>
            <!-- /Page Sidebar -->
            <!-- Chat Bar -->
            <?php
            //        $this->load->view('at-admin/include/chatbar');
            ?>
            <!-- /Chat Bar -->
            <!-- Page Content -->
            <div class="page-content">
                <?php
                if (isset($_view) && $_view)
                    //var_dump($_view);
                    $this->load->view('merchant/' . $_view);
                ?>
            </div>
            <!-- /Page Content -->
        </div>
        <!-- /Page Container -->
        <!-- Main Container -->
    </div>

    <!-- main content ends -->
    <?php
    $this->load->view('merchant/include/footer');
    ?>


    <script>
        $("#dropdown_menu_controller").on("click", function() {
            $(".mobile_menu").addClass("show");
        })

        $("#menu_close_button").on("click", function() {
            $(".mobile_menu").removeClass("show");
        })
    </script>