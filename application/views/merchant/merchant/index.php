<div class="page-body">
    <div class="row pl-0 pl-lg-5">
        <div class="col-12">
            <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5"><?= $page ?></h4>
            <div class="page-content-section p-5">
                <div class="row pb-5">
                    <div class="col-auto"><span class="pagination pt-2 pb-4">Showing 20 of 1,000 Merchants</span>
                    </div>
                    <div class="col">
                        <div class="row filters align-items-center justify-content-end">
                            <div class="form-group d-inline-block pr-3"> <select name="industry">
                                    <option value="" selected>--Industry--</option>
                                    <option value="artwork">Artworks & Crafts</option>
                                    <option value="automobile">Automobiles</option>
                                    <option value="beauty">Beauty & Salon</option>
                                    <option value="ecommerce">eCommerce</option>
                                    <option value="fashion">Fashion & Textiles</option>
                                    <option value="finance">Finance</option>
                                    <option value="food">Food & Beverages</option>
                                    <option value="furniture">Furnitures</option>
                                    <option value="health">Healthcare Household Goods</option>
                                    <option value="hotel">Hotel & Tourism</option>
                                    <option value="industrial">Industrials & Construction</option>
                                    <option value="it">Information Technology</option>
                                    <option value="logistics">Logistics</option>
                                    <option value="media">Media & Entertainment</option>
                                    <option value="clubhouse">Membership Clubhouse</option>
                                    <option value="properties">Properties</option>
                                    <option value="retail">Retail</option>
                                    <option value="service">Service Sector</option>
                                    <option value="spa">Spa/Gym</option>
                                    <option value="support">Support Services</option>
                                    <option value="telecommunications">Telecommunications</option>
                                    <option value="travel">Travel & Leisure</option>
                                    <option value="others">Others</option>
                                </select> </div>
                            <div class="form-group d-inline-block pr-3"> <select name="status">
                                    <option value="" selected>--Status--</option>
                                    <option value="Enterprise">Enterprise</option>
                                    <option value="Pro">Pro</option>
                                    <option value="Lite">Lite</option>
                                    <option value="Expired">Expired</option>
                                </select> </div>
                            <div class="form-group d-inline-block">
                                <div class="input-group input-group-search pr-3"> <input type="text" class="fa" placeholder="Search">
                                    <div class="input-group-append"> <span class="input-group-text"><i class="fa fa-search"></i></span> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="scroller">
                    <table class="datatable pt-5 w-100">
                        <tr class="first">
                            <td>Merchant ID</td>
                            <td>Password</td>
                            <td>Date Created</td>
                            <td>Valid Until</td>
                            <td>Status</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr style="margin-top: 30px;">
                            <td>
                                <p class="m-0 p-0 font-weight-bold">smartbiz</p>
                                <p class="subtitle">Smart Business</p>
                            </td>
                            <td><div class="text-security">********</div></td>
                            <td>05/11/2021</td>
                            <td>31/12/2022</td>
                            <td><div class="status enterprise">Enterprise</div></td>
                            <td><button class="btn btn-primary"> VIEW </button></td>
                            <td><button class="btn btn-primary"> EDIT </button></td>
                        </tr>
                        <tr style="margin-top: 30px;">
                            <td>
                                <p class="m-0 p-0 font-weight-bold"> kmstudio </p>
                                <p class="subtitle"> KM Studio </p>
                            </td>
                            <td><div class="text-security">********</div></td>
                            <td>05/11/2021</td>
                            <td>31/12/2022</td>
                            <td><div class="status pro">Pro</div></td>
                            <td><button class="btn btn-primary"> VIEW </button></td>
                            <td><button class="btn btn-primary"> EDIT </button></td>
                        </tr>
                        <tr style="margin-top: 30px;">
                            <td>
                                <p class="m-0 p-0 font-weight-bold"> youthretirement </p>
                                <p class="subtitle"> Youth Retirement </p>
                            </td>
                            <td><div class="text-security">********</div></td>
                            <td>05/11/2021</td>
                            <td>31/12/2022</td>
                            <td><div class="status pro">Lite</div></td>
                            <td><button class="btn btn-primary"> VIEW </button></td>
                            <td><button class="btn btn-primary"> EDIT </button></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>