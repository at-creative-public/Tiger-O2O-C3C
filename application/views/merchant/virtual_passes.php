<div class="page-body">
    <h4 class="page-title hidden-lg hidden-xl pt-5 pt-md-0 pb-5">Membership</h4>
    <div class="row">
        <div class="col-12 col-md-4 col-xl-3">
            <a class="item" href="/merchant/virtual_passes/add_virtual_passes">
                <div class="item_container">
                    <div class="box">

                        <span style="font-size:8rem">+</span>

                    </div>
                    <div class="name_container">
                        Create New
                    </div>
                </div>
            </a>
        </div>

        <?php foreach ($cards as $card) { ?>
            <div class="col-12 col-md-4 col-xl-3">

                <div class="item_container">
                    <a class="item" href="/merchant/virtual_passes/edit_virtual_passes/<?= $card['id'] ?>">
                        <div class="box">

                            <div class="d-flex flex-column" style="max-height:350px!important;min-width:220px;max-width:220px;height:100%;width:100%!important;background-color:<?= $card['background_color'] ?>;color:<?= $card['pri_color'] ?>;border-radius:15px;padding:5px;">
                                <div class="row pb-2" style="align-items:center;">
                                    <div class="col-4 ">
                                        <img class="img-fluid w-100" style="border-radius:50%;" src="<?= base_url($card['logo']) ?>?id=<?= rand(100000, 1000) ?>">
                                    </div>
                                    <div class="col-4 p-0">
                                        <small style="color:<?= $card['secondary_color'] ?>"><?= $card['logo_text'] ?></small>
                                    </div>
                                    <!--<div class="col-4 ">
                                        <div>
                                            <label style="color:<?= $card['secondary_color'] ?>">Points</label>
                                        </div>
                                        <div>
                                            <small style="color:<?= $card['pri_color'] ?>">100</small>
                                        </div>
                                    </div> -->
                                </div>
                                <div class="pt-2 pb-2" style="margin-left:-5px;margin-right:-5px;">
                                    <div style="position:absolute;padding:5px;">
                                        <h4 style="color:<?= $card['stripColor'] ?>"><?= $card['pri_value'] ?></h4>
                                        <small style="color:<?= $card['stripColor'] ?>"><?= $card['pri_label'] ?></small>
                                    </div>
                                    <img class="img-fluid w-100" style="height:100%;max-width:100%!important;" src="<?= base_url($card['strip']) ?>?id=<?= rand(100000, 1000) ?>">

                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <div><small style="color:<?= $card['secondary_color'] ?>">Name</small></div>
                                        <div style="color:<?= $card['pri_color'] ?>">text</div>
                                    </div>
                                    <div class="col-4">
                                        <div><small style="color:<?= $card['secondary_color'] ?>">Gender</small></div>
                                        <div style="color:<?= $card['pri_color'] ?>">text</div>

                                    </div>
                                    <div class="col-4">
                                        <div><small style="color:<?= $card['secondary_color'] ?>">Phone</small></div>
                                        <div style="color:<?= $card['pri_color'] ?>">text</div>
                                    </div>
                                </div>
                                <div class="row justify-content-center mt-auto mb-4">
                                    <div class="col-5 p-2" style="border-radius:15px;background-color:white;">
                                        <!--<img class="img-fliud" style="max-width:100%!important;" src="<?= base_url("assets/images/example_qr.png") ?>">-->
                                        <img class="img-fluid" style="max-width:100%!important;" src="<?= base_url("public/images/uploads/membership/" . $card['id'] . "/Merged_QR.png") ?>">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </a>
                    <div class="name_container">
                        <a id="name_<?= $card['id'] ?>" href="/merchant/virtual_passes/edit_virtual_passes/<?= $card['id'] ?>"><?= $card['management_name'] ?></a><span class="edit_name_button" data-id="<?= $card['id'] ?>"> &nbsp;&nbsp;<i class="fas fa-pencil-alt"></i></span>&nbsp;&nbsp;<span class="copy_button" data-id="<?= $card['id'] ?>"><i class="fas fa-copy"></i>&nbsp;&nbsp;</span><span class="delete_button" data-id="<?= $card['id'] ?>"><i class="fas fa-trash-alt"></i></span>
                    </div>
                </div>

            </div>
        <?php } ?>

    </div>

</div>

<div class="modal fade" id="rename_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg mx-auto" role="document">
        <div class="modal-content bg-accent">
            <div class="modal-content-wrapper">
                <div class="modal-header p-0 text-white bg-transparent border-0">
                    Rename Membership project name
                </div>
                <div class="modal-body mt-5 p-5 bg-white" style="border-radius:30px!important;">
                    <div class="form-group">
                        <input type="text" class="form-control" id="new_membership_name" placeholder="Please enter the new membership programme name">
                    </div>
                    <div class="d-flex">
                        <button class="btn btn-default font-weight-bold ml-auto" id="submit_button" style="background-color:#4a5ba3!important;color:white!important;">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script>
    var base_url = "<?= base_url() ?>";
    var selected_membership_id = "";

    $(".copy_button").on("click", function() {
        $("#loading").show();
        $.ajax({
            url: base_url + "merchant/virtual_passes/duplicate_pass/" + $(this).data('id'),
            error: function() {
                alert("Something is wrong");
            },
            success: function(data) {
                location.reload();
            }
        })

    })
    $(".delete_button").on("click", function() {
        console.log("This is delete");
        console.log($(this).data("id"));
        if (confirm("Are you sure to delete this record?")) {
            $.ajax({
                url: base_url + "merchant/virtual_passes/delete/" + $(this).data("id"),
                error: function() {
                    alert("Something is wrong");
                },
                success: function(data) {
                    location.reload();
                }
            });
        }
    })

    $(".edit_name_button").on("click", function() {
        $("#new_membership_name").val("");
        $("#rename_modal").modal("show");
        selected_membership_id = $(this).data('id');
    })

    $("#submit_button").on('click', function() {
        if ($("#new_membership_name").val() == "") {
            alert("Please enter the new project name");
            return;
        }
        if (confirm("Are you sure to rename this membership project name?")) {
            $.ajax({
                url: base_url + "merchant/virtual_passes/rename_project",
                data: {
                    "id": selected_membership_id,
                    "new_name": $("#new_membership_name").val()
                },
                method:"post",
                dataType:"json",
                success: function(data) {
                    $("#rename_modal").modal("hide");
                    console.log(data);
                    $("#name_"+data.id).text(data.new_name);
                },
                error:function(){

                }

            })
        }
    })
</script>