<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?= base_url('public/static/bootstrap-4.3.1-dist/css/bootstrap.min.css') ?>" rel="stylesheet" />
</head>

<body>
    <div class="container">
        <div style="min-height:100px;background-color:<?=$card_info['background_color']?>;color:<?=$card_info['pri_color']?>;text-align:center;border-radius:15px;padding-top:30px;padding-bottom:30px;">
            <img class="img-fluid" style="max-height:50px;" src="<?=base_url($card_info['logo'])?>">
            <h4><?=$card_info['project_name']?></h4>
        </div>
        <div style="padding:15px;">
        <form action="/registration/registration" method="post">
            <div class="form-group">
                <label class="control-label">姓名*</label>
                <input type="text" class="form-control" name="name" required>
            </div>
            <div class="form-group">
                <label class="control-label">性別*</label>
                <select class="form-control" name="gender">
                    <option value="1">男</option>
                    <option value="0">女</option>
                </select>
            </div>
            <div class="form-group">
                <label class="control-label">電話號碼*</label>
                <input type="text" class="form-control" name="phone" required>
            </div>
            <div class="form-group">
                <label class="control-label">電郵地址*</label>
                <input type="email" class="form-control" name="email" required>
            </div>
            <div class="form-group">
                <label class="control-label">地址*</label>
                <input type="text" class="form-control" name="address" required>
            </div>
            <div class="form-group">
                <small>*必須填寫</small>
            </div>
            <div class="form-group" style="text-align:center;">
                                <input type="hidden" name="pass_id" value="<?=$pass_id?>">
                <input type="hidden" name="platform" value="<?= $platform ?>">
                <button type="submit" class="btn btn-success">領取會員卡</button>
            </div>
        </form>
        </div>
    </div>
</body>

</html>