<?php

/**
 * @property CI_Session $session
 */
class MY_Model extends CI_Model
{
    protected $user_idname = 'user_id';
    public $table_prefix = 'tbl_';
    public $tbl = '';
    public $fillable = [];
    public $add_select_after_join = TRUE;


    public function join_model($his_modelname, $my_fieldname, $his_fieldname)
    {
        $this->load->model($his_modelname);
        $this->his = $this->$his_modelname;

        $this->db->join(
            $this->his->table_prefix . $this->his->tbl,
            sprintf(
                '%s.%s = %s.%s',
                $this->table_prefix . $this->tbl,
                $my_fieldname,
                $this->his->table_prefix . $this->his->tbl,
                $his_fieldname
            )
        );
        if ($this->add_select_after_join) $this->db->select($this->his->table_prefix . $this->his->tbl . '.*');

        return $this;
    }

    public function filter_params($params)
    {
        if (!empty($params)) {
            $keys = array_flip($this->fillable);
            $output = array_intersect_key($params, $keys);
            return $output;
        }
        return $params;
    }

    /**
     * Call row_array() or result_array()
     * 
     * @param array $where
     * @param int	$limit
     * @param int	$offset
     * @return CI_DB_mysqli_result
     */
    public function get($where = [], $limit = NULL, $offset = NULL)
    {
        $res = $this->db->get_where($this->table_prefix . $this->tbl, $where, $limit, $offset);
        // var_dump($res->row_array()); die();

        return $res;
    }

    public function get_id($id, $limit = NULL, $offset = NULL)
    {
        return $this->get(['id' => $id], $limit, $offset);
    }
    public function add($params)
    {
        $this->db->insert($this->table_prefix . $this->tbl, $params);
        $params['id'] = $this->db->insert_id();
        return $params;
    }

    public function add_userstamp($params)
    {
        $time = time();
        $admin_id = $this->session->userdata($this->user_idname);

        $params['updated_at'] = $time;
        $params['created_at'] = $time;
        $params['updated_by'] = $admin_id;
        $params['created_by'] = $admin_id;

        return $this->add($params);
    }

    public function update($params, $where = [], $limit = NULL)
    {
        $this->db->update($this->table_prefix . $this->tbl, $params, $where, $limit);

        return $params;
    }
    public function update_id($params, $id, $limit = NULL)
    {
        return $this->update($params, ['id' => $id], $limit);
    }
    public function update_userstamp_id($params, $id, $limit = NULL)
    {
        $time = time();
        $admin_id = $this->session->userdata($this->user_idname);

        $params['updated_at'] = $time;
        $params['updated_by'] = $admin_id;
        unset($params['created_at']);
        unset($params['created_by']);

        return $this->update_id($params, $id, $limit);
    }

    public function delete($where = [], $limit = NULL, $reset_data = TRUE)
    {
        return $this->db->delete($this->table_prefix . $this->tbl, $where, $limit, $reset_data);
    }
    public function delete_id($id, $limit = NULL, $reset_data = TRUE)
    {
        return $this->delete(['id' => $id], $limit, $reset_data);
    }
    public function soft_delete($params, $where, $limit = NULL)
    {
        if (empty($where)) throw new Exception('Cannot soft delete on empty where');
        $time = time();
        $admin_id = $this->session->userdata($this->user_idname);

        $params['deleted_at'] = $time;
        $params['updated_at'] = $time;
        $params['updated_by'] = $admin_id;
        unset($params['created_at']);
        unset($params['created_by']);

        if ($this->get($where)->num_rows()) {
            return $this->update($params, $where, $limit);
        } else {
            return $params;
        }
    }
    public function soft_delete_id($params, $id, $limit = NULL)
    {
        return $this->soft_delete($params, ['id' => $id], $limit);
    }
}
