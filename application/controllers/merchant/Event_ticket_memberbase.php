<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/application/controllers/" . "Config.php";
require_once "Services.php";

require dirname(__DIR__) . "../../libraries/phpqrcode/qrlib.php";

use Sunaoka\PushNotifications\Drivers\APNs;
use Sunaoka\PushNotifications\Pusher;

class Event_ticket_memberbase extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['id'])) {
            redirect("timeout");
        }
        $this->load->model("Event_ticket_model");
        $this->load->model("Event_ticket_memberbase_model");
        $this->load->model("Email_management_model");
    }

    public function index()
    {
        $data["_inactive_loading"] = true;
        $data['page'] = "Event Ticket Memberbase";
        $data["_view"] = "/event_ticket_memberbase/index";
        $data['tickets'] = $this->Event_ticket_model->get_event_tickets_all($_SESSION['id']);
        $this->load->view("merchant/index", $data);
    }

    public function add()
    {
        $data["_inactive_loading"] = true;
        $data['page'] = "Event Ticket Memberbase";
        $data["_view"] = "/event_ticket_memberbase/add";
        $data['tickets'] = $this->Event_ticket_model->get_event_tickets($_SESSION['id']);
        $this->load->model("Virtual_passes_model");
        $data['membership_program'] = $this->Virtual_passes_model->get_cards_by_id($_SESSION['id']);
        $this->load->view("merchant/index", $data);
    }

    public function insert()
    {
        $request = $this->input->post();
        $this->load->model("Registration_model");
        $insert_id = $this->Registration_model->event_ticket_registration($request);

        $service = new Services();
        $ticket_info = $this->Event_ticket_model->get_event_ticket($request['event_ticket_id']);
        $layout_info = $this->Event_ticket_model->get_layout($request['event_ticket_id']);
        $holder_info = $this->Event_ticket_memberbase_model->get_holder($insert_id);
        $vertical = "EVENTTICKET";
        $objectUid = $vertical . "_OBJECT_" . uniqid("", true);
        $objectId = sprintf("%s.%s", ISSUER_ID, $objectUid);
        $this->Event_ticket_memberbase_model->update_object_id($objectId, $insert_id);
        $link = $service->makeObject($insert_id, $insert_id, $ticket_info, $holder_info, $layout_info, [], $objectId, $vertical);
        $this->Event_ticket_memberbase_model->store_url($link, $insert_id);

        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . "/assets/user_qr/event_ticket/" . $insert_id . "/")) {
            mkdir($_SERVER['DOCUMENT_ROOT'] . "/assets/user_qr/event_ticket/" . $insert_id . "/", 0755);
        }
        $hash = "";
        for ($i = 1; $i <= 8; $i++) {
            $hash .= sha1($hash) . sha1($hash . time());
        }
        QRcode::png(base_url("event_ticket/retrieve/apple/" . $insert_id . "?v=" . $hash), $_SERVER["DOCUMENT_ROOT"] . "/assets/user_qr/event_ticket/" . $insert_id . "/apple.png");
        $this->Event_ticket_memberbase_model->store_apple_qr("/assets/user_qr/event_ticket/" . $insert_id . "/apple.png", $insert_id, $hash);
        QRcode::png("https://pay.google.com/gp/v/save/" . $link, $_SERVER["DOCUMENT_ROOT"] . "/assets/user_qr/event_ticket/" . $insert_id . "/google.png");
        $this->Event_ticket_memberbase_model->store_google_qr("/assets/user_qr/event_ticket/" . $insert_id . "/google.png", $insert_id);


        redirect("/merchant/event_ticket_memberbase");
    }

    public function edit($id)
    {
        $range = $this->Event_ticket_model->get_range($_SESSION['id']);
        $user_info = $this->Event_ticket_memberbase_model->get_id($id);
        if ($user_info === null) {
            redirect("/merchant/event_ticket_memberbase/no_permission");
        } else {
            if (in_array($user_info['event_ticket_id'], $range)) {
                $data['_inactive_loading'] = true;
                $data['page'] = "Event Ticket Memberbase";
                $data["holder_info"] = $user_info;
                $data['ticket_info'] = $this->Event_ticket_model->get_event_ticket($user_info['event_ticket_id']);
                if ($data["ticket_info"]['event_ticket_type'] == 1) {
                    $data["_view"] = "/event_ticket_memberbase/edit_api";
                } else {
                    $data["_view"] = '/event_ticket_memberbase/edit';
                }
                $this->load->view("merchant/index", $data);
            } else {
                redirect("/merchant/event_ticket_memberbase/no_permission");
            }
        }
    }

    public function update()
    {
        $request = $this->input->post();
        $range = $this->Event_ticket_model->get_range($_SESSION['id']);
        $user_info = $this->Event_ticket_memberbase_model->get_id($request['id']);
        if ($user_info === null) {
            redirect("/merchant/event_ticket_memberbase/no_permission");
        } else {
            if (in_array($user_info['event_ticket_id'], $range)) {
                $this->Event_ticket_memberbase_model->update_info($request, $request['id']);
                $user_info = $this->Event_ticket_memberbase_model->get_id($request['id']);
                if ($user_info['device_token'] !== "") {
                    $this->update_call($user_info);
                }
                if ($user_info['object_id'] !== "") {
                    $services = new Services();
                    $data_info = $this->Event_ticket_model->get_event_ticket($user_info['event_ticket_id']);
                    $layout_info = $this->Event_ticket_model->get_layout($user_info['event_ticket_id']);
                    $users[] = $user_info;
                    $services->update_event_ticket_object($users, $data_info, $layout_info);
                }
                redirect("/merchant/event_ticket_memberbase");
            } else {
                redirect("/merchant/event_ticket_memberbase/no_permission");
            }
        }
    }

    public function listing()
    {
        $request = $this->input->get();
        echo json_encode($this->Event_ticket_memberbase_model->build_datatables($request['draw'], $this->Event_ticket_memberbase_model->totalCount($request['ticket_id']), $this->Event_ticket_memberbase_model->filteredCounter($request, $request['ticket_id']), $this->Event_ticket_memberbase_model->listing($request, $request['ticket_id']), JSON_UNESCAPED_UNICODE));
    }

    public function detail()
    {
        $id = $this->input->get("id");
        $range = $this->Event_ticket_model->get_range($_SESSION['id']);
        $user_info = $this->Event_ticket_memberbase_model->get_id($id);
        if ($user_info === null) {
            echo "Access Denied";
            exit;
        } else {
            if (in_array($user_info['event_ticket_id'], $range)) {
                echo json_encode($user_info);
            } else {
                echo "Access Denied";
                exit;
            }
        }
    }

    public function scan()
    {
        $data['_inactive_loading'] = true;
        $data['page'] = "Event Ticket";
        $data['_view'] = "/event_ticket_memberbase/scan_default";
        $this->load->view("merchant/index", $data);
    }

    public function scan_enter()
    {
        $data['_inactive_loading'] = true;
        $data["page"] = "Event Ticket";
        $data['_view'] = "/event_ticket_memberbase/scan_enter";
        $this->load->view("merchant/index", $data);
    }

    public function scan_quit()
    {
        $data["_inactive_loading"] = true;
        $data["page"] = "Event Ticket";
        $data['_view'] = "/event_ticket_memberbase/scan_exit";
        $this->load->view("merchant/index", $data);
    }

    public function scan_code_enter()
    {
        $data['_inactive_loading'] = true;
        $data['page'] = "Event Ticket";
        $data['_view'] = "/event_ticket_memberbase/scan_code_enter";
        $this->load->view("merchant/index", $data);
    }

    public function scan_code_quit()
    {
        $data['_inactive_loading'] = true;
        $data['page'] = "Event Ticket";
        $data['_view'] = "/event_ticket_memberbase/scan_code_exit";
        $this->load->view("merchant/index", $data);
    }



    public function scanning()
    {
        $request = $this->input->post();
        $ids = $this->Event_ticket_model->get_range($_SESSION['id']);
        $result = $this->Event_ticket_memberbase_model->search_user($request['code'], $ids);
        if ($result == []) {
            echo json_encode(["found" => "false"]);
        } else {
            $user_info = $this->Event_ticket_memberbase_model->get_id($result['id']);
            if (($user_info['enter_time'] != "" && $request['mode'] == "enter") || ($user_info['quit_time'] != "" && $request['mode'] == "exit")) {
                echo json_encode(['found' => "true", "repeat" => "true"]);
            } else {

                switch ($request["mode"]) {

                    case "enter":
                        $this->Event_ticket_memberbase_model->update_enter_record($request['code']);
                        break;

                    case "exit":
                        $this->Event_ticket_memberbase_model->update_exit_record($request['code']);
                        break;
                    default:
                }
                $event_info = $this->Event_ticket_model->get_event_ticket($user_info["event_ticket_id"]);
                echo json_encode(["found" => "true", "repeat" => "false", "result" => $user_info, "event_info" => $event_info]);
            }
        }
    }

    public function quick_mail()
    {
        $id = $this->input->post("id");
        $range = $this->Event_ticket_model->get_range($_SESSION['id']);
        $user_info = $this->Event_ticket_memberbase_model->get_holder($id);
        if ($user_info == []) {
            //redirect("/merchant/event_ticket_memberbase/no_permission");
            echo "Access Denied";
        } else {
            if (in_array($user_info["event_ticket_id"], $range)) {
                $event_info = $this->Event_ticket_model->get_event_ticket($user_info['event_ticket_id']);
                $this->__email_action($user_info, $event_info);
                $this->Email_management_model->add_email_record($_SESSION['id'], time(), "Invitation", "Event_ticket", $user_info['id']);
                //redirect("/merchant/event_ticket_memberbase");
                echo json_encode(["success" => true]);
            } else {
                //redirect("/merchant/event_ticket_memberbase/no_permission");
                echo "Access Denied";
            }
        }
    }

    public function send_email($id)
    {
        $range = $this->Event_ticket_model->get_range($_SESSION['id']);
        $user_info = $this->Event_ticket_memberbase_model->get_holder($id);
        if ($user_info == []) {
            redirect("/merchant/event_ticket_memberbase/no_permission");
        } else {
            if (in_array($user_info["event_ticket_id"], $range)) {
                $event_info = $this->Event_ticket_model->get_event_ticket($user_info['event_ticket_id']);
                $this->__email_action($user_info, $event_info);
                $this->Email_management_model->add_email_record($_SESSION['id'], time(), "Invitation", "Event_ticket", $user_info['id']);
                redirect("/merchant/event_ticket_memberbase");
            } else {
                redirect("/merchant/event_ticket_memberbase/no_permission");
            }
        }
    }

    public function __email_action($user_info, $event_info)
    {
        require dirname(__DIR__) . "/../libraries/sendgrid/sendgrid-php.php";
        require_once dirname(__DIR__) . '/../libraries/Document.php';
        global $p, $doc, $a, $c;
        $email = new \SendGrid\Mail\Mail();

        $doc = new Document();
        $doc->loadHTMLFile('application/libraries/email.html');
        $doc->getElementsByTagName('title')->item(0)->nodeValue = 'Contact';

        $c = $doc->getElementById("event_name");
        $c->nodeValue = $event_info['event_name'];

        $c = $doc->getElementById("email_banner");
        $c->setAttribute("src", base_url($event_info['email_banner']) . "?id=" . rand(1, 999999999));
        $c = $doc->getElementById("apple_link");
        $c->setAttribute("href", base_url("event_ticket/retrieve/apple/" . $user_info["id"] . "?v=" . $user_info['hash']));
        $c = $doc->getElementById("apple_qr");
        $c->setAttribute("src", base_url($user_info['apple_qr']));
        $c = $doc->getElementById("google_link");
        $c->setAttribute("href", "https://pay.google.com/gp/v/save/" . $user_info['google_re']);
        $c = $doc->getElementById("google_qr");
        $c->setAttribute("src", base_url($user_info["google_qr"]));



        $html = $doc->saveHTML();
        $subj = "電子門票 Event Ticket //  " . $event_info['event_name'] . " - Powered by O2OC3C";

        $email->setFrom($this->config->item("sendgrid_from"), $this->config->item('sendgrid_user_name'));
        $email->setSubject($subj);
        $email->addTo($user_info['email']);

        $email->addContent(
            "text/html",
            $html
        );
        $sendgrid = new \SendGrid($this->config->item("sendgrid_api_key"));
        try {
            $response = $sendgrid->send($email);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function update_call($user_info)
    {
        $payload = [
            'aps' => [
                'alert' => [
                    'title' => 'Game Request',
                    'body'  => 'Bob wants to play poker',
                ],
            ],
        ];

        $options = new APNs\Token\Option();
        $options->payload = $payload;
        $options->authKey = FCPATH . 'apple_pay_passes/' . $this->config->item("apple_key");
        $options->keyId = $this->config->item("apple_cert_key_id");
        $options->teamId = $this->config->item("team_id");
        $options->topic = $this->config->item("topic");
        $driver = new APNs\Token($options);
        $pusher = new Pusher(true);
        $feedback = $pusher->to($user_info['device_token'])->send($driver);
    }

    public function void()
    {
        $request = $this->input->post();
        $range = $this->Event_ticket_model->get_range($_SESSION['id']);
        $target = $this->Event_ticket_memberbase_model->get_holder($request['id']);
        if ($target === null) {
            //redirect("/merchant/event_ticket_memberbase/no_permission");
            echo "Access Denied";
        } else {
            if (in_array($target['event_ticket_id'], $range)) {
                $this->Event_ticket_memberbase_model->void($request["id"]);
                $user_info = $this->Event_ticket_memberbase_model->get_holder($request['id']);
                if ($user_info['device_token'] !== "") {
                    $this->update_call($user_info);
                }
                if ($user_info['object_id'] !== "") {
                    $services = new Services();
                    $data_info = $this->Event_ticket_model->get_event_ticket($user_info['event_ticket_id']);
                    $layout_info = $this->Event_ticket_model->get_layout($user_info['event_ticket_id']);
                    $users[] = $user_info;
                    $services->update_event_ticket_object($users, $data_info, $layout_info);
                    echo json_encode(["success" => true]);
                }
                //redirect("/merchant/event_ticket_memberbase");
            } else {
                //redirect("/merchant/event_ticket_memberbase/no_permission");
                echo "Access Denied";
            }
        }
    }

    public function export_csv()
    {
        $ticket_id = $this->input->get("ticket_id");
        $range = $this->Event_ticket_model->get_range($_SESSION['id']);
        if (in_array($ticket_id, $range)) {
            $this->Event_ticket_memberbase_model->export_csv($ticket_id);
        } else {
            redirect("/merchant/event_ticket_memberbase/no_permission");
        }
    }

    public function simple_get()
    {
        $id = $this->input->post("id");
        $range = $this->Event_ticket_model->get_range($_SESSION['id']);
        $user_info = $this->Event_ticket_memberbase_model->get_holder($id);
        if ($user_info == []) {
            //redirect("/merchant/event_ticket_memberbase/no_permission");
            echo "Access Denied";
        } else {
            if (in_array($user_info["event_ticket_id"], $range)) {
                echo json_encode($user_info);
            } else {
                echo "Access Denied";
            }
        }
    }

    public function no_permission()
    {
        $data['_view'] = "no_permission";
        $data['_inactive_loading'] = true;
        $data['page'] = "No_permission";
        $this->load->view("merchant/index", $data);
    }
}
