<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/application/controllers/" . "Config.php";
require_once "Services.php";

use Sunaoka\PushNotifications\Drivers\APNs;
use Sunaoka\PushNotifications\Pusher;

class Smart_memberbase extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Virtual_passes_model");
        $this->load->model("Smart_memberbase_model");
        if (!isset($_SESSION['id'])) {
            redirect("timeout");
        }
    }

    public function index()
    {
        $data['page'] = "Smart MemberBase";
        $data['_inactive_loading'] = true;
        $data["_view"] = "smart_memberbase";
        $this->load->model("Virtual_passes_model");
        $this->load->model("Smart_memberbase_model");
        $data["cards"] = $this->Virtual_passes_model->get_cards_by_id_all($_SESSION['id']);
        $this->load->view("merchant/index", $data);
    }

    public function get_id($id)
    {
        $this->load->model("Smart_memberbase_model");
        $this->load->model("Virtual_passes_model");
        $range = $this->Virtual_passes_model->get_range($_SESSION['id']);

        $info = $this->Smart_memberbase_model->get_id($id);
        if ($info === null) {
            exit;
        } else {
            if (in_array($info['pass_id'], $range)) {
                if ($info['api'] == 1) {
                } else {
                    $info['user_level'] = $this->Virtual_passes_model->get_user_level($info['level']);
                }
                echo json_encode($info);
            } else {
            }
        }
    }

    public function edit($id)
    {
        $this->load->model("Smart_memberbase_model");
        $this->load->model("Virtual_passes_model");
        $range = $this->Virtual_passes_model->get_range($_SESSION['id']);
        $member_info = $this->Smart_memberbase_model->get_id($id);
        if ($member_info === null) {
            redirect("/merchant/smart_memberbase/no_permission");
        } else {
            if (in_array($member_info['pass_id'], $range)) {
                $data['member_info'] = $member_info;
                $data['pass_info'] = $this->Virtual_passes_model->get_id($data['member_info']['pass_id']);
                $data['pass_levels'] = $this->Virtual_passes_model->get_all_level_setting($data['member_info']['pass_id']);
                $data["_inactive_loading"] = true;
                $data['page'] = "Smart Memberbase Edit";
                if ($this->Virtual_passes_model->check_type($data['member_info']['pass_id'])) {
                    $data['_view'] = "smart_memberbase_edit_api";
                } else {
                    $data['_view'] = "smart_memberbase_edit";
                }
                $this->load->view("merchant/index", $data);
            } else {
                redirect("/merchant/smart_memberbase/no_permission");
            }
        }
    }

    public function update_info()
    {
        $request = $this->input->post();
        $this->load->model("Smart_memberbase_model");
        $this->load->model("Virtual_passes_model");
        $this->Smart_memberbase_model->update_info($request, $request['id']);

        $user_info = $this->Smart_memberbase_model->get_id($request['id']);
        if ($user_info['platform'] == "apple") {
            $token_info = $this->Smart_memberbase_model->get_user_token($user_info["id"]);
            $this->update_i_call($token_info);
        } else {
            $object_info = $this->Smart_memberbase_model->get_user_object($user_info['id']);
            $user_info['object_uid'] = $object_info['object_uid'];
            $users[] = $user_info;
            $card_info = $this->Virtual_passes_model->get_card($user_info['pass_id']);
            $level_info = $this->Virtual_passes_model->get_all_level_setting($user_info['pass_id']);
            $layout_info = $this->Virtual_passes_model->get_front_setting($user_info['pass_id']);
            $services = new Services();
            $services->update_loyalty_object($users, $card_info, $layout_info, $level_info);
        }

        redirect("/merchant/smart_memberbase/edit/" . $request['id']);
    }

    public function listing()
    {
        $request = $this->input->get();
        $this->load->model("Smart_memberbase_model");
        $this->load->model("Virtual_passes_model");
        $cards = $this->Virtual_passes_model->get_cards_by_id($_SESSION['id']);
        $card_ids = $request['pass_id'];
        echo json_encode($this->Smart_memberbase_model->build_datatables($request['draw'], $this->Smart_memberbase_model->totalCount($card_ids), $this->Smart_memberbase_model->filteredCounter($request, $card_ids), $this->Smart_memberbase_model->listing($request, $card_ids)), JSON_UNESCAPED_UNICODE);
    }

    public function delete($id)
    {
        $this->load->model("Smart_memberbase_model");
        $this->Smart_memberbase_model->delete($id);
    }

    public function push_notification($id)
    {


        $this->reset_call($id);
    }

    public function reset_call($user_info)
    {
        $payload = [
            'aps' => [
                'alert' => [
                    'title' => 'Game Request',
                    'body'  => 'Bob wants to play poker',
                ],
            ],
        ];
        $user = $this->db->select("*")->from("tbl_temp_registration")->where("serialnumber", $user_info)->get()->row_array();
        $options = new APNs\Token\Option();
        $options->payload = $payload;
        $options->authKey = FCPATH . 'apple_pay_passes/' . $this->config->item("apple_key");
        $options->keyId = $this->config->item("apple_cert_key_id");
        $options->teamId = $this->config->item("team_id");
        $options->topic = $this->config->item("topic");

        $driver = new APNs\Token($options);
        $pusher = new Pusher(true);
        $feedback = $pusher->to($user['pushToken'])->send($driver);
        //var_dump($feedback);
        //$push_ids = $this->db->distinct()->select("pushToken")->from("tbl_temp_registration")->get()->result_array();
        //foreach ($push_ids as $id) {
        //    $pusher = new Pusher(true);
        //    $feedback = $pusher->to($id['pushToken'])->send($driver);
        //}
    }

    public function update_i_call($token_info)
    {
        $payload = [
            'aps' => [
                'alert' => [
                    'title' => 'Game Request',
                    'body'  => 'Bob wants to play poker',
                ],
            ],
        ];

        $options = new APNs\Token\Option();
        $options->payload = $payload;
        $options->authKey = FCPATH . 'apple_pay_passes/' . $this->config->item("apple_key");
        $options->keyId = $this->config->item("apple_cert_key_id");
        $options->teamId = $this->config->item("team_id");
        $options->topic = $this->config->item("topic");

        $driver = new APNs\Token($options);

        $pusher = new Pusher(true);
        $feedback = $pusher->to($token_info['pushToken'])->send($driver);
    }

    public function update_call()
    {
        $payload = [
            'aps' => [
                'alert' => [
                    'title' => 'Game Request',
                    'body'  => 'Bob wants to play poker',
                ],
            ],
        ];

        $options = new APNs\Token\Option();
        $options->payload = $payload;
        $options->authKey = FCPATH . 'apple_pay_passes/' . $this->config->item("apple_key");
        $options->keyId = $this->config->item("apple_cert_key_id");
        $options->teamId = $this->config->item("team_id");
        $options->topic = $this->config->item("topic");

        $driver = new APNs\Token($options);
        $push_ids = $this->db->distinct()->select("pushToken")->from("tbl_temp_registration")->get()->result_array();
        foreach ($push_ids as $id) {
            $pusher = new Pusher(true);
            $feedback = $pusher->to($id['pushToken'])->send($driver);
        }
    }

    public function listing_pass_member()
    {
        $params = $this->input->post();

        $this->load->model("Smart_memberbase_model");
        $result = $this->Smart_memberbase_model->listing_pass_member($params);
        echo json_encode($result);
    }

    public function adding_message()
    {
        $request = $this->input->post();
        $this->load->model("Smart_memberbase_model");
        foreach ($request['ids'] as $id) {
            $params = [
                "serial" => $id,
                "message" => $request['message'],
                "timestamp" => time(),
                "sent" => 0
            ];
            $this->Smart_memberbase_model->adding_message($params);
        }
    }

    public function export_csv()
    {
        $this->load->model("Smart_memberbase_model");
        $this->load->model("Virtual_passes_model");
        $cards = $this->Virtual_passes_model->get_range($_SESSION['id']);
        $card_ids = $this->input->get("pass_id");



        if (in_array($card_ids, $cards)) {
            $this->Smart_memberbase_model->export_csv($card_ids);
        } else {
            redirect("/merchant/smart_memberbase/no_permission");
        }
    }

    public function scanning()
    {
        $data["_inactive_loading"] = true;
        $data['_view'] = "membership_scan";
        $data["page"] = "Membership";
        $this->load->view("merchant/index", $data);
    }

    public function scan_qr()
    {
        $request = $this->input->post();
        $user_info = $this->Smart_memberbase_model->get_id($request['id']);

        $range = $this->Virtual_passes_model->get_range($_SESSION['id']);
        if ($user_info === null) {
            echo json_encode(["success" => true, "found" => "false"]);
        } else {
            if (in_array($user_info['pass_id'], $range)) {
                $pass_info = $this->Virtual_passes_model->get_card($user_info['pass_id']);
                $level_info = $this->Virtual_passes_model->get_full_level_setting($user_info['pass_id']);
                $data = [
                    "success" => true,
                    "found" => "true",
                    "user" => $user_info,
                    "levels" => $level_info
                ];
                echo json_encode($data);
            } else {
                echo json_encode(["success" => true, "found" => "false"]);
            }
        }
    }

    public function scan_update()
    {
        $request = $this->input->post();
        $range = $this->Virtual_passes_model->get_range($_SESSION['id']);
        $user_info = $this->Smart_memberbase_model->get_id($request['id']);
        if ($user_info === null) {
            echo json_encode(['success' => true, "update" => "invalid"]);
        } else {
            if (in_array($user_info['pass_id'], $range)) {
                $user_info['level'] = $request['level'];
                $user_info['points'] = $request['points'];
                $this->Smart_memberbase_model->update_info($user_info, $request['id']);

                if ($user_info['platform'] == "apple") {
                    $token_info = $this->Smart_memberbase_model->get_user_token($user_info['id']);
                    $this->update_i_call($token_info);
                    echo json_encode(["success" => true, "update" => "valid"]);
                } else {
                    $object_info = $this->Smart_memberbase_model->get_user_object($user_info['id']);
                    $user_info['object_uid'] = $object_info["object_uid"];
                    $users[] = $user_info;
                    $card_info = $this->Virtual_passes_model->get_card($user_info['pass_id']);
                    $level_info = $this->Virtual_passes_model->get_all_level_setting($user_info["pass_id"]);
                    $layout_info = $this->Virtual_passes_model->get_front_setting($user_info['pass_id']);
                    $services = new Services();
                    $services->update_loyalty_object($users, $card_info, $layout_info, $level_info);

                    echo json_encode(["success" => true, "update" => "valid"]);
                }

                //echo json_encode(["success" => true, "update" => "valid"]);
            } else {
                echo json_encode(["success" => true, "update" => "invalid"]);
            }
        }
    }

    public function no_permission()
    {
        $data["_view"] = "no_permission";
        $data['_inactive_loading'] = true;
        $data['page'] = "No permission";
        $this->load->view("merchant/index", $data);
    }
}
