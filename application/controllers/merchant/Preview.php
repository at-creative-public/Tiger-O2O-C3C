<?php


use PKPass\PKPass;

require_once "Config.php";
require_once "Services.php";

require dirname(__DIR__) . "../../libraries/phpqrcode/qrlib.php";
//include 'phpqrcode/qrlib.php';

class Preview extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //$this->load->model("Security", "security_model");
        $this->load->model("Virtual_passes_model");
        if (!isset($_SESSION['id'])) {
            redirect("timeout");
        }
    }

    public function dashboard()
    {
        $data['page'] = "dashboard";
        $data['_inactive_loading'] = true;
        $data["_view"] = "dashboard";
        $this->load->view("merchant/index", $data);
    }

    public function virtual_passes()
    {
        $data['page'] = "Virtual Passes";
        $data['_inactive_loading'] = true;
        $data['_view'] = "preview/virtual_passes";
        $this->load->view("merchant/index", $data);
    }

    public function virtual_passes_edit()
    {
        $data['page'] = "Virtual Passes";
        $data['_inactive_loading'] = true;
        $data['_view'] = "preview/virtual_passes_edit";
        $this->load->view("merchant/index", $data);
    }

    public function smart_memberbase()
    {
        $data['page'] = "Smart MemberBase";
        $data['_inactive_loading'] = true;
        $data['_view'] = "preview/smart_memberbase";
        $this->load->view("merchant/index", $data);
    }

    public function landing_page()
    {
        $data["page"] = "Landing Page";
        $data["_inactive_loading"] = true;
        $data["_view"] = "preview/landing_page";
        $this->load->view("merchant/index", $data);
    }

    public function marketplace()
    {
        $data['page'] = "Marketplace";
        $data["_inactive_loading"] = true;
        $data['_view'] = "preview/marketplace";
        $this->load->view("merchant/index", $data);
    }

    public function submit_form()
    {
        $request = $this->input->post();
        $this->db->insert("temp_card", $request);
        $id = $this->db->insert_id();

        $path = $this->upload_logo($id);
        $request["logo"] = $path['url'];
        $path_2 = $this->upload_strip($id);
        $request["strip"] = $path_2['url'];
        $path_3 = $this->upload_icon($id);
        $request["icon"] = $path_3['url'];

        $this->db->where("id", $id)->update("temp_card", $request);
        $this->get_google_pay_link($id);
        QRcode::png(base_url("merchant/preview/trial_output/" . $id), $_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/" . $id . "/" . "Apple_QR.png");
        redirect("/merchant_system/preview_QR/" . $id);
    }

    public function show_QR($id)
    {
        $data['page'] = "Virtual Passes";
        $data["_inactive_loading"] = true;
        $data["_view"] = "preview/show_QR";
        $data['id'] = $id;
        $this->load->view("merchant/index", $data);
    }

    public function output_QR($id)
    {
        //$this->trial_output($id);
        return QRcode::png(base_url("merchant/preview/trial_output/" . $id));
    }



    public function trial_output($id)
    {
        $pass = new PKPass("apple_pay_passes/pass3.p12");
        $temp_data = $this->db->select("*")->from("temp_card")->where("id", $id)->get()->row_array();

        $data = [
            "formatVersion" => 1,
            "passTypeIdentifier" => "pass.com.at-creative.lollipoptwo",
            "serialNumber" => "E5982H-I10",
            "teamIdentifier" => "685RZJZRC7",
            "webServiceURL" => "https://example.com/passes/",
            "authenticationToken" => "vxwxd7J8AlNNFPS8k0a0FfUFtq0ewzFdc",

            "organizationName" => "GrowthFUN",
            "description" => $temp_data['description'],
            "logoText" => $temp_data["logo_text"],
            "foregroundColor" => $temp_data['pri_color'],
            "backgroundColor" => $temp_data["background_color"],
            "stripColor" => $temp_data["stripColor"],
            "labelColor" => $temp_data["secondary_color"],

            "suppressStripShine" => true,
            "storeCard" => array(
                "headerFields" => array(
                    array(
                        "key" => "hf",
                        "label" => "Points",
                        "value" => "100"
                    )
                ),
                "primaryFields" => array(
                    array(
                        "key" => "pf",
                        "label" => $temp_data["pri_label"],
                        "value" => $temp_data["pri_value"]
                    )
                ),
                "secondaryFields" => array(
                    array(
                        "key" => "sf1",
                        "label" => $temp_data['sec_label_1'],
                        "value" => $temp_data['sec_value_1']
                    ),
                    array(
                        "key" => "sf2",
                        "label" => $temp_data['sec_label_2'],
                        "value" => $temp_data['sec_value_2']
                    ),
                    array(
                        "key" => "sf3",
                        "label" => $temp_data['sec_label_3'],
                        "value" => $temp_data['sec_value_3']
                    )
                ),
                "backFields" => array(
                    array(
                        "key" => "bf1",
                        "value" => "",
                        "label" => "快速連結",
                        "attributedValue" => "<a href='" . $temp_data['url'] . "'>" . $temp_data['url'] . "</a>"
                    ),
                    array(
                        "key" => "bf1",
                        "value" => "",
                        "label" => "關於我們",
                        "attributedValue" => $temp_data['about_us']
                    ),
                    array(
                        "key" => "bf1",
                        "value" => "",
                        "label" => "條款細則",
                        "attributedValue" => $temp_data['terms_conditions']
                    ),
                )
            )
        ];
        $pass->setData($data);

        // Add files to the pass package
        $pass->addFile($_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/" . $id . "/" . 'icon.png');
        $pass->addFile($_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/" . $id . "/" . 'logo.png');
        $pass->addFile($_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/" . $id . "/" . 'strip.png');
        //$pass->addFile('apple_pay_passes/Test10.pass/icon@2x.png');
        //$pass->addFile('apple_pay_passes/Test10.pass/logo.png');
        if (!$pass->create(true)) {
            echo 'Error: ' . $pass->getError();
        }
    }


    public function get_google_pay_link($db_id)
    {
        $option = "l";
        $vertical = "LOYALTY";
        $verticalType = VerticalType::LOYALTY;
        while (strpos(" beglotq", $option) == false) {
            $option = readline("\n\n*****************************\n" .
                "Which pass type would you like to demo?\n" .
                "b - Boarding Pass\n" .
                "e - Event Ticket\n" .
                "g - Gift Card\n" .
                "l - Loyalty\n" .
                "o - Offer\n" .
                "t - Transit\n" .
                "q - Quit\n" .
                "\n\nEnter your choice:");
            if ($option == "b") {
                $verticalType = VerticalType::FLIGHT;
                $vertical = "FLIGHT";
            } elseif ($option == "e") {
                $verticalType = VerticalType::EVENTTICKET;
                $vertical = "EVENTTICKET";
            } elseif ($option == "g") {
                $verticalType = VerticalType::GIFTCARD;
                $vertical = "GIFTCARD";
            } elseif ($option == "l") {
                $verticalType = VerticalType::LOYALTY;
                $vertical = "LOYALTY";
            } elseif ($option == "o") {
                $verticalType = VerticalType::OFFER;
                $vertical = "OFFER";
            } elseif ($option == "t") {
                $verticalType = VerticalType::TRANSIT;
                $vertical = "TRANSIT";
            } elseif ($option == "q") {
                exit();
            } else {
                printf("\n* Invalid option - $option. Please select one of the pass types by entering it's related letter.\n");
            }
        }
        // your classUid should be a hash based off of pass metadata, for the demo we will use pass-type_class_uniqueid
        $classUid = $vertical . "_CLASS_" . uniqid('', true); // CHANGEME

        // check Reference API for format of "id", offer example: (https://developers.google.com/pay/passes/reference/v1/offerclass/insert).
        // must be alphanumeric characters, ".", "_", or "-".
        $classId = sprintf("%s.%s", ISSUER_ID, $classUid);

        // your objectUid should be a hash based off of pass metadata, for the demo we will use pass-type_class_uniqueid
        $objectUid = $vertical . "_OBJECT_" . uniqid('', true); // CHANGEME

        // check Reference API for format of "id", offer example: (https://developers.google.com/pay/passes/reference/v1/offerobject/insert).
        // Must be alphanumeric characters, ".", "_", or "-".
        $objectId = sprintf("%s.%s", ISSUER_ID, $objectUid);

        // demonstrate the different "services" that make links/values for frontend to render a functional "save to phone" button

        //demoFatJwt($verticalType, $classId, $objectId);
        //demoObjectJwt($verticalType, $classId, $objectId);
        $data_info = $this->db->select("*")->from("tbltemp_card")->where("id", $db_id)->get()->row_array();
        $user_info = [];
        $id = $this->demoSkinnyJwt($verticalType, $classId, $objectId, $data_info);
        $link = $this->db->select("*")->from("tbl_temp_google_card")->where("id", $id)->get()->row_array();
        //$this->generate_QR($link['link']);
        return QRcode::png($link['link'], $_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/" . $db_id . "/" . "Google_QR.png");
        //return QRcode::png("https://pay.google.com/gp/v/save/eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJvMm8tYzNjQG8yby1jM2MuaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iLCJhdWQiOiJnb29nbGUiLCJ0eXAiOiJzYXZldG9hbmRyb2lkcGF5IiwiaWF0IjoxNjM4MjQ4Mjg2LCJwYXlsb2FkIjp7ImxveWFsdHlPYmplY3RzIjpbeyJpZCI6IjMzODgwMDAwMDAwMTkzNzMwMzkuTE9ZQUxUWV9PQkpFQ1RfNjFhNWFmNWE1Y2RjMTUuMTMwMzA2NjEifV19LCJvcmlnaW5zIjpbImh0dHBzOlwvXC9hdC1jcmVhdGl2ZS5jb21cLyJdfQ.qNIASHMvVLwu7rusnsqMZO1TvxDDU7rGyZIgsYqUujaNmWrDGUbJMWiAMyThDQum6VuG2hMwQ2gS73CT6cseCC2o9AQg4zYShlDeRM6pzyTjf8Bi5U1scNtRx4Kt9Ik9b6Birqyr4DNqPEScbo76y8AuqiNwQNnARBfQ4r_VV5TtHFFxQiYPwLWnZ2Rb9WoRqiU2SoS5PnmUqIeGKrQ1-jGDngmsNYM3unKGtdi-KVkbTAkVpREAx0mR9EN3PhNycBLpT99-Kov48M4R00YtKuVwJWFmuEYwH-r1-sBX0I5wwEeWuTkH8MqvDvqCZaroX3_JOZ-N39LOPprpdhYyEA");
    }


    function demoSkinnyJwt($verticalType, $classId, $objectId, $data_info, $user_info)
    {
        /*printf(
            "\n#############################\n" .
                "#\n" .
                "#  Generates a signed \"skinny\" JWT.\n" .
                "#  2 REST calls are made:\n" .
                "#  x1 pre-insert one classes\n" .
                "#  x1 pre-insert one object which uses previously inserted class\n" .
                "#\n" .
                "#  This JWT can be used in JS web button.\n" .
                "#  This is the shortest type of JWT; recommended for Android intents/redirects.\n" .
                "#\n" .
                "#############################\n"
        ); */

        $services = new Services();
        $skinnyJwt = $services->makeSkinnyJwt($verticalType, $classId, $objectId, $data_info, $user_info);

        if ($skinnyJwt  != null) {
            //printf("\nThis is an \"skinny\" jwt:\n%s\n" , $skinnyJwt );
            //printf("\nyou can decode it with a tool to see the unsigned JWT representation:\n%s\n" , "https://jwt.io");
            //printf("\nTry this save link in your browser:\n%s%s\n", SAVE_LINK, $skinnyJwt );
            //printf("%s%s", "https://pay.google.com/gp/v/save/", $skinnyJwt);
            //printf("\nthis is the shortest type of JWT; recommended for Android intents/redirects\n\n\n");
            $this->db->insert("tbl_temp_google_card", ["link" => "https://pay.google.com/gp/v/save/" . $skinnyJwt]);
            return $id = $this->db->insert_id();

            //return QRcode::png("https://pay.google.com/gp/v/save/" . $skinnyJwt);
        }

        return;
    }


    public function logout()
    {
        $this->session->session_destroy();
        redirect("merchant/login");
    }

    public function upload_logo($id)
    {
        if ($this->input->method() == 'post') {
            $request = $this->input->post();

            $this->load->model('Upload_model', 'upload');
            if ($_FILES['logo']['error'] == 0) {
                $result = $this->upload->uploadfile('logo', "logo", $id);
                if ($result['status'] == 'success') {
                    return $result;
                }
            }
        }
    }

    public function upload_strip($id)
    {
        if ($this->input->method() == 'post') {
            $request = $this->input->post();

            $this->load->model('Upload_model', 'upload');
            if ($_FILES['pri_strip']['error'] == 0) {
                $result = $this->upload->uploadfile('pri_strip', "strip", $id);
                if ($result['status'] == 'success') {
                    return $result;
                }
            }
        }
    }

    public function upload_icon($id)
    {
        if ($this->input->method() == 'post') {
            $request = $this->input->post();

            $this->load->model('Upload_model', 'upload');
            if ($_FILES['icon']['error'] == 0) {
                $result = $this->upload->uploadfile('icon', "icon", $id);
                if ($result['status'] == 'success') {
                    return $result;
                }
            }
        }
    }
}
