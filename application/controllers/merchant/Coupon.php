<?php



require_once $_SERVER['DOCUMENT_ROOT']."/application/controllers/"."Config.php";
require_once "Services.php";

require dirname(__DIR__) . "../../libraries/phpqrcode/qrlib.php";

use Sunaoka\PushNotifications\Drivers\APNs;
use Sunaoka\PushNotifications\Pusher;

class Coupon extends CI_Controller
{

    public $data_fields = [
        "coupon info" => "separator",
        "coupon_name" => "coupon name",
        "expiry_date" => "expiry date",
        "coupon_amount" => "coupon amount",
        "status" => "status",
        "personal_info" => "separator",
        "name" => "name",
        "surname" => "surname",
        "given_name" => "given name",
        "english_name" => "english name",
        "chinese_name" => "chinese name",
        "gender" => "gender",
        "birthday" => "birthday",
        "age" => "age",
        "nationality" => "nationality",
        "contact information" => "separator",
        "phone" => "phone",
        "email" => "email",
        "address" => "address",
        "industry" => "industry",
        "work information" => "separator",
        "industry_2" => "industry",
        "job_title" => "job_title",
        "company" => "company",
        "salary" => "salary"

    ];

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['id'])) {
            redirect("timeout");
        }
        $this->load->helper("coupon");
        $this->load->model("Coupon_model");
    }

    public function index()
    {
        $data['coupons'] = $this->Coupon_model->get_coupons($_SESSION['id']);
        $data["_inactive_loading"] = true;
        $data["page"] = "Coupon";
        $data["_view"] = "/coupon/index";
        $this->load->view("merchant/index", $data);
    }

    public function add()
    {
        $data["page"] = "Coupon";
        $data['_inactive_loading'] = true;
        $data["_view"] = "coupon/add";
        $data["data_fields"] = $this->data_fields;
        $this->load->view("merchant/index", $data);
    }

    public function insert()
    {
        $request = $this->input->post();

        $request["user_id"] = $_SESSION['id'];
        $coupon_id = $this->Coupon_model->insert_coupon(["user_id" => $_SESSION['id']]);
        $logo = $this->upload_logo($coupon_id);
        $icon = $this->upload_icon($coupon_id);
        $strip = $this->upload_strip($coupon_id);
        $basic_info = creation_data($request, $logo, $icon, $strip);
        $classUid = "OFFER_CLASS_" . uniqid("", true);
        $classId = sprintf("%s.%s", ISSUER_ID, $classUid);
        $basic_info["class_uid"] = $classId;
        $this->Coupon_model->update_coupon($basic_info, $coupon_id);
        $location_info = process_locations($request);
        $location_info['id'] = $coupon_id;
        $this->Coupon_model->insert_locations($location_info);
        $form_info = process_form($request);
        $form_info['id'] = $coupon_id;
        $this->Coupon_model->insert_form_config($form_info);
        $layout_info = process_layout($request);
        $layout_info['id'] = $coupon_id;
        $this->Coupon_model->insert_layout_config($layout_info);

        QRcode::png(base_url("coupon/registration/" . $coupon_id), $_SERVER["DOCUMENT_ROOT"] . "/public/images/uploads/coupon/" . $coupon_id . "/" . "QR.png");

        $services = new Services();
        $verticalType = VerticalType::OFFER;
        $template_info = $this->Coupon_model->get_coupon($coupon_id);
        $layout_info = $this->Coupon_model->get_layout_config($coupon_id);

        $services->makeClass($verticalType, $classId, $template_info, $layout_info);


        redirect("/merchant/coupon");
    }

    public function edit($id)
    {
        $range = $this->Coupon_model->coupon_range($_SESSION['id']);
        if (in_array($id, $range)) {
            $data['coupon_setting'] = $this->Coupon_model->get_coupon($id);
            $data["location_setting"] = $this->Coupon_model->get_locations_setting($id);
            $data['form_config'] = $this->Coupon_model->get_form_config($id);
            $data['form_config_json'] = json_encode($data['form_config']);
            $data['layout_setting'] = $this->Coupon_model->get_layout_config($id);
            $data['_inactive_loading'] = true;
            $data['page'] = "Coupon";
            $data['_view'] = "coupon/edit";
            $data['data_fields'] = $this->data_fields;
            $this->load->view("merchant/index", $data);
        } else {
            redirect("/merchant/coupon/no_permission");
        }
    }

    public function update()
    {
        $request = $this->input->post();
        $coupon_id = $request['id'];
        $logo = $this->upload_logo($coupon_id);
        $icon = $this->upload_icon($coupon_id);
        $strip = $this->upload_strip($coupon_id);
        $basic_info = update_data($request, $logo, $icon, $strip);
        $this->Coupon_model->update_coupon($basic_info, $coupon_id);
        $location_info = process_locations($request);
        $this->Coupon_model->update_locations($location_info, $coupon_id);
        $layout_info = process_layout($request);
        $this->Coupon_model->update_layout_config($layout_info, $coupon_id);
        $form_info = process_form($request);
        $this->Coupon_model->update_form_config($form_info, $coupon_id);
        $this->update_call($coupon_id);

        $service = new Services();
        $offer_info = $this->Coupon_model->get_coupon($coupon_id);
        $layout_info = $this->Coupon_model->get_layout_config($coupon_id);
        $data = ResourceDefinitions::makeOfferClassResource($offer_info['class_uid'], $offer_info, $layout_info);
        $service->update_offer($offer_info['class_uid'], $data);

        $this->load->model("Coupon_holder_model");
        $holders = $this->Coupon_holder_model->get_google_holder($coupon_id);
        $service->update_offer_object($holders, $data, $layout_info);




        redirect("/merchant/coupon");
    }

    public function delete($id)
    {
        $this->Coupon_model->delete($id);
    }

    public function duplicate()
    {
        $request = $this->input->post();
        $range = $this->Coupon_model->coupon_range($_SESSION['id']);
        if (in_array($request['id'], $range)) {
            $ori = $this->Coupon_model->get_coupon($request['id']);
            $ori['management_name'] = "Duplicated " . $ori['management_name'];
            $ori['project_name'] = "Duplicated " . $ori['project_name'];
            $ori['coupon_code'] = $request['coupon_code'];
            $new_id = $this->Coupon_model->insert_coupon(['user_id' => $_SESSION['id']]);
            $ori_form = $this->Coupon_model->get_form_config($request['id']);
            $ori_form['id'] = $new_id;
            $this->Coupon_model->insert_form_config($ori_form);
            $ori_location = $this->Coupon_model->get_locations_setting($request['id']);
            $ori_location['id'] = $new_id;
            $this->Coupon_model->insert_locations($ori_location);
            $ori_layout = $this->Coupon_model->get_layout_config($request['id']);
            $ori_layout["id"] = $new_id;
            $this->Coupon_model->insert_layout_config($ori_layout);
            if (!file_exists($_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/coupon/" . $new_id . "/")) {
                mkdir($_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/coupon/" . $new_id . "/", 0755);
            }
            copy($ori['logo'], $_SERVER["DOCUMENT_ROOT"] . '/public/images/uploads/coupon/' . $new_id . "/logo.png");
            copy($ori['strip'], $_SERVER["DOCUMENT_ROOT"] . '/public/images/uploads/coupon/' . $new_id . "/strip.png");
            copy($ori['icon'], $_SERVER["DOCUMENT_ROOT"] . "/public/images/uploads/coupon/" . $new_id . "/icon.png");
            $ori['logo'] = "public/images/uploads/coupon/" . $new_id . "/logo.png";
            $ori['strip'] = "public/images/uploads/coupon/" . $new_id . "/strip.png";
            $ori['icon'] = "public/images/uploads/coupon/" . $new_id . "/icon.png";
            QRcode::png(base_url("coupon/registration/" . $new_id), $_SERVER["DOCUMENT_ROOT"] . "/public/images/uploads/coupon/" . $new_id . "/" . "QR.png");

            $vertical = "OFFER";
            $verticalType = VerticalType::OFFER;
            $classUid = $vertical . "_CLASS_" . uniqid("", true);
            $classId = sprintf("%s.%s", ISSUER_ID, $classUid);
            $ori['class_uid'] = $classId;
            unset($ori['id']);
            $this->Coupon_model->update_coupon($ori, $new_id);
            $services = new Services();
            $data_info = $this->Coupon_model->get_coupon($new_id);
            $layout_info = $this->Coupon_model->get_layout_config($new_id);
            $services->makeClass($verticalType, $classId, $data_info, $layout_info);
        } else {
            echo "Access Denied";
        }
    }

    public function upload_logo($id)
    {
        if ($this->input->method() == 'post') {
            $request = $this->input->post();

            $this->load->model('Upload_model', 'upload');
            if ($_FILES['logo']['error'] == 0) {
                $result = $this->upload->uploadfile('logo', "logo", $id, "coupon");
                if ($result['status'] == 'success') {
                    return $result;
                }
            }
        }
    }

    public function upload_icon($id)
    {
        if ($this->input->method() == 'post') {
            $request = $this->input->post();

            $this->load->model('Upload_model', 'upload');
            if ($_FILES['icon']['error'] == 0) {
                $result = $this->upload->uploadfile('icon', "icon", $id, "coupon");
                if ($result['status'] == 'success') {
                    return $result;
                }
            }
        }
    }

    public function upload_strip($id)
    {
        if ($this->input->method() == 'post') {
            $request = $this->input->post();

            $this->load->model('Upload_model', 'upload');
            if ($_FILES['strip']['error'] == 0) {
                $result = $this->upload->uploadfile('strip', "strip", $id, "coupon");
                if ($result['status'] == 'success') {
                    return $result;
                }
            }
        }
    }

    public function update_call($coupon_id)
    {
        $this->load->model("Coupon_holder_model");
        $payload = [
            'aps' => [
                'alert' => [
                    'title' => 'Game Request',
                    'body'  => 'Bob wants to play poker',
                ],
            ],
        ];

        $options = new APNs\Token\Option();
        $options->payload = $payload;
        $options->authKey = FCPATH . 'apple_pay_passes/' . $this->config->item("apple_key"); 
        $options->keyId = $this->config->item("apple_cert_key_id"); 
        $options->teamId = $this->config->item("team_id"); 
        $options->topic = $this->config->item("topic"); 

        $driver = new APNs\Token($options);
        $holders = $this->Coupon_holder_model->get_apple_holder($coupon_id);

        foreach ($holders as $holder) {
            $pusher = new Pusher(true);
            $response = $pusher->to($holder['device_token'])->send($driver);
        }
    }

    public function check_duplicate_code()
    {
        $request = $this->input->get();

        $result = $this->Coupon_model->check_duplicate_code($request['code']);
        if ($result === 1) {
            echo json_encode(['pass' => "true"]);
        } else {
            echo json_encode(['pass' => "false"]);
        }
    }

    public function scanning()
    {
        $data["_inactive_loading"] = true;
        $data['page'] = "Coupon";
        $data["_view"] = "/coupon/scan";
        $this->load->view("merchant/index", $data);
    }

    public function scan_code()
    {
        $data["_inactive_loading"] = true;
        $data['page'] = "Coupon";
        $data['_view'] = "/coupon/scan_code";
        $this->load->view("merchant/index", $data);
    }

    public function scan_qr()
    {
        $code = $this->input->get("code");
        $ids = $this->Coupon_model->coupon_range($_SESSION['id']);

        $this->load->model("Coupon_holder_model");
        $result = $this->Coupon_holder_model->search_holder($code, $ids);
        if ($result == []) {
            echo json_encode(["found" => "false"]);
        } else {
            echo json_encode(["found" => "true", "result" => $result]);
        }
    }

    public function void_coupon()
    {
        $request = $this->input->post();
        $this->load->model("Coupon_holder_model");
        $range = $this->Coupon_model->coupon_range($_SESSION['id']);
        $target = $this->Coupon_holder_model->get_holder($request['id']);
        if ($target === null) {
            redirect("/merchant/coupon/no_permission");
        } else {
            if (in_array($target['coupon_id'], $range)) {
                $this->Coupon_holder_model->void_coupon($request['id']);
                $target = $this->Coupon_holder_model->get_holder($request['id']);
                if ($target['platform'] == "apple") {
                    $this->single_update_call($target);
                } else {
                    $services = new Services();
                    $data_info = $this->Coupon_model->get_coupon($target['coupon_id']);
                    $layout_info = $this->Coupon_model->get_layout_config($target['coupon_id']);
                    $users[] = $target;
                    $services->update_offer_object($users, $data_info, $layout_info);
                }
                if ($request['return_to'] == "qr") {
                    redirect("/merchant/coupon/scanning");
                } else {
                    redirect("/merchant/coupon/scan_code");
                }
            } else {
                redirect("/merchant/coupon/no_permission");
            }
        }
    }

    public function single_update_call($user_info)
    {
        $payload = [
            'aps' => [
                'alert' => [
                    'title' => 'Game Request',
                    'body'  => 'Bob wants to play poker',
                ],
            ],
        ];

        $options = new APNs\Token\Option();
        $options->payload = $payload;
        $options->authKey = FCPATH . 'apple_pay_passes/' . $this->config->item("apple_key"); 
        $options->keyId = $this->config->item("apple_cert_key_id"); 
        $options->teamId = $this->config->item("team_id"); 
        $options->topic = $this->config->item("topic"); 
        $driver = new APNs\Token($options);
        $pusher = new Pusher(true);
        $feedback = $pusher->to($user_info['device_token'])->send($driver);
    }

    public function rename_project()
    {
        $request =  $this->input->post();
        $range = $this->Coupon_model->coupon_range($_SESSION['id']);
        if (in_array($request['id'], $range)) {
            $this->Coupon_model->update_coupon(['management_name' => $request['new_name']], $request['id']);
            echo json_encode(['success' => true, "error" => false, "id" => $request['id'], "new_name" => $request['new_name']]);
        } else {
            echo json_encode(['success' => false, "error" => true]);
        }
    }

    public function no_permission()
    {
        $data['_view'] = "no_permission";
        $data['_inactive_loading'] = true;
        $data['page'] = "No_permission";
        $this->load->view("merchant/index", $data);
    }
}
