<?php

use PKPass\PKPass;

require_once $_SERVER['DOCUMENT_ROOT'] . "/application/controllers/" . "Config.php";
require_once "Services.php";

require dirname(__DIR__) . "../../libraries/phpqrcode/qrlib.php";

use Sunaoka\PushNotifications\Drivers\APNs;
use Sunaoka\PushNotifications\Pusher;
//include 'phpqrcode/qrlib.php';


class Virtual_passes extends CI_Controller
{

    public $data_fields = [
        "personal_info" => "separator", "name" => "Name", "surname" => "surname", "given_name" => "given_name", "english_name" => "english_name", "chinese_name" => "chinese_name", "gender" => "gender", "birthday" => "birthday", "age" => "age", "nationality" => "nationality", "contact_info" => "separator", "phone" => "phone", "email" => "email", "address" => "address", "industry" => "industry", "work_info" => "separator", "industry_2" => "industry", "job_title" => "job_title", "company" => "company", "salary" => "salary", "others" => "separator", "points" => "points", "level" => "level"
    ];

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Virtual_passes_model");
        if (!isset($_SESSION['id'])) {
            redirect("timeout");
        }
    }

    public function index()
    {
        $id = $_SESSION['id'];
        $this->load->model("Virtual_passes_model");
        $data = [];
        $data["cards"] = $this->Virtual_passes_model->get_cards_by_id_all($id);
        $data["_inactive_loading"] = true;
        $data["page"] = "Membership Card";
        $data['_view'] = "virtual_passes";
        $this->load->view("merchant/index", $data);
    }

    public function add_virtual_passes()
    {
        $data['page'] = "Membership Card";
        $data['_inactive_loading'] = true;
        $data['front_fields'] = $this->data_fields;
        $data['_view'] = "virtual_passes_edit";
        $this->load->view("merchant/index", $data);
    }

    public function create_virtual_passes()
    {
        $request = $this->input->post();

        $data = [
            "management_name" => $request['management_name'],
            "project_name" => $request['project_name'],
            "type" => $request['type'],
            "logo_text" => $request['logo_text'],
            "description" => $request['description'],
            //"pri_label" => $request['pri_label'],
            //"pri_value" => $request['pri_value'],
            "pri_color" => $request['pri_color'],
            "secondary_color" => $request['secondary_color'],
            "background_color" => $request['background_color'],
            //"stripColor" => $request['stripColor'],
            "logo_text_display" => 0,
            //"header_data" => $request['header_data'],
            "url" => $request['url'],
            "start_from" => $request['start_from'],
            "about_us" => $request['about_us'],
            "terms_conditions" => $request['terms_conditions'],
            "data_requirement" => $request['data_requirement'],
            "user_id" =>  $_SESSION['id']
        ];
        if ($request['type'] == 1) {
            $data['ref_key'] = sha1(md5(time()) . "68UZ5l+53bDtYE2ufFsUfPuPwyZGbXj8hRLbZ9ufGpM=");
        }
        $this->db->insert("tbl_virtual_passes", $data);
        $id = $this->db->insert_id();
        $request['pass_id'] = $id;
        $path = $this->upload_logo($id);
        $data["logo"] = $path['url'];
        $path_2 = $this->upload_strip($id);
        $data["strip"] = $path_2['url'];
        $path_3 = $this->upload_icon($id);
        $data["icon"] = $path_3['url'];
        $option = "l";
        $vertical = "LOYALTY";
        $verticalType = VerticalType::LOYALTY;
        $classUid = $vertical . "_CLASS_" . uniqid('', true);
        $classId = sprintf("%s.%s", ISSUER_ID, $classUid);
        $data["class_uid"] = $classId;
        $this->load->model("Virtual_passes_model");
        $dli = $this->Virtual_passes_model->insert_default_level(["pass_id" => $id, "value" => $request['default_level_value'], "default_value" => 1]);
        $data['default_level'] = $dli;
        $this->db->where("id", $id)->update("tbl_virtual_passes", $data);

        $form_data = $this->update_e_form_p($request);
        $form_data['id'] = $id;
        $location_data = $this->location_process($request);
        $location_data['id'] = $id;
        $level_data = $this->level_process($request);
        foreach ($level_data['insert'] as $level) {
            $this->Virtual_passes_model->insert_level($level);
        }
        $layout_data = $this->layout_process($request);
        $layout_data["id"] = $id;
        $this->Virtual_passes_model->insert_e_form($form_data);
        $this->Virtual_passes_model->insert_location_setting($location_data);
        $this->Virtual_passes_model->insert_front_setting($layout_data);


        QRcode::png(base_url("registration/apple/" . $id), $_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/membership/" . $id . "/" . "Apple_QR.png");
        QRcode::png(base_url("registration/google/" . $id), $_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/membership/" . $id . "/" . "Google_QR.png");
        QRcode::png(base_url("registration/" . $id), $_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/membership/" . $id . "/" . "Merged_QR.png");
        //
        $services = new Services();
        $data_info = $this->db->select("*")->from("tbl_virtual_passes")->where("id", $id)->get()->row_array();
        $layout_info = $this->db->select("*")->from("tbl_virtual_passes_data_config")->where("id", $id)->get()->row_array();

        $services->makeClass($verticalType, $classId, $data_info, $layout_info);
        redirect("/merchant/virtual_passes");
    }

    public function duplicate_pass($id)
    {
        $ori = $this->Virtual_passes_model->get_card($id);
        $ori['management_name'] = "Duplicated " . $ori['management_name'];
        $ori['project_name'] = "Duplicated " . $ori['project_name'];
        $this->db->insert("tbl_virtual_passes", ["user_id" => $_SESSION['id']]);
        $new_id = $this->db->insert_id();
        $ori_form = $this->Virtual_passes_model->get_e_form($id);
        $ori_form['id'] = $new_id;
        $this->Virtual_passes_model->insert_e_form($ori_form);
        $ori_data = $this->Virtual_passes_model->get_front_setting($id);
        $ori_data['id'] = $new_id;
        $this->Virtual_passes_model->insert_front_setting($ori_data);
        $ori_location = $this->Virtual_passes_model->get_location_setting($id);
        $ori_location['id'] = $new_id;
        $old_default_value = $this->Virtual_passes_model->get_default_level($id);
        $new_default_level = [
            "pass_id" => $new_id,
            "value" => $old_default_value['value'],
            "default_value" => 1,
            "display" => 1
        ];
        $new_default_level_id = $this->Virtual_passes_model->insert_level($new_default_level);
        $ori['default_level'] = $new_default_level_id;
        $old_non_default_levels = $this->Virtual_passes_model->get_level_setting($id);
        foreach ($old_non_default_levels as $index => $level) {
            unset($old_non_default_levels[$index]['id']);
            $old_non_default_levels[$index]['pass_id'] = $new_id;
            $this->Virtual_passes_model->insert_level($old_non_default_levels[$index]);
        }
        $this->Virtual_passes_model->insert_location_setting($ori_location);
        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/membership/" . $new_id . "/")) {
            mkdir($_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/membership/" . $new_id . "/", 0755);
        }
        copy($ori['logo'], $_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/membership/" . $new_id . "/logo.png");
        copy($ori['strip'], $_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/membership/" . $new_id . "/strip.png");
        copy($ori['icon'], $_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/membership/" . $new_id . "/icon.png");
        $ori['logo'] = "public/images/uploads/membership/" . $new_id . "/logo.png";
        $ori['strip'] = "public/images/uploads/membership/" . $new_id . "/strip.png";
        $ori['icon'] = "public/images/uploads/membership/" . $new_id . "/icon.png";
        //QRcode::png(base_url("registration/apple/" . $new_id), $_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/membership/" . $new_id . "/" . "Apple_QR.png");
        //QRcode::png(base_url("registration/google/" . $new_id), $_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/membership/" . $new_id . "/" . "Google_QR.png");
        QRcode::png(base_url("registration/" . $new_id), $_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/membership/" . $new_id . "/" . "Merged_QR.png");
        unset($ori['id']);
        if ($ori['type'] == 1) {
            $ori['ref_key'] = sha1(md5(time()) . "68UZ5l+53bDtYE2ufFsUfPuPwyZGbXj8hRLbZ9ufGpM=");
        }
        $option = "l";
        $vertical = "LOYALTY";
        $verticalType = VerticalType::LOYALTY;
        $classUid = $vertical . "_CLASS_" . uniqid('', true);
        $classId = sprintf("%s.%s", ISSUER_ID, $classUid);
        $ori["class_uid"] = $classId;
        //$this->db->where("id", $new_id)->update("tbl_virtual_passes", $ori);
        unset($ori['id']);
        $this->Virtual_passes_model->update_pass($ori, $new_id);
        $services = new Services();
        $data_info = $this->Virtual_passes_model->get_card($new_id);
        $layout_info = $this->Virtual_passes_model->get_front_setting($new_id);
        $services->makeClass($verticalType, $classId, $data_info, $layout_info);
    }

    public function update_virtual_passes()
    {
        $request = $this->input->post();

        $data = [];
        $data = [
            "management_name" => $request['management_name'],
            "project_name" => $request['project_name'],
            "logo_text" => $request['logo_text'],
            //"issuer" => $request['issuer'],
            "description" => $request['description'],
            //"pri_label" => $request['pri_label'],
            //"pri_value" => $request['pri_value'],
            "pri_color" => $request['pri_color'],
            "secondary_color" => $request['secondary_color'],
            "background_color" => $request['background_color'],
            //"stripColor" => $request['stripColor'],
            "logo_text_display" => 0,
            //"header_data" => $request["header_data"],
            "url" => $request['url'],
            "about_us" => $request['about_us'],
            "terms_conditions" => $request['terms_conditions'],
            "data_requirement" => $request['data_requirement']
        ];
        $path = $this->upload_logo($request["id"]);
        if ($path != null) {
            $data["logo"] = $path['url'];
        }
        $path_2 = $this->upload_strip($request['id']);
        if ($path_2 != null) {
            $data["strip"] = $path_2['url'];
        }
        $path_3 = $this->upload_icon($request['id']);
        if ($path_3 != null) {
            $data["icon"] = $path_3['url'];
        }
        $this->load->model("Virtual_passes_model");
        $this->Virtual_passes_model->update_pass($data, $request['id']);
        $this->Virtual_passes_model->update_e_form($this->update_e_form_p($request), $request['id']);
        $this->Virtual_passes_model->update_front_setting($this->layout_process($request), $request["id"]);
        $this->Virtual_passes_model->update_location_setting($this->location_process($request), $request['id']);

        $level_result = $this->level_process($request);
        foreach ($level_result["update"] as $record) {
            $this->Virtual_passes_model->update_level($record, $record['id']);
        }
        foreach ($level_result["insert"] as $record) {
            $this->Virtual_passes_model->insert_level($record);
        }

        //
        $services = new Services();
        $data_info = $this->db->select("*")->from("tbl_virtual_passes")->where("id", $request['id'])->get()->row_array();
        $layout_info = $this->db->select("*")->from("tbl_virtual_passes_data_config")->where("id", $request['id'])->get()->row_array();
        $data = ResourceDefinitions::makeLoyaltyClassResource($data_info['class_uid'], $data_info, $layout_info);
        $services->update_loyalty_card($data_info['class_uid'], $data);
        $this->load->model("Smart_memberbase_model");
        $users = $this->Smart_memberbase_model->get_update_user($request['id']);
        //var_dump($users);
        $level_data = $this->Virtual_passes_model->get_all_level_setting($request['id']);
        //  var_dump($level_data);


        $services->update_loyalty_object($users, $data_info, $layout_info, $level_data);
        //
        $this->update_push();
        redirect("/merchant/virtual_passes");
    }

    public function update_e_form_p($post)
    {
        $meta_info = ["name", "surname", "given_name", "english_name", "chinese_name", "gender", "birthday", "age", "nationality", "phone", "email", "address", "industry", "industry_2", "job_title", "company", "salary"];
        $simple_info_set = ["english_name", "chinese_name", "phone",  "email"];

        $data = [];
        if ($post['data_requirement'] == "simple") {
            foreach ($meta_info as $index => $value) {
                $data[$value] = 0;
            }
            foreach ($simple_info_set as $index => $value) {
                $data[$value] = 1;
            }
        } else {
            foreach ($meta_info as $index => $value) {
                if (isset($post[$value])) {
                    $data[$value] = 1;
                } else {
                    $data[$value] = 0;
                }
            }
        }
        return $data;
    }

    public function edit_virtual_passes($id)
    {
        $this->load->model("Virtual_passes_model");
        $range = $this->Virtual_passes_model->get_range($_SESSION['id']);
        if (in_array($id, $range)) {


            $data["card"] = $this->Virtual_passes_model->get_id($id);
            $data['form_config'] = $this->Virtual_passes_model->get_form_config($id);
            $data['form_config_json'] = json_encode($data['form_config']);
            $data["data_config"] = $this->Virtual_passes_model->get_front_setting($id);
            $data['location_config'] = $this->Virtual_passes_model->get_location_setting($id);
            $data["default_level"] = $this->Virtual_passes_model->get_default_level($id);

            $data['level_setting'] = $this->Virtual_passes_model->get_level_setting($id);
            $data["_inactive_loading"] = true;
            $data['front_fields'] = $this->data_fields;
            $data["page"] = "Membership Card";
            $data["_view"] = "virtual_passes_update";
            $this->load->view("merchant/index", $data);
        } else {
            redirect("/merchant/virtual_passes/no_permission");
        }
    }

    public function delete($id)
    {
        $this->db->where("id", $id)->set("display", 0)->update("tbl_virtual_passes");
    }



    public function upload_logo($id)
    {
        if ($this->input->method() == 'post') {
            $request = $this->input->post();

            $this->load->model('Upload_model', 'upload');
            if ($_FILES['logo']['error'] == 0) {
                $result = $this->upload->uploadfile('logo', "logo", $id);
                if ($result['status'] == 'success') {
                    return $result;
                }
            }
        }
    }

    public function upload_strip($id)
    {
        if ($this->input->method() == 'post') {
            $request = $this->input->post();

            $this->load->model('Upload_model', 'upload');
            if ($_FILES['pri_strip']['error'] == 0) {
                $result = $this->upload->uploadfile('pri_strip', "strip", $id);
                if ($result['status'] == 'success') {
                    return $result;
                }
            }
        }
    }

    public function upload_icon($id)
    {
        if ($this->input->method() == 'post') {
            $request = $this->input->post();

            $this->load->model('Upload_model', 'upload');
            if ($_FILES['icon']['error'] == 0) {
                $result = $this->upload->uploadfile('icon', "icon", $id);
                if ($result['status'] == 'success') {
                    return $result;
                }
            }
        }
    }

    public function update_push()
    {
        $payload = [
            'aps' => [
                'alert' => [
                    'title' => 'Game Request',
                    'body'  => 'Bob wants to play poker',
                ],
            ],
        ];

        $options = new APNs\Token\Option();
        $options->payload = $payload;
        $options->authKey = FCPATH . 'apple_pay_passes/' . $this->config->item("apple_key");
        $options->keyId = $this->config->item("apple_cert_key_id");
        $options->teamId = $this->config->item("team_id");
        $options->topic = $this->config->item("topic");

        $driver = new APNs\Token($options);
        $push_ids = $this->db->distinct()->select("pushToken")->from("tbl_temp_registration")->get()->result_array();
        foreach ($push_ids as $id) {
            $pusher = new Pusher(true);
            $feedback = $pusher->to($id['pushToken'])->send($driver);
        }
    }

    public function post_test()
    {

        $post = $this->input->post();

        //$data = $this->location_process($post);
        $data = $this->level_process($post);
        var_dump($post);
        var_dump($data);
        exit;
    }

    public function level_process($post)
    {
        $data['update'] = [];
        $data['insert'] = [];
        if (isset($post['level']['exist_record'])) {
            foreach ($post['level']['exist_record'] as $index => $record) {
                if ($record == "true") {
                    $data['update'][] = [
                        "id" => $post['level']['level_id'][$index],
                        "value" => $post['level']['value'][$index]
                    ];
                } else {
                    $data["insert"][] = [
                        "pass_id" => $post['pass_id'],
                        "value" => $post['level']['value'][$index]
                    ];
                }
            }
        }
        if (isset($post['default_level'])) {
            $data['update'][] = [
                "id" => $post['default_level'],
                "value" => $post['default_level_value']
            ];
        }
        return $data;
    }

    public function location_process($post)
    {
        $fields = ["location_1_status", "location_2_status", "location_3_status"];
        $fields_2 = ["location_1_lat", "location_1_lon", "location_1_message", "location_2_lat", "location_2_lon", "location_2_message", "location_3_lat", "location_3_lon", "location_3_message"];
        $data = [];
        foreach ($fields as $index => $value) {
            if (isset($post[$value])) {
                $data[$value] = 1;
            } else {
                $data[$value] = 0;
            }
        }
        foreach ($fields_2 as $index => $value) {
            $data[$value] = $post[$value];
        }
        return $data;
    }

    public function layout_process($post)
    {
        $fields = ["front_data_1_status", "front_data_2_status", "front_data_3_status", "front_data_4_status",];
        $fields_2 = ["front_data_1_data", "front_data_2_data", "front_data_3_data", "front_data_4_data", "front_data_1_label", "front_data_2_label", "front_data_3_label", "front_data_4_label"];
        $data = [];
        foreach ($fields as $index => $value) {
            if (isset($post[$value])) {
                $data[$value] = 1;
            } else {
                $data[$value] = 0;
            }
        }
        foreach ($fields_2 as $index => $value) {
            $data[$value] = $post[$value];
        }

        return $data;
    }

    public function check_user_ref()
    {
        $post = $this->input->post();
        $this->load->model("Smart_memberbase_model");
        $this->load->model("Virtual_passes_model");
        $result = $this->Smart_memberbase_model->check_user_ref($post['pass_id'], $post['id']);
        if ($result > 0) {
            echo json_encode(array("success" => 1, "delete" => 0));
        } else {
            $this->Virtual_passes_model->delete_level($post['id']);
            echo json_encode(array("success" => 1, "delete" => 1, "row_id" => $post['row_id']));
        }
    }

    public function fetch_member()
    {
        $request = $this->input->post();
        $this->load->model("Virtual_passes_model");
        $this->load->model("Smart_memberbase_model");
        $range = $this->Virtual_passes_model->get_range($_SESSION['id']);
        if (in_array($request['id'], $range)) {
            $result = $this->Smart_memberbase_model->simple_search($request);
            echo json_encode($result);
        } else {
            echo "Access Denied";
        }
    }

    public function select_user()
    {
        $id = $this->input->post("id");
        $this->load->model("Virtual_passes_model");
        $this->load->model("Smart_memberbase_model");
        $range = $this->Virtual_passes_model->get_range($_SESSION['id']);
        $user_info = $this->Smart_memberbase_model->get_id($id);
        if ($user_info !== null) {
            if (in_array($user_info['pass_id'], $range)) {
                echo json_encode($user_info);
            } else {
                echo "Access Denied";
            }
        } else {
            echo "Access Denied";
        }
    }

    public function rename_project()
    {
        $request  = $this->input->post();
        $this->load->model("Virtual_passes_model");
        $range = $this->Virtual_passes_model->get_range($_SESSION['id']);
        if (in_array($request['id'], $range)) {
            $this->Virtual_passes_model->update_pass(['management_name' => $request["new_name"]], $request["id"]);
            echo json_encode(['success' => true, "error" => false, "id" => $request['id'], "new_name" => $request['new_name']]);
        } else {
            echo json_encode(['success' => false, "error" => true]);
        }
    }

    public function no_permission()
    {
        $data['_view'] = "no_permission";
        $data['_inactive_loading'] = true;
        $data['page'] = "No_permission";
        $this->load->view("merchant/index", $data);
    }
}
