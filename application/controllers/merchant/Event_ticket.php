<?php


require_once $_SERVER['DOCUMENT_ROOT'] . "/application/controllers/" . "Config.php";
require_once "Services.php";

require dirname(__DIR__) . "../../libraries/phpqrcode/qrlib.php";

use Sunaoka\PushNotifications\Drivers\APNs;
use Sunaoka\PushNotifications\Pusher;

class Event_ticket extends CI_Controller
{
    public $data_fields = [
        "Event ticket info" => "separator",
        "event_name" => "Event Name",
        "issuer_name" => "Issuer Name",
        //"event_code" => "Event Code",
        "event_date" => "Event Date",
        "event_venue" => "Event Venue",
        "location_detail" => "Location Detail",
        "seat_info" => "Seat info",
        "User info" => "separator",
        "name" => "Name",
        "surname" => "Surname",
        "given_name" => "Given name",
        "english_name" => "English name",
        "chinese_name" => "Chinese name",
        "gender" => "Gender",
        "birthday" => "Birthday",
        "age" => "Age",
        "nationality" => "Nationality",
        "Contact info"  => "separator",
        "phone" => "Phone",
        "email" => "Email",
        "address" => "Address",
        "industry" => "Industry",
        "Work info" => "separator",
        "industry_2" => "Industry",
        "job_title" => "Job title",
        "compnay" => "Company",
        "salary" => "Salary"
    ];

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['id'])) {
            redirect("timeout");
        }
        $this->load->helper("event_ticket");
        $this->load->model("Event_ticket_model");
    }

    public function index()
    {
        $data['_inactive_loading'] = true;
        $data['page'] = "Event Ticket";
        $data["_view"] = "/event_ticket/index";
        $data['event_tickets'] = $this->Event_ticket_model->get_event_tickets_all($_SESSION['id']);

        $this->load->view("merchant/index", $data);
    }

    public function add()
    {
        $data['_inactive_loading'] = true;
        $data['page'] = "Event Ticket";
        $data['_view'] = "/event_ticket/add";
        $data["data_fields"] = $this->data_fields;
        $this->load->view("merchant/index", $data);
    }

    public function insert()
    {
        $request = $this->input->post();
        $request['user_id'] = $_SESSION['id'];
        $event_ticket_id = $this->Event_ticket_model->insert_event_ticket(["user_id" => $_SESSION['id']]);
        $logo = $this->upload_logo($event_ticket_id);
        $icon = $this->upload_icon($event_ticket_id);
        $strip = $this->upload_strip($event_ticket_id);
        $email_banner = $this->upload_email_banner($event_ticket_id);
        $basic_info = creation_data($request, $logo, $icon, $strip, $email_banner);
        if ($request['event_ticket_type'] == 1) {
            $basic_info["ref_key"] = md5(sha1(time()) . "68UZ5l+53bDtYE2hRLbZ9ufGpM=fFsUfPuPwyZGbXj8");
        }
        $classUid = "EVENT_TICKET_CLASS_" . uniqid("", true);
        $classId = sprintf("%s.%s", ISSUER_ID, $classUid);
        $basic_info["class_uid"] = $classId;
        $this->Event_ticket_model->update_event_ticket($basic_info, $event_ticket_id);
        $form_info = form_data($request);
        $form_info['id'] = $event_ticket_id;
        $this->Event_ticket_model->insert_form($form_info);
        $layout_info = layout_data($request);
        $layout_info['id'] = $event_ticket_id;
        $this->Event_ticket_model->insert_layout($layout_info);
        QRcode::png(base_url("event_ticket/registration/" . $event_ticket_id), $_SERVER["DOCUMENT_ROOT"] . "/public/images/uploads/event_ticket/" . $event_ticket_id . "/" . "QR.png");

        $services = new Services();
        $verticalType = VerticalType::EVENTTICKET;
        $template_info = $this->Event_ticket_model->get_event_ticket($event_ticket_id);
        $layout_info = $this->Event_ticket_model->get_layout($event_ticket_id);

        $services->makeClass($verticalType, $classId, $template_info, $layout_info);

        redirect("/merchant/event_ticket");
    }

    public function edit($id)
    {

        $range = $this->Event_ticket_model->get_range($_SESSION['id']);
        if (in_array($id, $range)) {
            $data["_inactive_loading"] = true;
            $data['page'] = "Event Ticket";
            $data['_view'] = "/event_ticket/edit";
            $data['data_fields'] = $this->data_fields;
            $data["ticket_info"] = $this->Event_ticket_model->get_event_ticket($id);
            $data['form_info'] = $this->Event_ticket_model->get_form($id);
            $data['form_info_json'] = json_encode($data['form_info']);
            $data['layout_info'] = $this->Event_ticket_model->get_layout($id);
            $this->load->view("merchant/index", $data);
        } else {
            redirect("/merchant/event_ticket/no_permission");
        }
    }

    public function update()
    {
        $request = $this->input->post();

        $event_ticket_id = $this->input->post("id");
        $range = $this->Event_ticket_model->get_range($_SESSION['id']);
        if (in_array($event_ticket_id, $range)) {
            $logo = $this->upload_logo($event_ticket_id);
            $icon = $this->upload_icon($event_ticket_id);
            $strip = $this->upload_strip($event_ticket_id);
            $email_banner = $this->upload_email_banner($event_ticket_id);
            $basic_info = update_data($request, $logo, $icon, $strip, $email_banner);
            $this->Event_ticket_model->update_event_ticket($basic_info, $event_ticket_id);
            $form_info = form_data($request);
            $this->Event_ticket_model->update_form($form_info, $event_ticket_id);
            $layout_info = layout_data($request);
            $this->Event_ticket_model->update_layout($layout_info, $event_ticket_id);
            $this->update_call($event_ticket_id);

            $service = new Services();
            $ticket_info = $this->Event_ticket_model->get_event_ticket($event_ticket_id);
            $layout_info = $this->Event_ticket_model->get_layout($event_ticket_id);
            $data = ResourceDefinitions::makeEventTicketClassResource($ticket_info['class_uid'], $ticket_info, $layout_info);
            $service->update_event_ticket($ticket_info['class_uid'], $data);


            $this->load->model("Event_ticket_memberbase_model");
            $holders = $this->Event_ticket_memberbase_model->get_google_holder($event_ticket_id);

            $service->update_event_ticket_object($holders, $ticket_info, $layout_info);



            redirect("/merchant/event_ticket");
        } else {
            redirect("/merchant/event_ticket/no_permission");
        }
    }

    public function duplicate()
    {

        $request = $this->input->post();
        $range = $this->Event_ticket_model->get_range($_SESSION["id"]);
        if (in_array($request['id'], $range)) {
            $ori = $this->Event_ticket_model->get_event_ticket($request['id']);
            $ori['management_name'] = "Duplicated " . $ori['management_name'];
            $ori['event_code'] = $request['event_code'];
            $new_id = $this->Event_ticket_model->insert_event_ticket(["user_id" => $_SESSION['id']]);
            $ori_form = $this->Event_ticket_model->get_form($request['id']);
            $ori_form['id'] = $new_id;
            $this->Event_ticket_model->insert_form($ori_form);
            $ori_layout = $this->Event_ticket_model->get_layout($request['id']);
            $ori_layout['id'] = $new_id;
            $this->Event_ticket_model->insert_layout($ori_layout);
            if (!file_exists($_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/event_ticket/" . $new_id . "/")) {
                mkdir($_SERVER["DOCUMENT_ROOT"] . "/public/images/uploads/event_ticket/" . $new_id . "/", 0755);
            }
            copy($ori['logo'], $_SERVER["DOCUMENT_ROOT"] . "/public/images/uploads/event_ticket/" . $new_id . "/logo.png");
            copy($ori['strip'], $_SERVER["DOCUMENT_ROOT"] . "/public/images/uploads/event_ticket/" . $new_id . "/strip.png");
            copy($ori['icon'], $_SERVER["DOCUMENT_ROOT"] . "/public/images/uploads/event_ticket/" . $new_id . "/icon.png");
            copy($ori['email_banner'], $_SERVER["DOCUMENT_ROOT"] . "/public/images/uploads/event_ticket/" . $new_id . "/email_banner.png");
            $ori['logo'] = "public/images/uploads/event_ticket/" . $new_id . "/logo.png";
            $ori['strip'] = "public/images/uploads/event_ticket/" . $new_id . "/strip.png";
            $ori['icon'] = "public/images/uploads/event_ticket/" . $new_id . "/icon.png";
            $ori['email_banner'] = "public/images/uploads/event_ticket/" . $new_id . "/email_banner.png";
            QRcode::png(base_url("event_ticket/registration/" . $new_id), $_SERVER["DOCUMENT_ROOT"] . "/public/images/uploads/event_ticket/" . $new_id . "/QR.png");

            $vertical = "EVENTTICKET";
            $verticalType = VerticalType::EVENTTICKET;
            $classUid = $vertical . "_CLASS_" . uniqid("", true);
            $classId = sprintf("%s.%s", ISSUER_ID, $classUid);
            $ori['class_uid'] = $classId;
            unset($ori['id']);
            if ($ori['event_ticket_type'] == 1) {
            $ori["ref_key"] = md5(sha1(time()) . "68UZ5l+53bDtYE2hRLbZ9ufGpM=fFsUfPuPwyZGbXj8");
            }
            $this->Event_ticket_model->update_event_ticket($ori, $new_id);
            $services = new Services();
            $data_info = $this->Event_ticket_model->get_event_ticket($new_id);
            $layout_info = $this->Event_ticket_model->get_layout($new_id);
            $services->makeClass($verticalType, $classId, $data_info, $layout_info);
        } else {
            echo "Access Denied";
        }
    }



    public function delete($id)
    {
        $this->Event_ticket_model->delete($id);
    }

    public function upload_logo($id)
    {
        if ($this->input->method() == 'post') {
            $request = $this->input->post();

            $this->load->model('Upload_model', 'upload');
            if ($_FILES['logo']['error'] == 0) {
                $result = $this->upload->uploadfile('logo', "logo", $id, "event_ticket");
                if ($result['status'] == 'success') {
                    return $result;
                }
            }
        }
    }

    public function upload_icon($id)
    {
        if ($this->input->method() == 'post') {
            $request = $this->input->post();

            $this->load->model('Upload_model', 'upload');
            if ($_FILES['icon']['error'] == 0) {
                $result = $this->upload->uploadfile('icon', "icon", $id, "event_ticket");
                if ($result['status'] == 'success') {
                    return $result;
                }
            }
        }
    }

    public function upload_strip($id)
    {
        if ($this->input->method() == 'post') {
            $request = $this->input->post();

            $this->load->model('Upload_model', 'upload');
            if ($_FILES['strip']['error'] == 0) {
                $result = $this->upload->uploadfile('strip', "strip", $id, "event_ticket");
                if ($result['status'] == 'success') {
                    return $result;
                }
            }
        }
    }

    public function upload_email_banner($id)
    {
        if ($this->input->method() == 'post') {
            $request = $this->input->post();

            $this->load->model('Upload_model', 'upload');
            if ($_FILES['email_banner']['error'] == 0) {
                $result = $this->upload->uploadfile('email_banner', "email_banner", $id, "event_ticket");
                if ($result['status'] == 'success') {
                    return $result;
                }
            }
        }
    }

    public function update_call($event_ticket_id)
    {
        $this->load->model("Event_ticket_memberbase_model");
        $payload = [
            'aps' => [
                'alert' => [
                    'title' => 'Game Request',
                    'body'  => 'Bob wants to play poker',
                ],
            ],
        ];

        $options = new APNs\Token\Option();
        $options->payload = $payload;
        $options->authKey = FCPATH . 'apple_pay_passes/' . $this->config->item("apple_key");
        $options->keyId = $this->config->item("apple_cert_key_id");
        $options->teamId = $this->config->item("team_id");
        $options->topic = $this->config->item("topic");

        $driver = new APNs\Token($options);
        $holders = $this->Event_ticket_memberbase_model->get_apple_holder($event_ticket_id);
        foreach ($holders as $holder) {
            $pusher = new Pusher(true);
            $pusher->to($holder['device_token'])->send($driver);
        }
    }

    public function check_duplicate_code()
    {
        $request = $this->input->get();
        $result = $this->Event_ticket_model->check_duplicate_code($request['code']);
        if ($result === 1) {
            echo json_encode(['pass' => "true"]);
        } else {
            echo json_encode(['pass' => "false"]);
        }
    }

    public function rename_project()
    {
        $request = $this->input->post();
        $range = $this->Event_ticket_model->get_range($_SESSION["id"]);
        if (in_array($request['id'], $range)) {
            $this->Event_ticket_model->update_event_ticket(["management_name" => $request['new_name']], $request['id']);
            echo json_encode(["success" => true, "error" => false, "id" => $request['id'], "new_name" => $request['new_name']]);
        } else {
            echo json_encode(['success' => false, "error" => true]);
        }
    }


    public function no_permission()
    {
        $data['_view'] = "no_permission";
        $data["_inactive_loading"] = true;
        $data['page'] = "No_permission";
        $this->load->view("merchant/index", $data);
    }
}
