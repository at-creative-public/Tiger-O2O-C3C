<?php

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Virtual_passes_model");
        $this->load->model("Smart_memberbase_model");
        $this->load->model("Statistics_model");
        if (!isset($_SESSION['id'])) {
            redirect("timeout");
        }
    }

    public function index()
    {
        $data['page'] = "dashboard";
        $data['_inactive_loading'] = true;
        $data["_view"] = "dashboard";
        $this->load->model("Merchant_model");
        $user_info = $this->Merchant_model->get_own_plan($_SESSION['id']);

        $data['current_plan'] = $user_info['merchant_plan'];
        $data['expired_date'] = date("d-m-Y", $user_info['merchant_valid_until']);
        $this->load->view("merchant/index", $data);
    }

    public function report_listing()
    {
        $data['page'] = "report_listing";
        $data['_inactive_loading'] = true;
        $data["_view"]  = "report_listing";
        $data['reports'] = $this->Statistics_model->report_listing($_SESSION["id"]);
        $data['pass_info'] = $this->Statistics_model->get_all_pass($_SESSION['id']);
        $this->load->view("merchant/index", $data);
    }

    public function get_pass_distribution()
    {
        $params = $this->input->post();
        $result = $this->Statistics_model->issued_pass($_SESSION['id']);
        $total = $result['membership'] + $result['coupon'] + $result['event_ticket'];
        $result = json_encode($result);
        echo json_encode(["result" => $result, "total" => $total]);
        exit;
    }

    public function get_issued_number()
    {
        $params = $this->input->post();
        $result = $this->Statistics_model->issued_pass_detail($_SESSION['id']);
        $total = $result['membership'] + $result["coupon"] + $result['event_ticket'];
        echo json_encode(["result" => $result, "total" => $total]);
        exit;
    }

    public function get_top_ten_users()
    {
        $params = $this->input->post();
        $this->load->model("Virtual_passes_model");
        if ($this->Virtual_passes_model->check_type($params['pass_id'])) {
            echo json_encode(['api' => true]);
        } else {
            $result = $this->Statistics_model->get_top_ten_users_integrated($params['pass_id']);
            echo json_encode($result);
        }
        exit;
    }

    public function update_selection()
    {
        $params = $this->input->post("selection");
        $result = $this->Statistics_model->listing_pass($_SESSION["id"], $params);
        echo json_encode(["result" => $result]);
        exit;
    }
}
