<?php

require_once $_SERVER['DOCUMENT_ROOT']."/application/controllers/"."Config.php";
require_once "Services.php";

use Sunaoka\PushNotifications\Drivers\APNs;
use Sunaoka\PushNotifications\Pusher;


class Coupon_memberbase extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Coupon_model");
        $this->load->model("Coupon_holder_model");
        if (!isset($_SESSION['id'])) {
            redirect("timeout");
        }
    }

    public function index()
    {
        $data['page'] = "Coupon Memberbase";
        $data['_inactive_loading'] = true;
        $data["_view"] = "coupon_memberbase/index";
        $data["coupons"] = $this->Coupon_model->get_coupons($_SESSION['id']);
        $this->load->view("merchant/index", $data);
    }

    public function detail()
    {
        $id = $this->input->get("id");
        $range = $this->Coupon_model->coupon_range($_SESSION['id']);
        $result = $this->Coupon_holder_model->get_holder($id);
        if ($result === null) {
            echo "Access Denied";
            exit;
        } else {
            if (in_array($result['coupon_id'], $range)) {
                echo json_encode($result);
            } else {
                echo "Access Denied";
                exit;
            }
        }
    }

    public function edit($id)
    {

        $range = $this->Coupon_model->coupon_range($_SESSION['id']);
        $detail = $this->Coupon_holder_model->get_holder($id);

        if ($detail === null) {
            redirect("/merchant/coupon_memberbase/no_permission");
        }

        if (in_array($detail['coupon_id'], $range)) {
            $data['_inactive_loading'] = true;
            $data['_view'] = "coupon_memberbase/edit";
            $data['page'] = "coupon_memberbase/edit";
            $data['detail'] = $detail;
            $this->load->view("merchant/index", $data);
        } else {
            redirect("/merchant/coupon_memberbase/no_permission");
        }
    }

    public function update()
    {
        $request = $this->input->post();
        $this->Coupon_holder_model->update_holder($request, $request['id']);

        $user_info = $this->Coupon_holder_model->get_holder($request['id']);
        if ($user_info['platform'] == "apple") {
            $this->update_call($user_info);
        } else {
            $services = new Services();
            $data_info = $this->Coupon_model->get_coupon($user_info['coupon_id']);
            $layout_info = $this->Coupon_model->get_layout_config($user_info['coupon_id']);
            $users[] = $user_info;
            $services->update_offer_object($users, $data_info, $layout_info);
        }
        redirect("/merchant/coupon_memberbase");
    }

    public function void()
    {
        $id = $this->input->post("id");
        $this->load->model("Coupon_holder_model");
        $range = $this->Coupon_model->coupon_range($_SESSION['id']);
        $target = $this->Coupon_holder_model->get_holder($id);
        if ($target === null) {
            echo "Access Denied";
        } else {
            if (in_array($target['coupon_id'], $range)) {
                $this->Coupon_holder_model->void_coupon($id);
                
                
             if ($target['platform'] == "apple") {
                 $this->update_call($target);
             } else {
                  $services = new Services();
                  $target = $this->Coupon_holder_model->get_holder($id);
                  $data_info = $this->Coupon_model->get_coupon($target['coupon_id']);
                  $layout_info = $this->Coupon_model->get_layout_config($target['coupon_id']);
                  $users[] = $target;
                  $services->update_offer_object($users, $data_info, $layout_info);
                  }
                
                
                echo json_encode(["success" => true]);
            } else {
                echo "Access Denied";
            }
        }
    }

    public function listing()
    {
        $request = $this->input->get();
        $this->load->model("Coupon_model");
        $this->load->model("Coupon_holder_model");
        echo json_encode($this->Coupon_holder_model->build_datatables($request['draw'], $this->Coupon_holder_model->totalCount($request['coupon_id']), $this->Coupon_holder_model->filteredCounter($request, $request['coupon_id']), $this->Coupon_holder_model->listing($request, $request['coupon_id'])), JSON_UNESCAPED_UNICODE);
    }

    public function no_permission()
    {
        $data['_view'] = "coupon_memberbase/no_permission";
        $data['_inactive_loading'] = true;
        $data['page'] = "No_permission";
        $this->load->view("merchant/index", $data);
    }

    public function export_csv()
    {
        $coupon_id = $this->input->get("coupon_id");
        $range = $this->Coupon_model->coupon_range($_SESSION['id']);
        if (in_array($coupon_id, $range)) {
            $this->Coupon_holder_model->export_csv($coupon_id);
        } else {
            redirect("/merchant/coupon_memberbase/no_permission");
        }
    }

    public function update_call($user_info)
    {
        $payload = [
            'aps' => [
                'alert' => [
                    'title' => 'Game Request',
                    'body'  => 'Bob wants to play poker',
                ],
            ],
        ];

        $options = new APNs\Token\Option();
        $options->payload = $payload;
        $options->authKey = FCPATH . 'apple_pay_passes/' . $this->config->item("apple_key"); 
        $options->keyId = $this->config->item("apple_cert_key_id"); 
        $options->teamId = $this->config->item("team_id"); 
        $options->topic = $this->config->item("topic"); 
        $driver = new APNs\Token($options);
        $pusher = new Pusher(true);
        $feedback = $pusher->to($user_info['device_token'])->send($driver);
        //var_dump($feedback);
        //exit;
    }
}
