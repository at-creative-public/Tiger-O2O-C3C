<?php

/**
 * @property CI_DB_query_builder $db
 * @property CI_Output $output
 * @property Merchant_model $Merchant_model
 */
class Api extends CI_Controller
{
    public function __construct()
    {
        // $this->load does not exist until after you call this
        parent::__construct(); // Construct CI's core so that you can use it

        $this->load->model('Security', 'security_model');

        //check user have login or not
        $this->security_model->admin_api($this->session->userdata('session_id'), $this->session->userdata('user_id'), $this->session->userdata('username'));
        //end check user have login or not
    }

    public function unlink_file()
    {

        $filename = $this->input->get('filename');
        $folder = $this->input->get('folder') ?? '';

        $this->load->library('common');
        $this->common->unlink_file($filename, $folder);

        return $this->respond([]);
    }

    public function upload_plan_poster()
    {
        $this->load->library('common');
        $uploaded_res = $this->common->single_upload(APPPATH . '../assets/uploads/', 'plan_poster.png', 'poster');

        $old = APPPATH . '../assets/uploads/plan_poster.png';
        $new = APPPATH . '../assets/uploads/plan_poster1.png';
        if (file_exists($new)) {
            unlink($old);
            rename($new, $old);
        }
        return $this->respond($uploaded_res);
    }

    public function get_merchant($merchant_id)
    {
        $this->load->model('Merchant_model');
        $data = $this->Merchant_model->get_joined(['merchant_id' => $merchant_id])->row_array();
        // var_dump($data);
        // exit();
        $data['merchant_valid_until'] = date('Y-m-d', $data['merchant_valid_until']);

        $this->load->library('common');
        $data['related_documents'] = $this->common->get_files_from_directory($data['created_at']);

        return $this->respond($data);
    }

    protected function datatables_MySQL_adapter()
    {
        $this->load->database();
        return new \Ozdemir\Datatables\Datatables(new \Ozdemir\Datatables\DB\MySQL([
            'host' => $this->db->hostname,
            'username' => $this->db->username,
            'password' => $this->db->password,
            'database' => $this->db->database,
            'port' => '3306',
        ]));
    }

    public function get_merchant_datatables()
    {
        $dt = $this->datatables_MySQL_adapter();

        $this->load->model('Merchant_model');
        $dt->query($this->Merchant_model->get_datatables_query());

        // add 'action' column
        $dt->add('', function ($data) {
            return sprintf(
                '<button class="btn btn-primary view" data-merchantid="%s"> VIEW </button>',
                $data['merchant_id']
            );
        });
        $dt->add('', function ($data) {
            return sprintf(
                '<a class="btn btn-primary" href="%s"> EDIT </a>',
                base_url() . 'backend/merchant/edit/' . $data['merchant_id']
            );
        });

        $dt->edit('merchant_id', function ($data) {
            return sprintf(
                '<p class="m-0 p-0 font-weight-bold">%s</p><p class="subtitle">%s</p>',
                $data['merchant_id'],
                $data['company_name']
            );
        });
        $dt->edit('merchant_password', function ($data) {
            return sprintf(
                '<div class="text-security clickable">%s</div>',
                $data['merchant_password']
            );
        });
        $dt->edit('merchant_plan', function ($data) {
            return sprintf(
                '<div class="status %s">%s</div>',
                strtolower($data['merchant_plan']),
                ucfirst($data['merchant_plan'])
            );
        });

        echo $dt->generate();
    }

    public function datatables($query_name)
    {
        return $this->get_merchant_datatables();
    }

    protected function respond($data = [], $status = 200)
    {
        $this->output
            ->set_status_header($status)
            // ->_display(json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
            ->_display(json_encode($data, JSON_UNESCAPED_UNICODE));
    }
}
