<?php

class Merchant extends CI_Controller
{

    public $layout = 'full';
    public $module = 'merchant';
    public $model = 'users';

    public function __construct()
    {
        // $this->load does not exist until after you call this
        parent::__construct(); // Construct CI's core so that you can use it

        $this->load->model('Security', 'security_model');

        //check user have login or not
        $this->security_model->admin($this->session->userdata('session_id'), $this->session->userdata('user_id'), $this->session->userdata('username'));
        //end check user have login or not

        $this->load->model('Landingcontentmultistyle_Model', 'landingontentmultistyle_model');
        $this->load->model('Formsubmit_Model', 'formsubmit_model');
        $this->load->model('Blog_list_Model', 'blog_list_model');
    }

    public function index()
    {
        //$this->permission();
        $data['users'] = $this->db->get('_users')->num_rows();
        $data['selectedmenu'] = "";
        $data['countLandingPage'] = $this->landingontentmultistyle_model->countNumOfLandingContent();
        $data['countMainPage'] = $this->landingontentmultistyle_model->countNumOfMainContent();
        $data['countFormsubmit'] = $this->formsubmit_model->countNumOfFormsubmit();
        $data['countBlog'] = $this->blog_list_model->countNumOfBlog();
        $data['_inactive_loading'] = true;
        $data['page'] = 'Merchant Management';
        $data['controller'] = $this->router->fetch_class();
        $data['_view'] = 'merchant/index';
        $this->load->view('backend/index', $data);
    }

    public function add()
    {
        //$this->permission();
        $data['users'] = $this->db->get('_users')->num_rows();
        $data['selectedmenu'] = "";
        $data['countLandingPage'] = $this->landingontentmultistyle_model->countNumOfLandingContent();
        $data['countMainPage'] = $this->landingontentmultistyle_model->countNumOfMainContent();
        $data['countFormsubmit'] = $this->formsubmit_model->countNumOfFormsubmit();
        $data['countBlog'] = $this->blog_list_model->countNumOfBlog();
        $data['_inactive_loading'] = true;
        $data['page'] = 'Create New Account';
        $data['controller'] = $this->router->fetch_class();
        $data['_view'] = 'merchant/add';

        $this->load->helper('form');
        $this->load->view('backend/index', $data);
    }
}
