<?php

class Signout extends CI_Controller{
	public function __construct()
    {
        // $this->load does not exist until after you call this
        parent::__construct(); // Construct CI's core so that you can use it

//        $this->load->database();
//        $this->load->helper('language');
//        $this->load->library('session');
        $this->load->model('Security','security_model');
        $this->load->model('Activity','activity_model');


    }
	function index(){
		
		// $session_id = $this->security_model->getcode();
		// $sess = $this->security_model->web_track($session_id);
		// $property = $this->Property->get();
		
		// $data['sess']		 = $sess;
		// $data['property'] = $property;

		// $arr['logout'] = 1;
		// $arr['login'] = 0;
		// $arr['member_id'] = 0;

		// if($this->input->get('logout') == false){
		// 	$this->Session->web_track_update($session_id, $arr);
		// 	header('location:'.base_url().'signin');
		// 	exit;
		// }

		//added signout function
		$this->session->sess_destroy();

		header('location: '.base_url().'cms/login');
        exit;

        //end added signout function
	}
}

?>