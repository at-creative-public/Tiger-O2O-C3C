<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/application/controllers/" . "Config.php";
require_once "Services.php";

require dirname(__DIR__) . "/../libraries/phpqrcode/qrlib.php";

use Sunaoka\PushNotifications\Drivers\APNs;
use Sunaoka\PushNotifications\Pusher;

class Event_ticket extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function auth()
    {
        if ($this->input->server("REQUEST_METHOD") !== "POST") {
            $res = [
                "error" => true,
                "status" => "400",
                "reason" => "Invalid request"
            ];
            echo json_encode($res);
            exit;
        }
        $incoming_data = $this->input->post();
        if (!isset($incoming_data['auth_key']) || !isset($incoming_data['ref_key'])) {
            $res = [
                "error" => true,
                "status" => "400",
                "reason" => "Invalid request"
            ];
            echo json_encode($res);
            exit;
        }
        $this->load->model("Api_auth_model");
        $access_resource = [];
        $access_resource['merchant_id'] = $this->Api_auth_model->auth($incoming_data['auth_key']);
        $access_resource['pass_id'] = $this->Api_auth_model->event_ticket_ref_auth($access_resource['merchant_id'], $incoming_data['ref_key']);
        if (isset($access_resource['merchant_id']) && isset($access_resource['pass_id'])) {
            return $access_resource;
        }
    }

    public function show()
    {
        $access_resource = $this->auth();
        $incoming_data = $this->input->post();
        if (!isset($incoming_data['ref_id'])) {
            $res = [
                "error" => true,
                "status" => "400",
                'reason' => "Invalid request"
            ];
            echo json_encode($res);
            exit;
        }
        $this->load->model("Api_event_ticket_model");
        $ticket_holder = $this->Api_event_ticket_model->get_id($access_resource, $incoming_data['ref_id']);
        $res = [
            "error" => false,
            "status" => "200",
            "result" => $ticket_holder
        ];
        echo json_encode($res);
        exit;
    }

    public function insert()
    {
        $access_resource = $this->auth();
        $incoming_data = $this->input->post();
        unset($incoming_data['auth_key']);
        unset($incoming_data['ref_key']);
        if (empty($incoming_data)) {
            $res = [
                "error" => true,
                "status" => "400",
                "reason" => "Invalid request"
            ];
            echo json_encode($res);
            exit;
        }
        $this->load->model("Api_event_ticket_model");
        $result = $this->Api_event_ticket_model->insert($access_resource, $incoming_data);

        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . "/assets/user_qr/event_ticket/" . $result['insert_id'] . "/")) {
            mkdir($_SERVER["DOCUMENT_ROOT"] . "/assets/user_qr/event_ticket/" . $result['insert_id'] . "/", 0755);
        }
        $this->load->model("Event_ticket_memberbase_model");
        $this->load->model("Event_ticket_model");
        QRcode::png(base_url("event_ticket/retrieve/apple/" . $result['insert_id'] . '?v=' . $result['hash']), $_SERVER['DOCUMENT_ROOT'] . "/assets/user_qr/event_ticket/" . $result['insert_id'] . "/apple.png");
        $this->Event_ticket_memberbase_model->store_apple_qr("/assets/user_qr/event_ticket/" . $result['insert_id'] . "/apple.png", $result['insert_id'], $result['hash']);
        $service = new Services();
        $ticket_info = $this->Event_ticket_model->get_event_ticket($access_resource['pass_id']);
        $layout_info = $this->Event_ticket_model->get_layout($access_resource['pass_id']);
        $holder_info = $this->Event_ticket_memberbase_model->get_holder($result['insert_id']);
         $vertical = "EVENTTICKET";
        $objectUid = $vertical . "_OBJECT_" . uniqid("", true);
        $objectId = sprintf("%s.%s", ISSUER_ID, $objectUid);
        $this->Event_ticket_memberbase_model->update_object_id($objectId, $result['insert_id']);
        $link = $service->makeObject($ticket_info['id'], $result['insert_id'], $ticket_info, $holder_info, $layout_info,$objectId, $vertical);
        $this->Event_ticket_memberbase_model->store_url($link, $result['insert_id']);
        QRcode::png("https://pay.google.com/gp/v/save/" . $link, $_SERVER['DOCUMENT_ROOT'] . "/assets/user_qr/event_ticket/" . $result['insert_id'] . "/google.png");
        $this->Event_ticket_memberbase_model->store_google_qr("/assets/user_qr/event_ticket/" . $result['insert_id'] . "/google.png", $result['insert_id']);
        $res = [
            "error" => false,
            "status" => "200",
            "result" => "Create event ticket success"
        ];
        echo json_encode($res);
        $wait = true;
        while($holder_info['google_qr'] == ""){
            $holder_info = $this->Event_ticket_memberbase_model->get_holder($result['insert_id']);
        }
        $this->send_invitation($holder_info,$ticket_info);
        exit;
    }

    public function update()
    {
         $access_resource = $this->auth();
        $incoming_data = $this->input->post();
        unset($incoming_data['auth_key']);
        unset($incoming_data['ref_key']);
        if (empty($incoming_data)) {
            $res = [
                "error" => true,
                "status" => "400",
                "reason" => "Invalid request"
            ];
            echo json_encode($res);
            exit;
        }
        $this->load->model("Api_event_ticket_model");
        $updated_info = $this->Api_event_ticket_model->update($access_resource, $incoming_data);

        if ($updated_info['device_token'] != "") {
            $this->update_call($updated_info['device_token']);
        }
        $this->load->model("Event_ticket_model");
        $pass_info = $this->Event_ticket_model->get_event_ticket($access_resource['pass_id']);
        $layout_info = $this->Event_ticket_model->get_layout($access_resource['pass_id']);
        $service = new Services();
        $passes[] = $updated_info;
        $service->update_event_ticket_object($passes, $pass_info, $layout_info);
        $res = [
            "error" => false,
            "status" => "200",
            "result" => "Update event ticket success"
            ];
        echo json_encode($res);
        exit;
        
    }

    public function update_call($token)
    {
         $payload = [
            "aps" => [
                "alert" => [
                    "title" => "Update call",
                    "body" => "Update your card"
                ]
            ]
        ];

        $options = new APNs\Token\Option();
        $options->payload = $payload;
        $options->authKey = FCPATH . 'apple_pay_passes/' . $this->config->item("apple_key");
        $options->keyId = $this->config->item("apple_cert_key_id");
        $options->teamId = $this->config->item("team_id");
        $options->topic = $this->config->item("topic");

        $driver = new APNs\Token($options);

        $pusher = new Pusher(true);
        $feedback = $pusher->to($token)->send($driver);
    }

   public function send_invitation($user_info, $event_info)
    {
        require dirname(__DIR__) . "/../libraries/sendgrid/sendgrid-php.php";
        require_once dirname(__DIR__) . '/../libraries/Document.php';
        global $p, $doc, $a, $c;
        $email = new \SendGrid\Mail\Mail();

        $doc = new Document();
        $doc->loadHTMLFile('application/libraries/email.html');
        $doc->getElementsByTagName('title')->item(0)->nodeValue = 'Contact';

        $c = $doc->getElementById("event_name");
        $c->nodeValue = $event_info['event_name'];
        
        $c = $doc->getElementById("email_banner");
        $c->setAttribute("src", base_url($event_info['email_banner']) . "?id=" . rand(1, 999999999));
        $c = $doc->getElementById("apple_link");
        $c->setAttribute("href", base_url("event_ticket/retrieve/apple/" . $user_info["id"] . "?v=" . $user_info['hash']));
        $c = $doc->getElementById("apple_qr");
        $c->setAttribute("src", base_url($user_info['apple_qr']));
        $c = $doc->getElementById("google_link");
        $c->setAttribute("href", "https://pay.google.com/gp/v/save/" . $user_info['google_re']);
        $c = $doc->getElementById("google_qr");
        $c->setAttribute("src", base_url($user_info["google_qr"]));



        $html = $doc->saveHTML();
        $subj = "電子門票 Event Ticket //  " . $event_info['event_name'] . " - Powered by O2OC3C";

        $email->setFrom($this->config->item("sendgrid_from"), $this->config->item('sendgrid_user_name'));
        $email->setSubject($subj);
        $email->addTo($user_info['email']);

        $email->addContent(
            "text/html",
            $html
        );
        $sendgrid = new \SendGrid($this->config->item("sendgrid_api_key"));
        try {
            $response = $sendgrid->send($email);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
