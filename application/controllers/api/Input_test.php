<?php

class Input_test extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Api_auth_model");
    }

    public function insert()
    {
        if ($this->input->server('REQUEST_METHOD') !== 'POST') {
            $res = [
                "error" => true,
                "status" => "401",
                "reason" => "Invalid request"
            ];
            echo json_encode($res);
            exit;
        }
        $incoming_data = $this->input->post();
        if (!isset($incoming_data['key'])) {
            $res = [
                "error" => true,
                "status" => "401",
                "reason" => "Invalid request"
            ];
            echo json_encode($res);
            exit;
        }
        $auth_result = $this->Api_auth_model->auth($incoming_data['key']);
        if ($auth_result['error'] === false) {
            $this->load->model("Api_testing_model");

            if (isset($incoming_data['content'])) {
                $this->Api_testing_model->insert($incoming_data);
                $res = ['error' => false, "status" => 200, "message" => "create record success."];
                echo json_encode($res);
                exit;
            } else {
                $res = [
                    "error" => true,
                    "status" => "400",
                    "reason" => "The request does not contain 'content'."
                ];
                echo json_encode($res);
                exit;
            }
        } else {
            echo json_encode($auth_result);
        }
    }
}
