<?php

class Authentication extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
    }

    public function auth()
    {
        if ($this->input->server('REQUEST_METHOD') !== 'POST') {
            $res = [
                "error" => true,
                "status" => "401",
                "reason" => "Invalid request"
            ];
            echo json_encode($res);
            exit;
        }
        $incoming_data = $this->input->post();
        if (!isset($incoming_data['key'])) {
            $res = [
                "error" => true,
                "status" => "401",
                "reason" => "Invalid request"
            ];
            echo json_encode($res);
            exit;
        }
        $this->load->model("Api_auth_model");
        $result = $this->Api_auth_model->auth($incoming_data['key']);
        echo json_encode($result);
        exit;
    }
}
