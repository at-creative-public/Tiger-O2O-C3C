<?php


require_once $_SERVER['DOCUMENT_ROOT'] . "/application/controllers/" . "Config.php";
require_once "Services.php";

require dirname(__DIR__) . "/../libraries/phpqrcode/qrlib.php";

use Sunaoka\PushNotifications\Drivers\APNs;
use Sunaoka\PushNotifications\Pusher;

class Membership extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function auth()
    {
        if ($this->input->server("REQUEST_METHOD") !== "POST") {
            $res = [
                "error" => true,
                "status" => "400",
                "reason" => "Invalid request "
            ];
            echo json_encode($res);
            exit;
        }
        $incoming_data = $this->input->post();

        if (!isset($incoming_data['auth_key']) || !isset($incoming_data['ref_key'])) {
            $res = [
                "error" => true,
                "status" => "400",
                "reason" => "Invalid request"
            ];
            echo json_encode($res);
            exit;
        }
        $this->load->model("Api_auth_model");
        $access_resource = [];
        $access_resource['merchant_id'] = $this->Api_auth_model->auth($incoming_data['auth_key']);
        $access_resource['pass_id'] = $this->Api_auth_model->ref_auth($access_resource['merchant_id'], $incoming_data['ref_key']);
        if (isset($access_resource['merchant_id']) && isset($access_resource['pass_id'])) {
            return $access_resource;
        }
    }

    public function show()
    {
        $access_resource = $this->auth();
        $incoming_data = $this->input->post();
        if (!isset($incoming_data['ref_id'])) {
            $res = [
                "error" => true,
                "status" => "400",
                "reason" => "Invalid request"
            ];
            echo json_encode($res);
            exit;
        }
        $this->load->model("Api_membership_model");
        $member_info = $this->Api_membership_model->get_id($access_resource, $incoming_data['ref_id']);
        $res = [
            "error" => false,
            "status" => 200,
            "result" => $member_info
        ];
        echo json_encode($res);
        exit;
    }

    public function insert()
    {
        $access_resource = $this->auth();
        $incoming_data = $this->input->post();
        unset($incoming_data['auth_key']);
        unset($incoming_data['ref_key']);

        if (empty($incoming_data)) {
            $res = [
                "error" => true,
                "status" => "400",
                "reason" => "Invalid request"
            ];
            echo json_encode($res);
            exit;
        }
        $this->load->model("Api_membership_model");
        $result = $this->Api_membership_model->insert($access_resource, $incoming_data);
        QRcode::png(base_url("registration/retrieve/" . $result['insert_id'] . "?v=" . $result['hash']), $_SERVER["DOCUMENT_ROOT"] . "/assets/user_qr/membership/" . $result['insert_id'] . "_" . "QR.png");

        if ($result['platform'] == "google") {
            $service = new Services();
            $vertical = "LOYALTY";
            $objectUid = $vertical . "_OBJECT_" . uniqid("", true);
            $objectId = sprintf("%s.%s", ISSUER_ID, $objectUid);
            $this->db->insert("tbl_google_registration", ["user_id" => $result['insert_id'], "object_uid" => $objectId]);
            $this->load->model("Virtual_passes_model");
            $this->load->model("Smart_memberbase_model");
            $pass_info = $this->Virtual_passes_model->get_id($access_resource['pass_id']);
            $member_info = $this->Smart_memberbase_model->get_id($result['insert_id']);
            $card_config = $this->Virtual_passes_model->get_front_setting($access_resource['pass_id']);
            $link = $service->makeObject("", "", $pass_info, $member_info, $card_config, $objectId, $vertical);
        }

        $this->send_invitation($access_resource['pass_id'], $result['ref_id'], $result['insert_id'], $result['hash']);

        $res = [
            "error" => false,
            "status" => "200",
            "result" => "Create member success"
        ];
        echo json_encode($res);
        exit;
    }

    public function update()
    {
        $access_resource = $this->auth();
        $incoming_data = $this->input->post();
        unset($incoming_data['auth_key']);
        unset($incoming_data['ref_key']);

        if (empty($incoming_data)) {
            $res = [
                "error" => true,
                "status" => "400",
                "reason" => "Invalid request"
            ];
            echo json_encode($res);
            exit;
        }
        $this->load->model("Api_membership_model");
        $updated_info = $this->Api_membership_model->update($access_resource, $incoming_data);
        $this->load->model("Smart_memberbase_model");

        if ($updated_info['platform'] == "apple") {
            //apple
            $push_info = $this->Smart_memberbase_model->get_user_token($updated_info['id']);
            if (!empty($push_info)) {
                $this->update_call($push_info['pushToken']);
            }
        } else {
            //google
            $push_info = $this->Smart_memberbase_model->get_user_object($updated_info['id']);
            $updated_info['object_uid'] = $push_info['object_uid'];
            $this->load->model("Virtual_passes_model");
            $pass_info = $this->Virtual_passes_model->get_id($access_resource['pass_id']);
            $card_config = $this->Virtual_passes_model->get_front_setting($access_resource['pass_id']);
            $users[] = $updated_info;
            $service = new Services();
            $service->update_loyalty_object($users, $pass_info, $card_config);
        }

        $res = [
            "error" => false,
            "status" => "200",
            "result" => "Update member success"
        ];
        echo json_encode($res);
        exit;
    }


    public function update_status()
    {
        $access_resource = $this->auth();
        $incoming_data = $this->input->post();
        if (!isset($incoming_data['ref_id'])) {
            $res = [
                "error" => true,
                "status" => "400",
                "reason" => "Invalid request"
            ];
            echo json_encode($res);
            exit;
        }
        $this->load->model("api_membership_model");
        $this->api_membership_model->update_status($access_resource, $incoming_data);
        $res = [
            "error" => false,
            "status" => "200",
            "result" => "update status success"
        ];
        echo json_encode($res);
        exit;
    }

    public function send_invitation($pass_id, $ref_id, $insert_id, $hash)
    {

        $pass = $this->db->select("*")->from('tbl_virtual_passes')->where("id", $pass_id)->get()->row_array();
        $user = $this->db->select("*")->from("tbl_members")->where("pass_id", $pass_id)->where("ref_id", $ref_id)->get()->row_array();

        require dirname(__DIR__) . "/../libraries/sendgrid/sendgrid-php.php";
        require_once dirname(__DIR__) . '/../libraries/Document.php';
        global $p, $doc, $a, $c;
        $email = new \SendGrid\Mail\Mail();

        $doc = new Document();
        $doc->loadHTMLFile('application/libraries/membership_email.html');

        $c = $doc->getElementById("link");
        $c->setAttribute("href", base_url("registration/retrieve/" . $insert_id . "/?v=" . $hash));
        $c = $doc->getElementById("qr");
        $c->setAttribute("src", base_url("assets/user_qr/membership/" . $insert_id . "_QR.png"));
        $html = $doc->saveHTML();
        $subj = "電子會員卡 Membership Card //  " . $pass['project_name'] . " - Powered by O2OC3C";


        $email->setFrom($this->config->item("sendgrid_from"), $this->config->item('sendgrid_user_name'));
        $email->setSubject($subj);
        $email->addTo($user['email']);

        $email->addContent(
            "text/html",
            $html
        );
        $sendgrid = new \SendGrid($this->config->item("sendgrid_api_key"));
        try {
            $response = $sendgrid->send($email);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }



    public function update_call($token)
    {
        $payload = [
            "aps" => [
                "alert" => [
                    "title" => "Update call",
                    "body" => "Update your card"
                ]
            ]
        ];

        $options = new APNs\Token\Option();
        $options->payload = $payload;
        $options->authKey = FCPATH . 'apple_pay_passes/' . $this->config->item("apple_key");
        $options->keyId = $this->config->item("apple_cert_key_id");
        $options->teamId = $this->config->item("team_id");
        $options->topic = $this->config->item("topic");

        $driver = new APNs\Token($options);

        $pusher = new Pusher(true);
        $feedback = $pusher->to($token)->send($driver);
    }
}
