<?php

/**
 * @property Company_model $Company_model
 * @property Contact_model $Contact_model
 * @property Merchant_model $Merchant_model
 */
class Merchant extends CI_Controller
{

    public $layout = 'full';
    public $module = 'merchant';
    public $model = 'users';

    public function __construct()
    {
        // $this->load does not exist until after you call this
        parent::__construct(); // Construct CI's core so that you can use it

        $this->load->model('Security', 'security_model');

        //check user have login or not
        $this->security_model->admin($this->session->userdata('session_id'), $this->session->userdata('user_id'), $this->session->userdata('username'));
        //end check user have login or not
    }

    public function index()
    {
        $data['users'] = $this->db->get('_users')->num_rows();

        $data['_inactive_loading'] = true;
        $data['page'] = 'Merchant Management';
        $data['controller'] = $this->router->fetch_class();
        $data['_view'] = 'merchant/index';
        $this->load->view('admin/index', $data);
    }

    public function add()
    {
        if ($this->input->method() == 'post') $this->submit();

        //$this->permission();
        $data['users'] = $this->db->get('_users')->num_rows();

        $data['_inactive_loading'] = true;
        $data['page'] = 'Create New Account';
        $data['controller'] = $this->router->fetch_class();
        $data['_view'] = 'merchant/form';
        
        $this->load->helper('form');
        $this->load->view('admin/index', $data);
    }

    public function edit($merchant_id)
    {
        //$this->permission();
        $data['users'] = $this->db->get('_users')->num_rows();
        $this->load->model('Merchant_model');
        $merchant = $this->Merchant_model->get_joined(['merchant_id' => $merchant_id])->row_array();
        if (empty($merchant)) header('location: ' . base_url() . 'admin/merchant');

        $data['_inactive_loading'] = true;
        $data['page'] = 'Edit Account - ' . $merchant_id;
        $data['controller'] = $this->router->fetch_class();
        $data['_view'] = 'merchant/form';

        $this->load->library('common');
        $data['related_documents'] = $this->common->get_files_from_directory($merchant_id);
        
        $this->load->helper('form');
        $this->load->view('admin/index', array_merge($data, $merchant));
    }

    public function submit()
    {
        $id = $this->input->post('id');
        if ($id) return $this->update_submit($id);

        $params = $this->input->post();
        $params['merchant_valid_until'] = strtotime($params['merchant_valid_until'] . ' 23:59:59');
        unset($params['confirm_password']);
        unset($params['submit']);

        // $ts = time();
        // $this->load->library('common');
        // $upload_path = $this->common->make_directory_if_not_exist($ts);
        // // echo $upload_path; die();
        // $uploaded_res = $this->common->multiple_upload($upload_path, 'upload');
        // // var_dump($_FILES);
        // // var_dump($params);
        // // die();
        $this->load->model('Company_model');
        // $this->Company_model->add($company_params);


        // // unset($params['related_documents']);
        $this->load->model('Company_model');
        $company_params = $this->Company_model->filter_params($params);
        $company = $this->Company_model->add_userstamp($company_params);

        $this->load->model('Contact_model');
        $contact_params = $this->Contact_model->filter_params($params);
        $contact = $this->Contact_model->add_userstamp($contact_params);

        $this->load->model('Merchant_model');
        $merchant_params = $this->Merchant_model->filter_params($params);
        $merchant_params['company_id'] = $company['id'];
        $merchant_params['contact_id'] = $contact['id'];
        //
        $merchant_params['api_key'] = $this->Merchant_model->generate_api_key();
        //
        $merchant = $this->Merchant_model->add_userstamp($merchant_params);

        $ts = $merchant['merchant_id'];
        $this->load->library('common');
        $upload_path = $this->common->make_directory_if_not_exist($ts);
        $uploaded_res = $this->common->multiple_upload($upload_path, 'upload');

        header('location: ' . base_url() . 'admin/merchant/index');
    }

    public function update_submit($id)
    {
        $params = $this->input->post();
        $params['merchant_valid_until'] = strtotime($params['merchant_valid_until'] . ' 23:59:59');
        unset($params['confirm_password']);
        unset($params['submit']);

        $old_merchant_id = $params['old_merchant_id'];
        unset($params['old_merchant_id']);

        // $ts = time();
        // $this->load->library('common');
        // $upload_path = $this->common->make_directory_if_not_exist($ts);
        // // echo $upload_path; die();
        // $uploaded_res = $this->common->multiple_upload($upload_path, 'upload');
        // // var_dump($_FILES);
        // // var_dump($params);
        // // die();
        $this->load->model('Company_model');
        $company_params = $this->Company_model->filter_params($params);
        $this->Company_model->update_id($company_params, $params['company_id']);

        $this->load->model('Contact_model');
        $contact_params = $this->Contact_model->filter_params($params);
        $this->Contact_model->update_id($contact_params, $params['contact_id']);

        $this->load->model('Merchant_model');
        $merchant_params = $this->Merchant_model->filter_params($params);
        $this->Merchant_model->update_id($merchant_params, $id);

        $merchant = $this->Merchant_model->get_id($id)->row_array();
        $ts = $merchant['merchant_id'];
        $this->load->library('common');
        if ($old_merchant_id !== $params['merchant_id']) {
            $this->common->rename_file(
                APPPATH . '../assets/uploads/' . $old_merchant_id,
                APPPATH . '../assets/uploads/' . $params['merchant_id']
            );
        }
        $upload_path = $this->common->make_directory_if_not_exist($ts);
        $uploaded_res = $this->common->multiple_upload($upload_path, 'upload');

        header('location: ' . base_url() . 'admin/merchant/index');
    }

    public function delete($merchant_id)
    {
        $this->load->model('Merchant_model');
        $data = $this->Merchant_model->soft_delete([], ['merchant_id' => $merchant_id]);

        $this->load->library('common');
        $this->common->rename_file(
            APPPATH . '../assets/uploads/' . $merchant_id,
            APPPATH . '../assets/uploads/' . $merchant_id . '_deleted_at_' . $data['deleted_at']
        );

        header('location: ' . base_url() . 'admin/merchant/index');
    }

    public function get_email_record()
    {
        $params = $this->input->post();
        $this->load->model("Email_management_model");
        $result = $this->Email_management_model->get_record($params["merchant_id"], $params['month'], $params['year']);
        echo json_encode(["success" => 1, "error" => 0, "result" => $result]);
        exit;
    }
}
