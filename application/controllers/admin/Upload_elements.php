<?php

class Upload_elements extends CI_Controller
{
    public $layout = 'full';
    public $module = 'Upload Elements';
    public $model = 'users';

    public function __construct()
    {
        // $this->load does not exist until after you call this
        parent::__construct(); // Construct CI's core so that you can use it

        $this->load->model('Security', 'security_model');

        //check user have login or not
        $this->security_model->admin($this->session->userdata('session_id'), $this->session->userdata('user_id'), $this->session->userdata('username'));
        //end check user have login or not

        // $this->load->model('Landingcontentmultistyle_Model','landingontentmultistyle_model');
        // $this->load->model('Formsubmit_Model','formsubmit_model');
        // $this->load->model('Blog_list_Model','blog_list_model');
    }

    public function index()
    {
        //$this->permission();
        $data['users'] = $this->db->get('_users')->num_rows();
        // $data['selectedmenu']="";
        // $data['countLandingPage']=$this->landingontentmultistyle_model->countNumOfLandingContent();
        // $data['countMainPage']=$this->landingontentmultistyle_model->countNumOfMainContent();
        // $data['countFormsubmit']=$this->formsubmit_model->countNumOfFormsubmit();
        // $data['countBlog']=$this->blog_list_model->countNumOfBlog();
        $data['_inactive_loading'] = true;
        $data['page'] = $this->module;
        $data['controller'] = $this->router->fetch_class();

        $this->load->library('common');

        $prefix = 'upload_elements_' . 'ordinary';
        foreach (['tutorial_video', 'theme_background', 'banner', 'font'] as $name) {
            $folder = $prefix . '_' . $name;
            $this->common->make_directory_if_not_exist($folder);
            $data[$name] = $this->common->get_files_from_directory($folder);
        }

        $data['_view'] = 'upload_elements';
        $this->load->view('admin/index', $data);
    }
}
