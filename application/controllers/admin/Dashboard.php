<?php

class Dashboard extends CI_Controller
{

    public $layout = 'full';
    public $module = 'dashboard';
    public $model = 'users';

    public function __construct()
    {
        // $this->load does not exist until after you call this
        parent::__construct(); // Construct CI's core so that you can use it

        $this->load->model('Security', 'security_model');
        $this->load->model("A_Statistics_model");

        //check user have login or not
        $this->security_model->admin($this->session->userdata('session_id'), $this->session->userdata('user_id'), $this->session->userdata('username'));
        //end check user have login or not

        // $this->load->model('Landingcontentmultistyle_Model','landingontentmultistyle_model');
        // $this->load->model('Formsubmit_Model','formsubmit_model');
        // $this->load->model('Blog_list_Model','blog_list_model');
    }

    public function index()
    {
        //$this->permission();
        $data['users'] = $this->db->get('_users')->num_rows();
        $data["merchants"] = $this->A_Statistics_model->get_merchants();
        $data['top_merchant'] = $this->A_Statistics_model->get_top_ten_merchant();
        $data["distribution"] = $this->A_Statistics_model->get_distribution();
        $data["passes"] = $this->A_Statistics_model->get_all_pass();
        $data["sales"] = $this->A_Statistics_model->sum_sales();
        $data["total"] = 0;
        foreach($data['passes'] as $p){
            $data["total"] += $p;
        }
        $data['cards'] = $this->A_Statistics_model->get_cards_distribution();
        // $data['selectedmenu']="";
        // $data['countLandingPage']=$this->landingontentmultistyle_model->countNumOfLandingContent();
        // $data['countMainPage']=$this->landingontentmultistyle_model->countNumOfMainContent();
        // $data['countFormsubmit']=$this->formsubmit_model->countNumOfFormsubmit();
        // $data['countBlog']=$this->blog_list_model->countNumOfBlog();
        $data['_inactive_loading'] = true;
        $data['page'] = $this->module;
        $data['controller'] = $this->router->fetch_class();
        $data['_view'] = 'dashboard';
        $this->load->view('admin/index', $data);
    }
}
