<?php

class Merchant_login extends CI_Controller
{
    public function __construct()
    {
        // $this->load does not exist until after you call this
        parent::__construct(); // Construct CI's core so that you can use it

        $this->load->model('Security', 'security_model');
        // $this->load->model('Activity', 'activity_model');
        if (isset($_SESSION['id'])) {
            unset($_SESSION['id']);
        }
    }
    public function index($timeout = false)
    {
        $data = [
            'page_title' => 'Source Radiance - 登入',
            '_description' => '登入',
            //            '_error' => $msg,
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'contact_email' => 'info@at-creative.com',
            'contact_phone' => '(852) 3428 8328',
            'timeout' => $timeout

        ];
        //$this->lang->load('users');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('userName', 'lang:users_username', 'required');
        $this->form_validation->set_rules('password', 'lang:users_password', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('/merchant/login/index', $data);
            return;
        } else {
            $username = $this->input->post('userName');
            $password = $this->input->post('password');

            $this->load->model('Merchant_model');
            $rs = $this->Merchant_model->validate($username, $password);


            if ($rs == null) {
                $rs = false;
            }

            if ($rs !== false) {

                if ($rs['force_disabled'] == 1) {
                    redirect("merchant/contact_us");
                }

                if ($rs['merchant_valid_until'] < time()) {

                    $data['expired_date']  = date("d-m-Y", $rs['merchant_valid_until']);


                    return $this->load->view("login_expired", $data);
                }

                $this->session->set_userdata(array(
                    "id" => $rs['id'],
                    'user_id' => $rs['merchant_id'],
                    'username' => $rs['merchant_id']
                ));
                //$this->activity_model->msg($rs['user_id'], 'Login to system');
                redirect('merchant/dashboard');
            } else {
                // $this->activity_model->msg(0, '$username Login false');
                $this->session->set_flashdata('msg', '錯誤登入名稱或密碼');
                $this->load->view('/merchant/login/index', $data);
            }
        }
    }
}
