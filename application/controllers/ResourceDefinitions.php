<?php

/**
 * Copyright 2019 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 *
 * require_once 'Walletobjects.php'
 * contains the Google_service_.* definitions.
 * Is is the helper client library to implement REST definitions defined at:
 * https://developers.google.com/pay/passes/reference/v1/
 * Download newest at https://developers.google.com/pay/passes/support/libraries#libraries
 *
 **/

class ResourceDefinitions
{
	/******************************
	 *
	 *  Define an Offer Class
	 *
	 *  See https://developers.google.com/pay/passes/reference/v1/offerclass
	 *
	 * @param String $classId - The unique identifier for a class
	 * @return Google_Service_Walletobjects_OfferClass $payload - object representing OfferClass resource
	 *
	 *******************************/
	public static function makeOfferClassResource($classId)
	{
		// Define the resource representation of the Class
		// values should be from your DB/services; here we hardcode information
		// below defines an offer class. For more properties, check:
		//// https://developers.google.com/pay/passes/reference/v1/offerclass/insert
		//// https://developers.google.com/pay/passes/guides/pass-verticals/offers/design

		// There is a client lib to help make the data structure. Newest client is on devsite:
		//// https://developers.google.com/pay/passes/support/libraries#libraries
		$titleImageUri = new Google_Service_Walletobjects_ImageUri();
		$titleImageUri->setUri("http://farm4.staticflickr.com/3723/11177041115_6e6a3b6f49_o.jpg");
		$titleImage = new Google_Service_Walletobjects_Image();
		$titleImage->setSourceUri($titleImageUri);

		$payload = new Google_Service_Walletobjects_OfferClass();
		//required properties
		$payload->setId($classId);
		$payload->setIssuerName("Baconrista Coffee");
		$payload->setProvider("Baconrista Deals");
		$payload->setRedemptionChannel("online");
		$payload->setReviewStatus("underReview");
		$payload->setTitle("20% off one bacon fat latte");
		// optional.  Check design and reference api to decide what's desirable
		$payload->setTitleImage($titleImage);

		return $payload;
	}

	/******************************
	 *
	 *  Define an Offer Object
	 *
	 * See https://developers.google.com/pay/passes/reference/v1/offerobject
	 *
	 * @param String $classId - The unique identifier for a class
	 * @param String $objectId - The unique identifier for an object
	 * @return Google_Service_Walletobjects_OfferObject $payload - object representing OfferObject resource
	 *
	 *******************************/
	public static function makeOfferObjectResource($classId, $objectId, $template_info, $user_info, $layout_info)
	{
		// Define the resource representation of the Object
		// values should be from your DB/services; here we hardcode information
		// below defines an offer object. For more properties, check:
		//// https://developers.google.com/pay/passes/reference/v1/offerobject/insert
		//// https://developers.google.com/pay/passes/guides/pass-verticals/offers/design

		// There is a client lib to help make the data structure. Newest client is on devsite:
		//// https://developers.google.com/pay/passes/support/libraries#libraries
		// Define Barcode
		$barcode = new Google_Service_Walletobjects_Barcode();
		$barcode->setType("qrCode");
		$barcode->setValue($user_info['display_code']);
		$barcode->setAlternateText($user_info['display_code']);

		$tmds = [];
		for ($i = 1; $i <= 4; $i++) {
			$temp_textModulesData = new Google_Service_Walletobjects_TextModuleData();
			switch (true) {
				case $layout_info["front_data_" . $i . "_data"] == "gender":
					$temp_textModulesData->setBody($user_info[$layout_info["front_data_" . $i . "_data"]] == 1 ? "男" : "女");
					break;
				default:
					$temp_textModulesData->setBody($user_info[$layout_info["front_data_" . $i . "_data"]]);
					break;
			}
			$temp_textModulesData->setHeader($layout_info['front_data_' . $i . '_label']);
			if ($layout_info['front_data_' . $i . "_status"] == 1) {
				$temp_textModulesData->setId("myfield_" . $i);
				$tmds[] = $temp_textModulesData;
			}
		}


		// Define offer object
		$payload = new Google_Service_Walletobjects_OfferObject();
		// required properties
		$payload->setClassId($classId);
		$payload->setId($objectId);
		$payload->setState("active");
		// optional.  Check design and reference api to decide what's desirable
		$payload->setTextModulesData($tmds);
		$payload->setBarcode($barcode);


		return $payload;
	}

	/******************************
	 *
	 *  Define an EventTicket Class
	 *
	 *  See https://developers.google.com/pay/passes/reference/v1/eventticketclass
	 *
	 * @param String $classId - The unique identifier for a class
	 * @return Google_Service_Walletobjects_EventTicketClass $payload - object representing EventTicketClass resource
	 *
	 *******************************/
	public static function makeEventTicketClassResource($classId)
	{
		// Define the resource representation of the Class
		// values should be from your DB/services; here we hardcode information
		// below defines an eventticket class. For more properties, check:
		//// https://developers.google.com/pay/passes/reference/v1/eventticketclass/insert
		//// https://developers.google.com/pay/passes/guides/pass-verticals/event-tickets/design

		// There is a client lib to help make the data structure. Newest client is on devsite:
		//// https://developers.google.com/pay/passes/support/libraries#libraries
		$localEventName = new Google_Service_Walletobjects_LocalizedString();
		$localEventNameTranslated = new Google_Service_Walletobjects_TranslatedString();
		$localEventNameTranslated->setLanguage("en-US");
		$localEventNameTranslated->setValue("Bacon Coffee Fun Event");
		$localEventName->setDefaultValue($localEventNameTranslated);

		$location = new Google_Service_Walletobjects_LatLongPoint();
		$location->setLatitude(37.424015499999996);
		$location->setLongitude(-122.09259560000001);
		$locations = array($location);

		$logoUri = new Google_Service_Walletobjects_ImageUri();
		$logoUri->setUri("https://farm8.staticflickr.com/7340/11177041185_a61a7f2139_o.jpg");
		$logoUri->setDescription("Baconrista stadium logo");
		$logoImage = new Google_Service_Walletobjects_Image();
		$logoImage->setSourceUri($logoUri);

		$localVenueName = new Google_Service_Walletobjects_LocalizedString();
		$localVenueNameTranslated = new Google_Service_Walletobjects_TranslatedString();
		$localVenueNameTranslated->setLanguage("en-US");
		$localVenueNameTranslated->setValue("Baconrista Stadium");
		$localVenueName->setDefaultValue($localVenueNameTranslated);
		$localVenueAddress = new Google_Service_Walletobjects_LocalizedString();
		$localVenueAddressTranslated = new Google_Service_Walletobjects_TranslatedString();
		$localVenueAddressTranslated->setLanguage("en-US");
		$localVenueAddressTranslated->setValue("101 Baconrista Dr.");
		$localVenueAddress->setDefaultValue($localVenueAddressTranslated);
		$localEventVenue = new Google_Service_Walletobjects_EventVenue();
		$localEventVenue->setName($localVenueName);
		$localEventVenue->setAddress($localVenueAddress);

		$eventDateTime = new Google_Service_Walletobjects_EventDateTime();
		$eventDateTime->setStart("2023-04-12T11:20:50.52Z");
		$eventDateTime->setEnd("2023-04-12T16:20:50.52Z");

		$textModulesData = new Google_Service_Walletobjects_TextModuleData();
		$textModulesData->setBody("Baconrista events have pushed the limits since its founding.");
		$textModulesData->setHeader("Custom Details");
		$textModulesDatas = array($textModulesData);

		$locationUri = new Google_Service_Walletobjects_Uri();
		$locationUri->setUri("http://maps.google.com/");
		$locationUri->setDescription("Nearby Locations");
		$telephoneUri = new Google_Service_Walletobjects_Uri();
		$telephoneUri->setUri("tel:6505555555");
		$telephoneUri->setDescription("Call Customer Service");
		$linksModuleData = new Google_Service_Walletobjects_LinksModuleData();
		$linksModuleData->setUris($locationUri, $telephoneUri);

		$payload = new Google_Service_Walletobjects_EventTicketClass();
		//required properties
		$payload->setId($classId);
		$payload->setIssuerName("Baconrista Stadium");
		$payload->setReviewStatus("underReview");
		$payload->setEventName($localEventName);
		// optional.  Check design and reference api to decide what's desirable
		$payload->setLocations($locations);
		$payload->setLogo($logoImage);
		$payload->setVenue($localEventVenue);
		$payload->setDateTime($eventDateTime);
		$payload->setTextModulesData($textModulesDatas);
		$payload->setLinksModuleData($linksModuleData);

		return $payload;
	}

	/******************************
	 *
	 *  Define an EventTicket Object
	 *
	 * See https://developers.google.com/pay/passes/reference/v1/eventticketobject
	 *
	 * @param String $classId - The unique identifier for a class
	 * @param String $objectId - The unique identifier for an object
	 * @return Google_Service_Walletobjects_EventTicketObject $payload - object representing EventTicketObject resource
	 *
	 *******************************/
	public static function makeEventTicketObjectResource($classId, $objectId, $template_info, $user_info, $layout_info)
	{
		// Define the resource representation of the Object
		// values should be from your DB/services; here we hardcode information
		// below defines an eventticket object. For more properties, check:
		//// https://developers.google.com/pay/passes/reference/v1/eventticketobject/insert
		//// https://developers.google.com/pay/passes/guides/pass-verticals/event-tickets/design

		// There is a client lib to help make the data structure. Newest client is on devsite:
		//// https://developers.google.com/pay/passes/support/libraries#libraries
		// Define Barcode
		$barcode = new Google_Service_Walletobjects_Barcode();
		$barcode->setType("qrCode");
		$barcode->setValue($user_info['api'] == 1 ? $user_info['ref_id'] : $user_info['display_code']);
		$barcode->setAlternateText($user_info['display_code']);

		if ($user_info['seat'] !== "" || $user_info['row'] !== "" || $user_info['section'] !== "") {
			$eventSeat = new Google_Service_Walletobjects_EventSeat();
		}

		if ($user_info['seat'] !== "") {
			$localSeatValue = new Google_Service_Walletobjects_LocalizedString();
			$localSeatValueTranslated = new Google_Service_Walletobjects_TranslatedString();
			$localSeatValueTranslated->setLanguage("en-US");
			$localSeatValueTranslated->setValue($user_info['seat']);
			$localSeatValue->setDefaultValue($localSeatValueTranslated);
			$eventSeat->setSeat($localSeatValue);
		}
		if ($user_info['row'] !== "") {
			$localRowValue = new Google_Service_Walletobjects_LocalizedString();
			$localRowValueTranslated = new Google_Service_Walletobjects_TranslatedString();
			$localRowValueTranslated->setLanguage("en-US");
			$localRowValueTranslated->setValue($user_info['row']);
			$localRowValue->setDefaultValue($localRowValueTranslated);
			$eventSeat->setRow($localRowValue);
		}
		if ($user_info["section"] !== "") {
			$localSectionValue = new Google_Service_Walletobjects_LocalizedString();
			$localSectionValueTranslated = new Google_Service_Walletobjects_TranslatedString();
			$localSectionValueTranslated->setLanguage("en-US");
			$localSectionValueTranslated->setValue($user_info['section']);
			$localSectionValue->setDefaultValue($localSectionValueTranslated);
			$eventSeat->setSection($localSectionValue);
		}
		$seat_string = "";
		if ($user_info['seat'] !== "") {
			$seat_string .= "Seat:" . $user_info['seat'];
		}
		if ($user_info['row'] !== "") {
			if ($user_info['seat'] !== "") {
				$seat_string .= "\n";
			}
			$seat_string .= "Row:" . $user_info["row"];
		}
		if ($user_info['section'] !== "") {
			if ($user_info['seat'] !== "" || $user_info['row'] !== "") {
				$seat_string .= "\n";
			}
			$seat_string .= "Section:" . $user_info['section'];
		}
		$tmds = [];
		for ($i = 1; $i <= 6; $i++) {
			$temp_textModulesData = new Google_Service_Walletobjects_TextModuleData();
			switch (true) {
				case $layout_info['front_data_' . $i . '_data'] == "event_name":
					$temp_textModulesData->setBody($template_info['event_name']);
					break;
				case $layout_info['front_data_' . $i . '_data'] == "issuer_name":
					$temp_textModulesData->setBody($template_info['issuer_name']);
					break;
				case $layout_info['front_data_' . $i . '_data'] == "event_date":
					$temp_textModulesData->setBody($template_info['event_start_time'] . "-" . $template_info['event_end_time'] . " " . $template_info['event_date']);
					break;
				case $layout_info['front_data_' . $i . '_data'] == "event_venue":
					$temp_textModulesData->setBody($template_info['event_venue']);
					break;
				case $layout_info['front_data_' . $i . '_data'] == "location_detail":
					$temp_textModulesData->setBody($template_info['location_detail']);
					break;
				case $layout_info['front_data_' . $i . '_data'] == "seat_info":
					//$temp_textModulesData->setBody("Section:".$user_info['section']."\nRow:".$user_info['row']."\nSeat:".$user_info['section']);
					$temp_textModulesData->setBody($seat_string);
					break;
				case $layout_info['front_data_' . $i . '_data'] == "gender":
					$temp_textModulesData->setBody($user_info['gender'] == "1" ? "M" : "F");
					break;
				default:
					$temp_textModulesData->setBody($user_info[$layout_info['front_data_' . $i . "_data"]]);
					break;
			}
			$temp_textModulesData->setHeader($layout_info['front_data_' . $i . '_label']);
			if ($layout_info['front_data_' . $i . '_status'] == 1) {
				$temp_textModulesData->setId("myfield_" . $i);
			}
			$tmds[] = $temp_textModulesData;
		}


		// Define eventticket object
		$payload = new Google_Service_Walletobjects_EventTicketObject();
		// required fields
		$payload->setClassId($classId);
		$payload->setId($objectId);
		$payload->setState("active");
		// optional.  Check design and reference api to decide what's desirable
		$payload->setBarcode($barcode);
		if ($user_info['seat'] !== "" || $user_info['row'] !== "" || $user_info['section'] !== "") {
			$payload->setSeatInfo($eventSeat);
		}
		$payload->setTicketHolderName($template_info['issuer_name']);
		$payload->setTextModulesData($tmds);
		$payload->setTicketNumber($user_info['api'] == 1 ? $user_info['ref_id'] : $user_info['display_code']);

		return $payload;
	}
	/******************************
	 *
	 *  Define an Flight Class
	 *
	 *  See https://developers.google.com/pay/passes/reference/v1/flightclass
	 *
	 * @param String $classId - The unique identifier for a class
	 * @return Google_Service_Walletobjects_FlightClass $payload - object representing FlightClass resource
	 *
	 *******************************/
	public static function makeFlightClassResource($classId)
	{
		// Define the resource representation of the Class
		// values should be from your DB/services; here we hardcode information
		// below defines an flight class. For more properties, check:
		//// https://developers.google.com/pay/passes/reference/v1/flightclass/insert
		//// https://developers.google.com/pay/passes/guides/pass-verticals/boarding-passes/design

		// There is a client lib to help make the data structure. Newest client is on devsite:
		//// https://developers.google.com/pay/passes/support/libraries#libraries

		$destination = new Google_Service_Walletobjects_AirportInfo();
		$destination->setAirportIataCode("SFO");
		$destination->setGate("C3");
		$destination->setTerminal("2");

		$origin = new Google_Service_Walletobjects_AirportInfo();
		$origin->setAirportIataCode("LAX");
		$origin->setGate("A2");
		$origin->setTerminal("4");

		$flightCarrier = new Google_Service_Walletobjects_FlightCarrier();
		$flightCarrier->setCarrierIataCode("LX");
		$flightHeader = new Google_Service_Walletobjects_FlightHeader();
		$flightHeader->setFlightNumber("123");
		$flightHeader->setCarrier($flightCarrier);

		$location = new Google_Service_Walletobjects_LatLongPoint();
		$location->setLatitude(37.424015499999996);
		$location->setLongitude(-122.09259560000001);
		$locations = array($location);

		$textModulesData = new Google_Service_Walletobjects_TextModuleData();
		$textModulesData->setBody("Baconrista flights has served snacks in-flight since its founding.");
		$textModulesData->setHeader("Custom Flight Details");
		$textModulesDatas = array($textModulesData);

		$locationUri = new Google_Service_Walletobjects_Uri();
		$locationUri->setUri("http://maps.google.com/");
		$locationUri->setDescription("Nearby Locations");
		$telephoneUri = new Google_Service_Walletobjects_Uri();
		$telephoneUri->setUri("tel:6505555555");
		$telephoneUri->setDescription("Call Customer Service");
		$linksModuleData = new Google_Service_Walletobjects_LinksModuleData();
		$linksModuleData->setUris(array($locationUri, $telephoneUri));

		$imageUri = new Google_Service_Walletobjects_ImageUri();
		$imageUri->setUri("https://farm8.staticflickr.com/7340/11177041185_a61a7f2139_o.jpg");
		$imageUri->setDescription("Baconrista flights image");
		$image = new Google_Service_Walletobjects_Image();
		$image->setSourceUri($imageUri);
		$imageModulesData = new Google_Service_Walletobjects_ImageModuleData();
		$imageModulesData->setMainImage($image);

		$payload = new Google_Service_Walletobjects_FlightClass();
		//required properties
		$payload->setId($classId);
		$payload->setIssuerName("Baconrista Flights");
		$payload->setReviewStatus("underReview");
		$payload->setDestination($destination);
		$payload->setOrigin($origin);
		$payload->setFlightHeader($flightHeader);
		$payload->setLocalScheduledDepartureDateTime("2023-07-02T15:30:00");
		// optional.  Check design and reference api to decide what's desirable
		$payload->setLocations($locations);
		$payload->setTextModulesData($textModulesDatas);
		$payload->setLinksModuleData($linksModuleData);
		$payload->setImageModulesData($imageModulesData);

		return $payload;
	}

	/******************************
	 *
	 *  Define an Flight Object
	 *
	 * See https://developers.google.com/pay/passes/reference/v1/flightobject
	 *
	 * @param String $classId - The unique identifier for a class
	 * @param String $objectId - The unique identifier for an object
	 * @return Google_Service_Walletobjects_FlightObject $payload - object representing FlightObject resource
	 *
	 *******************************/
	public static function makeFlightObjectResource($classId, $objectId)
	{
		// Define the resource representation of the Object
		// values should be from your DB/services; here we hardcode information
		// below defines an flight object. For more properties, check:
		//// https://developers.google.com/pay/passes/reference/v1/flightobject/insert
		//// https://developers.google.com/pay/passes/guides/pass-verticals/boarding-passes/design

		// There is a client lib to help make the data structure. Newest client is on devsite:
		//// https://developers.google.com/pay/passes/support/libraries#libraries
		// Define Barcode
		$barcode = new Google_Service_Walletobjects_Barcode();
		$barcode->setType("qrCode");
		$barcode->setValue("1234abc");
		$barcode->setAlternateText("optional alternate text");

		$reservationInfo = new Google_Service_Walletobjects_ReservationInfo();
		$reservationInfo->setConfirmationCode("42aQw");

		$boardingAndSeatingInfo = new Google_Service_Walletobjects_BoardingAndSeatingInfo();
		$boardingAndSeatingInfo->setSeatNumber("42");
		$boardingAndSeatingInfo->setBoardingGroup("B");

		// Define flight object
		$payload = new Google_Service_Walletobjects_FlightObject();
		// required properties
		$payload->setClassId($classId);
		$payload->setId($objectId);
		$payload->setState("active");
		$payload->setPassengerName("Sir Bacon the IV");
		$payload->setReservationInfo($reservationInfo);
		// optional.  Check design and reference api to decide what's desirable
		$payload->setBarcode($barcode);
		$payload->setBoardingAndSeatingInfo($boardingAndSeatingInfo);
		return $payload;
	}
	/******************************
	 *
	 *  Define an GiftCard Class
	 *
	 *  See https://developers.google.com/pay/passes/reference/v1/giftcardclass
	 *
	 * @param String $classId - The unique identifier for a class
	 * @return Google_Service_Walletobjects_GiftCardClass $payload - object representing GiftCardClass resource
	 *
	 *******************************/
	public static function makeGiftCardClassResource($classId)
	{
		// Define the resource representation of the Class
		// values should be from your DB/services; here we hardcode information
		// below defines an giftcard class. For more properties, check:
		//// https://developers.google.com/pay/passes/reference/v1/giftcardclass/insert
		//// https://developers.google.com/pay/passes/guides/pass-verticals/gift-cards/design

		// There is a client lib to help make the data structure. Newest client is on devsite:
		//// https://developers.google.com/pay/passes/support/libraries#libraries


		$logoUri = new Google_Service_Walletobjects_ImageUri();
		$logoUri->setUri("http://farm8.staticflickr.com/7340/11177041185_a61a7f2139_o.jpg");
		$logoImage = new Google_Service_Walletobjects_Image();
		$logoImage->setSourceUri($logoUri);

		$textModulesData = new Google_Service_Walletobjects_TextModuleData();
		$textModulesData->setBody("All US gift cards are redeemable in any US and Puerto Rico" .
			" Baconrista retail locations, or online at Baconrista.com where" .
			" available, for merchandise or services.");
		$textModulesData->setHeader("Where to Redeem");
		$textModulesDatas = array($textModulesData);


		$locationUri = new Google_Service_Walletobjects_Uri();
		$locationUri->setUri("http://maps.google.com/");
		$locationUri->setDescription("Nearby Locations");
		$telephoneUri = new Google_Service_Walletobjects_Uri();
		$telephoneUri->setUri("tel:6505555555");
		$telephoneUri->setDescription("Call Customer Service");
		$linksModuleData = new Google_Service_Walletobjects_LinksModuleData();
		$linksModuleData->setUris(array($locationUri, $telephoneUri));

		$location = new Google_Service_Walletobjects_LatLongPoint();
		$location->setLatitude(37.424015499999996);
		$location->setLongitude(-122.09259560000001);
		$locations = array($location);

		$payload = new Google_Service_Walletobjects_GiftCardClass();
		//required properties
		$payload->setId($classId);
		$payload->setReviewStatus("underReview");
		$payload->setIssuerName("Baconrista Gift Cards");
		// optional.  Check design and reference api to decide what's desirable
		$payload->setMerchantName("Baconrista");
		$payload->setProgramLogo($logoImage);
		$payload->setTextModulesData($textModulesDatas);
		$payload->setLinksModuleData($linksModuleData);
		$payload->setLocations($locations);
		$payload->setAllowMultipleUsersPerObject(true);

		return $payload;
	}

	/******************************
	 *
	 *  Define an GiftCard Object
	 *
	 * See https://developers.google.com/pay/passes/reference/v1/giftcardobject
	 *
	 * @param String $classId - The unique identifier for a class
	 * @param String $objectId - The unique identifier for an object
	 * @return Google_Service_Walletobjects_GiftCardObject $payload - object representing GiftCardObject resource
	 *
	 *******************************/
	public static function makeGiftCardObjectResource($classId, $objectId)
	{
		// Define the resource representation of the Object
		// values should be from your DB/services; here we hardcode information
		// below defines an giftcard object. For more properties, check:
		//// https://developers.google.com/pay/passes/reference/v1/giftcardobject/insert
		//// https://developers.google.com/pay/passes/guides/pass-verticals/gift-cards/design

		// There is a client lib to help make the data structure. Newest client is on devsite:
		//// https://developers.google.com/pay/passes/support/libraries#libraries
		// Define Barcode
		$barcode = new Google_Service_Walletobjects_Barcode();
		$barcode->setType("qrCode");
		$barcode->setValue("1234abc");
		$barcode->setAlternateText("optional alternate text");

		$balance = new Google_Service_Walletobjects_Money();
		$balance->setMicros(20000000);
		$balance->setCurrencyCode("USD");

		$balanceUpdateTime = new Google_Service_Walletobjects_DateTime();
		$balanceUpdateTime->setDate("2023-04-12T11:20:50.52Z");

		$textModulesData = new Google_Service_Walletobjects_TextModuleData();
		$textModulesData->setBody("Jane, don\"t forget to use your Baconrista Rewards when  " .
			"paying with this gift card to earn additional points. ");
		$textModulesData->setHeader("Earn double points");
		$textModulesDatas = array($textModulesData);

		// Define giftcard object
		$payload = new Google_Service_Walletobjects_GiftCardObject();
		// required properties
		$payload->setClassId($classId);
		$payload->setId($objectId);
		$payload->setState("active");
		$payload->setCardNumber("123jkl4889");
		// optional.  Check design and reference api to decide what's desirable
		$payload->setBarcode($barcode);
		$payload->setPin("1111");
		$payload->setBalance($balance);
		$payload->setBalanceUpdateTime($balanceUpdateTime);
		$payload->setTextModulesData($textModulesDatas);

		return $payload;
	}
	/******************************
	 *
	 *  Define an Loyalty Class
	 *
	 *  See https://developers.google.com/pay/passes/reference/v1/loyaltyclass
	 *
	 * @param String $classId - The unique identifier for a class
	 * @return Google_Service_Walletobjects_LoyaltyClass $payload - object representing LoyaltyClass resource
	 *
	 *******************************/
	public static function makeLoyaltyClassResource($classId, $data_info)
	{
		// Define the resource representation of the Class
		// values should be from your DB/services; here we hardcode information
		// below defines an loyalty class. For more properties, check:
		//// https://developers.google.com/pay/passes/reference/v1/loyaltyclass/insert
		//// https://developers.google.com/pay/passes/guides/pass-verticals/loyalty/design

		// There is a client lib to help make the data structure. Newest client is on devsite:
		//// https://developers.google.com/pay/passes/support/libraries#libraries

		$logoUri = new Google_Service_Walletobjects_ImageUri();
		//$logoUri->setUri("https://o2o-c3c.artech-appmaker.com/assets/resource/card03-logo.png");
		$logoUri->setUri(base_url($data_info['logo']));
		$logoImage = new Google_Service_Walletobjects_Image();
		$logoImage->setSourceUri($logoUri);


		$textModulesData = new Google_Service_Walletobjects_TextModuleData();
		$textModulesData->setBody($data_info['description']);
		$textModulesData->setHeader("Description");
		$textModulesDatas = array($textModulesData);


		$locationUri = new Google_Service_Walletobjects_Uri();
		$locationUri->setUri("http://maps.google.com/");
		$locationUri->setDescription("Nearby Locations");
		$telephoneUri = new Google_Service_Walletobjects_Uri();
		$telephoneUri->setUri("tel:6505555555");
		$telephoneUri->setDescription("Call Customer Service");
		$linksModuleData = new Google_Service_Walletobjects_LinksModuleData();
		$linksModuleData->setUris(array($locationUri, $telephoneUri));

		$imageUri = new Google_Service_Walletobjects_ImageUri();
		//$imageUri->setUri("https://o2o.artech-onlinesystems.com/images/card03-bg.png");
		//$imageUri->setUri("https://o2o-c3c.artech-appmaker.com/assets/resource/card03-bg.png");
		$imageUri->setUri(base_url($data_info["strip"]));

		$imageHero = new Google_Service_Walletobjects_Image();
		$imageHero->setSourceUri($imageUri);

		$imageUri = new Google_Service_Walletobjects_ImageUri();
		//$imageUri->setUri("https://o2o-c3c.artech-appmaker.com/assets/resource/coffee-bg.png");
		$imageUri->setUri(base_url($data_info["strip"]));
		$imageUri->setDescription($data_info['description']);
		$image = new Google_Service_Walletobjects_Image();
		$image->setSourceUri($imageUri);

		$imageModulesData = new Google_Service_Walletobjects_ImageModuleData();
		$imageModulesData->setMainImage($image);

		$location = new Google_Service_Walletobjects_LatLongPoint();
		$location->setLatitude(22.3213225);
		$location->setLongitude(114.16622);
		$locations = array($location);



		$startItemField = new Google_Service_Walletobjects_FieldReference();
		$startItemField->setFieldPath("class.textModulesData['myfield1']");

		$startItemFirstValue = new Google_Service_Walletobjects_FieldSelector();
		$startItemFirstValue->setFields(array($startItemField));

		$startItem = new Google_Service_Walletobjects_TemplateItem();
		$startItem->setFirstValue($startItemFirstValue);

		$middleItemField = new Google_Service_Walletobjects_FieldReference();
		$middleItemField->setFieldPath("class.textModulesData['myfield2']");

		$middleItemFirstValue = new Google_Service_Walletobjects_FieldSelector();
		$middleItemFirstValue->setFields(array($middleItemField));

		$middleItem = new Google_Service_Walletobjects_TemplateItem();
		$middleItem->setFirstValue($middleItemFirstValue);

		$endItemField = new Google_Service_Walletobjects_FieldReference();
		$endItemField->setFieldPath("class.textModulesData['myfield3']");

		$endItemFirstValue = new Google_Service_Walletobjects_FieldSelector();
		$endItemFirstValue->setFields(array($endItemField));

		$endItem = new Google_Service_Walletobjects_TemplateItem();
		$endItem->setFirstValue($endItemFirstValue);

		$cardRowTemplate = new Google_Service_Walletobjects_CardRowThreeItems();
		$cardRowTemplate->setStartItem($startItem);
		$cardRowTemplate->setMiddleItem($middleItem);
		$cardRowTemplate->setEndItem($endItem);

		$cardRowTemplateInfo1 = new Google_Service_Walletobjects_CardRowTemplateInfo();
		$cardRowTemplateInfo1->setThreeItems($cardRowTemplate);



		$cardTemplateOverride = new Google_Service_Walletobjects_CardTemplateOverride();
		$cardTemplateOverride->setCardRowTemplateInfos(array($cardRowTemplateInfo1));

		$classTemplateInfo = new Google_Service_Walletobjects_ClassTemplateInfo();
		$classTemplateInfo->setCardTemplateOverride($cardTemplateOverride);


		$payload = new Google_Service_Walletobjects_LoyaltyClass();
		//required properties
		//
		$payload->setTextModulesData($textModulesDatas);
		$payload->setClassTemplateInfo($classTemplateInfo);


		$payload->setId($classId);
		$payload->setIssuerName($data_info["logo_text"]);
		$payload->setProgramName($data_info['description']);
		$payload->setProgramLogo($logoImage);
		$payload->setReviewStatus("underReview");
		$payload->hexBackgroundColor = $data_info['background_color']; // this is change the global background color, cannot change the font colour, it is determine by the google base on the background color

		// optional.  Check design and reference api to decide what's desirable
		$payload->setHeroImage($imageHero);
		$payload->setTextModulesData($textModulesDatas);
		//$payload->setLinksModuleData($linksModuleData);
		$payload->setImageModulesData($imageModulesData);
		$payload->setRewardsTier("Gold");
		$payload->setRewardsTierLabel("Membership");
		$payload->setLocations($locations);


		//var_dump($payload);
		//exit;
		return $payload;
	}

	/******************************
	 *
	 *  Define an Loyalty Object
	 *
	 * See https://developers.google.com/pay/passes/reference/v1/loyaltyobject
	 *
	 * @param String $classId - The unique identifier for a class
	 * @param String $objectId - The unique identifier for an object
	 * @return Google_Service_Walletobjects_LoyaltyObject $payload - object representing LoyaltyObject resource
	 *
	 *******************************/
	public static function makeLoyaltyObjectResource($classId, $objectId, $data_info, $user_info, $card_setting, $level_setting)
	{
		// Define the resource representation of the Object
		// values should be from your DB/services; here we hardcode information
		// below defines an loyalty object. For more properties, check:
		//// https://developers.google.com/pay/passes/reference/v1/loyaltyobject/insert
		//// https://developers.google.com/pay/passes/guides/pass-verticals/loyalty/design

		// There is a client lib to help make the data structure. Newest client is on devsite:
		//// https://developers.google.com/pay/passes/support/libraries#libraries
		// Define Barcode
		$barcode = new Google_Service_Walletobjects_Barcode();
		$barcode->setType("qrCode");
		$barcode->setValue($user_info['id']);
		$barcode->setAlternateText($user_info["account_id"]);



		$accountUri = new Google_Service_Walletobjects_Uri();
		$accountUri->setUri($data_info["url"]);
		$accountUri->setDescription("快速連結");
		$linksModuleData = new Google_Service_Walletobjects_LinksModuleData();
		$linksModuleData->setUris(array($accountUri));


		//if($data_info['header_data'] == "level"){
		/*
		$balance = new Google_Service_Walletobjects_LoyaltyPointsBalance();
		$balance->setString($level_setting['value']);
		$secondaryLoyaltyPoints = new Google_Service_Walletobjects_LoyaltyPoints();
		$secondaryLoyaltyPoints->setBalance($balance);
		$secondaryLoyaltyPoints->setLabel("Level");
		*/
		//}else{
		/*
		$balance = new Google_Service_Walletobjects_LoyaltyPointsBalance();
		$balance->setString($user_info['points']);
		$loyaltyPoints = new Google_Service_Walletobjects_LoyaltyPoints();
		$loyaltyPoints->setBalance($balance);
		$loyaltyPoints->setLabel("Points");
		*/
		//}










		$textModulesData_Reward = new Google_Service_Walletobjects_TextModuleData();
		if ($data_info['header_data'] == "level") {
			$textModulesData_Reward->setBody($level_setting['value']);
			$textModulesData_Reward->setHeader("Level");
			$textModulesData_Reward->setId("Reward");
		} else {
			$textModulesData_Reward->setBody($user_info['points']);
			$textModulesData_Reward->setHeader("Points");
			$textModulesData_Reward->setId("Reward");
		}





		$textModulesData3 = new Google_Service_Walletobjects_TextModuleData();
		switch (true) {
			case $card_setting['front_data_1_data'] == "gender":
				$textModulesData3->setBody($user_info[$card_setting['front_data_1_data']] == 1 ? "男" : "女");
				break;
			case $data_info['type'] == 1 && $card_setting['front_data_1_data'] == "level":
				$textModulesData3->setBody($user_info['level_api']);
				break;
			case $card_setting['front_data_1_data'] == "level":
				$textModulesData3->setBody($level_setting['value']);
				break;
			default:
				$textModulesData3->setBody($user_info[$card_setting['front_data_1_data']]);
				break;
		}
		$textModulesData3->setHeader($card_setting['front_data_1_label']);
		if ($card_setting['front_data_1_status'] == "1") {
			$textModulesData3->setId("myfield_1");
		}

		$textModulesData4 = new Google_Service_Walletobjects_TextModuleData();
		switch (true) {
			case $card_setting['front_data_2_data'] == "gender":
				$textModulesData4->setBody($user_info[$card_setting['front_data_2_data']] == 1 ? "男" : "女");
				break;
			case $data_info['type'] == 1 && $card_setting['front_data_2_data'] == "level":
				$textModulesData4->setBody($user_info['level_api']);
				break;
			case $card_setting['front_data_2_data'] == "level":
				$textModulesData4->setBody($level_setting['value']);
				break;
			default:
				$textModulesData4->setBody($user_info[$card_setting['front_data_2_data']]);
				break;
		}
		$textModulesData4->setHeader($card_setting['front_data_2_label']);
		if ($card_setting['front_data_2_status'] == "1") {
			$textModulesData4->setId("myfield_2");
		}

		$textModulesData5 = new Google_Service_Walletobjects_TextModuleData();
		switch (true) {
			case $card_setting['front_data_3_data'] == "gender":
				$textModulesData5->setBody($user_info[$card_setting['front_data_3_data']] == 1 ? "男" : "女");
				break;
			case $data_info['type'] == 1 && $card_setting['front_data_3_data'] == "level":
				$textModulesData5->setBody($user_info['level_api']);
				break;
			case $card_setting['front_data_3_data'] == "level":
				$textModulesData5->setBody($level_setting['value']);
				break;
			default:
				$textModulesData5->setBody($user_info[$card_setting['front_data_3_data']]);
				break;
		}
		$textModulesData5->setHeader($card_setting['front_data_3_label']);
		if ($card_setting['front_data_3_status'] == "1") {
			$textModulesData5->setId("myfield_3");
		}

		$textModulesData6 = new Google_Service_Walletobjects_TextModuleData();
		switch (true) {
			case $card_setting['front_data_4_data'] == "gender":
				$textModulesData6->setBody($user_info[$card_setting['front_data_4_data']] == 1 ? "男" : "女");
				break;
			case $data_info['type'] == 1 && $card_setting['front_data_4_data'] == "level":
				$textModulesData6->setBody($user_info['level_api']);
				break;
			case $card_setting['front_data_4_data'] == "level":
				$textModulesData6->setBody($level_setting['value']);
				break;
			default:
				$textModulesData6->setBody($user_info[$card_setting['front_data_4_data']]);
				break;
		}

		$textModulesData6->setHeader($card_setting['front_data_4_label']);
		if ($card_setting['front_data_4_status'] == "1") {
			$textModulesData6->setId("myfield_4");
		}

		//$textModulesDatas = array($textModulesData1, $textModulesData2);
		$textModulesDatas = array($textModulesData3, $textModulesData4, $textModulesData5, $textModulesData6);



		// Define loyalty object
		$payload = new Google_Service_Walletobjects_LoyaltyObject();
		// required properties

		$payload->setTextModulesData($textModulesDatas);


		$payload->setClassId($classId);
		$payload->setId($objectId);
		$payload->setState("active");
		// optional.  Check design and reference api to decide what's desirable
		$payload->setBarcode($barcode);
		//$payload->setAccountId("001-23354-565-11");
		$payload->setAccountId($user_info['account_id']);
		//$payload->setAccountName("Spencer Li");
		//$payload->setTextModulesData($textModulesDatas);
		$payload->setLinksModuleData($linksModuleData);
		//$payload->setLocations($locations);
		//$payload->setMessages($messages);
		//if($data_info['header_data'] == "level"){
		//$payload->setSecondaryLoyaltyPoints($secondaryLoyaltyPoints);
		//}else{
		//$payload->setLoyaltyPoints($loyaltyPoints);
		//}

		//$payload->setInfoModuleData($infoModuleData);

		return $payload;
	}
	/******************************
	 *
	 *  Define an Transit Class
	 *
	 *  See https://developers.google.com/pay/passes/reference/v1/transitclass
	 *
	 * @param String $classId - The unique identifier for a class
	 * @return Google_Service_Walletobjects_TransitClass $payload - object representing TransitClass resource
	 *
	 *******************************/
	public static function makeTransitClassResource($classId)
	{
		// Define the resource representation of the Class
		// values should be from your DB/services; here we hardcode information
		// below defines an transit class. For more properties, check:
		//// https://developers.google.com/pay/passes/reference/v1/transitclass/insert
		//// https://developers.google.com/pay/passes/guides/pass-verticals/transit-passes/design

		// There is a client lib to help make the data structure. Newest client is on devsite:
		//// https://developers.google.com/pay/passes/support/libraries#libraries
		$titleImageUri = new Google_Service_Walletobjects_ImageUri();
		$titleImageUri->setUri("https://live.staticflickr.com/65535/48690277162_cd05f03f4d_o.png");
		$titleImage = new Google_Service_Walletobjects_Image();
		$titleImage->setSourceUri($titleImageUri);


		$payload = new Google_Service_Walletobjects_TransitClass();
		//required properties
		$payload->setId($classId);
		$payload->setIssuerName("Baconrista Bus");
		$payload->setReviewStatus("underReview");
		$payload->setTransitType("bus");
		$payload->setLogo($titleImage);
		return $payload;
	}

	/******************************
	 *
	 *  Define an Transit Object
	 *
	 * See https://developers.google.com/pay/passes/reference/v1/transitobject
	 *
	 * @param String $classId - The unique identifier for a class
	 * @param String $objectId - The unique identifier for an object
	 * @return Google_Service_Walletobjects_TransitObject $payload - object representing TransitObject resource
	 *
	 *******************************/
	public static function makeTransitObjectResource($classId, $objectId)
	{
		// Define the resource representation of the Object
		// values should be from your DB/services; here we hardcode information
		// below defines an transit object. For more properties, check:
		//// https://developers.google.com/pay/passes/reference/v1/transitobject/insert
		//// https://developers.google.com/pay/passes/guides/pass-verticals/transit-passes/design

		// There is a client lib to help make the data structure. Newest client is on devsite:
		//// https://developers.google.com/pay/passes/support/libraries#libraries
		// Define Barcode
		$barcode = new Google_Service_Walletobjects_Barcode();
		$barcode->setType("qrCode");
		$barcode->setValue("1234abc");
		$barcode->setAlternateText("optional alternate text");

		$localFare = new Google_Service_Walletobjects_LocalizedString();
		$localFareTranslated = new Google_Service_Walletobjects_TranslatedString();
		$localFareTranslated->setLanguage("en-US");
		$localFareTranslated->setValue("Anytime Single Use");
		$localFare->setDefaultValue($localFareTranslated);
		$localDestinationName = new Google_Service_Walletobjects_LocalizedString();
		$localDestinationNameTranslated = new Google_Service_Walletobjects_TranslatedString();
		$localDestinationNameTranslated->setLanguage("en-US");
		$localDestinationNameTranslated->setValue("SFO Transit Center");
		$localDestinationName->setDefaultValue($localDestinationNameTranslated);
		$localOriginName = new Google_Service_Walletobjects_LocalizedString();
		$localOriginNameTranslated = new Google_Service_Walletobjects_TranslatedString();
		$localOriginNameTranslated->setLanguage("en-US");
		$localOriginNameTranslated->setValue("SFO Transit Center");
		$localOriginName->setDefaultValue($localOriginNameTranslated);
		$ticketleg = new Google_Service_Walletobjects_TicketLeg();
		$ticketleg->setArrivalDateTime("2020-04-12T20:20:50.52Z");
		$ticketleg->setDepartureDateTime("2020-04-12T16:20:50.52Z");
		$ticketleg->setOriginStationCode("LA");
		$ticketleg->setDestinationStationCode("SFO");
		$ticketleg->setDestinationName($localDestinationName);
		$ticketleg->setOriginName($localOriginName);
		$ticketleg->setFareName($localFare);


		// Define transit object
		$payload = new Google_Service_Walletobjects_TransitObject();
		// required properties
		$payload->setClassId($classId);
		$payload->setId($objectId);
		$payload->setState("active");
		$payload->setTripType("oneWay");
		// optional.  Check design and reference api to decide what's desirable
		$payload->setBarcode($barcode);
		$payload->setPassengerNames("Sir Bacon the IV");
		$payload->setPassengerType("singlePassenger");
		$payload->setTicketLegs(array($ticketleg));


		return $payload;
	}
}
