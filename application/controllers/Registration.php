<?php


use PKPass\PKPass;

require_once "Config.php";
require_once "Services.php";
require dirname(__DIR__) . "/libraries/phpqrcode/qrlib.php";

use Sunaoka\PushNotifications\Drivers\APNs;
use Sunaoka\PushNotifications\Pusher;


class Registration extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function apple_registration($id)
    {
        $this->load->model("Virtual_passes_model");
        $card_info = $this->Virtual_passes_model->get_id($id);
        $data = [];
        $data['pass_id'] = $id;
        $data['platform'] = "apple";
        $data['card_info'] = $card_info;
        $this->load->view("registration_form", $data);
    }

    public function google_registration($id)
    {
        $this->load->model("Virtual_passes_model");
        $card_info = $this->Virtual_passes_model->get_id($id);
        $data = [];
        $data['pass_id'] = $id;
        $data['platform'] = "google";
        $data['card_info'] = $card_info;
        $this->load->view("registration_form", $data);
    }

    public function merge_registration($id)
    {
        $this->load->model("Virtual_passes_model");
        $card_info = $this->Virtual_passes_model->get_id($id);
        if ($card_info['type'] == 0) {
            $info_config = $this->Virtual_passes_model->get_form_config($id);
            $data['pass_id'] = $id;
            $data["card_info"] = $card_info;
            $data['info_config'] = $info_config;
            $this->load->view("new_registration_form", $data);
        } else {
            $this->not_found();
        }
    }

    public function registration()
    {
        $request = $this->input->post();
        $this->load->model("Registration_model");
        $member_id = $this->Registration_model->registration($request);
        $hash = "";
        for ($i = 1; $i <= 8; $i++) {
            $hash .= sha1($hash) . sha1($hash . time());
        }
        $this->db->update("tbl_members", ['hash' => $hash], ["id" => $member_id]);
        QRcode::png(base_url("registration/retrieve/" . $member_id . "?v=" . $hash), $_SERVER["DOCUMENT_ROOT"] . "/assets/user_qr/membership/" . $member_id . "_" . "QR.png");
        if ($request['platform'] == "apple") {
            $this->apple_pass_output($request['pass_id'], $member_id);
        } else {
            //$this->get_google_pay_link($request['pass_id'], $member_id);
            $this->google_pay_registration($request['pass_id'], $member_id);
        }
    }



    public function registration_test($v1, $v2, $v3)
    {

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $post =  json_decode(file_get_contents('php://input'), true);
            $data['devicelibraryidentifier'] = $v1;
            $data['passtypeidentifier'] = $v2;
            $data['serialnumber'] = $v3;
            $data['pushToken'] = $post["pushToken"];
            $this->db->insert("tbl_temp_registration", $data);
        }
    }

    public function update_apple_card($v1, $v2)
    {

        //$get = $this->input->get();

        $user = $this->db->select("*")->from("tbl_members")->where("id", $v2)->get()->row_array();
        $pass = $this->db->select("*")->from("tbl_virtual_passes")->where("id", $user['pass_id'])->get()->row_array();
        $this->apple_pass_output($pass['id'], $user['id']);
    }

    public function update_handler($v1, $v2)
    {

        $get = $this->input->get();
        if ($get != null) {
            $this->db->insert("tbl_update_testing", ["pass_id" => $v2, "devicelibraryidentifier" => strval($v1), "passesUpdatedSince" => $get["passesUpdatedSince"]]);
        } else {
            $this->db->insert("tbl_update_testing", ["pass_id" => $v2, "devicelibraryidentifier" => strval($v1)]);
        }

        $updates = $this->db->select("*")->from("tbl_members")->get()->result_array();
        foreach ($updates as $update) {
            $serials[] = $update['id'];
        }
        $serial = array(
            //"serialNumbers" => ["103"],
            "serialNumbers" => $serials,
            "lastUpdated" => strval((time() - 978307200))
        );
        echo json_encode($serial);
    }


    public function apple_pass_output($id, $member_id)
    {
        $pass = new PKPass("apple_pay_passes/" . $this->config->item("apple_cert")); //Input the apple cert's name
        $temp_data = $this->db->select("*")->from("tbl_virtual_passes")->where("id", $id)->get()->row_array();
        $user_info = $this->db->select("*")->from("tbl_members")->where('id', $member_id)->get()->row_array();
        $location_setting = $this->db->select("*")->from("tbl_virtual_passes_locations")->where("id", $id)->get()->row_array();
        $card_setting = $this->db->select("*")->from("tbl_virtual_passes_data_config")->where("id", $id)->get()->row_array();
        $level_setting = $this->db->select("*")->from("tbl_virtual_passes_levels")->where("id", $user_info["level"])->get()->row_array();
        $msg = $this->db->select("*")->from("tbl_testing_notification")->where("serial", $member_id)->order_by("timestamp", "desc")->get()->row_array();
        $location_trigger = 0;
        $locations_fields = [];
        for ($i = 1; $i <= 3; $i++) {
            if ($location_setting["location_" . $i . "_status"] == "1") {
                $location_trigger = 1;
                $locations_fields[] = [
                    "latitude" => floatval($location_setting['location_' . $i . "_lat"]),
                    "longitude" => floatval($location_setting['location_' . $i . "_lon"]),
                    "relevantText" => $location_setting["location_" . $i . "_message"]
                ];
            }
        }

        $front_fields = [];
        for ($i = 1; $i <= 4; $i++) {
            if ($card_setting["front_data_" . $i . "_status"] == 1) {
                switch (true) {
                    case $card_setting['front_data_' . $i . "_data"] == "gender":
                        $front_fields[] = array(
                            "key" => "sf" . $i,
                            "label" => $card_setting["front_data_" . $i . "_label"],
                            "value" => $user_info[$card_setting["front_data_" . $i . "_data"]] == 1 ? "男" : "女"
                        );
                        break;
                    case $temp_data['type'] == 1 && $card_setting['front_data_' . $i . "_data"] == "level":
                        $front_fields[] = array(
                            "key" => "sf" . $i,
                            "label" => $card_setting['front_data_' . $i . "_label"],
                            "value" => $user_info['level_api']
                        );
                        break;
                    case $card_setting['front_data_' . $i . "_data"] == "level":
                        $front_fields[] = array(
                            "key" => "sf" . $i,
                            "label" => $card_setting["front_data_" . $i . "_label"],
                            "value" => $level_setting['value']
                        );
                        break;
                    default:
                        $front_fields[] = array(
                            "key" => "sf" . $i,
                            "label" => $card_setting["front_data_" . $i . "_label"],
                            "value" => $user_info[$card_setting["front_data_" . $i . "_data"]]
                        );
                        break;
                }
            }
        }
        $header_fields = [];
        if ($temp_data["header_data"] == "level") {
            $header_fields[] = [
                "key" => "hf",
                "label" => "Level",
                "value" => $level_setting['value']
            ];
        } else {
            $header_fields[] = [
                "key" => "hf",
                "label" => "Points",
                "value" => $user_info['points']
            ];
        }


        if ($msg != "") {
            if ($msg['sent'] == 1) {
                $data = [
                    "formatVersion" => 1,
                    "passTypeIdentifier" => $this->config->item("topic"),
                    "serialNumber" => $user_info['id'],
                    "teamIdentifier" => $this->config->item("team_id"),
                    "webServiceURL" => base_url(),
                    "authenticationToken" => $this->config->item("auth_token"),

                    "organizationName" => $temp_data['description'],
                    "description" => $temp_data['description'],
                    "foregroundColor" => $temp_data['pri_color'],
                    "backgroundColor" => $temp_data["background_color"],
                    "labelColor" => $temp_data["secondary_color"],
                    "barcodes" => array(
                        array(
                            "message" => $user_info['api'] == 1 ? $user_info['ref_id'] : $user_info['id'],
                            "altText" => $user_info['api'] == 1 ? $user_info['ref_id'] : $user_info['account_id'],
                            "format" => "PKBarcodeFormatQR",
                            "messageEncoding" => "iso-8859-1"
                        )
                    ),
                    "suppressStripShine" => true,
                    "storeCard" => array(
                        //"headerFields" => $header_fields,

                        "secondaryFields" => $front_fields,


                        "backFields" => array(
                            array(
                                "key" => "bf1",
                                "value" => "",
                                "label" => "Our Website",
                                "attributedValue" => "<a href='" . $temp_data['url'] . "'>" . $temp_data['url'] . "</a>"
                            ),
                            array(
                                "key" => "bf1",
                                "value" => $temp_data['about_us'],
                                "label" => "About Us",

                            ),
                            array(
                                "key" => "bf1",
                                "value" => "",
                                "label" => "Terms & Conditions",
                                "attributedValue" => $temp_data['terms_conditions']
                            ),
                        )
                    )
                ];

                $data['logoText'] = $temp_data['logo_text'];

                if ($location_trigger == true) {
                    $data["locations"] = $locations_fields;
                }
                $pass->setData($data);
            } else {
                $this->db->set("sent", 1)->where("id", $msg['id'])->update("tbl_testing_notification");
                $data = [
                    "formatVersion" => 1,
                    "passTypeIdentifier" => $this->config->item("topic"),
                    "serialNumber" => $user_info['id'],
                    "teamIdentifier" => $this->config->item("team_id"),
                    "webServiceURL" => base_url(),
                    "authenticationToken" => $this->config->item("auth_token"),

                    "organizationName" => $temp_data['description'],
                    "description" => $temp_data['description'],
                    "foregroundColor" => $temp_data['pri_color'],
                    "backgroundColor" => $temp_data["background_color"],
                    "labelColor" => $temp_data["secondary_color"],
                    "barcodes" => array(
                        array(
                            "message" => $user_info['api'] == 1 ? $user_info['ref_id'] : $user_info['id'],
                            "altText" => $user_info['api'] == 1 ? $user_info['ref_id'] : $user_info['account_id'],
                            "format" => "PKBarcodeFormatQR",
                            "messageEncoding" => "iso-8859-1"
                        )
                    ),
                    "suppressStripShine" => true,
                    "storeCard" => array(
                        //"headerFields" => $header_fields,

                        "secondaryFields" => $front_fields,


                        "backFields" => array(
                            array(
                                "key" => "bf1",
                                "value" => "",
                                "label" => "Our Website",
                                "attributedValue" => "<a href='" . $temp_data['url'] . "'>" . $temp_data['url'] . "</a>"
                            ),
                            array(
                                "key" => "bf1",
                                "value" => $temp_data['about_us'],
                                "label" => "About Us",

                            ),    array(
                                "key" => "news",
                                "value" => $msg['message'],
                                "label" => "Latest Notification",
                                "changeMessage" => "%@"
                            ),
                            array(
                                "key" => "bf1",
                                "value" => "",
                                "label" => "Terms & Conditions",
                                "attributedValue" => $temp_data['terms_conditions']
                            ),
                        )
                    )
                ];

                $data['logoText'] = $temp_data['logo_text'];

                if ($location_trigger == true) {
                    $data["locations"] = $locations_fields;
                }

                $pass->setData($data);
            }
        } else {
            $data = [
                "formatVersion" => 1,
                "passTypeIdentifier" => $this->config->item("topic"),
                "serialNumber" => $user_info['id'],
                "teamIdentifier" => $this->config->item("team_id"),
                "webServiceURL" => base_url(),
                "authenticationToken" => $this->config->item("auth_token"),

                "organizationName" => $temp_data['description'],
                "description" => $temp_data['description'],
                "foregroundColor" => $temp_data['pri_color'],
                "backgroundColor" => $temp_data["background_color"],
                "labelColor" => $temp_data["secondary_color"],
                "barcodes" => array(
                    array(
                        "message" => $user_info['api'] == 1 ? $user_info['ref_id'] : $user_info['id'],
                        "altText" => $user_info['api'] == 1 ? $user_info['ref_id'] : $user_info['account_id'],
                        "format" => "PKBarcodeFormatQR",
                        "messageEncoding" => "iso-8859-1"
                    )
                ),
                "suppressStripShine" => true,
                "storeCard" => array(
                    //"headerFields" => $header_fields,

                    "secondaryFields" => $front_fields,


                    "backFields" => array(
                        array(
                            "key" => "bf1",
                            "value" => "",
                            "label" => "Our Website",
                            "attributedValue" => "<a href='" . $temp_data['url'] . "'>" . $temp_data['url'] . "</a>"
                        ),
                        array(
                            "key" => "bf1",
                            "value" => "",
                            "label" => "About Us",
                            "attributedValue" => $temp_data['about_us']

                        ),
                        array(
                            "key" => "bf1",
                            "value" => "",
                            "label" => "Terms & Conditions",
                            "attributedValue" => $temp_data['terms_conditions']
                        ),
                    )
                )
            ];

            $data['logoText'] = $temp_data['logo_text'];

            if ($location_trigger == true) {
                $data["locations"] = $locations_fields;
            }

            $pass->setData($data);
        }
        //var_dump($pass);
        //exit;
        // Add files to the pass package
        $pass->addFile($_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/membership/" . $id . "/" . 'icon.png');
        $pass->addFile($_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/membership/" . $id . "/" . 'logo.png');
        $pass->addFile($_SERVER['DOCUMENT_ROOT'] . "/public/images/uploads/membership/" . $id . "/" . 'strip.png');
        //$pass->addFile('apple_pay_passes/Test10.pass/icon@2x.png');
        //$pass->addFile('apple_pay_passes/Test10.pass/logo.png');
        if (!$pass->create(true)) {
            echo 'Error: ' . $pass->getError();
        }
    }

    public function get_google_pay_link($db_id, $member_id)
    {
        $option = "l";
        $vertical = "LOYALTY";
        $verticalType = VerticalType::LOYALTY;

        while (strpos(" beglotq", $option) == false) {
            $option = readline("\n\n*****************************\n" .
                "Which pass type would you like to demo?\n" .
                "b - Boarding Pass\n" .
                "e - Event Ticket\n" .
                "g - Gift Card\n" .
                "l - Loyalty\n" .
                "o - Offer\n" .
                "t - Transit\n" .
                "q - Quit\n" .
                "\n\nEnter your choice:");
            if ($option == "b") {
                $verticalType = VerticalType::FLIGHT;
                $vertical = "FLIGHT";
            } elseif ($option == "e") {
                $verticalType = VerticalType::EVENTTICKET;
                $vertical = "EVENTTICKET";
            } elseif ($option == "g") {
                $verticalType = VerticalType::GIFTCARD;
                $vertical = "GIFTCARD";
            } elseif ($option == "l") {
                $verticalType = VerticalType::LOYALTY;
                $vertical = "LOYALTY";
            } elseif ($option == "o") {
                $verticalType = VerticalType::OFFER;
                $vertical = "OFFER";
            } elseif ($option == "t") {
                $verticalType = VerticalType::TRANSIT;
                $vertical = "TRANSIT";
            } elseif ($option == "q") {
                exit();
            } else {
                printf("\n* Invalid option - $option. Please select one of the pass types by entering it's related letter.\n");
            }
        }
        $data_info = $this->db->select("*")->from("tbl_virtual_passes")->where("id", $db_id)->get()->row_array();
        $user_info = $this->db->select("*")->from("tbl_members")->where('id', $member_id)->get()->row_array();

        $classUid = $vertical . "_CLASS_" . uniqid('', true); // CHANGEME

        $classId = sprintf("%s.%s", ISSUER_ID, $classUid);

        $objectUid = $vertical . "_OBJECT_" . uniqid('', true); // CHANGEME



        $objectId = sprintf("%s.%s", ISSUER_ID, $objectUid);


        $link = $this->demoSkinnyJwt($verticalType, $classId, $objectId, $data_info, $user_info);



        redirect($link);
    }

    function google_pay_registration($pass_id, $member_id)
    {
        $services = new Services();
        $data_info = $this->db->select("*")->from("tbl_virtual_passes")->where("id", $pass_id)->get()->row_array();
        $user_info = $this->db->select("*")->from("tbl_members")->where("id", $member_id)->get()->row_array();
        $card_setting = $this->db->select('*')->from("tbl_virtual_passes_data_config")->where("id", $pass_id)->get()->row_array();
        $level_setting = $this->db->select("*")->from("tbl_virtual_passes_levels")->where("id", $user_info['level'])->get()->row_array();


        $vertical = "LOYALTY";
        $objectUid = $vertical . "_OBJECT_" . uniqid('', true); // CHANGEME
        $objectId = sprintf("%s.%s", ISSUER_ID, $objectUid);
        $this->db->insert("tbl_google_registration", ["user_id" => $member_id, "object_uid" => $objectId]);
        $link = $services->makeObject($pass_id, $member_id, $data_info, $user_info, $card_setting, $level_setting, $objectId, $vertical);
        if ($link != null) {
            redirect("https://pay.google.com/gp/v/save/" . $link);
        }
        return;
    }


    function demoSkinnyJwt($verticalType, $classId, $objectId, $data_info, $user_info)
    {
        /*printf(
            "\n#############################\n" .
                "#\n" .
                "#  Generates a signed \"skinny\" JWT.\n" .
                "#  2 REST calls are made:\n" .
                "#  x1 pre-insert one classes\n" .
                "#  x1 pre-insert one object which uses previously inserted class\n" .
                "#\n" .
                "#  This JWT can be used in JS web button.\n" .
                "#  This is the shortest type of JWT; recommended for Android intents/redirects.\n" .
                "#\n" .
                "#############################\n"
        ); */

        $services = new Services();
        $skinnyJwt = $services->makeSkinnyJwt($verticalType, $classId, $objectId, $data_info, $user_info);

        if ($skinnyJwt  != null) {
            //printf("\nThis is an \"skinny\" jwt:\n%s\n" , $skinnyJwt );
            //printf("\nyou can decode it with a tool to see the unsigned JWT representation:\n%s\n" , "https://jwt.io");
            //printf("\nTry this save link in your browser:\n%s%s\n", SAVE_LINK, $skinnyJwt );
            //printf("%s%s", "https://pay.google.com/gp/v/save/", $skinnyJwt);
            //printf("\nthis is the shortest type of JWT; recommended for Android intents/redirects\n\n\n");
            //$this->db->insert("tbl_temp_google_card", ["link" => "https://pay.google.com/gp/v/save/" . $skinnyJwt]);
            //return $id = $this->db->insert_id();
            return "https://pay.google.com/gp/v/save/" . $skinnyJwt;
            //return QRcode::png("https://pay.google.com/gp/v/save/" . $skinnyJwt);
        }

        return;
    }


    public function new_push_test()
    {
        $payload = [
            'aps' => [
                'alert' => [
                    'title' => 'Game Request',
                    'body'  => 'Bob wants to play poker',
                ],
            ],
        ];

        $options = new APNs\Token\Option();
        $options->payload = $payload;
        $options->authKey = FCPATH . 'apple_pay_passes/' . $this->config->item("apple_key");
        $options->keyId = $this->config->item("apple_cert_key_id");
        $options->teamId = $this->config->item("team_id");
        $options->topic = $this->config->item("topic");

        $driver = new APNs\Token($options);
        $push_ids = $this->db->distinct()->select("pushToken")->from("tbl_temp_registration")->get()->result_array();
        foreach ($push_ids as $id) {
            $pusher = new Pusher(true);
            $feedback = $pusher->to($id['pushToken'])->send($driver);
        }
    }

    public function reset_call($user_info)
    {
        $payload = [
            'aps' => [
                'alert' => [
                    'title' => 'Game Request',
                    'body'  => 'Bob wants to play poker',
                ],
            ],
        ];
        $user = $this->db->select("*")->from("tbl_temp_registration")->where("serialnumber", $user_info['id'])->get()->row_array();
        $options = new APNs\Token\Option();
        $options->payload = $payload;
        $options->authKey = FCPATH . 'apple_pay_passes/' . $this->config->item("apple_key");
        $options->keyId = $this->config->item("apple_cert_key_id");
        $options->teamId = $this->config->item("team_id");
        $options->topic = $this->config->item("topic");

        $driver = new APNs\Token($options);
        $pusher = new Pusher(true);
        $feedback = $pusher->to($user['pushToken'])->send($driver);
    }

    public function retrieve($id)
    {
        $hash = $this->input->get("v");
        if ($hash == "") {
            echo "Access Denied";
            exit;
        }
        $result = $this->db->select("id,pass_id,platform,hash")->from("tbl_members")->where("id", $id)->get()->row_array();
        if ($result == "") {
            echo "Access Denied";
            exit;
        }
        if ($result['hash'] != $hash) {
            echo "Access Denied";
            exit;
        }
        if ($result['platform'] == "apple") {
            $this->apple_pass_output($result['pass_id'], $result['id']);
        } else {

            $gr = $this->db->select("*")->from("tbl_google_registration")->where("user_id", $result['id'])->get()->row_array();
            $services = new Services();
            $restMethods = RestMethods::getInstance();
            //$result = $restMethods->getLoyaltyObject($gr['object_uid']);
            $googlePassJwt = new GpapJwt();
            $googlePassJwt->addLoyaltyObject(array("id" => $gr['object_uid']));
            $signedJwt = $googlePassJwt->generateSignedJwt();
            redirect("https://pay.google.com/gp/v/save/" . $signedJwt);
        }
    }

    public function not_found()
    {
        $this->load->view("access_denied", []);
    }
}
