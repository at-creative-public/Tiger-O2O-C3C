<?php

class Login_exception extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function handle_expired()
    {

        $this->load->view("login_banend");
    }

    public function handle_ban()
    {
        $this->load->view("login_banned");
    }
}
