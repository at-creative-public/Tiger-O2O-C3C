<?php

use PKPass\PKPass;

require_once "Config.php";
require_once "Services.php";
require dirname(__DIR__) . "/libraries/phpqrcode/qrlib.php";

use Sunaoka\PushNotifications\Drivers\APNs;
use Sunaoka\PushNotifications\Pusher;

class Eventticketregistration extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Event_ticket_model");
        $this->load->model("Event_ticket_memberbase_model");
        $this->load->model("Registration_model");
    }

    public function registration($id)
    {
        $reg = $this->Event_ticket_model->check_event_type($id);
        $type = $this->Event_ticket_model->check_event_ticket_type($id);
        
        if ($reg === true && $type === true) {
            $data['ticket_info'] = $this->Event_ticket_model->get_event_ticket($id);
            $data['form_setting'] = $this->Event_ticket_model->get_form($id);
            $this->load->view("event_ticket_registration_form", $data);
        } else {
            $this->not_found();
        }
    }

    public function register()
    {
        $request = $this->input->post();
        $reg = $this->Event_ticket_model->check_event_type($request['event_ticket_id']);
        if ($reg === true) {
            $register_id = $this->Registration_model->event_ticket_registration($request);
            if (!file_exists($_SERVER['DOCUMENT_ROOT'] . "/assets/user_qr/event_ticket/" . $register_id . "/")) {
                mkdir($_SERVER["DOCUMENT_ROOT"] . "/assets/user_qr/event_ticket/" . $register_id . "/", 0755);
            }
            $hash = "";
            for ($i = 1; $i <= 8; $i++) {
                $hash .= sha1($hash) . sha1($hash . time());
            }
            QRcode::png(base_url("event_ticket/retrieve/apple/" . $register_id . "?v=" . $hash), $_SERVER["DOCUMENT_ROOT"] . "/assets/user_qr/event_ticket/" . $register_id . "/apple.png");
            $this->Event_ticket_memberbase_model->store_apple_qr("/assets/user_qr/event_ticket/" . $register_id . "/apple.png", $register_id, $hash);
            if ($request['platform'] == "apple") {
                $this->google_handle($request['event_ticket_id'], $register_id, false);
                $this->apple_handle($request['event_ticket_id'], $register_id);
            } else {
                $this->google_handle($request['event_ticket_id'], $register_id);
            }
        } else {
            $this->not_found();
        }
    }

    public function apple_handle($ticket_id, $holder_id)
    {
        $pass = new PKPass("apple_pay_passes/" . $this->config->item("apple_cert")); //Input the apple cert's name
        $ticket_info = $this->Event_ticket_model->get_event_ticket($ticket_id);
        $layout_info = $this->Event_ticket_model->get_layout($ticket_id);
        $holder_info = $this->Event_ticket_memberbase_model->get_holder($holder_id);
        $front_fields = [];
        $seat_string = "";
        if ($holder_info['seat'] !== "") {
            $seat_string .= "Seat:" . $holder_info['seat'] . " ";
        }
        if ($holder_info['row'] !== "") {
            $seat_string .= "Row:" . $holder_info["row"] . " ";
        }
        if ($holder_info['section'] !== "") {
            $seat_string .= "Section:" . $holder_info['section'] . " ";
        }
        for ($i = 1; $i <= 6; $i++) {
            if ($layout_info['front_data_' . $i . '_status'] == 1) {
                switch (true) {

                    case $layout_info['front_data_' . $i . '_data'] == "event_name":
                        $front_fields[] = array(
                            "key" => "sf" . $i,
                            "label" => $layout_info['front_data_' . $i . "_label"],
                            "value" => $ticket_info['event_name'],
                            "row" => $i >= 4 ? 1 : 0
                        );
                        break;

                    case $layout_info['front_data_' . $i . '_data'] == "issuer_name":
                        $front_fields[] = array(
                            "key" => "sf" . $i,
                            "label" => $layout_info['front_data_' . $i . '_label'],
                            "value" => $ticket_info['issuer_name'],
                            "row" => $i >= 4 ? 1 : 0
                        );
                        break;

                    case $layout_info['front_data_' . $i . '_data'] == "event_date":
                        $front_fields[] = array(
                            "key" => "sf" . $i,
                            "label" => $layout_info["front_data_" . $i . "_label"],
                            "value" => $ticket_info['event_start_time'] . "-" . $ticket_info['event_end_time'] . " " . $ticket_info['event_date'],
                            "changeMessage" => "日期更新:%@",
                            "row" => $i >= 4 ? 1 : 0
                        );
                        break;
                    case $layout_info['front_data_' . $i . '_data'] == "event_venue":
                        $front_fields[] = array(
                            "key" => "sf" . $i,
                            "label" => $layout_info["front_data_" . $i . '_label'],
                            "value" => $ticket_info["event_venue"],
                            "changeMessage" => "場地更新:%@",
                            "row" => $i >= 4 ? 1 : 0
                        );
                        break;
                    case $layout_info['front_data_' . $i . '_data'] == "location_detail":
                        $front_fields[] = array(
                            "key" => "sf" . $i,
                            "label" => $layout_info['front_data_' . $i . '_label'],
                            "value" => $ticket_info['location_detail'],
                            "changeMessage" => "場地詳情更新:%@",
                            "row" => $i >= 4 ? 1 : 0
                        );
                        break;
                    case $layout_info['front_data_' . $i . '_data'] == "seat_info":
                        $front_fields[] = array(
                            "key" => "sf" . $i,
                            "label" => $layout_info['front_data_' . $i . '_label'],
                            "value" => $seat_string,
                            "changeMessage" => "位置更新:%@",
                            "row" => $i >= 4 ? 1 : 0
                        );
                        break;
                    case $layout_info['front_data_' . $i . '_data'] == "gender":
                        $front_fields[] = array(
                            "key" => "sf" . $i,
                            "label" => $layout_info["front_data_" . $i . '_label'],
                            "value" => $holder_info['gender'] == 1 ? "男" : "女",
                            "row" => $i >= 4 ? 1 : 0
                        );
                        break;
                    default:
                        $front_fields[] = array(
                            "key" => "sf" . $i,
                            "label" => $layout_info['front_data_' . $i . '_label'],
                            "value" => $holder_info[$layout_info["front_data_" . $i . "_data"]],
                            "row" => $i >= 4 ? 1 : 0
                        );
                        break;
                }
            }
        }

        $data = [
            "formatVersion" => 1,
            "passTypeIdentifier" => $this->config->item("topic"),
            "serialNumber" => "eventticket_" . $holder_info["id"],
            "teamIdentifier" => $this->config->item("team_id"),
            "webServiceURL" => base_url() . "event_ticket/",
            "authenticationToken" => $this->config->item("auth_token"),
            "organizationName" => $ticket_info["issuer_name"],
            "description" => $ticket_info['event_name'],
            'logoText' => $ticket_info['logo_text'],
            "foregroundColor" => $ticket_info['content_color'],
            "labelColor" => $ticket_info['label_color'],
            "backgroundColor" => $ticket_info['background_color'],
            "voided" => $holder_info['status'] == "valid" ? false : true,
            "barcodes" => array(
                array(
                    "message" => $holder_info['api'] == 1 ? $holder_info['ref_id'] : $holder_info['display_code'],
                    "altText" => $holder_info['api'] == 1 ? $holder_info['ref_id'] : $holder_info['display_code'],
                    "format" => "PKBarcodeFormatQR",
                    "messageEncoding" => "iso-8859-1"
                )
            ),
            "eventTicket" => array(

                "secondaryFields" => array(
                    array(
                        "key" => "event_title",
                        "value" => $ticket_info['event_name']
                    )
                ),
                "auxiliaryFields" => $front_fields,

                "backFields" => array(
                    array(
                        "key" => "bf1",
                        "label" => "Our Website",
                        "value" => "",
                        "attributedValue" => "<a href='" . $ticket_info['url'] . "'>" . $ticket_info['url'] . "</a>"
                    ),
                    array(
                        "key" => "bf2",
                        "label" => "About Us",
                        "value" => $ticket_info['about_us']
                    ),
                    array(
                        "key" => "bf3",
                        "label" => "Terms & Conditions",
                        "value" => $ticket_info['terms_conditions']
                    )
                )
            )
        ];
        $pass->setData($data);

        $pass->addFile($_SERVER["DOCUMENT_ROOT"] . "/public/images/uploads/event_ticket/" . $ticket_id . "/icon.png");
        $pass->addFile($_SERVER["DOCUMENT_ROOT"] . "/public/images/uploads/event_ticket/" . $ticket_id . "/logo.png");
        $pass->addFile($_SERVER["DOCUMENT_ROOT"] . "/public/images/uploads/event_ticket/" . $ticket_id . "/strip.png");

        //$pass->addFile("https://o2o-c3c-dev.artech-appmaker.com/public/images/testing/icon.png");
        //$pass->addFile("https://o2o-c3c-dev.artech-appmaker.com/public/images/testing/logo.png");
        //$pass->addFile("https://o2o-c3c-dev.artech-appmaker.com/public/images/testing/strip.png");

        if (!$pass->create(true)) {
            echo 'Error: ' . $pass->getError();
        }
    }

    public function google_handle($ticket_id, $holder_id, $return = true)
    {
        $service = new Services();
        $ticket_info = $this->Event_ticket_model->get_event_ticket($ticket_id);
        $layout_info = $this->Event_ticket_model->get_layout($ticket_id);
        $holder_info = $this->Event_ticket_memberbase_model->get_holder($holder_id);
        $vertical = "EVENTTICKET";
        $objectUid = $vertical . "_OBJECT_" . uniqid("", true);
        $objectId = sprintf("%s.%s", ISSUER_ID, $objectUid);
        $this->Event_ticket_memberbase_model->update_object_id($objectId, $holder_id);
        $link = $service->makeObject($ticket_id, $holder_id, $ticket_info, $holder_info, $layout_info, [], $objectId, $vertical);
        $this->Event_ticket_memberbase_model->store_url($link, $holder_id);
        QRcode::png("https://pay.google.com/gp/v/save/" . $link, $_SERVER["DOCUMENT_ROOT"] . "/assets/user_qr/event_ticket/" . $holder_id . "/google.png");
        $this->Event_ticket_memberbase_model->store_google_qr("/assets/user_qr/event_ticket/" . $holder_id . "/google.png", $holder_id);
        if ($link != null && $return === true) {
            redirect("https://pay.google.com/gp/v/save/" . $link);
        }
        return;
    }


    public function not_found()
    {
        $this->load->view("access_denied", []);
    }



    public function registration_callback($v1, $v2, $v3)
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            $post = json_decode(file_get_contents("php://input"), true);
            $this->Event_ticket_memberbase_model->update_device_token($v3, $post['pushToken']);
        }
    }

    public function registration_check($v1, $v2)
    {
        $get = $this->input->get();
        $serial = [];
        $holders = $this->Event_ticket_memberbase_model->get_apple_holders();
        foreach ($holders as $holder) {
            $serial[] = "eventticket_" . $holder['id'];
        }
        $response = array(
            "serialNumbers" => $serial,
            "lastUpdated" => strval((time() - 978307200))
        );
        echo json_encode($response);
    }

    public function card_info_update($v1, $v2)
    {
        $serial_code = explode("_", $v2);
        $holder = $this->Event_ticket_memberbase_model->get_holder($serial_code[1]);
        $this->apple_handle($holder['event_ticket_id'], $holder['id']);
    }

    public function retrieve_apple($id)
    {
        $user_info = $this->Event_ticket_memberbase_model->get_holder($id);

        $this->apple_handle($user_info['event_ticket_id'], $user_info['id']);
    }
}
