<?php

use PKPass\PKPass;

require_once "Config.php";
require_once "Services.php";
require dirname(__DIR__) . "/libraries/phpqrcode/qrlib.php";

use Sunaoka\PushNotifications\Drivers\APNs;
use Sunaoka\PushNotifications\Pusher;

class Couponregistration extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Coupon_model");
        $this->load->model("Coupon_holder_model");
        $this->load->model("Registration_model");
    }

    public function registration($id)
    {
        $data['coupon_setting'] = $this->Coupon_model->get_coupon($id);
        $data["form_setting"] = $this->Coupon_model->get_form_config($id);
        $this->load->view("coupon_registration_form", $data);
    }

    public function register()
    {
        $request = $this->input->post();
        $register_id = $this->Registration_model->coupon_registration($request);
        $hash = "";
        for ($i = 1; $i <= 8; $i++) {
            $hash .= sha1($hash) . sha1($hash . time());
        }
        $this->db->update("tbl_coupons_holder", ['hash' => $hash], ["id" => $register_id]);
        QRcode::png(base_url("coupon/retrieve/" . $register_id . "?v=" . $hash), $_SERVER["DOCUMENT_ROOT"] . "/assets/user_qr/coupon/" . $register_id . "_" . "QR.png");
        if ($request['platform'] == "apple") {
            $this->apple_handle($request['coupon_id'], $register_id);
        } else {
            $this->google_handle($request['coupon_id'], $register_id);
        }
    }

    public function apple_handle($coupon_id, $holder_id)
    {
        $pass = new PKPass("apple_pay_passes/" . $this->config->item("apple_cert"));
        $coupon_info = $this->Coupon_model->get_coupon($coupon_id);
        $locations_info = $this->Coupon_model->get_locations_setting($coupon_id);
        $layout_info = $this->Coupon_model->get_layout_config($coupon_id);
        $holder_info = $this->Coupon_holder_model->get_holder($holder_id);

        $location_trigger = 0;
        $locations_fields = [];
        for ($i = 1; $i <= 3; $i++) {
            if ($locations_info["location_" . $i . "_status"] == "1") {
                $location_trigger = 1;
                $locations_fields[] = [
                    "latitude" => floatval($locations_info['location_' . $i . "_lat"]),
                    "longitude" => floatval($locations_info["location_" . $i . "_lon"]),
                    "relevantText" => $locations_info['location_' . $i . "_message"]
                ];
            }
        }
        $front_fields = [];
        for ($i = 1; $i <= 4; $i++) {
            if ($layout_info['front_data_' . $i . '_status'] == 1) {
                switch (true) {
                    case $layout_info["front_data_" . $i . "_data"] == "gender":
                        $front_fields[] = array(
                            "key" => "sf" . $i,
                            "label" => $layout_info['front_data_' . $i . "_label"],
                            "value" => $holder_info[$layout_info["front_data_" . $i . "_data"]] == 1 ? "男" : "女"
                        );
                        break;

                    default:
                        $front_fields[] = array(
                            "key" => "sf" . $i,
                            "label" => $layout_info["front_data_" . $i . "_label"],
                            "value" => $holder_info[$layout_info["front_data_" . $i . "_data"]]
                        );
                        break;
                }
            }
        }
        $data = [
            "formatVersion" => 1,
            "passTypeIdentifier" => $this->config->item("topic"),
            "serialNumber" => "coupon_" . $holder_info['id'],
            "teamIdentifier" => $this->config->item("team_id"),
            "webServiceURL" => base_url() . "coupon/",
            "authenticationToken" => $this->config->item("auth_token"), 

            "organizationName" => $coupon_info["project_name"],
            "description" => $coupon_info["project_name"],
            "logoText" => $coupon_info['logo_text'],
            "foregroundColor" => $coupon_info["pri_color"],
            "labelColor" => $coupon_info['secondary_color'],
            "backgroundColor" => $coupon_info['background_color'],
            "barcodes" => array(
                array(
                    "message" => $holder_info['display_code'],
                    "altText" => $holder_info['display_code'],
                    "format" => "PKBarcodeFormatQR",
                    "messageEncoding" => "iso-8859-1"
                )
            ),
            "coupon" => array(
                "secondaryFields" => $front_fields,
                "backFields" => array(
                    array(
                        "key" => "bf1",
                        "value" => "",
                        "label" => "Our Website",
                        "attributedValue" => "<a href='" . $coupon_info['url'] . "'>" . $coupon_info['url'] . "</a>"
                    ),
                    array(
                        "key" => "bf2",
                        "value" => $coupon_info["about_us"],
                        "label" => "About Us",
                        //"attributedValue" => $coupon_info['about_us']
                    ),
                    array(
                        "key" => "bf3",
                        "value" => "",
                        "label" => "Terms & Conditions",
                        "attributedValue" => $coupon_info["terms_conditions"]
                    )
                )
            )
        ];
        if ($location_trigger == true) {
            $data["locations"] = $locations_fields;
        }
        $pass->setData($data);
        //var_dump($pass);
        //var_dump($data);
        //exit;
        $pass->addFile($_SERVER["DOCUMENT_ROOT"] . "/public/images/uploads/coupon/" . $coupon_id . "/icon.png");
        $pass->addFile($_SERVER["DOCUMENT_ROOT"] . "/public/images/uploads/coupon/" . $coupon_id . "/logo.png");
        $pass->addFile($_SERVER["DOCUMENT_ROOT"] . "/public/images/uploads/coupon/" . $coupon_id . "/strip.png");
        if (!$pass->create(true)) {
            echo 'Error: ' . $pass->getError();
        }
    }

    public function google_handle($coupon_id, $holder_id)
    {

        $service = new Services();
        $coupon_info = $this->Coupon_model->get_coupon($coupon_id);
        $layout_info = $this->Coupon_model->get_layout_config($coupon_id);
        $holder_info = $this->Coupon_holder_model->get_holder($holder_id);

        $vertical = "OFFER";
        $objectUid = $vertical . "_OBJECT_" . uniqid("", true);
        $objectId = sprintf("%s.%s", ISSUER_ID, $objectUid);
        $this->Coupon_holder_model->update_object_id($objectId, $holder_id);

        $link = $service->makeObject($coupon_id, $holder_id, $coupon_info, $holder_info, $layout_info, [], $objectId, $vertical);

        if ($link != null) {
            redirect("https://pay.google.com/gp/v/save/" . $link);
        }
        return;
    }

    public function registration_callback($v1, $v2, $v3)
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            $post = json_decode(file_get_contents("php://input"), true);
            $this->Coupon_holder_model->update_device_token($v3, $post['pushToken']);
        }
    }

    public function card_info_update($v1, $v2)
    {
        $serial_code = explode("_", $v2);
        $holder = $this->Coupon_holder_model->get_holder($serial_code[1]);
        $this->apple_handle($holder['coupon_id'], $holder['id']);
    }

    public function registration_check($v1, $v2)
    {

        $get = $this->input->get();
        $serial = [];
        $holders = $this->Coupon_holder_model->get_apple_holders();
        foreach ($holders as $holder) {
            $serial[] = "coupon_" . $holder['id'];
        }
        $response = array(
            "serialNumbers" => $serial,
            "lastUpdated" => strval((time() - 978307200))
        );
        echo json_encode($response);
    }

    public function retrieve($id)
    {
        $hash = $this->input->get("v");
        if ($hash == "") {
            echo "Access Denied";
            exit;
        }
        $result = $this->db->select("id,coupon_id,platform,object_id,hash")->from("tbl_coupons_holder")->where('id', $id)->get()->row_array();
        if ($result == "") {
            echo "Access Denied";
            exit;
        }
        if ($result['hash'] != $hash) {
            echo "Access Denied";
            exit;
        }
        if ($result['platform'] == "apple") {
            $this->apple_handle($result['coupon_id'], $result['id']);
        } else {

            $services = new Services();
            $restMethods = RestMethods::getInstance();
            //$result = $restMethods->getOfferObject($result['object_id']);
            $googlePassJwt = new GpapJwt();
            $googlePassJwt->addOfferObject(array("id" => $result['object_id']));
            $signedJwt = $googlePassJwt->generateSignedJwt();
            redirect("https://pay.google.com/gp/v/save/" . $signedJwt);
        }
    }
}
