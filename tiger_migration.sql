-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 2023-02-24 11:29:07
-- 伺服器版本： 10.4.18-MariaDB
-- PHP 版本： 7.4.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `tiger_migration`
--

-- --------------------------------------------------------

--
-- 資料表結構 `atadmin_cms_user_activity_2021`
--

CREATE TABLE `atadmin_cms_user_activity_2021` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `entered` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 資料表結構 `tbltemp_card`
--

CREATE TABLE `tbltemp_card` (
  `id` int(11) NOT NULL,
  `logo_text` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `strip` varchar(2555) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `pri_label` varchar(255) NOT NULL,
  `pri_value` varchar(255) NOT NULL,
  `sec_label_1` varchar(255) NOT NULL,
  `sec_value_1` varchar(255) NOT NULL,
  `sec_label_2` varchar(255) NOT NULL,
  `sec_value_2` varchar(255) NOT NULL,
  `sec_label_3` varchar(255) NOT NULL,
  `sec_value_3` varchar(255) NOT NULL,
  `pri_color` varchar(255) NOT NULL,
  `secondary_color` varchar(255) NOT NULL,
  `background_color` varchar(255) NOT NULL,
  `stripColor` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `about_us` text NOT NULL,
  `terms_conditions` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_api_testing`
--

CREATE TABLE `tbl_api_testing` (
  `id` int(11) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `create_time` int(11) NOT NULL,
  `delete_time` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_company`
--

CREATE TABLE `tbl_company` (
  `id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL DEFAULT '',
  `company_email` varchar(255) NOT NULL DEFAULT '',
  `company_hotline` varchar(50) NOT NULL DEFAULT '',
  `company_industry` varchar(100) NOT NULL DEFAULT '',
  `company_website` varchar(255) NOT NULL DEFAULT '',
  `deleted_at` int(11) NOT NULL DEFAULT 0,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_at` int(11) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 傾印資料表的資料 `tbl_company`
--

INSERT INTO `tbl_company` (`id`, `company_name`, `company_email`, `company_hotline`, `company_industry`, `company_website`, `deleted_at`, `updated_by`, `updated_at`, `created_by`, `created_at`) VALUES
(1, 'Smart Business Consultancy Limited', 'info@smartbusiness.com.hk', '+852 3611 9223', 'it', 'https://www.smartbusiness.com.hk/', 0, 2, 1637763554, 2, 1637763554),
(2, 'KM Studio Co.', 'test1@example.com', '12345678', 'artwork', 'test.example.com', 0, 2, 1637765236, 2, 1637765236),
(5, 'Youth Retirement Investment Co. Ltd', 'test2@example.com', '12345678', 'automobile', 'test.example.com', 0, 2, 1637834328, 2, 1637834328),
(6, 'Test Company', 'test2@example.com', '12345678', 'beauty', 'test.example.com', 0, 2, 1637859957, 2, 1637859957),
(7, 'Merchant', 'Merchant@b.com', '32132333', '', 'Merchant.com', 0, 2, 1637867474, 2, 1637867474),
(8, 'Merchant2', 'Merchant2@example.con', '12345678', 'fashion', 'Merchant2.com', 0, 2, 1638105721, 2, 1638105721),
(9, 'Merchant1', 'Merchant2@example.con', '12345678', 'fashion', 'Merchant2.com', 0, 2, 1638105731, 2, 1638105731),
(10, 'Merchant3', 'Merchant3@example.com', '12345678', 'artwork', 'Merchant3.com', 0, 2, 1638106637, 2, 1638106637),
(11, 'LifeWay Regain Insurance', 'LifeWay@gmail.com', '34879970', 'finance', 'LifeWay.com', 0, 2, 1638379294, 2, 1638379294),
(12, 'Smart Business IT Consultancy Limited', 'smartbusiness@gmail.com', '93278132', 'ecommerce', '', 0, 2, 1638464279, 2, 1638464279),
(13, 'mtm labo', 'mtm@gmail.com', '98738667', 'ecommerce', '', 0, 2, 1638464367, 2, 1638464367),
(14, 'Hair Again 2000', 'hairagain@gmail.com', '98736821', 'health', '', 0, 2, 1638464448, 2, 1638464448),
(15, 'Testing Company', 'edward@at-creative', '34288328', 'it', '', 0, 2, 1638599034, 2, 1638599034),
(16, 'LifeWay', 'lifeway@gmail.com', '23307798', 'finance', '', 0, 2, 1639288576, 2, 1639288576),
(17, 'LifeWay', 'lifeway@gmail.com', '23366789', 'finance', '', 0, 2, 1639288856, 2, 1639288856),
(18, 'LifeWay Regain Insurance', 'LifeWay@gmail.com', '34879970', 'finance', 'LifeWay.com', 0, 2, 1639289389, 2, 1639289389),
(19, 'testing123', 'testing123@example.com', '12345678', 'artwork', 'testing123.example.com', 0, 2, 1639385688, 2, 1639385688),
(20, 'testing12345', 'testing123@example.com', '12345678', 'artwork', 'testing123.example.com', 0, 2, 1639386224, 2, 1639386224),
(21, 'Choi Fung Hong Company Limited', 'sales@choi-fung.com', '24630318', 'beauty', 'www.choi-fung.com', 0, 2, 1639425151, 2, 1639425151),
(22, 'New', 'edward@at-creative', '34288328', 'it', '', 0, 2, 1657684348, 2, 1657684348),
(23, 'HKMU', 'hkmu@gmail.com', '23389022', 'support', '', 0, 2, 1664518406, 2, 1664518406),
(24, 'Meta Space', 'metaspace@gmail.com', '00000000', 'ecommerce', '', 0, 2, 1664638904, 2, 1664638904),
(25, 'Zebra Wellness', '', '', 'health', '', 0, 2, 1666099786, 2, 1666099786),
(26, 'Celine', 'celine@gmail.com', '24499088', 'fashion', '', 0, 2, 1666161787, 2, 1666161787),
(27, 'Orix Finance', '', '', '', '', 0, 2, 1666231205, 2, 1666231205),
(28, 'Manulife', '', '', '', '', 0, 2, 1666340217, 2, 1666340217),
(29, 'Tiger Web', 'tigerweb@gmail.com', '12345678', 'others', '', 0, 2, 1667802925, 2, 1667802925);

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_contact`
--

CREATE TABLE `tbl_contact` (
  `id` int(11) NOT NULL,
  `contact_person` varchar(255) NOT NULL DEFAULT '',
  `contact_title` varchar(50) NOT NULL DEFAULT '',
  `contact_position` varchar(255) NOT NULL DEFAULT '',
  `contact_phone` varchar(50) NOT NULL DEFAULT '',
  `contact_email` varchar(255) NOT NULL DEFAULT '',
  `deleted_at` int(11) NOT NULL DEFAULT 0,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_at` int(11) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 傾印資料表的資料 `tbl_contact`
--

INSERT INTO `tbl_contact` (`id`, `contact_person`, `contact_title`, `contact_position`, `contact_phone`, `contact_email`, `deleted_at`, `updated_by`, `updated_at`, `created_by`, `created_at`) VALUES
(1, 'Karis Yeung', 'Miss', '', '+852 95472118', 'smartbiz@gmail.com', 0, 2, 1637763554, 2, 1637763554),
(2, 'Test1', 'Mr', '', '12345678', 'test@example.com', 0, 2, 1637765236, 2, 1637765236),
(5, 'Test2', 'Mr', '', '12345678', 'test2@example.com', 0, 2, 1637834328, 2, 1637834328),
(6, 'Test', 'Ms', '', '12345678', 'Test@test.com', 0, 2, 1637859957, 2, 1637859957),
(7, 'Merchant', 'Mr', '', '34288328', 'Merchant@b.com', 0, 2, 1637867474, 2, 1637867474),
(8, 'Merchant2', 'Mr', '', '12345678', 'Merchant2@example.com', 0, 2, 1638105721, 2, 1638105721),
(9, 'Merchant1', 'Mr', '', '12345678', 'Merchant2@example.com', 0, 2, 1638105731, 2, 1638105731),
(10, 'Merchant3', 'Mr', '', '12345678', 'Merchant3@example.com', 0, 2, 1638106637, 2, 1638106637),
(11, 'David', 'Mr', '', '96783427', 'david@gmail.com', 0, 2, 1638379294, 2, 1638379294),
(12, 'David', 'Mr', '', '98647368', 'david@gmail.com', 0, 2, 1638464279, 2, 1638464279),
(13, 'May', 'Miss', '', '96683346', 'may@gmail.com', 0, 2, 1638464367, 2, 1638464367),
(14, 'Sally', 'Miss', '', '98876557', 'sally@gmail.com', 0, 2, 1638464448, 2, 1638464448),
(15, 'edward', 'Mr', '', '34288328', 'edward@at-creative', 0, 2, 1638599034, 2, 1638599034),
(16, 'Roy', 'Mr', '', '98876445', 'roy@gmail.com', 0, 2, 1639288576, 2, 1639288576),
(17, 'David', 'Mr', '', '98007866', 'david@gmail.com', 0, 2, 1639288856, 2, 1639288856),
(18, 'David', 'Mr', '', '96783427', 'david@gmail.com', 0, 2, 1639289389, 2, 1639289389),
(19, 'testing123', 'Mr', 'testing123', '12345678', 'testing123@example.com', 0, 2, 1639385688, 2, 1639385688),
(20, 'testing123', 'Mr', 'testing123', '12345678', 'testing123@example.com', 0, 2, 1639386224, 2, 1639386224),
(21, 'Joyce', 'Ms', 'Staff', '24630318', 'sales@choi-fung.com', 0, 2, 1639425151, 2, 1639425151),
(22, 'edward', 'Mr', '', '34288328', 'edward@at-creative.com', 0, 2, 1657684348, 2, 1657684348),
(23, 'Kelly Li ', 'Ms', '', '56889567', 'kelly@gmail.com', 0, 2, 1664518406, 2, 1664518406),
(24, 'Spencer', 'Mr', '', '00000000', 'Spencer@gmail.com', 0, 2, 1664638904, 2, 1664638904),
(25, 'Amy Chan', 'Miss', '', '', '', 0, 2, 1666099786, 2, 1666099786),
(26, 'Lisa', 'Miss', '', '65577345', 'lisa@gmail.com', 0, 2, 1666161787, 2, 1666161787),
(27, 'Orix Finance', 'Mr', '', '', '', 0, 2, 1666231205, 2, 1666231205),
(28, 'Manulife', 'Mr', '', '', '', 0, 2, 1666340217, 2, 1666340217),
(29, 'Tiger web', 'Mr', '', '12345678', 'tigerweb@gmail.com', 0, 2, 1667802925, 2, 1667802925);

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_coupons`
--

CREATE TABLE `tbl_coupons` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `management_name` varchar(1000) CHARACTER SET utf8mb4 NOT NULL,
  `project_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `coupon_code` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `coupon_amount` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `expiry_date` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `icon` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `strip` varchar(255) NOT NULL,
  `logo_text` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `pri_color` varchar(255) NOT NULL,
  `secondary_color` varchar(255) NOT NULL,
  `background_color` varchar(255) NOT NULL,
  `data_requirement` varchar(255) NOT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `about_us` text CHARACTER SET utf8mb4 NOT NULL,
  `terms_conditions` text CHARACTER SET utf8mb4 NOT NULL,
  `class_uid` varchar(10000) NOT NULL,
  `display` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_coupons_data_config`
--

CREATE TABLE `tbl_coupons_data_config` (
  `id` int(11) NOT NULL,
  `front_data_1_status` tinyint(1) NOT NULL,
  `front_data_2_status` tinyint(1) NOT NULL,
  `front_data_3_status` tinyint(1) NOT NULL,
  `front_data_4_status` tinyint(1) NOT NULL,
  `front_data_1_data` varchar(255) NOT NULL,
  `front_data_2_data` varchar(255) NOT NULL,
  `front_data_3_data` varchar(255) NOT NULL,
  `front_data_4_data` varchar(255) NOT NULL,
  `front_data_1_label` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `front_data_2_label` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `front_data_3_label` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `front_data_4_label` varchar(255) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_coupons_e_form_config`
--

CREATE TABLE `tbl_coupons_e_form_config` (
  `id` int(11) NOT NULL,
  `name` tinyint(1) NOT NULL,
  `surname` tinyint(1) NOT NULL,
  `given_name` tinyint(1) NOT NULL,
  `english_name` tinyint(1) NOT NULL,
  `chinese_name` tinyint(1) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `birthday` tinyint(1) NOT NULL,
  `age` tinyint(1) NOT NULL,
  `nationality` tinyint(1) NOT NULL,
  `phone` tinyint(1) NOT NULL,
  `email` tinyint(1) NOT NULL,
  `address` tinyint(1) NOT NULL,
  `industry` tinyint(1) NOT NULL,
  `industry_2` tinyint(1) NOT NULL,
  `job_title` tinyint(1) NOT NULL,
  `company` tinyint(1) NOT NULL,
  `salary` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_coupons_holder`
--

CREATE TABLE `tbl_coupons_holder` (
  `id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `display_code` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `coupon_series` int(11) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `coupon_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `coupon_amount` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `surname` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `given_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `english_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `chinese_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `gender` varchar(1) CHARACTER SET utf8mb4 NOT NULL,
  `birthday` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `age` int(11) NOT NULL,
  `nationality` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `industry` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `industry_2` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `job_title` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `company` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `salary` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `status` varchar(255) NOT NULL,
  `void_date` varchar(255) NOT NULL,
  `create_date` varchar(255) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `device_token` varchar(255) NOT NULL,
  `object_id` varchar(255) NOT NULL,
  `hash` varchar(4096) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_coupons_locations`
--

CREATE TABLE `tbl_coupons_locations` (
  `id` int(11) NOT NULL,
  `location_1_status` tinyint(1) NOT NULL,
  `location_1_lat` double NOT NULL,
  `location_1_lon` double NOT NULL,
  `location_1_message` text CHARACTER SET utf8mb4 NOT NULL,
  `location_2_status` tinyint(1) NOT NULL,
  `location_2_lat` double NOT NULL,
  `location_2_lon` double NOT NULL,
  `location_2_message` text CHARACTER SET utf8mb4 NOT NULL,
  `location_3_status` tinyint(1) NOT NULL,
  `location_3_lat` double NOT NULL,
  `location_3_lon` double NOT NULL,
  `location_3_message` text CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_event_ticket`
--

CREATE TABLE `tbl_event_ticket` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `event_ticket_type` tinyint(1) NOT NULL,
  `management_name` varchar(1000) CHARACTER SET utf8mb4 NOT NULL,
  `event_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `issuer_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `event_code` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `event_start_time` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `event_end_time` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `event_date` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `event_venue` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `location_detail` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `label_color` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `content_color` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `background_color` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `logo` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `strip` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `email_banner` varchar(1000) CHARACTER SET utf8mb4 NOT NULL,
  `logo_text` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `data_requirement` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `about_us` text CHARACTER SET utf8mb4 NOT NULL,
  `terms_conditions` text CHARACTER SET utf8mb4 NOT NULL,
  `event_type` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `display` tinyint(1) NOT NULL DEFAULT 1,
  `class_uid` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `ref_key` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_event_ticket_form`
--

CREATE TABLE `tbl_event_ticket_form` (
  `id` int(11) NOT NULL,
  `name` tinyint(1) NOT NULL,
  `surname` tinyint(1) NOT NULL,
  `given_name` tinyint(1) NOT NULL,
  `english_name` tinyint(1) NOT NULL,
  `chinese_name` tinyint(1) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `birthday` tinyint(1) NOT NULL,
  `age` tinyint(1) NOT NULL,
  `nationality` tinyint(1) NOT NULL,
  `phone` tinyint(1) NOT NULL,
  `email` tinyint(1) NOT NULL,
  `address` tinyint(1) NOT NULL,
  `industry` tinyint(1) NOT NULL,
  `industry_2` tinyint(1) NOT NULL,
  `job_title` tinyint(1) NOT NULL,
  `company` tinyint(1) NOT NULL,
  `salary` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_event_ticket_holder`
--

CREATE TABLE `tbl_event_ticket_holder` (
  `id` int(11) NOT NULL,
  `event_ticket_id` int(11) NOT NULL,
  `display_code` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `event_code_series` int(11) NOT NULL,
  `row` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `seat` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `section` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `surname` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `given_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `english_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `chinese_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `birthday` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `age` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `nationality` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `industry` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `industry_2` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `job_title` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `company` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `salary` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT 'valid',
  `create_date` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `platform` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `device_token` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `object_id` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `enter_time` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `quit_time` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `apple_qr` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `google_qr` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `google_re` text CHARACTER SET utf8mb4 NOT NULL,
  `hash` text NOT NULL,
  `ref_id` varchar(255) NOT NULL,
  `api` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_event_ticket_layout`
--

CREATE TABLE `tbl_event_ticket_layout` (
  `id` int(11) NOT NULL,
  `front_data_1_status` tinyint(1) NOT NULL,
  `front_data_2_status` tinyint(1) NOT NULL,
  `front_data_3_status` tinyint(1) NOT NULL,
  `front_data_4_status` tinyint(1) NOT NULL,
  `front_data_5_status` tinyint(1) NOT NULL,
  `front_data_6_status` tinyint(1) NOT NULL,
  `front_data_1_label` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `front_data_2_label` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `front_data_3_label` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `front_data_4_label` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `front_data_5_label` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `front_data_6_label` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `front_data_1_data` varchar(255) NOT NULL,
  `front_data_2_data` varchar(255) NOT NULL,
  `front_data_3_data` varchar(255) NOT NULL,
  `front_data_4_data` varchar(255) NOT NULL,
  `front_data_5_data` varchar(255) NOT NULL,
  `front_data_6_data` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_google_registration`
--

CREATE TABLE `tbl_google_registration` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `object_uid` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_members`
--

CREATE TABLE `tbl_members` (
  `id` int(11) NOT NULL COMMENT 'The actual serial number for card update',
  `pass_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL COMMENT 'Just for customer to display their member serial number',
  `name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `surname` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `given_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `english_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `chinese_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `birthday` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `age` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `industry` varchar(255) NOT NULL,
  `industry_2` varchar(255) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `salary` varchar(255) NOT NULL,
  `status` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT 'valid',
  `level` varchar(255) NOT NULL,
  `level_api` varchar(255) NOT NULL,
  `ref_id` varchar(1000) NOT NULL,
  `api` tinyint(1) NOT NULL,
  `points` int(11) NOT NULL,
  `platform` varchar(255) CHARACTER SET latin1 NOT NULL,
  `create_date` varchar(255) NOT NULL,
  `hash` varchar(4096) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_merchants`
--

CREATE TABLE `tbl_merchants` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL DEFAULT 0,
  `contact_id` int(11) NOT NULL DEFAULT 0,
  `merchant_id` varchar(255) NOT NULL DEFAULT '' COMMENT 'merchant_username',
  `merchant_password` varchar(255) NOT NULL DEFAULT '',
  `merchant_plan` varchar(100) NOT NULL DEFAULT '',
  `sales` int(11) NOT NULL,
  `merchant_valid_until` int(11) NOT NULL DEFAULT 0,
  `force_disabled` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` int(11) NOT NULL DEFAULT 0,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_at` int(11) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` int(11) NOT NULL DEFAULT 0,
  `api_key` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 傾印資料表的資料 `tbl_merchants`
--

INSERT INTO `tbl_merchants` (`id`, `company_id`, `contact_id`, `merchant_id`, `merchant_password`, `merchant_plan`, `sales`, `merchant_valid_until`, `force_disabled`, `deleted_at`, `updated_by`, `updated_at`, `created_by`, `created_at`, `api_key`) VALUES
(24, 29, 29, 'tigerweb', '123456', 'Enterprise', 0, 2147483647, 0, 0, 2, 1667802925, 2, 1667802925, 's3dw8m4U8p013ccszewSyhYO3Q38dUcvLmFlDLocCFWfIbC6keZzPWYaAhSAyQsc');

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_merchants_old`
--

CREATE TABLE `tbl_merchants_old` (
  `id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL DEFAULT '',
  `company_email` varchar(255) NOT NULL DEFAULT '',
  `company_hotline` varchar(50) NOT NULL DEFAULT '',
  `company_industry` varchar(100) NOT NULL DEFAULT '',
  `company_website` varchar(255) NOT NULL DEFAULT '',
  `merchant_id` varchar(255) NOT NULL DEFAULT '' COMMENT 'merchant_username',
  `merchant_password` varchar(255) NOT NULL DEFAULT '',
  `merchant_plan` varchar(100) NOT NULL DEFAULT '',
  `merchant_valid_until` int(11) NOT NULL DEFAULT 0,
  `contact_person` varchar(255) NOT NULL DEFAULT '',
  `contact_title` varchar(50) NOT NULL DEFAULT '',
  `contact_position` varchar(255) NOT NULL DEFAULT '',
  `contact_phone` varchar(50) NOT NULL DEFAULT '',
  `contact_email` varchar(255) NOT NULL DEFAULT '',
  `deleted_at` int(11) NOT NULL DEFAULT 0,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `updated_at` int(11) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `created_at` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_merchant_email`
--

CREATE TABLE `tbl_merchant_email` (
  `id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `sum` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_merchant_email_record`
--

CREATE TABLE `tbl_merchant_email_record` (
  `id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `customer_type` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `customer_id` int(11) NOT NULL,
  `sent_date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_session_admin`
--

CREATE TABLE `tbl_session_admin` (
  `id` int(11) NOT NULL,
  `session_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL DEFAULT 0,
  `ipaddress` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `logout` int(11) NOT NULL DEFAULT 0,
  `updated` timestamp NOT NULL DEFAULT current_timestamp(),
  `entered` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_temp_google_card`
--

CREATE TABLE `tbl_temp_google_card` (
  `id` int(11) NOT NULL,
  `link` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_temp_registration`
--

CREATE TABLE `tbl_temp_registration` (
  `id` int(11) NOT NULL,
  `devicelibraryidentifier` varchar(4000) NOT NULL,
  `passtypeidentifier` varchar(4000) NOT NULL,
  `serialnumber` varchar(4000) NOT NULL,
  `pushToken` varchar(4000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_testing_notification`
--

CREATE TABLE `tbl_testing_notification` (
  `id` int(11) NOT NULL,
  `serial` varchar(255) NOT NULL,
  `message` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `timestamp` int(11) NOT NULL,
  `sent` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_update_testing`
--

CREATE TABLE `tbl_update_testing` (
  `id` int(11) NOT NULL,
  `passesUpdatedSince` varchar(1000) NOT NULL,
  `pass_id` varchar(255) NOT NULL,
  `devicelibraryidentifier` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `image` text DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL COMMENT 'tigerapi'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tbl_admin, tablename not changed due to the use of security model';

--
-- 傾印資料表的資料 `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `username`, `email`, `image`, `password`) VALUES
(2, 'admin', NULL, NULL, '80847822b6a5923a43004ba9e6eb354f');

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_virtual_passes`
--

CREATE TABLE `tbl_virtual_passes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `management_name` varchar(255) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `issuer` varchar(255) NOT NULL,
  `logo_text_display` tinyint(1) NOT NULL,
  `logo_text` varchar(255) NOT NULL,
  `header_data` varchar(255) NOT NULL,
  `logo` varchar(255) CHARACTER SET latin1 NOT NULL,
  `strip` varchar(255) CHARACTER SET latin1 NOT NULL,
  `icon` varchar(255) CHARACTER SET latin1 NOT NULL,
  `description` varchar(255) NOT NULL,
  `pri_label` varchar(255) NOT NULL,
  `pri_value` varchar(255) NOT NULL,
  `sec_label_1` varchar(255) NOT NULL,
  `sec_value_1` varchar(255) CHARACTER SET latin1 NOT NULL,
  `sec_label_2` varchar(255) NOT NULL,
  `sec_value_2` varchar(255) CHARACTER SET latin1 NOT NULL,
  `sec_label_3` varchar(255) NOT NULL,
  `sec_value_3` varchar(255) CHARACTER SET latin1 NOT NULL,
  `pri_color` varchar(255) CHARACTER SET latin1 NOT NULL,
  `secondary_color` varchar(255) CHARACTER SET latin1 NOT NULL,
  `background_color` varchar(255) CHARACTER SET latin1 NOT NULL,
  `stripColor` varchar(255) CHARACTER SET latin1 NOT NULL,
  `url` varchar(255) NOT NULL,
  `about_us` text NOT NULL,
  `terms_conditions` text NOT NULL,
  `data_requirement` varchar(255) NOT NULL,
  `default_level` int(11) NOT NULL,
  `class_uid` varchar(1000) CHARACTER SET latin1 NOT NULL,
  `ref_key` varchar(1000) NOT NULL,
  `start_from` int(11) NOT NULL DEFAULT 1,
  `display` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_virtual_passes_data_config`
--

CREATE TABLE `tbl_virtual_passes_data_config` (
  `id` int(11) NOT NULL,
  `front_data_1_status` tinyint(1) NOT NULL,
  `front_data_2_status` tinyint(1) NOT NULL,
  `front_data_3_status` tinyint(1) NOT NULL,
  `front_data_4_status` tinyint(1) NOT NULL,
  `front_data_5_status` tinyint(1) NOT NULL,
  `front_data_6_status` tinyint(1) NOT NULL,
  `front_data_1_data` varchar(255) NOT NULL,
  `front_data_2_data` varchar(255) NOT NULL,
  `front_data_3_data` varchar(255) NOT NULL,
  `front_data_4_data` varchar(255) NOT NULL,
  `front_data_5_data` varchar(255) NOT NULL,
  `front_data_6_data` varchar(255) NOT NULL,
  `front_data_1_label` varchar(255) NOT NULL,
  `front_data_2_label` varchar(255) NOT NULL,
  `front_data_3_label` varchar(255) NOT NULL,
  `front_data_4_label` varchar(255) NOT NULL,
  `front_data_5_label` varchar(255) NOT NULL,
  `front_data_6_label` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_virtual_passes_levels`
--

CREATE TABLE `tbl_virtual_passes_levels` (
  `id` int(11) NOT NULL,
  `pass_id` int(11) NOT NULL,
  `value` text CHARACTER SET utf8mb4 NOT NULL,
  `default_value` tinyint(1) NOT NULL,
  `display` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_virtual_passes_locations`
--

CREATE TABLE `tbl_virtual_passes_locations` (
  `id` int(11) NOT NULL,
  `location_1_status` tinyint(1) NOT NULL,
  `location_1_lat` double NOT NULL,
  `location_1_lon` double NOT NULL,
  `location_1_message` text CHARACTER SET utf8mb4 NOT NULL,
  `location_2_status` tinyint(1) NOT NULL,
  `location_2_lat` double NOT NULL,
  `location_2_lon` double NOT NULL,
  `location_2_message` text CHARACTER SET utf8mb4 NOT NULL,
  `location_3_status` tinyint(1) NOT NULL,
  `location_3_lat` double NOT NULL,
  `location_3_lon` double NOT NULL,
  `location_3_message` text CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `tbl_virtual_pass_e_form_config`
--

CREATE TABLE `tbl_virtual_pass_e_form_config` (
  `id` int(11) NOT NULL,
  `name` tinyint(1) NOT NULL,
  `surname` tinyint(1) NOT NULL,
  `given_name` tinyint(1) NOT NULL,
  `english_name` tinyint(1) NOT NULL,
  `chinese_name` tinyint(1) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `birthday` tinyint(1) NOT NULL,
  `age` tinyint(1) NOT NULL,
  `nationality` tinyint(1) NOT NULL,
  `phone` tinyint(1) NOT NULL,
  `email` tinyint(1) NOT NULL,
  `address` tinyint(1) NOT NULL,
  `industry` tinyint(1) NOT NULL,
  `industry_2` tinyint(1) NOT NULL,
  `job_title` tinyint(1) NOT NULL,
  `company` tinyint(1) NOT NULL,
  `salary` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `atadmin_cms_user_activity_2021`
--
ALTER TABLE `atadmin_cms_user_activity_2021`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbltemp_card`
--
ALTER TABLE `tbltemp_card`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_api_testing`
--
ALTER TABLE `tbl_api_testing`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_company`
--
ALTER TABLE `tbl_company`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_contact`
--
ALTER TABLE `tbl_contact`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_coupons`
--
ALTER TABLE `tbl_coupons`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_coupons_data_config`
--
ALTER TABLE `tbl_coupons_data_config`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_coupons_e_form_config`
--
ALTER TABLE `tbl_coupons_e_form_config`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_coupons_holder`
--
ALTER TABLE `tbl_coupons_holder`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_coupons_locations`
--
ALTER TABLE `tbl_coupons_locations`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_event_ticket`
--
ALTER TABLE `tbl_event_ticket`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_event_ticket_form`
--
ALTER TABLE `tbl_event_ticket_form`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_event_ticket_holder`
--
ALTER TABLE `tbl_event_ticket_holder`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_event_ticket_layout`
--
ALTER TABLE `tbl_event_ticket_layout`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_google_registration`
--
ALTER TABLE `tbl_google_registration`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_members`
--
ALTER TABLE `tbl_members`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_merchants`
--
ALTER TABLE `tbl_merchants`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_merchants_old`
--
ALTER TABLE `tbl_merchants_old`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_merchant_email`
--
ALTER TABLE `tbl_merchant_email`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_merchant_email_record`
--
ALTER TABLE `tbl_merchant_email_record`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_session_admin`
--
ALTER TABLE `tbl_session_admin`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_temp_google_card`
--
ALTER TABLE `tbl_temp_google_card`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_temp_registration`
--
ALTER TABLE `tbl_temp_registration`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_testing_notification`
--
ALTER TABLE `tbl_testing_notification`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_update_testing`
--
ALTER TABLE `tbl_update_testing`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- 資料表索引 `tbl_virtual_passes`
--
ALTER TABLE `tbl_virtual_passes`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_virtual_passes_data_config`
--
ALTER TABLE `tbl_virtual_passes_data_config`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_virtual_passes_levels`
--
ALTER TABLE `tbl_virtual_passes_levels`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_virtual_passes_locations`
--
ALTER TABLE `tbl_virtual_passes_locations`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `tbl_virtual_pass_e_form_config`
--
ALTER TABLE `tbl_virtual_pass_e_form_config`
  ADD PRIMARY KEY (`id`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `atadmin_cms_user_activity_2021`
--
ALTER TABLE `atadmin_cms_user_activity_2021`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbltemp_card`
--
ALTER TABLE `tbltemp_card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_api_testing`
--
ALTER TABLE `tbl_api_testing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_company`
--
ALTER TABLE `tbl_company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_contact`
--
ALTER TABLE `tbl_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_coupons`
--
ALTER TABLE `tbl_coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_coupons_holder`
--
ALTER TABLE `tbl_coupons_holder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_event_ticket`
--
ALTER TABLE `tbl_event_ticket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_event_ticket_holder`
--
ALTER TABLE `tbl_event_ticket_holder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_google_registration`
--
ALTER TABLE `tbl_google_registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_members`
--
ALTER TABLE `tbl_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'The actual serial number for card update';

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_merchants`
--
ALTER TABLE `tbl_merchants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_merchants_old`
--
ALTER TABLE `tbl_merchants_old`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_merchant_email`
--
ALTER TABLE `tbl_merchant_email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_merchant_email_record`
--
ALTER TABLE `tbl_merchant_email_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_session_admin`
--
ALTER TABLE `tbl_session_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_temp_google_card`
--
ALTER TABLE `tbl_temp_google_card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_temp_registration`
--
ALTER TABLE `tbl_temp_registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_testing_notification`
--
ALTER TABLE `tbl_testing_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_update_testing`
--
ALTER TABLE `tbl_update_testing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_virtual_passes`
--
ALTER TABLE `tbl_virtual_passes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `tbl_virtual_passes_levels`
--
ALTER TABLE `tbl_virtual_passes_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
