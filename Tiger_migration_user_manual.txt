1: Place the apple certificate.
	Location: /apple_pay_passes
	Instruction: place the p12 file and p8 file here and copy their name and extension for step 3.

2 Place the Google certificate.
	Location: /certificates
	Instruction: place json file here and copy the name and  extension for step 4

3: Set the params.
	Location:/application/config/config.php
	Line:27-36

4: Set the config file
	Location:  /application/controllers/Config.php
	Line:29,32,35,38,42

5 Set database.
	Location:/application/config/database.php
	Line,79,80,81
